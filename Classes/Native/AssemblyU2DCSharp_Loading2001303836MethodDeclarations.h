﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Loading
struct Loading_t2001303836;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void Loading::.ctor()
extern "C"  void Loading__ctor_m3414177231 (Loading_t2001303836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loading::Start()
extern "C"  void Loading_Start_m2361315023 (Loading_t2001303836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Loading::Load()
extern "C"  Il2CppObject * Loading_Load_m1257196979 (Loading_t2001303836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
