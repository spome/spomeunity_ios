﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t461342257;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Object
struct Il2CppObject;
// AlbumPathCheck
struct AlbumPathCheck_t1505991252;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AlbumPathCheck/<Send>c__Iterator2
struct  U3CSendU3Ec__Iterator2_t611604662  : public Il2CppObject
{
public:
	// UnityEngine.WWWForm AlbumPathCheck/<Send>c__Iterator2::<form>__0
	WWWForm_t461342257 * ___U3CformU3E__0_0;
	// System.String AlbumPathCheck/<Send>c__Iterator2::title
	String_t* ___title_1;
	// System.String AlbumPathCheck/<Send>c__Iterator2::filename
	String_t* ___filename_2;
	// System.String AlbumPathCheck/<Send>c__Iterator2::addr01
	String_t* ___addr01_3;
	// System.String AlbumPathCheck/<Send>c__Iterator2::addr02
	String_t* ___addr02_4;
	// UnityEngine.WWW AlbumPathCheck/<Send>c__Iterator2::<w>__1
	WWW_t3134621005 * ___U3CwU3E__1_5;
	// System.Int32 AlbumPathCheck/<Send>c__Iterator2::$PC
	int32_t ___U24PC_6;
	// System.Object AlbumPathCheck/<Send>c__Iterator2::$current
	Il2CppObject * ___U24current_7;
	// System.String AlbumPathCheck/<Send>c__Iterator2::<$>title
	String_t* ___U3CU24U3Etitle_8;
	// System.String AlbumPathCheck/<Send>c__Iterator2::<$>filename
	String_t* ___U3CU24U3Efilename_9;
	// System.String AlbumPathCheck/<Send>c__Iterator2::<$>addr01
	String_t* ___U3CU24U3Eaddr01_10;
	// System.String AlbumPathCheck/<Send>c__Iterator2::<$>addr02
	String_t* ___U3CU24U3Eaddr02_11;
	// AlbumPathCheck AlbumPathCheck/<Send>c__Iterator2::<>f__this
	AlbumPathCheck_t1505991252 * ___U3CU3Ef__this_12;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSendU3Ec__Iterator2_t611604662, ___U3CformU3E__0_0)); }
	inline WWWForm_t461342257 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t461342257 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t461342257 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CformU3E__0_0, value);
	}

	inline static int32_t get_offset_of_title_1() { return static_cast<int32_t>(offsetof(U3CSendU3Ec__Iterator2_t611604662, ___title_1)); }
	inline String_t* get_title_1() const { return ___title_1; }
	inline String_t** get_address_of_title_1() { return &___title_1; }
	inline void set_title_1(String_t* value)
	{
		___title_1 = value;
		Il2CppCodeGenWriteBarrier(&___title_1, value);
	}

	inline static int32_t get_offset_of_filename_2() { return static_cast<int32_t>(offsetof(U3CSendU3Ec__Iterator2_t611604662, ___filename_2)); }
	inline String_t* get_filename_2() const { return ___filename_2; }
	inline String_t** get_address_of_filename_2() { return &___filename_2; }
	inline void set_filename_2(String_t* value)
	{
		___filename_2 = value;
		Il2CppCodeGenWriteBarrier(&___filename_2, value);
	}

	inline static int32_t get_offset_of_addr01_3() { return static_cast<int32_t>(offsetof(U3CSendU3Ec__Iterator2_t611604662, ___addr01_3)); }
	inline String_t* get_addr01_3() const { return ___addr01_3; }
	inline String_t** get_address_of_addr01_3() { return &___addr01_3; }
	inline void set_addr01_3(String_t* value)
	{
		___addr01_3 = value;
		Il2CppCodeGenWriteBarrier(&___addr01_3, value);
	}

	inline static int32_t get_offset_of_addr02_4() { return static_cast<int32_t>(offsetof(U3CSendU3Ec__Iterator2_t611604662, ___addr02_4)); }
	inline String_t* get_addr02_4() const { return ___addr02_4; }
	inline String_t** get_address_of_addr02_4() { return &___addr02_4; }
	inline void set_addr02_4(String_t* value)
	{
		___addr02_4 = value;
		Il2CppCodeGenWriteBarrier(&___addr02_4, value);
	}

	inline static int32_t get_offset_of_U3CwU3E__1_5() { return static_cast<int32_t>(offsetof(U3CSendU3Ec__Iterator2_t611604662, ___U3CwU3E__1_5)); }
	inline WWW_t3134621005 * get_U3CwU3E__1_5() const { return ___U3CwU3E__1_5; }
	inline WWW_t3134621005 ** get_address_of_U3CwU3E__1_5() { return &___U3CwU3E__1_5; }
	inline void set_U3CwU3E__1_5(WWW_t3134621005 * value)
	{
		___U3CwU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwU3E__1_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CSendU3Ec__Iterator2_t611604662, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CSendU3Ec__Iterator2_t611604662, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Etitle_8() { return static_cast<int32_t>(offsetof(U3CSendU3Ec__Iterator2_t611604662, ___U3CU24U3Etitle_8)); }
	inline String_t* get_U3CU24U3Etitle_8() const { return ___U3CU24U3Etitle_8; }
	inline String_t** get_address_of_U3CU24U3Etitle_8() { return &___U3CU24U3Etitle_8; }
	inline void set_U3CU24U3Etitle_8(String_t* value)
	{
		___U3CU24U3Etitle_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Etitle_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Efilename_9() { return static_cast<int32_t>(offsetof(U3CSendU3Ec__Iterator2_t611604662, ___U3CU24U3Efilename_9)); }
	inline String_t* get_U3CU24U3Efilename_9() const { return ___U3CU24U3Efilename_9; }
	inline String_t** get_address_of_U3CU24U3Efilename_9() { return &___U3CU24U3Efilename_9; }
	inline void set_U3CU24U3Efilename_9(String_t* value)
	{
		___U3CU24U3Efilename_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Efilename_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eaddr01_10() { return static_cast<int32_t>(offsetof(U3CSendU3Ec__Iterator2_t611604662, ___U3CU24U3Eaddr01_10)); }
	inline String_t* get_U3CU24U3Eaddr01_10() const { return ___U3CU24U3Eaddr01_10; }
	inline String_t** get_address_of_U3CU24U3Eaddr01_10() { return &___U3CU24U3Eaddr01_10; }
	inline void set_U3CU24U3Eaddr01_10(String_t* value)
	{
		___U3CU24U3Eaddr01_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eaddr01_10, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eaddr02_11() { return static_cast<int32_t>(offsetof(U3CSendU3Ec__Iterator2_t611604662, ___U3CU24U3Eaddr02_11)); }
	inline String_t* get_U3CU24U3Eaddr02_11() const { return ___U3CU24U3Eaddr02_11; }
	inline String_t** get_address_of_U3CU24U3Eaddr02_11() { return &___U3CU24U3Eaddr02_11; }
	inline void set_U3CU24U3Eaddr02_11(String_t* value)
	{
		___U3CU24U3Eaddr02_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eaddr02_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_12() { return static_cast<int32_t>(offsetof(U3CSendU3Ec__Iterator2_t611604662, ___U3CU3Ef__this_12)); }
	inline AlbumPathCheck_t1505991252 * get_U3CU3Ef__this_12() const { return ___U3CU3Ef__this_12; }
	inline AlbumPathCheck_t1505991252 ** get_address_of_U3CU3Ef__this_12() { return &___U3CU3Ef__this_12; }
	inline void set_U3CU3Ef__this_12(AlbumPathCheck_t1505991252 * value)
	{
		___U3CU3Ef__this_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
