﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpotMessageGet/<GreatManCheckProgress>c__Iterator21
struct U3CGreatManCheckProgressU3Ec__Iterator21_t200358716;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SpotMessageGet/<GreatManCheckProgress>c__Iterator21::.ctor()
extern "C"  void U3CGreatManCheckProgressU3Ec__Iterator21__ctor_m1852840879 (U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SpotMessageGet/<GreatManCheckProgress>c__Iterator21::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGreatManCheckProgressU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1988715395 (U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SpotMessageGet/<GreatManCheckProgress>c__Iterator21::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGreatManCheckProgressU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m2306919703 (U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SpotMessageGet/<GreatManCheckProgress>c__Iterator21::MoveNext()
extern "C"  bool U3CGreatManCheckProgressU3Ec__Iterator21_MoveNext_m2769637861 (U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpotMessageGet/<GreatManCheckProgress>c__Iterator21::Dispose()
extern "C"  void U3CGreatManCheckProgressU3Ec__Iterator21_Dispose_m2361907244 (U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpotMessageGet/<GreatManCheckProgress>c__Iterator21::Reset()
extern "C"  void U3CGreatManCheckProgressU3Ec__Iterator21_Reset_m3794241116 (U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
