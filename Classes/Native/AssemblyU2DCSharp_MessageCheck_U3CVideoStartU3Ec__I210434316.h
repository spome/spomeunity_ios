﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// MessageCheck
struct MessageCheck_t3146986849;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageCheck/<VideoStart>c__Iterator15
struct  U3CVideoStartU3Ec__Iterator15_t210434316  : public Il2CppObject
{
public:
	// System.Int32 MessageCheck/<VideoStart>c__Iterator15::num
	int32_t ___num_0;
	// System.Single MessageCheck/<VideoStart>c__Iterator15::<latitude_wikitude>__0
	float ___U3Clatitude_wikitudeU3E__0_1;
	// System.Single MessageCheck/<VideoStart>c__Iterator15::<longtitude_wikitude>__1
	float ___U3Clongtitude_wikitudeU3E__1_2;
	// System.String MessageCheck/<VideoStart>c__Iterator15::filecategory
	String_t* ___filecategory_3;
	// System.String MessageCheck/<VideoStart>c__Iterator15::<_video_localpath>__2
	String_t* ___U3C_video_localpathU3E__2_4;
	// System.Int32 MessageCheck/<VideoStart>c__Iterator15::$PC
	int32_t ___U24PC_5;
	// System.Object MessageCheck/<VideoStart>c__Iterator15::$current
	Il2CppObject * ___U24current_6;
	// System.Int32 MessageCheck/<VideoStart>c__Iterator15::<$>num
	int32_t ___U3CU24U3Enum_7;
	// System.String MessageCheck/<VideoStart>c__Iterator15::<$>filecategory
	String_t* ___U3CU24U3Efilecategory_8;
	// MessageCheck MessageCheck/<VideoStart>c__Iterator15::<>f__this
	MessageCheck_t3146986849 * ___U3CU3Ef__this_9;

public:
	inline static int32_t get_offset_of_num_0() { return static_cast<int32_t>(offsetof(U3CVideoStartU3Ec__Iterator15_t210434316, ___num_0)); }
	inline int32_t get_num_0() const { return ___num_0; }
	inline int32_t* get_address_of_num_0() { return &___num_0; }
	inline void set_num_0(int32_t value)
	{
		___num_0 = value;
	}

	inline static int32_t get_offset_of_U3Clatitude_wikitudeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CVideoStartU3Ec__Iterator15_t210434316, ___U3Clatitude_wikitudeU3E__0_1)); }
	inline float get_U3Clatitude_wikitudeU3E__0_1() const { return ___U3Clatitude_wikitudeU3E__0_1; }
	inline float* get_address_of_U3Clatitude_wikitudeU3E__0_1() { return &___U3Clatitude_wikitudeU3E__0_1; }
	inline void set_U3Clatitude_wikitudeU3E__0_1(float value)
	{
		___U3Clatitude_wikitudeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Clongtitude_wikitudeU3E__1_2() { return static_cast<int32_t>(offsetof(U3CVideoStartU3Ec__Iterator15_t210434316, ___U3Clongtitude_wikitudeU3E__1_2)); }
	inline float get_U3Clongtitude_wikitudeU3E__1_2() const { return ___U3Clongtitude_wikitudeU3E__1_2; }
	inline float* get_address_of_U3Clongtitude_wikitudeU3E__1_2() { return &___U3Clongtitude_wikitudeU3E__1_2; }
	inline void set_U3Clongtitude_wikitudeU3E__1_2(float value)
	{
		___U3Clongtitude_wikitudeU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_filecategory_3() { return static_cast<int32_t>(offsetof(U3CVideoStartU3Ec__Iterator15_t210434316, ___filecategory_3)); }
	inline String_t* get_filecategory_3() const { return ___filecategory_3; }
	inline String_t** get_address_of_filecategory_3() { return &___filecategory_3; }
	inline void set_filecategory_3(String_t* value)
	{
		___filecategory_3 = value;
		Il2CppCodeGenWriteBarrier(&___filecategory_3, value);
	}

	inline static int32_t get_offset_of_U3C_video_localpathU3E__2_4() { return static_cast<int32_t>(offsetof(U3CVideoStartU3Ec__Iterator15_t210434316, ___U3C_video_localpathU3E__2_4)); }
	inline String_t* get_U3C_video_localpathU3E__2_4() const { return ___U3C_video_localpathU3E__2_4; }
	inline String_t** get_address_of_U3C_video_localpathU3E__2_4() { return &___U3C_video_localpathU3E__2_4; }
	inline void set_U3C_video_localpathU3E__2_4(String_t* value)
	{
		___U3C_video_localpathU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_video_localpathU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CVideoStartU3Ec__Iterator15_t210434316, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CVideoStartU3Ec__Iterator15_t210434316, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Enum_7() { return static_cast<int32_t>(offsetof(U3CVideoStartU3Ec__Iterator15_t210434316, ___U3CU24U3Enum_7)); }
	inline int32_t get_U3CU24U3Enum_7() const { return ___U3CU24U3Enum_7; }
	inline int32_t* get_address_of_U3CU24U3Enum_7() { return &___U3CU24U3Enum_7; }
	inline void set_U3CU24U3Enum_7(int32_t value)
	{
		___U3CU24U3Enum_7 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Efilecategory_8() { return static_cast<int32_t>(offsetof(U3CVideoStartU3Ec__Iterator15_t210434316, ___U3CU24U3Efilecategory_8)); }
	inline String_t* get_U3CU24U3Efilecategory_8() const { return ___U3CU24U3Efilecategory_8; }
	inline String_t** get_address_of_U3CU24U3Efilecategory_8() { return &___U3CU24U3Efilecategory_8; }
	inline void set_U3CU24U3Efilecategory_8(String_t* value)
	{
		___U3CU24U3Efilecategory_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Efilecategory_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_9() { return static_cast<int32_t>(offsetof(U3CVideoStartU3Ec__Iterator15_t210434316, ___U3CU3Ef__this_9)); }
	inline MessageCheck_t3146986849 * get_U3CU3Ef__this_9() const { return ___U3CU3Ef__this_9; }
	inline MessageCheck_t3146986849 ** get_address_of_U3CU3Ef__this_9() { return &___U3CU3Ef__this_9; }
	inline void set_U3CU3Ef__this_9(MessageCheck_t3146986849 * value)
	{
		___U3CU3Ef__this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
