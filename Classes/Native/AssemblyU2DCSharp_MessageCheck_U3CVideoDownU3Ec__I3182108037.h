﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t3134621005;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;
// MessageCheck
struct MessageCheck_t3146986849;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageCheck/<VideoDown>c__Iterator14
struct  U3CVideoDownU3Ec__Iterator14_t3182108037  : public Il2CppObject
{
public:
	// UnityEngine.WWW MessageCheck/<VideoDown>c__Iterator14::<www>__0
	WWW_t3134621005 * ___U3CwwwU3E__0_0;
	// System.String MessageCheck/<VideoDown>c__Iterator14::<temp>__1
	String_t* ___U3CtempU3E__1_1;
	// System.Byte[] MessageCheck/<VideoDown>c__Iterator14::<bytes>__2
	ByteU5BU5D_t4260760469* ___U3CbytesU3E__2_2;
	// System.Int32 MessageCheck/<VideoDown>c__Iterator14::$PC
	int32_t ___U24PC_3;
	// System.Object MessageCheck/<VideoDown>c__Iterator14::$current
	Il2CppObject * ___U24current_4;
	// MessageCheck MessageCheck/<VideoDown>c__Iterator14::<>f__this
	MessageCheck_t3146986849 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CVideoDownU3Ec__Iterator14_t3182108037, ___U3CwwwU3E__0_0)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CtempU3E__1_1() { return static_cast<int32_t>(offsetof(U3CVideoDownU3Ec__Iterator14_t3182108037, ___U3CtempU3E__1_1)); }
	inline String_t* get_U3CtempU3E__1_1() const { return ___U3CtempU3E__1_1; }
	inline String_t** get_address_of_U3CtempU3E__1_1() { return &___U3CtempU3E__1_1; }
	inline void set_U3CtempU3E__1_1(String_t* value)
	{
		___U3CtempU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtempU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CbytesU3E__2_2() { return static_cast<int32_t>(offsetof(U3CVideoDownU3Ec__Iterator14_t3182108037, ___U3CbytesU3E__2_2)); }
	inline ByteU5BU5D_t4260760469* get_U3CbytesU3E__2_2() const { return ___U3CbytesU3E__2_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CbytesU3E__2_2() { return &___U3CbytesU3E__2_2; }
	inline void set_U3CbytesU3E__2_2(ByteU5BU5D_t4260760469* value)
	{
		___U3CbytesU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbytesU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CVideoDownU3Ec__Iterator14_t3182108037, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CVideoDownU3Ec__Iterator14_t3182108037, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CVideoDownU3Ec__Iterator14_t3182108037, ___U3CU3Ef__this_5)); }
	inline MessageCheck_t3146986849 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline MessageCheck_t3146986849 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(MessageCheck_t3146986849 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
