﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpotMessageGet/<MessageGetJsonStart>c__Iterator20
struct U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SpotMessageGet/<MessageGetJsonStart>c__Iterator20::.ctor()
extern "C"  void U3CMessageGetJsonStartU3Ec__Iterator20__ctor_m1229495853 (U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SpotMessageGet/<MessageGetJsonStart>c__Iterator20::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMessageGetJsonStartU3Ec__Iterator20_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3327881861 (U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SpotMessageGet/<MessageGetJsonStart>c__Iterator20::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMessageGetJsonStartU3Ec__Iterator20_System_Collections_IEnumerator_get_Current_m2902794265 (U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SpotMessageGet/<MessageGetJsonStart>c__Iterator20::MoveNext()
extern "C"  bool U3CMessageGetJsonStartU3Ec__Iterator20_MoveNext_m4029356455 (U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpotMessageGet/<MessageGetJsonStart>c__Iterator20::Dispose()
extern "C"  void U3CMessageGetJsonStartU3Ec__Iterator20_Dispose_m327791402 (U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpotMessageGet/<MessageGetJsonStart>c__Iterator20::Reset()
extern "C"  void U3CMessageGetJsonStartU3Ec__Iterator20_Reset_m3170896090 (U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
