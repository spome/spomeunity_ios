﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190435706MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TMPro.TMP_Glyph>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4120849468(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3313843725 *, Dictionary_2_t1996520333 *, const MethodInfo*))Enumerator__ctor_m2377115088_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TMPro.TMP_Glyph>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2881155055(__this, method) ((  Il2CppObject * (*) (Enumerator_t3313843725 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1037642267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TMPro.TMP_Glyph>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1428928249(__this, method) ((  void (*) (Enumerator_t3313843725 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2809374949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TMPro.TMP_Glyph>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2524119088(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3313843725 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2434214620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TMPro.TMP_Glyph>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2584493451(__this, method) ((  Il2CppObject * (*) (Enumerator_t3313843725 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3735627447_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TMPro.TMP_Glyph>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2255545693(__this, method) ((  Il2CppObject * (*) (Enumerator_t3313843725 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m393753481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TMPro.TMP_Glyph>::MoveNext()
#define Enumerator_MoveNext_m235025321(__this, method) ((  bool (*) (Enumerator_t3313843725 *, const MethodInfo*))Enumerator_MoveNext_m1213995029_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TMPro.TMP_Glyph>::get_Current()
#define Enumerator_get_Current_m3167659763(__this, method) ((  KeyValuePair_2_t1895301039  (*) (Enumerator_t3313843725 *, const MethodInfo*))Enumerator_get_Current_m1399860359_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TMPro.TMP_Glyph>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m877338610(__this, method) ((  int32_t (*) (Enumerator_t3313843725 *, const MethodInfo*))Enumerator_get_CurrentKey_m1767398110_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TMPro.TMP_Glyph>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1416185394(__this, method) ((  TMP_Glyph_t1999257094 * (*) (Enumerator_t3313843725 *, const MethodInfo*))Enumerator_get_CurrentValue_m3384846750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TMPro.TMP_Glyph>::Reset()
#define Enumerator_Reset_m3308727438(__this, method) ((  void (*) (Enumerator_t3313843725 *, const MethodInfo*))Enumerator_Reset_m1080084514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TMPro.TMP_Glyph>::VerifyState()
#define Enumerator_VerifyState_m3689830935(__this, method) ((  void (*) (Enumerator_t3313843725 *, const MethodInfo*))Enumerator_VerifyState_m2404513451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TMPro.TMP_Glyph>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1029413823(__this, method) ((  void (*) (Enumerator_t3313843725 *, const MethodInfo*))Enumerator_VerifyCurrent_m2789892947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TMPro.TMP_Glyph>::Dispose()
#define Enumerator_Dispose_m3934697950(__this, method) ((  void (*) (Enumerator_t3313843725 *, const MethodInfo*))Enumerator_Dispose_m1102561394_gshared)(__this, method)
