﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GreatmanMessage/<VideoDown>c__IteratorF
struct U3CVideoDownU3Ec__IteratorF_t830887869;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GreatmanMessage/<VideoDown>c__IteratorF::.ctor()
extern "C"  void U3CVideoDownU3Ec__IteratorF__ctor_m2658638926 (U3CVideoDownU3Ec__IteratorF_t830887869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GreatmanMessage/<VideoDown>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CVideoDownU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m135123140 (U3CVideoDownU3Ec__IteratorF_t830887869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GreatmanMessage/<VideoDown>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CVideoDownU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m2380389464 (U3CVideoDownU3Ec__IteratorF_t830887869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GreatmanMessage/<VideoDown>c__IteratorF::MoveNext()
extern "C"  bool U3CVideoDownU3Ec__IteratorF_MoveNext_m4205243110 (U3CVideoDownU3Ec__IteratorF_t830887869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreatmanMessage/<VideoDown>c__IteratorF::Dispose()
extern "C"  void U3CVideoDownU3Ec__IteratorF_Dispose_m3639717131 (U3CVideoDownU3Ec__IteratorF_t830887869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreatmanMessage/<VideoDown>c__IteratorF::Reset()
extern "C"  void U3CVideoDownU3Ec__IteratorF_Reset_m305071867 (U3CVideoDownU3Ec__IteratorF_t830887869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
