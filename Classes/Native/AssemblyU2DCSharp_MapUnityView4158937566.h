﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// MapUnityView
struct MapUnityView_t4158937566;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapUnityView
struct  MapUnityView_t4158937566  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject MapUnityView::panel
	GameObject_t3674682005 * ___panel_2;
	// UnityEngine.GameObject MapUnityView::mapBackButton
	GameObject_t3674682005 * ___mapBackButton_3;
	// UnityEngine.GameObject MapUnityView::messageChekMapBackButton
	GameObject_t3674682005 * ___messageChekMapBackButton_4;

public:
	inline static int32_t get_offset_of_panel_2() { return static_cast<int32_t>(offsetof(MapUnityView_t4158937566, ___panel_2)); }
	inline GameObject_t3674682005 * get_panel_2() const { return ___panel_2; }
	inline GameObject_t3674682005 ** get_address_of_panel_2() { return &___panel_2; }
	inline void set_panel_2(GameObject_t3674682005 * value)
	{
		___panel_2 = value;
		Il2CppCodeGenWriteBarrier(&___panel_2, value);
	}

	inline static int32_t get_offset_of_mapBackButton_3() { return static_cast<int32_t>(offsetof(MapUnityView_t4158937566, ___mapBackButton_3)); }
	inline GameObject_t3674682005 * get_mapBackButton_3() const { return ___mapBackButton_3; }
	inline GameObject_t3674682005 ** get_address_of_mapBackButton_3() { return &___mapBackButton_3; }
	inline void set_mapBackButton_3(GameObject_t3674682005 * value)
	{
		___mapBackButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___mapBackButton_3, value);
	}

	inline static int32_t get_offset_of_messageChekMapBackButton_4() { return static_cast<int32_t>(offsetof(MapUnityView_t4158937566, ___messageChekMapBackButton_4)); }
	inline GameObject_t3674682005 * get_messageChekMapBackButton_4() const { return ___messageChekMapBackButton_4; }
	inline GameObject_t3674682005 ** get_address_of_messageChekMapBackButton_4() { return &___messageChekMapBackButton_4; }
	inline void set_messageChekMapBackButton_4(GameObject_t3674682005 * value)
	{
		___messageChekMapBackButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___messageChekMapBackButton_4, value);
	}
};

struct MapUnityView_t4158937566_StaticFields
{
public:
	// MapUnityView MapUnityView::_instance
	MapUnityView_t4158937566 * ____instance_5;

public:
	inline static int32_t get_offset_of__instance_5() { return static_cast<int32_t>(offsetof(MapUnityView_t4158937566_StaticFields, ____instance_5)); }
	inline MapUnityView_t4158937566 * get__instance_5() const { return ____instance_5; }
	inline MapUnityView_t4158937566 ** get_address_of__instance_5() { return &____instance_5; }
	inline void set__instance_5(MapUnityView_t4158937566 * value)
	{
		____instance_5 = value;
		Il2CppCodeGenWriteBarrier(&____instance_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
