﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebViewObject
struct WebViewObject_t388577433;
// System.String
struct String_t;
// System.Action`1<System.String>
struct Action_1_t403047693;
// IWebViewCallback
struct IWebViewCallback_t883754997;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void WebViewObject::.ctor()
extern "C"  void WebViewObject__ctor_m3926610899 (WebViewObject_t388577433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebViewObject::get_IsKeyboardVisible()
extern "C"  bool WebViewObject_get_IsKeyboardVisible_m3587886589 (WebViewObject_t388577433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr WebViewObject::_CWebViewPlugin_Init(System.String,System.Boolean)
extern "C"  IntPtr_t WebViewObject__CWebViewPlugin_Init_m3510492254 (Il2CppObject * __this /* static, unused */, String_t* ___gameObject0, bool ___transparent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebViewObject::_CWebViewPlugin_Destroy(System.IntPtr)
extern "C"  int32_t WebViewObject__CWebViewPlugin_Destroy_m2353415506 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_SetMargins(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void WebViewObject__CWebViewPlugin_SetMargins_m1989304009 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, int32_t ___left1, int32_t ___top2, int32_t ___right3, int32_t ___bottom4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_SetVisibility(System.IntPtr,System.Boolean)
extern "C"  void WebViewObject__CWebViewPlugin_SetVisibility_m4275967415 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, bool ___visibility1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_LoadURL(System.IntPtr,System.String)
extern "C"  void WebViewObject__CWebViewPlugin_LoadURL_m1779362445 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, String_t* ___url1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_EvaluateJS(System.IntPtr,System.String)
extern "C"  void WebViewObject__CWebViewPlugin_EvaluateJS_m3707640198 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, String_t* ___url1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_SetFrame(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void WebViewObject__CWebViewPlugin_SetFrame_m2439032737 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, int32_t ___x1, int32_t ___y2, int32_t ___width3, int32_t ___height4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::Init(System.Action`1<System.String>,System.Boolean)
extern "C"  void WebViewObject_Init_m700700128 (WebViewObject_t388577433 * __this, Action_1_t403047693 * ___cb0, bool ___transparent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::OnDestroy()
extern "C"  void WebViewObject_OnDestroy_m429459276 (WebViewObject_t388577433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::SetCenterPositionWithScale(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void WebViewObject_SetCenterPositionWithScale_m3605192217 (WebViewObject_t388577433 * __this, Vector2_t4282066565  ___center0, Vector2_t4282066565  ___scale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void WebViewObject_SetMargins_m3805796466 (WebViewObject_t388577433 * __this, int32_t ___left0, int32_t ___top1, int32_t ___right2, int32_t ___bottom3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::SetVisibility(System.Boolean)
extern "C"  void WebViewObject_SetVisibility_m927614396 (WebViewObject_t388577433 * __this, bool ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebViewObject::GetVisibility()
extern "C"  bool WebViewObject_GetVisibility_m1451916717 (WebViewObject_t388577433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::LoadURL(System.String)
extern "C"  void WebViewObject_LoadURL_m3283687400 (WebViewObject_t388577433 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::EvaluateJS(System.String)
extern "C"  void WebViewObject_EvaluateJS_m3850448815 (WebViewObject_t388577433 * __this, String_t* ___js0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::CallFromJS(System.String)
extern "C"  void WebViewObject_CallFromJS_m2466694400 (WebViewObject_t388577433 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::setCallback(IWebViewCallback)
extern "C"  void WebViewObject_setCallback_m1762604739 (WebViewObject_t388577433 * __this, Il2CppObject * ____callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::onLoadStart(System.String)
extern "C"  void WebViewObject_onLoadStart_m367642612 (WebViewObject_t388577433 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::onLoadFinish(System.String)
extern "C"  void WebViewObject_onLoadFinish_m1596154969 (WebViewObject_t388577433 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::onLoadFail(System.String)
extern "C"  void WebViewObject_onLoadFail_m2998845742 (WebViewObject_t388577433 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
