﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EmailChange/<RegisterDelete>c__IteratorA
struct U3CRegisterDeleteU3Ec__IteratorA_t1809913895;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EmailChange/<RegisterDelete>c__IteratorA::.ctor()
extern "C"  void U3CRegisterDeleteU3Ec__IteratorA__ctor_m4184638036 (U3CRegisterDeleteU3Ec__IteratorA_t1809913895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EmailChange/<RegisterDelete>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRegisterDeleteU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m677748488 (U3CRegisterDeleteU3Ec__IteratorA_t1809913895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EmailChange/<RegisterDelete>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRegisterDeleteU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m159822492 (U3CRegisterDeleteU3Ec__IteratorA_t1809913895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EmailChange/<RegisterDelete>c__IteratorA::MoveNext()
extern "C"  bool U3CRegisterDeleteU3Ec__IteratorA_MoveNext_m1790893832 (U3CRegisterDeleteU3Ec__IteratorA_t1809913895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmailChange/<RegisterDelete>c__IteratorA::Dispose()
extern "C"  void U3CRegisterDeleteU3Ec__IteratorA_Dispose_m1246046609 (U3CRegisterDeleteU3Ec__IteratorA_t1809913895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmailChange/<RegisterDelete>c__IteratorA::Reset()
extern "C"  void U3CRegisterDeleteU3Ec__IteratorA_Reset_m1831070977 (U3CRegisterDeleteU3Ec__IteratorA_t1809913895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
