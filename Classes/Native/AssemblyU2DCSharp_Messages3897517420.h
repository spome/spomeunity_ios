﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Messages
struct  Messages_t3897517420 
{
public:
	// System.String Messages::Title
	String_t* ___Title_0;
	// System.String Messages::type
	String_t* ___type_1;
	// System.String Messages::address
	String_t* ___address_2;
	// System.String Messages::lat
	String_t* ___lat_3;
	// System.String Messages::lon
	String_t* ___lon_4;
	// System.String Messages::videourl
	String_t* ___videourl_5;
	// System.String Messages::videoname
	String_t* ___videoname_6;
	// System.String Messages::createdata
	String_t* ___createdata_7;
	// System.String Messages::messageid
	String_t* ___messageid_8;
	// System.Int32 Messages::MessageCategory
	int32_t ___MessageCategory_9;

public:
	inline static int32_t get_offset_of_Title_0() { return static_cast<int32_t>(offsetof(Messages_t3897517420, ___Title_0)); }
	inline String_t* get_Title_0() const { return ___Title_0; }
	inline String_t** get_address_of_Title_0() { return &___Title_0; }
	inline void set_Title_0(String_t* value)
	{
		___Title_0 = value;
		Il2CppCodeGenWriteBarrier(&___Title_0, value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(Messages_t3897517420, ___type_1)); }
	inline String_t* get_type_1() const { return ___type_1; }
	inline String_t** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(String_t* value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier(&___type_1, value);
	}

	inline static int32_t get_offset_of_address_2() { return static_cast<int32_t>(offsetof(Messages_t3897517420, ___address_2)); }
	inline String_t* get_address_2() const { return ___address_2; }
	inline String_t** get_address_of_address_2() { return &___address_2; }
	inline void set_address_2(String_t* value)
	{
		___address_2 = value;
		Il2CppCodeGenWriteBarrier(&___address_2, value);
	}

	inline static int32_t get_offset_of_lat_3() { return static_cast<int32_t>(offsetof(Messages_t3897517420, ___lat_3)); }
	inline String_t* get_lat_3() const { return ___lat_3; }
	inline String_t** get_address_of_lat_3() { return &___lat_3; }
	inline void set_lat_3(String_t* value)
	{
		___lat_3 = value;
		Il2CppCodeGenWriteBarrier(&___lat_3, value);
	}

	inline static int32_t get_offset_of_lon_4() { return static_cast<int32_t>(offsetof(Messages_t3897517420, ___lon_4)); }
	inline String_t* get_lon_4() const { return ___lon_4; }
	inline String_t** get_address_of_lon_4() { return &___lon_4; }
	inline void set_lon_4(String_t* value)
	{
		___lon_4 = value;
		Il2CppCodeGenWriteBarrier(&___lon_4, value);
	}

	inline static int32_t get_offset_of_videourl_5() { return static_cast<int32_t>(offsetof(Messages_t3897517420, ___videourl_5)); }
	inline String_t* get_videourl_5() const { return ___videourl_5; }
	inline String_t** get_address_of_videourl_5() { return &___videourl_5; }
	inline void set_videourl_5(String_t* value)
	{
		___videourl_5 = value;
		Il2CppCodeGenWriteBarrier(&___videourl_5, value);
	}

	inline static int32_t get_offset_of_videoname_6() { return static_cast<int32_t>(offsetof(Messages_t3897517420, ___videoname_6)); }
	inline String_t* get_videoname_6() const { return ___videoname_6; }
	inline String_t** get_address_of_videoname_6() { return &___videoname_6; }
	inline void set_videoname_6(String_t* value)
	{
		___videoname_6 = value;
		Il2CppCodeGenWriteBarrier(&___videoname_6, value);
	}

	inline static int32_t get_offset_of_createdata_7() { return static_cast<int32_t>(offsetof(Messages_t3897517420, ___createdata_7)); }
	inline String_t* get_createdata_7() const { return ___createdata_7; }
	inline String_t** get_address_of_createdata_7() { return &___createdata_7; }
	inline void set_createdata_7(String_t* value)
	{
		___createdata_7 = value;
		Il2CppCodeGenWriteBarrier(&___createdata_7, value);
	}

	inline static int32_t get_offset_of_messageid_8() { return static_cast<int32_t>(offsetof(Messages_t3897517420, ___messageid_8)); }
	inline String_t* get_messageid_8() const { return ___messageid_8; }
	inline String_t** get_address_of_messageid_8() { return &___messageid_8; }
	inline void set_messageid_8(String_t* value)
	{
		___messageid_8 = value;
		Il2CppCodeGenWriteBarrier(&___messageid_8, value);
	}

	inline static int32_t get_offset_of_MessageCategory_9() { return static_cast<int32_t>(offsetof(Messages_t3897517420, ___MessageCategory_9)); }
	inline int32_t get_MessageCategory_9() const { return ___MessageCategory_9; }
	inline int32_t* get_address_of_MessageCategory_9() { return &___MessageCategory_9; }
	inline void set_MessageCategory_9(int32_t value)
	{
		___MessageCategory_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Messages
struct Messages_t3897517420_marshaled_pinvoke
{
	char* ___Title_0;
	char* ___type_1;
	char* ___address_2;
	char* ___lat_3;
	char* ___lon_4;
	char* ___videourl_5;
	char* ___videoname_6;
	char* ___createdata_7;
	char* ___messageid_8;
	int32_t ___MessageCategory_9;
};
// Native definition for marshalling of: Messages
struct Messages_t3897517420_marshaled_com
{
	Il2CppChar* ___Title_0;
	Il2CppChar* ___type_1;
	Il2CppChar* ___address_2;
	Il2CppChar* ___lat_3;
	Il2CppChar* ___lon_4;
	Il2CppChar* ___videourl_5;
	Il2CppChar* ___videoname_6;
	Il2CppChar* ___createdata_7;
	Il2CppChar* ___messageid_8;
	int32_t ___MessageCategory_9;
};
