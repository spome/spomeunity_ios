﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EmailChange
struct EmailChange_t3079654988;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.WWW
struct WWW_t3134621005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"

// System.Void EmailChange::.ctor()
extern "C"  void EmailChange__ctor_m1263600287 (EmailChange_t3079654988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmailChange::EmailButtonClick(System.String[])
extern "C"  void EmailChange_EmailButtonClick_m1046917441 (EmailChange_t3079654988 * __this, StringU5BU5D_t4054002952* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmailChange::register()
extern "C"  void EmailChange_register_m913772168 (EmailChange_t3079654988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EmailChange::RegisterSend()
extern "C"  Il2CppObject * EmailChange_RegisterSend_m3888352648 (EmailChange_t3079654988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EmailChange::CheckProgress(UnityEngine.WWW)
extern "C"  Il2CppObject * EmailChange_CheckProgress_m3793964824 (EmailChange_t3079654988 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmailChange::EmailDelete()
extern "C"  void EmailChange_EmailDelete_m1830258020 (EmailChange_t3079654988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EmailChange::RegisterDelete()
extern "C"  Il2CppObject * EmailChange_RegisterDelete_m3953951531 (EmailChange_t3079654988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
