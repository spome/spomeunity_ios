﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ErrorMessage/NegativeButtonListner
struct NegativeButtonListner_t2426404166;
// ErrorMessage
struct ErrorMessage_t1367556351;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ErrorMessage1367556351.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject2362096582.h"

// System.Void ErrorMessage/NegativeButtonListner::.ctor(ErrorMessage)
extern "C"  void NegativeButtonListner__ctor_m3832135414 (NegativeButtonListner_t2426404166 * __this, ErrorMessage_t1367556351 * ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessage/NegativeButtonListner::onClick(UnityEngine.AndroidJavaObject,System.Int32)
extern "C"  void NegativeButtonListner_onClick_m1966294682 (NegativeButtonListner_t2426404166 * __this, AndroidJavaObject_t2362096582 * ___obj0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
