﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Main
struct Main_t2390489;

#include "codegen/il2cpp-codegen.h"

// System.Void Main::.ctor()
extern "C"  void Main__ctor_m2928940258 (Main_t2390489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main::Awake()
extern "C"  void Main_Awake_m3166545477 (Main_t2390489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main::Start()
extern "C"  void Main_Start_m1876078050 (Main_t2390489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
