﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CurrentScreen/<MessageCountGetJsonStart>c__Iterator7
struct U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CurrentScreen/<MessageCountGetJsonStart>c__Iterator7::.ctor()
extern "C"  void U3CMessageCountGetJsonStartU3Ec__Iterator7__ctor_m3694852373 (U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CurrentScreen/<MessageCountGetJsonStart>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMessageCountGetJsonStartU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2167314279 (U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CurrentScreen/<MessageCountGetJsonStart>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMessageCountGetJsonStartU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m2704427259 (U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CurrentScreen/<MessageCountGetJsonStart>c__Iterator7::MoveNext()
extern "C"  bool U3CMessageCountGetJsonStartU3Ec__Iterator7_MoveNext_m2945644199 (U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen/<MessageCountGetJsonStart>c__Iterator7::Dispose()
extern "C"  void U3CMessageCountGetJsonStartU3Ec__Iterator7_Dispose_m3008427026 (U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen/<MessageCountGetJsonStart>c__Iterator7::Reset()
extern "C"  void U3CMessageCountGetJsonStartU3Ec__Iterator7_Reset_m1341285314 (U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
