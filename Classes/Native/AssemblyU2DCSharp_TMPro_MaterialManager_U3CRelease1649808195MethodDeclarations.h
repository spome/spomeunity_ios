﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.MaterialManager/<ReleaseBaseMaterial>c__AnonStorey2F
struct U3CReleaseBaseMaterialU3Ec__AnonStorey2F_t1649808195;
// TMPro.MaterialManager/MaskingMaterial
struct MaskingMaterial_t2640101498;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_MaterialManager_MaskingMat2640101498.h"

// System.Void TMPro.MaterialManager/<ReleaseBaseMaterial>c__AnonStorey2F::.ctor()
extern "C"  void U3CReleaseBaseMaterialU3Ec__AnonStorey2F__ctor_m3311005880 (U3CReleaseBaseMaterialU3Ec__AnonStorey2F_t1649808195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.MaterialManager/<ReleaseBaseMaterial>c__AnonStorey2F::<>m__D(TMPro.MaterialManager/MaskingMaterial)
extern "C"  bool U3CReleaseBaseMaterialU3Ec__AnonStorey2F_U3CU3Em__D_m3840802975 (U3CReleaseBaseMaterialU3Ec__AnonStorey2F_t1649808195 * __this, MaskingMaterial_t2640101498 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
