﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.XML_TagAttribute
struct  XML_TagAttribute_t35801856 
{
public:
	// System.Int32 TMPro.XML_TagAttribute::nameHashCode
	int32_t ___nameHashCode_0;
	// System.Int32 TMPro.XML_TagAttribute::valueStartIndex
	int32_t ___valueStartIndex_1;
	// System.Int32 TMPro.XML_TagAttribute::valueDecimalIndex
	int32_t ___valueDecimalIndex_2;
	// System.Int32 TMPro.XML_TagAttribute::valueLength
	int32_t ___valueLength_3;
	// System.Int32 TMPro.XML_TagAttribute::valueHashCode
	int32_t ___valueHashCode_4;

public:
	inline static int32_t get_offset_of_nameHashCode_0() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t35801856, ___nameHashCode_0)); }
	inline int32_t get_nameHashCode_0() const { return ___nameHashCode_0; }
	inline int32_t* get_address_of_nameHashCode_0() { return &___nameHashCode_0; }
	inline void set_nameHashCode_0(int32_t value)
	{
		___nameHashCode_0 = value;
	}

	inline static int32_t get_offset_of_valueStartIndex_1() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t35801856, ___valueStartIndex_1)); }
	inline int32_t get_valueStartIndex_1() const { return ___valueStartIndex_1; }
	inline int32_t* get_address_of_valueStartIndex_1() { return &___valueStartIndex_1; }
	inline void set_valueStartIndex_1(int32_t value)
	{
		___valueStartIndex_1 = value;
	}

	inline static int32_t get_offset_of_valueDecimalIndex_2() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t35801856, ___valueDecimalIndex_2)); }
	inline int32_t get_valueDecimalIndex_2() const { return ___valueDecimalIndex_2; }
	inline int32_t* get_address_of_valueDecimalIndex_2() { return &___valueDecimalIndex_2; }
	inline void set_valueDecimalIndex_2(int32_t value)
	{
		___valueDecimalIndex_2 = value;
	}

	inline static int32_t get_offset_of_valueLength_3() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t35801856, ___valueLength_3)); }
	inline int32_t get_valueLength_3() const { return ___valueLength_3; }
	inline int32_t* get_address_of_valueLength_3() { return &___valueLength_3; }
	inline void set_valueLength_3(int32_t value)
	{
		___valueLength_3 = value;
	}

	inline static int32_t get_offset_of_valueHashCode_4() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t35801856, ___valueHashCode_4)); }
	inline int32_t get_valueHashCode_4() const { return ___valueHashCode_4; }
	inline int32_t* get_address_of_valueHashCode_4() { return &___valueHashCode_4; }
	inline void set_valueHashCode_4(int32_t value)
	{
		___valueHashCode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: TMPro.XML_TagAttribute
struct XML_TagAttribute_t35801856_marshaled_pinvoke
{
	int32_t ___nameHashCode_0;
	int32_t ___valueStartIndex_1;
	int32_t ___valueDecimalIndex_2;
	int32_t ___valueLength_3;
	int32_t ___valueHashCode_4;
};
// Native definition for marshalling of: TMPro.XML_TagAttribute
struct XML_TagAttribute_t35801856_marshaled_com
{
	int32_t ___nameHashCode_0;
	int32_t ___valueStartIndex_1;
	int32_t ___valueDecimalIndex_2;
	int32_t ___valueLength_3;
	int32_t ___valueHashCode_4;
};
