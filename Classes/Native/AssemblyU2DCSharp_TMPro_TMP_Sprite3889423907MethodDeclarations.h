﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.TMP_Sprite
struct TMP_Sprite_t3889423907;

#include "codegen/il2cpp-codegen.h"

// System.Void TMPro.TMP_Sprite::.ctor()
extern "C"  void TMP_Sprite__ctor_m4044761748 (TMP_Sprite_t3889423907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
