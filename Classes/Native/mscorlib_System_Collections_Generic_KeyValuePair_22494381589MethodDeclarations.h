﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22990515955MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<CurrentState/States,UnityEngine.GameObject>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2756455028(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2494381589 *, int32_t, GameObject_t3674682005 *, const MethodInfo*))KeyValuePair_2__ctor_m1942999012_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<CurrentState/States,UnityEngine.GameObject>::get_Key()
#define KeyValuePair_2_get_Key_m390700788(__this, method) ((  int32_t (*) (KeyValuePair_2_t2494381589 *, const MethodInfo*))KeyValuePair_2_get_Key_m465482500_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<CurrentState/States,UnityEngine.GameObject>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2676455221(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2494381589 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1775119301_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<CurrentState/States,UnityEngine.GameObject>::get_Value()
#define KeyValuePair_2_get_Value_m3228647476(__this, method) ((  GameObject_t3674682005 * (*) (KeyValuePair_2_t2494381589 *, const MethodInfo*))KeyValuePair_2_get_Value_m1848063272_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<CurrentState/States,UnityEngine.GameObject>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m138029237(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2494381589 *, GameObject_t3674682005 *, const MethodInfo*))KeyValuePair_2_set_Value_m804525381_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<CurrentState/States,UnityEngine.GameObject>::ToString()
#define KeyValuePair_2_ToString_m53934925(__this, method) ((  String_t* (*) (KeyValuePair_2_t2494381589 *, const MethodInfo*))KeyValuePair_2_ToString_m2796541731_gshared)(__this, method)
