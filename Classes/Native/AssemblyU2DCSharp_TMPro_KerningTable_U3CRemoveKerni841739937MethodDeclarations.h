﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey32
struct U3CRemoveKerningPairU3Ec__AnonStorey32_t841739937;
// TMPro.KerningPair
struct KerningPair_t1904833704;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_KerningPair1904833704.h"

// System.Void TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey32::.ctor()
extern "C"  void U3CRemoveKerningPairU3Ec__AnonStorey32__ctor_m3099152362 (U3CRemoveKerningPairU3Ec__AnonStorey32_t841739937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey32::<>m__11(TMPro.KerningPair)
extern "C"  bool U3CRemoveKerningPairU3Ec__AnonStorey32_U3CU3Em__11_m3144725023 (U3CRemoveKerningPairU3Ec__AnonStorey32_t841739937 * __this, KerningPair_t1904833704 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
