﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.MaterialManager/<RemoveStencilMaterial>c__AnonStorey2E
struct U3CRemoveStencilMaterialU3Ec__AnonStorey2E_t2712845986;
// TMPro.MaterialManager/MaskingMaterial
struct MaskingMaterial_t2640101498;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_MaterialManager_MaskingMat2640101498.h"

// System.Void TMPro.MaterialManager/<RemoveStencilMaterial>c__AnonStorey2E::.ctor()
extern "C"  void U3CRemoveStencilMaterialU3Ec__AnonStorey2E__ctor_m2809068089 (U3CRemoveStencilMaterialU3Ec__AnonStorey2E_t2712845986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.MaterialManager/<RemoveStencilMaterial>c__AnonStorey2E::<>m__C(TMPro.MaterialManager/MaskingMaterial)
extern "C"  bool U3CRemoveStencilMaterialU3Ec__AnonStorey2E_U3CU3Em__C_m2010066305 (U3CRemoveStencilMaterialU3Ec__AnonStorey2E_t2712845986 * __this, MaskingMaterial_t2640101498 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
