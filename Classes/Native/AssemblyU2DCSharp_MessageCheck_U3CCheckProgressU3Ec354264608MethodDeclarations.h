﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageCheck/<CheckProgress>c__Iterator17
struct U3CCheckProgressU3Ec__Iterator17_t354264608;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageCheck/<CheckProgress>c__Iterator17::.ctor()
extern "C"  void U3CCheckProgressU3Ec__Iterator17__ctor_m1199092555 (U3CCheckProgressU3Ec__Iterator17_t354264608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageCheck/<CheckProgress>c__Iterator17::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckProgressU3Ec__Iterator17_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4097453863 (U3CCheckProgressU3Ec__Iterator17_t354264608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageCheck/<CheckProgress>c__Iterator17::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckProgressU3Ec__Iterator17_System_Collections_IEnumerator_get_Current_m400908475 (U3CCheckProgressU3Ec__Iterator17_t354264608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MessageCheck/<CheckProgress>c__Iterator17::MoveNext()
extern "C"  bool U3CCheckProgressU3Ec__Iterator17_MoveNext_m267719625 (U3CCheckProgressU3Ec__Iterator17_t354264608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck/<CheckProgress>c__Iterator17::Dispose()
extern "C"  void U3CCheckProgressU3Ec__Iterator17_Dispose_m1174993096 (U3CCheckProgressU3Ec__Iterator17_t354264608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck/<CheckProgress>c__Iterator17::Reset()
extern "C"  void U3CCheckProgressU3Ec__Iterator17_Reset_m3140492792 (U3CCheckProgressU3Ec__Iterator17_t354264608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
