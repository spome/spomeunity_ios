﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TMPro.TMP_Settings
struct TMP_Settings_t2708659969;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t967550395;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t3955808645;
// TMPro.TMP_StyleSheet
struct TMP_StyleSheet_t3103963276;

#include "UnityEngine_UnityEngine_ScriptableObject2970544072.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Settings
struct  TMP_Settings_t2708659969  : public ScriptableObject_t2970544072
{
public:
	// System.Boolean TMPro.TMP_Settings::enableWordWrapping
	bool ___enableWordWrapping_3;
	// System.Boolean TMPro.TMP_Settings::enableKerning
	bool ___enableKerning_4;
	// System.Boolean TMPro.TMP_Settings::enableExtraPadding
	bool ___enableExtraPadding_5;
	// System.Boolean TMPro.TMP_Settings::warningsDisabled
	bool ___warningsDisabled_6;
	// TMPro.TMP_FontAsset TMPro.TMP_Settings::fontAsset
	TMP_FontAsset_t967550395 * ___fontAsset_7;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Settings::spriteAsset
	TMP_SpriteAsset_t3955808645 * ___spriteAsset_8;
	// TMPro.TMP_StyleSheet TMPro.TMP_Settings::styleSheet
	TMP_StyleSheet_t3103963276 * ___styleSheet_9;

public:
	inline static int32_t get_offset_of_enableWordWrapping_3() { return static_cast<int32_t>(offsetof(TMP_Settings_t2708659969, ___enableWordWrapping_3)); }
	inline bool get_enableWordWrapping_3() const { return ___enableWordWrapping_3; }
	inline bool* get_address_of_enableWordWrapping_3() { return &___enableWordWrapping_3; }
	inline void set_enableWordWrapping_3(bool value)
	{
		___enableWordWrapping_3 = value;
	}

	inline static int32_t get_offset_of_enableKerning_4() { return static_cast<int32_t>(offsetof(TMP_Settings_t2708659969, ___enableKerning_4)); }
	inline bool get_enableKerning_4() const { return ___enableKerning_4; }
	inline bool* get_address_of_enableKerning_4() { return &___enableKerning_4; }
	inline void set_enableKerning_4(bool value)
	{
		___enableKerning_4 = value;
	}

	inline static int32_t get_offset_of_enableExtraPadding_5() { return static_cast<int32_t>(offsetof(TMP_Settings_t2708659969, ___enableExtraPadding_5)); }
	inline bool get_enableExtraPadding_5() const { return ___enableExtraPadding_5; }
	inline bool* get_address_of_enableExtraPadding_5() { return &___enableExtraPadding_5; }
	inline void set_enableExtraPadding_5(bool value)
	{
		___enableExtraPadding_5 = value;
	}

	inline static int32_t get_offset_of_warningsDisabled_6() { return static_cast<int32_t>(offsetof(TMP_Settings_t2708659969, ___warningsDisabled_6)); }
	inline bool get_warningsDisabled_6() const { return ___warningsDisabled_6; }
	inline bool* get_address_of_warningsDisabled_6() { return &___warningsDisabled_6; }
	inline void set_warningsDisabled_6(bool value)
	{
		___warningsDisabled_6 = value;
	}

	inline static int32_t get_offset_of_fontAsset_7() { return static_cast<int32_t>(offsetof(TMP_Settings_t2708659969, ___fontAsset_7)); }
	inline TMP_FontAsset_t967550395 * get_fontAsset_7() const { return ___fontAsset_7; }
	inline TMP_FontAsset_t967550395 ** get_address_of_fontAsset_7() { return &___fontAsset_7; }
	inline void set_fontAsset_7(TMP_FontAsset_t967550395 * value)
	{
		___fontAsset_7 = value;
		Il2CppCodeGenWriteBarrier(&___fontAsset_7, value);
	}

	inline static int32_t get_offset_of_spriteAsset_8() { return static_cast<int32_t>(offsetof(TMP_Settings_t2708659969, ___spriteAsset_8)); }
	inline TMP_SpriteAsset_t3955808645 * get_spriteAsset_8() const { return ___spriteAsset_8; }
	inline TMP_SpriteAsset_t3955808645 ** get_address_of_spriteAsset_8() { return &___spriteAsset_8; }
	inline void set_spriteAsset_8(TMP_SpriteAsset_t3955808645 * value)
	{
		___spriteAsset_8 = value;
		Il2CppCodeGenWriteBarrier(&___spriteAsset_8, value);
	}

	inline static int32_t get_offset_of_styleSheet_9() { return static_cast<int32_t>(offsetof(TMP_Settings_t2708659969, ___styleSheet_9)); }
	inline TMP_StyleSheet_t3103963276 * get_styleSheet_9() const { return ___styleSheet_9; }
	inline TMP_StyleSheet_t3103963276 ** get_address_of_styleSheet_9() { return &___styleSheet_9; }
	inline void set_styleSheet_9(TMP_StyleSheet_t3103963276 * value)
	{
		___styleSheet_9 = value;
		Il2CppCodeGenWriteBarrier(&___styleSheet_9, value);
	}
};

struct TMP_Settings_t2708659969_StaticFields
{
public:
	// TMPro.TMP_Settings TMPro.TMP_Settings::s_Instance
	TMP_Settings_t2708659969 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(TMP_Settings_t2708659969_StaticFields, ___s_Instance_2)); }
	inline TMP_Settings_t2708659969 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline TMP_Settings_t2708659969 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(TMP_Settings_t2708659969 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___s_Instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
