﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidBackBtn
struct AndroidBackBtn_t3275646630;

#include "codegen/il2cpp-codegen.h"

// System.Void AndroidBackBtn::.ctor()
extern "C"  void AndroidBackBtn__ctor_m323584181 (AndroidBackBtn_t3275646630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidBackBtn::Update()
extern "C"  void AndroidBackBtn_Update_m3168037112 (AndroidBackBtn_t3275646630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
