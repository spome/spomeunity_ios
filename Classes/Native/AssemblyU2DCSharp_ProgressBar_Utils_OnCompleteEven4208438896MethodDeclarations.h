﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProgressBar.Utils.OnCompleteEvent
struct OnCompleteEvent_t4208438896;

#include "codegen/il2cpp-codegen.h"

// System.Void ProgressBar.Utils.OnCompleteEvent::.ctor()
extern "C"  void OnCompleteEvent__ctor_m3920728494 (OnCompleteEvent_t4208438896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
