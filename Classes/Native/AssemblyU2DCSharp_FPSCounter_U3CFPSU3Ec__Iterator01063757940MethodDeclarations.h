﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FPSCounter/<FPS>c__Iterator0
struct U3CFPSU3Ec__Iterator0_t1063757940;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FPSCounter/<FPS>c__Iterator0::.ctor()
extern "C"  void U3CFPSU3Ec__Iterator0__ctor_m463297703 (U3CFPSU3Ec__Iterator0_t1063757940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FPSCounter/<FPS>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFPSU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m713234965 (U3CFPSU3Ec__Iterator0_t1063757940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FPSCounter/<FPS>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFPSU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3054364585 (U3CFPSU3Ec__Iterator0_t1063757940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FPSCounter/<FPS>c__Iterator0::MoveNext()
extern "C"  bool U3CFPSU3Ec__Iterator0_MoveNext_m2845568469 (U3CFPSU3Ec__Iterator0_t1063757940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounter/<FPS>c__Iterator0::Dispose()
extern "C"  void U3CFPSU3Ec__Iterator0_Dispose_m2745744164 (U3CFPSU3Ec__Iterator0_t1063757940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounter/<FPS>c__Iterator0::Reset()
extern "C"  void U3CFPSU3Ec__Iterator0_Reset_m2404697940 (U3CFPSU3Ec__Iterator0_t1063757940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
