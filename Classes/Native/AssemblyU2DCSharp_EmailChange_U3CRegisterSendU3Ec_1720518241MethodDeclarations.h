﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EmailChange/<RegisterSend>c__Iterator8
struct U3CRegisterSendU3Ec__Iterator8_t1720518241;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EmailChange/<RegisterSend>c__Iterator8::.ctor()
extern "C"  void U3CRegisterSendU3Ec__Iterator8__ctor_m4271807322 (U3CRegisterSendU3Ec__Iterator8_t1720518241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EmailChange/<RegisterSend>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRegisterSendU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2177685250 (U3CRegisterSendU3Ec__Iterator8_t1720518241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EmailChange/<RegisterSend>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRegisterSendU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1363222166 (U3CRegisterSendU3Ec__Iterator8_t1720518241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EmailChange/<RegisterSend>c__Iterator8::MoveNext()
extern "C"  bool U3CRegisterSendU3Ec__Iterator8_MoveNext_m2568781122 (U3CRegisterSendU3Ec__Iterator8_t1720518241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmailChange/<RegisterSend>c__Iterator8::Dispose()
extern "C"  void U3CRegisterSendU3Ec__Iterator8_Dispose_m3411351831 (U3CRegisterSendU3Ec__Iterator8_t1720518241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmailChange/<RegisterSend>c__Iterator8::Reset()
extern "C"  void U3CRegisterSendU3Ec__Iterator8_Reset_m1918240263 (U3CRegisterSendU3Ec__Iterator8_t1720518241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
