﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MapUnityView
struct MapUnityView_t4158937566;

#include "codegen/il2cpp-codegen.h"

// System.Void MapUnityView::.ctor()
extern "C"  void MapUnityView__ctor_m2422723197 (MapUnityView_t4158937566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapUnityView::.cctor()
extern "C"  void MapUnityView__cctor_m1607878864 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapUnityView::Awake()
extern "C"  void MapUnityView_Awake_m2660328416 (MapUnityView_t4158937566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapUnityView::Start()
extern "C"  void MapUnityView_Start_m1369860989 (MapUnityView_t4158937566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapUnityView::MapOpen()
extern "C"  void MapUnityView_MapOpen_m806408545 (MapUnityView_t4158937566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapUnityView::MapClose()
extern "C"  void MapUnityView_MapClose_m1358645027 (MapUnityView_t4158937566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapUnityView::MeChekOpen()
extern "C"  void MapUnityView_MeChekOpen_m3898353108 (MapUnityView_t4158937566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapUnityView::MeChekClose()
extern "C"  void MapUnityView_MeChekClose_m2719645968 (MapUnityView_t4158937566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
