﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.InputField
struct InputField_t609046876;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginCheck
struct  LoginCheck_t219618111  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.InputField LoginCheck::m_ID
	InputField_t609046876 * ___m_ID_2;
	// UnityEngine.UI.InputField LoginCheck::m_PW
	InputField_t609046876 * ___m_PW_3;

public:
	inline static int32_t get_offset_of_m_ID_2() { return static_cast<int32_t>(offsetof(LoginCheck_t219618111, ___m_ID_2)); }
	inline InputField_t609046876 * get_m_ID_2() const { return ___m_ID_2; }
	inline InputField_t609046876 ** get_address_of_m_ID_2() { return &___m_ID_2; }
	inline void set_m_ID_2(InputField_t609046876 * value)
	{
		___m_ID_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_ID_2, value);
	}

	inline static int32_t get_offset_of_m_PW_3() { return static_cast<int32_t>(offsetof(LoginCheck_t219618111, ___m_PW_3)); }
	inline InputField_t609046876 * get_m_PW_3() const { return ___m_PW_3; }
	inline InputField_t609046876 ** get_address_of_m_PW_3() { return &___m_PW_3; }
	inline void set_m_PW_3(InputField_t609046876 * value)
	{
		___m_PW_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_PW_3, value);
	}
};

struct LoginCheck_t219618111_StaticFields
{
public:
	// System.Int32 LoginCheck::_LoginResult
	int32_t ____LoginResult_4;

public:
	inline static int32_t get_offset_of__LoginResult_4() { return static_cast<int32_t>(offsetof(LoginCheck_t219618111_StaticFields, ____LoginResult_4)); }
	inline int32_t get__LoginResult_4() const { return ____LoginResult_4; }
	inline int32_t* get_address_of__LoginResult_4() { return &____LoginResult_4; }
	inline void set__LoginResult_4(int32_t value)
	{
		____LoginResult_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
