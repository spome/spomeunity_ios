﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen2887177558MethodDeclarations.h"

// System.Void System.Comparison`1<TMPro.TMP_Sprite>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m1607559074(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t2605785094 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m487232819_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<TMPro.TMP_Sprite>::Invoke(T,T)
#define Comparison_1_Invoke_m3169311894(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t2605785094 *, TMP_Sprite_t3889423907 *, TMP_Sprite_t3889423907 *, const MethodInfo*))Comparison_1_Invoke_m1888033133_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<TMPro.TMP_Sprite>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m1030701023(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t2605785094 *, TMP_Sprite_t3889423907 *, TMP_Sprite_t3889423907 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3177996774_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<TMPro.TMP_Sprite>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m1894608918(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t2605785094 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m651541983_gshared)(__this, ___result0, method)
