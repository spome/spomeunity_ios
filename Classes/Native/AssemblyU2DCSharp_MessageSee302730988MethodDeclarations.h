﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageSee
struct MessageSee_t302730988;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CurrentState_MemberStates3108678483.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "mscorlib_System_String7231557.h"

// System.Void MessageSee::.ctor()
extern "C"  void MessageSee__ctor_m978080559 (MessageSee_t302730988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MessageSee::JsonStart(CurrentState/MemberStates)
extern "C"  Il2CppObject * MessageSee_JsonStart_m1847667520 (MessageSee_t302730988 * __this, int32_t ___MemberState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MessageSee::MessageSeeCheckProgress(UnityEngine.WWW)
extern "C"  Il2CppObject * MessageSee_MessageSeeCheckProgress_m1726939908 (MessageSee_t302730988 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSee::TakeJson(System.String)
extern "C"  void MessageSee_TakeJson_m4186039550 (MessageSee_t302730988 * __this, String_t* ___js0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MessageSee::RecvJsonStart()
extern "C"  Il2CppObject * MessageSee_RecvJsonStart_m2967129369 (MessageSee_t302730988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MessageSee::MessageSeeRecvCheckProgress(UnityEngine.WWW)
extern "C"  Il2CppObject * MessageSee_MessageSeeRecvCheckProgress_m1648991882 (MessageSee_t302730988 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSee::RecvTakeJson(System.String)
extern "C"  void MessageSee_RecvTakeJson_m3430226488 (MessageSee_t302730988 * __this, String_t* ___js0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSee::OnEnable()
extern "C"  void MessageSee_OnEnable_m3011375607 (MessageSee_t302730988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSee::MessageGet()
extern "C"  void MessageSee_MessageGet_m1172803396 (MessageSee_t302730988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSee::MessageRecv()
extern "C"  void MessageSee_MessageRecv_m2311656090 (MessageSee_t302730988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSee::MessageseeButton(System.Int32)
extern "C"  void MessageSee_MessageseeButton_m1046720708 (MessageSee_t302730988 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSee::MessageseeBackButton()
extern "C"  void MessageSee_MessageseeBackButton_m2729165306 (MessageSee_t302730988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSee::Update()
extern "C"  void MessageSee_Update_m1982588350 (MessageSee_t302730988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
