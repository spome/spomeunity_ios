﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageCheck
struct MessageCheck_t3146986849;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3134621005;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"

// System.Void MessageCheck::.ctor()
extern "C"  void MessageCheck__ctor_m3855891034 (MessageCheck_t3146986849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck::Awake()
extern "C"  void MessageCheck_Awake_m4093496253 (MessageCheck_t3146986849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck::Start()
extern "C"  void MessageCheck_Start_m2803028826 (MessageCheck_t3146986849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck::ButtonClick(System.Int32)
extern "C"  void MessageCheck_ButtonClick_m2729009791 (MessageCheck_t3146986849 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck::VideoPlay()
extern "C"  void MessageCheck_VideoPlay_m1709871751 (MessageCheck_t3146986849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck::MapView()
extern "C"  void MessageCheck_MapView_m3885113241 (MessageCheck_t3146986849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MessageCheck::VideoDown()
extern "C"  Il2CppObject * MessageCheck_VideoDown_m622930733 (MessageCheck_t3146986849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MessageCheck::VideoStart(System.Int32,System.String)
extern "C"  Il2CppObject * MessageCheck_VideoStart_m2620798630 (MessageCheck_t3146986849 * __this, int32_t ___num0, String_t* ___filecategory1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MessageCheck::ImageLoad(System.String)
extern "C"  Il2CppObject * MessageCheck_ImageLoad_m1220711249 (MessageCheck_t3146986849 * __this, String_t* ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck::ImageVisible()
extern "C"  void MessageCheck_ImageVisible_m3359338369 (MessageCheck_t3146986849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MessageCheck::CheckProgress(UnityEngine.WWW)
extern "C"  Il2CppObject * MessageCheck_CheckProgress_m2269471373 (MessageCheck_t3146986849 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck::emailInsert()
extern "C"  void MessageCheck_emailInsert_m3021129357 (MessageCheck_t3146986849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MessageCheck::EmailInsertserver(System.String,System.String,System.String,System.String)
extern "C"  Il2CppObject * MessageCheck_EmailInsertserver_m687391374 (MessageCheck_t3146986849 * __this, String_t* ___title0, String_t* ___email1, String_t* ___customid2, String_t* ___messageid3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MessageCheck::EmailInsertserverProgress(UnityEngine.WWW)
extern "C"  Il2CppObject * MessageCheck_EmailInsertserverProgress_m3801537309 (MessageCheck_t3146986849 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck::EmailInsertserverTakeJson(System.String)
extern "C"  void MessageCheck_EmailInsertserverTakeJson_m3006726659 (MessageCheck_t3146986849 * __this, String_t* ___js0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck::MessageDeleteBtn()
extern "C"  void MessageCheck_MessageDeleteBtn_m1972104212 (MessageCheck_t3146986849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MessageCheck::MessageDeleteServerUpdate()
extern "C"  Il2CppObject * MessageCheck_MessageDeleteServerUpdate_m1372336334 (MessageCheck_t3146986849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck::MessageDeleteCheck()
extern "C"  void MessageCheck_MessageDeleteCheck_m1649034752 (MessageCheck_t3146986849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MessageCheck::MessageDeleteServerCheck()
extern "C"  Il2CppObject * MessageCheck_MessageDeleteServerCheck_m3514863173 (MessageCheck_t3146986849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck::MessageDeleteTakeJson(System.String)
extern "C"  void MessageCheck_MessageDeleteTakeJson_m2005989801 (MessageCheck_t3146986849 * __this, String_t* ___js0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck::ArView()
extern "C"  void MessageCheck_ArView_m1885089376 (MessageCheck_t3146986849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck::Update()
extern "C"  void MessageCheck_Update_m1000399859 (MessageCheck_t3146986849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
