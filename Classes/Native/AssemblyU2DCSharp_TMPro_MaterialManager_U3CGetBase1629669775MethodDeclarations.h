﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.MaterialManager/<GetBaseMaterial>c__AnonStorey2C
struct U3CGetBaseMaterialU3Ec__AnonStorey2C_t1629669775;
// TMPro.MaterialManager/MaskingMaterial
struct MaskingMaterial_t2640101498;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_MaterialManager_MaskingMat2640101498.h"

// System.Void TMPro.MaterialManager/<GetBaseMaterial>c__AnonStorey2C::.ctor()
extern "C"  void U3CGetBaseMaterialU3Ec__AnonStorey2C__ctor_m3178978284 (U3CGetBaseMaterialU3Ec__AnonStorey2C_t1629669775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.MaterialManager/<GetBaseMaterial>c__AnonStorey2C::<>m__A(TMPro.MaterialManager/MaskingMaterial)
extern "C"  bool U3CGetBaseMaterialU3Ec__AnonStorey2C_U3CU3Em__A_m1643468854 (U3CGetBaseMaterialU3Ec__AnonStorey2C_t1629669775 * __this, MaskingMaterial_t2640101498 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
