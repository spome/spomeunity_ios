﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Single[]
struct SingleU5BU5D_t2316563989;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_TMP_XmlTagStack_1_gen1206294321.h"

// System.Void TMPro.TMP_XmlTagStack`1<System.Single>::.ctor(T[])
extern "C"  void TMP_XmlTagStack_1__ctor_m3599830400_gshared (TMP_XmlTagStack_1_t1206294321 * __this, SingleU5BU5D_t2316563989* ___tagStack0, const MethodInfo* method);
#define TMP_XmlTagStack_1__ctor_m3599830400(__this, ___tagStack0, method) ((  void (*) (TMP_XmlTagStack_1_t1206294321 *, SingleU5BU5D_t2316563989*, const MethodInfo*))TMP_XmlTagStack_1__ctor_m3599830400_gshared)(__this, ___tagStack0, method)
// System.Void TMPro.TMP_XmlTagStack`1<System.Single>::Clear()
extern "C"  void TMP_XmlTagStack_1_Clear_m3069970983_gshared (TMP_XmlTagStack_1_t1206294321 * __this, const MethodInfo* method);
#define TMP_XmlTagStack_1_Clear_m3069970983(__this, method) ((  void (*) (TMP_XmlTagStack_1_t1206294321 *, const MethodInfo*))TMP_XmlTagStack_1_Clear_m3069970983_gshared)(__this, method)
// System.Void TMPro.TMP_XmlTagStack`1<System.Single>::SetDefault(T)
extern "C"  void TMP_XmlTagStack_1_SetDefault_m1476629495_gshared (TMP_XmlTagStack_1_t1206294321 * __this, float ___item0, const MethodInfo* method);
#define TMP_XmlTagStack_1_SetDefault_m1476629495(__this, ___item0, method) ((  void (*) (TMP_XmlTagStack_1_t1206294321 *, float, const MethodInfo*))TMP_XmlTagStack_1_SetDefault_m1476629495_gshared)(__this, ___item0, method)
// System.Void TMPro.TMP_XmlTagStack`1<System.Single>::Add(T)
extern "C"  void TMP_XmlTagStack_1_Add_m1419772675_gshared (TMP_XmlTagStack_1_t1206294321 * __this, float ___item0, const MethodInfo* method);
#define TMP_XmlTagStack_1_Add_m1419772675(__this, ___item0, method) ((  void (*) (TMP_XmlTagStack_1_t1206294321 *, float, const MethodInfo*))TMP_XmlTagStack_1_Add_m1419772675_gshared)(__this, ___item0, method)
// T TMPro.TMP_XmlTagStack`1<System.Single>::Remove()
extern "C"  float TMP_XmlTagStack_1_Remove_m1497142027_gshared (TMP_XmlTagStack_1_t1206294321 * __this, const MethodInfo* method);
#define TMP_XmlTagStack_1_Remove_m1497142027(__this, method) ((  float (*) (TMP_XmlTagStack_1_t1206294321 *, const MethodInfo*))TMP_XmlTagStack_1_Remove_m1497142027_gshared)(__this, method)
