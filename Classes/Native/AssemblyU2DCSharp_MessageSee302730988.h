﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.Button
struct Button_t3896396478;
// TMPro.TMP_Text
struct TMP_Text_t980027659;

#include "AssemblyU2DCSharp_CurrentScreen3026574117.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageSee
struct  MessageSee_t302730988  : public CurrentScreen_t3026574117
{
public:
	// UnityEngine.GameObject MessageSee::btn
	GameObject_t3674682005 * ___btn_7;
	// UnityEngine.UI.Text MessageSee::tm
	Text_t9039225 * ___tm_8;
	// UnityEngine.GameObject MessageSee::btns
	GameObject_t3674682005 * ___btns_9;
	// UnityEngine.GameObject MessageSee::mc
	GameObject_t3674682005 * ___mc_10;
	// UnityEngine.UI.Button MessageSee::recvbtn
	Button_t3896396478 * ___recvbtn_11;
	// System.Int32 MessageSee::categorynum
	int32_t ___categorynum_12;
	// UnityEngine.GameObject MessageSee::m_Text
	GameObject_t3674682005 * ___m_Text_13;
	// UnityEngine.GameObject MessageSee::m_GOBtn
	GameObject_t3674682005 * ___m_GOBtn_14;
	// UnityEngine.GameObject MessageSee::m_Button
	GameObject_t3674682005 * ___m_Button_15;
	// UnityEngine.GameObject MessageSee::m_Parent
	GameObject_t3674682005 * ___m_Parent_16;
	// TMPro.TMP_Text MessageSee::m_TitleText
	TMP_Text_t980027659 * ___m_TitleText_17;

public:
	inline static int32_t get_offset_of_btn_7() { return static_cast<int32_t>(offsetof(MessageSee_t302730988, ___btn_7)); }
	inline GameObject_t3674682005 * get_btn_7() const { return ___btn_7; }
	inline GameObject_t3674682005 ** get_address_of_btn_7() { return &___btn_7; }
	inline void set_btn_7(GameObject_t3674682005 * value)
	{
		___btn_7 = value;
		Il2CppCodeGenWriteBarrier(&___btn_7, value);
	}

	inline static int32_t get_offset_of_tm_8() { return static_cast<int32_t>(offsetof(MessageSee_t302730988, ___tm_8)); }
	inline Text_t9039225 * get_tm_8() const { return ___tm_8; }
	inline Text_t9039225 ** get_address_of_tm_8() { return &___tm_8; }
	inline void set_tm_8(Text_t9039225 * value)
	{
		___tm_8 = value;
		Il2CppCodeGenWriteBarrier(&___tm_8, value);
	}

	inline static int32_t get_offset_of_btns_9() { return static_cast<int32_t>(offsetof(MessageSee_t302730988, ___btns_9)); }
	inline GameObject_t3674682005 * get_btns_9() const { return ___btns_9; }
	inline GameObject_t3674682005 ** get_address_of_btns_9() { return &___btns_9; }
	inline void set_btns_9(GameObject_t3674682005 * value)
	{
		___btns_9 = value;
		Il2CppCodeGenWriteBarrier(&___btns_9, value);
	}

	inline static int32_t get_offset_of_mc_10() { return static_cast<int32_t>(offsetof(MessageSee_t302730988, ___mc_10)); }
	inline GameObject_t3674682005 * get_mc_10() const { return ___mc_10; }
	inline GameObject_t3674682005 ** get_address_of_mc_10() { return &___mc_10; }
	inline void set_mc_10(GameObject_t3674682005 * value)
	{
		___mc_10 = value;
		Il2CppCodeGenWriteBarrier(&___mc_10, value);
	}

	inline static int32_t get_offset_of_recvbtn_11() { return static_cast<int32_t>(offsetof(MessageSee_t302730988, ___recvbtn_11)); }
	inline Button_t3896396478 * get_recvbtn_11() const { return ___recvbtn_11; }
	inline Button_t3896396478 ** get_address_of_recvbtn_11() { return &___recvbtn_11; }
	inline void set_recvbtn_11(Button_t3896396478 * value)
	{
		___recvbtn_11 = value;
		Il2CppCodeGenWriteBarrier(&___recvbtn_11, value);
	}

	inline static int32_t get_offset_of_categorynum_12() { return static_cast<int32_t>(offsetof(MessageSee_t302730988, ___categorynum_12)); }
	inline int32_t get_categorynum_12() const { return ___categorynum_12; }
	inline int32_t* get_address_of_categorynum_12() { return &___categorynum_12; }
	inline void set_categorynum_12(int32_t value)
	{
		___categorynum_12 = value;
	}

	inline static int32_t get_offset_of_m_Text_13() { return static_cast<int32_t>(offsetof(MessageSee_t302730988, ___m_Text_13)); }
	inline GameObject_t3674682005 * get_m_Text_13() const { return ___m_Text_13; }
	inline GameObject_t3674682005 ** get_address_of_m_Text_13() { return &___m_Text_13; }
	inline void set_m_Text_13(GameObject_t3674682005 * value)
	{
		___m_Text_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_Text_13, value);
	}

	inline static int32_t get_offset_of_m_GOBtn_14() { return static_cast<int32_t>(offsetof(MessageSee_t302730988, ___m_GOBtn_14)); }
	inline GameObject_t3674682005 * get_m_GOBtn_14() const { return ___m_GOBtn_14; }
	inline GameObject_t3674682005 ** get_address_of_m_GOBtn_14() { return &___m_GOBtn_14; }
	inline void set_m_GOBtn_14(GameObject_t3674682005 * value)
	{
		___m_GOBtn_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_GOBtn_14, value);
	}

	inline static int32_t get_offset_of_m_Button_15() { return static_cast<int32_t>(offsetof(MessageSee_t302730988, ___m_Button_15)); }
	inline GameObject_t3674682005 * get_m_Button_15() const { return ___m_Button_15; }
	inline GameObject_t3674682005 ** get_address_of_m_Button_15() { return &___m_Button_15; }
	inline void set_m_Button_15(GameObject_t3674682005 * value)
	{
		___m_Button_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_Button_15, value);
	}

	inline static int32_t get_offset_of_m_Parent_16() { return static_cast<int32_t>(offsetof(MessageSee_t302730988, ___m_Parent_16)); }
	inline GameObject_t3674682005 * get_m_Parent_16() const { return ___m_Parent_16; }
	inline GameObject_t3674682005 ** get_address_of_m_Parent_16() { return &___m_Parent_16; }
	inline void set_m_Parent_16(GameObject_t3674682005 * value)
	{
		___m_Parent_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_Parent_16, value);
	}

	inline static int32_t get_offset_of_m_TitleText_17() { return static_cast<int32_t>(offsetof(MessageSee_t302730988, ___m_TitleText_17)); }
	inline TMP_Text_t980027659 * get_m_TitleText_17() const { return ___m_TitleText_17; }
	inline TMP_Text_t980027659 ** get_address_of_m_TitleText_17() { return &___m_TitleText_17; }
	inline void set_m_TitleText_17(TMP_Text_t980027659 * value)
	{
		___m_TitleText_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_TitleText_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
