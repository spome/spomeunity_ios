﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EmailChange/<CheckProgress>c__Iterator9
struct U3CCheckProgressU3Ec__Iterator9_t1393611626;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EmailChange/<CheckProgress>c__Iterator9::.ctor()
extern "C"  void U3CCheckProgressU3Ec__Iterator9__ctor_m4146656065 (U3CCheckProgressU3Ec__Iterator9_t1393611626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EmailChange/<CheckProgress>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckProgressU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1084189617 (U3CCheckProgressU3Ec__Iterator9_t1393611626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EmailChange/<CheckProgress>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckProgressU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m2715349317 (U3CCheckProgressU3Ec__Iterator9_t1393611626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EmailChange/<CheckProgress>c__Iterator9::MoveNext()
extern "C"  bool U3CCheckProgressU3Ec__Iterator9_MoveNext_m1071401747 (U3CCheckProgressU3Ec__Iterator9_t1393611626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmailChange/<CheckProgress>c__Iterator9::Dispose()
extern "C"  void U3CCheckProgressU3Ec__Iterator9_Dispose_m3400078142 (U3CCheckProgressU3Ec__Iterator9_t1393611626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmailChange/<CheckProgress>c__Iterator9::Reset()
extern "C"  void U3CCheckProgressU3Ec__Iterator9_Reset_m1793089006 (U3CCheckProgressU3Ec__Iterator9_t1393611626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
