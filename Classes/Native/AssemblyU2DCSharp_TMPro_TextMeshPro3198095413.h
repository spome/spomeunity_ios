﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Renderer
struct Renderer_t3076687687;
// UnityEngine.MeshFilter
struct MeshFilter_t3839065225;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material>
struct Dictionary_2_t3867863346;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t943818363;
// TMPro.TextContainer
struct TextContainer_t2231787766;

#include "AssemblyU2DCSharp_TMPro_TMP_Text980027659.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_TMPro_WordWrapState4047764895.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "AssemblyU2DCSharp_TMPro_MaskingTypes406604089.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "AssemblyU2DCSharp_TMPro_TMP_Compatibility_AnchorPo2956782356.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextMeshPro
struct  TextMeshPro_t3198095413  : public TMP_Text_t980027659
{
public:
	// UnityEngine.Vector2 TMPro.TextMeshPro::m_uvOffset
	Vector2_t4282066565  ___m_uvOffset_185;
	// System.Single TMPro.TextMeshPro::m_uvLineOffset
	float ___m_uvLineOffset_186;
	// System.Boolean TMPro.TextMeshPro::m_hasFontAssetChanged
	bool ___m_hasFontAssetChanged_187;
	// System.Boolean TMPro.TextMeshPro::m_isRightToLeft
	bool ___m_isRightToLeft_188;
	// UnityEngine.Vector3 TMPro.TextMeshPro::m_previousLossyScale
	Vector3_t4282066566  ___m_previousLossyScale_189;
	// UnityEngine.Renderer TMPro.TextMeshPro::m_renderer
	Renderer_t3076687687 * ___m_renderer_190;
	// UnityEngine.MeshFilter TMPro.TextMeshPro::m_meshFilter
	MeshFilter_t3839065225 * ___m_meshFilter_191;
	// System.Boolean TMPro.TextMeshPro::m_isFirstAllocation
	bool ___m_isFirstAllocation_192;
	// System.Int32 TMPro.TextMeshPro::m_max_characters
	int32_t ___m_max_characters_193;
	// System.Int32 TMPro.TextMeshPro::m_max_numberOfLines
	int32_t ___m_max_numberOfLines_194;
	// TMPro.WordWrapState TMPro.TextMeshPro::m_SavedWordWrapState
	WordWrapState_t4047764895  ___m_SavedWordWrapState_195;
	// TMPro.WordWrapState TMPro.TextMeshPro::m_SavedLineState
	WordWrapState_t4047764895  ___m_SavedLineState_196;
	// UnityEngine.Bounds TMPro.TextMeshPro::m_default_bounds
	Bounds_t2711641849  ___m_default_bounds_197;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material> TMPro.TextMeshPro::m_referencedMaterials
	Dictionary_2_t3867863346 * ___m_referencedMaterials_198;
	// System.Collections.Generic.List`1<UnityEngine.Material> TMPro.TextMeshPro::m_sharedMaterials
	List_1_t943818363 * ___m_sharedMaterials_199;
	// System.Boolean TMPro.TextMeshPro::m_isMaskingEnabled
	bool ___m_isMaskingEnabled_200;
	// System.Boolean TMPro.TextMeshPro::isMaskUpdateRequired
	bool ___isMaskUpdateRequired_201;
	// TMPro.MaskingTypes TMPro.TextMeshPro::m_maskType
	int32_t ___m_maskType_202;
	// UnityEngine.Matrix4x4 TMPro.TextMeshPro::m_EnvMapMatrix
	Matrix4x4_t1651859333  ___m_EnvMapMatrix_203;
	// TMPro.TextContainer TMPro.TextMeshPro::m_textContainer
	TextContainer_t2231787766 * ___m_textContainer_204;
	// System.Boolean TMPro.TextMeshPro::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_205;
	// System.Int32 TMPro.TextMeshPro::m_recursiveCount
	int32_t ___m_recursiveCount_206;
	// System.Int32 TMPro.TextMeshPro::loopCountA
	int32_t ___loopCountA_207;
	// System.Single TMPro.TextMeshPro::m_lineLength
	float ___m_lineLength_208;
	// TMPro.TMP_Compatibility/AnchorPositions TMPro.TextMeshPro::m_anchor
	int32_t ___m_anchor_209;
	// System.Boolean TMPro.TextMeshPro::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_210;
	// System.Boolean TMPro.TextMeshPro::m_currentAutoSizeMode
	bool ___m_currentAutoSizeMode_211;

public:
	inline static int32_t get_offset_of_m_uvOffset_185() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_uvOffset_185)); }
	inline Vector2_t4282066565  get_m_uvOffset_185() const { return ___m_uvOffset_185; }
	inline Vector2_t4282066565 * get_address_of_m_uvOffset_185() { return &___m_uvOffset_185; }
	inline void set_m_uvOffset_185(Vector2_t4282066565  value)
	{
		___m_uvOffset_185 = value;
	}

	inline static int32_t get_offset_of_m_uvLineOffset_186() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_uvLineOffset_186)); }
	inline float get_m_uvLineOffset_186() const { return ___m_uvLineOffset_186; }
	inline float* get_address_of_m_uvLineOffset_186() { return &___m_uvLineOffset_186; }
	inline void set_m_uvLineOffset_186(float value)
	{
		___m_uvLineOffset_186 = value;
	}

	inline static int32_t get_offset_of_m_hasFontAssetChanged_187() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_hasFontAssetChanged_187)); }
	inline bool get_m_hasFontAssetChanged_187() const { return ___m_hasFontAssetChanged_187; }
	inline bool* get_address_of_m_hasFontAssetChanged_187() { return &___m_hasFontAssetChanged_187; }
	inline void set_m_hasFontAssetChanged_187(bool value)
	{
		___m_hasFontAssetChanged_187 = value;
	}

	inline static int32_t get_offset_of_m_isRightToLeft_188() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_isRightToLeft_188)); }
	inline bool get_m_isRightToLeft_188() const { return ___m_isRightToLeft_188; }
	inline bool* get_address_of_m_isRightToLeft_188() { return &___m_isRightToLeft_188; }
	inline void set_m_isRightToLeft_188(bool value)
	{
		___m_isRightToLeft_188 = value;
	}

	inline static int32_t get_offset_of_m_previousLossyScale_189() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_previousLossyScale_189)); }
	inline Vector3_t4282066566  get_m_previousLossyScale_189() const { return ___m_previousLossyScale_189; }
	inline Vector3_t4282066566 * get_address_of_m_previousLossyScale_189() { return &___m_previousLossyScale_189; }
	inline void set_m_previousLossyScale_189(Vector3_t4282066566  value)
	{
		___m_previousLossyScale_189 = value;
	}

	inline static int32_t get_offset_of_m_renderer_190() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_renderer_190)); }
	inline Renderer_t3076687687 * get_m_renderer_190() const { return ___m_renderer_190; }
	inline Renderer_t3076687687 ** get_address_of_m_renderer_190() { return &___m_renderer_190; }
	inline void set_m_renderer_190(Renderer_t3076687687 * value)
	{
		___m_renderer_190 = value;
		Il2CppCodeGenWriteBarrier(&___m_renderer_190, value);
	}

	inline static int32_t get_offset_of_m_meshFilter_191() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_meshFilter_191)); }
	inline MeshFilter_t3839065225 * get_m_meshFilter_191() const { return ___m_meshFilter_191; }
	inline MeshFilter_t3839065225 ** get_address_of_m_meshFilter_191() { return &___m_meshFilter_191; }
	inline void set_m_meshFilter_191(MeshFilter_t3839065225 * value)
	{
		___m_meshFilter_191 = value;
		Il2CppCodeGenWriteBarrier(&___m_meshFilter_191, value);
	}

	inline static int32_t get_offset_of_m_isFirstAllocation_192() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_isFirstAllocation_192)); }
	inline bool get_m_isFirstAllocation_192() const { return ___m_isFirstAllocation_192; }
	inline bool* get_address_of_m_isFirstAllocation_192() { return &___m_isFirstAllocation_192; }
	inline void set_m_isFirstAllocation_192(bool value)
	{
		___m_isFirstAllocation_192 = value;
	}

	inline static int32_t get_offset_of_m_max_characters_193() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_max_characters_193)); }
	inline int32_t get_m_max_characters_193() const { return ___m_max_characters_193; }
	inline int32_t* get_address_of_m_max_characters_193() { return &___m_max_characters_193; }
	inline void set_m_max_characters_193(int32_t value)
	{
		___m_max_characters_193 = value;
	}

	inline static int32_t get_offset_of_m_max_numberOfLines_194() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_max_numberOfLines_194)); }
	inline int32_t get_m_max_numberOfLines_194() const { return ___m_max_numberOfLines_194; }
	inline int32_t* get_address_of_m_max_numberOfLines_194() { return &___m_max_numberOfLines_194; }
	inline void set_m_max_numberOfLines_194(int32_t value)
	{
		___m_max_numberOfLines_194 = value;
	}

	inline static int32_t get_offset_of_m_SavedWordWrapState_195() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_SavedWordWrapState_195)); }
	inline WordWrapState_t4047764895  get_m_SavedWordWrapState_195() const { return ___m_SavedWordWrapState_195; }
	inline WordWrapState_t4047764895 * get_address_of_m_SavedWordWrapState_195() { return &___m_SavedWordWrapState_195; }
	inline void set_m_SavedWordWrapState_195(WordWrapState_t4047764895  value)
	{
		___m_SavedWordWrapState_195 = value;
	}

	inline static int32_t get_offset_of_m_SavedLineState_196() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_SavedLineState_196)); }
	inline WordWrapState_t4047764895  get_m_SavedLineState_196() const { return ___m_SavedLineState_196; }
	inline WordWrapState_t4047764895 * get_address_of_m_SavedLineState_196() { return &___m_SavedLineState_196; }
	inline void set_m_SavedLineState_196(WordWrapState_t4047764895  value)
	{
		___m_SavedLineState_196 = value;
	}

	inline static int32_t get_offset_of_m_default_bounds_197() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_default_bounds_197)); }
	inline Bounds_t2711641849  get_m_default_bounds_197() const { return ___m_default_bounds_197; }
	inline Bounds_t2711641849 * get_address_of_m_default_bounds_197() { return &___m_default_bounds_197; }
	inline void set_m_default_bounds_197(Bounds_t2711641849  value)
	{
		___m_default_bounds_197 = value;
	}

	inline static int32_t get_offset_of_m_referencedMaterials_198() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_referencedMaterials_198)); }
	inline Dictionary_2_t3867863346 * get_m_referencedMaterials_198() const { return ___m_referencedMaterials_198; }
	inline Dictionary_2_t3867863346 ** get_address_of_m_referencedMaterials_198() { return &___m_referencedMaterials_198; }
	inline void set_m_referencedMaterials_198(Dictionary_2_t3867863346 * value)
	{
		___m_referencedMaterials_198 = value;
		Il2CppCodeGenWriteBarrier(&___m_referencedMaterials_198, value);
	}

	inline static int32_t get_offset_of_m_sharedMaterials_199() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_sharedMaterials_199)); }
	inline List_1_t943818363 * get_m_sharedMaterials_199() const { return ___m_sharedMaterials_199; }
	inline List_1_t943818363 ** get_address_of_m_sharedMaterials_199() { return &___m_sharedMaterials_199; }
	inline void set_m_sharedMaterials_199(List_1_t943818363 * value)
	{
		___m_sharedMaterials_199 = value;
		Il2CppCodeGenWriteBarrier(&___m_sharedMaterials_199, value);
	}

	inline static int32_t get_offset_of_m_isMaskingEnabled_200() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_isMaskingEnabled_200)); }
	inline bool get_m_isMaskingEnabled_200() const { return ___m_isMaskingEnabled_200; }
	inline bool* get_address_of_m_isMaskingEnabled_200() { return &___m_isMaskingEnabled_200; }
	inline void set_m_isMaskingEnabled_200(bool value)
	{
		___m_isMaskingEnabled_200 = value;
	}

	inline static int32_t get_offset_of_isMaskUpdateRequired_201() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___isMaskUpdateRequired_201)); }
	inline bool get_isMaskUpdateRequired_201() const { return ___isMaskUpdateRequired_201; }
	inline bool* get_address_of_isMaskUpdateRequired_201() { return &___isMaskUpdateRequired_201; }
	inline void set_isMaskUpdateRequired_201(bool value)
	{
		___isMaskUpdateRequired_201 = value;
	}

	inline static int32_t get_offset_of_m_maskType_202() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_maskType_202)); }
	inline int32_t get_m_maskType_202() const { return ___m_maskType_202; }
	inline int32_t* get_address_of_m_maskType_202() { return &___m_maskType_202; }
	inline void set_m_maskType_202(int32_t value)
	{
		___m_maskType_202 = value;
	}

	inline static int32_t get_offset_of_m_EnvMapMatrix_203() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_EnvMapMatrix_203)); }
	inline Matrix4x4_t1651859333  get_m_EnvMapMatrix_203() const { return ___m_EnvMapMatrix_203; }
	inline Matrix4x4_t1651859333 * get_address_of_m_EnvMapMatrix_203() { return &___m_EnvMapMatrix_203; }
	inline void set_m_EnvMapMatrix_203(Matrix4x4_t1651859333  value)
	{
		___m_EnvMapMatrix_203 = value;
	}

	inline static int32_t get_offset_of_m_textContainer_204() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_textContainer_204)); }
	inline TextContainer_t2231787766 * get_m_textContainer_204() const { return ___m_textContainer_204; }
	inline TextContainer_t2231787766 ** get_address_of_m_textContainer_204() { return &___m_textContainer_204; }
	inline void set_m_textContainer_204(TextContainer_t2231787766 * value)
	{
		___m_textContainer_204 = value;
		Il2CppCodeGenWriteBarrier(&___m_textContainer_204, value);
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_205() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_isRegisteredForEvents_205)); }
	inline bool get_m_isRegisteredForEvents_205() const { return ___m_isRegisteredForEvents_205; }
	inline bool* get_address_of_m_isRegisteredForEvents_205() { return &___m_isRegisteredForEvents_205; }
	inline void set_m_isRegisteredForEvents_205(bool value)
	{
		___m_isRegisteredForEvents_205 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCount_206() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_recursiveCount_206)); }
	inline int32_t get_m_recursiveCount_206() const { return ___m_recursiveCount_206; }
	inline int32_t* get_address_of_m_recursiveCount_206() { return &___m_recursiveCount_206; }
	inline void set_m_recursiveCount_206(int32_t value)
	{
		___m_recursiveCount_206 = value;
	}

	inline static int32_t get_offset_of_loopCountA_207() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___loopCountA_207)); }
	inline int32_t get_loopCountA_207() const { return ___loopCountA_207; }
	inline int32_t* get_address_of_loopCountA_207() { return &___loopCountA_207; }
	inline void set_loopCountA_207(int32_t value)
	{
		___loopCountA_207 = value;
	}

	inline static int32_t get_offset_of_m_lineLength_208() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_lineLength_208)); }
	inline float get_m_lineLength_208() const { return ___m_lineLength_208; }
	inline float* get_address_of_m_lineLength_208() { return &___m_lineLength_208; }
	inline void set_m_lineLength_208(float value)
	{
		___m_lineLength_208 = value;
	}

	inline static int32_t get_offset_of_m_anchor_209() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_anchor_209)); }
	inline int32_t get_m_anchor_209() const { return ___m_anchor_209; }
	inline int32_t* get_address_of_m_anchor_209() { return &___m_anchor_209; }
	inline void set_m_anchor_209(int32_t value)
	{
		___m_anchor_209 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_210() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_autoSizeTextContainer_210)); }
	inline bool get_m_autoSizeTextContainer_210() const { return ___m_autoSizeTextContainer_210; }
	inline bool* get_address_of_m_autoSizeTextContainer_210() { return &___m_autoSizeTextContainer_210; }
	inline void set_m_autoSizeTextContainer_210(bool value)
	{
		___m_autoSizeTextContainer_210 = value;
	}

	inline static int32_t get_offset_of_m_currentAutoSizeMode_211() { return static_cast<int32_t>(offsetof(TextMeshPro_t3198095413, ___m_currentAutoSizeMode_211)); }
	inline bool get_m_currentAutoSizeMode_211() const { return ___m_currentAutoSizeMode_211; }
	inline bool* get_address_of_m_currentAutoSizeMode_211() { return &___m_currentAutoSizeMode_211; }
	inline void set_m_currentAutoSizeMode_211(bool value)
	{
		___m_currentAutoSizeMode_211 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
