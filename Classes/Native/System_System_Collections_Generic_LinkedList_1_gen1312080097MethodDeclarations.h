﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2045451540MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::.ctor()
#define LinkedList_1__ctor_m336989318(__this, method) ((  void (*) (LinkedList_1_t1312080097 *, const MethodInfo*))LinkedList_1__ctor_m2955457271_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1__ctor_m757479623(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t1312080097 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))LinkedList_1__ctor_m3369579448_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::System.Collections.Generic.ICollection<T>.Add(T)
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1356499129(__this, ___value0, method) ((  void (*) (LinkedList_1_t1312080097 *, Action_2_t3437444928 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3576108392_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define LinkedList_1_System_Collections_ICollection_CopyTo_m354855806(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t1312080097 *, Il2CppArray *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3811412568(__this, method) ((  Il2CppObject* (*) (LinkedList_1_t1312080097 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2027502985_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::System.Collections.IEnumerable.GetEnumerator()
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m1715860109(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t1312080097 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m51916412_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2666809545(__this, method) ((  bool (*) (LinkedList_1_t1312080097 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2230847288_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::System.Collections.ICollection.get_IsSynchronized()
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m1215680404(__this, method) ((  bool (*) (LinkedList_1_t1312080097 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m683991045_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::System.Collections.ICollection.get_SyncRoot()
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m588443796(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t1312080097 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m573420165_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_VerifyReferencedNode_m1102435173(__this, ___node0, method) ((  void (*) (LinkedList_1_t1312080097 *, LinkedListNode_1_t3054179635 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m3939775124_gshared)(__this, ___node0, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::AddLast(T)
#define LinkedList_1_AddLast_m2525213909(__this, ___value0, method) ((  LinkedListNode_1_t3054179635 * (*) (LinkedList_1_t1312080097 *, Action_2_t3437444928 *, const MethodInfo*))LinkedList_1_AddLast_m4070107716_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::Clear()
#define LinkedList_1_Clear_m2038089905(__this, method) ((  void (*) (LinkedList_1_t1312080097 *, const MethodInfo*))LinkedList_1_Clear_m361590562_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::Contains(T)
#define LinkedList_1_Contains_m2895826251(__this, ___value0, method) ((  bool (*) (LinkedList_1_t1312080097 *, Action_2_t3437444928 *, const MethodInfo*))LinkedList_1_Contains_m3484410556_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::CopyTo(T[],System.Int32)
#define LinkedList_1_CopyTo_m2812491497(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t1312080097 *, Action_2U5BU5D_t3639450561*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m3470139544_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::Find(T)
#define LinkedList_1_Find_m3620956149(__this, ___value0, method) ((  LinkedListNode_1_t3054179635 * (*) (LinkedList_1_t1312080097 *, Action_2_t3437444928 *, const MethodInfo*))LinkedList_1_Find_m2643247334_gshared)(__this, ___value0, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::GetEnumerator()
#define LinkedList_1_GetEnumerator_m3719937365(__this, method) ((  Enumerator_t2795234657  (*) (LinkedList_1_t1312080097 *, const MethodInfo*))LinkedList_1_GetEnumerator_m3713737734_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1_GetObjectData_m1625409828(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t1312080097 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))LinkedList_1_GetObjectData_m3974480661_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::OnDeserialization(System.Object)
#define LinkedList_1_OnDeserialization_m1414846400(__this, ___sender0, method) ((  void (*) (LinkedList_1_t1312080097 *, Il2CppObject *, const MethodInfo*))LinkedList_1_OnDeserialization_m3445006959_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::Remove(T)
#define LinkedList_1_Remove_m4096287750(__this, ___value0, method) ((  bool (*) (LinkedList_1_t1312080097 *, Action_2_t3437444928 *, const MethodInfo*))LinkedList_1_Remove_m3283493303_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_Remove_m3278370229(__this, ___node0, method) ((  void (*) (LinkedList_1_t1312080097 *, LinkedListNode_1_t3054179635 *, const MethodInfo*))LinkedList_1_Remove_m4034790180_gshared)(__this, ___node0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::RemoveLast()
#define LinkedList_1_RemoveLast_m1961147640(__this, method) ((  void (*) (LinkedList_1_t1312080097 *, const MethodInfo*))LinkedList_1_RemoveLast_m2573038887_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::get_Count()
#define LinkedList_1_get_Count_m2761310042(__this, method) ((  int32_t (*) (LinkedList_1_t1312080097 *, const MethodInfo*))LinkedList_1_get_Count_m1368924491_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::get_First()
#define LinkedList_1_get_First_m2631520025(__this, method) ((  LinkedListNode_1_t3054179635 * (*) (LinkedList_1_t1312080097 *, const MethodInfo*))LinkedList_1_get_First_m3278587786_gshared)(__this, method)
