﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// Messages[]
struct MessagesU5BU5D_t3583122277;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.Button
struct Button_t3896396478;
// UnityEngine.UI.InputField
struct InputField_t609046876;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageCheck
struct  MessageCheck_t3146986849  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject MessageCheck::panel
	GameObject_t3674682005 * ___panel_2;
	// UnityEngine.GameObject MessageCheck::ProgressBar
	GameObject_t3674682005 * ___ProgressBar_3;
	// UnityEngine.UI.Image MessageCheck::image
	Image_t538875265 * ___image_4;
	// UnityEngine.RectTransform MessageCheck::imagesize
	RectTransform_t972643934 * ___imagesize_5;
	// UnityEngine.UI.Text MessageCheck::Title
	Text_t9039225 * ___Title_7;
	// UnityEngine.UI.Text MessageCheck::CurrentDistance
	Text_t9039225 * ___CurrentDistance_8;
	// UnityEngine.UI.Button MessageCheck::MessageSeeBtn
	Button_t3896396478 * ___MessageSeeBtn_9;
	// UnityEngine.UI.Button MessageCheck::EmailInsertBtn
	Button_t3896396478 * ___EmailInsertBtn_10;
	// UnityEngine.UI.InputField MessageCheck::email_input
	InputField_t609046876 * ___email_input_11;
	// UnityEngine.UI.InputField MessageCheck::name_input
	InputField_t609046876 * ___name_input_12;
	// UnityEngine.UI.Text MessageCheck::_name
	Text_t9039225 * ____name_13;
	// UnityEngine.UI.Text MessageCheck::_email
	Text_t9039225 * ____email_14;
	// UnityEngine.GameObject MessageCheck::_Sprite
	GameObject_t3674682005 * ____Sprite_15;
	// UnityEngine.UI.Button MessageCheck::_MessageDelete
	Button_t3896396478 * ____MessageDelete_16;
	// System.Boolean MessageCheck::_messagecheck
	bool ____messagecheck_17;
	// System.Double MessageCheck::_distancedouble
	double ____distancedouble_18;
	// System.String MessageCheck::_videourl
	String_t* ____videourl_19;
	// System.String MessageCheck::_videoname
	String_t* ____videoname_20;
	// System.String MessageCheck::lat
	String_t* ___lat_21;
	// System.String MessageCheck::lon
	String_t* ___lon_22;
	// System.String MessageCheck::_messageid
	String_t* ____messageid_23;
	// System.String MessageCheck::filetype
	String_t* ___filetype_24;
	// System.Single MessageCheck::timer
	float ___timer_26;
	// System.Int32 MessageCheck::waitingTime
	int32_t ___waitingTime_27;
	// System.String MessageCheck::filenamecategory
	String_t* ___filenamecategory_28;
	// System.Int32 MessageCheck::category
	int32_t ___category_29;

public:
	inline static int32_t get_offset_of_panel_2() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ___panel_2)); }
	inline GameObject_t3674682005 * get_panel_2() const { return ___panel_2; }
	inline GameObject_t3674682005 ** get_address_of_panel_2() { return &___panel_2; }
	inline void set_panel_2(GameObject_t3674682005 * value)
	{
		___panel_2 = value;
		Il2CppCodeGenWriteBarrier(&___panel_2, value);
	}

	inline static int32_t get_offset_of_ProgressBar_3() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ___ProgressBar_3)); }
	inline GameObject_t3674682005 * get_ProgressBar_3() const { return ___ProgressBar_3; }
	inline GameObject_t3674682005 ** get_address_of_ProgressBar_3() { return &___ProgressBar_3; }
	inline void set_ProgressBar_3(GameObject_t3674682005 * value)
	{
		___ProgressBar_3 = value;
		Il2CppCodeGenWriteBarrier(&___ProgressBar_3, value);
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ___image_4)); }
	inline Image_t538875265 * get_image_4() const { return ___image_4; }
	inline Image_t538875265 ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(Image_t538875265 * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier(&___image_4, value);
	}

	inline static int32_t get_offset_of_imagesize_5() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ___imagesize_5)); }
	inline RectTransform_t972643934 * get_imagesize_5() const { return ___imagesize_5; }
	inline RectTransform_t972643934 ** get_address_of_imagesize_5() { return &___imagesize_5; }
	inline void set_imagesize_5(RectTransform_t972643934 * value)
	{
		___imagesize_5 = value;
		Il2CppCodeGenWriteBarrier(&___imagesize_5, value);
	}

	inline static int32_t get_offset_of_Title_7() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ___Title_7)); }
	inline Text_t9039225 * get_Title_7() const { return ___Title_7; }
	inline Text_t9039225 ** get_address_of_Title_7() { return &___Title_7; }
	inline void set_Title_7(Text_t9039225 * value)
	{
		___Title_7 = value;
		Il2CppCodeGenWriteBarrier(&___Title_7, value);
	}

	inline static int32_t get_offset_of_CurrentDistance_8() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ___CurrentDistance_8)); }
	inline Text_t9039225 * get_CurrentDistance_8() const { return ___CurrentDistance_8; }
	inline Text_t9039225 ** get_address_of_CurrentDistance_8() { return &___CurrentDistance_8; }
	inline void set_CurrentDistance_8(Text_t9039225 * value)
	{
		___CurrentDistance_8 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentDistance_8, value);
	}

	inline static int32_t get_offset_of_MessageSeeBtn_9() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ___MessageSeeBtn_9)); }
	inline Button_t3896396478 * get_MessageSeeBtn_9() const { return ___MessageSeeBtn_9; }
	inline Button_t3896396478 ** get_address_of_MessageSeeBtn_9() { return &___MessageSeeBtn_9; }
	inline void set_MessageSeeBtn_9(Button_t3896396478 * value)
	{
		___MessageSeeBtn_9 = value;
		Il2CppCodeGenWriteBarrier(&___MessageSeeBtn_9, value);
	}

	inline static int32_t get_offset_of_EmailInsertBtn_10() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ___EmailInsertBtn_10)); }
	inline Button_t3896396478 * get_EmailInsertBtn_10() const { return ___EmailInsertBtn_10; }
	inline Button_t3896396478 ** get_address_of_EmailInsertBtn_10() { return &___EmailInsertBtn_10; }
	inline void set_EmailInsertBtn_10(Button_t3896396478 * value)
	{
		___EmailInsertBtn_10 = value;
		Il2CppCodeGenWriteBarrier(&___EmailInsertBtn_10, value);
	}

	inline static int32_t get_offset_of_email_input_11() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ___email_input_11)); }
	inline InputField_t609046876 * get_email_input_11() const { return ___email_input_11; }
	inline InputField_t609046876 ** get_address_of_email_input_11() { return &___email_input_11; }
	inline void set_email_input_11(InputField_t609046876 * value)
	{
		___email_input_11 = value;
		Il2CppCodeGenWriteBarrier(&___email_input_11, value);
	}

	inline static int32_t get_offset_of_name_input_12() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ___name_input_12)); }
	inline InputField_t609046876 * get_name_input_12() const { return ___name_input_12; }
	inline InputField_t609046876 ** get_address_of_name_input_12() { return &___name_input_12; }
	inline void set_name_input_12(InputField_t609046876 * value)
	{
		___name_input_12 = value;
		Il2CppCodeGenWriteBarrier(&___name_input_12, value);
	}

	inline static int32_t get_offset_of__name_13() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ____name_13)); }
	inline Text_t9039225 * get__name_13() const { return ____name_13; }
	inline Text_t9039225 ** get_address_of__name_13() { return &____name_13; }
	inline void set__name_13(Text_t9039225 * value)
	{
		____name_13 = value;
		Il2CppCodeGenWriteBarrier(&____name_13, value);
	}

	inline static int32_t get_offset_of__email_14() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ____email_14)); }
	inline Text_t9039225 * get__email_14() const { return ____email_14; }
	inline Text_t9039225 ** get_address_of__email_14() { return &____email_14; }
	inline void set__email_14(Text_t9039225 * value)
	{
		____email_14 = value;
		Il2CppCodeGenWriteBarrier(&____email_14, value);
	}

	inline static int32_t get_offset_of__Sprite_15() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ____Sprite_15)); }
	inline GameObject_t3674682005 * get__Sprite_15() const { return ____Sprite_15; }
	inline GameObject_t3674682005 ** get_address_of__Sprite_15() { return &____Sprite_15; }
	inline void set__Sprite_15(GameObject_t3674682005 * value)
	{
		____Sprite_15 = value;
		Il2CppCodeGenWriteBarrier(&____Sprite_15, value);
	}

	inline static int32_t get_offset_of__MessageDelete_16() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ____MessageDelete_16)); }
	inline Button_t3896396478 * get__MessageDelete_16() const { return ____MessageDelete_16; }
	inline Button_t3896396478 ** get_address_of__MessageDelete_16() { return &____MessageDelete_16; }
	inline void set__MessageDelete_16(Button_t3896396478 * value)
	{
		____MessageDelete_16 = value;
		Il2CppCodeGenWriteBarrier(&____MessageDelete_16, value);
	}

	inline static int32_t get_offset_of__messagecheck_17() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ____messagecheck_17)); }
	inline bool get__messagecheck_17() const { return ____messagecheck_17; }
	inline bool* get_address_of__messagecheck_17() { return &____messagecheck_17; }
	inline void set__messagecheck_17(bool value)
	{
		____messagecheck_17 = value;
	}

	inline static int32_t get_offset_of__distancedouble_18() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ____distancedouble_18)); }
	inline double get__distancedouble_18() const { return ____distancedouble_18; }
	inline double* get_address_of__distancedouble_18() { return &____distancedouble_18; }
	inline void set__distancedouble_18(double value)
	{
		____distancedouble_18 = value;
	}

	inline static int32_t get_offset_of__videourl_19() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ____videourl_19)); }
	inline String_t* get__videourl_19() const { return ____videourl_19; }
	inline String_t** get_address_of__videourl_19() { return &____videourl_19; }
	inline void set__videourl_19(String_t* value)
	{
		____videourl_19 = value;
		Il2CppCodeGenWriteBarrier(&____videourl_19, value);
	}

	inline static int32_t get_offset_of__videoname_20() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ____videoname_20)); }
	inline String_t* get__videoname_20() const { return ____videoname_20; }
	inline String_t** get_address_of__videoname_20() { return &____videoname_20; }
	inline void set__videoname_20(String_t* value)
	{
		____videoname_20 = value;
		Il2CppCodeGenWriteBarrier(&____videoname_20, value);
	}

	inline static int32_t get_offset_of_lat_21() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ___lat_21)); }
	inline String_t* get_lat_21() const { return ___lat_21; }
	inline String_t** get_address_of_lat_21() { return &___lat_21; }
	inline void set_lat_21(String_t* value)
	{
		___lat_21 = value;
		Il2CppCodeGenWriteBarrier(&___lat_21, value);
	}

	inline static int32_t get_offset_of_lon_22() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ___lon_22)); }
	inline String_t* get_lon_22() const { return ___lon_22; }
	inline String_t** get_address_of_lon_22() { return &___lon_22; }
	inline void set_lon_22(String_t* value)
	{
		___lon_22 = value;
		Il2CppCodeGenWriteBarrier(&___lon_22, value);
	}

	inline static int32_t get_offset_of__messageid_23() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ____messageid_23)); }
	inline String_t* get__messageid_23() const { return ____messageid_23; }
	inline String_t** get_address_of__messageid_23() { return &____messageid_23; }
	inline void set__messageid_23(String_t* value)
	{
		____messageid_23 = value;
		Il2CppCodeGenWriteBarrier(&____messageid_23, value);
	}

	inline static int32_t get_offset_of_filetype_24() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ___filetype_24)); }
	inline String_t* get_filetype_24() const { return ___filetype_24; }
	inline String_t** get_address_of_filetype_24() { return &___filetype_24; }
	inline void set_filetype_24(String_t* value)
	{
		___filetype_24 = value;
		Il2CppCodeGenWriteBarrier(&___filetype_24, value);
	}

	inline static int32_t get_offset_of_timer_26() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ___timer_26)); }
	inline float get_timer_26() const { return ___timer_26; }
	inline float* get_address_of_timer_26() { return &___timer_26; }
	inline void set_timer_26(float value)
	{
		___timer_26 = value;
	}

	inline static int32_t get_offset_of_waitingTime_27() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ___waitingTime_27)); }
	inline int32_t get_waitingTime_27() const { return ___waitingTime_27; }
	inline int32_t* get_address_of_waitingTime_27() { return &___waitingTime_27; }
	inline void set_waitingTime_27(int32_t value)
	{
		___waitingTime_27 = value;
	}

	inline static int32_t get_offset_of_filenamecategory_28() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ___filenamecategory_28)); }
	inline String_t* get_filenamecategory_28() const { return ___filenamecategory_28; }
	inline String_t** get_address_of_filenamecategory_28() { return &___filenamecategory_28; }
	inline void set_filenamecategory_28(String_t* value)
	{
		___filenamecategory_28 = value;
		Il2CppCodeGenWriteBarrier(&___filenamecategory_28, value);
	}

	inline static int32_t get_offset_of_category_29() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849, ___category_29)); }
	inline int32_t get_category_29() const { return ___category_29; }
	inline int32_t* get_address_of_category_29() { return &___category_29; }
	inline void set_category_29(int32_t value)
	{
		___category_29 = value;
	}
};

struct MessageCheck_t3146986849_StaticFields
{
public:
	// Messages[] MessageCheck::m_Messages
	MessagesU5BU5D_t3583122277* ___m_Messages_6;
	// System.Int32 MessageCheck::_index
	int32_t ____index_25;

public:
	inline static int32_t get_offset_of_m_Messages_6() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849_StaticFields, ___m_Messages_6)); }
	inline MessagesU5BU5D_t3583122277* get_m_Messages_6() const { return ___m_Messages_6; }
	inline MessagesU5BU5D_t3583122277** get_address_of_m_Messages_6() { return &___m_Messages_6; }
	inline void set_m_Messages_6(MessagesU5BU5D_t3583122277* value)
	{
		___m_Messages_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_Messages_6, value);
	}

	inline static int32_t get_offset_of__index_25() { return static_cast<int32_t>(offsetof(MessageCheck_t3146986849_StaticFields, ____index_25)); }
	inline int32_t get__index_25() const { return ____index_25; }
	inline int32_t* get_address_of__index_25() { return &____index_25; }
	inline void set__index_25(int32_t value)
	{
		____index_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
