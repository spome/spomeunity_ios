﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu3528606100MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Action`2<System.Boolean,System.Object>>::.ctor(System.Collections.Generic.LinkedList`1<T>)
#define Enumerator__ctor_m3297857611(__this, ___parent0, method) ((  void (*) (Enumerator_t1703533337 *, LinkedList_1_t220378777 *, const MethodInfo*))Enumerator__ctor_m857368315_gshared)(__this, ___parent0, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Action`2<System.Boolean,System.Object>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4026342956(__this, method) ((  Il2CppObject * (*) (Enumerator_t1703533337 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1753810300_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Action`2<System.Boolean,System.Object>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3957110720(__this, method) ((  void (*) (Enumerator_t1703533337 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4062113552_gshared)(__this, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Action`2<System.Boolean,System.Object>>::get_Current()
#define Enumerator_get_Current_m3094385575(__this, method) ((  Action_2_t2345743608 * (*) (Enumerator_t1703533337 *, const MethodInfo*))Enumerator_get_Current_m1124073047_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Action`2<System.Boolean,System.Object>>::MoveNext()
#define Enumerator_MoveNext_m961314840(__this, method) ((  bool (*) (Enumerator_t1703533337 *, const MethodInfo*))Enumerator_MoveNext_m2358966120_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Action`2<System.Boolean,System.Object>>::Dispose()
#define Enumerator_Dispose_m1230361527(__this, method) ((  void (*) (Enumerator_t1703533337 *, const MethodInfo*))Enumerator_Dispose_m272587367_gshared)(__this, method)
