﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t3950887807;
// UnityEngine.Canvas
struct Canvas_t2727140764;

#include "AssemblyU2DCSharp_TMPro_TMP_Text980027659.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_TMPro_WordWrapState4047764895.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextMeshProUGUI
struct  TextMeshProUGUI_t3603375195  : public TMP_Text_t980027659
{
public:
	// UnityEngine.Vector2 TMPro.TextMeshProUGUI::m_uvOffset
	Vector2_t4282066565  ___m_uvOffset_185;
	// System.Single TMPro.TextMeshProUGUI::m_uvLineOffset
	float ___m_uvLineOffset_186;
	// System.Boolean TMPro.TextMeshProUGUI::m_hasFontAssetChanged
	bool ___m_hasFontAssetChanged_187;
	// UnityEngine.Vector3 TMPro.TextMeshProUGUI::m_previousLossyScale
	Vector3_t4282066566  ___m_previousLossyScale_188;
	// UnityEngine.Vector3[] TMPro.TextMeshProUGUI::m_RectTransformCorners
	Vector3U5BU5D_t215400611* ___m_RectTransformCorners_189;
	// UnityEngine.CanvasRenderer TMPro.TextMeshProUGUI::m_uiRenderer
	CanvasRenderer_t3950887807 * ___m_uiRenderer_190;
	// UnityEngine.Canvas TMPro.TextMeshProUGUI::m_canvas
	Canvas_t2727140764 * ___m_canvas_191;
	// System.Boolean TMPro.TextMeshProUGUI::m_isFirstAllocation
	bool ___m_isFirstAllocation_192;
	// System.Int32 TMPro.TextMeshProUGUI::m_max_characters
	int32_t ___m_max_characters_193;
	// TMPro.WordWrapState TMPro.TextMeshProUGUI::m_SavedWordWrapState
	WordWrapState_t4047764895  ___m_SavedWordWrapState_194;
	// TMPro.WordWrapState TMPro.TextMeshProUGUI::m_SavedLineState
	WordWrapState_t4047764895  ___m_SavedLineState_195;
	// System.Boolean TMPro.TextMeshProUGUI::m_isMaskingEnabled
	bool ___m_isMaskingEnabled_196;
	// System.Boolean TMPro.TextMeshProUGUI::m_isScrollRegionSet
	bool ___m_isScrollRegionSet_197;
	// System.Int32 TMPro.TextMeshProUGUI::m_stencilID
	int32_t ___m_stencilID_198;
	// UnityEngine.Vector4 TMPro.TextMeshProUGUI::m_maskOffset
	Vector4_t4282066567  ___m_maskOffset_199;
	// UnityEngine.Matrix4x4 TMPro.TextMeshProUGUI::m_EnvMapMatrix
	Matrix4x4_t1651859333  ___m_EnvMapMatrix_200;
	// System.Boolean TMPro.TextMeshProUGUI::m_isAwake
	bool ___m_isAwake_201;
	// System.Boolean TMPro.TextMeshProUGUI::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_202;
	// System.Int32 TMPro.TextMeshProUGUI::m_recursiveCount
	int32_t ___m_recursiveCount_203;
	// System.Int32 TMPro.TextMeshProUGUI::m_recursiveCountA
	int32_t ___m_recursiveCountA_204;
	// System.Int32 TMPro.TextMeshProUGUI::loopCountA
	int32_t ___loopCountA_205;
	// System.Boolean TMPro.TextMeshProUGUI::m_isRebuildingLayout
	bool ___m_isRebuildingLayout_206;

public:
	inline static int32_t get_offset_of_m_uvOffset_185() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_uvOffset_185)); }
	inline Vector2_t4282066565  get_m_uvOffset_185() const { return ___m_uvOffset_185; }
	inline Vector2_t4282066565 * get_address_of_m_uvOffset_185() { return &___m_uvOffset_185; }
	inline void set_m_uvOffset_185(Vector2_t4282066565  value)
	{
		___m_uvOffset_185 = value;
	}

	inline static int32_t get_offset_of_m_uvLineOffset_186() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_uvLineOffset_186)); }
	inline float get_m_uvLineOffset_186() const { return ___m_uvLineOffset_186; }
	inline float* get_address_of_m_uvLineOffset_186() { return &___m_uvLineOffset_186; }
	inline void set_m_uvLineOffset_186(float value)
	{
		___m_uvLineOffset_186 = value;
	}

	inline static int32_t get_offset_of_m_hasFontAssetChanged_187() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_hasFontAssetChanged_187)); }
	inline bool get_m_hasFontAssetChanged_187() const { return ___m_hasFontAssetChanged_187; }
	inline bool* get_address_of_m_hasFontAssetChanged_187() { return &___m_hasFontAssetChanged_187; }
	inline void set_m_hasFontAssetChanged_187(bool value)
	{
		___m_hasFontAssetChanged_187 = value;
	}

	inline static int32_t get_offset_of_m_previousLossyScale_188() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_previousLossyScale_188)); }
	inline Vector3_t4282066566  get_m_previousLossyScale_188() const { return ___m_previousLossyScale_188; }
	inline Vector3_t4282066566 * get_address_of_m_previousLossyScale_188() { return &___m_previousLossyScale_188; }
	inline void set_m_previousLossyScale_188(Vector3_t4282066566  value)
	{
		___m_previousLossyScale_188 = value;
	}

	inline static int32_t get_offset_of_m_RectTransformCorners_189() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_RectTransformCorners_189)); }
	inline Vector3U5BU5D_t215400611* get_m_RectTransformCorners_189() const { return ___m_RectTransformCorners_189; }
	inline Vector3U5BU5D_t215400611** get_address_of_m_RectTransformCorners_189() { return &___m_RectTransformCorners_189; }
	inline void set_m_RectTransformCorners_189(Vector3U5BU5D_t215400611* value)
	{
		___m_RectTransformCorners_189 = value;
		Il2CppCodeGenWriteBarrier(&___m_RectTransformCorners_189, value);
	}

	inline static int32_t get_offset_of_m_uiRenderer_190() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_uiRenderer_190)); }
	inline CanvasRenderer_t3950887807 * get_m_uiRenderer_190() const { return ___m_uiRenderer_190; }
	inline CanvasRenderer_t3950887807 ** get_address_of_m_uiRenderer_190() { return &___m_uiRenderer_190; }
	inline void set_m_uiRenderer_190(CanvasRenderer_t3950887807 * value)
	{
		___m_uiRenderer_190 = value;
		Il2CppCodeGenWriteBarrier(&___m_uiRenderer_190, value);
	}

	inline static int32_t get_offset_of_m_canvas_191() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_canvas_191)); }
	inline Canvas_t2727140764 * get_m_canvas_191() const { return ___m_canvas_191; }
	inline Canvas_t2727140764 ** get_address_of_m_canvas_191() { return &___m_canvas_191; }
	inline void set_m_canvas_191(Canvas_t2727140764 * value)
	{
		___m_canvas_191 = value;
		Il2CppCodeGenWriteBarrier(&___m_canvas_191, value);
	}

	inline static int32_t get_offset_of_m_isFirstAllocation_192() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_isFirstAllocation_192)); }
	inline bool get_m_isFirstAllocation_192() const { return ___m_isFirstAllocation_192; }
	inline bool* get_address_of_m_isFirstAllocation_192() { return &___m_isFirstAllocation_192; }
	inline void set_m_isFirstAllocation_192(bool value)
	{
		___m_isFirstAllocation_192 = value;
	}

	inline static int32_t get_offset_of_m_max_characters_193() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_max_characters_193)); }
	inline int32_t get_m_max_characters_193() const { return ___m_max_characters_193; }
	inline int32_t* get_address_of_m_max_characters_193() { return &___m_max_characters_193; }
	inline void set_m_max_characters_193(int32_t value)
	{
		___m_max_characters_193 = value;
	}

	inline static int32_t get_offset_of_m_SavedWordWrapState_194() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_SavedWordWrapState_194)); }
	inline WordWrapState_t4047764895  get_m_SavedWordWrapState_194() const { return ___m_SavedWordWrapState_194; }
	inline WordWrapState_t4047764895 * get_address_of_m_SavedWordWrapState_194() { return &___m_SavedWordWrapState_194; }
	inline void set_m_SavedWordWrapState_194(WordWrapState_t4047764895  value)
	{
		___m_SavedWordWrapState_194 = value;
	}

	inline static int32_t get_offset_of_m_SavedLineState_195() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_SavedLineState_195)); }
	inline WordWrapState_t4047764895  get_m_SavedLineState_195() const { return ___m_SavedLineState_195; }
	inline WordWrapState_t4047764895 * get_address_of_m_SavedLineState_195() { return &___m_SavedLineState_195; }
	inline void set_m_SavedLineState_195(WordWrapState_t4047764895  value)
	{
		___m_SavedLineState_195 = value;
	}

	inline static int32_t get_offset_of_m_isMaskingEnabled_196() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_isMaskingEnabled_196)); }
	inline bool get_m_isMaskingEnabled_196() const { return ___m_isMaskingEnabled_196; }
	inline bool* get_address_of_m_isMaskingEnabled_196() { return &___m_isMaskingEnabled_196; }
	inline void set_m_isMaskingEnabled_196(bool value)
	{
		___m_isMaskingEnabled_196 = value;
	}

	inline static int32_t get_offset_of_m_isScrollRegionSet_197() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_isScrollRegionSet_197)); }
	inline bool get_m_isScrollRegionSet_197() const { return ___m_isScrollRegionSet_197; }
	inline bool* get_address_of_m_isScrollRegionSet_197() { return &___m_isScrollRegionSet_197; }
	inline void set_m_isScrollRegionSet_197(bool value)
	{
		___m_isScrollRegionSet_197 = value;
	}

	inline static int32_t get_offset_of_m_stencilID_198() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_stencilID_198)); }
	inline int32_t get_m_stencilID_198() const { return ___m_stencilID_198; }
	inline int32_t* get_address_of_m_stencilID_198() { return &___m_stencilID_198; }
	inline void set_m_stencilID_198(int32_t value)
	{
		___m_stencilID_198 = value;
	}

	inline static int32_t get_offset_of_m_maskOffset_199() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_maskOffset_199)); }
	inline Vector4_t4282066567  get_m_maskOffset_199() const { return ___m_maskOffset_199; }
	inline Vector4_t4282066567 * get_address_of_m_maskOffset_199() { return &___m_maskOffset_199; }
	inline void set_m_maskOffset_199(Vector4_t4282066567  value)
	{
		___m_maskOffset_199 = value;
	}

	inline static int32_t get_offset_of_m_EnvMapMatrix_200() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_EnvMapMatrix_200)); }
	inline Matrix4x4_t1651859333  get_m_EnvMapMatrix_200() const { return ___m_EnvMapMatrix_200; }
	inline Matrix4x4_t1651859333 * get_address_of_m_EnvMapMatrix_200() { return &___m_EnvMapMatrix_200; }
	inline void set_m_EnvMapMatrix_200(Matrix4x4_t1651859333  value)
	{
		___m_EnvMapMatrix_200 = value;
	}

	inline static int32_t get_offset_of_m_isAwake_201() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_isAwake_201)); }
	inline bool get_m_isAwake_201() const { return ___m_isAwake_201; }
	inline bool* get_address_of_m_isAwake_201() { return &___m_isAwake_201; }
	inline void set_m_isAwake_201(bool value)
	{
		___m_isAwake_201 = value;
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_202() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_isRegisteredForEvents_202)); }
	inline bool get_m_isRegisteredForEvents_202() const { return ___m_isRegisteredForEvents_202; }
	inline bool* get_address_of_m_isRegisteredForEvents_202() { return &___m_isRegisteredForEvents_202; }
	inline void set_m_isRegisteredForEvents_202(bool value)
	{
		___m_isRegisteredForEvents_202 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCount_203() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_recursiveCount_203)); }
	inline int32_t get_m_recursiveCount_203() const { return ___m_recursiveCount_203; }
	inline int32_t* get_address_of_m_recursiveCount_203() { return &___m_recursiveCount_203; }
	inline void set_m_recursiveCount_203(int32_t value)
	{
		___m_recursiveCount_203 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCountA_204() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_recursiveCountA_204)); }
	inline int32_t get_m_recursiveCountA_204() const { return ___m_recursiveCountA_204; }
	inline int32_t* get_address_of_m_recursiveCountA_204() { return &___m_recursiveCountA_204; }
	inline void set_m_recursiveCountA_204(int32_t value)
	{
		___m_recursiveCountA_204 = value;
	}

	inline static int32_t get_offset_of_loopCountA_205() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___loopCountA_205)); }
	inline int32_t get_loopCountA_205() const { return ___loopCountA_205; }
	inline int32_t* get_address_of_loopCountA_205() { return &___loopCountA_205; }
	inline void set_loopCountA_205(int32_t value)
	{
		___loopCountA_205 = value;
	}

	inline static int32_t get_offset_of_m_isRebuildingLayout_206() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t3603375195, ___m_isRebuildingLayout_206)); }
	inline bool get_m_isRebuildingLayout_206() const { return ___m_isRebuildingLayout_206; }
	inline bool* get_address_of_m_isRebuildingLayout_206() { return &___m_isRebuildingLayout_206; }
	inline void set_m_isRebuildingLayout_206(bool value)
	{
		___m_isRebuildingLayout_206 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
