﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En114091345MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CurrentState/States,UnityEngine.GameObject>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1262223178(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3912924275 *, Dictionary_2_t2595600883 *, const MethodInfo*))Enumerator__ctor_m1705238236_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<CurrentState/States,UnityEngine.GameObject>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3309437409(__this, method) ((  Il2CppObject * (*) (Enumerator_t3912924275 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2572094213_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CurrentState/States,UnityEngine.GameObject>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1735481131(__this, method) ((  void (*) (Enumerator_t3912924275 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m866689113_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<CurrentState/States,UnityEngine.GameObject>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1401911458(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3912924275 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2153128034_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<CurrentState/States,UnityEngine.GameObject>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2112262653(__this, method) ((  Il2CppObject * (*) (Enumerator_t3912924275 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3702075425_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<CurrentState/States,UnityEngine.GameObject>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3708282191(__this, method) ((  Il2CppObject * (*) (Enumerator_t3912924275 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2509998707_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<CurrentState/States,UnityEngine.GameObject>::MoveNext()
#define Enumerator_MoveNext_m2736686939(__this, method) ((  bool (*) (Enumerator_t3912924275 *, const MethodInfo*))Enumerator_MoveNext_m3734269509_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<CurrentState/States,UnityEngine.GameObject>::get_Current()
#define Enumerator_get_Current_m2997822337(__this, method) ((  KeyValuePair_2_t2494381589  (*) (Enumerator_t3912924275 *, const MethodInfo*))Enumerator_get_Current_m3086517835_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<CurrentState/States,UnityEngine.GameObject>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2846982564(__this, method) ((  int32_t (*) (Enumerator_t3912924275 *, const MethodInfo*))Enumerator_get_CurrentKey_m884111570_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<CurrentState/States,UnityEngine.GameObject>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2158107364(__this, method) ((  GameObject_t3674682005 * (*) (Enumerator_t3912924275 *, const MethodInfo*))Enumerator_get_CurrentValue_m3774438262_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CurrentState/States,UnityEngine.GameObject>::Reset()
#define Enumerator_Reset_m3117501084(__this, method) ((  void (*) (Enumerator_t3912924275 *, const MethodInfo*))Enumerator_Reset_m1527377710_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CurrentState/States,UnityEngine.GameObject>::VerifyState()
#define Enumerator_VerifyState_m1418802597(__this, method) ((  void (*) (Enumerator_t3912924275 *, const MethodInfo*))Enumerator_VerifyState_m3574375607_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CurrentState/States,UnityEngine.GameObject>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m414567373(__this, method) ((  void (*) (Enumerator_t3912924275 *, const MethodInfo*))Enumerator_VerifyCurrent_m1745993311_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CurrentState/States,UnityEngine.GameObject>::Dispose()
#define Enumerator_Dispose_m554798188(__this, method) ((  void (*) (Enumerator_t3912924275 *, const MethodInfo*))Enumerator_Dispose_m1454593150_gshared)(__this, method)
