﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.TMP_Glyph
struct TMP_Glyph_t1999257094;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_TMP_Glyph1999257094.h"

// System.Void TMPro.TMP_Glyph::.ctor()
extern "C"  void TMP_Glyph__ctor_m3202589409 (TMP_Glyph_t1999257094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TMP_Glyph TMPro.TMP_Glyph::Clone(TMPro.TMP_Glyph)
extern "C"  TMP_Glyph_t1999257094 * TMP_Glyph_Clone_m2199464867 (Il2CppObject * __this /* static, unused */, TMP_Glyph_t1999257094 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
