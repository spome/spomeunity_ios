﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.InlineGraphic
struct InlineGraphic_t3606901041;
// UnityEngine.Texture
struct Texture_t2526458961;

#include "codegen/il2cpp-codegen.h"

// System.Void TMPro.InlineGraphic::.ctor()
extern "C"  void InlineGraphic__ctor_m1035377686 (InlineGraphic_t3606901041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture TMPro.InlineGraphic::get_mainTexture()
extern "C"  Texture_t2526458961 * InlineGraphic_get_mainTexture_m2725741276 (InlineGraphic_t3606901041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.InlineGraphic::Awake()
extern "C"  void InlineGraphic_Awake_m1272982905 (InlineGraphic_t3606901041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.InlineGraphic::OnEnable()
extern "C"  void InlineGraphic_OnEnable_m553102256 (InlineGraphic_t3606901041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.InlineGraphic::OnDisable()
extern "C"  void InlineGraphic_OnDisable_m407238013 (InlineGraphic_t3606901041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.InlineGraphic::OnTransformParentChanged()
extern "C"  void InlineGraphic_OnTransformParentChanged_m3639723531 (InlineGraphic_t3606901041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.InlineGraphic::OnRectTransformDimensionsChange()
extern "C"  void InlineGraphic_OnRectTransformDimensionsChange_m514316346 (InlineGraphic_t3606901041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.InlineGraphic::UpdateMaterial()
extern "C"  void InlineGraphic_UpdateMaterial_m505296702 (InlineGraphic_t3606901041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.InlineGraphic::UpdateGeometry()
extern "C"  void InlineGraphic_UpdateGeometry_m1068970953 (InlineGraphic_t3606901041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
