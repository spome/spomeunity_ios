﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.TMP_Text
struct TMP_Text_t980027659;
// System.String
struct String_t;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t967550395;
// UnityEngine.Material
struct Material_t3870600107;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t270066265;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_TMPro_TMP_FontAsset967550395.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_TMPro_VertexGradient2166651658.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "AssemblyU2DCSharp_TMPro_FontStyles3228051751.h"
#include "AssemblyU2DCSharp_TMPro_TextAlignmentOptions3798547742.h"
#include "AssemblyU2DCSharp_TMPro_TextOverflowModes832755779.h"
#include "AssemblyU2DCSharp_TMPro_TextureMappingOptions707307789.h"
#include "AssemblyU2DCSharp_TMPro_TextRenderFlags2400171206.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_TMPro_WordWrapState4047764895.h"

// System.Void TMPro.TMP_Text::.ctor()
extern "C"  void TMP_Text__ctor_m3365237420 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::.cctor()
extern "C"  void TMP_Text__cctor_m761048705 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TMPro.TMP_Text::get_text()
extern "C"  String_t* TMP_Text_get_text_m179062993 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_text(System.String)
extern "C"  void TMP_Text_set_text_m27929696 (TMP_Text_t980027659 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TMP_FontAsset TMPro.TMP_Text::get_font()
extern "C"  TMP_FontAsset_t967550395 * TMP_Text_get_font_m3491951364 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_font(TMPro.TMP_FontAsset)
extern "C"  void TMP_Text_set_font_m1109515343 (TMP_Text_t980027659 * __this, TMP_FontAsset_t967550395 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material TMPro.TMP_Text::get_fontSharedMaterial()
extern "C"  Material_t3870600107 * TMP_Text_get_fontSharedMaterial_m4212520251 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_fontSharedMaterial(UnityEngine.Material)
extern "C"  void TMP_Text_set_fontSharedMaterial_m3635538930 (TMP_Text_t980027659 * __this, Material_t3870600107 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material TMPro.TMP_Text::get_fontMaterial()
extern "C"  Material_t3870600107 * TMP_Text_get_fontMaterial_m3328862582 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_fontMaterial(UnityEngine.Material)
extern "C"  void TMP_Text_set_fontMaterial_m4033108077 (TMP_Text_t980027659 * __this, Material_t3870600107 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material TMPro.TMP_Text::get_fontBaseMaterial()
extern "C"  Material_t3870600107 * TMP_Text_get_fontBaseMaterial_m1744274471 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_fontBaseMaterial(UnityEngine.Material)
extern "C"  void TMP_Text_set_fontBaseMaterial_m3818429982 (TMP_Text_t980027659 * __this, Material_t3870600107 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color TMPro.TMP_Text::get_color()
extern "C"  Color_t4194546905  TMP_Text_get_color_m2185315709 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_color(UnityEngine.Color)
extern "C"  void TMP_Text_set_color_m3920390390 (TMP_Text_t980027659 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::get_alpha()
extern "C"  float TMP_Text_get_alpha_m1651652883 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_alpha(System.Single)
extern "C"  void TMP_Text_set_alpha_m2406527264 (TMP_Text_t980027659 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_Text::get_tintAllSprites()
extern "C"  bool TMP_Text_get_tintAllSprites_m2188756989 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_tintAllSprites(System.Boolean)
extern "C"  void TMP_Text_set_tintAllSprites_m2652771956 (TMP_Text_t980027659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_Text::get_enableVertexGradient()
extern "C"  bool TMP_Text_get_enableVertexGradient_m3838976972 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_enableVertexGradient(System.Boolean)
extern "C"  void TMP_Text_set_enableVertexGradient_m3998829571 (TMP_Text_t980027659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.VertexGradient TMPro.TMP_Text::get_colorGradient()
extern "C"  VertexGradient_t2166651658  TMP_Text_get_colorGradient_m3830739429 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_colorGradient(TMPro.VertexGradient)
extern "C"  void TMP_Text_set_colorGradient_m1084986098 (TMP_Text_t980027659 * __this, VertexGradient_t2166651658  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_Text::get_overrideColorTags()
extern "C"  bool TMP_Text_get_overrideColorTags_m4101692925 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_overrideColorTags(System.Boolean)
extern "C"  void TMP_Text_set_overrideColorTags_m4057261300 (TMP_Text_t980027659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32 TMPro.TMP_Text::get_faceColor()
extern "C"  Color32_t598853688  TMP_Text_get_faceColor_m1047677439 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_faceColor(UnityEngine.Color32)
extern "C"  void TMP_Text_set_faceColor_m1590664564 (TMP_Text_t980027659 * __this, Color32_t598853688  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32 TMPro.TMP_Text::get_outlineColor()
extern "C"  Color32_t598853688  TMP_Text_get_outlineColor_m3702497930 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_outlineColor(UnityEngine.Color32)
extern "C"  void TMP_Text_set_outlineColor_m3654201521 (TMP_Text_t980027659 * __this, Color32_t598853688  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::get_outlineWidth()
extern "C"  float TMP_Text_get_outlineWidth_m2583852529 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_outlineWidth(System.Single)
extern "C"  void TMP_Text_set_outlineWidth_m2869198002 (TMP_Text_t980027659 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::get_fontSize()
extern "C"  float TMP_Text_get_fontSize_m1666919229 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_fontSize(System.Single)
extern "C"  void TMP_Text_set_fontSize_m3719835366 (TMP_Text_t980027659 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::get_fontScale()
extern "C"  float TMP_Text_get_fontScale_m4235260080 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_Text::get_enableAutoSizing()
extern "C"  bool TMP_Text_get_enableAutoSizing_m4023084837 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_enableAutoSizing(System.Boolean)
extern "C"  void TMP_Text_set_enableAutoSizing_m115548060 (TMP_Text_t980027659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::get_fontSizeMin()
extern "C"  float TMP_Text_get_fontSizeMin_m815058679 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_fontSizeMin(System.Single)
extern "C"  void TMP_Text_set_fontSizeMin_m3632656316 (TMP_Text_t980027659 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::get_fontSizeMax()
extern "C"  float TMP_Text_get_fontSizeMax_m814829961 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_fontSizeMax(System.Single)
extern "C"  void TMP_Text_set_fontSizeMax_m585738858 (TMP_Text_t980027659 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.FontStyles TMPro.TMP_Text::get_fontStyle()
extern "C"  int32_t TMP_Text_get_fontStyle_m3385652529 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_fontStyle(TMPro.FontStyles)
extern "C"  void TMP_Text_set_fontStyle_m3603011364 (TMP_Text_t980027659 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TextAlignmentOptions TMPro.TMP_Text::get_alignment()
extern "C"  int32_t TMP_Text_get_alignment_m3706303081 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_alignment(TMPro.TextAlignmentOptions)
extern "C"  void TMP_Text_set_alignment_m2540387918 (TMP_Text_t980027659 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::get_characterSpacing()
extern "C"  float TMP_Text_get_characterSpacing_m3000614055 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_characterSpacing(System.Single)
extern "C"  void TMP_Text_set_characterSpacing_m1103880636 (TMP_Text_t980027659 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::get_lineSpacing()
extern "C"  float TMP_Text_get_lineSpacing_m4093571620 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_lineSpacing(System.Single)
extern "C"  void TMP_Text_set_lineSpacing_m2011766191 (TMP_Text_t980027659 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::get_paragraphSpacing()
extern "C"  float TMP_Text_get_paragraphSpacing_m174124034 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_paragraphSpacing(System.Single)
extern "C"  void TMP_Text_set_paragraphSpacing_m2675949121 (TMP_Text_t980027659 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::get_characterWidthAdjustment()
extern "C"  float TMP_Text_get_characterWidthAdjustment_m1124817207 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_characterWidthAdjustment(System.Single)
extern "C"  void TMP_Text_set_characterWidthAdjustment_m1190811436 (TMP_Text_t980027659 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_Text::get_enableWordWrapping()
extern "C"  bool TMP_Text_get_enableWordWrapping_m3755404798 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_enableWordWrapping(System.Boolean)
extern "C"  void TMP_Text_set_enableWordWrapping_m2569946805 (TMP_Text_t980027659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::get_wordWrappingRatios()
extern "C"  float TMP_Text_get_wordWrappingRatios_m2063921947 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_wordWrappingRatios(System.Single)
extern "C"  void TMP_Text_set_wordWrappingRatios_m3392655816 (TMP_Text_t980027659 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TextOverflowModes TMPro.TMP_Text::get_OverflowMode()
extern "C"  int32_t TMP_Text_get_OverflowMode_m2598241554 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_OverflowMode(TMPro.TextOverflowModes)
extern "C"  void TMP_Text_set_OverflowMode_m34087025 (TMP_Text_t980027659 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_Text::get_enableKerning()
extern "C"  bool TMP_Text_get_enableKerning_m3040726550 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_enableKerning(System.Boolean)
extern "C"  void TMP_Text_set_enableKerning_m590566733 (TMP_Text_t980027659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_Text::get_extraPadding()
extern "C"  bool TMP_Text_get_extraPadding_m4113134038 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_extraPadding(System.Boolean)
extern "C"  void TMP_Text_set_extraPadding_m4123394189 (TMP_Text_t980027659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_Text::get_richText()
extern "C"  bool TMP_Text_get_richText_m1593999390 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_richText(System.Boolean)
extern "C"  void TMP_Text_set_richText_m3363721429 (TMP_Text_t980027659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_Text::get_parseCtrlCharacters()
extern "C"  bool TMP_Text_get_parseCtrlCharacters_m2082344469 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_parseCtrlCharacters(System.Boolean)
extern "C"  void TMP_Text_set_parseCtrlCharacters_m7535372 (TMP_Text_t980027659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_Text::get_isOverlay()
extern "C"  bool TMP_Text_get_isOverlay_m2179059859 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_isOverlay(System.Boolean)
extern "C"  void TMP_Text_set_isOverlay_m670095626 (TMP_Text_t980027659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_Text::get_isOrthographic()
extern "C"  bool TMP_Text_get_isOrthographic_m464185135 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_isOrthographic(System.Boolean)
extern "C"  void TMP_Text_set_isOrthographic_m2504767014 (TMP_Text_t980027659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_Text::get_enableCulling()
extern "C"  bool TMP_Text_get_enableCulling_m1348341562 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_enableCulling(System.Boolean)
extern "C"  void TMP_Text_set_enableCulling_m522766193 (TMP_Text_t980027659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_Text::get_ignoreVisibility()
extern "C"  bool TMP_Text_get_ignoreVisibility_m1416899513 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_ignoreVisibility(System.Boolean)
extern "C"  void TMP_Text_set_ignoreVisibility_m364794160 (TMP_Text_t980027659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TextureMappingOptions TMPro.TMP_Text::get_horizontalMapping()
extern "C"  int32_t TMP_Text_get_horizontalMapping_m3425441257 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_horizontalMapping(TMPro.TextureMappingOptions)
extern "C"  void TMP_Text_set_horizontalMapping_m1122760394 (TMP_Text_t980027659 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TextureMappingOptions TMPro.TMP_Text::get_verticalMapping()
extern "C"  int32_t TMP_Text_get_verticalMapping_m1680086615 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_verticalMapping(TMPro.TextureMappingOptions)
extern "C"  void TMP_Text_set_verticalMapping_m1663385116 (TMP_Text_t980027659 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TextRenderFlags TMPro.TMP_Text::get_renderMode()
extern "C"  int32_t TMP_Text_get_renderMode_m4132187427 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_renderMode(TMPro.TextRenderFlags)
extern "C"  void TMP_Text_set_renderMode_m763684122 (TMP_Text_t980027659 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TMP_Text::get_maxVisibleCharacters()
extern "C"  int32_t TMP_Text_get_maxVisibleCharacters_m39086215 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_maxVisibleCharacters(System.Int32)
extern "C"  void TMP_Text_set_maxVisibleCharacters_m4140537662 (TMP_Text_t980027659 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TMP_Text::get_maxVisibleWords()
extern "C"  int32_t TMP_Text_get_maxVisibleWords_m815635534 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_maxVisibleWords(System.Int32)
extern "C"  void TMP_Text_set_maxVisibleWords_m1542842617 (TMP_Text_t980027659 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TMP_Text::get_maxVisibleLines()
extern "C"  int32_t TMP_Text_get_maxVisibleLines_m3762557732 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_maxVisibleLines(System.Int32)
extern "C"  void TMP_Text_set_maxVisibleLines_m2300759759 (TMP_Text_t980027659 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TMP_Text::get_pageToDisplay()
extern "C"  int32_t TMP_Text_get_pageToDisplay_m530241707 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_pageToDisplay(System.Int32)
extern "C"  void TMP_Text_set_pageToDisplay_m3772895318 (TMP_Text_t980027659 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 TMPro.TMP_Text::get_margin()
extern "C"  Vector4_t4282066567  TMP_Text_get_margin_m3268644552 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_margin(UnityEngine.Vector4)
extern "C"  void TMP_Text_set_margin_m42309109 (TMP_Text_t980027659 * __this, Vector4_t4282066567  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TMP_TextInfo TMPro.TMP_Text::get_textInfo()
extern "C"  TMP_TextInfo_t270066265 * TMP_Text_get_textInfo_m3248489788 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_Text::get_havePropertiesChanged()
extern "C"  bool TMP_Text_get_havePropertiesChanged_m3456846726 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_havePropertiesChanged(System.Boolean)
extern "C"  void TMP_Text_set_havePropertiesChanged_m1980518077 (TMP_Text_t980027659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform TMPro.TMP_Text::get_transform()
extern "C"  Transform_t1659122786 * TMP_Text_get_transform_m3331481551 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform TMPro.TMP_Text::get_rectTransform()
extern "C"  RectTransform_t972643934 * TMP_Text_get_rectTransform_m2810505159 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_Text::get_autoSizeTextContainer()
extern "C"  bool TMP_Text_get_autoSizeTextContainer_m1197743281 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_autoSizeTextContainer(System.Boolean)
extern "C"  void TMP_Text_set_autoSizeTextContainer_m4009638056 (TMP_Text_t980027659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh TMPro.TMP_Text::get_mesh()
extern "C"  Mesh_t4241756145 * TMP_Text_get_mesh_m1499367559 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::get_flexibleHeight()
extern "C"  float TMP_Text_get_flexibleHeight_m2344402335 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::get_flexibleWidth()
extern "C"  float TMP_Text_get_flexibleWidth_m2276131120 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::get_minHeight()
extern "C"  float TMP_Text_get_minHeight_m1023781134 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::get_minWidth()
extern "C"  float TMP_Text_get_minWidth_m3203361761 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::get_preferredWidth()
extern "C"  float TMP_Text_get_preferredWidth_m310335954 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::get_preferredHeight()
extern "C"  float TMP_Text_get_preferredHeight_m1534294333 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TMP_Text::get_layoutPriority()
extern "C"  int32_t TMP_Text_get_layoutPriority_m3613455613 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::LoadFontAsset()
extern "C"  void TMP_Text_LoadFontAsset_m4219390373 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::SetFontSharedMaterial(UnityEngine.Material)
extern "C"  void TMP_Text_SetFontSharedMaterial_m129647305 (TMP_Text_t980027659 * __this, Material_t3870600107 * ___mat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::SetFontMaterial(UnityEngine.Material)
extern "C"  void TMP_Text_SetFontMaterial_m1188060292 (TMP_Text_t980027659 * __this, Material_t3870600107 * ___mat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::SetFontBaseMaterial(UnityEngine.Material)
extern "C"  void TMP_Text_SetFontBaseMaterial_m1571208885 (TMP_Text_t980027659 * __this, Material_t3870600107 * ___mat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::SetFaceColor(UnityEngine.Color32)
extern "C"  void TMP_Text_SetFaceColor_m4145497355 (TMP_Text_t980027659 * __this, Color32_t598853688  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::SetOutlineColor(UnityEngine.Color32)
extern "C"  void TMP_Text_SetOutlineColor_m3562425786 (TMP_Text_t980027659 * __this, Color32_t598853688  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::SetOutlineThickness(System.Single)
extern "C"  void TMP_Text_SetOutlineThickness_m2188215693 (TMP_Text_t980027659 * __this, float ___thickness0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::SetShaderDepth()
extern "C"  void TMP_Text_SetShaderDepth_m3293180276 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::SetCulling()
extern "C"  void TMP_Text_SetCulling_m2186392262 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::GetPaddingForMaterial()
extern "C"  float TMP_Text_GetPaddingForMaterial_m1942718579 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::GetPaddingForMaterial(UnityEngine.Material)
extern "C"  float TMP_Text_GetPaddingForMaterial_m2028479445 (TMP_Text_t980027659 * __this, Material_t3870600107 * ___mat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] TMPro.TMP_Text::GetTextContainerLocalCorners()
extern "C"  Vector3U5BU5D_t215400611* TMP_Text_GetTextContainerLocalCorners_m4258014145 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::ForceMeshUpdate()
extern "C"  void TMP_Text_ForceMeshUpdate_m3176656555 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::UpdateMeshPadding()
extern "C"  void TMP_Text_UpdateMeshPadding_m1605441637 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::SetText(System.String)
extern "C"  void TMP_Text_SetText_m331308329 (TMP_Text_t980027659 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::SetText(System.String,System.Single)
extern "C"  void TMP_Text_SetText_m817310798 (TMP_Text_t980027659 * __this, String_t* ___text0, float ___arg01, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::SetText(System.String,System.Single,System.Single)
extern "C"  void TMP_Text_SetText_m957290163 (TMP_Text_t980027659 * __this, String_t* ___text0, float ___arg01, float ___arg12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::SetText(System.String,System.Single,System.Single,System.Single)
extern "C"  void TMP_Text_SetText_m1607887960 (TMP_Text_t980027659 * __this, String_t* ___text0, float ___arg01, float ___arg12, float ___arg23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::SetCharArray(System.Char[])
extern "C"  void TMP_Text_SetCharArray_m2825520930 (TMP_Text_t980027659 * __this, CharU5BU5D_t3324145743* ___charArray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::SetTextArrayToCharArray(System.Char[],System.Int32[]&)
extern "C"  void TMP_Text_SetTextArrayToCharArray_m3736216266 (TMP_Text_t980027659 * __this, CharU5BU5D_t3324145743* ___charArray0, Int32U5BU5D_t3230847821** ___charBuffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::StringToCharArray(System.String,System.Int32[]&)
extern "C"  void TMP_Text_StringToCharArray_m2727856824 (TMP_Text_t980027659 * __this, String_t* ___text0, Int32U5BU5D_t3230847821** ___chars1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::AddFloatToCharArray(System.Single,System.Int32&,System.Int32)
extern "C"  void TMP_Text_AddFloatToCharArray_m3092432270 (TMP_Text_t980027659 * __this, float ___number0, int32_t* ___index1, int32_t ___precision2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::AddIntToCharArray(System.Int32,System.Int32&,System.Int32)
extern "C"  void TMP_Text_AddIntToCharArray_m2050409709 (TMP_Text_t980027659 * __this, int32_t ___number0, int32_t* ___index1, int32_t ___precision2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TMP_Text::SetArraySizes(System.Int32[])
extern "C"  int32_t TMP_Text_SetArraySizes_m2536315878 (TMP_Text_t980027659 * __this, Int32U5BU5D_t3230847821* ___chars0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::ParseInputText()
extern "C"  void TMP_Text_ParseInputText_m2806556156 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::GenerateTextMesh()
extern "C"  void TMP_Text_GenerateTextMesh_m3145029671 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TMPro.TMP_Text::GetPreferredValues()
extern "C"  Vector2_t4282066565  TMP_Text_GetPreferredValues_m2680694848 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TMPro.TMP_Text::GetPreferredValues(System.Single,System.Single)
extern "C"  Vector2_t4282066565  TMP_Text_GetPreferredValues_m423130384 (TMP_Text_t980027659 * __this, float ___width0, float ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TMPro.TMP_Text::GetPreferredValues(System.String)
extern "C"  Vector2_t4282066565  TMP_Text_GetPreferredValues_m982310594 (TMP_Text_t980027659 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TMPro.TMP_Text::GetPreferredValues(System.String,System.Single,System.Single)
extern "C"  Vector2_t4282066565  TMP_Text_GetPreferredValues_m1296083148 (TMP_Text_t980027659 * __this, String_t* ___text0, float ___width1, float ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::GetPreferredWidth()
extern "C"  float TMP_Text_GetPreferredWidth_m188753401 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::GetPreferredWidth(UnityEngine.Vector2)
extern "C"  float TMP_Text_GetPreferredWidth_m779906337 (TMP_Text_t980027659 * __this, Vector2_t4282066565  ___margin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::GetPreferredHeight()
extern "C"  float TMP_Text_GetPreferredHeight_m2060202486 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::GetPreferredHeight(UnityEngine.Vector2)
extern "C"  float TMP_Text_GetPreferredHeight_m2056740356 (TMP_Text_t980027659 * __this, Vector2_t4282066565  ___margin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TMPro.TMP_Text::CalculatePreferredValues(System.Single,UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  TMP_Text_CalculatePreferredValues_m1113485551 (TMP_Text_t980027659 * __this, float ___defaultFontSize0, Vector2_t4282066565  ___marginSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::AdjustLineOffset(System.Int32,System.Int32,System.Single)
extern "C"  void TMP_Text_AdjustLineOffset_m1121048605 (TMP_Text_t980027659 * __this, int32_t ___startIndex0, int32_t ___endIndex1, float ___offset2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::ResizeLineExtents(System.Int32)
extern "C"  void TMP_Text_ResizeLineExtents_m931504508 (TMP_Text_t980027659 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TMP_TextInfo TMPro.TMP_Text::GetTextInfo(System.String)
extern "C"  TMP_TextInfo_t270066265 * TMP_Text_GetTextInfo_m2866349863 (TMP_Text_t980027659 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::ComputeMarginSize()
extern "C"  void TMP_Text_ComputeMarginSize_m69144368 (TMP_Text_t980027659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TMP_Text::GetArraySizes(System.Int32[])
extern "C"  int32_t TMP_Text_GetArraySizes_m2809030106 (TMP_Text_t980027659 * __this, Int32U5BU5D_t3230847821* ___chars0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::SaveWordWrappingState(TMPro.WordWrapState&,System.Int32,System.Int32)
extern "C"  void TMP_Text_SaveWordWrappingState_m3330465653 (TMP_Text_t980027659 * __this, WordWrapState_t4047764895 * ___state0, int32_t ___index1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TMP_Text::RestoreWordWrappingState(TMPro.WordWrapState&)
extern "C"  int32_t TMP_Text_RestoreWordWrappingState_m2128793728 (TMP_Text_t980027659 * __this, WordWrapState_t4047764895 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::SaveGlyphVertexInfo(System.Single,System.Single,UnityEngine.Color32)
extern "C"  void TMP_Text_SaveGlyphVertexInfo_m1629167814 (TMP_Text_t980027659 * __this, float ___padding0, float ___style_padding1, Color32_t598853688  ___vertexColor2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::SaveSpriteVertexInfo(UnityEngine.Color32)
extern "C"  void TMP_Text_SaveSpriteVertexInfo_m1464933915 (TMP_Text_t980027659 * __this, Color32_t598853688  ___vertexColor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::FillCharacterVertexBuffers(System.Int32,System.Int32)
extern "C"  void TMP_Text_FillCharacterVertexBuffers_m3118289925 (TMP_Text_t980027659 * __this, int32_t ___i0, int32_t ___index_X41, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::FillSpriteVertexBuffers(System.Int32,System.Int32)
extern "C"  void TMP_Text_FillSpriteVertexBuffers_m2139932117 (TMP_Text_t980027659 * __this, int32_t ___i0, int32_t ___spriteIndex_X41, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TMPro.TMP_Text::PackUV(System.Single,System.Single,System.Single)
extern "C"  Vector2_t4282066565  TMP_Text_PackUV_m3329857320 (TMP_Text_t980027659 * __this, float ___x0, float ___y1, float ___scale2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TMP_Text::HexToInt(System.Char)
extern "C"  int32_t TMP_Text_HexToInt_m2645445758 (TMP_Text_t980027659 * __this, Il2CppChar ___hex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TMP_Text::GetUTF16(System.Int32)
extern "C"  int32_t TMP_Text_GetUTF16_m2601245773 (TMP_Text_t980027659 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TMP_Text::GetUTF32(System.Int32)
extern "C"  int32_t TMP_Text_GetUTF32_m4139904775 (TMP_Text_t980027659 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32 TMPro.TMP_Text::HexCharsToColor(System.Char[],System.Int32)
extern "C"  Color32_t598853688  TMP_Text_HexCharsToColor_m211673028 (TMP_Text_t980027659 * __this, CharU5BU5D_t3324145743* ___hexChars0, int32_t ___tagCount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32 TMPro.TMP_Text::HexCharsToColor(System.Char[],System.Int32,System.Int32)
extern "C"  Color32_t598853688  TMP_Text_HexCharsToColor_m706083731 (TMP_Text_t980027659 * __this, CharU5BU5D_t3324145743* ___hexChars0, int32_t ___startIndex1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TMP_Text::ConvertToFloat(System.Char[],System.Int32,System.Int32,System.Int32)
extern "C"  float TMP_Text_ConvertToFloat_m3595235438 (TMP_Text_t980027659 * __this, CharU5BU5D_t3324145743* ___chars0, int32_t ___startIndex1, int32_t ___length2, int32_t ___decimalPointIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_Text::ValidateHtmlTag(System.Int32[],System.Int32,System.Int32&)
extern "C"  bool TMP_Text_ValidateHtmlTag_m1656176046 (TMP_Text_t980027659 * __this, Int32U5BU5D_t3230847821* ___chars0, int32_t ___startIndex1, int32_t* ___endIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
