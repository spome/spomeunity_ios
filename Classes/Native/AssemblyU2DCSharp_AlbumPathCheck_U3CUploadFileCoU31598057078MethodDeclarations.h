﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AlbumPathCheck/<UploadFileCo>c__Iterator3
struct U3CUploadFileCoU3Ec__Iterator3_t1598057078;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AlbumPathCheck/<UploadFileCo>c__Iterator3::.ctor()
extern "C"  void U3CUploadFileCoU3Ec__Iterator3__ctor_m2396942261 (U3CUploadFileCoU3Ec__Iterator3_t1598057078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AlbumPathCheck/<UploadFileCo>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CUploadFileCoU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m39084285 (U3CUploadFileCoU3Ec__Iterator3_t1598057078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AlbumPathCheck/<UploadFileCo>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CUploadFileCoU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m4074372753 (U3CUploadFileCoU3Ec__Iterator3_t1598057078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AlbumPathCheck/<UploadFileCo>c__Iterator3::MoveNext()
extern "C"  bool U3CUploadFileCoU3Ec__Iterator3_MoveNext_m2820015903 (U3CUploadFileCoU3Ec__Iterator3_t1598057078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck/<UploadFileCo>c__Iterator3::Dispose()
extern "C"  void U3CUploadFileCoU3Ec__Iterator3_Dispose_m1257325234 (U3CUploadFileCoU3Ec__Iterator3_t1598057078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck/<UploadFileCo>c__Iterator3::Reset()
extern "C"  void U3CUploadFileCoU3Ec__Iterator3_Reset_m43375202 (U3CUploadFileCoU3Ec__Iterator3_t1598057078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
