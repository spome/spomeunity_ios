﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// UnityEngine.UI.InputField
struct InputField_t609046876;
// CurrentScreen
struct CurrentScreen_t3026574117;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CurrentScreen
struct  CurrentScreen_t3026574117  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject[] CurrentScreen::m_Display
	GameObjectU5BU5D_t2662109048* ___m_Display_2;
	// UnityEngine.GameObject[] CurrentScreen::m_MenuButton
	GameObjectU5BU5D_t2662109048* ___m_MenuButton_3;
	// UnityEngine.UI.InputField CurrentScreen::m_UserLoginEmail
	InputField_t609046876 * ___m_UserLoginEmail_4;

public:
	inline static int32_t get_offset_of_m_Display_2() { return static_cast<int32_t>(offsetof(CurrentScreen_t3026574117, ___m_Display_2)); }
	inline GameObjectU5BU5D_t2662109048* get_m_Display_2() const { return ___m_Display_2; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_m_Display_2() { return &___m_Display_2; }
	inline void set_m_Display_2(GameObjectU5BU5D_t2662109048* value)
	{
		___m_Display_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Display_2, value);
	}

	inline static int32_t get_offset_of_m_MenuButton_3() { return static_cast<int32_t>(offsetof(CurrentScreen_t3026574117, ___m_MenuButton_3)); }
	inline GameObjectU5BU5D_t2662109048* get_m_MenuButton_3() const { return ___m_MenuButton_3; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_m_MenuButton_3() { return &___m_MenuButton_3; }
	inline void set_m_MenuButton_3(GameObjectU5BU5D_t2662109048* value)
	{
		___m_MenuButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_MenuButton_3, value);
	}

	inline static int32_t get_offset_of_m_UserLoginEmail_4() { return static_cast<int32_t>(offsetof(CurrentScreen_t3026574117, ___m_UserLoginEmail_4)); }
	inline InputField_t609046876 * get_m_UserLoginEmail_4() const { return ___m_UserLoginEmail_4; }
	inline InputField_t609046876 ** get_address_of_m_UserLoginEmail_4() { return &___m_UserLoginEmail_4; }
	inline void set_m_UserLoginEmail_4(InputField_t609046876 * value)
	{
		___m_UserLoginEmail_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_UserLoginEmail_4, value);
	}
};

struct CurrentScreen_t3026574117_StaticFields
{
public:
	// System.Boolean CurrentScreen::FirstIn
	bool ___FirstIn_5;
	// CurrentScreen CurrentScreen::_instance
	CurrentScreen_t3026574117 * ____instance_6;

public:
	inline static int32_t get_offset_of_FirstIn_5() { return static_cast<int32_t>(offsetof(CurrentScreen_t3026574117_StaticFields, ___FirstIn_5)); }
	inline bool get_FirstIn_5() const { return ___FirstIn_5; }
	inline bool* get_address_of_FirstIn_5() { return &___FirstIn_5; }
	inline void set_FirstIn_5(bool value)
	{
		___FirstIn_5 = value;
	}

	inline static int32_t get_offset_of__instance_6() { return static_cast<int32_t>(offsetof(CurrentScreen_t3026574117_StaticFields, ____instance_6)); }
	inline CurrentScreen_t3026574117 * get__instance_6() const { return ____instance_6; }
	inline CurrentScreen_t3026574117 ** get_address_of__instance_6() { return &____instance_6; }
	inline void set__instance_6(CurrentScreen_t3026574117 * value)
	{
		____instance_6 = value;
		Il2CppCodeGenWriteBarrier(&____instance_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
