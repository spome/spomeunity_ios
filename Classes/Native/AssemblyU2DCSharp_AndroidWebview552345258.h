﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;
// AndroidWebview
struct AndroidWebview_t552345258;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidWebview
struct  AndroidWebview_t552345258  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.AndroidJavaObject AndroidWebview::_activity
	AndroidJavaObject_t2362096582 * ____activity_2;

public:
	inline static int32_t get_offset_of__activity_2() { return static_cast<int32_t>(offsetof(AndroidWebview_t552345258, ____activity_2)); }
	inline AndroidJavaObject_t2362096582 * get__activity_2() const { return ____activity_2; }
	inline AndroidJavaObject_t2362096582 ** get_address_of__activity_2() { return &____activity_2; }
	inline void set__activity_2(AndroidJavaObject_t2362096582 * value)
	{
		____activity_2 = value;
		Il2CppCodeGenWriteBarrier(&____activity_2, value);
	}
};

struct AndroidWebview_t552345258_StaticFields
{
public:
	// AndroidWebview AndroidWebview::_instance
	AndroidWebview_t552345258 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(AndroidWebview_t552345258_StaticFields, ____instance_3)); }
	inline AndroidWebview_t552345258 * get__instance_3() const { return ____instance_3; }
	inline AndroidWebview_t552345258 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(AndroidWebview_t552345258 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier(&____instance_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
