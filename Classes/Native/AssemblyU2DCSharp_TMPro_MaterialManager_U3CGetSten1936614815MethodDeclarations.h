﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.MaterialManager/<GetStencilMaterial>c__AnonStorey2B
struct U3CGetStencilMaterialU3Ec__AnonStorey2B_t1936614815;
// TMPro.MaterialManager/MaskingMaterial
struct MaskingMaterial_t2640101498;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_MaterialManager_MaskingMat2640101498.h"

// System.Void TMPro.MaterialManager/<GetStencilMaterial>c__AnonStorey2B::.ctor()
extern "C"  void U3CGetStencilMaterialU3Ec__AnonStorey2B__ctor_m1838621356 (U3CGetStencilMaterialU3Ec__AnonStorey2B_t1936614815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.MaterialManager/<GetStencilMaterial>c__AnonStorey2B::<>m__9(TMPro.MaterialManager/MaskingMaterial)
extern "C"  bool U3CGetStencilMaterialU3Ec__AnonStorey2B_U3CU3Em__9_m2974056678 (U3CGetStencilMaterialU3Ec__AnonStorey2B_t1936614815 * __this, MaskingMaterial_t2640101498 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
