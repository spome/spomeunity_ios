﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t3134621005;
// System.Object
struct Il2CppObject;
// MessageCheck
struct MessageCheck_t3146986849;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageCheck/<MessageDeleteServerCheck>c__Iterator1B
struct  U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789  : public Il2CppObject
{
public:
	// UnityEngine.WWW MessageCheck/<MessageDeleteServerCheck>c__Iterator1B::<www>__0
	WWW_t3134621005 * ___U3CwwwU3E__0_0;
	// System.Int32 MessageCheck/<MessageDeleteServerCheck>c__Iterator1B::$PC
	int32_t ___U24PC_1;
	// System.Object MessageCheck/<MessageDeleteServerCheck>c__Iterator1B::$current
	Il2CppObject * ___U24current_2;
	// MessageCheck MessageCheck/<MessageDeleteServerCheck>c__Iterator1B::<>f__this
	MessageCheck_t3146986849 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789, ___U3CwwwU3E__0_0)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789, ___U3CU3Ef__this_3)); }
	inline MessageCheck_t3146986849 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline MessageCheck_t3146986849 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(MessageCheck_t3146986849 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
