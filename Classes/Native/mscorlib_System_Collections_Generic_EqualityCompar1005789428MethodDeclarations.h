﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<CurrentState/States>
struct DefaultComparer_t1005789428;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CurrentState_States2558012761.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<CurrentState/States>::.ctor()
extern "C"  void DefaultComparer__ctor_m3206260951_gshared (DefaultComparer_t1005789428 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3206260951(__this, method) ((  void (*) (DefaultComparer_t1005789428 *, const MethodInfo*))DefaultComparer__ctor_m3206260951_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<CurrentState/States>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m653292276_gshared (DefaultComparer_t1005789428 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m653292276(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1005789428 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m653292276_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<CurrentState/States>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m899402280_gshared (DefaultComparer_t1005789428 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m899402280(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1005789428 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m899402280_gshared)(__this, ___x0, ___y1, method)
