﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AddDropDownContents
struct AddDropDownContents_t2054805772;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void AddDropDownContents::.ctor()
extern "C"  void AddDropDownContents__ctor_m2484709343 (AddDropDownContents_t2054805772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddDropDownContents::Start()
extern "C"  void AddDropDownContents_Start_m1431847135 (AddDropDownContents_t2054805772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddDropDownContents::Changed(System.Int32)
extern "C"  void AddDropDownContents_Changed_m1971402626 (AddDropDownContents_t2054805772 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddDropDownContents::JapanKenName()
extern "C"  void AddDropDownContents_JapanKenName_m1437829982 (AddDropDownContents_t2054805772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AddDropDownContents::Process()
extern "C"  Il2CppObject * AddDropDownContents_Process_m1126075636 (AddDropDownContents_t2054805772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddDropDownContents::Interpret(System.String)
extern "C"  void AddDropDownContents_Interpret_m2831212152 (AddDropDownContents_t2054805772 * __this, String_t* ____strSource0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
