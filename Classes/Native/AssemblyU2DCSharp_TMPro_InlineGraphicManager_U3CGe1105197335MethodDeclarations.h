﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.InlineGraphicManager/<GetSpriteIndexByIndex>c__AnonStorey2A
struct U3CGetSpriteIndexByIndexU3Ec__AnonStorey2A_t1105197335;
// TMPro.TMP_Sprite
struct TMP_Sprite_t3889423907;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_TMP_Sprite3889423907.h"

// System.Void TMPro.InlineGraphicManager/<GetSpriteIndexByIndex>c__AnonStorey2A::.ctor()
extern "C"  void U3CGetSpriteIndexByIndexU3Ec__AnonStorey2A__ctor_m2060488244 (U3CGetSpriteIndexByIndexU3Ec__AnonStorey2A_t1105197335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.InlineGraphicManager/<GetSpriteIndexByIndex>c__AnonStorey2A::<>m__8(TMPro.TMP_Sprite)
extern "C"  bool U3CGetSpriteIndexByIndexU3Ec__AnonStorey2A_U3CU3Em__8_m3038172802 (U3CGetSpriteIndexByIndexU3Ec__AnonStorey2A_t1105197335 * __this, TMP_Sprite_t3889423907 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
