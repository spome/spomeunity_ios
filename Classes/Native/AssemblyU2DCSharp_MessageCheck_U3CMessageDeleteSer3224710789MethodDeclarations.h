﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageCheck/<MessageDeleteServerCheck>c__Iterator1B
struct U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageCheck/<MessageDeleteServerCheck>c__Iterator1B::.ctor()
extern "C"  void U3CMessageDeleteServerCheckU3Ec__Iterator1B__ctor_m1870680502 (U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageCheck/<MessageDeleteServerCheck>c__Iterator1B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMessageDeleteServerCheckU3Ec__Iterator1B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3453673830 (U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageCheck/<MessageDeleteServerCheck>c__Iterator1B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMessageDeleteServerCheckU3Ec__Iterator1B_System_Collections_IEnumerator_get_Current_m3218632954 (U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MessageCheck/<MessageDeleteServerCheck>c__Iterator1B::MoveNext()
extern "C"  bool U3CMessageDeleteServerCheckU3Ec__Iterator1B_MoveNext_m3262631526 (U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck/<MessageDeleteServerCheck>c__Iterator1B::Dispose()
extern "C"  void U3CMessageDeleteServerCheckU3Ec__Iterator1B_Dispose_m2325915763 (U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck/<MessageDeleteServerCheck>c__Iterator1B::Reset()
extern "C"  void U3CMessageDeleteServerCheckU3Ec__Iterator1B_Reset_m3812080739 (U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
