﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageSee/<MessageSeeCheckProgress>c__Iterator1D
struct U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageSee/<MessageSeeCheckProgress>c__Iterator1D::.ctor()
extern "C"  void U3CMessageSeeCheckProgressU3Ec__Iterator1D__ctor_m1954795733 (U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageSee/<MessageSeeCheckProgress>c__Iterator1D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMessageSeeCheckProgressU3Ec__Iterator1D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m717658333 (U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageSee/<MessageSeeCheckProgress>c__Iterator1D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMessageSeeCheckProgressU3Ec__Iterator1D_System_Collections_IEnumerator_get_Current_m2071459441 (U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MessageSee/<MessageSeeCheckProgress>c__Iterator1D::MoveNext()
extern "C"  bool U3CMessageSeeCheckProgressU3Ec__Iterator1D_MoveNext_m3457615359 (U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSee/<MessageSeeCheckProgress>c__Iterator1D::Dispose()
extern "C"  void U3CMessageSeeCheckProgressU3Ec__Iterator1D_Dispose_m1556274130 (U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSee/<MessageSeeCheckProgress>c__Iterator1D::Reset()
extern "C"  void U3CMessageSeeCheckProgressU3Ec__Iterator1D_Reset_m3896195970 (U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
