﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.XPath.XPathEditableDocument
struct XPathEditableDocument_t61026146;
// System.Xml.XmlNode
struct XmlNode_t856910923;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t1075073278;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNode856910923.h"

// System.Void Mono.Xml.XPath.XPathEditableDocument::.ctor(System.Xml.XmlNode)
extern "C"  void XPathEditableDocument__ctor_m2910074809 (XPathEditableDocument_t61026146 * __this, XmlNode_t856910923 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode Mono.Xml.XPath.XPathEditableDocument::get_Node()
extern "C"  XmlNode_t856910923 * XPathEditableDocument_get_Node_m560412385 (XPathEditableDocument_t61026146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNavigator Mono.Xml.XPath.XPathEditableDocument::CreateNavigator()
extern "C"  XPathNavigator_t1075073278 * XPathEditableDocument_CreateNavigator_m3544958723 (XPathEditableDocument_t61026146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
