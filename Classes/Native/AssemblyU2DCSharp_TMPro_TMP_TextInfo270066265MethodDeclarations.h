﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.TMP_TextInfo
struct TMP_TextInfo_t270066265;
// TMPro.TMP_Text
struct TMP_Text_t980027659;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_TMP_Text980027659.h"

// System.Void TMPro.TMP_TextInfo::.ctor()
extern "C"  void TMP_TextInfo__ctor_m2549835038 (TMP_TextInfo_t270066265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_TextInfo::.ctor(TMPro.TMP_Text)
extern "C"  void TMP_TextInfo__ctor_m593779567 (TMP_TextInfo_t270066265 * __this, TMP_Text_t980027659 * ___textComponent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_TextInfo::.cctor()
extern "C"  void TMP_TextInfo__cctor_m1253378639 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_TextInfo::Clear()
extern "C"  void TMP_TextInfo_Clear_m4250935625 (TMP_TextInfo_t270066265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_TextInfo::ClearMeshInfo()
extern "C"  void TMP_TextInfo_ClearMeshInfo_m1884898020 (TMP_TextInfo_t270066265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_TextInfo::ClearAllMeshInfo()
extern "C"  void TMP_TextInfo_ClearAllMeshInfo_m3237259669 (TMP_TextInfo_t270066265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_TextInfo::ClearLineInfo()
extern "C"  void TMP_TextInfo_ClearLineInfo_m654900139 (TMP_TextInfo_t270066265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
