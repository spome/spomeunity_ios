﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Action
struct Action_t3771233898;
// System.Action`2<System.Object,System.Object>
struct Action_2_t4293064463;
// System.Action`3<System.Object,System.Object,System.Object>
struct Action_3_t2968268857;
// System.Action`2<System.Object,TMPro.Compute_DT_EventArgs>
struct Action_2_t2458033858;
// System.Action`2<System.Boolean,System.Object>
struct Action_2_t2345743608;
// System.Action`2<System.Boolean,UnityEngine.Material>
struct Action_2_t2045527344;
// System.Action`2<System.Boolean,TMPro.TMP_FontAsset>
struct Action_2_t3437444928;
// System.Action`2<System.Boolean,UnityEngine.Object>
struct Action_2_t1246405896;
// System.Action`2<System.Boolean,TMPro.TextMeshPro>
struct Action_2_t1373022650;
// System.Action`3<UnityEngine.GameObject,UnityEngine.Material,UnityEngine.Material>
struct Action_3_t1457834283;
// System.Action`2<System.Boolean,TMPro.TextMeshProUGUI>
struct Action_2_t1778302432;

#include "mscorlib_System_Array1146569071.h"
#include "System_Core_System_Action3771233898.h"
#include "System_Core_System_Action_2_gen4293064463.h"
#include "System_Core_System_Action_3_gen2968268857.h"
#include "System_Core_System_Action_2_gen2458033858.h"
#include "System_Core_System_Action_2_gen2345743608.h"
#include "System_Core_System_Action_2_gen2045527344.h"
#include "System_Core_System_Action_2_gen3437444928.h"
#include "System_Core_System_Action_2_gen1246405896.h"
#include "System_Core_System_Action_2_gen1373022650.h"
#include "System_Core_System_Action_3_gen1457834283.h"
#include "System_Core_System_Action_2_gen1778302432.h"

#pragma once
// System.Action[]
struct ActionU5BU5D_t1643143343  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_t3771233898 * m_Items[1];

public:
	inline Action_t3771233898 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_t3771233898 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_t3771233898 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Action`2<System.Object,System.Object>[]
struct Action_2U5BU5D_t1881593814  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_2_t4293064463 * m_Items[1];

public:
	inline Action_2_t4293064463 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_2_t4293064463 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_2_t4293064463 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Action`3<System.Object,System.Object,System.Object>[]
struct Action_3U5BU5D_t607867588  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_3_t2968268857 * m_Items[1];

public:
	inline Action_3_t2968268857 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_3_t2968268857 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_3_t2968268857 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Action`2<System.Object,TMPro.Compute_DT_EventArgs>[]
struct Action_2U5BU5D_t1068012535  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_2_t2458033858 * m_Items[1];

public:
	inline Action_2_t2458033858 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_2_t2458033858 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_2_t2458033858 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Action`2<System.Boolean,System.Object>[]
struct Action_2U5BU5D_t3652010025  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_2_t2345743608 * m_Items[1];

public:
	inline Action_2_t2345743608 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_2_t2345743608 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_2_t2345743608 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Action`2<System.Boolean,UnityEngine.Material>[]
struct Action_2U5BU5D_t2714210321  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_2_t2045527344 * m_Items[1];

public:
	inline Action_2_t2045527344 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_2_t2045527344 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_2_t2045527344 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Action`2<System.Boolean,TMPro.TMP_FontAsset>[]
struct Action_2U5BU5D_t3639450561  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_2_t3437444928 * m_Items[1];

public:
	inline Action_2_t3437444928 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_2_t3437444928 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_2_t3437444928 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Action`2<System.Boolean,UnityEngine.Object>[]
struct Action_2U5BU5D_t3558489561  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_2_t1246405896 * m_Items[1];

public:
	inline Action_2_t1246405896 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_2_t1246405896 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_2_t1246405896 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Action`2<System.Boolean,TMPro.TextMeshPro>[]
struct Action_2U5BU5D_t4054771231  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_2_t1373022650 * m_Items[1];

public:
	inline Action_2_t1373022650 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_2_t1373022650 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_2_t1373022650 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Action`3<UnityEngine.GameObject,UnityEngine.Material,UnityEngine.Material>[]
struct Action_3U5BU5D_t1157844426  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_3_t1457834283 * m_Items[1];

public:
	inline Action_3_t1457834283 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_3_t1457834283 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_3_t1457834283 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Action`2<System.Boolean,TMPro.TextMeshProUGUI>[]
struct Action_2U5BU5D_t3453234849  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_2_t1778302432 * m_Items[1];

public:
	inline Action_2_t1778302432 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_2_t1778302432 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_2_t1778302432 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
