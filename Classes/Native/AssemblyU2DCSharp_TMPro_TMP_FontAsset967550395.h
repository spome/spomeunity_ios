﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TMPro.FaceInfo
struct FaceInfo_t3469866561;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.Material
struct Material_t3870600107;
// System.Collections.Generic.List`1<TMPro.TMP_Glyph>
struct List_1_t3367442646;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Glyph>
struct Dictionary_2_t1996520333;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.KerningPair>
struct Dictionary_2_t1902096943;
// TMPro.KerningTable
struct KerningTable_t618956984;
// TMPro.KerningPair
struct KerningPair_t1904833704;
// TMPro.LineBreakingTable
struct LineBreakingTable_t1570184089;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Func`2<TMPro.TMP_Glyph,System.Int32>
struct Func_2_t4216775979;

#include "AssemblyU2DCSharp_TMPro_TMP_Asset1993918410.h"
#include "AssemblyU2DCSharp_TMPro_TMP_FontAsset_FontAssetTyp3941512648.h"
#include "AssemblyU2DCSharp_TMPro_FontCreationSetting3395101892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontAsset
struct  TMP_FontAsset_t967550395  : public TMP_Asset_t1993918410
{
public:
	// TMPro.TMP_FontAsset/FontAssetTypes TMPro.TMP_FontAsset::fontAssetType
	int32_t ___fontAssetType_2;
	// TMPro.FaceInfo TMPro.TMP_FontAsset::m_fontInfo
	FaceInfo_t3469866561 * ___m_fontInfo_3;
	// System.Int32 TMPro.TMP_FontAsset::fontHashCode
	int32_t ___fontHashCode_4;
	// UnityEngine.Texture2D TMPro.TMP_FontAsset::atlas
	Texture2D_t3884108195 * ___atlas_5;
	// UnityEngine.Material TMPro.TMP_FontAsset::material
	Material_t3870600107 * ___material_6;
	// System.Int32 TMPro.TMP_FontAsset::materialHashCode
	int32_t ___materialHashCode_7;
	// System.Collections.Generic.List`1<TMPro.TMP_Glyph> TMPro.TMP_FontAsset::m_glyphInfoList
	List_1_t3367442646 * ___m_glyphInfoList_8;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Glyph> TMPro.TMP_FontAsset::m_characterDictionary
	Dictionary_2_t1996520333 * ___m_characterDictionary_9;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.KerningPair> TMPro.TMP_FontAsset::m_kerningDictionary
	Dictionary_2_t1902096943 * ___m_kerningDictionary_10;
	// TMPro.KerningTable TMPro.TMP_FontAsset::m_kerningInfo
	KerningTable_t618956984 * ___m_kerningInfo_11;
	// TMPro.KerningPair TMPro.TMP_FontAsset::m_kerningPair
	KerningPair_t1904833704 * ___m_kerningPair_12;
	// TMPro.LineBreakingTable TMPro.TMP_FontAsset::m_lineBreakingInfo
	LineBreakingTable_t1570184089 * ___m_lineBreakingInfo_13;
	// TMPro.FontCreationSetting TMPro.TMP_FontAsset::fontCreationSettings
	FontCreationSetting_t3395101892  ___fontCreationSettings_14;
	// System.Int32[] TMPro.TMP_FontAsset::m_characterSet
	Int32U5BU5D_t3230847821* ___m_characterSet_15;
	// System.Single TMPro.TMP_FontAsset::normalStyle
	float ___normalStyle_16;
	// System.Single TMPro.TMP_FontAsset::normalSpacingOffset
	float ___normalSpacingOffset_17;
	// System.Single TMPro.TMP_FontAsset::boldStyle
	float ___boldStyle_18;
	// System.Single TMPro.TMP_FontAsset::boldSpacing
	float ___boldSpacing_19;
	// System.Byte TMPro.TMP_FontAsset::italicStyle
	uint8_t ___italicStyle_20;
	// System.Byte TMPro.TMP_FontAsset::tabSize
	uint8_t ___tabSize_21;
	// System.Byte TMPro.TMP_FontAsset::m_oldTabSize
	uint8_t ___m_oldTabSize_22;

public:
	inline static int32_t get_offset_of_fontAssetType_2() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___fontAssetType_2)); }
	inline int32_t get_fontAssetType_2() const { return ___fontAssetType_2; }
	inline int32_t* get_address_of_fontAssetType_2() { return &___fontAssetType_2; }
	inline void set_fontAssetType_2(int32_t value)
	{
		___fontAssetType_2 = value;
	}

	inline static int32_t get_offset_of_m_fontInfo_3() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___m_fontInfo_3)); }
	inline FaceInfo_t3469866561 * get_m_fontInfo_3() const { return ___m_fontInfo_3; }
	inline FaceInfo_t3469866561 ** get_address_of_m_fontInfo_3() { return &___m_fontInfo_3; }
	inline void set_m_fontInfo_3(FaceInfo_t3469866561 * value)
	{
		___m_fontInfo_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_fontInfo_3, value);
	}

	inline static int32_t get_offset_of_fontHashCode_4() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___fontHashCode_4)); }
	inline int32_t get_fontHashCode_4() const { return ___fontHashCode_4; }
	inline int32_t* get_address_of_fontHashCode_4() { return &___fontHashCode_4; }
	inline void set_fontHashCode_4(int32_t value)
	{
		___fontHashCode_4 = value;
	}

	inline static int32_t get_offset_of_atlas_5() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___atlas_5)); }
	inline Texture2D_t3884108195 * get_atlas_5() const { return ___atlas_5; }
	inline Texture2D_t3884108195 ** get_address_of_atlas_5() { return &___atlas_5; }
	inline void set_atlas_5(Texture2D_t3884108195 * value)
	{
		___atlas_5 = value;
		Il2CppCodeGenWriteBarrier(&___atlas_5, value);
	}

	inline static int32_t get_offset_of_material_6() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___material_6)); }
	inline Material_t3870600107 * get_material_6() const { return ___material_6; }
	inline Material_t3870600107 ** get_address_of_material_6() { return &___material_6; }
	inline void set_material_6(Material_t3870600107 * value)
	{
		___material_6 = value;
		Il2CppCodeGenWriteBarrier(&___material_6, value);
	}

	inline static int32_t get_offset_of_materialHashCode_7() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___materialHashCode_7)); }
	inline int32_t get_materialHashCode_7() const { return ___materialHashCode_7; }
	inline int32_t* get_address_of_materialHashCode_7() { return &___materialHashCode_7; }
	inline void set_materialHashCode_7(int32_t value)
	{
		___materialHashCode_7 = value;
	}

	inline static int32_t get_offset_of_m_glyphInfoList_8() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___m_glyphInfoList_8)); }
	inline List_1_t3367442646 * get_m_glyphInfoList_8() const { return ___m_glyphInfoList_8; }
	inline List_1_t3367442646 ** get_address_of_m_glyphInfoList_8() { return &___m_glyphInfoList_8; }
	inline void set_m_glyphInfoList_8(List_1_t3367442646 * value)
	{
		___m_glyphInfoList_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_glyphInfoList_8, value);
	}

	inline static int32_t get_offset_of_m_characterDictionary_9() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___m_characterDictionary_9)); }
	inline Dictionary_2_t1996520333 * get_m_characterDictionary_9() const { return ___m_characterDictionary_9; }
	inline Dictionary_2_t1996520333 ** get_address_of_m_characterDictionary_9() { return &___m_characterDictionary_9; }
	inline void set_m_characterDictionary_9(Dictionary_2_t1996520333 * value)
	{
		___m_characterDictionary_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_characterDictionary_9, value);
	}

	inline static int32_t get_offset_of_m_kerningDictionary_10() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___m_kerningDictionary_10)); }
	inline Dictionary_2_t1902096943 * get_m_kerningDictionary_10() const { return ___m_kerningDictionary_10; }
	inline Dictionary_2_t1902096943 ** get_address_of_m_kerningDictionary_10() { return &___m_kerningDictionary_10; }
	inline void set_m_kerningDictionary_10(Dictionary_2_t1902096943 * value)
	{
		___m_kerningDictionary_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_kerningDictionary_10, value);
	}

	inline static int32_t get_offset_of_m_kerningInfo_11() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___m_kerningInfo_11)); }
	inline KerningTable_t618956984 * get_m_kerningInfo_11() const { return ___m_kerningInfo_11; }
	inline KerningTable_t618956984 ** get_address_of_m_kerningInfo_11() { return &___m_kerningInfo_11; }
	inline void set_m_kerningInfo_11(KerningTable_t618956984 * value)
	{
		___m_kerningInfo_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_kerningInfo_11, value);
	}

	inline static int32_t get_offset_of_m_kerningPair_12() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___m_kerningPair_12)); }
	inline KerningPair_t1904833704 * get_m_kerningPair_12() const { return ___m_kerningPair_12; }
	inline KerningPair_t1904833704 ** get_address_of_m_kerningPair_12() { return &___m_kerningPair_12; }
	inline void set_m_kerningPair_12(KerningPair_t1904833704 * value)
	{
		___m_kerningPair_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_kerningPair_12, value);
	}

	inline static int32_t get_offset_of_m_lineBreakingInfo_13() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___m_lineBreakingInfo_13)); }
	inline LineBreakingTable_t1570184089 * get_m_lineBreakingInfo_13() const { return ___m_lineBreakingInfo_13; }
	inline LineBreakingTable_t1570184089 ** get_address_of_m_lineBreakingInfo_13() { return &___m_lineBreakingInfo_13; }
	inline void set_m_lineBreakingInfo_13(LineBreakingTable_t1570184089 * value)
	{
		___m_lineBreakingInfo_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_lineBreakingInfo_13, value);
	}

	inline static int32_t get_offset_of_fontCreationSettings_14() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___fontCreationSettings_14)); }
	inline FontCreationSetting_t3395101892  get_fontCreationSettings_14() const { return ___fontCreationSettings_14; }
	inline FontCreationSetting_t3395101892 * get_address_of_fontCreationSettings_14() { return &___fontCreationSettings_14; }
	inline void set_fontCreationSettings_14(FontCreationSetting_t3395101892  value)
	{
		___fontCreationSettings_14 = value;
	}

	inline static int32_t get_offset_of_m_characterSet_15() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___m_characterSet_15)); }
	inline Int32U5BU5D_t3230847821* get_m_characterSet_15() const { return ___m_characterSet_15; }
	inline Int32U5BU5D_t3230847821** get_address_of_m_characterSet_15() { return &___m_characterSet_15; }
	inline void set_m_characterSet_15(Int32U5BU5D_t3230847821* value)
	{
		___m_characterSet_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_characterSet_15, value);
	}

	inline static int32_t get_offset_of_normalStyle_16() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___normalStyle_16)); }
	inline float get_normalStyle_16() const { return ___normalStyle_16; }
	inline float* get_address_of_normalStyle_16() { return &___normalStyle_16; }
	inline void set_normalStyle_16(float value)
	{
		___normalStyle_16 = value;
	}

	inline static int32_t get_offset_of_normalSpacingOffset_17() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___normalSpacingOffset_17)); }
	inline float get_normalSpacingOffset_17() const { return ___normalSpacingOffset_17; }
	inline float* get_address_of_normalSpacingOffset_17() { return &___normalSpacingOffset_17; }
	inline void set_normalSpacingOffset_17(float value)
	{
		___normalSpacingOffset_17 = value;
	}

	inline static int32_t get_offset_of_boldStyle_18() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___boldStyle_18)); }
	inline float get_boldStyle_18() const { return ___boldStyle_18; }
	inline float* get_address_of_boldStyle_18() { return &___boldStyle_18; }
	inline void set_boldStyle_18(float value)
	{
		___boldStyle_18 = value;
	}

	inline static int32_t get_offset_of_boldSpacing_19() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___boldSpacing_19)); }
	inline float get_boldSpacing_19() const { return ___boldSpacing_19; }
	inline float* get_address_of_boldSpacing_19() { return &___boldSpacing_19; }
	inline void set_boldSpacing_19(float value)
	{
		___boldSpacing_19 = value;
	}

	inline static int32_t get_offset_of_italicStyle_20() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___italicStyle_20)); }
	inline uint8_t get_italicStyle_20() const { return ___italicStyle_20; }
	inline uint8_t* get_address_of_italicStyle_20() { return &___italicStyle_20; }
	inline void set_italicStyle_20(uint8_t value)
	{
		___italicStyle_20 = value;
	}

	inline static int32_t get_offset_of_tabSize_21() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___tabSize_21)); }
	inline uint8_t get_tabSize_21() const { return ___tabSize_21; }
	inline uint8_t* get_address_of_tabSize_21() { return &___tabSize_21; }
	inline void set_tabSize_21(uint8_t value)
	{
		___tabSize_21 = value;
	}

	inline static int32_t get_offset_of_m_oldTabSize_22() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395, ___m_oldTabSize_22)); }
	inline uint8_t get_m_oldTabSize_22() const { return ___m_oldTabSize_22; }
	inline uint8_t* get_address_of_m_oldTabSize_22() { return &___m_oldTabSize_22; }
	inline void set_m_oldTabSize_22(uint8_t value)
	{
		___m_oldTabSize_22 = value;
	}
};

struct TMP_FontAsset_t967550395_StaticFields
{
public:
	// System.Func`2<TMPro.TMP_Glyph,System.Int32> TMPro.TMP_FontAsset::<>f__am$cache15
	Func_2_t4216775979 * ___U3CU3Ef__amU24cache15_23;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache15_23() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t967550395_StaticFields, ___U3CU3Ef__amU24cache15_23)); }
	inline Func_2_t4216775979 * get_U3CU3Ef__amU24cache15_23() const { return ___U3CU3Ef__amU24cache15_23; }
	inline Func_2_t4216775979 ** get_address_of_U3CU3Ef__amU24cache15_23() { return &___U3CU3Ef__amU24cache15_23; }
	inline void set_U3CU3Ef__amU24cache15_23(Func_2_t4216775979 * value)
	{
		___U3CU3Ef__amU24cache15_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache15_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
