﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AlbumPathCheck/<Send>c__Iterator2
struct U3CSendU3Ec__Iterator2_t611604662;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AlbumPathCheck/<Send>c__Iterator2::.ctor()
extern "C"  void U3CSendU3Ec__Iterator2__ctor_m740422005 (U3CSendU3Ec__Iterator2_t611604662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AlbumPathCheck/<Send>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3084669501 (U3CSendU3Ec__Iterator2_t611604662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AlbumPathCheck/<Send>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3874254801 (U3CSendU3Ec__Iterator2_t611604662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AlbumPathCheck/<Send>c__Iterator2::MoveNext()
extern "C"  bool U3CSendU3Ec__Iterator2_MoveNext_m2627385695 (U3CSendU3Ec__Iterator2_t611604662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck/<Send>c__Iterator2::Dispose()
extern "C"  void U3CSendU3Ec__Iterator2_Dispose_m2774226034 (U3CSendU3Ec__Iterator2_t611604662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck/<Send>c__Iterator2::Reset()
extern "C"  void U3CSendU3Ec__Iterator2_Reset_m2681822242 (U3CSendU3Ec__Iterator2_t611604662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
