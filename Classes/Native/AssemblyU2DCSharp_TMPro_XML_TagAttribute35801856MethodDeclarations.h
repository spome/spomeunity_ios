﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.XML_TagAttribute
struct XML_TagAttribute_t35801856;
struct XML_TagAttribute_t35801856_marshaled_pinvoke;
struct XML_TagAttribute_t35801856_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct XML_TagAttribute_t35801856;
struct XML_TagAttribute_t35801856_marshaled_pinvoke;

extern "C" void XML_TagAttribute_t35801856_marshal_pinvoke(const XML_TagAttribute_t35801856& unmarshaled, XML_TagAttribute_t35801856_marshaled_pinvoke& marshaled);
extern "C" void XML_TagAttribute_t35801856_marshal_pinvoke_back(const XML_TagAttribute_t35801856_marshaled_pinvoke& marshaled, XML_TagAttribute_t35801856& unmarshaled);
extern "C" void XML_TagAttribute_t35801856_marshal_pinvoke_cleanup(XML_TagAttribute_t35801856_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct XML_TagAttribute_t35801856;
struct XML_TagAttribute_t35801856_marshaled_com;

extern "C" void XML_TagAttribute_t35801856_marshal_com(const XML_TagAttribute_t35801856& unmarshaled, XML_TagAttribute_t35801856_marshaled_com& marshaled);
extern "C" void XML_TagAttribute_t35801856_marshal_com_back(const XML_TagAttribute_t35801856_marshaled_com& marshaled, XML_TagAttribute_t35801856& unmarshaled);
extern "C" void XML_TagAttribute_t35801856_marshal_com_cleanup(XML_TagAttribute_t35801856_marshaled_com& marshaled);
