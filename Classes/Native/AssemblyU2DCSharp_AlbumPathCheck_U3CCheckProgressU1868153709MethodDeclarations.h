﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AlbumPathCheck/<CheckProgress>c__Iterator4
struct U3CCheckProgressU3Ec__Iterator4_t1868153709;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AlbumPathCheck/<CheckProgress>c__Iterator4::.ctor()
extern "C"  void U3CCheckProgressU3Ec__Iterator4__ctor_m3640393678 (U3CCheckProgressU3Ec__Iterator4_t1868153709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AlbumPathCheck/<CheckProgress>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckProgressU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2241278094 (U3CCheckProgressU3Ec__Iterator4_t1868153709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AlbumPathCheck/<CheckProgress>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckProgressU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m3660058658 (U3CCheckProgressU3Ec__Iterator4_t1868153709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AlbumPathCheck/<CheckProgress>c__Iterator4::MoveNext()
extern "C"  bool U3CCheckProgressU3Ec__Iterator4_MoveNext_m1977440846 (U3CCheckProgressU3Ec__Iterator4_t1868153709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck/<CheckProgress>c__Iterator4::Dispose()
extern "C"  void U3CCheckProgressU3Ec__Iterator4_Dispose_m2213228683 (U3CCheckProgressU3Ec__Iterator4_t1868153709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck/<CheckProgress>c__Iterator4::Reset()
extern "C"  void U3CCheckProgressU3Ec__Iterator4_Reset_m1286826619 (U3CCheckProgressU3Ec__Iterator4_t1868153709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
