﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.TMP_UpdateManager
struct TMP_UpdateManager_t803277182;
// TMPro.TMP_Text
struct TMP_Text_t980027659;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_TMP_Text980027659.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"

// System.Void TMPro.TMP_UpdateManager::.ctor()
extern "C"  void TMP_UpdateManager__ctor_m368367209 (TMP_UpdateManager_t803277182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TMP_UpdateManager TMPro.TMP_UpdateManager::get_instance()
extern "C"  TMP_UpdateManager_t803277182 * TMP_UpdateManager_get_instance_m3054425672 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_UpdateManager::RegisterTextElementForLayoutRebuild(TMPro.TMP_Text)
extern "C"  void TMP_UpdateManager_RegisterTextElementForLayoutRebuild_m1576083820 (Il2CppObject * __this /* static, unused */, TMP_Text_t980027659 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_UpdateManager::InternalRegisterTextElementForLayoutRebuild(TMPro.TMP_Text)
extern "C"  bool TMP_UpdateManager_InternalRegisterTextElementForLayoutRebuild_m3736527395 (TMP_UpdateManager_t803277182 * __this, TMP_Text_t980027659 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_UpdateManager::RegisterTextElementForGraphicRebuild(TMPro.TMP_Text)
extern "C"  void TMP_UpdateManager_RegisterTextElementForGraphicRebuild_m638524796 (Il2CppObject * __this /* static, unused */, TMP_Text_t980027659 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_UpdateManager::InternalRegisterTextElementForGraphicRebuild(TMPro.TMP_Text)
extern "C"  bool TMP_UpdateManager_InternalRegisterTextElementForGraphicRebuild_m3187766181 (TMP_UpdateManager_t803277182 * __this, TMP_Text_t980027659 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_UpdateManager::OnCameraPreRender(UnityEngine.Camera)
extern "C"  void TMP_UpdateManager_OnCameraPreRender_m1795524480 (TMP_UpdateManager_t803277182 * __this, Camera_t2727095145 * ___cam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_UpdateManager::UnRegisterTextElementForRebuild(TMPro.TMP_Text)
extern "C"  void TMP_UpdateManager_UnRegisterTextElementForRebuild_m2325060381 (Il2CppObject * __this /* static, unused */, TMP_Text_t980027659 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_UpdateManager::InternalUnRegisterTextElementForGraphicRebuild(TMPro.TMP_Text)
extern "C"  void TMP_UpdateManager_InternalUnRegisterTextElementForGraphicRebuild_m348002386 (TMP_UpdateManager_t803277182 * __this, TMP_Text_t980027659 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_UpdateManager::InternalUnRegisterTextElementForLayoutRebuild(TMPro.TMP_Text)
extern "C"  void TMP_UpdateManager_InternalUnRegisterTextElementForLayoutRebuild_m2259448790 (TMP_UpdateManager_t803277182 * __this, TMP_Text_t980027659 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
