﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPSManager/<OnApplicationPause>c__IteratorE
struct U3COnApplicationPauseU3Ec__IteratorE_t330808093;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GPSManager/<OnApplicationPause>c__IteratorE::.ctor()
extern "C"  void U3COnApplicationPauseU3Ec__IteratorE__ctor_m3397538030 (U3COnApplicationPauseU3Ec__IteratorE_t330808093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GPSManager/<OnApplicationPause>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3COnApplicationPauseU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1809486500 (U3COnApplicationPauseU3Ec__IteratorE_t330808093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GPSManager/<OnApplicationPause>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3COnApplicationPauseU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m3694026296 (U3COnApplicationPauseU3Ec__IteratorE_t330808093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GPSManager/<OnApplicationPause>c__IteratorE::MoveNext()
extern "C"  bool U3COnApplicationPauseU3Ec__IteratorE_MoveNext_m4234488902 (U3COnApplicationPauseU3Ec__IteratorE_t330808093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPSManager/<OnApplicationPause>c__IteratorE::Dispose()
extern "C"  void U3COnApplicationPauseU3Ec__IteratorE_Dispose_m757184939 (U3COnApplicationPauseU3Ec__IteratorE_t330808093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPSManager/<OnApplicationPause>c__IteratorE::Reset()
extern "C"  void U3COnApplicationPauseU3Ec__IteratorE_Reset_m1043970971 (U3COnApplicationPauseU3Ec__IteratorE_t330808093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
