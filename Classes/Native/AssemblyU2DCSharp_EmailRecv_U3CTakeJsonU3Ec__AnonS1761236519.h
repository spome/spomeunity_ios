﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// EmailRecv/<TakeJson>c__AnonStorey23
struct U3CTakeJsonU3Ec__AnonStorey23_t1761236520;
// EmailRecv
struct EmailRecv_t4124314498;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EmailRecv/<TakeJson>c__AnonStorey22
struct  U3CTakeJsonU3Ec__AnonStorey22_t1761236519  : public Il2CppObject
{
public:
	// UnityEngine.GameObject EmailRecv/<TakeJson>c__AnonStorey22::Btn
	GameObject_t3674682005 * ___Btn_0;
	// EmailRecv/<TakeJson>c__AnonStorey23 EmailRecv/<TakeJson>c__AnonStorey22::<>f__ref$35
	U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * ___U3CU3Ef__refU2435_1;
	// EmailRecv EmailRecv/<TakeJson>c__AnonStorey22::<>f__this
	EmailRecv_t4124314498 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_Btn_0() { return static_cast<int32_t>(offsetof(U3CTakeJsonU3Ec__AnonStorey22_t1761236519, ___Btn_0)); }
	inline GameObject_t3674682005 * get_Btn_0() const { return ___Btn_0; }
	inline GameObject_t3674682005 ** get_address_of_Btn_0() { return &___Btn_0; }
	inline void set_Btn_0(GameObject_t3674682005 * value)
	{
		___Btn_0 = value;
		Il2CppCodeGenWriteBarrier(&___Btn_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2435_1() { return static_cast<int32_t>(offsetof(U3CTakeJsonU3Ec__AnonStorey22_t1761236519, ___U3CU3Ef__refU2435_1)); }
	inline U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * get_U3CU3Ef__refU2435_1() const { return ___U3CU3Ef__refU2435_1; }
	inline U3CTakeJsonU3Ec__AnonStorey23_t1761236520 ** get_address_of_U3CU3Ef__refU2435_1() { return &___U3CU3Ef__refU2435_1; }
	inline void set_U3CU3Ef__refU2435_1(U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * value)
	{
		___U3CU3Ef__refU2435_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2435_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CTakeJsonU3Ec__AnonStorey22_t1761236519, ___U3CU3Ef__this_2)); }
	inline EmailRecv_t4124314498 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline EmailRecv_t4124314498 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(EmailRecv_t4124314498 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
