﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PrivateURL
struct PrivateURL_t117402892;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void PrivateURL::.ctor()
extern "C"  void PrivateURL__ctor_m3158841615 (PrivateURL_t117402892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrivateURL::get_ID()
extern "C"  String_t* PrivateURL_get_ID_m659887484 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrivateURL::set_ID(System.String)
extern "C"  void PrivateURL_set_ID_m2762899157 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrivateURL::get_PW()
extern "C"  String_t* PrivateURL_get_PW_m660114280 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrivateURL::set_PW(System.String)
extern "C"  void PrivateURL_set_PW_m2535917673 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrivateURL::get_customerId()
extern "C"  String_t* PrivateURL_get_customerId_m4251176570 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrivateURL::set_customerId(System.String)
extern "C"  void PrivateURL_set_customerId_m1347654551 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrivateURL::get_userloginemail()
extern "C"  String_t* PrivateURL_get_userloginemail_m1497216895 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrivateURL::set_userloginemail(System.String)
extern "C"  void PrivateURL_set_userloginemail_m704658226 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrivateURL::get_selectmessageid()
extern "C"  String_t* PrivateURL_get_selectmessageid_m1725662023 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrivateURL::set_selectmessageid(System.String)
extern "C"  void PrivateURL_set_selectmessageid_m2533335980 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrivateURL::get_Login()
extern "C"  String_t* PrivateURL_get_Login_m301514730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrivateURL::get_Message()
extern "C"  String_t* PrivateURL_get_Message_m2273489160 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrivateURL::get_MessageOneDelete()
extern "C"  String_t* PrivateURL_get_MessageOneDelete_m1855499307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrivateURL::get_MessageRecv()
extern "C"  String_t* PrivateURL_get_MessageRecv_m5945870 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrivateURL::get_DeviceMessage()
extern "C"  String_t* PrivateURL_get_DeviceMessage_m3117622098 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrivateURL::get_RegistrationMessage()
extern "C"  String_t* PrivateURL_get_RegistrationMessage_m4175169615 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrivateURL::get_MessageCount()
extern "C"  String_t* PrivateURL_get_MessageCount_m59418249 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrivateURL::get_MemberMessageCount()
extern "C"  String_t* PrivateURL_get_MemberMessageCount_m606779331 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrivateURL::get_MemberShipChange()
extern "C"  String_t* PrivateURL_get_MemberShipChange_m2995936519 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrivateURL::get_EmailRecvUpdate()
extern "C"  String_t* PrivateURL_get_EmailRecvUpdate_m298749708 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
