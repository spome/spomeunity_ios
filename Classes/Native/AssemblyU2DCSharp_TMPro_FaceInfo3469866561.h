﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FaceInfo
struct  FaceInfo_t3469866561  : public Il2CppObject
{
public:
	// System.String TMPro.FaceInfo::Name
	String_t* ___Name_0;
	// System.Single TMPro.FaceInfo::PointSize
	float ___PointSize_1;
	// System.Single TMPro.FaceInfo::Scale
	float ___Scale_2;
	// System.Int32 TMPro.FaceInfo::CharacterCount
	int32_t ___CharacterCount_3;
	// System.Single TMPro.FaceInfo::LineHeight
	float ___LineHeight_4;
	// System.Single TMPro.FaceInfo::Baseline
	float ___Baseline_5;
	// System.Single TMPro.FaceInfo::Ascender
	float ___Ascender_6;
	// System.Single TMPro.FaceInfo::Descender
	float ___Descender_7;
	// System.Single TMPro.FaceInfo::CenterLine
	float ___CenterLine_8;
	// System.Single TMPro.FaceInfo::SuperscriptOffset
	float ___SuperscriptOffset_9;
	// System.Single TMPro.FaceInfo::SubscriptOffset
	float ___SubscriptOffset_10;
	// System.Single TMPro.FaceInfo::SubSize
	float ___SubSize_11;
	// System.Single TMPro.FaceInfo::Underline
	float ___Underline_12;
	// System.Single TMPro.FaceInfo::UnderlineThickness
	float ___UnderlineThickness_13;
	// System.Single TMPro.FaceInfo::TabWidth
	float ___TabWidth_14;
	// System.Single TMPro.FaceInfo::Padding
	float ___Padding_15;
	// System.Single TMPro.FaceInfo::AtlasWidth
	float ___AtlasWidth_16;
	// System.Single TMPro.FaceInfo::AtlasHeight
	float ___AtlasHeight_17;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier(&___Name_0, value);
	}

	inline static int32_t get_offset_of_PointSize_1() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___PointSize_1)); }
	inline float get_PointSize_1() const { return ___PointSize_1; }
	inline float* get_address_of_PointSize_1() { return &___PointSize_1; }
	inline void set_PointSize_1(float value)
	{
		___PointSize_1 = value;
	}

	inline static int32_t get_offset_of_Scale_2() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___Scale_2)); }
	inline float get_Scale_2() const { return ___Scale_2; }
	inline float* get_address_of_Scale_2() { return &___Scale_2; }
	inline void set_Scale_2(float value)
	{
		___Scale_2 = value;
	}

	inline static int32_t get_offset_of_CharacterCount_3() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___CharacterCount_3)); }
	inline int32_t get_CharacterCount_3() const { return ___CharacterCount_3; }
	inline int32_t* get_address_of_CharacterCount_3() { return &___CharacterCount_3; }
	inline void set_CharacterCount_3(int32_t value)
	{
		___CharacterCount_3 = value;
	}

	inline static int32_t get_offset_of_LineHeight_4() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___LineHeight_4)); }
	inline float get_LineHeight_4() const { return ___LineHeight_4; }
	inline float* get_address_of_LineHeight_4() { return &___LineHeight_4; }
	inline void set_LineHeight_4(float value)
	{
		___LineHeight_4 = value;
	}

	inline static int32_t get_offset_of_Baseline_5() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___Baseline_5)); }
	inline float get_Baseline_5() const { return ___Baseline_5; }
	inline float* get_address_of_Baseline_5() { return &___Baseline_5; }
	inline void set_Baseline_5(float value)
	{
		___Baseline_5 = value;
	}

	inline static int32_t get_offset_of_Ascender_6() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___Ascender_6)); }
	inline float get_Ascender_6() const { return ___Ascender_6; }
	inline float* get_address_of_Ascender_6() { return &___Ascender_6; }
	inline void set_Ascender_6(float value)
	{
		___Ascender_6 = value;
	}

	inline static int32_t get_offset_of_Descender_7() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___Descender_7)); }
	inline float get_Descender_7() const { return ___Descender_7; }
	inline float* get_address_of_Descender_7() { return &___Descender_7; }
	inline void set_Descender_7(float value)
	{
		___Descender_7 = value;
	}

	inline static int32_t get_offset_of_CenterLine_8() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___CenterLine_8)); }
	inline float get_CenterLine_8() const { return ___CenterLine_8; }
	inline float* get_address_of_CenterLine_8() { return &___CenterLine_8; }
	inline void set_CenterLine_8(float value)
	{
		___CenterLine_8 = value;
	}

	inline static int32_t get_offset_of_SuperscriptOffset_9() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___SuperscriptOffset_9)); }
	inline float get_SuperscriptOffset_9() const { return ___SuperscriptOffset_9; }
	inline float* get_address_of_SuperscriptOffset_9() { return &___SuperscriptOffset_9; }
	inline void set_SuperscriptOffset_9(float value)
	{
		___SuperscriptOffset_9 = value;
	}

	inline static int32_t get_offset_of_SubscriptOffset_10() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___SubscriptOffset_10)); }
	inline float get_SubscriptOffset_10() const { return ___SubscriptOffset_10; }
	inline float* get_address_of_SubscriptOffset_10() { return &___SubscriptOffset_10; }
	inline void set_SubscriptOffset_10(float value)
	{
		___SubscriptOffset_10 = value;
	}

	inline static int32_t get_offset_of_SubSize_11() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___SubSize_11)); }
	inline float get_SubSize_11() const { return ___SubSize_11; }
	inline float* get_address_of_SubSize_11() { return &___SubSize_11; }
	inline void set_SubSize_11(float value)
	{
		___SubSize_11 = value;
	}

	inline static int32_t get_offset_of_Underline_12() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___Underline_12)); }
	inline float get_Underline_12() const { return ___Underline_12; }
	inline float* get_address_of_Underline_12() { return &___Underline_12; }
	inline void set_Underline_12(float value)
	{
		___Underline_12 = value;
	}

	inline static int32_t get_offset_of_UnderlineThickness_13() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___UnderlineThickness_13)); }
	inline float get_UnderlineThickness_13() const { return ___UnderlineThickness_13; }
	inline float* get_address_of_UnderlineThickness_13() { return &___UnderlineThickness_13; }
	inline void set_UnderlineThickness_13(float value)
	{
		___UnderlineThickness_13 = value;
	}

	inline static int32_t get_offset_of_TabWidth_14() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___TabWidth_14)); }
	inline float get_TabWidth_14() const { return ___TabWidth_14; }
	inline float* get_address_of_TabWidth_14() { return &___TabWidth_14; }
	inline void set_TabWidth_14(float value)
	{
		___TabWidth_14 = value;
	}

	inline static int32_t get_offset_of_Padding_15() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___Padding_15)); }
	inline float get_Padding_15() const { return ___Padding_15; }
	inline float* get_address_of_Padding_15() { return &___Padding_15; }
	inline void set_Padding_15(float value)
	{
		___Padding_15 = value;
	}

	inline static int32_t get_offset_of_AtlasWidth_16() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___AtlasWidth_16)); }
	inline float get_AtlasWidth_16() const { return ___AtlasWidth_16; }
	inline float* get_address_of_AtlasWidth_16() { return &___AtlasWidth_16; }
	inline void set_AtlasWidth_16(float value)
	{
		___AtlasWidth_16 = value;
	}

	inline static int32_t get_offset_of_AtlasHeight_17() { return static_cast<int32_t>(offsetof(FaceInfo_t3469866561, ___AtlasHeight_17)); }
	inline float get_AtlasHeight_17() const { return ___AtlasHeight_17; }
	inline float* get_address_of_AtlasHeight_17() { return &___AtlasHeight_17; }
	inline void set_AtlasHeight_17(float value)
	{
		___AtlasHeight_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
