﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<CurrentState/States,UnityEngine.GameObject>
struct Dictionary_2_t2595600883;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_CurrentState_States2558012761.h"
#include "AssemblyU2DCSharp_CurrentState_InquiryTitles1700261515.h"
#include "AssemblyU2DCSharp_CurrentState_GPSPopupMessages960090149.h"
#include "AssemblyU2DCSharp_CurrentState_LoginPopupMessages1285607942.h"
#include "AssemblyU2DCSharp_CurrentState_PopupMessages2071685857.h"
#include "AssemblyU2DCSharp_CurrentState_MemberStates3108678483.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CurrentState
struct  CurrentState_t1622142648  : public Il2CppObject
{
public:

public:
};

struct CurrentState_t1622142648_StaticFields
{
public:
	// CurrentState/States CurrentState::_State
	int32_t ____State_0;
	// CurrentState/States CurrentState::_LoginState
	int32_t ____LoginState_1;
	// CurrentState/InquiryTitles CurrentState::_InquiryTitle
	int32_t ____InquiryTitle_2;
	// CurrentState/GPSPopupMessages CurrentState::_GPSPopupMessage
	int32_t ____GPSPopupMessage_3;
	// CurrentState/LoginPopupMessages CurrentState::_LoginPopupMessage
	int32_t ____LoginPopupMessage_4;
	// CurrentState/PopupMessages CurrentState::_PopupMessage
	int32_t ____PopupMessage_5;
	// CurrentState/MemberStates CurrentState::_MemberState
	int32_t ____MemberState_6;
	// System.Collections.Generic.Dictionary`2<CurrentState/States,UnityEngine.GameObject> CurrentState::_ScreenState
	Dictionary_2_t2595600883 * ____ScreenState_7;

public:
	inline static int32_t get_offset_of__State_0() { return static_cast<int32_t>(offsetof(CurrentState_t1622142648_StaticFields, ____State_0)); }
	inline int32_t get__State_0() const { return ____State_0; }
	inline int32_t* get_address_of__State_0() { return &____State_0; }
	inline void set__State_0(int32_t value)
	{
		____State_0 = value;
	}

	inline static int32_t get_offset_of__LoginState_1() { return static_cast<int32_t>(offsetof(CurrentState_t1622142648_StaticFields, ____LoginState_1)); }
	inline int32_t get__LoginState_1() const { return ____LoginState_1; }
	inline int32_t* get_address_of__LoginState_1() { return &____LoginState_1; }
	inline void set__LoginState_1(int32_t value)
	{
		____LoginState_1 = value;
	}

	inline static int32_t get_offset_of__InquiryTitle_2() { return static_cast<int32_t>(offsetof(CurrentState_t1622142648_StaticFields, ____InquiryTitle_2)); }
	inline int32_t get__InquiryTitle_2() const { return ____InquiryTitle_2; }
	inline int32_t* get_address_of__InquiryTitle_2() { return &____InquiryTitle_2; }
	inline void set__InquiryTitle_2(int32_t value)
	{
		____InquiryTitle_2 = value;
	}

	inline static int32_t get_offset_of__GPSPopupMessage_3() { return static_cast<int32_t>(offsetof(CurrentState_t1622142648_StaticFields, ____GPSPopupMessage_3)); }
	inline int32_t get__GPSPopupMessage_3() const { return ____GPSPopupMessage_3; }
	inline int32_t* get_address_of__GPSPopupMessage_3() { return &____GPSPopupMessage_3; }
	inline void set__GPSPopupMessage_3(int32_t value)
	{
		____GPSPopupMessage_3 = value;
	}

	inline static int32_t get_offset_of__LoginPopupMessage_4() { return static_cast<int32_t>(offsetof(CurrentState_t1622142648_StaticFields, ____LoginPopupMessage_4)); }
	inline int32_t get__LoginPopupMessage_4() const { return ____LoginPopupMessage_4; }
	inline int32_t* get_address_of__LoginPopupMessage_4() { return &____LoginPopupMessage_4; }
	inline void set__LoginPopupMessage_4(int32_t value)
	{
		____LoginPopupMessage_4 = value;
	}

	inline static int32_t get_offset_of__PopupMessage_5() { return static_cast<int32_t>(offsetof(CurrentState_t1622142648_StaticFields, ____PopupMessage_5)); }
	inline int32_t get__PopupMessage_5() const { return ____PopupMessage_5; }
	inline int32_t* get_address_of__PopupMessage_5() { return &____PopupMessage_5; }
	inline void set__PopupMessage_5(int32_t value)
	{
		____PopupMessage_5 = value;
	}

	inline static int32_t get_offset_of__MemberState_6() { return static_cast<int32_t>(offsetof(CurrentState_t1622142648_StaticFields, ____MemberState_6)); }
	inline int32_t get__MemberState_6() const { return ____MemberState_6; }
	inline int32_t* get_address_of__MemberState_6() { return &____MemberState_6; }
	inline void set__MemberState_6(int32_t value)
	{
		____MemberState_6 = value;
	}

	inline static int32_t get_offset_of__ScreenState_7() { return static_cast<int32_t>(offsetof(CurrentState_t1622142648_StaticFields, ____ScreenState_7)); }
	inline Dictionary_2_t2595600883 * get__ScreenState_7() const { return ____ScreenState_7; }
	inline Dictionary_2_t2595600883 ** get_address_of__ScreenState_7() { return &____ScreenState_7; }
	inline void set__ScreenState_7(Dictionary_2_t2595600883 * value)
	{
		____ScreenState_7 = value;
		Il2CppCodeGenWriteBarrier(&____ScreenState_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
