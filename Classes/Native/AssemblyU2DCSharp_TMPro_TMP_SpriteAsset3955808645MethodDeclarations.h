﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t3955808645;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void TMPro.TMP_SpriteAsset::.ctor()
extern "C"  void TMP_SpriteAsset__ctor_m3039934530 (TMP_SpriteAsset_t3955808645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_SpriteAsset::OnEnable()
extern "C"  void TMP_SpriteAsset_OnEnable_m1080758276 (TMP_SpriteAsset_t3955808645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_SpriteAsset::AddSprites(System.String)
extern "C"  void TMP_SpriteAsset_AddSprites_m2785143859 (TMP_SpriteAsset_t3955808645 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_SpriteAsset::OnValidate()
extern "C"  void TMP_SpriteAsset_OnValidate_m1586158295 (TMP_SpriteAsset_t3955808645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
