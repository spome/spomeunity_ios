﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPSManager
struct GPSManager_t1489257411;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void GPSManager::.ctor()
extern "C"  void GPSManager__ctor_m739500088 (GPSManager_t1489257411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GPSManager::get__latitude()
extern "C"  double GPSManager_get__latitude_m2672992213 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GPSManager::get__longitude()
extern "C"  double GPSManager_get__longitude_m385823368 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GPSManager::get__distance()
extern "C"  double GPSManager_get__distance_m1549713694 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GPSManager::get__TargetLatitude()
extern "C"  double GPSManager_get__TargetLatitude_m846758502 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPSManager::set__TargetLatitude(System.Double)
extern "C"  void GPSManager_set__TargetLatitude_m470540013 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GPSManager::get__TargetLongitude()
extern "C"  double GPSManager_get__TargetLongitude_m3902120471 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPSManager::set__TargetLongitude(System.Double)
extern "C"  void GPSManager_set__TargetLongitude_m2588667994 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GPSManager::Start()
extern "C"  Il2CppObject * GPSManager_Start_m3100476912 (GPSManager_t1489257411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GPSManager::OnApplicationPause(System.Boolean)
extern "C"  Il2CppObject * GPSManager_OnApplicationPause_m827286544 (GPSManager_t1489257411 * __this, bool ___pauseState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GPSManager::Haversine(System.Single&,System.Single&)
extern "C"  float GPSManager_Haversine_m430118693 (GPSManager_t1489257411 * __this, float* ___lastLatitude0, float* ___lastLongitude1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GPSManager::Distance(System.Double,System.Double,System.Double,System.Double)
extern "C"  double GPSManager_Distance_m4256732498 (GPSManager_t1489257411 * __this, double ___lat10, double ___lon11, double ___lat22, double ___lon23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GPSManager::deg2rad(System.Double)
extern "C"  double GPSManager_deg2rad_m276033958 (GPSManager_t1489257411 * __this, double ___deg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GPSManager::rad2deg(System.Double)
extern "C"  double GPSManager_rad2deg_m2849866022 (GPSManager_t1489257411 * __this, double ___rad0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPSManager::Update()
extern "C"  void GPSManager_Update_m3176528341 (GPSManager_t1489257411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPSManager::FixedUpdate()
extern "C"  void GPSManager_FixedUpdate_m1846719411 (GPSManager_t1489257411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
