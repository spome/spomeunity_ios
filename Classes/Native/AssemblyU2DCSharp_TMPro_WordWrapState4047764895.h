﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TMPro.TMP_TextInfo
struct TMP_TextInfo_t270066265;

#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_TMPro_FontStyles3228051751.h"
#include "AssemblyU2DCSharp_TMPro_TMP_LineInfo2462355872.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "AssemblyU2DCSharp_TMPro_TMP_XmlTagStack_1_gen1808196333.h"
#include "AssemblyU2DCSharp_TMPro_TMP_XmlTagStack_1_gen1206294321.h"
#include "AssemblyU2DCSharp_TMPro_TMP_XmlTagStack_1_gen2363181145.h"
#include "AssemblyU2DCSharp_TMPro_Extents2060714539.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t4047764895 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_10;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_11;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_12;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_13;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_14;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_15;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_16;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_17;
	// System.Single TMPro.WordWrapState::maxFontScale
	float ___maxFontScale_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_23;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_24;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_25;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_t270066265 * ___textInfo_26;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_t2462355872  ___lineInfo_27;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t598853688  ___vertexColor_28;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_XmlTagStack_1_t1808196333  ___colorStack_29;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_XmlTagStack_1_t1206294321  ___sizeStack_30;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_XmlTagStack_1_t2363181145  ___styleStack_31;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_t2060714539  ___meshExtents_32;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_33;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxAscender_10() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___maxAscender_10)); }
	inline float get_maxAscender_10() const { return ___maxAscender_10; }
	inline float* get_address_of_maxAscender_10() { return &___maxAscender_10; }
	inline void set_maxAscender_10(float value)
	{
		___maxAscender_10 = value;
	}

	inline static int32_t get_offset_of_maxDescender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___maxDescender_11)); }
	inline float get_maxDescender_11() const { return ___maxDescender_11; }
	inline float* get_address_of_maxDescender_11() { return &___maxDescender_11; }
	inline void set_maxDescender_11(float value)
	{
		___maxDescender_11 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___maxLineAscender_12)); }
	inline float get_maxLineAscender_12() const { return ___maxLineAscender_12; }
	inline float* get_address_of_maxLineAscender_12() { return &___maxLineAscender_12; }
	inline void set_maxLineAscender_12(float value)
	{
		___maxLineAscender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___maxLineDescender_13)); }
	inline float get_maxLineDescender_13() const { return ___maxLineDescender_13; }
	inline float* get_address_of_maxLineDescender_13() { return &___maxLineDescender_13; }
	inline void set_maxLineDescender_13(float value)
	{
		___maxLineDescender_13 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___previousLineAscender_14)); }
	inline float get_previousLineAscender_14() const { return ___previousLineAscender_14; }
	inline float* get_address_of_previousLineAscender_14() { return &___previousLineAscender_14; }
	inline void set_previousLineAscender_14(float value)
	{
		___previousLineAscender_14 = value;
	}

	inline static int32_t get_offset_of_xAdvance_15() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___xAdvance_15)); }
	inline float get_xAdvance_15() const { return ___xAdvance_15; }
	inline float* get_address_of_xAdvance_15() { return &___xAdvance_15; }
	inline void set_xAdvance_15(float value)
	{
		___xAdvance_15 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_16() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___preferredWidth_16)); }
	inline float get_preferredWidth_16() const { return ___preferredWidth_16; }
	inline float* get_address_of_preferredWidth_16() { return &___preferredWidth_16; }
	inline void set_preferredWidth_16(float value)
	{
		___preferredWidth_16 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_17() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___preferredHeight_17)); }
	inline float get_preferredHeight_17() const { return ___preferredHeight_17; }
	inline float* get_address_of_preferredHeight_17() { return &___preferredHeight_17; }
	inline void set_preferredHeight_17(float value)
	{
		___preferredHeight_17 = value;
	}

	inline static int32_t get_offset_of_maxFontScale_18() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___maxFontScale_18)); }
	inline float get_maxFontScale_18() const { return ___maxFontScale_18; }
	inline float* get_address_of_maxFontScale_18() { return &___maxFontScale_18; }
	inline void set_maxFontScale_18(float value)
	{
		___maxFontScale_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_23() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___currentFontSize_23)); }
	inline float get_currentFontSize_23() const { return ___currentFontSize_23; }
	inline float* get_address_of_currentFontSize_23() { return &___currentFontSize_23; }
	inline void set_currentFontSize_23(float value)
	{
		___currentFontSize_23 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_24() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___baselineOffset_24)); }
	inline float get_baselineOffset_24() const { return ___baselineOffset_24; }
	inline float* get_address_of_baselineOffset_24() { return &___baselineOffset_24; }
	inline void set_baselineOffset_24(float value)
	{
		___baselineOffset_24 = value;
	}

	inline static int32_t get_offset_of_lineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___lineOffset_25)); }
	inline float get_lineOffset_25() const { return ___lineOffset_25; }
	inline float* get_address_of_lineOffset_25() { return &___lineOffset_25; }
	inline void set_lineOffset_25(float value)
	{
		___lineOffset_25 = value;
	}

	inline static int32_t get_offset_of_textInfo_26() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___textInfo_26)); }
	inline TMP_TextInfo_t270066265 * get_textInfo_26() const { return ___textInfo_26; }
	inline TMP_TextInfo_t270066265 ** get_address_of_textInfo_26() { return &___textInfo_26; }
	inline void set_textInfo_26(TMP_TextInfo_t270066265 * value)
	{
		___textInfo_26 = value;
		Il2CppCodeGenWriteBarrier(&___textInfo_26, value);
	}

	inline static int32_t get_offset_of_lineInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___lineInfo_27)); }
	inline TMP_LineInfo_t2462355872  get_lineInfo_27() const { return ___lineInfo_27; }
	inline TMP_LineInfo_t2462355872 * get_address_of_lineInfo_27() { return &___lineInfo_27; }
	inline void set_lineInfo_27(TMP_LineInfo_t2462355872  value)
	{
		___lineInfo_27 = value;
	}

	inline static int32_t get_offset_of_vertexColor_28() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___vertexColor_28)); }
	inline Color32_t598853688  get_vertexColor_28() const { return ___vertexColor_28; }
	inline Color32_t598853688 * get_address_of_vertexColor_28() { return &___vertexColor_28; }
	inline void set_vertexColor_28(Color32_t598853688  value)
	{
		___vertexColor_28 = value;
	}

	inline static int32_t get_offset_of_colorStack_29() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___colorStack_29)); }
	inline TMP_XmlTagStack_1_t1808196333  get_colorStack_29() const { return ___colorStack_29; }
	inline TMP_XmlTagStack_1_t1808196333 * get_address_of_colorStack_29() { return &___colorStack_29; }
	inline void set_colorStack_29(TMP_XmlTagStack_1_t1808196333  value)
	{
		___colorStack_29 = value;
	}

	inline static int32_t get_offset_of_sizeStack_30() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___sizeStack_30)); }
	inline TMP_XmlTagStack_1_t1206294321  get_sizeStack_30() const { return ___sizeStack_30; }
	inline TMP_XmlTagStack_1_t1206294321 * get_address_of_sizeStack_30() { return &___sizeStack_30; }
	inline void set_sizeStack_30(TMP_XmlTagStack_1_t1206294321  value)
	{
		___sizeStack_30 = value;
	}

	inline static int32_t get_offset_of_styleStack_31() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___styleStack_31)); }
	inline TMP_XmlTagStack_1_t2363181145  get_styleStack_31() const { return ___styleStack_31; }
	inline TMP_XmlTagStack_1_t2363181145 * get_address_of_styleStack_31() { return &___styleStack_31; }
	inline void set_styleStack_31(TMP_XmlTagStack_1_t2363181145  value)
	{
		___styleStack_31 = value;
	}

	inline static int32_t get_offset_of_meshExtents_32() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___meshExtents_32)); }
	inline Extents_t2060714539  get_meshExtents_32() const { return ___meshExtents_32; }
	inline Extents_t2060714539 * get_address_of_meshExtents_32() { return &___meshExtents_32; }
	inline void set_meshExtents_32(Extents_t2060714539  value)
	{
		___meshExtents_32 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_33() { return static_cast<int32_t>(offsetof(WordWrapState_t4047764895, ___tagNoParsing_33)); }
	inline bool get_tagNoParsing_33() const { return ___tagNoParsing_33; }
	inline bool* get_address_of_tagNoParsing_33() { return &___tagNoParsing_33; }
	inline void set_tagNoParsing_33(bool value)
	{
		___tagNoParsing_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: TMPro.WordWrapState
struct WordWrapState_t4047764895_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxAscender_10;
	float ___maxDescender_11;
	float ___maxLineAscender_12;
	float ___maxLineDescender_13;
	float ___previousLineAscender_14;
	float ___xAdvance_15;
	float ___preferredWidth_16;
	float ___preferredHeight_17;
	float ___maxFontScale_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___currentFontSize_23;
	float ___baselineOffset_24;
	float ___lineOffset_25;
	TMP_TextInfo_t270066265 * ___textInfo_26;
	TMP_LineInfo_t2462355872_marshaled_pinvoke ___lineInfo_27;
	Color32_t598853688_marshaled_pinvoke ___vertexColor_28;
	TMP_XmlTagStack_1_t1808196333  ___colorStack_29;
	TMP_XmlTagStack_1_t1206294321  ___sizeStack_30;
	TMP_XmlTagStack_1_t2363181145  ___styleStack_31;
	Extents_t2060714539_marshaled_pinvoke ___meshExtents_32;
	int32_t ___tagNoParsing_33;
};
// Native definition for marshalling of: TMPro.WordWrapState
struct WordWrapState_t4047764895_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxAscender_10;
	float ___maxDescender_11;
	float ___maxLineAscender_12;
	float ___maxLineDescender_13;
	float ___previousLineAscender_14;
	float ___xAdvance_15;
	float ___preferredWidth_16;
	float ___preferredHeight_17;
	float ___maxFontScale_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___currentFontSize_23;
	float ___baselineOffset_24;
	float ___lineOffset_25;
	TMP_TextInfo_t270066265 * ___textInfo_26;
	TMP_LineInfo_t2462355872_marshaled_com ___lineInfo_27;
	Color32_t598853688_marshaled_com ___vertexColor_28;
	TMP_XmlTagStack_1_t1808196333  ___colorStack_29;
	TMP_XmlTagStack_1_t1206294321  ___sizeStack_30;
	TMP_XmlTagStack_1_t2363181145  ___styleStack_31;
	Extents_t2060714539_marshaled_com ___meshExtents_32;
	int32_t ___tagNoParsing_33;
};
