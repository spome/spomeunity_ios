﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CurrentScreen/<LoginCheckProgress>c__Iterator6
struct U3CLoginCheckProgressU3Ec__Iterator6_t4282206053;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CurrentScreen/<LoginCheckProgress>c__Iterator6::.ctor()
extern "C"  void U3CLoginCheckProgressU3Ec__Iterator6__ctor_m2835064534 (U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CurrentScreen/<LoginCheckProgress>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoginCheckProgressU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2942237702 (U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CurrentScreen/<LoginCheckProgress>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoginCheckProgressU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m3106261914 (U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CurrentScreen/<LoginCheckProgress>c__Iterator6::MoveNext()
extern "C"  bool U3CLoginCheckProgressU3Ec__Iterator6_MoveNext_m3935108166 (U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen/<LoginCheckProgress>c__Iterator6::Dispose()
extern "C"  void U3CLoginCheckProgressU3Ec__Iterator6_Dispose_m1386034579 (U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen/<LoginCheckProgress>c__Iterator6::Reset()
extern "C"  void U3CLoginCheckProgressU3Ec__Iterator6_Reset_m481497475 (U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
