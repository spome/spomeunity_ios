﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t3603375195;
// UnityEngine.Material
struct Material_t3870600107;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t3950887807;
// TMPro.InlineGraphicManager
struct InlineGraphicManager_t3583857972;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate2847075725.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"

// System.Void TMPro.TextMeshProUGUI::.ctor()
extern "C"  void TextMeshProUGUI__ctor_m3087744556 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::Awake()
extern "C"  void TextMeshProUGUI_Awake_m3325349775 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::OnEnable()
extern "C"  void TextMeshProUGUI_OnEnable_m3755067866 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::OnDisable()
extern "C"  void TextMeshProUGUI_OnDisable_m883924115 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::OnDestroy()
extern "C"  void TextMeshProUGUI_OnDestroy_m3061456165 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::LoadFontAsset()
extern "C"  void TextMeshProUGUI_LoadFontAsset_m204543269 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::UpdateEnvMapMatrix()
extern "C"  void TextMeshProUGUI_UpdateEnvMapMatrix_m3111839825 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::EnableMasking()
extern "C"  void TextMeshProUGUI_EnableMasking_m2839763965 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::DisableMasking()
extern "C"  void TextMeshProUGUI_DisableMasking_m1119928006 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::UpdateMask()
extern "C"  void TextMeshProUGUI_UpdateMask_m3075641965 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::SetFontMaterial(UnityEngine.Material)
extern "C"  void TextMeshProUGUI_SetFontMaterial_m2260011012 (TextMeshProUGUI_t3603375195 * __this, Material_t3870600107 * ___mat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::SetFontSharedMaterial(UnityEngine.Material)
extern "C"  void TextMeshProUGUI_SetFontSharedMaterial_m156257865 (TextMeshProUGUI_t3603375195 * __this, Material_t3870600107 * ___mat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::SetFontBaseMaterial(UnityEngine.Material)
extern "C"  void TextMeshProUGUI_SetFontBaseMaterial_m2085202485 (TextMeshProUGUI_t3603375195 * __this, Material_t3870600107 * ___mat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::SetOutlineThickness(System.Single)
extern "C"  void TextMeshProUGUI_SetOutlineThickness_m3140782605 (TextMeshProUGUI_t3603375195 * __this, float ___thickness0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::SetFaceColor(UnityEngine.Color32)
extern "C"  void TextMeshProUGUI_SetFaceColor_m3206393995 (TextMeshProUGUI_t3603375195 * __this, Color32_t598853688  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::SetOutlineColor(UnityEngine.Color32)
extern "C"  void TextMeshProUGUI_SetOutlineColor_m4151194170 (TextMeshProUGUI_t3603375195 * __this, Color32_t598853688  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material TMPro.TextMeshProUGUI::CreateMaterialInstance(UnityEngine.Material)
extern "C"  Material_t3870600107 * TextMeshProUGUI_CreateMaterialInstance_m2559078579 (TextMeshProUGUI_t3603375195 * __this, Material_t3870600107 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::SetShaderDepth()
extern "C"  void TextMeshProUGUI_SetShaderDepth_m3386971636 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::SetCulling()
extern "C"  void TextMeshProUGUI_SetCulling_m909055814 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::SetPerspectiveCorrection()
extern "C"  void TextMeshProUGUI_SetPerspectiveCorrection_m876472720 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TextMeshProUGUI::GetPaddingForMaterial(UnityEngine.Material)
extern "C"  float TextMeshProUGUI_GetPaddingForMaterial_m725579117 (TextMeshProUGUI_t3603375195 * __this, Material_t3870600107 * ___mat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TextMeshProUGUI::GetPaddingForMaterial()
extern "C"  float TextMeshProUGUI_GetPaddingForMaterial_m2044302859 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::SetMeshArrays(System.Int32)
extern "C"  void TextMeshProUGUI_SetMeshArrays_m466319460 (TextMeshProUGUI_t3603375195 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TextMeshProUGUI::SetArraySizes(System.Int32[])
extern "C"  int32_t TextMeshProUGUI_SetArraySizes_m1766539714 (TextMeshProUGUI_t3603375195 * __this, Int32U5BU5D_t3230847821* ___chars0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::ComputeMarginSize()
extern "C"  void TextMeshProUGUI_ComputeMarginSize_m2478807728 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::OnDidApplyAnimationProperties()
extern "C"  void TextMeshProUGUI_OnDidApplyAnimationProperties_m1228946899 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::OnTransformParentChanged()
extern "C"  void TextMeshProUGUI_OnTransformParentChanged_m3350089781 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::OnRectTransformDimensionsChange()
extern "C"  void TextMeshProUGUI_OnRectTransformDimensionsChange_m3515706064 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::LateUpdate()
extern "C"  void TextMeshProUGUI_LateUpdate_m657852327 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::OnPreRenderCanvas()
extern "C"  void TextMeshProUGUI_OnPreRenderCanvas_m1028660796 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::GenerateTextMesh()
extern "C"  void TextMeshProUGUI_GenerateTextMesh_m3084213415 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::SaveSpriteVertexInfo(UnityEngine.Color32)
extern "C"  void TextMeshProUGUI_SaveSpriteVertexInfo_m1978927515 (TextMeshProUGUI_t3603375195 * __this, Color32_t598853688  ___vertexColor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::FillSpriteVertexBuffers(System.Int32,System.Int32)
extern "C"  void TextMeshProUGUI_FillSpriteVertexBuffers_m1186646613 (TextMeshProUGUI_t3603375195 * __this, int32_t ___i0, int32_t ___spriteIndex_X41, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] TMPro.TextMeshProUGUI::GetTextContainerLocalCorners()
extern "C"  Vector3U5BU5D_t215400611* TextMeshProUGUI_GetTextContainerLocalCorners_m1554398609 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::DrawUnderlineMesh(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32&,System.Single,System.Single,System.Single,UnityEngine.Color32)
extern "C"  void TextMeshProUGUI_DrawUnderlineMesh_m636850226 (TextMeshProUGUI_t3603375195 * __this, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, int32_t* ___index2, float ___startScale3, float ___endScale4, float ___maxScale5, Color32_t598853688  ___underlineColor6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::ClearMesh()
extern "C"  void TextMeshProUGUI_ClearMesh_m3629357604 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::UpdateSDFScale(System.Single,System.Single)
extern "C"  void TextMeshProUGUI_UpdateSDFScale_m994050106 (TextMeshProUGUI_t3603375195 * __this, float ___prevScale0, float ___newScale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::AdjustLineOffset(System.Int32,System.Int32,System.Single)
extern "C"  void TextMeshProUGUI_AdjustLineOffset_m556647325 (TextMeshProUGUI_t3603375195 * __this, int32_t ___startIndex0, int32_t ___endIndex1, float ___offset2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material TMPro.TextMeshProUGUI::get_fontSharedMaterial()
extern "C"  Material_t3870600107 * TextMeshProUGUI_get_fontSharedMaterial_m2447436701 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::set_fontSharedMaterial(UnityEngine.Material)
extern "C"  void TextMeshProUGUI_set_fontSharedMaterial_m165498994 (TextMeshProUGUI_t3603375195 * __this, Material_t3870600107 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh TMPro.TextMeshProUGUI::get_mesh()
extern "C"  Mesh_t4241756145 * TextMeshProUGUI_get_mesh_m1964769333 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CanvasRenderer TMPro.TextMeshProUGUI::get_canvasRenderer()
extern "C"  CanvasRenderer_t3950887807 * TextMeshProUGUI_get_canvasRenderer_m750662929 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.InlineGraphicManager TMPro.TextMeshProUGUI::get_inlineGraphicManager()
extern "C"  InlineGraphicManager_t3583857972 * TextMeshProUGUI_get_inlineGraphicManager_m284881722 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds TMPro.TextMeshProUGUI::get_bounds()
extern "C"  Bounds_t2711641849  TextMeshProUGUI_get_bounds_m2975254917 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::CalculateLayoutInputHorizontal()
extern "C"  void TextMeshProUGUI_CalculateLayoutInputHorizontal_m617831062 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::CalculateLayoutInputVertical()
extern "C"  void TextMeshProUGUI_CalculateLayoutInputVertical_m552636840 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::SetVerticesDirty()
extern "C"  void TextMeshProUGUI_SetVerticesDirty_m880486255 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::SetLayoutDirty()
extern "C"  void TextMeshProUGUI_SetLayoutDirty_m2650938462 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::Rebuild(UnityEngine.UI.CanvasUpdate)
extern "C"  void TextMeshProUGUI_Rebuild_m2648107369 (TextMeshProUGUI_t3603375195 * __this, int32_t ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 TMPro.TextMeshProUGUI::get_maskOffset()
extern "C"  Vector4_t4282066567  TextMeshProUGUI_get_maskOffset_m1546896935 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::set_maskOffset(UnityEngine.Vector4)
extern "C"  void TextMeshProUGUI_set_maskOffset_m1803631396 (TextMeshProUGUI_t3603375195 * __this, Vector4_t4282066567  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::RecalculateClipping()
extern "C"  void TextMeshProUGUI_RecalculateClipping_m3048571583 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::RecalculateMasking()
extern "C"  void TextMeshProUGUI_RecalculateMasking_m2197409787 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::UpdateMeshPadding()
extern "C"  void TextMeshProUGUI_UpdateMeshPadding_m4015104997 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::ForceMeshUpdate()
extern "C"  void TextMeshProUGUI_ForceMeshUpdate_m1789221419 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshProUGUI::UpdateFontAsset()
extern "C"  void TextMeshProUGUI_UpdateFontAsset_m2752984866 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TextMeshProUGUI::UnityEngine.UI.ILayoutElement.get_minWidth()
extern "C"  float TextMeshProUGUI_UnityEngine_UI_ILayoutElement_get_minWidth_m2719076139 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TextMeshProUGUI::UnityEngine.UI.ILayoutElement.get_flexibleWidth()
extern "C"  float TextMeshProUGUI_UnityEngine_UI_ILayoutElement_get_flexibleWidth_m3084196902 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TextMeshProUGUI::UnityEngine.UI.ILayoutElement.get_minHeight()
extern "C"  float TextMeshProUGUI_UnityEngine_UI_ILayoutElement_get_minHeight_m3190796036 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TextMeshProUGUI::UnityEngine.UI.ILayoutElement.get_flexibleHeight()
extern "C"  float TextMeshProUGUI_UnityEngine_UI_ILayoutElement_get_flexibleHeight_m1624637801 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TextMeshProUGUI::UnityEngine.UI.ILayoutElement.get_layoutPriority()
extern "C"  int32_t TextMeshProUGUI_UnityEngine_UI_ILayoutElement_get_layoutPriority_m4013556099 (TextMeshProUGUI_t3603375195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
