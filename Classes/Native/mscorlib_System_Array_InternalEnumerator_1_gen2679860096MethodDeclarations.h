﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2679860096.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Messages3897517420.h"

// System.Void System.Array/InternalEnumerator`1<Messages>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2426012541_gshared (InternalEnumerator_1_t2679860096 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2426012541(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2679860096 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2426012541_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Messages>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2432499459_gshared (InternalEnumerator_1_t2679860096 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2432499459(__this, method) ((  void (*) (InternalEnumerator_1_t2679860096 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2432499459_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Messages>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3522389999_gshared (InternalEnumerator_1_t2679860096 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3522389999(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2679860096 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3522389999_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Messages>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3282130324_gshared (InternalEnumerator_1_t2679860096 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3282130324(__this, method) ((  void (*) (InternalEnumerator_1_t2679860096 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3282130324_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Messages>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2448809327_gshared (InternalEnumerator_1_t2679860096 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2448809327(__this, method) ((  bool (*) (InternalEnumerator_1_t2679860096 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2448809327_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Messages>::get_Current()
extern "C"  Messages_t3897517420  InternalEnumerator_1_get_Current_m1784717508_gshared (InternalEnumerator_1_t2679860096 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1784717508(__this, method) ((  Messages_t3897517420  (*) (InternalEnumerator_1_t2679860096 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1784717508_gshared)(__this, method)
