﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu3528606100MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Action>::.ctor(System.Collections.Generic.LinkedList`1<T>)
#define Enumerator__ctor_m1058112626(__this, ___parent0, method) ((  void (*) (Enumerator_t3129023627 *, LinkedList_1_t1645869067 *, const MethodInfo*))Enumerator__ctor_m857368315_gshared)(__this, ___parent0, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Action>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3874822565(__this, method) ((  Il2CppObject * (*) (Enumerator_t3129023627 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1753810300_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Action>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3885209081(__this, method) ((  void (*) (Enumerator_t3129023627 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4062113552_gshared)(__this, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Action>::get_Current()
#define Enumerator_get_Current_m2317355854(__this, method) ((  Action_t3771233898 * (*) (Enumerator_t3129023627 *, const MethodInfo*))Enumerator_get_Current_m1124073047_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Action>::MoveNext()
#define Enumerator_MoveNext_m2849760721(__this, method) ((  bool (*) (Enumerator_t3129023627 *, const MethodInfo*))Enumerator_MoveNext_m2358966120_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Action>::Dispose()
#define Enumerator_Dispose_m3613555422(__this, method) ((  void (*) (Enumerator_t3129023627 *, const MethodInfo*))Enumerator_Dispose_m272587367_gshared)(__this, method)
