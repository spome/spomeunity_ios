﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GreatmanMessage/<CheckProgress>c__Iterator12
struct U3CCheckProgressU3Ec__Iterator12_t3998861986;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GreatmanMessage/<CheckProgress>c__Iterator12::.ctor()
extern "C"  void U3CCheckProgressU3Ec__Iterator12__ctor_m2740756025 (U3CCheckProgressU3Ec__Iterator12_t3998861986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GreatmanMessage/<CheckProgress>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckProgressU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2315462083 (U3CCheckProgressU3Ec__Iterator12_t3998861986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GreatmanMessage/<CheckProgress>c__Iterator12::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckProgressU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m4081520471 (U3CCheckProgressU3Ec__Iterator12_t3998861986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GreatmanMessage/<CheckProgress>c__Iterator12::MoveNext()
extern "C"  bool U3CCheckProgressU3Ec__Iterator12_MoveNext_m1965523715 (U3CCheckProgressU3Ec__Iterator12_t3998861986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreatmanMessage/<CheckProgress>c__Iterator12::Dispose()
extern "C"  void U3CCheckProgressU3Ec__Iterator12_Dispose_m949870646 (U3CCheckProgressU3Ec__Iterator12_t3998861986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreatmanMessage/<CheckProgress>c__Iterator12::Reset()
extern "C"  void U3CCheckProgressU3Ec__Iterator12_Reset_m387188966 (U3CCheckProgressU3Ec__Iterator12_t3998861986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
