﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpotMessageGet/<MessageGetTakeJson>c__AnonStorey28
struct U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019;

#include "codegen/il2cpp-codegen.h"

// System.Void SpotMessageGet/<MessageGetTakeJson>c__AnonStorey28::.ctor()
extern "C"  void U3CMessageGetTakeJsonU3Ec__AnonStorey28__ctor_m3432855312 (U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpotMessageGet/<MessageGetTakeJson>c__AnonStorey28::<>m__5()
extern "C"  void U3CMessageGetTakeJsonU3Ec__AnonStorey28_U3CU3Em__5_m3275208734 (U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
