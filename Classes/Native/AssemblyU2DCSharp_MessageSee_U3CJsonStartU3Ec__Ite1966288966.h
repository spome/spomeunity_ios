﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Object
struct Il2CppObject;
// MessageSee
struct MessageSee_t302730988;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_CurrentState_MemberStates3108678483.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageSee/<JsonStart>c__Iterator1C
struct  U3CJsonStartU3Ec__Iterator1C_t1966288966  : public Il2CppObject
{
public:
	// System.String MessageSee/<JsonStart>c__Iterator1C::<Url>__0
	String_t* ___U3CUrlU3E__0_0;
	// CurrentState/MemberStates MessageSee/<JsonStart>c__Iterator1C::MemberState
	int32_t ___MemberState_1;
	// UnityEngine.WWW MessageSee/<JsonStart>c__Iterator1C::<www>__1
	WWW_t3134621005 * ___U3CwwwU3E__1_2;
	// System.Int32 MessageSee/<JsonStart>c__Iterator1C::$PC
	int32_t ___U24PC_3;
	// System.Object MessageSee/<JsonStart>c__Iterator1C::$current
	Il2CppObject * ___U24current_4;
	// CurrentState/MemberStates MessageSee/<JsonStart>c__Iterator1C::<$>MemberState
	int32_t ___U3CU24U3EMemberState_5;
	// MessageSee MessageSee/<JsonStart>c__Iterator1C::<>f__this
	MessageSee_t302730988 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_U3CUrlU3E__0_0() { return static_cast<int32_t>(offsetof(U3CJsonStartU3Ec__Iterator1C_t1966288966, ___U3CUrlU3E__0_0)); }
	inline String_t* get_U3CUrlU3E__0_0() const { return ___U3CUrlU3E__0_0; }
	inline String_t** get_address_of_U3CUrlU3E__0_0() { return &___U3CUrlU3E__0_0; }
	inline void set_U3CUrlU3E__0_0(String_t* value)
	{
		___U3CUrlU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUrlU3E__0_0, value);
	}

	inline static int32_t get_offset_of_MemberState_1() { return static_cast<int32_t>(offsetof(U3CJsonStartU3Ec__Iterator1C_t1966288966, ___MemberState_1)); }
	inline int32_t get_MemberState_1() const { return ___MemberState_1; }
	inline int32_t* get_address_of_MemberState_1() { return &___MemberState_1; }
	inline void set_MemberState_1(int32_t value)
	{
		___MemberState_1 = value;
	}

	inline static int32_t get_offset_of_U3CwwwU3E__1_2() { return static_cast<int32_t>(offsetof(U3CJsonStartU3Ec__Iterator1C_t1966288966, ___U3CwwwU3E__1_2)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__1_2() const { return ___U3CwwwU3E__1_2; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__1_2() { return &___U3CwwwU3E__1_2; }
	inline void set_U3CwwwU3E__1_2(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CJsonStartU3Ec__Iterator1C_t1966288966, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CJsonStartU3Ec__Iterator1C_t1966288966, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EMemberState_5() { return static_cast<int32_t>(offsetof(U3CJsonStartU3Ec__Iterator1C_t1966288966, ___U3CU24U3EMemberState_5)); }
	inline int32_t get_U3CU24U3EMemberState_5() const { return ___U3CU24U3EMemberState_5; }
	inline int32_t* get_address_of_U3CU24U3EMemberState_5() { return &___U3CU24U3EMemberState_5; }
	inline void set_U3CU24U3EMemberState_5(int32_t value)
	{
		___U3CU24U3EMemberState_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CJsonStartU3Ec__Iterator1C_t1966288966, ___U3CU3Ef__this_6)); }
	inline MessageSee_t302730988 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline MessageSee_t302730988 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(MessageSee_t302730988 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
