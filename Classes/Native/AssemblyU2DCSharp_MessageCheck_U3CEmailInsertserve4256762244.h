﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t461342257;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Object
struct Il2CppObject;
// MessageCheck
struct MessageCheck_t3146986849;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageCheck/<EmailInsertserver>c__Iterator18
struct  U3CEmailInsertserverU3Ec__Iterator18_t4256762244  : public Il2CppObject
{
public:
	// UnityEngine.WWWForm MessageCheck/<EmailInsertserver>c__Iterator18::<form>__0
	WWWForm_t461342257 * ___U3CformU3E__0_0;
	// System.String MessageCheck/<EmailInsertserver>c__Iterator18::title
	String_t* ___title_1;
	// System.String MessageCheck/<EmailInsertserver>c__Iterator18::email
	String_t* ___email_2;
	// System.String MessageCheck/<EmailInsertserver>c__Iterator18::customid
	String_t* ___customid_3;
	// System.String MessageCheck/<EmailInsertserver>c__Iterator18::messageid
	String_t* ___messageid_4;
	// UnityEngine.WWW MessageCheck/<EmailInsertserver>c__Iterator18::<www>__1
	WWW_t3134621005 * ___U3CwwwU3E__1_5;
	// System.Int32 MessageCheck/<EmailInsertserver>c__Iterator18::$PC
	int32_t ___U24PC_6;
	// System.Object MessageCheck/<EmailInsertserver>c__Iterator18::$current
	Il2CppObject * ___U24current_7;
	// System.String MessageCheck/<EmailInsertserver>c__Iterator18::<$>title
	String_t* ___U3CU24U3Etitle_8;
	// System.String MessageCheck/<EmailInsertserver>c__Iterator18::<$>email
	String_t* ___U3CU24U3Eemail_9;
	// System.String MessageCheck/<EmailInsertserver>c__Iterator18::<$>customid
	String_t* ___U3CU24U3Ecustomid_10;
	// System.String MessageCheck/<EmailInsertserver>c__Iterator18::<$>messageid
	String_t* ___U3CU24U3Emessageid_11;
	// MessageCheck MessageCheck/<EmailInsertserver>c__Iterator18::<>f__this
	MessageCheck_t3146986849 * ___U3CU3Ef__this_12;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CEmailInsertserverU3Ec__Iterator18_t4256762244, ___U3CformU3E__0_0)); }
	inline WWWForm_t461342257 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t461342257 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t461342257 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CformU3E__0_0, value);
	}

	inline static int32_t get_offset_of_title_1() { return static_cast<int32_t>(offsetof(U3CEmailInsertserverU3Ec__Iterator18_t4256762244, ___title_1)); }
	inline String_t* get_title_1() const { return ___title_1; }
	inline String_t** get_address_of_title_1() { return &___title_1; }
	inline void set_title_1(String_t* value)
	{
		___title_1 = value;
		Il2CppCodeGenWriteBarrier(&___title_1, value);
	}

	inline static int32_t get_offset_of_email_2() { return static_cast<int32_t>(offsetof(U3CEmailInsertserverU3Ec__Iterator18_t4256762244, ___email_2)); }
	inline String_t* get_email_2() const { return ___email_2; }
	inline String_t** get_address_of_email_2() { return &___email_2; }
	inline void set_email_2(String_t* value)
	{
		___email_2 = value;
		Il2CppCodeGenWriteBarrier(&___email_2, value);
	}

	inline static int32_t get_offset_of_customid_3() { return static_cast<int32_t>(offsetof(U3CEmailInsertserverU3Ec__Iterator18_t4256762244, ___customid_3)); }
	inline String_t* get_customid_3() const { return ___customid_3; }
	inline String_t** get_address_of_customid_3() { return &___customid_3; }
	inline void set_customid_3(String_t* value)
	{
		___customid_3 = value;
		Il2CppCodeGenWriteBarrier(&___customid_3, value);
	}

	inline static int32_t get_offset_of_messageid_4() { return static_cast<int32_t>(offsetof(U3CEmailInsertserverU3Ec__Iterator18_t4256762244, ___messageid_4)); }
	inline String_t* get_messageid_4() const { return ___messageid_4; }
	inline String_t** get_address_of_messageid_4() { return &___messageid_4; }
	inline void set_messageid_4(String_t* value)
	{
		___messageid_4 = value;
		Il2CppCodeGenWriteBarrier(&___messageid_4, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__1_5() { return static_cast<int32_t>(offsetof(U3CEmailInsertserverU3Ec__Iterator18_t4256762244, ___U3CwwwU3E__1_5)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__1_5() const { return ___U3CwwwU3E__1_5; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__1_5() { return &___U3CwwwU3E__1_5; }
	inline void set_U3CwwwU3E__1_5(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__1_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CEmailInsertserverU3Ec__Iterator18_t4256762244, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CEmailInsertserverU3Ec__Iterator18_t4256762244, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Etitle_8() { return static_cast<int32_t>(offsetof(U3CEmailInsertserverU3Ec__Iterator18_t4256762244, ___U3CU24U3Etitle_8)); }
	inline String_t* get_U3CU24U3Etitle_8() const { return ___U3CU24U3Etitle_8; }
	inline String_t** get_address_of_U3CU24U3Etitle_8() { return &___U3CU24U3Etitle_8; }
	inline void set_U3CU24U3Etitle_8(String_t* value)
	{
		___U3CU24U3Etitle_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Etitle_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eemail_9() { return static_cast<int32_t>(offsetof(U3CEmailInsertserverU3Ec__Iterator18_t4256762244, ___U3CU24U3Eemail_9)); }
	inline String_t* get_U3CU24U3Eemail_9() const { return ___U3CU24U3Eemail_9; }
	inline String_t** get_address_of_U3CU24U3Eemail_9() { return &___U3CU24U3Eemail_9; }
	inline void set_U3CU24U3Eemail_9(String_t* value)
	{
		___U3CU24U3Eemail_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eemail_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecustomid_10() { return static_cast<int32_t>(offsetof(U3CEmailInsertserverU3Ec__Iterator18_t4256762244, ___U3CU24U3Ecustomid_10)); }
	inline String_t* get_U3CU24U3Ecustomid_10() const { return ___U3CU24U3Ecustomid_10; }
	inline String_t** get_address_of_U3CU24U3Ecustomid_10() { return &___U3CU24U3Ecustomid_10; }
	inline void set_U3CU24U3Ecustomid_10(String_t* value)
	{
		___U3CU24U3Ecustomid_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ecustomid_10, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Emessageid_11() { return static_cast<int32_t>(offsetof(U3CEmailInsertserverU3Ec__Iterator18_t4256762244, ___U3CU24U3Emessageid_11)); }
	inline String_t* get_U3CU24U3Emessageid_11() const { return ___U3CU24U3Emessageid_11; }
	inline String_t** get_address_of_U3CU24U3Emessageid_11() { return &___U3CU24U3Emessageid_11; }
	inline void set_U3CU24U3Emessageid_11(String_t* value)
	{
		___U3CU24U3Emessageid_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Emessageid_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_12() { return static_cast<int32_t>(offsetof(U3CEmailInsertserverU3Ec__Iterator18_t4256762244, ___U3CU3Ef__this_12)); }
	inline MessageCheck_t3146986849 * get_U3CU3Ef__this_12() const { return ___U3CU3Ef__this_12; }
	inline MessageCheck_t3146986849 ** get_address_of_U3CU3Ef__this_12() { return &___U3CU3Ef__this_12; }
	inline void set_U3CU3Ef__this_12(MessageCheck_t3146986849 * value)
	{
		___U3CU3Ef__this_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
