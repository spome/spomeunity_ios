﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.String>
struct Action_1_t403047693;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "mscorlib_System_IntPtr4010401971.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebViewObject
struct  WebViewObject_t388577433  : public MonoBehaviour_t667441552
{
public:
	// System.Action`1<System.String> WebViewObject::callback
	Action_1_t403047693 * ___callback_2;
	// System.Boolean WebViewObject::visibility
	bool ___visibility_3;
	// System.IntPtr WebViewObject::webView
	IntPtr_t ___webView_4;

public:
	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(WebViewObject_t388577433, ___callback_2)); }
	inline Action_1_t403047693 * get_callback_2() const { return ___callback_2; }
	inline Action_1_t403047693 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_1_t403047693 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier(&___callback_2, value);
	}

	inline static int32_t get_offset_of_visibility_3() { return static_cast<int32_t>(offsetof(WebViewObject_t388577433, ___visibility_3)); }
	inline bool get_visibility_3() const { return ___visibility_3; }
	inline bool* get_address_of_visibility_3() { return &___visibility_3; }
	inline void set_visibility_3(bool value)
	{
		___visibility_3 = value;
	}

	inline static int32_t get_offset_of_webView_4() { return static_cast<int32_t>(offsetof(WebViewObject_t388577433, ___webView_4)); }
	inline IntPtr_t get_webView_4() const { return ___webView_4; }
	inline IntPtr_t* get_address_of_webView_4() { return &___webView_4; }
	inline void set_webView_4(IntPtr_t value)
	{
		___webView_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
