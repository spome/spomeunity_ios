﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22990515955.h"
#include "AssemblyU2DCSharp_CurrentState_States2558012761.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<CurrentState/States,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1942999012_gshared (KeyValuePair_2_t2990515955 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1942999012(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2990515955 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m1942999012_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<CurrentState/States,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m465482500_gshared (KeyValuePair_2_t2990515955 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m465482500(__this, method) ((  int32_t (*) (KeyValuePair_2_t2990515955 *, const MethodInfo*))KeyValuePair_2_get_Key_m465482500_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<CurrentState/States,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1775119301_gshared (KeyValuePair_2_t2990515955 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1775119301(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2990515955 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1775119301_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<CurrentState/States,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1848063272_gshared (KeyValuePair_2_t2990515955 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1848063272(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2990515955 *, const MethodInfo*))KeyValuePair_2_get_Value_m1848063272_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<CurrentState/States,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m804525381_gshared (KeyValuePair_2_t2990515955 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m804525381(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2990515955 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m804525381_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<CurrentState/States,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2796541731_gshared (KeyValuePair_2_t2990515955 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2796541731(__this, method) ((  String_t* (*) (KeyValuePair_2_t2990515955 *, const MethodInfo*))KeyValuePair_2_ToString_m2796541731_gshared)(__this, method)
