﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Loading
struct  Loading_t2001303836  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.AndroidJavaObject Loading::_activity
	AndroidJavaObject_t2362096582 * ____activity_2;

public:
	inline static int32_t get_offset_of__activity_2() { return static_cast<int32_t>(offsetof(Loading_t2001303836, ____activity_2)); }
	inline AndroidJavaObject_t2362096582 * get__activity_2() const { return ____activity_2; }
	inline AndroidJavaObject_t2362096582 ** get_address_of__activity_2() { return &____activity_2; }
	inline void set__activity_2(AndroidJavaObject_t2362096582 * value)
	{
		____activity_2 = value;
		Il2CppCodeGenWriteBarrier(&____activity_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
