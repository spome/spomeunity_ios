﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// MessageSee
struct MessageSee_t302730988;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageSee/<TakeJson>c__AnonStorey26
struct  U3CTakeJsonU3Ec__AnonStorey26_t291817749  : public Il2CppObject
{
public:
	// UnityEngine.GameObject MessageSee/<TakeJson>c__AnonStorey26::Btn
	GameObject_t3674682005 * ___Btn_0;
	// MessageSee MessageSee/<TakeJson>c__AnonStorey26::<>f__this
	MessageSee_t302730988 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_Btn_0() { return static_cast<int32_t>(offsetof(U3CTakeJsonU3Ec__AnonStorey26_t291817749, ___Btn_0)); }
	inline GameObject_t3674682005 * get_Btn_0() const { return ___Btn_0; }
	inline GameObject_t3674682005 ** get_address_of_Btn_0() { return &___Btn_0; }
	inline void set_Btn_0(GameObject_t3674682005 * value)
	{
		___Btn_0 = value;
		Il2CppCodeGenWriteBarrier(&___Btn_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CTakeJsonU3Ec__AnonStorey26_t291817749, ___U3CU3Ef__this_1)); }
	inline MessageSee_t302730988 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline MessageSee_t302730988 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(MessageSee_t302730988 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
