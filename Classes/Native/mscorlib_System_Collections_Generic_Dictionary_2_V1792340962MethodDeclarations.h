﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,System.Object>
struct ValueCollection_t1792340962;
// System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>
struct Dictionary_2_t3091735249;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1023568657.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1155904981_gshared (ValueCollection_t1792340962 * __this, Dictionary_2_t3091735249 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1155904981(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1792340962 *, Dictionary_2_t3091735249 *, const MethodInfo*))ValueCollection__ctor_m1155904981_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1984952253_gshared (ValueCollection_t1792340962 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1984952253(__this, ___item0, method) ((  void (*) (ValueCollection_t1792340962 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1984952253_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m71912198_gshared (ValueCollection_t1792340962 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m71912198(__this, method) ((  void (*) (ValueCollection_t1792340962 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m71912198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2971646985_gshared (ValueCollection_t1792340962 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2971646985(__this, ___item0, method) ((  bool (*) (ValueCollection_t1792340962 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2971646985_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1738162926_gshared (ValueCollection_t1792340962 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1738162926(__this, ___item0, method) ((  bool (*) (ValueCollection_t1792340962 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1738162926_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1114479828_gshared (ValueCollection_t1792340962 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1114479828(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1792340962 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1114479828_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3338296458_gshared (ValueCollection_t1792340962 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3338296458(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1792340962 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3338296458_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4075324549_gshared (ValueCollection_t1792340962 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4075324549(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1792340962 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4075324549_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1246822844_gshared (ValueCollection_t1792340962 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1246822844(__this, method) ((  bool (*) (ValueCollection_t1792340962 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1246822844_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3101622684_gshared (ValueCollection_t1792340962 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3101622684(__this, method) ((  bool (*) (ValueCollection_t1792340962 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3101622684_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3106526280_gshared (ValueCollection_t1792340962 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3106526280(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1792340962 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3106526280_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2635984092_gshared (ValueCollection_t1792340962 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m2635984092(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1792340962 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2635984092_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1023568657  ValueCollection_GetEnumerator_m3505452287_gshared (ValueCollection_t1792340962 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3505452287(__this, method) ((  Enumerator_t1023568657  (*) (ValueCollection_t1792340962 *, const MethodInfo*))ValueCollection_GetEnumerator_m3505452287_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3710668130_gshared (ValueCollection_t1792340962 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3710668130(__this, method) ((  int32_t (*) (ValueCollection_t1792340962 *, const MethodInfo*))ValueCollection_get_Count_m3710668130_gshared)(__this, method)
