﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.InlineGraphicManager/<GetSpriteIndexByHashCode>c__AnonStorey29
struct  U3CGetSpriteIndexByHashCodeU3Ec__AnonStorey29_t1993853662  : public Il2CppObject
{
public:
	// System.Int32 TMPro.InlineGraphicManager/<GetSpriteIndexByHashCode>c__AnonStorey29::hashCode
	int32_t ___hashCode_0;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(U3CGetSpriteIndexByHashCodeU3Ec__AnonStorey29_t1993853662, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
