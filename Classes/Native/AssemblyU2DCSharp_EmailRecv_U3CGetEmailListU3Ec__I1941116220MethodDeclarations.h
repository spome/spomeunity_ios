﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EmailRecv/<GetEmailList>c__IteratorB
struct U3CGetEmailListU3Ec__IteratorB_t1941116220;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EmailRecv/<GetEmailList>c__IteratorB::.ctor()
extern "C"  void U3CGetEmailListU3Ec__IteratorB__ctor_m844904671 (U3CGetEmailListU3Ec__IteratorB_t1941116220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EmailRecv/<GetEmailList>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetEmailListU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2337549277 (U3CGetEmailListU3Ec__IteratorB_t1941116220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EmailRecv/<GetEmailList>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEmailListU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m3616246129 (U3CGetEmailListU3Ec__IteratorB_t1941116220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EmailRecv/<GetEmailList>c__IteratorB::MoveNext()
extern "C"  bool U3CGetEmailListU3Ec__IteratorB_MoveNext_m646779549 (U3CGetEmailListU3Ec__IteratorB_t1941116220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmailRecv/<GetEmailList>c__IteratorB::Dispose()
extern "C"  void U3CGetEmailListU3Ec__IteratorB_Dispose_m102852956 (U3CGetEmailListU3Ec__IteratorB_t1941116220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmailRecv/<GetEmailList>c__IteratorB::Reset()
extern "C"  void U3CGetEmailListU3Ec__IteratorB_Reset_m2786304908 (U3CGetEmailListU3Ec__IteratorB_t1941116220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
