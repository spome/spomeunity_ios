﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PublicSpotMessage
struct PublicSpotMessage_t3890192988;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void PublicSpotMessage::.ctor(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PublicSpotMessage__ctor_m1772971704 (PublicSpotMessage_t3890192988 * __this, int32_t ___messageindex0, String_t* ___message_id1, String_t* ___type2, String_t* ___title3, String_t* ___realfilename4, String_t* ___pref5, String_t* ___addr016, String_t* ___addr027, String_t* ___latitude8, String_t* ___longtitude9, String_t* ___create_data10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
