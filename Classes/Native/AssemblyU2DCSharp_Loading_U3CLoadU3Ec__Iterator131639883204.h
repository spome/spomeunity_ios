﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AsyncOperation
struct AsyncOperation_t3699081103;
// System.Object
struct Il2CppObject;
// Loading
struct Loading_t2001303836;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Loading/<Load>c__Iterator13
struct  U3CLoadU3Ec__Iterator13_t1639883204  : public Il2CppObject
{
public:
	// UnityEngine.AsyncOperation Loading/<Load>c__Iterator13::<async>__0
	AsyncOperation_t3699081103 * ___U3CasyncU3E__0_0;
	// UnityEngine.AsyncOperation Loading/<Load>c__Iterator13::<async>__1
	AsyncOperation_t3699081103 * ___U3CasyncU3E__1_1;
	// System.Int32 Loading/<Load>c__Iterator13::$PC
	int32_t ___U24PC_2;
	// System.Object Loading/<Load>c__Iterator13::$current
	Il2CppObject * ___U24current_3;
	// Loading Loading/<Load>c__Iterator13::<>f__this
	Loading_t2001303836 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CasyncU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator13_t1639883204, ___U3CasyncU3E__0_0)); }
	inline AsyncOperation_t3699081103 * get_U3CasyncU3E__0_0() const { return ___U3CasyncU3E__0_0; }
	inline AsyncOperation_t3699081103 ** get_address_of_U3CasyncU3E__0_0() { return &___U3CasyncU3E__0_0; }
	inline void set_U3CasyncU3E__0_0(AsyncOperation_t3699081103 * value)
	{
		___U3CasyncU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CasyncU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CasyncU3E__1_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator13_t1639883204, ___U3CasyncU3E__1_1)); }
	inline AsyncOperation_t3699081103 * get_U3CasyncU3E__1_1() const { return ___U3CasyncU3E__1_1; }
	inline AsyncOperation_t3699081103 ** get_address_of_U3CasyncU3E__1_1() { return &___U3CasyncU3E__1_1; }
	inline void set_U3CasyncU3E__1_1(AsyncOperation_t3699081103 * value)
	{
		___U3CasyncU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CasyncU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator13_t1639883204, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator13_t1639883204, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator13_t1639883204, ___U3CU3Ef__this_4)); }
	inline Loading_t2001303836 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline Loading_t2001303836 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(Loading_t2001303836 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
