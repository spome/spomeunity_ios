﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_TMP_XmlTagStack_1_gen2363181145.h"

// System.Void TMPro.TMP_XmlTagStack`1<System.Int32>::.ctor(T[])
extern "C"  void TMP_XmlTagStack_1__ctor_m3016139512_gshared (TMP_XmlTagStack_1_t2363181145 * __this, Int32U5BU5D_t3230847821* ___tagStack0, const MethodInfo* method);
#define TMP_XmlTagStack_1__ctor_m3016139512(__this, ___tagStack0, method) ((  void (*) (TMP_XmlTagStack_1_t2363181145 *, Int32U5BU5D_t3230847821*, const MethodInfo*))TMP_XmlTagStack_1__ctor_m3016139512_gshared)(__this, ___tagStack0, method)
// System.Void TMPro.TMP_XmlTagStack`1<System.Int32>::Clear()
extern "C"  void TMP_XmlTagStack_1_Clear_m601040815_gshared (TMP_XmlTagStack_1_t2363181145 * __this, const MethodInfo* method);
#define TMP_XmlTagStack_1_Clear_m601040815(__this, method) ((  void (*) (TMP_XmlTagStack_1_t2363181145 *, const MethodInfo*))TMP_XmlTagStack_1_Clear_m601040815_gshared)(__this, method)
// System.Void TMPro.TMP_XmlTagStack`1<System.Int32>::SetDefault(T)
extern "C"  void TMP_XmlTagStack_1_SetDefault_m3063966591_gshared (TMP_XmlTagStack_1_t2363181145 * __this, int32_t ___item0, const MethodInfo* method);
#define TMP_XmlTagStack_1_SetDefault_m3063966591(__this, ___item0, method) ((  void (*) (TMP_XmlTagStack_1_t2363181145 *, int32_t, const MethodInfo*))TMP_XmlTagStack_1_SetDefault_m3063966591_gshared)(__this, ___item0, method)
// System.Void TMPro.TMP_XmlTagStack`1<System.Int32>::Add(T)
extern "C"  void TMP_XmlTagStack_1_Add_m2032866427_gshared (TMP_XmlTagStack_1_t2363181145 * __this, int32_t ___item0, const MethodInfo* method);
#define TMP_XmlTagStack_1_Add_m2032866427(__this, ___item0, method) ((  void (*) (TMP_XmlTagStack_1_t2363181145 *, int32_t, const MethodInfo*))TMP_XmlTagStack_1_Add_m2032866427_gshared)(__this, ___item0, method)
// T TMPro.TMP_XmlTagStack`1<System.Int32>::Remove()
extern "C"  int32_t TMP_XmlTagStack_1_Remove_m356276133_gshared (TMP_XmlTagStack_1_t2363181145 * __this, const MethodInfo* method);
#define TMP_XmlTagStack_1_Remove_m356276133(__this, method) ((  int32_t (*) (TMP_XmlTagStack_1_t2363181145 *, const MethodInfo*))TMP_XmlTagStack_1_Remove_m356276133_gshared)(__this, method)
