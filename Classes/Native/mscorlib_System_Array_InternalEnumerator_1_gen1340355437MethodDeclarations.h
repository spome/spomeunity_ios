﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1340355437.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_CurrentState_States2558012761.h"

// System.Void System.Array/InternalEnumerator`1<CurrentState/States>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1080061086_gshared (InternalEnumerator_1_t1340355437 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1080061086(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1340355437 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1080061086_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<CurrentState/States>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2353043522_gshared (InternalEnumerator_1_t1340355437 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2353043522(__this, method) ((  void (*) (InternalEnumerator_1_t1340355437 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2353043522_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<CurrentState/States>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2358095544_gshared (InternalEnumerator_1_t1340355437 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2358095544(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1340355437 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2358095544_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<CurrentState/States>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m815891445_gshared (InternalEnumerator_1_t1340355437 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m815891445(__this, method) ((  void (*) (InternalEnumerator_1_t1340355437 *, const MethodInfo*))InternalEnumerator_1_Dispose_m815891445_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<CurrentState/States>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2449844978_gshared (InternalEnumerator_1_t1340355437 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2449844978(__this, method) ((  bool (*) (InternalEnumerator_1_t1340355437 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2449844978_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<CurrentState/States>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m994846023_gshared (InternalEnumerator_1_t1340355437 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m994846023(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1340355437 *, const MethodInfo*))InternalEnumerator_1_get_Current_m994846023_gshared)(__this, method)
