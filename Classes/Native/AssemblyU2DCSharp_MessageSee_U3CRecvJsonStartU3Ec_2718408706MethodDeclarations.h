﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageSee/<RecvJsonStart>c__Iterator1E
struct U3CRecvJsonStartU3Ec__Iterator1E_t2718408706;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageSee/<RecvJsonStart>c__Iterator1E::.ctor()
extern "C"  void U3CRecvJsonStartU3Ec__Iterator1E__ctor_m2930954665 (U3CRecvJsonStartU3Ec__Iterator1E_t2718408706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageSee/<RecvJsonStart>c__Iterator1E::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRecvJsonStartU3Ec__Iterator1E_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m531611209 (U3CRecvJsonStartU3Ec__Iterator1E_t2718408706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageSee/<RecvJsonStart>c__Iterator1E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRecvJsonStartU3Ec__Iterator1E_System_Collections_IEnumerator_get_Current_m1230074845 (U3CRecvJsonStartU3Ec__Iterator1E_t2718408706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MessageSee/<RecvJsonStart>c__Iterator1E::MoveNext()
extern "C"  bool U3CRecvJsonStartU3Ec__Iterator1E_MoveNext_m3570201515 (U3CRecvJsonStartU3Ec__Iterator1E_t2718408706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSee/<RecvJsonStart>c__Iterator1E::Dispose()
extern "C"  void U3CRecvJsonStartU3Ec__Iterator1E_Dispose_m3342137254 (U3CRecvJsonStartU3Ec__Iterator1E_t2718408706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSee/<RecvJsonStart>c__Iterator1E::Reset()
extern "C"  void U3CRecvJsonStartU3Ec__Iterator1E_Reset_m577387606 (U3CRecvJsonStartU3Ec__Iterator1E_t2718408706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
