﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.MaterialManager/<ReleaseStencilMaterial>c__AnonStorey30
struct U3CReleaseStencilMaterialU3Ec__AnonStorey30_t584774235;
// TMPro.MaterialManager/MaskingMaterial
struct MaskingMaterial_t2640101498;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_MaterialManager_MaskingMat2640101498.h"

// System.Void TMPro.MaterialManager/<ReleaseStencilMaterial>c__AnonStorey30::.ctor()
extern "C"  void U3CReleaseStencilMaterialU3Ec__AnonStorey30__ctor_m3503191408 (U3CReleaseStencilMaterialU3Ec__AnonStorey30_t584774235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.MaterialManager/<ReleaseStencilMaterial>c__AnonStorey30::<>m__E(TMPro.MaterialManager/MaskingMaterial)
extern "C"  bool U3CReleaseStencilMaterialU3Ec__AnonStorey30_U3CU3Em__E_m2183557406 (U3CReleaseStencilMaterialU3Ec__AnonStorey30_t584774235 * __this, MaskingMaterial_t2640101498 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
