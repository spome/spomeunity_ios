﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CurrentScreen
struct CurrentScreen_t3026574117;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CurrentState_States2558012761.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "mscorlib_System_String7231557.h"

// System.Void CurrentScreen::.ctor()
extern "C"  void CurrentScreen__ctor_m1985495014 (CurrentScreen_t3026574117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::.cctor()
extern "C"  void CurrentScreen__cctor_m938707079 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CurrentScreen CurrentScreen::get_Instance()
extern "C"  CurrentScreen_t3026574117 * CurrentScreen_get_Instance_m3161187310 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::Awake()
extern "C"  void CurrentScreen_Awake_m2223100233 (CurrentScreen_t3026574117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::BackButton()
extern "C"  void CurrentScreen_BackButton_m2788205463 (CurrentScreen_t3026574117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::State(CurrentState/States)
extern "C"  void CurrentScreen_State_m3173002564 (CurrentScreen_t3026574117 * __this, int32_t ___Current0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::VisibleFalseState(CurrentState/States)
extern "C"  void CurrentScreen_VisibleFalseState_m2603065973 (CurrentScreen_t3026574117 * __this, int32_t ___Current0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::LoginButton()
extern "C"  void CurrentScreen_LoginButton_m1187927039 (CurrentScreen_t3026574117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CurrentScreen::Loggied()
extern "C"  bool CurrentScreen_Loggied_m1683951613 (CurrentScreen_t3026574117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::MainButton(System.Int32)
extern "C"  void CurrentScreen_MainButton_m841552442 (CurrentScreen_t3026574117 * __this, int32_t ___btn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::OKButton()
extern "C"  void CurrentScreen_OKButton_m2045106860 (CurrentScreen_t3026574117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::SumabomenuButton(System.Int32)
extern "C"  void CurrentScreen_SumabomenuButton_m2687570531 (CurrentScreen_t3026574117 * __this, int32_t ___btn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::PremiumserviceRegist()
extern "C"  void CurrentScreen_PremiumserviceRegist_m1864671026 (CurrentScreen_t3026574117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CurrentScreen::JsonStart()
extern "C"  Il2CppObject * CurrentScreen_JsonStart_m140421542 (CurrentScreen_t3026574117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CurrentScreen::LoginCheckProgress(UnityEngine.WWW)
extern "C"  Il2CppObject * CurrentScreen_LoginCheckProgress_m1704831024 (CurrentScreen_t3026574117 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::TakeJson(System.String)
extern "C"  void CurrentScreen_TakeJson_m27493301 (CurrentScreen_t3026574117 * __this, String_t* ___js0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::LoginScreenButton(System.Int32)
extern "C"  void CurrentScreen_LoginScreenButton_m4268466268 (CurrentScreen_t3026574117 * __this, int32_t ___btn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CurrentScreen::MessageCountGetJsonStart()
extern "C"  Il2CppObject * CurrentScreen_MessageCountGetJsonStart_m2449123042 (CurrentScreen_t3026574117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::MessageCountGetTakeJson(System.String)
extern "C"  void CurrentScreen_MessageCountGetTakeJson_m2221092993 (CurrentScreen_t3026574117 * __this, String_t* ___js0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::MessageWriteButton()
extern "C"  void CurrentScreen_MessageWriteButton_m3529376968 (CurrentScreen_t3026574117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::MemberServiceWriteButton()
extern "C"  void CurrentScreen_MemberServiceWriteButton_m2401017044 (CurrentScreen_t3026574117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::MemberServiceMemberButton()
extern "C"  void CurrentScreen_MemberServiceMemberButton_m1543428235 (CurrentScreen_t3026574117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::MessageRegistrationButton()
extern "C"  void CurrentScreen_MessageRegistrationButton_m3324319414 (CurrentScreen_t3026574117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::Sumabo110Button()
extern "C"  void CurrentScreen_Sumabo110Button_m1567156227 (CurrentScreen_t3026574117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::Sumabo1101Button(System.Int32)
extern "C"  void CurrentScreen_Sumabo1101Button_m2428242981 (CurrentScreen_t3026574117 * __this, int32_t ___btn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::Sumabo1102Button()
extern "C"  void CurrentScreen_Sumabo1102Button_m2394907605 (CurrentScreen_t3026574117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::AccountForgetButton()
extern "C"  void CurrentScreen_AccountForgetButton_m1160876816 (CurrentScreen_t3026574117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::EmailRegister()
extern "C"  void CurrentScreen_EmailRegister_m1311987875 (CurrentScreen_t3026574117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen::onWindowFocusChanged(System.String)
extern "C"  void CurrentScreen_onWindowFocusChanged_m3383064281 (CurrentScreen_t3026574117 * __this, String_t* ___hasFocus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
