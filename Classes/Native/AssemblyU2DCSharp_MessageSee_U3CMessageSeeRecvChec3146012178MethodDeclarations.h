﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageSee/<MessageSeeRecvCheckProgress>c__Iterator1F
struct U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageSee/<MessageSeeRecvCheckProgress>c__Iterator1F::.ctor()
extern "C"  void U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F__ctor_m2471475097 (U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageSee/<MessageSeeRecvCheckProgress>c__Iterator1F::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4282783513 (U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageSee/<MessageSeeRecvCheckProgress>c__Iterator1F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_System_Collections_IEnumerator_get_Current_m835939501 (U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MessageSee/<MessageSeeRecvCheckProgress>c__Iterator1F::MoveNext()
extern "C"  bool U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_MoveNext_m2736698043 (U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSee/<MessageSeeRecvCheckProgress>c__Iterator1F::Dispose()
extern "C"  void U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_Dispose_m4163903894 (U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSee/<MessageSeeRecvCheckProgress>c__Iterator1F::Reset()
extern "C"  void U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_Reset_m117908038 (U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
