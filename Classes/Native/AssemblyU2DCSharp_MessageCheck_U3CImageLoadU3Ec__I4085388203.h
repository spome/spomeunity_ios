﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;
// MessageCheck
struct MessageCheck_t3146986849;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageCheck/<ImageLoad>c__Iterator16
struct  U3CImageLoadU3Ec__Iterator16_t4085388203  : public Il2CppObject
{
public:
	// UnityEngine.Texture2D MessageCheck/<ImageLoad>c__Iterator16::<tex>__0
	Texture2D_t3884108195 * ___U3CtexU3E__0_0;
	// System.String MessageCheck/<ImageLoad>c__Iterator16::file
	String_t* ___file_1;
	// System.Byte[] MessageCheck/<ImageLoad>c__Iterator16::<binaryImageData>__1
	ByteU5BU5D_t4260760469* ___U3CbinaryImageDataU3E__1_2;
	// System.Int32 MessageCheck/<ImageLoad>c__Iterator16::$PC
	int32_t ___U24PC_3;
	// System.Object MessageCheck/<ImageLoad>c__Iterator16::$current
	Il2CppObject * ___U24current_4;
	// System.String MessageCheck/<ImageLoad>c__Iterator16::<$>file
	String_t* ___U3CU24U3Efile_5;
	// MessageCheck MessageCheck/<ImageLoad>c__Iterator16::<>f__this
	MessageCheck_t3146986849 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_U3CtexU3E__0_0() { return static_cast<int32_t>(offsetof(U3CImageLoadU3Ec__Iterator16_t4085388203, ___U3CtexU3E__0_0)); }
	inline Texture2D_t3884108195 * get_U3CtexU3E__0_0() const { return ___U3CtexU3E__0_0; }
	inline Texture2D_t3884108195 ** get_address_of_U3CtexU3E__0_0() { return &___U3CtexU3E__0_0; }
	inline void set_U3CtexU3E__0_0(Texture2D_t3884108195 * value)
	{
		___U3CtexU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtexU3E__0_0, value);
	}

	inline static int32_t get_offset_of_file_1() { return static_cast<int32_t>(offsetof(U3CImageLoadU3Ec__Iterator16_t4085388203, ___file_1)); }
	inline String_t* get_file_1() const { return ___file_1; }
	inline String_t** get_address_of_file_1() { return &___file_1; }
	inline void set_file_1(String_t* value)
	{
		___file_1 = value;
		Il2CppCodeGenWriteBarrier(&___file_1, value);
	}

	inline static int32_t get_offset_of_U3CbinaryImageDataU3E__1_2() { return static_cast<int32_t>(offsetof(U3CImageLoadU3Ec__Iterator16_t4085388203, ___U3CbinaryImageDataU3E__1_2)); }
	inline ByteU5BU5D_t4260760469* get_U3CbinaryImageDataU3E__1_2() const { return ___U3CbinaryImageDataU3E__1_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CbinaryImageDataU3E__1_2() { return &___U3CbinaryImageDataU3E__1_2; }
	inline void set_U3CbinaryImageDataU3E__1_2(ByteU5BU5D_t4260760469* value)
	{
		___U3CbinaryImageDataU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbinaryImageDataU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CImageLoadU3Ec__Iterator16_t4085388203, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CImageLoadU3Ec__Iterator16_t4085388203, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Efile_5() { return static_cast<int32_t>(offsetof(U3CImageLoadU3Ec__Iterator16_t4085388203, ___U3CU24U3Efile_5)); }
	inline String_t* get_U3CU24U3Efile_5() const { return ___U3CU24U3Efile_5; }
	inline String_t** get_address_of_U3CU24U3Efile_5() { return &___U3CU24U3Efile_5; }
	inline void set_U3CU24U3Efile_5(String_t* value)
	{
		___U3CU24U3Efile_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Efile_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CImageLoadU3Ec__Iterator16_t4085388203, ___U3CU3Ef__this_6)); }
	inline MessageCheck_t3146986849 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline MessageCheck_t3146986849 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(MessageCheck_t3146986849 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
