﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginCheck
struct LoginCheck_t219618111;

#include "codegen/il2cpp-codegen.h"

// System.Void LoginCheck::.ctor()
extern "C"  void LoginCheck__ctor_m899003452 (LoginCheck_t219618111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginCheck::.cctor()
extern "C"  void LoginCheck__cctor_m1617207025 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LoginCheck::get_LoginResult()
extern "C"  int32_t LoginCheck_get_LoginResult_m2321846537 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginCheck::set_LoginResult(System.Int32)
extern "C"  void LoginCheck_set_LoginResult_m2617684276 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginCheck::Awake()
extern "C"  void LoginCheck_Awake_m1136608671 (LoginCheck_t219618111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginCheck::Update()
extern "C"  void LoginCheck_Update_m3826165329 (LoginCheck_t219618111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
