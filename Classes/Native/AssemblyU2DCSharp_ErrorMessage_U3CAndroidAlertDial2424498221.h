﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ErrorMessage/<AndroidAlertDialog>c__AnonStorey25
struct  U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221  : public Il2CppObject
{
public:
	// System.String ErrorMessage/<AndroidAlertDialog>c__AnonStorey25::title
	String_t* ___title_0;
	// System.String ErrorMessage/<AndroidAlertDialog>c__AnonStorey25::message
	String_t* ___message_1;
	// System.Int32 ErrorMessage/<AndroidAlertDialog>c__AnonStorey25::index
	int32_t ___index_2;
	// System.String ErrorMessage/<AndroidAlertDialog>c__AnonStorey25::button
	String_t* ___button_3;

public:
	inline static int32_t get_offset_of_title_0() { return static_cast<int32_t>(offsetof(U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221, ___title_0)); }
	inline String_t* get_title_0() const { return ___title_0; }
	inline String_t** get_address_of_title_0() { return &___title_0; }
	inline void set_title_0(String_t* value)
	{
		___title_0 = value;
		Il2CppCodeGenWriteBarrier(&___title_0, value);
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier(&___message_1, value);
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_button_3() { return static_cast<int32_t>(offsetof(U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221, ___button_3)); }
	inline String_t* get_button_3() const { return ___button_3; }
	inline String_t** get_address_of_button_3() { return &___button_3; }
	inline void set_button_3(String_t* value)
	{
		___button_3 = value;
		Il2CppCodeGenWriteBarrier(&___button_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
