﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EmailRecv
struct EmailRecv_t4124314498;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "mscorlib_System_String7231557.h"

// System.Void EmailRecv::.ctor()
extern "C"  void EmailRecv__ctor_m183426089 (EmailRecv_t4124314498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmailRecv::OnEnable()
extern "C"  void EmailRecv_OnEnable_m3319795389 (EmailRecv_t4124314498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EmailRecv::GetEmailList()
extern "C"  Il2CppObject * EmailRecv_GetEmailList_m4085222231 (EmailRecv_t4124314498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EmailRecv::Progress(UnityEngine.WWW)
extern "C"  Il2CppObject * EmailRecv_Progress_m4217691602 (EmailRecv_t4124314498 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmailRecv::TakeJson(System.String)
extern "C"  void EmailRecv_TakeJson_m1240939000 (EmailRecv_t4124314498 * __this, String_t* ___js0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
