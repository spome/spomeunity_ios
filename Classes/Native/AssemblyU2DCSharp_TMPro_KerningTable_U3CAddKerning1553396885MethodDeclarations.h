﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.KerningTable/<AddKerningPair>c__AnonStorey31
struct U3CAddKerningPairU3Ec__AnonStorey31_t1553396885;
// TMPro.KerningPair
struct KerningPair_t1904833704;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_KerningPair1904833704.h"

// System.Void TMPro.KerningTable/<AddKerningPair>c__AnonStorey31::.ctor()
extern "C"  void U3CAddKerningPairU3Ec__AnonStorey31__ctor_m3006923686 (U3CAddKerningPairU3Ec__AnonStorey31_t1553396885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.KerningTable/<AddKerningPair>c__AnonStorey31::<>m__10(TMPro.KerningPair)
extern "C"  bool U3CAddKerningPairU3Ec__AnonStorey31_U3CU3Em__10_m3627993708 (U3CAddKerningPairU3Ec__AnonStorey31_t1553396885 * __this, KerningPair_t1904833704 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
