﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageCheck/<VideoDown>c__Iterator14
struct U3CVideoDownU3Ec__Iterator14_t3182108037;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageCheck/<VideoDown>c__Iterator14::.ctor()
extern "C"  void U3CVideoDownU3Ec__Iterator14__ctor_m3844436358 (U3CVideoDownU3Ec__Iterator14_t3182108037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageCheck/<VideoDown>c__Iterator14::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CVideoDownU3Ec__Iterator14_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3156995404 (U3CVideoDownU3Ec__Iterator14_t3182108037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageCheck/<VideoDown>c__Iterator14::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CVideoDownU3Ec__Iterator14_System_Collections_IEnumerator_get_Current_m2778335456 (U3CVideoDownU3Ec__Iterator14_t3182108037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MessageCheck/<VideoDown>c__Iterator14::MoveNext()
extern "C"  bool U3CVideoDownU3Ec__Iterator14_MoveNext_m1634676142 (U3CVideoDownU3Ec__Iterator14_t3182108037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck/<VideoDown>c__Iterator14::Dispose()
extern "C"  void U3CVideoDownU3Ec__Iterator14_Dispose_m729748547 (U3CVideoDownU3Ec__Iterator14_t3182108037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck/<VideoDown>c__Iterator14::Reset()
extern "C"  void U3CVideoDownU3Ec__Iterator14_Reset_m1490869299 (U3CVideoDownU3Ec__Iterator14_t3182108037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
