﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPSCounter
struct  FPSCounter_t1808341235  : public MonoBehaviour_t667441552
{
public:
	// System.Single FPSCounter::frequency
	float ___frequency_2;
	// System.Int32 FPSCounter::<FramesPerSec>k__BackingField
	int32_t ___U3CFramesPerSecU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_frequency_2() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235, ___frequency_2)); }
	inline float get_frequency_2() const { return ___frequency_2; }
	inline float* get_address_of_frequency_2() { return &___frequency_2; }
	inline void set_frequency_2(float value)
	{
		___frequency_2 = value;
	}

	inline static int32_t get_offset_of_U3CFramesPerSecU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235, ___U3CFramesPerSecU3Ek__BackingField_3)); }
	inline int32_t get_U3CFramesPerSecU3Ek__BackingField_3() const { return ___U3CFramesPerSecU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CFramesPerSecU3Ek__BackingField_3() { return &___U3CFramesPerSecU3Ek__BackingField_3; }
	inline void set_U3CFramesPerSecU3Ek__BackingField_3(int32_t value)
	{
		___U3CFramesPerSecU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
