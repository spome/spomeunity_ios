﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CurrentState
struct CurrentState_t1622142648;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CurrentState_States2558012761.h"
#include "AssemblyU2DCSharp_CurrentState_InquiryTitles1700261515.h"
#include "AssemblyU2DCSharp_CurrentState_GPSPopupMessages960090149.h"
#include "AssemblyU2DCSharp_CurrentState_LoginPopupMessages1285607942.h"
#include "AssemblyU2DCSharp_CurrentState_PopupMessages2071685857.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_CurrentState_MemberStates3108678483.h"

// System.Void CurrentState::.ctor()
extern "C"  void CurrentState__ctor_m1475154659 (CurrentState_t1622142648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentState::.cctor()
extern "C"  void CurrentState__cctor_m2298025258 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CurrentState/States CurrentState::get_State()
extern "C"  int32_t CurrentState_get_State_m3296155471 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentState::set_State(CurrentState/States)
extern "C"  void CurrentState_set_State_m3490075940 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CurrentState/States CurrentState::get_LoginState()
extern "C"  int32_t CurrentState_get_LoginState_m3052003788 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentState::set_LoginState(CurrentState/States)
extern "C"  void CurrentState_set_LoginState_m3940368275 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CurrentState/InquiryTitles CurrentState::get_InquiryTitle()
extern "C"  int32_t CurrentState_get_InquiryTitle_m3036927725 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentState::set_InquiryTitle(CurrentState/InquiryTitles)
extern "C"  void CurrentState_set_InquiryTitle_m2236206948 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CurrentState/GPSPopupMessages CurrentState::get_GPSPopupMessage()
extern "C"  int32_t CurrentState_get_GPSPopupMessage_m652265359 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentState::set_GPSPopupMessage(CurrentState/GPSPopupMessages)
extern "C"  void CurrentState_set_GPSPopupMessage_m1603270948 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CurrentState/LoginPopupMessages CurrentState::get_LoginPopupMessage()
extern "C"  int32_t CurrentState_get_LoginPopupMessage_m464967087 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentState::set_LoginPopupMessage(CurrentState/LoginPopupMessages)
extern "C"  void CurrentState_set_LoginPopupMessage_m2175582148 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CurrentState/PopupMessages CurrentState::get_PopupMessage()
extern "C"  int32_t CurrentState_get_PopupMessage_m3375877057 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentState::set_PopupMessage(CurrentState/PopupMessages)
extern "C"  void CurrentState_set_PopupMessage_m2237488056 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentState::AddScreenState(CurrentState/States,UnityEngine.GameObject)
extern "C"  void CurrentState_AddScreenState_m2795282826 (Il2CppObject * __this /* static, unused */, int32_t ___key0, GameObject_t3674682005 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject CurrentState::GetScreenState(CurrentState/States)
extern "C"  GameObject_t3674682005 * CurrentState_GetScreenState_m3808002068 (Il2CppObject * __this /* static, unused */, int32_t ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CurrentState/MemberStates CurrentState::get_MemberState()
extern "C"  int32_t CurrentState_get_MemberState_m3859431375 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentState::set_MemberState(CurrentState/MemberStates)
extern "C"  void CurrentState_set_MemberState_m2378785892 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
