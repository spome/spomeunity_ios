﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EmailRecv/<Progress>c__IteratorC
struct U3CProgressU3Ec__IteratorC_t2850438612;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EmailRecv/<Progress>c__IteratorC::.ctor()
extern "C"  void U3CProgressU3Ec__IteratorC__ctor_m551946055 (U3CProgressU3Ec__IteratorC_t2850438612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EmailRecv/<Progress>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CProgressU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m197824757 (U3CProgressU3Ec__IteratorC_t2850438612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EmailRecv/<Progress>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CProgressU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m4024376969 (U3CProgressU3Ec__IteratorC_t2850438612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EmailRecv/<Progress>c__IteratorC::MoveNext()
extern "C"  bool U3CProgressU3Ec__IteratorC_MoveNext_m833045813 (U3CProgressU3Ec__IteratorC_t2850438612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmailRecv/<Progress>c__IteratorC::Dispose()
extern "C"  void U3CProgressU3Ec__IteratorC_Dispose_m2037464516 (U3CProgressU3Ec__IteratorC_t2850438612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmailRecv/<Progress>c__IteratorC::Reset()
extern "C"  void U3CProgressU3Ec__IteratorC_Reset_m2493346292 (U3CProgressU3Ec__IteratorC_t2850438612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
