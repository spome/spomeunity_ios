﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AddDropDownContents
struct  AddDropDownContents_t2054805772  : public MonoBehaviour_t667441552
{
public:
	// System.String AddDropDownContents::m_strName
	String_t* ___m_strName_2;
	// UnityEngine.GameObject AddDropDownContents::DD
	GameObject_t3674682005 * ___DD_3;

public:
	inline static int32_t get_offset_of_m_strName_2() { return static_cast<int32_t>(offsetof(AddDropDownContents_t2054805772, ___m_strName_2)); }
	inline String_t* get_m_strName_2() const { return ___m_strName_2; }
	inline String_t** get_address_of_m_strName_2() { return &___m_strName_2; }
	inline void set_m_strName_2(String_t* value)
	{
		___m_strName_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_strName_2, value);
	}

	inline static int32_t get_offset_of_DD_3() { return static_cast<int32_t>(offsetof(AddDropDownContents_t2054805772, ___DD_3)); }
	inline GameObject_t3674682005 * get_DD_3() const { return ___DD_3; }
	inline GameObject_t3674682005 ** get_address_of_DD_3() { return &___DD_3; }
	inline void set_DD_3(GameObject_t3674682005 * value)
	{
		___DD_3 = value;
		Il2CppCodeGenWriteBarrier(&___DD_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
