﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FPSCounter
struct FPSCounter_t1808341235;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void FPSCounter::.ctor()
extern "C"  void FPSCounter__ctor_m645128456 (FPSCounter_t1808341235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FPSCounter::get_FramesPerSec()
extern "C"  int32_t FPSCounter_get_FramesPerSec_m2683539469 (FPSCounter_t1808341235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounter::set_FramesPerSec(System.Int32)
extern "C"  void FPSCounter_set_FramesPerSec_m1406923972 (FPSCounter_t1808341235 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounter::Start()
extern "C"  void FPSCounter_Start_m3887233544 (FPSCounter_t1808341235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FPSCounter::FPS()
extern "C"  Il2CppObject * FPSCounter_FPS_m1661540551 (FPSCounter_t1808341235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
