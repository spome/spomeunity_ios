﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageSee/<JsonStart>c__Iterator1C
struct U3CJsonStartU3Ec__Iterator1C_t1966288966;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageSee/<JsonStart>c__Iterator1C::.ctor()
extern "C"  void U3CJsonStartU3Ec__Iterator1C__ctor_m3442672101 (U3CJsonStartU3Ec__Iterator1C_t1966288966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageSee/<JsonStart>c__Iterator1C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CJsonStartU3Ec__Iterator1C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2251434253 (U3CJsonStartU3Ec__Iterator1C_t1966288966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageSee/<JsonStart>c__Iterator1C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CJsonStartU3Ec__Iterator1C_System_Collections_IEnumerator_get_Current_m2028594849 (U3CJsonStartU3Ec__Iterator1C_t1966288966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MessageSee/<JsonStart>c__Iterator1C::MoveNext()
extern "C"  bool U3CJsonStartU3Ec__Iterator1C_MoveNext_m1423986671 (U3CJsonStartU3Ec__Iterator1C_t1966288966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSee/<JsonStart>c__Iterator1C::Dispose()
extern "C"  void U3CJsonStartU3Ec__Iterator1C_Dispose_m1181354210 (U3CJsonStartU3Ec__Iterator1C_t1966288966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSee/<JsonStart>c__Iterator1C::Reset()
extern "C"  void U3CJsonStartU3Ec__Iterator1C_Reset_m1089105042 (U3CJsonStartU3Ec__Iterator1C_t1966288966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
