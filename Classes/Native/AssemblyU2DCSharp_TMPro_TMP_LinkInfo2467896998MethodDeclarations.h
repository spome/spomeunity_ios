﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t2467896998;
struct TMP_LinkInfo_t2467896998_marshaled_pinvoke;
struct TMP_LinkInfo_t2467896998_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_TMP_LinkInfo2467896998.h"

// System.String TMPro.TMP_LinkInfo::GetLinkText()
extern "C"  String_t* TMP_LinkInfo_GetLinkText_m2288035529 (TMP_LinkInfo_t2467896998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TMPro.TMP_LinkInfo::GetLinkID()
extern "C"  String_t* TMP_LinkInfo_GetLinkID_m1512631767 (TMP_LinkInfo_t2467896998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct TMP_LinkInfo_t2467896998;
struct TMP_LinkInfo_t2467896998_marshaled_pinvoke;

extern "C" void TMP_LinkInfo_t2467896998_marshal_pinvoke(const TMP_LinkInfo_t2467896998& unmarshaled, TMP_LinkInfo_t2467896998_marshaled_pinvoke& marshaled);
extern "C" void TMP_LinkInfo_t2467896998_marshal_pinvoke_back(const TMP_LinkInfo_t2467896998_marshaled_pinvoke& marshaled, TMP_LinkInfo_t2467896998& unmarshaled);
extern "C" void TMP_LinkInfo_t2467896998_marshal_pinvoke_cleanup(TMP_LinkInfo_t2467896998_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct TMP_LinkInfo_t2467896998;
struct TMP_LinkInfo_t2467896998_marshaled_com;

extern "C" void TMP_LinkInfo_t2467896998_marshal_com(const TMP_LinkInfo_t2467896998& unmarshaled, TMP_LinkInfo_t2467896998_marshaled_com& marshaled);
extern "C" void TMP_LinkInfo_t2467896998_marshal_com_back(const TMP_LinkInfo_t2467896998_marshaled_com& marshaled, TMP_LinkInfo_t2467896998& unmarshaled);
extern "C" void TMP_LinkInfo_t2467896998_marshal_com_cleanup(TMP_LinkInfo_t2467896998_marshaled_com& marshaled);
