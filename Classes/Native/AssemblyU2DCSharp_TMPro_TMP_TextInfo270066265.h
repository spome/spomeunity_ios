﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TMPro.TMP_Text
struct TMP_Text_t980027659;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t2688989516;
// TMPro.TMP_WordInfo[]
struct TMP_WordInfoU5BU5D_t3309586099;
// TMPro.TMP_LinkInfo[]
struct TMP_LinkInfoU5BU5D_t3523101699;
// TMPro.TMP_LineInfo[]
struct TMP_LineInfoU5BU5D_t2827998177;
// TMPro.TMP_PageInfo[]
struct TMP_PageInfoU5BU5D_t3051601914;
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t3450595428;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextInfo
struct  TMP_TextInfo_t270066265  : public Il2CppObject
{
public:
	// TMPro.TMP_Text TMPro.TMP_TextInfo::textComponent
	TMP_Text_t980027659 * ___textComponent_1;
	// System.Int32 TMPro.TMP_TextInfo::characterCount
	int32_t ___characterCount_2;
	// System.Int32 TMPro.TMP_TextInfo::spriteCount
	int32_t ___spriteCount_3;
	// System.Int32 TMPro.TMP_TextInfo::spaceCount
	int32_t ___spaceCount_4;
	// System.Int32 TMPro.TMP_TextInfo::wordCount
	int32_t ___wordCount_5;
	// System.Int32 TMPro.TMP_TextInfo::linkCount
	int32_t ___linkCount_6;
	// System.Int32 TMPro.TMP_TextInfo::lineCount
	int32_t ___lineCount_7;
	// System.Int32 TMPro.TMP_TextInfo::pageCount
	int32_t ___pageCount_8;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_TextInfo::characterInfo
	TMP_CharacterInfoU5BU5D_t2688989516* ___characterInfo_9;
	// TMPro.TMP_WordInfo[] TMPro.TMP_TextInfo::wordInfo
	TMP_WordInfoU5BU5D_t3309586099* ___wordInfo_10;
	// TMPro.TMP_LinkInfo[] TMPro.TMP_TextInfo::linkInfo
	TMP_LinkInfoU5BU5D_t3523101699* ___linkInfo_11;
	// TMPro.TMP_LineInfo[] TMPro.TMP_TextInfo::lineInfo
	TMP_LineInfoU5BU5D_t2827998177* ___lineInfo_12;
	// TMPro.TMP_PageInfo[] TMPro.TMP_TextInfo::pageInfo
	TMP_PageInfoU5BU5D_t3051601914* ___pageInfo_13;
	// TMPro.TMP_MeshInfo[] TMPro.TMP_TextInfo::meshInfo
	TMP_MeshInfoU5BU5D_t3450595428* ___meshInfo_14;

public:
	inline static int32_t get_offset_of_textComponent_1() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t270066265, ___textComponent_1)); }
	inline TMP_Text_t980027659 * get_textComponent_1() const { return ___textComponent_1; }
	inline TMP_Text_t980027659 ** get_address_of_textComponent_1() { return &___textComponent_1; }
	inline void set_textComponent_1(TMP_Text_t980027659 * value)
	{
		___textComponent_1 = value;
		Il2CppCodeGenWriteBarrier(&___textComponent_1, value);
	}

	inline static int32_t get_offset_of_characterCount_2() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t270066265, ___characterCount_2)); }
	inline int32_t get_characterCount_2() const { return ___characterCount_2; }
	inline int32_t* get_address_of_characterCount_2() { return &___characterCount_2; }
	inline void set_characterCount_2(int32_t value)
	{
		___characterCount_2 = value;
	}

	inline static int32_t get_offset_of_spriteCount_3() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t270066265, ___spriteCount_3)); }
	inline int32_t get_spriteCount_3() const { return ___spriteCount_3; }
	inline int32_t* get_address_of_spriteCount_3() { return &___spriteCount_3; }
	inline void set_spriteCount_3(int32_t value)
	{
		___spriteCount_3 = value;
	}

	inline static int32_t get_offset_of_spaceCount_4() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t270066265, ___spaceCount_4)); }
	inline int32_t get_spaceCount_4() const { return ___spaceCount_4; }
	inline int32_t* get_address_of_spaceCount_4() { return &___spaceCount_4; }
	inline void set_spaceCount_4(int32_t value)
	{
		___spaceCount_4 = value;
	}

	inline static int32_t get_offset_of_wordCount_5() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t270066265, ___wordCount_5)); }
	inline int32_t get_wordCount_5() const { return ___wordCount_5; }
	inline int32_t* get_address_of_wordCount_5() { return &___wordCount_5; }
	inline void set_wordCount_5(int32_t value)
	{
		___wordCount_5 = value;
	}

	inline static int32_t get_offset_of_linkCount_6() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t270066265, ___linkCount_6)); }
	inline int32_t get_linkCount_6() const { return ___linkCount_6; }
	inline int32_t* get_address_of_linkCount_6() { return &___linkCount_6; }
	inline void set_linkCount_6(int32_t value)
	{
		___linkCount_6 = value;
	}

	inline static int32_t get_offset_of_lineCount_7() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t270066265, ___lineCount_7)); }
	inline int32_t get_lineCount_7() const { return ___lineCount_7; }
	inline int32_t* get_address_of_lineCount_7() { return &___lineCount_7; }
	inline void set_lineCount_7(int32_t value)
	{
		___lineCount_7 = value;
	}

	inline static int32_t get_offset_of_pageCount_8() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t270066265, ___pageCount_8)); }
	inline int32_t get_pageCount_8() const { return ___pageCount_8; }
	inline int32_t* get_address_of_pageCount_8() { return &___pageCount_8; }
	inline void set_pageCount_8(int32_t value)
	{
		___pageCount_8 = value;
	}

	inline static int32_t get_offset_of_characterInfo_9() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t270066265, ___characterInfo_9)); }
	inline TMP_CharacterInfoU5BU5D_t2688989516* get_characterInfo_9() const { return ___characterInfo_9; }
	inline TMP_CharacterInfoU5BU5D_t2688989516** get_address_of_characterInfo_9() { return &___characterInfo_9; }
	inline void set_characterInfo_9(TMP_CharacterInfoU5BU5D_t2688989516* value)
	{
		___characterInfo_9 = value;
		Il2CppCodeGenWriteBarrier(&___characterInfo_9, value);
	}

	inline static int32_t get_offset_of_wordInfo_10() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t270066265, ___wordInfo_10)); }
	inline TMP_WordInfoU5BU5D_t3309586099* get_wordInfo_10() const { return ___wordInfo_10; }
	inline TMP_WordInfoU5BU5D_t3309586099** get_address_of_wordInfo_10() { return &___wordInfo_10; }
	inline void set_wordInfo_10(TMP_WordInfoU5BU5D_t3309586099* value)
	{
		___wordInfo_10 = value;
		Il2CppCodeGenWriteBarrier(&___wordInfo_10, value);
	}

	inline static int32_t get_offset_of_linkInfo_11() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t270066265, ___linkInfo_11)); }
	inline TMP_LinkInfoU5BU5D_t3523101699* get_linkInfo_11() const { return ___linkInfo_11; }
	inline TMP_LinkInfoU5BU5D_t3523101699** get_address_of_linkInfo_11() { return &___linkInfo_11; }
	inline void set_linkInfo_11(TMP_LinkInfoU5BU5D_t3523101699* value)
	{
		___linkInfo_11 = value;
		Il2CppCodeGenWriteBarrier(&___linkInfo_11, value);
	}

	inline static int32_t get_offset_of_lineInfo_12() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t270066265, ___lineInfo_12)); }
	inline TMP_LineInfoU5BU5D_t2827998177* get_lineInfo_12() const { return ___lineInfo_12; }
	inline TMP_LineInfoU5BU5D_t2827998177** get_address_of_lineInfo_12() { return &___lineInfo_12; }
	inline void set_lineInfo_12(TMP_LineInfoU5BU5D_t2827998177* value)
	{
		___lineInfo_12 = value;
		Il2CppCodeGenWriteBarrier(&___lineInfo_12, value);
	}

	inline static int32_t get_offset_of_pageInfo_13() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t270066265, ___pageInfo_13)); }
	inline TMP_PageInfoU5BU5D_t3051601914* get_pageInfo_13() const { return ___pageInfo_13; }
	inline TMP_PageInfoU5BU5D_t3051601914** get_address_of_pageInfo_13() { return &___pageInfo_13; }
	inline void set_pageInfo_13(TMP_PageInfoU5BU5D_t3051601914* value)
	{
		___pageInfo_13 = value;
		Il2CppCodeGenWriteBarrier(&___pageInfo_13, value);
	}

	inline static int32_t get_offset_of_meshInfo_14() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t270066265, ___meshInfo_14)); }
	inline TMP_MeshInfoU5BU5D_t3450595428* get_meshInfo_14() const { return ___meshInfo_14; }
	inline TMP_MeshInfoU5BU5D_t3450595428** get_address_of_meshInfo_14() { return &___meshInfo_14; }
	inline void set_meshInfo_14(TMP_MeshInfoU5BU5D_t3450595428* value)
	{
		___meshInfo_14 = value;
		Il2CppCodeGenWriteBarrier(&___meshInfo_14, value);
	}
};

struct TMP_TextInfo_t270066265_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_TextInfo::k_InfinityVector
	Vector2_t4282066565  ___k_InfinityVector_0;

public:
	inline static int32_t get_offset_of_k_InfinityVector_0() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t270066265_StaticFields, ___k_InfinityVector_0)); }
	inline Vector2_t4282066565  get_k_InfinityVector_0() const { return ___k_InfinityVector_0; }
	inline Vector2_t4282066565 * get_address_of_k_InfinityVector_0() { return &___k_InfinityVector_0; }
	inline void set_k_InfinityVector_0(Vector2_t4282066565  value)
	{
		___k_InfinityVector_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
