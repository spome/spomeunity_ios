﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// WebViewObject
struct WebViewObject_t388577433;
// iosWebView
struct iosWebView_t3524785996;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Action`1<System.String>
struct Action_1_t403047693;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iosWebView
struct  iosWebView_t3524785996  : public MonoBehaviour_t667441552
{
public:
	// System.String iosWebView::url
	String_t* ___url_2;
	// WebViewObject iosWebView::webViewObject
	WebViewObject_t388577433 * ___webViewObject_3;
	// UnityEngine.GameObject iosWebView::panel
	GameObject_t3674682005 * ___panel_5;
	// UnityEngine.GameObject iosWebView::webBackButton
	GameObject_t3674682005 * ___webBackButton_6;

public:
	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(iosWebView_t3524785996, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier(&___url_2, value);
	}

	inline static int32_t get_offset_of_webViewObject_3() { return static_cast<int32_t>(offsetof(iosWebView_t3524785996, ___webViewObject_3)); }
	inline WebViewObject_t388577433 * get_webViewObject_3() const { return ___webViewObject_3; }
	inline WebViewObject_t388577433 ** get_address_of_webViewObject_3() { return &___webViewObject_3; }
	inline void set_webViewObject_3(WebViewObject_t388577433 * value)
	{
		___webViewObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___webViewObject_3, value);
	}

	inline static int32_t get_offset_of_panel_5() { return static_cast<int32_t>(offsetof(iosWebView_t3524785996, ___panel_5)); }
	inline GameObject_t3674682005 * get_panel_5() const { return ___panel_5; }
	inline GameObject_t3674682005 ** get_address_of_panel_5() { return &___panel_5; }
	inline void set_panel_5(GameObject_t3674682005 * value)
	{
		___panel_5 = value;
		Il2CppCodeGenWriteBarrier(&___panel_5, value);
	}

	inline static int32_t get_offset_of_webBackButton_6() { return static_cast<int32_t>(offsetof(iosWebView_t3524785996, ___webBackButton_6)); }
	inline GameObject_t3674682005 * get_webBackButton_6() const { return ___webBackButton_6; }
	inline GameObject_t3674682005 ** get_address_of_webBackButton_6() { return &___webBackButton_6; }
	inline void set_webBackButton_6(GameObject_t3674682005 * value)
	{
		___webBackButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___webBackButton_6, value);
	}
};

struct iosWebView_t3524785996_StaticFields
{
public:
	// iosWebView iosWebView::_instance
	iosWebView_t3524785996 * ____instance_4;
	// System.Action`1<System.String> iosWebView::<>f__am$cache5
	Action_1_t403047693 * ___U3CU3Ef__amU24cache5_7;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(iosWebView_t3524785996_StaticFields, ____instance_4)); }
	inline iosWebView_t3524785996 * get__instance_4() const { return ____instance_4; }
	inline iosWebView_t3524785996 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(iosWebView_t3524785996 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier(&____instance_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_7() { return static_cast<int32_t>(offsetof(iosWebView_t3524785996_StaticFields, ___U3CU3Ef__amU24cache5_7)); }
	inline Action_1_t403047693 * get_U3CU3Ef__amU24cache5_7() const { return ___U3CU3Ef__amU24cache5_7; }
	inline Action_1_t403047693 ** get_address_of_U3CU3Ef__amU24cache5_7() { return &___U3CU3Ef__amU24cache5_7; }
	inline void set_U3CU3Ef__amU24cache5_7(Action_1_t403047693 * value)
	{
		___U3CU3Ef__amU24cache5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
