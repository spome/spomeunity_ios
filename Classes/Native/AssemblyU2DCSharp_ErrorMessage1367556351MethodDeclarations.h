﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ErrorMessage
struct ErrorMessage_t1367556351;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ErrorMessage::.ctor()
extern "C"  void ErrorMessage__ctor_m3943981180 (ErrorMessage_t1367556351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessage::.cctor()
extern "C"  void ErrorMessage__cctor_m1522236081 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessage::Start()
extern "C"  void ErrorMessage_Start_m2891118972 (ErrorMessage_t1367556351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ErrorMessage::get_PopupON()
extern "C"  bool ErrorMessage_get_PopupON_m360300232 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessage::AndroidAlertDialog(System.String,System.String,System.String)
extern "C"  void ErrorMessage_AndroidAlertDialog_m3506572413 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___button2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessage::AndroidAlertDialog(System.Int32,System.String,System.String,System.String)
extern "C"  void ErrorMessage_AndroidAlertDialog_m297139618 (Il2CppObject * __this /* static, unused */, int32_t ___index0, String_t* ___title1, String_t* ___message2, String_t* ___button3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
