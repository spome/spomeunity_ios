﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GreatmanMessage/<ImageLoad>c__Iterator11
struct U3CImageLoadU3Ec__Iterator11_t890999725;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GreatmanMessage/<ImageLoad>c__Iterator11::.ctor()
extern "C"  void U3CImageLoadU3Ec__Iterator11__ctor_m423468430 (U3CImageLoadU3Ec__Iterator11_t890999725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GreatmanMessage/<ImageLoad>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CImageLoadU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2580157454 (U3CImageLoadU3Ec__Iterator11_t890999725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GreatmanMessage/<ImageLoad>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CImageLoadU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m3952210850 (U3CImageLoadU3Ec__Iterator11_t890999725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GreatmanMessage/<ImageLoad>c__Iterator11::MoveNext()
extern "C"  bool U3CImageLoadU3Ec__Iterator11_MoveNext_m4188946830 (U3CImageLoadU3Ec__Iterator11_t890999725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreatmanMessage/<ImageLoad>c__Iterator11::Dispose()
extern "C"  void U3CImageLoadU3Ec__Iterator11_Dispose_m3124518475 (U3CImageLoadU3Ec__Iterator11_t890999725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreatmanMessage/<ImageLoad>c__Iterator11::Reset()
extern "C"  void U3CImageLoadU3Ec__Iterator11_Reset_m2364868667 (U3CImageLoadU3Ec__Iterator11_t890999725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
