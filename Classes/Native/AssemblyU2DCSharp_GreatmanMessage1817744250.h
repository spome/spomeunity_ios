﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.UI.Button
struct Button_t3896396478;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GreatmanMessage
struct  GreatmanMessage_t1817744250  : public MonoBehaviour_t667441552
{
public:
	// System.String GreatmanMessage::_title
	String_t* ____title_2;
	// System.String GreatmanMessage::_realfilename
	String_t* ____realfilename_3;
	// System.String GreatmanMessage::_pref
	String_t* ____pref_4;
	// System.String GreatmanMessage::_addr01
	String_t* ____addr01_5;
	// System.String GreatmanMessage::_addr02
	String_t* ____addr02_6;
	// System.String GreatmanMessage::_latitude
	String_t* ____latitude_7;
	// System.String GreatmanMessage::_longtitude
	String_t* ____longtitude_8;
	// System.String GreatmanMessage::_type
	String_t* ____type_9;
	// System.String GreatmanMessage::_video_localpath
	String_t* ____video_localpath_10;
	// System.String GreatmanMessage::_video_localpath2
	String_t* ____video_localpath2_11;
	// System.String GreatmanMessage::_image_localpath
	String_t* ____image_localpath_12;
	// System.String GreatmanMessage::_videoname
	String_t* ____videoname_13;
	// UnityEngine.GameObject GreatmanMessage::_Sprite
	GameObject_t3674682005 * ____Sprite_14;
	// UnityEngine.RectTransform GreatmanMessage::imagesize
	RectTransform_t972643934 * ___imagesize_15;
	// UnityEngine.UI.Text GreatmanMessage::BodyText
	Text_t9039225 * ___BodyText_16;
	// UnityEngine.UI.Text GreatmanMessage::TitleText
	Text_t9039225 * ___TitleText_17;
	// UnityEngine.GameObject GreatmanMessage::panel
	GameObject_t3674682005 * ___panel_18;
	// UnityEngine.GameObject GreatmanMessage::ProgressBar
	GameObject_t3674682005 * ___ProgressBar_19;
	// UnityEngine.UI.Image GreatmanMessage::image
	Image_t538875265 * ___image_20;
	// UnityEngine.UI.Text GreatmanMessage::CurrentDistance
	Text_t9039225 * ___CurrentDistance_21;
	// UnityEngine.UI.Button GreatmanMessage::MessageSeeBtn
	Button_t3896396478 * ___MessageSeeBtn_22;
	// System.Boolean GreatmanMessage::_messagecheck
	bool ____messagecheck_23;
	// System.Double GreatmanMessage::_distancedouble
	double ____distancedouble_24;
	// System.String GreatmanMessage::lat
	String_t* ___lat_25;
	// System.String GreatmanMessage::lon
	String_t* ___lon_26;
	// System.Single GreatmanMessage::timer
	float ___timer_27;
	// System.Int32 GreatmanMessage::waitingTime
	int32_t ___waitingTime_28;

public:
	inline static int32_t get_offset_of__title_2() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ____title_2)); }
	inline String_t* get__title_2() const { return ____title_2; }
	inline String_t** get_address_of__title_2() { return &____title_2; }
	inline void set__title_2(String_t* value)
	{
		____title_2 = value;
		Il2CppCodeGenWriteBarrier(&____title_2, value);
	}

	inline static int32_t get_offset_of__realfilename_3() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ____realfilename_3)); }
	inline String_t* get__realfilename_3() const { return ____realfilename_3; }
	inline String_t** get_address_of__realfilename_3() { return &____realfilename_3; }
	inline void set__realfilename_3(String_t* value)
	{
		____realfilename_3 = value;
		Il2CppCodeGenWriteBarrier(&____realfilename_3, value);
	}

	inline static int32_t get_offset_of__pref_4() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ____pref_4)); }
	inline String_t* get__pref_4() const { return ____pref_4; }
	inline String_t** get_address_of__pref_4() { return &____pref_4; }
	inline void set__pref_4(String_t* value)
	{
		____pref_4 = value;
		Il2CppCodeGenWriteBarrier(&____pref_4, value);
	}

	inline static int32_t get_offset_of__addr01_5() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ____addr01_5)); }
	inline String_t* get__addr01_5() const { return ____addr01_5; }
	inline String_t** get_address_of__addr01_5() { return &____addr01_5; }
	inline void set__addr01_5(String_t* value)
	{
		____addr01_5 = value;
		Il2CppCodeGenWriteBarrier(&____addr01_5, value);
	}

	inline static int32_t get_offset_of__addr02_6() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ____addr02_6)); }
	inline String_t* get__addr02_6() const { return ____addr02_6; }
	inline String_t** get_address_of__addr02_6() { return &____addr02_6; }
	inline void set__addr02_6(String_t* value)
	{
		____addr02_6 = value;
		Il2CppCodeGenWriteBarrier(&____addr02_6, value);
	}

	inline static int32_t get_offset_of__latitude_7() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ____latitude_7)); }
	inline String_t* get__latitude_7() const { return ____latitude_7; }
	inline String_t** get_address_of__latitude_7() { return &____latitude_7; }
	inline void set__latitude_7(String_t* value)
	{
		____latitude_7 = value;
		Il2CppCodeGenWriteBarrier(&____latitude_7, value);
	}

	inline static int32_t get_offset_of__longtitude_8() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ____longtitude_8)); }
	inline String_t* get__longtitude_8() const { return ____longtitude_8; }
	inline String_t** get_address_of__longtitude_8() { return &____longtitude_8; }
	inline void set__longtitude_8(String_t* value)
	{
		____longtitude_8 = value;
		Il2CppCodeGenWriteBarrier(&____longtitude_8, value);
	}

	inline static int32_t get_offset_of__type_9() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ____type_9)); }
	inline String_t* get__type_9() const { return ____type_9; }
	inline String_t** get_address_of__type_9() { return &____type_9; }
	inline void set__type_9(String_t* value)
	{
		____type_9 = value;
		Il2CppCodeGenWriteBarrier(&____type_9, value);
	}

	inline static int32_t get_offset_of__video_localpath_10() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ____video_localpath_10)); }
	inline String_t* get__video_localpath_10() const { return ____video_localpath_10; }
	inline String_t** get_address_of__video_localpath_10() { return &____video_localpath_10; }
	inline void set__video_localpath_10(String_t* value)
	{
		____video_localpath_10 = value;
		Il2CppCodeGenWriteBarrier(&____video_localpath_10, value);
	}

	inline static int32_t get_offset_of__video_localpath2_11() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ____video_localpath2_11)); }
	inline String_t* get__video_localpath2_11() const { return ____video_localpath2_11; }
	inline String_t** get_address_of__video_localpath2_11() { return &____video_localpath2_11; }
	inline void set__video_localpath2_11(String_t* value)
	{
		____video_localpath2_11 = value;
		Il2CppCodeGenWriteBarrier(&____video_localpath2_11, value);
	}

	inline static int32_t get_offset_of__image_localpath_12() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ____image_localpath_12)); }
	inline String_t* get__image_localpath_12() const { return ____image_localpath_12; }
	inline String_t** get_address_of__image_localpath_12() { return &____image_localpath_12; }
	inline void set__image_localpath_12(String_t* value)
	{
		____image_localpath_12 = value;
		Il2CppCodeGenWriteBarrier(&____image_localpath_12, value);
	}

	inline static int32_t get_offset_of__videoname_13() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ____videoname_13)); }
	inline String_t* get__videoname_13() const { return ____videoname_13; }
	inline String_t** get_address_of__videoname_13() { return &____videoname_13; }
	inline void set__videoname_13(String_t* value)
	{
		____videoname_13 = value;
		Il2CppCodeGenWriteBarrier(&____videoname_13, value);
	}

	inline static int32_t get_offset_of__Sprite_14() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ____Sprite_14)); }
	inline GameObject_t3674682005 * get__Sprite_14() const { return ____Sprite_14; }
	inline GameObject_t3674682005 ** get_address_of__Sprite_14() { return &____Sprite_14; }
	inline void set__Sprite_14(GameObject_t3674682005 * value)
	{
		____Sprite_14 = value;
		Il2CppCodeGenWriteBarrier(&____Sprite_14, value);
	}

	inline static int32_t get_offset_of_imagesize_15() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ___imagesize_15)); }
	inline RectTransform_t972643934 * get_imagesize_15() const { return ___imagesize_15; }
	inline RectTransform_t972643934 ** get_address_of_imagesize_15() { return &___imagesize_15; }
	inline void set_imagesize_15(RectTransform_t972643934 * value)
	{
		___imagesize_15 = value;
		Il2CppCodeGenWriteBarrier(&___imagesize_15, value);
	}

	inline static int32_t get_offset_of_BodyText_16() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ___BodyText_16)); }
	inline Text_t9039225 * get_BodyText_16() const { return ___BodyText_16; }
	inline Text_t9039225 ** get_address_of_BodyText_16() { return &___BodyText_16; }
	inline void set_BodyText_16(Text_t9039225 * value)
	{
		___BodyText_16 = value;
		Il2CppCodeGenWriteBarrier(&___BodyText_16, value);
	}

	inline static int32_t get_offset_of_TitleText_17() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ___TitleText_17)); }
	inline Text_t9039225 * get_TitleText_17() const { return ___TitleText_17; }
	inline Text_t9039225 ** get_address_of_TitleText_17() { return &___TitleText_17; }
	inline void set_TitleText_17(Text_t9039225 * value)
	{
		___TitleText_17 = value;
		Il2CppCodeGenWriteBarrier(&___TitleText_17, value);
	}

	inline static int32_t get_offset_of_panel_18() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ___panel_18)); }
	inline GameObject_t3674682005 * get_panel_18() const { return ___panel_18; }
	inline GameObject_t3674682005 ** get_address_of_panel_18() { return &___panel_18; }
	inline void set_panel_18(GameObject_t3674682005 * value)
	{
		___panel_18 = value;
		Il2CppCodeGenWriteBarrier(&___panel_18, value);
	}

	inline static int32_t get_offset_of_ProgressBar_19() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ___ProgressBar_19)); }
	inline GameObject_t3674682005 * get_ProgressBar_19() const { return ___ProgressBar_19; }
	inline GameObject_t3674682005 ** get_address_of_ProgressBar_19() { return &___ProgressBar_19; }
	inline void set_ProgressBar_19(GameObject_t3674682005 * value)
	{
		___ProgressBar_19 = value;
		Il2CppCodeGenWriteBarrier(&___ProgressBar_19, value);
	}

	inline static int32_t get_offset_of_image_20() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ___image_20)); }
	inline Image_t538875265 * get_image_20() const { return ___image_20; }
	inline Image_t538875265 ** get_address_of_image_20() { return &___image_20; }
	inline void set_image_20(Image_t538875265 * value)
	{
		___image_20 = value;
		Il2CppCodeGenWriteBarrier(&___image_20, value);
	}

	inline static int32_t get_offset_of_CurrentDistance_21() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ___CurrentDistance_21)); }
	inline Text_t9039225 * get_CurrentDistance_21() const { return ___CurrentDistance_21; }
	inline Text_t9039225 ** get_address_of_CurrentDistance_21() { return &___CurrentDistance_21; }
	inline void set_CurrentDistance_21(Text_t9039225 * value)
	{
		___CurrentDistance_21 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentDistance_21, value);
	}

	inline static int32_t get_offset_of_MessageSeeBtn_22() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ___MessageSeeBtn_22)); }
	inline Button_t3896396478 * get_MessageSeeBtn_22() const { return ___MessageSeeBtn_22; }
	inline Button_t3896396478 ** get_address_of_MessageSeeBtn_22() { return &___MessageSeeBtn_22; }
	inline void set_MessageSeeBtn_22(Button_t3896396478 * value)
	{
		___MessageSeeBtn_22 = value;
		Il2CppCodeGenWriteBarrier(&___MessageSeeBtn_22, value);
	}

	inline static int32_t get_offset_of__messagecheck_23() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ____messagecheck_23)); }
	inline bool get__messagecheck_23() const { return ____messagecheck_23; }
	inline bool* get_address_of__messagecheck_23() { return &____messagecheck_23; }
	inline void set__messagecheck_23(bool value)
	{
		____messagecheck_23 = value;
	}

	inline static int32_t get_offset_of__distancedouble_24() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ____distancedouble_24)); }
	inline double get__distancedouble_24() const { return ____distancedouble_24; }
	inline double* get_address_of__distancedouble_24() { return &____distancedouble_24; }
	inline void set__distancedouble_24(double value)
	{
		____distancedouble_24 = value;
	}

	inline static int32_t get_offset_of_lat_25() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ___lat_25)); }
	inline String_t* get_lat_25() const { return ___lat_25; }
	inline String_t** get_address_of_lat_25() { return &___lat_25; }
	inline void set_lat_25(String_t* value)
	{
		___lat_25 = value;
		Il2CppCodeGenWriteBarrier(&___lat_25, value);
	}

	inline static int32_t get_offset_of_lon_26() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ___lon_26)); }
	inline String_t* get_lon_26() const { return ___lon_26; }
	inline String_t** get_address_of_lon_26() { return &___lon_26; }
	inline void set_lon_26(String_t* value)
	{
		___lon_26 = value;
		Il2CppCodeGenWriteBarrier(&___lon_26, value);
	}

	inline static int32_t get_offset_of_timer_27() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ___timer_27)); }
	inline float get_timer_27() const { return ___timer_27; }
	inline float* get_address_of_timer_27() { return &___timer_27; }
	inline void set_timer_27(float value)
	{
		___timer_27 = value;
	}

	inline static int32_t get_offset_of_waitingTime_28() { return static_cast<int32_t>(offsetof(GreatmanMessage_t1817744250, ___waitingTime_28)); }
	inline int32_t get_waitingTime_28() const { return ___waitingTime_28; }
	inline int32_t* get_address_of_waitingTime_28() { return &___waitingTime_28; }
	inline void set_waitingTime_28(int32_t value)
	{
		___waitingTime_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
