﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// FPSCounter
struct FPSCounter_t1808341235;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPSCounter/<FPS>c__Iterator0
struct  U3CFPSU3Ec__Iterator0_t1063757940  : public Il2CppObject
{
public:
	// System.Int32 FPSCounter/<FPS>c__Iterator0::<lastFrameCount>__0
	int32_t ___U3ClastFrameCountU3E__0_0;
	// System.Single FPSCounter/<FPS>c__Iterator0::<lastTime>__1
	float ___U3ClastTimeU3E__1_1;
	// System.Single FPSCounter/<FPS>c__Iterator0::<timeSpan>__2
	float ___U3CtimeSpanU3E__2_2;
	// System.Int32 FPSCounter/<FPS>c__Iterator0::<frameCount>__3
	int32_t ___U3CframeCountU3E__3_3;
	// System.Int32 FPSCounter/<FPS>c__Iterator0::$PC
	int32_t ___U24PC_4;
	// System.Object FPSCounter/<FPS>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// FPSCounter FPSCounter/<FPS>c__Iterator0::<>f__this
	FPSCounter_t1808341235 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_U3ClastFrameCountU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t1063757940, ___U3ClastFrameCountU3E__0_0)); }
	inline int32_t get_U3ClastFrameCountU3E__0_0() const { return ___U3ClastFrameCountU3E__0_0; }
	inline int32_t* get_address_of_U3ClastFrameCountU3E__0_0() { return &___U3ClastFrameCountU3E__0_0; }
	inline void set_U3ClastFrameCountU3E__0_0(int32_t value)
	{
		___U3ClastFrameCountU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3ClastTimeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t1063757940, ___U3ClastTimeU3E__1_1)); }
	inline float get_U3ClastTimeU3E__1_1() const { return ___U3ClastTimeU3E__1_1; }
	inline float* get_address_of_U3ClastTimeU3E__1_1() { return &___U3ClastTimeU3E__1_1; }
	inline void set_U3ClastTimeU3E__1_1(float value)
	{
		___U3ClastTimeU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CtimeSpanU3E__2_2() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t1063757940, ___U3CtimeSpanU3E__2_2)); }
	inline float get_U3CtimeSpanU3E__2_2() const { return ___U3CtimeSpanU3E__2_2; }
	inline float* get_address_of_U3CtimeSpanU3E__2_2() { return &___U3CtimeSpanU3E__2_2; }
	inline void set_U3CtimeSpanU3E__2_2(float value)
	{
		___U3CtimeSpanU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CframeCountU3E__3_3() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t1063757940, ___U3CframeCountU3E__3_3)); }
	inline int32_t get_U3CframeCountU3E__3_3() const { return ___U3CframeCountU3E__3_3; }
	inline int32_t* get_address_of_U3CframeCountU3E__3_3() { return &___U3CframeCountU3E__3_3; }
	inline void set_U3CframeCountU3E__3_3(int32_t value)
	{
		___U3CframeCountU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t1063757940, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t1063757940, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t1063757940, ___U3CU3Ef__this_6)); }
	inline FPSCounter_t1808341235 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline FPSCounter_t1808341235 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(FPSCounter_t1808341235 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
