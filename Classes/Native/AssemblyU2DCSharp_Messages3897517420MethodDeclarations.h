﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// Messages
struct Messages_t3897517420;
struct Messages_t3897517420_marshaled_pinvoke;
struct Messages_t3897517420_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Messages3897517420.h"
#include "mscorlib_System_String7231557.h"

// System.Void Messages::_Message(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void Messages__Message_m2086894250 (Messages_t3897517420 * __this, int32_t ____messagecategory0, String_t* ____title1, String_t* ____type2, String_t* ____address3, String_t* ____lat4, String_t* ____lon5, String_t* ____videourl6, String_t* ____videoname7, String_t* ____createdata8, String_t* ____messageid9, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Messages_t3897517420;
struct Messages_t3897517420_marshaled_pinvoke;

extern "C" void Messages_t3897517420_marshal_pinvoke(const Messages_t3897517420& unmarshaled, Messages_t3897517420_marshaled_pinvoke& marshaled);
extern "C" void Messages_t3897517420_marshal_pinvoke_back(const Messages_t3897517420_marshaled_pinvoke& marshaled, Messages_t3897517420& unmarshaled);
extern "C" void Messages_t3897517420_marshal_pinvoke_cleanup(Messages_t3897517420_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Messages_t3897517420;
struct Messages_t3897517420_marshaled_com;

extern "C" void Messages_t3897517420_marshal_com(const Messages_t3897517420& unmarshaled, Messages_t3897517420_marshaled_com& marshaled);
extern "C" void Messages_t3897517420_marshal_com_back(const Messages_t3897517420_marshaled_com& marshaled, Messages_t3897517420& unmarshaled);
extern "C" void Messages_t3897517420_marshal_com_cleanup(Messages_t3897517420_marshaled_com& marshaled);
