﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageCheck/<MessageDeleteServerUpdate>c__Iterator1A
struct U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageCheck/<MessageDeleteServerUpdate>c__Iterator1A::.ctor()
extern "C"  void U3CMessageDeleteServerUpdateU3Ec__Iterator1A__ctor_m1594179352 (U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageCheck/<MessageDeleteServerUpdate>c__Iterator1A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMessageDeleteServerUpdateU3Ec__Iterator1A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m236634362 (U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageCheck/<MessageDeleteServerUpdate>c__Iterator1A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMessageDeleteServerUpdateU3Ec__Iterator1A_System_Collections_IEnumerator_get_Current_m3687970958 (U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MessageCheck/<MessageDeleteServerUpdate>c__Iterator1A::MoveNext()
extern "C"  bool U3CMessageDeleteServerUpdateU3Ec__Iterator1A_MoveNext_m2095154908 (U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck/<MessageDeleteServerUpdate>c__Iterator1A::Dispose()
extern "C"  void U3CMessageDeleteServerUpdateU3Ec__Iterator1A_Dispose_m2896282965 (U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck/<MessageDeleteServerUpdate>c__Iterator1A::Reset()
extern "C"  void U3CMessageDeleteServerUpdateU3Ec__Iterator1A_Reset_m3535579589 (U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
