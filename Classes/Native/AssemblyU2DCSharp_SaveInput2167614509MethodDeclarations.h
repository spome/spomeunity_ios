﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SaveInput
struct SaveInput_t2167614509;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void SaveInput::.ctor()
extern "C"  void SaveInput__ctor_m899828702 (SaveInput_t2167614509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveInput::SaveText()
extern "C"  void SaveInput_SaveText_m650321136 (SaveInput_t2167614509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SaveInput::isContainother(System.String)
extern "C"  bool SaveInput_isContainother_m1480183402 (SaveInput_t2167614509 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
