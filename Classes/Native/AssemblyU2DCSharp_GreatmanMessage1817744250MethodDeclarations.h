﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GreatmanMessage
struct GreatmanMessage_t1817744250;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.WWW
struct WWW_t3134621005;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"

// System.Void GreatmanMessage::.ctor()
extern "C"  void GreatmanMessage__ctor_m1606712625 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreatmanMessage::Awake()
extern "C"  void GreatmanMessage_Awake_m1844317844 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreatmanMessage::PSMButtonClick(System.Int32)
extern "C"  void GreatmanMessage_PSMButtonClick_m1735655312 (GreatmanMessage_t1817744250 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreatmanMessage::MessageSet(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void GreatmanMessage_MessageSet_m2814655900 (GreatmanMessage_t1817744250 * __this, String_t* ___title0, String_t* ___realfilename1, String_t* ___pref2, String_t* ___addr013, String_t* ___addr024, String_t* ___latitude5, String_t* ___longtitude6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreatmanMessage::VideoPlay()
extern "C"  void GreatmanMessage_VideoPlay_m364876254 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GreatmanMessage::VideoDown()
extern "C"  Il2CppObject * GreatmanMessage_VideoDown_m4134881812 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GreatmanMessage::VideoStart()
extern "C"  Il2CppObject * GreatmanMessage_VideoStart_m4177958418 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GreatmanMessage::ImageLoad()
extern "C"  Il2CppObject * GreatmanMessage_ImageLoad_m1200707128 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreatmanMessage::ImageVisible()
extern "C"  void GreatmanMessage_ImageVisible_m2348391626 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GreatmanMessage::CheckProgress(UnityEngine.WWW)
extern "C"  Il2CppObject * GreatmanMessage_CheckProgress_m1738077254 (GreatmanMessage_t1817744250 * __this, WWW_t3134621005 * ____www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreatmanMessage::MapView()
extern "C"  void GreatmanMessage_MapView_m2793212080 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreatmanMessage::OnEnable()
extern "C"  void GreatmanMessage_OnEnable_m236875957 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreatmanMessage::OnDisable()
extern "C"  void GreatmanMessage_OnDisable_m3489124632 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreatmanMessage::Update()
extern "C"  void GreatmanMessage_Update_m4290313212 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
