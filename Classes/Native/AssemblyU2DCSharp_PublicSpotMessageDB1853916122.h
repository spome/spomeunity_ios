﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PublicSpotMessageDB
struct PublicSpotMessageDB_t1853916122;
// System.Collections.Generic.List`1<PublicSpotMessage>
struct List_1_t963411244;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PublicSpotMessageDB
struct  PublicSpotMessageDB_t1853916122  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.List`1<PublicSpotMessage> PublicSpotMessageDB::m_PSMDB
	List_1_t963411244 * ___m_PSMDB_3;

public:
	inline static int32_t get_offset_of_m_PSMDB_3() { return static_cast<int32_t>(offsetof(PublicSpotMessageDB_t1853916122, ___m_PSMDB_3)); }
	inline List_1_t963411244 * get_m_PSMDB_3() const { return ___m_PSMDB_3; }
	inline List_1_t963411244 ** get_address_of_m_PSMDB_3() { return &___m_PSMDB_3; }
	inline void set_m_PSMDB_3(List_1_t963411244 * value)
	{
		___m_PSMDB_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_PSMDB_3, value);
	}
};

struct PublicSpotMessageDB_t1853916122_StaticFields
{
public:
	// PublicSpotMessageDB PublicSpotMessageDB::_instance
	PublicSpotMessageDB_t1853916122 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(PublicSpotMessageDB_t1853916122_StaticFields, ____instance_2)); }
	inline PublicSpotMessageDB_t1853916122 * get__instance_2() const { return ____instance_2; }
	inline PublicSpotMessageDB_t1853916122 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(PublicSpotMessageDB_t1853916122 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
