﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CurrentScreen/<JsonStart>c__Iterator5
struct U3CJsonStartU3Ec__Iterator5_t1185739994;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CurrentScreen/<JsonStart>c__Iterator5::.ctor()
extern "C"  void U3CJsonStartU3Ec__Iterator5__ctor_m3081243089 (U3CJsonStartU3Ec__Iterator5_t1185739994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CurrentScreen/<JsonStart>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CJsonStartU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1881566689 (U3CJsonStartU3Ec__Iterator5_t1185739994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CurrentScreen/<JsonStart>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CJsonStartU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m747262837 (U3CJsonStartU3Ec__Iterator5_t1185739994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CurrentScreen/<JsonStart>c__Iterator5::MoveNext()
extern "C"  bool U3CJsonStartU3Ec__Iterator5_MoveNext_m2900864387 (U3CJsonStartU3Ec__Iterator5_t1185739994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen/<JsonStart>c__Iterator5::Dispose()
extern "C"  void U3CJsonStartU3Ec__Iterator5_Dispose_m1740424654 (U3CJsonStartU3Ec__Iterator5_t1185739994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentScreen/<JsonStart>c__Iterator5::Reset()
extern "C"  void U3CJsonStartU3Ec__Iterator5_Reset_m727676030 (U3CJsonStartU3Ec__Iterator5_t1185739994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
