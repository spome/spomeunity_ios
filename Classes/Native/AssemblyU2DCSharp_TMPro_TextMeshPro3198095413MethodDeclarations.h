﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.TextMeshPro
struct TextMeshPro_t3198095413;
// UnityEngine.Material
struct Material_t3870600107;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// TMPro.TextContainer
struct TextContainer_t2231787766;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Renderer
struct Renderer_t3076687687;
// UnityEngine.Mesh
struct Mesh_t4241756145;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_MaskingTypes406604089.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_TMPro_TMP_Compatibility_AnchorPo2956782356.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate2847075725.h"

// System.Void TMPro.TextMeshPro::.ctor()
extern "C"  void TextMeshPro__ctor_m2011347858 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::Awake()
extern "C"  void TextMeshPro_Awake_m2248953077 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::OnEnable()
extern "C"  void TextMeshPro_OnEnable_m3046869684 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::OnDisable()
extern "C"  void TextMeshPro_OnDisable_m404616953 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::OnDestroy()
extern "C"  void TextMeshPro_OnDestroy_m2582149003 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::LoadFontAsset()
extern "C"  void TextMeshPro_LoadFontAsset_m2189413515 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::ScheduleUpdate()
extern "C"  void TextMeshPro_ScheduleUpdate_m242104434 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::UpdateEnvMapMatrix()
extern "C"  void TextMeshPro_UpdateEnvMapMatrix_m4174700715 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::SetMask(TMPro.MaskingTypes)
extern "C"  void TextMeshPro_SetMask_m618966337 (TextMeshPro_t3198095413 * __this, int32_t ___maskType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::SetMaskCoordinates(UnityEngine.Vector4)
extern "C"  void TextMeshPro_SetMaskCoordinates_m1380246649 (TextMeshPro_t3198095413 * __this, Vector4_t4282066567  ___coords0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::SetMaskCoordinates(UnityEngine.Vector4,System.Single,System.Single)
extern "C"  void TextMeshPro_SetMaskCoordinates_m948667907 (TextMeshPro_t3198095413 * __this, Vector4_t4282066567  ___coords0, float ___softX1, float ___softY2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::EnableMasking()
extern "C"  void TextMeshPro_EnableMasking_m529666915 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::DisableMasking()
extern "C"  void TextMeshPro_DisableMasking_m2521363488 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::UpdateMask()
extern "C"  void TextMeshPro_UpdateMask_m1102021831 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::SetFontMaterial(UnityEngine.Material)
extern "C"  void TextMeshPro_SetFontMaterial_m852725482 (TextMeshPro_t3198095413 * __this, Material_t3870600107 * ___mat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::SetFontSharedMaterial(UnityEngine.Material)
extern "C"  void TextMeshPro_SetFontSharedMaterial_m2966130863 (TextMeshPro_t3198095413 * __this, Material_t3870600107 * ___mat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::SetOutlineThickness(System.Single)
extern "C"  void TextMeshPro_SetOutlineThickness_m3504332007 (TextMeshPro_t3198095413 * __this, float ___thickness0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::SetFaceColor(UnityEngine.Color32)
extern "C"  void TextMeshPro_SetFaceColor_m1001364081 (TextMeshPro_t3198095413 * __this, Color32_t598853688  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::SetOutlineColor(UnityEngine.Color32)
extern "C"  void TextMeshPro_SetOutlineColor_m1334851220 (TextMeshPro_t3198095413 * __this, Color32_t598853688  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::CreateMaterialInstance()
extern "C"  void TextMeshPro_CreateMaterialInstance_m2782688042 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::SetShaderType()
extern "C"  void TextMeshPro_SetShaderType_m3401931825 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::SetCulling()
extern "C"  void TextMeshPro_SetCulling_m3230402976 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::SetPerspectiveCorrection()
extern "C"  void TextMeshPro_SetPerspectiveCorrection_m1865332330 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TextMeshPro::GetPaddingForMaterial(UnityEngine.Material)
extern "C"  float TextMeshPro_GetPaddingForMaterial_m2002222547 (TextMeshPro_t3198095413 * __this, Material_t3870600107 * ___mat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TextMeshPro::GetPaddingForMaterial()
extern "C"  float TextMeshPro_GetPaddingForMaterial_m2197900657 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::SetMeshArrays(System.Int32)
extern "C"  void TextMeshPro_SetMeshArrays_m1056196298 (TextMeshPro_t3198095413 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TextMeshPro::SetArraySizes(System.Int32[])
extern "C"  int32_t TextMeshPro_SetArraySizes_m3660301736 (TextMeshPro_t3198095413 * __this, Int32U5BU5D_t3230847821* ___chars0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::ComputeMarginSize()
extern "C"  void TextMeshPro_ComputeMarginSize_m1266167574 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::OnDidApplyAnimationProperties()
extern "C"  void TextMeshPro_OnDidApplyAnimationProperties_m4109330745 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::OnTransformParentChanged()
extern "C"  void TextMeshPro_OnTransformParentChanged_m43982095 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::OnRectTransformDimensionsChange()
extern "C"  void TextMeshPro_OnRectTransformDimensionsChange_m1310676150 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::LateUpdate()
extern "C"  void TextMeshPro_LateUpdate_m2979199489 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::OnPreRenderObject()
extern "C"  void TextMeshPro_OnPreRenderObject_m12255945 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::GenerateTextMesh()
extern "C"  void TextMeshPro_GenerateTextMesh_m1243980673 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] TMPro.TextMeshPro::GetTextContainerLocalCorners()
extern "C"  Vector3U5BU5D_t215400611* TextMeshPro_GetTextContainerLocalCorners_m2711871851 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::DrawUnderlineMesh(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32&,System.Single,System.Single,System.Single,UnityEngine.Color32)
extern "C"  void TextMeshPro_DrawUnderlineMesh_m2616077836 (TextMeshPro_t3198095413 * __this, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, int32_t* ___index2, float ___startScale3, float ___endScale4, float ___maxScale5, Color32_t598853688  ___underlineColor6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::ClearMesh()
extern "C"  void TextMeshPro_ClearMesh_m3150050442 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::UpdateSDFScale(System.Single,System.Single)
extern "C"  void TextMeshPro_UpdateSDFScale_m3803923104 (TextMeshPro_t3198095413 * __this, float ___prevScale0, float ___newScale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::AdjustLineOffset(System.Int32,System.Int32,System.Single)
extern "C"  void TextMeshPro_AdjustLineOffset_m4097432451 (TextMeshPro_t3198095413 * __this, int32_t ___startIndex0, int32_t ___endIndex1, float ___offset2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material TMPro.TextMeshPro::get_fontSharedMaterial()
extern "C"  Material_t3870600107 * TextMeshPro_get_fontSharedMaterial_m3684536183 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::set_fontSharedMaterial(UnityEngine.Material)
extern "C"  void TextMeshPro_set_fontSharedMaterial_m1372216012 (TextMeshPro_t3198095413 * __this, Material_t3870600107 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TextMeshPro::get_lineLength()
extern "C"  float TextMeshPro_get_lineLength_m3073350025 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::set_lineLength(System.Single)
extern "C"  void TextMeshPro_set_lineLength_m2855129090 (TextMeshPro_t3198095413 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TMP_Compatibility/AnchorPositions TMPro.TextMeshPro::get_anchor()
extern "C"  int32_t TextMeshPro_get_anchor_m3294984817 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 TMPro.TextMeshPro::get_margin()
extern "C"  Vector4_t4282066567  TextMeshPro_get_margin_m2070640336 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::set_margin(UnityEngine.Vector4)
extern "C"  void TextMeshPro_set_margin_m870321371 (TextMeshPro_t3198095413 * __this, Vector4_t4282066567  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TextMeshPro::get_sortingLayerID()
extern "C"  int32_t TextMeshPro_get_sortingLayerID_m3608055701 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::set_sortingLayerID(System.Int32)
extern "C"  void TextMeshPro_set_sortingLayerID_m2018344104 (TextMeshPro_t3198095413 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TextMeshPro::get_sortingOrder()
extern "C"  int32_t TextMeshPro_get_sortingOrder_m1462059575 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::set_sortingOrder(System.Int32)
extern "C"  void TextMeshPro_set_sortingOrder_m2590651594 (TextMeshPro_t3198095413 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TextMeshPro::get_autoSizeTextContainer()
extern "C"  bool TextMeshPro_get_autoSizeTextContainer_m1143923071 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::set_autoSizeTextContainer(System.Boolean)
extern "C"  void TextMeshPro_set_autoSizeTextContainer_m3887450126 (TextMeshPro_t3198095413 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TextContainer TMPro.TextMeshPro::get_textContainer()
extern "C"  TextContainer_t2231787766 * TextMeshPro_get_textContainer_m1000409748 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform TMPro.TextMeshPro::get_transform()
extern "C"  Transform_t1659122786 * TextMeshPro_get_transform_m2498742193 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Renderer TMPro.TextMeshPro::get_renderer()
extern "C"  Renderer_t3076687687 * TextMeshPro_get_renderer_m1893477819 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh TMPro.TextMeshPro::get_mesh()
extern "C"  Mesh_t4241756145 * TextMeshPro_get_mesh_m1577852047 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds TMPro.TextMeshPro::get_bounds()
extern "C"  Bounds_t2711641849  TextMeshPro_get_bounds_m4128021343 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.MaskingTypes TMPro.TextMeshPro::get_maskType()
extern "C"  int32_t TextMeshPro_get_maskType_m1679664769 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::set_maskType(TMPro.MaskingTypes)
extern "C"  void TextMeshPro_set_maskType_m1183758008 (TextMeshPro_t3198095413 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::SetMask(TMPro.MaskingTypes,UnityEngine.Vector4)
extern "C"  void TextMeshPro_SetMask_m1806690419 (TextMeshPro_t3198095413 * __this, int32_t ___type0, Vector4_t4282066567  ___maskCoords1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::SetMask(TMPro.MaskingTypes,UnityEngine.Vector4,System.Single,System.Single)
extern "C"  void TextMeshPro_SetMask_m2603167485 (TextMeshPro_t3198095413 * __this, int32_t ___type0, Vector4_t4282066567  ___maskCoords1, float ___softnessX2, float ___softnessY3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::SetVerticesDirty()
extern "C"  void TextMeshPro_SetVerticesDirty_m3335220809 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::SetLayoutDirty()
extern "C"  void TextMeshPro_SetLayoutDirty_m4052373944 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::Rebuild(UnityEngine.UI.CanvasUpdate)
extern "C"  void TextMeshPro_Rebuild_m4126731715 (TextMeshPro_t3198095413 * __this, int32_t ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::UpdateMeshPadding()
extern "C"  void TextMeshPro_UpdateMeshPadding_m2802464843 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::ForceMeshUpdate()
extern "C"  void TextMeshPro_ForceMeshUpdate_m2284048401 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::UpdateFontAsset()
extern "C"  void TextMeshPro_UpdateFontAsset_m3247811848 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::CalculateLayoutInputHorizontal()
extern "C"  void TextMeshPro_CalculateLayoutInputHorizontal_m4010384368 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TextMeshPro::CalculateLayoutInputVertical()
extern "C"  void TextMeshPro_CalculateLayoutInputVertical_m2862309762 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TextMeshPro::UnityEngine.UI.ILayoutElement.get_minWidth()
extern "C"  float TextMeshPro_UnityEngine_UI_ILayoutElement_get_minWidth_m3640316805 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TextMeshPro::UnityEngine.UI.ILayoutElement.get_flexibleWidth()
extern "C"  float TextMeshPro_UnityEngine_UI_ILayoutElement_get_flexibleWidth_m3615670284 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TextMeshPro::UnityEngine.UI.ILayoutElement.get_minHeight()
extern "C"  float TextMeshPro_UnityEngine_UI_ILayoutElement_get_minHeight_m1684485610 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TMPro.TextMeshPro::UnityEngine.UI.ILayoutElement.get_flexibleHeight()
extern "C"  float TextMeshPro_UnityEngine_UI_ILayoutElement_get_flexibleHeight_m920443459 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TextMeshPro::UnityEngine.UI.ILayoutElement.get_layoutPriority()
extern "C"  int32_t TextMeshPro_UnityEngine_UI_ILayoutElement_get_layoutPriority_m352258397 (TextMeshPro_t3198095413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
