﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t3134621005;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;
// GreatmanMessage
struct GreatmanMessage_t1817744250;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GreatmanMessage/<VideoDown>c__IteratorF
struct  U3CVideoDownU3Ec__IteratorF_t830887869  : public Il2CppObject
{
public:
	// UnityEngine.WWW GreatmanMessage/<VideoDown>c__IteratorF::<www>__0
	WWW_t3134621005 * ___U3CwwwU3E__0_0;
	// System.Byte[] GreatmanMessage/<VideoDown>c__IteratorF::<bytes>__1
	ByteU5BU5D_t4260760469* ___U3CbytesU3E__1_1;
	// System.Int32 GreatmanMessage/<VideoDown>c__IteratorF::$PC
	int32_t ___U24PC_2;
	// System.Object GreatmanMessage/<VideoDown>c__IteratorF::$current
	Il2CppObject * ___U24current_3;
	// GreatmanMessage GreatmanMessage/<VideoDown>c__IteratorF::<>f__this
	GreatmanMessage_t1817744250 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CVideoDownU3Ec__IteratorF_t830887869, ___U3CwwwU3E__0_0)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CbytesU3E__1_1() { return static_cast<int32_t>(offsetof(U3CVideoDownU3Ec__IteratorF_t830887869, ___U3CbytesU3E__1_1)); }
	inline ByteU5BU5D_t4260760469* get_U3CbytesU3E__1_1() const { return ___U3CbytesU3E__1_1; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CbytesU3E__1_1() { return &___U3CbytesU3E__1_1; }
	inline void set_U3CbytesU3E__1_1(ByteU5BU5D_t4260760469* value)
	{
		___U3CbytesU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbytesU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CVideoDownU3Ec__IteratorF_t830887869, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CVideoDownU3Ec__IteratorF_t830887869, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CVideoDownU3Ec__IteratorF_t830887869, ___U3CU3Ef__this_4)); }
	inline GreatmanMessage_t1817744250 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline GreatmanMessage_t1817744250 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(GreatmanMessage_t1817744250 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
