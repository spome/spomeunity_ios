﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Mesh
struct Mesh_t4241756145;
// TMPro.TMP_MeshInfo
struct TMP_MeshInfo_t801067801;
struct TMP_MeshInfo_t801067801_marshaled_pinvoke;
struct TMP_MeshInfo_t801067801_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_TMP_MeshInfo801067801.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"

// System.Void TMPro.TMP_MeshInfo::.ctor(UnityEngine.Mesh,System.Int32)
extern "C"  void TMP_MeshInfo__ctor_m3019579357 (TMP_MeshInfo_t801067801 * __this, Mesh_t4241756145 * ___mesh0, int32_t ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_MeshInfo::.cctor()
extern "C"  void TMP_MeshInfo__cctor_m1284868879 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_MeshInfo::ResizeMeshInfo(System.Int32)
extern "C"  void TMP_MeshInfo_ResizeMeshInfo_m3935303174 (TMP_MeshInfo_t801067801 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_MeshInfo::Clear()
extern "C"  void TMP_MeshInfo_Clear_m1896646793 (TMP_MeshInfo_t801067801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct TMP_MeshInfo_t801067801;
struct TMP_MeshInfo_t801067801_marshaled_pinvoke;

extern "C" void TMP_MeshInfo_t801067801_marshal_pinvoke(const TMP_MeshInfo_t801067801& unmarshaled, TMP_MeshInfo_t801067801_marshaled_pinvoke& marshaled);
extern "C" void TMP_MeshInfo_t801067801_marshal_pinvoke_back(const TMP_MeshInfo_t801067801_marshaled_pinvoke& marshaled, TMP_MeshInfo_t801067801& unmarshaled);
extern "C" void TMP_MeshInfo_t801067801_marshal_pinvoke_cleanup(TMP_MeshInfo_t801067801_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct TMP_MeshInfo_t801067801;
struct TMP_MeshInfo_t801067801_marshaled_com;

extern "C" void TMP_MeshInfo_t801067801_marshal_com(const TMP_MeshInfo_t801067801& unmarshaled, TMP_MeshInfo_t801067801_marshaled_com& marshaled);
extern "C" void TMP_MeshInfo_t801067801_marshal_com_back(const TMP_MeshInfo_t801067801_marshaled_com& marshaled, TMP_MeshInfo_t801067801& unmarshaled);
extern "C" void TMP_MeshInfo_t801067801_marshal_com_cleanup(TMP_MeshInfo_t801067801_marshaled_com& marshaled);
