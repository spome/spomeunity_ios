﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ErrorMessage/<AndroidAlertDialog>c__AnonStorey24
struct U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220;

#include "codegen/il2cpp-codegen.h"

// System.Void ErrorMessage/<AndroidAlertDialog>c__AnonStorey24::.ctor()
extern "C"  void U3CAndroidAlertDialogU3Ec__AnonStorey24__ctor_m527978479 (U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessage/<AndroidAlertDialog>c__AnonStorey24::<>m__1()
extern "C"  void U3CAndroidAlertDialogU3Ec__AnonStorey24_U3CU3Em__1_m3418336283 (U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
