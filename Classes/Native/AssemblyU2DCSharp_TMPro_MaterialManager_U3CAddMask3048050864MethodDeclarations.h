﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.MaterialManager/<AddMaskingMaterial>c__AnonStorey2D
struct U3CAddMaskingMaterialU3Ec__AnonStorey2D_t3048050864;
// TMPro.MaterialManager/MaskingMaterial
struct MaskingMaterial_t2640101498;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_MaterialManager_MaskingMat2640101498.h"

// System.Void TMPro.MaterialManager/<AddMaskingMaterial>c__AnonStorey2D::.ctor()
extern "C"  void U3CAddMaskingMaterialU3Ec__AnonStorey2D__ctor_m3508067003 (U3CAddMaskingMaterialU3Ec__AnonStorey2D_t3048050864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.MaterialManager/<AddMaskingMaterial>c__AnonStorey2D::<>m__B(TMPro.MaterialManager/MaskingMaterial)
extern "C"  bool U3CAddMaskingMaterialU3Ec__AnonStorey2D_U3CU3Em__B_m4107808140 (U3CAddMaskingMaterialU3Ec__AnonStorey2D_t3048050864 * __this, MaskingMaterial_t2640101498 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
