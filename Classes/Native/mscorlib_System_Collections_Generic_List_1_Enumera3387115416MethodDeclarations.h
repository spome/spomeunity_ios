﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<TMPro.TMP_Glyph>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2616394417(__this, ___l0, method) ((  void (*) (Enumerator_t3387115416 *, List_1_t3367442646 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TMPro.TMP_Glyph>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2013226753(__this, method) ((  void (*) (Enumerator_t3387115416 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<TMPro.TMP_Glyph>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2039874733(__this, method) ((  Il2CppObject * (*) (Enumerator_t3387115416 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TMPro.TMP_Glyph>::Dispose()
#define Enumerator_Dispose_m228756694(__this, method) ((  void (*) (Enumerator_t3387115416 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TMPro.TMP_Glyph>::VerifyState()
#define Enumerator_VerifyState_m2524442895(__this, method) ((  void (*) (Enumerator_t3387115416 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<TMPro.TMP_Glyph>::MoveNext()
#define Enumerator_MoveNext_m543583460(__this, method) ((  bool (*) (Enumerator_t3387115416 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<TMPro.TMP_Glyph>::get_Current()
#define Enumerator_get_Current_m772854828(__this, method) ((  TMP_Glyph_t1999257094 * (*) (Enumerator_t3387115416 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
