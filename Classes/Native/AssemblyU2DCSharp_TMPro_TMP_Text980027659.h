﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t967550395;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_FontAsset>
struct Dictionary_2_t964813634;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material>
struct Dictionary_2_t3867863346;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t270066265;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// System.Char[]
struct CharU5BU5D_t3324145743;
// TMPro.XML_TagAttribute[]
struct XML_TagAttributeU5BU5D_t3301714177;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t2688989516;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t4230808090;
// TMPro.TMP_Settings
struct TMP_Settings_t2708659969;
// TMPro.TMP_TextElement
struct TMP_TextElement_t1232249705;
// TMPro.TMP_Glyph
struct TMP_Glyph_t1999257094;
// TMPro.InlineGraphicManager
struct InlineGraphicManager_t3583857972;
// System.Single[]
struct SingleU5BU5D_t2316563989;

#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic3186046376.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "AssemblyU2DCSharp_TMPro_VertexGradient2166651658.h"
#include "AssemblyU2DCSharp_TMPro_TMP_XmlTagStack_1_gen1206294321.h"
#include "AssemblyU2DCSharp_TMPro_FontStyles3228051751.h"
#include "AssemblyU2DCSharp_TMPro_TextAlignmentOptions3798547742.h"
#include "AssemblyU2DCSharp_TMPro_TextOverflowModes832755779.h"
#include "AssemblyU2DCSharp_TMPro_TextureMappingOptions707307789.h"
#include "AssemblyU2DCSharp_TMPro_TextRenderFlags2400171206.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "AssemblyU2DCSharp_TMPro_TMP_Text_TextInputSources2987044283.h"
#include "AssemblyU2DCSharp_TMPro_TMP_LinkInfo2467896998.h"
#include "AssemblyU2DCSharp_TMPro_Extents2060714539.h"
#include "AssemblyU2DCSharp_TMPro_TMP_XmlTagStack_1_gen1808196333.h"
#include "AssemblyU2DCSharp_TMPro_TMP_XmlTagStack_1_gen2363181145.h"
#include "AssemblyU2DCSharp_TMPro_TMP_TextElementType3665184835.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text
struct  TMP_Text_t980027659  : public MaskableGraphic_t3186046376
{
public:
	// System.String TMPro.TMP_Text::m_text
	String_t* ___m_text_28;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_fontAsset
	TMP_FontAsset_t967550395 * ___m_fontAsset_29;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_currentFontAsset
	TMP_FontAsset_t967550395 * ___m_currentFontAsset_30;
	// System.Int32 TMPro.TMP_Text::m_fontIndex
	int32_t ___m_fontIndex_31;
	// System.Boolean TMPro.TMP_Text::m_isSDFShader
	bool ___m_isSDFShader_32;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_FontAsset> TMPro.TMP_Text::m_fontAsset_Dict
	Dictionary_2_t964813634 * ___m_fontAsset_Dict_33;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material> TMPro.TMP_Text::m_fontMaterial_Dict
	Dictionary_2_t3867863346 * ___m_fontMaterial_Dict_34;
	// UnityEngine.Material TMPro.TMP_Text::m_sharedMaterial
	Material_t3870600107 * ___m_sharedMaterial_35;
	// UnityEngine.Material TMPro.TMP_Text::m_currentMaterial
	Material_t3870600107 * ___m_currentMaterial_36;
	// UnityEngine.Material TMPro.TMP_Text::m_fontMaterial
	Material_t3870600107 * ___m_fontMaterial_37;
	// UnityEngine.Material TMPro.TMP_Text::m_baseMaterial
	Material_t3870600107 * ___m_baseMaterial_38;
	// UnityEngine.Material TMPro.TMP_Text::m_lastBaseMaterial
	Material_t3870600107 * ___m_lastBaseMaterial_39;
	// System.Boolean TMPro.TMP_Text::m_isNewBaseMaterial
	bool ___m_isNewBaseMaterial_40;
	// UnityEngine.Material TMPro.TMP_Text::m_maskingMaterial
	Material_t3870600107 * ___m_maskingMaterial_41;
	// UnityEngine.Color TMPro.TMP_Text::m_fontColor
	Color_t4194546905  ___m_fontColor_42;
	// UnityEngine.Color32 TMPro.TMP_Text::m_fontColor32
	Color32_t598853688  ___m_fontColor32_43;
	// System.Boolean TMPro.TMP_Text::m_tintAllSprites
	bool ___m_tintAllSprites_45;
	// System.Boolean TMPro.TMP_Text::m_tintSprite
	bool ___m_tintSprite_46;
	// UnityEngine.Color32 TMPro.TMP_Text::m_spriteColor
	Color32_t598853688  ___m_spriteColor_47;
	// System.Boolean TMPro.TMP_Text::m_enableVertexGradient
	bool ___m_enableVertexGradient_48;
	// TMPro.VertexGradient TMPro.TMP_Text::m_fontColorGradient
	VertexGradient_t2166651658  ___m_fontColorGradient_49;
	// System.Boolean TMPro.TMP_Text::m_overrideHtmlColors
	bool ___m_overrideHtmlColors_50;
	// UnityEngine.Color32 TMPro.TMP_Text::m_faceColor
	Color32_t598853688  ___m_faceColor_51;
	// UnityEngine.Color32 TMPro.TMP_Text::m_outlineColor
	Color32_t598853688  ___m_outlineColor_52;
	// System.Single TMPro.TMP_Text::m_outlineWidth
	float ___m_outlineWidth_53;
	// System.Single TMPro.TMP_Text::m_fontSize
	float ___m_fontSize_54;
	// System.Single TMPro.TMP_Text::m_currentFontSize
	float ___m_currentFontSize_55;
	// System.Single TMPro.TMP_Text::m_fontSizeBase
	float ___m_fontSizeBase_56;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_sizeStack
	TMP_XmlTagStack_1_t1206294321  ___m_sizeStack_57;
	// System.Boolean TMPro.TMP_Text::m_enableAutoSizing
	bool ___m_enableAutoSizing_58;
	// System.Single TMPro.TMP_Text::m_maxFontSize
	float ___m_maxFontSize_59;
	// System.Single TMPro.TMP_Text::m_minFontSize
	float ___m_minFontSize_60;
	// System.Single TMPro.TMP_Text::m_fontSizeMin
	float ___m_fontSizeMin_61;
	// System.Single TMPro.TMP_Text::m_fontSizeMax
	float ___m_fontSizeMax_62;
	// TMPro.FontStyles TMPro.TMP_Text::m_fontStyle
	int32_t ___m_fontStyle_63;
	// TMPro.FontStyles TMPro.TMP_Text::m_style
	int32_t ___m_style_64;
	// System.Boolean TMPro.TMP_Text::m_isUsingBold
	bool ___m_isUsingBold_65;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_textAlignment
	int32_t ___m_textAlignment_66;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_lineJustification
	int32_t ___m_lineJustification_67;
	// UnityEngine.Vector3[] TMPro.TMP_Text::m_textContainerLocalCorners
	Vector3U5BU5D_t215400611* ___m_textContainerLocalCorners_68;
	// System.Single TMPro.TMP_Text::m_characterSpacing
	float ___m_characterSpacing_69;
	// System.Single TMPro.TMP_Text::m_cSpacing
	float ___m_cSpacing_70;
	// System.Single TMPro.TMP_Text::m_monoSpacing
	float ___m_monoSpacing_71;
	// System.Single TMPro.TMP_Text::m_lineSpacing
	float ___m_lineSpacing_72;
	// System.Single TMPro.TMP_Text::m_lineSpacingDelta
	float ___m_lineSpacingDelta_73;
	// System.Single TMPro.TMP_Text::m_lineHeight
	float ___m_lineHeight_74;
	// System.Single TMPro.TMP_Text::m_lineSpacingMax
	float ___m_lineSpacingMax_75;
	// System.Single TMPro.TMP_Text::m_paragraphSpacing
	float ___m_paragraphSpacing_76;
	// System.Single TMPro.TMP_Text::m_charWidthMaxAdj
	float ___m_charWidthMaxAdj_77;
	// System.Single TMPro.TMP_Text::m_charWidthAdjDelta
	float ___m_charWidthAdjDelta_78;
	// System.Boolean TMPro.TMP_Text::m_enableWordWrapping
	bool ___m_enableWordWrapping_79;
	// System.Boolean TMPro.TMP_Text::m_isCharacterWrappingEnabled
	bool ___m_isCharacterWrappingEnabled_80;
	// System.Boolean TMPro.TMP_Text::m_isNonBreakingSpace
	bool ___m_isNonBreakingSpace_81;
	// System.Boolean TMPro.TMP_Text::m_isIgnoringAlignment
	bool ___m_isIgnoringAlignment_82;
	// System.Single TMPro.TMP_Text::m_wordWrappingRatios
	float ___m_wordWrappingRatios_83;
	// TMPro.TextOverflowModes TMPro.TMP_Text::m_overflowMode
	int32_t ___m_overflowMode_84;
	// System.Boolean TMPro.TMP_Text::m_isTextTruncated
	bool ___m_isTextTruncated_85;
	// System.Boolean TMPro.TMP_Text::m_enableKerning
	bool ___m_enableKerning_86;
	// System.Boolean TMPro.TMP_Text::m_enableExtraPadding
	bool ___m_enableExtraPadding_87;
	// System.Boolean TMPro.TMP_Text::checkPaddingRequired
	bool ___checkPaddingRequired_88;
	// System.Boolean TMPro.TMP_Text::m_isRichText
	bool ___m_isRichText_89;
	// System.Boolean TMPro.TMP_Text::m_parseCtrlCharacters
	bool ___m_parseCtrlCharacters_90;
	// System.Boolean TMPro.TMP_Text::m_isOverlay
	bool ___m_isOverlay_91;
	// System.Boolean TMPro.TMP_Text::m_isOrthographic
	bool ___m_isOrthographic_92;
	// System.Boolean TMPro.TMP_Text::m_isCullingEnabled
	bool ___m_isCullingEnabled_93;
	// System.Boolean TMPro.TMP_Text::m_ignoreCulling
	bool ___m_ignoreCulling_94;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_horizontalMapping
	int32_t ___m_horizontalMapping_95;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_verticalMapping
	int32_t ___m_verticalMapping_96;
	// TMPro.TextRenderFlags TMPro.TMP_Text::m_renderMode
	int32_t ___m_renderMode_97;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleCharacters
	int32_t ___m_maxVisibleCharacters_98;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleWords
	int32_t ___m_maxVisibleWords_99;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleLines
	int32_t ___m_maxVisibleLines_100;
	// System.Int32 TMPro.TMP_Text::m_pageToDisplay
	int32_t ___m_pageToDisplay_101;
	// System.Boolean TMPro.TMP_Text::m_isNewPage
	bool ___m_isNewPage_102;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_margin
	Vector4_t4282066567  ___m_margin_103;
	// System.Single TMPro.TMP_Text::m_marginLeft
	float ___m_marginLeft_104;
	// System.Single TMPro.TMP_Text::m_marginRight
	float ___m_marginRight_105;
	// System.Single TMPro.TMP_Text::m_marginWidth
	float ___m_marginWidth_106;
	// System.Single TMPro.TMP_Text::m_marginHeight
	float ___m_marginHeight_107;
	// System.Single TMPro.TMP_Text::m_width
	float ___m_width_108;
	// TMPro.TMP_TextInfo TMPro.TMP_Text::m_textInfo
	TMP_TextInfo_t270066265 * ___m_textInfo_109;
	// System.Boolean TMPro.TMP_Text::m_havePropertiesChanged
	bool ___m_havePropertiesChanged_110;
	// UnityEngine.Transform TMPro.TMP_Text::m_transform
	Transform_t1659122786 * ___m_transform_111;
	// UnityEngine.RectTransform TMPro.TMP_Text::m_rectTransform
	RectTransform_t972643934 * ___m_rectTransform_112;
	// UnityEngine.Mesh TMPro.TMP_Text::m_mesh
	Mesh_t4241756145 * ___m_mesh_113;
	// System.Single TMPro.TMP_Text::m_flexibleHeight
	float ___m_flexibleHeight_114;
	// System.Single TMPro.TMP_Text::m_flexibleWidth
	float ___m_flexibleWidth_115;
	// System.Single TMPro.TMP_Text::m_minHeight
	float ___m_minHeight_116;
	// System.Single TMPro.TMP_Text::m_minWidth
	float ___m_minWidth_117;
	// System.Single TMPro.TMP_Text::m_preferredWidth
	float ___m_preferredWidth_118;
	// System.Single TMPro.TMP_Text::m_renderedWidth
	float ___m_renderedWidth_119;
	// System.Single TMPro.TMP_Text::m_preferredHeight
	float ___m_preferredHeight_120;
	// System.Single TMPro.TMP_Text::m_renderedHeight
	float ___m_renderedHeight_121;
	// System.Int32 TMPro.TMP_Text::m_layoutPriority
	int32_t ___m_layoutPriority_122;
	// System.Boolean TMPro.TMP_Text::m_isCalculateSizeRequired
	bool ___m_isCalculateSizeRequired_123;
	// System.Boolean TMPro.TMP_Text::m_isLayoutDirty
	bool ___m_isLayoutDirty_124;
	// System.Boolean TMPro.TMP_Text::m_verticesAlreadyDirty
	bool ___m_verticesAlreadyDirty_125;
	// System.Boolean TMPro.TMP_Text::m_layoutAlreadyDirty
	bool ___m_layoutAlreadyDirty_126;
	// System.Boolean TMPro.TMP_Text::m_isInputParsingRequired
	bool ___m_isInputParsingRequired_127;
	// TMPro.TMP_Text/TextInputSources TMPro.TMP_Text::m_inputSource
	int32_t ___m_inputSource_128;
	// System.String TMPro.TMP_Text::old_text
	String_t* ___old_text_129;
	// System.Single TMPro.TMP_Text::old_arg0
	float ___old_arg0_130;
	// System.Single TMPro.TMP_Text::old_arg1
	float ___old_arg1_131;
	// System.Single TMPro.TMP_Text::old_arg2
	float ___old_arg2_132;
	// System.Single TMPro.TMP_Text::m_fontScale
	float ___m_fontScale_133;
	// System.Char[] TMPro.TMP_Text::m_htmlTag
	CharU5BU5D_t3324145743* ___m_htmlTag_134;
	// TMPro.XML_TagAttribute[] TMPro.TMP_Text::m_xmlAttribute
	XML_TagAttributeU5BU5D_t3301714177* ___m_xmlAttribute_135;
	// System.Single TMPro.TMP_Text::tag_LineIndent
	float ___tag_LineIndent_136;
	// System.Single TMPro.TMP_Text::tag_Indent
	float ___tag_Indent_137;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_indentStack
	TMP_XmlTagStack_1_t1206294321  ___m_indentStack_138;
	// System.Boolean TMPro.TMP_Text::tag_NoParsing
	bool ___tag_NoParsing_139;
	// TMPro.TMP_LinkInfo TMPro.TMP_Text::tag_LinkInfo
	TMP_LinkInfo_t2467896998  ___tag_LinkInfo_140;
	// System.Boolean TMPro.TMP_Text::m_isParsingText
	bool ___m_isParsingText_141;
	// System.Int32[] TMPro.TMP_Text::m_char_buffer
	Int32U5BU5D_t3230847821* ___m_char_buffer_142;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_Text::m_internalCharacterInfo
	TMP_CharacterInfoU5BU5D_t2688989516* ___m_internalCharacterInfo_143;
	// System.Char[] TMPro.TMP_Text::m_input_CharArray
	CharU5BU5D_t3324145743* ___m_input_CharArray_144;
	// System.Int32 TMPro.TMP_Text::m_charArray_Length
	int32_t ___m_charArray_Length_145;
	// System.Collections.Generic.List`1<System.Char> TMPro.TMP_Text::m_VisibleCharacters
	List_1_t4230808090 * ___m_VisibleCharacters_146;
	// TMPro.TMP_Settings TMPro.TMP_Text::m_settings
	TMP_Settings_t2708659969 * ___m_settings_147;
	// System.Boolean TMPro.TMP_Text::m_isNewTextObject
	bool ___m_isNewTextObject_148;
	// System.Boolean TMPro.TMP_Text::m_warningsDisabled
	bool ___m_warningsDisabled_149;
	// System.Int32 TMPro.TMP_Text::m_characterCount
	int32_t ___m_characterCount_150;
	// System.Int32 TMPro.TMP_Text::m_visibleCharacterCount
	int32_t ___m_visibleCharacterCount_151;
	// System.Int32 TMPro.TMP_Text::m_visibleSpriteCount
	int32_t ___m_visibleSpriteCount_152;
	// System.Int32 TMPro.TMP_Text::m_firstCharacterOfLine
	int32_t ___m_firstCharacterOfLine_153;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacterOfLine
	int32_t ___m_firstVisibleCharacterOfLine_154;
	// System.Int32 TMPro.TMP_Text::m_lastCharacterOfLine
	int32_t ___m_lastCharacterOfLine_155;
	// System.Int32 TMPro.TMP_Text::m_lastVisibleCharacterOfLine
	int32_t ___m_lastVisibleCharacterOfLine_156;
	// System.Int32 TMPro.TMP_Text::m_lineNumber
	int32_t ___m_lineNumber_157;
	// System.Int32 TMPro.TMP_Text::m_pageNumber
	int32_t ___m_pageNumber_158;
	// System.Single TMPro.TMP_Text::m_maxAscender
	float ___m_maxAscender_159;
	// System.Single TMPro.TMP_Text::m_maxDescender
	float ___m_maxDescender_160;
	// System.Single TMPro.TMP_Text::m_maxLineAscender
	float ___m_maxLineAscender_161;
	// System.Single TMPro.TMP_Text::m_maxLineDescender
	float ___m_maxLineDescender_162;
	// System.Single TMPro.TMP_Text::m_startOfLineAscender
	float ___m_startOfLineAscender_163;
	// System.Single TMPro.TMP_Text::m_maxFontScale
	float ___m_maxFontScale_164;
	// System.Single TMPro.TMP_Text::m_lineOffset
	float ___m_lineOffset_165;
	// TMPro.Extents TMPro.TMP_Text::m_meshExtents
	Extents_t2060714539  ___m_meshExtents_166;
	// UnityEngine.Color32 TMPro.TMP_Text::m_htmlColor
	Color32_t598853688  ___m_htmlColor_167;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_colorStack
	TMP_XmlTagStack_1_t1808196333  ___m_colorStack_168;
	// System.Single TMPro.TMP_Text::m_tabSpacing
	float ___m_tabSpacing_169;
	// System.Single TMPro.TMP_Text::m_spacing
	float ___m_spacing_170;
	// System.Boolean TMPro.TMP_Text::IsRectTransformDriven
	bool ___IsRectTransformDriven_171;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_styleStack
	TMP_XmlTagStack_1_t2363181145  ___m_styleStack_172;
	// System.Single TMPro.TMP_Text::m_padding
	float ___m_padding_173;
	// System.Single TMPro.TMP_Text::m_baselineOffset
	float ___m_baselineOffset_174;
	// System.Single TMPro.TMP_Text::m_xAdvance
	float ___m_xAdvance_175;
	// TMPro.TMP_TextElementType TMPro.TMP_Text::m_textElementType
	int32_t ___m_textElementType_176;
	// TMPro.TMP_TextElement TMPro.TMP_Text::m_cached_TextElement
	TMP_TextElement_t1232249705 * ___m_cached_TextElement_177;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Underline_GlyphInfo
	TMP_Glyph_t1999257094 * ___m_cached_Underline_GlyphInfo_178;
	// System.Int32 TMPro.TMP_Text::m_spriteCount
	int32_t ___m_spriteCount_179;
	// System.Int32 TMPro.TMP_Text::m_spriteIndex
	int32_t ___m_spriteIndex_180;
	// TMPro.InlineGraphicManager TMPro.TMP_Text::m_inlineGraphics
	InlineGraphicManager_t3583857972 * ___m_inlineGraphics_181;
	// System.Single[] TMPro.TMP_Text::k_Power
	SingleU5BU5D_t2316563989* ___k_Power_182;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_InfinityVector
	Vector2_t4282066565  ___k_InfinityVector_183;
	// System.Boolean TMPro.TMP_Text::<autoSizeTextContainer>k__BackingField
	bool ___U3CautoSizeTextContainerU3Ek__BackingField_184;

public:
	inline static int32_t get_offset_of_m_text_28() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_text_28)); }
	inline String_t* get_m_text_28() const { return ___m_text_28; }
	inline String_t** get_address_of_m_text_28() { return &___m_text_28; }
	inline void set_m_text_28(String_t* value)
	{
		___m_text_28 = value;
		Il2CppCodeGenWriteBarrier(&___m_text_28, value);
	}

	inline static int32_t get_offset_of_m_fontAsset_29() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_fontAsset_29)); }
	inline TMP_FontAsset_t967550395 * get_m_fontAsset_29() const { return ___m_fontAsset_29; }
	inline TMP_FontAsset_t967550395 ** get_address_of_m_fontAsset_29() { return &___m_fontAsset_29; }
	inline void set_m_fontAsset_29(TMP_FontAsset_t967550395 * value)
	{
		___m_fontAsset_29 = value;
		Il2CppCodeGenWriteBarrier(&___m_fontAsset_29, value);
	}

	inline static int32_t get_offset_of_m_currentFontAsset_30() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_currentFontAsset_30)); }
	inline TMP_FontAsset_t967550395 * get_m_currentFontAsset_30() const { return ___m_currentFontAsset_30; }
	inline TMP_FontAsset_t967550395 ** get_address_of_m_currentFontAsset_30() { return &___m_currentFontAsset_30; }
	inline void set_m_currentFontAsset_30(TMP_FontAsset_t967550395 * value)
	{
		___m_currentFontAsset_30 = value;
		Il2CppCodeGenWriteBarrier(&___m_currentFontAsset_30, value);
	}

	inline static int32_t get_offset_of_m_fontIndex_31() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_fontIndex_31)); }
	inline int32_t get_m_fontIndex_31() const { return ___m_fontIndex_31; }
	inline int32_t* get_address_of_m_fontIndex_31() { return &___m_fontIndex_31; }
	inline void set_m_fontIndex_31(int32_t value)
	{
		___m_fontIndex_31 = value;
	}

	inline static int32_t get_offset_of_m_isSDFShader_32() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_isSDFShader_32)); }
	inline bool get_m_isSDFShader_32() const { return ___m_isSDFShader_32; }
	inline bool* get_address_of_m_isSDFShader_32() { return &___m_isSDFShader_32; }
	inline void set_m_isSDFShader_32(bool value)
	{
		___m_isSDFShader_32 = value;
	}

	inline static int32_t get_offset_of_m_fontAsset_Dict_33() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_fontAsset_Dict_33)); }
	inline Dictionary_2_t964813634 * get_m_fontAsset_Dict_33() const { return ___m_fontAsset_Dict_33; }
	inline Dictionary_2_t964813634 ** get_address_of_m_fontAsset_Dict_33() { return &___m_fontAsset_Dict_33; }
	inline void set_m_fontAsset_Dict_33(Dictionary_2_t964813634 * value)
	{
		___m_fontAsset_Dict_33 = value;
		Il2CppCodeGenWriteBarrier(&___m_fontAsset_Dict_33, value);
	}

	inline static int32_t get_offset_of_m_fontMaterial_Dict_34() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_fontMaterial_Dict_34)); }
	inline Dictionary_2_t3867863346 * get_m_fontMaterial_Dict_34() const { return ___m_fontMaterial_Dict_34; }
	inline Dictionary_2_t3867863346 ** get_address_of_m_fontMaterial_Dict_34() { return &___m_fontMaterial_Dict_34; }
	inline void set_m_fontMaterial_Dict_34(Dictionary_2_t3867863346 * value)
	{
		___m_fontMaterial_Dict_34 = value;
		Il2CppCodeGenWriteBarrier(&___m_fontMaterial_Dict_34, value);
	}

	inline static int32_t get_offset_of_m_sharedMaterial_35() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_sharedMaterial_35)); }
	inline Material_t3870600107 * get_m_sharedMaterial_35() const { return ___m_sharedMaterial_35; }
	inline Material_t3870600107 ** get_address_of_m_sharedMaterial_35() { return &___m_sharedMaterial_35; }
	inline void set_m_sharedMaterial_35(Material_t3870600107 * value)
	{
		___m_sharedMaterial_35 = value;
		Il2CppCodeGenWriteBarrier(&___m_sharedMaterial_35, value);
	}

	inline static int32_t get_offset_of_m_currentMaterial_36() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_currentMaterial_36)); }
	inline Material_t3870600107 * get_m_currentMaterial_36() const { return ___m_currentMaterial_36; }
	inline Material_t3870600107 ** get_address_of_m_currentMaterial_36() { return &___m_currentMaterial_36; }
	inline void set_m_currentMaterial_36(Material_t3870600107 * value)
	{
		___m_currentMaterial_36 = value;
		Il2CppCodeGenWriteBarrier(&___m_currentMaterial_36, value);
	}

	inline static int32_t get_offset_of_m_fontMaterial_37() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_fontMaterial_37)); }
	inline Material_t3870600107 * get_m_fontMaterial_37() const { return ___m_fontMaterial_37; }
	inline Material_t3870600107 ** get_address_of_m_fontMaterial_37() { return &___m_fontMaterial_37; }
	inline void set_m_fontMaterial_37(Material_t3870600107 * value)
	{
		___m_fontMaterial_37 = value;
		Il2CppCodeGenWriteBarrier(&___m_fontMaterial_37, value);
	}

	inline static int32_t get_offset_of_m_baseMaterial_38() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_baseMaterial_38)); }
	inline Material_t3870600107 * get_m_baseMaterial_38() const { return ___m_baseMaterial_38; }
	inline Material_t3870600107 ** get_address_of_m_baseMaterial_38() { return &___m_baseMaterial_38; }
	inline void set_m_baseMaterial_38(Material_t3870600107 * value)
	{
		___m_baseMaterial_38 = value;
		Il2CppCodeGenWriteBarrier(&___m_baseMaterial_38, value);
	}

	inline static int32_t get_offset_of_m_lastBaseMaterial_39() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_lastBaseMaterial_39)); }
	inline Material_t3870600107 * get_m_lastBaseMaterial_39() const { return ___m_lastBaseMaterial_39; }
	inline Material_t3870600107 ** get_address_of_m_lastBaseMaterial_39() { return &___m_lastBaseMaterial_39; }
	inline void set_m_lastBaseMaterial_39(Material_t3870600107 * value)
	{
		___m_lastBaseMaterial_39 = value;
		Il2CppCodeGenWriteBarrier(&___m_lastBaseMaterial_39, value);
	}

	inline static int32_t get_offset_of_m_isNewBaseMaterial_40() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_isNewBaseMaterial_40)); }
	inline bool get_m_isNewBaseMaterial_40() const { return ___m_isNewBaseMaterial_40; }
	inline bool* get_address_of_m_isNewBaseMaterial_40() { return &___m_isNewBaseMaterial_40; }
	inline void set_m_isNewBaseMaterial_40(bool value)
	{
		___m_isNewBaseMaterial_40 = value;
	}

	inline static int32_t get_offset_of_m_maskingMaterial_41() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_maskingMaterial_41)); }
	inline Material_t3870600107 * get_m_maskingMaterial_41() const { return ___m_maskingMaterial_41; }
	inline Material_t3870600107 ** get_address_of_m_maskingMaterial_41() { return &___m_maskingMaterial_41; }
	inline void set_m_maskingMaterial_41(Material_t3870600107 * value)
	{
		___m_maskingMaterial_41 = value;
		Il2CppCodeGenWriteBarrier(&___m_maskingMaterial_41, value);
	}

	inline static int32_t get_offset_of_m_fontColor_42() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_fontColor_42)); }
	inline Color_t4194546905  get_m_fontColor_42() const { return ___m_fontColor_42; }
	inline Color_t4194546905 * get_address_of_m_fontColor_42() { return &___m_fontColor_42; }
	inline void set_m_fontColor_42(Color_t4194546905  value)
	{
		___m_fontColor_42 = value;
	}

	inline static int32_t get_offset_of_m_fontColor32_43() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_fontColor32_43)); }
	inline Color32_t598853688  get_m_fontColor32_43() const { return ___m_fontColor32_43; }
	inline Color32_t598853688 * get_address_of_m_fontColor32_43() { return &___m_fontColor32_43; }
	inline void set_m_fontColor32_43(Color32_t598853688  value)
	{
		___m_fontColor32_43 = value;
	}

	inline static int32_t get_offset_of_m_tintAllSprites_45() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_tintAllSprites_45)); }
	inline bool get_m_tintAllSprites_45() const { return ___m_tintAllSprites_45; }
	inline bool* get_address_of_m_tintAllSprites_45() { return &___m_tintAllSprites_45; }
	inline void set_m_tintAllSprites_45(bool value)
	{
		___m_tintAllSprites_45 = value;
	}

	inline static int32_t get_offset_of_m_tintSprite_46() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_tintSprite_46)); }
	inline bool get_m_tintSprite_46() const { return ___m_tintSprite_46; }
	inline bool* get_address_of_m_tintSprite_46() { return &___m_tintSprite_46; }
	inline void set_m_tintSprite_46(bool value)
	{
		___m_tintSprite_46 = value;
	}

	inline static int32_t get_offset_of_m_spriteColor_47() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_spriteColor_47)); }
	inline Color32_t598853688  get_m_spriteColor_47() const { return ___m_spriteColor_47; }
	inline Color32_t598853688 * get_address_of_m_spriteColor_47() { return &___m_spriteColor_47; }
	inline void set_m_spriteColor_47(Color32_t598853688  value)
	{
		___m_spriteColor_47 = value;
	}

	inline static int32_t get_offset_of_m_enableVertexGradient_48() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_enableVertexGradient_48)); }
	inline bool get_m_enableVertexGradient_48() const { return ___m_enableVertexGradient_48; }
	inline bool* get_address_of_m_enableVertexGradient_48() { return &___m_enableVertexGradient_48; }
	inline void set_m_enableVertexGradient_48(bool value)
	{
		___m_enableVertexGradient_48 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradient_49() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_fontColorGradient_49)); }
	inline VertexGradient_t2166651658  get_m_fontColorGradient_49() const { return ___m_fontColorGradient_49; }
	inline VertexGradient_t2166651658 * get_address_of_m_fontColorGradient_49() { return &___m_fontColorGradient_49; }
	inline void set_m_fontColorGradient_49(VertexGradient_t2166651658  value)
	{
		___m_fontColorGradient_49 = value;
	}

	inline static int32_t get_offset_of_m_overrideHtmlColors_50() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_overrideHtmlColors_50)); }
	inline bool get_m_overrideHtmlColors_50() const { return ___m_overrideHtmlColors_50; }
	inline bool* get_address_of_m_overrideHtmlColors_50() { return &___m_overrideHtmlColors_50; }
	inline void set_m_overrideHtmlColors_50(bool value)
	{
		___m_overrideHtmlColors_50 = value;
	}

	inline static int32_t get_offset_of_m_faceColor_51() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_faceColor_51)); }
	inline Color32_t598853688  get_m_faceColor_51() const { return ___m_faceColor_51; }
	inline Color32_t598853688 * get_address_of_m_faceColor_51() { return &___m_faceColor_51; }
	inline void set_m_faceColor_51(Color32_t598853688  value)
	{
		___m_faceColor_51 = value;
	}

	inline static int32_t get_offset_of_m_outlineColor_52() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_outlineColor_52)); }
	inline Color32_t598853688  get_m_outlineColor_52() const { return ___m_outlineColor_52; }
	inline Color32_t598853688 * get_address_of_m_outlineColor_52() { return &___m_outlineColor_52; }
	inline void set_m_outlineColor_52(Color32_t598853688  value)
	{
		___m_outlineColor_52 = value;
	}

	inline static int32_t get_offset_of_m_outlineWidth_53() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_outlineWidth_53)); }
	inline float get_m_outlineWidth_53() const { return ___m_outlineWidth_53; }
	inline float* get_address_of_m_outlineWidth_53() { return &___m_outlineWidth_53; }
	inline void set_m_outlineWidth_53(float value)
	{
		___m_outlineWidth_53 = value;
	}

	inline static int32_t get_offset_of_m_fontSize_54() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_fontSize_54)); }
	inline float get_m_fontSize_54() const { return ___m_fontSize_54; }
	inline float* get_address_of_m_fontSize_54() { return &___m_fontSize_54; }
	inline void set_m_fontSize_54(float value)
	{
		___m_fontSize_54 = value;
	}

	inline static int32_t get_offset_of_m_currentFontSize_55() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_currentFontSize_55)); }
	inline float get_m_currentFontSize_55() const { return ___m_currentFontSize_55; }
	inline float* get_address_of_m_currentFontSize_55() { return &___m_currentFontSize_55; }
	inline void set_m_currentFontSize_55(float value)
	{
		___m_currentFontSize_55 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeBase_56() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_fontSizeBase_56)); }
	inline float get_m_fontSizeBase_56() const { return ___m_fontSizeBase_56; }
	inline float* get_address_of_m_fontSizeBase_56() { return &___m_fontSizeBase_56; }
	inline void set_m_fontSizeBase_56(float value)
	{
		___m_fontSizeBase_56 = value;
	}

	inline static int32_t get_offset_of_m_sizeStack_57() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_sizeStack_57)); }
	inline TMP_XmlTagStack_1_t1206294321  get_m_sizeStack_57() const { return ___m_sizeStack_57; }
	inline TMP_XmlTagStack_1_t1206294321 * get_address_of_m_sizeStack_57() { return &___m_sizeStack_57; }
	inline void set_m_sizeStack_57(TMP_XmlTagStack_1_t1206294321  value)
	{
		___m_sizeStack_57 = value;
	}

	inline static int32_t get_offset_of_m_enableAutoSizing_58() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_enableAutoSizing_58)); }
	inline bool get_m_enableAutoSizing_58() const { return ___m_enableAutoSizing_58; }
	inline bool* get_address_of_m_enableAutoSizing_58() { return &___m_enableAutoSizing_58; }
	inline void set_m_enableAutoSizing_58(bool value)
	{
		___m_enableAutoSizing_58 = value;
	}

	inline static int32_t get_offset_of_m_maxFontSize_59() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_maxFontSize_59)); }
	inline float get_m_maxFontSize_59() const { return ___m_maxFontSize_59; }
	inline float* get_address_of_m_maxFontSize_59() { return &___m_maxFontSize_59; }
	inline void set_m_maxFontSize_59(float value)
	{
		___m_maxFontSize_59 = value;
	}

	inline static int32_t get_offset_of_m_minFontSize_60() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_minFontSize_60)); }
	inline float get_m_minFontSize_60() const { return ___m_minFontSize_60; }
	inline float* get_address_of_m_minFontSize_60() { return &___m_minFontSize_60; }
	inline void set_m_minFontSize_60(float value)
	{
		___m_minFontSize_60 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMin_61() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_fontSizeMin_61)); }
	inline float get_m_fontSizeMin_61() const { return ___m_fontSizeMin_61; }
	inline float* get_address_of_m_fontSizeMin_61() { return &___m_fontSizeMin_61; }
	inline void set_m_fontSizeMin_61(float value)
	{
		___m_fontSizeMin_61 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMax_62() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_fontSizeMax_62)); }
	inline float get_m_fontSizeMax_62() const { return ___m_fontSizeMax_62; }
	inline float* get_address_of_m_fontSizeMax_62() { return &___m_fontSizeMax_62; }
	inline void set_m_fontSizeMax_62(float value)
	{
		___m_fontSizeMax_62 = value;
	}

	inline static int32_t get_offset_of_m_fontStyle_63() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_fontStyle_63)); }
	inline int32_t get_m_fontStyle_63() const { return ___m_fontStyle_63; }
	inline int32_t* get_address_of_m_fontStyle_63() { return &___m_fontStyle_63; }
	inline void set_m_fontStyle_63(int32_t value)
	{
		___m_fontStyle_63 = value;
	}

	inline static int32_t get_offset_of_m_style_64() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_style_64)); }
	inline int32_t get_m_style_64() const { return ___m_style_64; }
	inline int32_t* get_address_of_m_style_64() { return &___m_style_64; }
	inline void set_m_style_64(int32_t value)
	{
		___m_style_64 = value;
	}

	inline static int32_t get_offset_of_m_isUsingBold_65() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_isUsingBold_65)); }
	inline bool get_m_isUsingBold_65() const { return ___m_isUsingBold_65; }
	inline bool* get_address_of_m_isUsingBold_65() { return &___m_isUsingBold_65; }
	inline void set_m_isUsingBold_65(bool value)
	{
		___m_isUsingBold_65 = value;
	}

	inline static int32_t get_offset_of_m_textAlignment_66() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_textAlignment_66)); }
	inline int32_t get_m_textAlignment_66() const { return ___m_textAlignment_66; }
	inline int32_t* get_address_of_m_textAlignment_66() { return &___m_textAlignment_66; }
	inline void set_m_textAlignment_66(int32_t value)
	{
		___m_textAlignment_66 = value;
	}

	inline static int32_t get_offset_of_m_lineJustification_67() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_lineJustification_67)); }
	inline int32_t get_m_lineJustification_67() const { return ___m_lineJustification_67; }
	inline int32_t* get_address_of_m_lineJustification_67() { return &___m_lineJustification_67; }
	inline void set_m_lineJustification_67(int32_t value)
	{
		___m_lineJustification_67 = value;
	}

	inline static int32_t get_offset_of_m_textContainerLocalCorners_68() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_textContainerLocalCorners_68)); }
	inline Vector3U5BU5D_t215400611* get_m_textContainerLocalCorners_68() const { return ___m_textContainerLocalCorners_68; }
	inline Vector3U5BU5D_t215400611** get_address_of_m_textContainerLocalCorners_68() { return &___m_textContainerLocalCorners_68; }
	inline void set_m_textContainerLocalCorners_68(Vector3U5BU5D_t215400611* value)
	{
		___m_textContainerLocalCorners_68 = value;
		Il2CppCodeGenWriteBarrier(&___m_textContainerLocalCorners_68, value);
	}

	inline static int32_t get_offset_of_m_characterSpacing_69() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_characterSpacing_69)); }
	inline float get_m_characterSpacing_69() const { return ___m_characterSpacing_69; }
	inline float* get_address_of_m_characterSpacing_69() { return &___m_characterSpacing_69; }
	inline void set_m_characterSpacing_69(float value)
	{
		___m_characterSpacing_69 = value;
	}

	inline static int32_t get_offset_of_m_cSpacing_70() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_cSpacing_70)); }
	inline float get_m_cSpacing_70() const { return ___m_cSpacing_70; }
	inline float* get_address_of_m_cSpacing_70() { return &___m_cSpacing_70; }
	inline void set_m_cSpacing_70(float value)
	{
		___m_cSpacing_70 = value;
	}

	inline static int32_t get_offset_of_m_monoSpacing_71() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_monoSpacing_71)); }
	inline float get_m_monoSpacing_71() const { return ___m_monoSpacing_71; }
	inline float* get_address_of_m_monoSpacing_71() { return &___m_monoSpacing_71; }
	inline void set_m_monoSpacing_71(float value)
	{
		___m_monoSpacing_71 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacing_72() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_lineSpacing_72)); }
	inline float get_m_lineSpacing_72() const { return ___m_lineSpacing_72; }
	inline float* get_address_of_m_lineSpacing_72() { return &___m_lineSpacing_72; }
	inline void set_m_lineSpacing_72(float value)
	{
		___m_lineSpacing_72 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingDelta_73() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_lineSpacingDelta_73)); }
	inline float get_m_lineSpacingDelta_73() const { return ___m_lineSpacingDelta_73; }
	inline float* get_address_of_m_lineSpacingDelta_73() { return &___m_lineSpacingDelta_73; }
	inline void set_m_lineSpacingDelta_73(float value)
	{
		___m_lineSpacingDelta_73 = value;
	}

	inline static int32_t get_offset_of_m_lineHeight_74() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_lineHeight_74)); }
	inline float get_m_lineHeight_74() const { return ___m_lineHeight_74; }
	inline float* get_address_of_m_lineHeight_74() { return &___m_lineHeight_74; }
	inline void set_m_lineHeight_74(float value)
	{
		___m_lineHeight_74 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingMax_75() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_lineSpacingMax_75)); }
	inline float get_m_lineSpacingMax_75() const { return ___m_lineSpacingMax_75; }
	inline float* get_address_of_m_lineSpacingMax_75() { return &___m_lineSpacingMax_75; }
	inline void set_m_lineSpacingMax_75(float value)
	{
		___m_lineSpacingMax_75 = value;
	}

	inline static int32_t get_offset_of_m_paragraphSpacing_76() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_paragraphSpacing_76)); }
	inline float get_m_paragraphSpacing_76() const { return ___m_paragraphSpacing_76; }
	inline float* get_address_of_m_paragraphSpacing_76() { return &___m_paragraphSpacing_76; }
	inline void set_m_paragraphSpacing_76(float value)
	{
		___m_paragraphSpacing_76 = value;
	}

	inline static int32_t get_offset_of_m_charWidthMaxAdj_77() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_charWidthMaxAdj_77)); }
	inline float get_m_charWidthMaxAdj_77() const { return ___m_charWidthMaxAdj_77; }
	inline float* get_address_of_m_charWidthMaxAdj_77() { return &___m_charWidthMaxAdj_77; }
	inline void set_m_charWidthMaxAdj_77(float value)
	{
		___m_charWidthMaxAdj_77 = value;
	}

	inline static int32_t get_offset_of_m_charWidthAdjDelta_78() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_charWidthAdjDelta_78)); }
	inline float get_m_charWidthAdjDelta_78() const { return ___m_charWidthAdjDelta_78; }
	inline float* get_address_of_m_charWidthAdjDelta_78() { return &___m_charWidthAdjDelta_78; }
	inline void set_m_charWidthAdjDelta_78(float value)
	{
		___m_charWidthAdjDelta_78 = value;
	}

	inline static int32_t get_offset_of_m_enableWordWrapping_79() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_enableWordWrapping_79)); }
	inline bool get_m_enableWordWrapping_79() const { return ___m_enableWordWrapping_79; }
	inline bool* get_address_of_m_enableWordWrapping_79() { return &___m_enableWordWrapping_79; }
	inline void set_m_enableWordWrapping_79(bool value)
	{
		___m_enableWordWrapping_79 = value;
	}

	inline static int32_t get_offset_of_m_isCharacterWrappingEnabled_80() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_isCharacterWrappingEnabled_80)); }
	inline bool get_m_isCharacterWrappingEnabled_80() const { return ___m_isCharacterWrappingEnabled_80; }
	inline bool* get_address_of_m_isCharacterWrappingEnabled_80() { return &___m_isCharacterWrappingEnabled_80; }
	inline void set_m_isCharacterWrappingEnabled_80(bool value)
	{
		___m_isCharacterWrappingEnabled_80 = value;
	}

	inline static int32_t get_offset_of_m_isNonBreakingSpace_81() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_isNonBreakingSpace_81)); }
	inline bool get_m_isNonBreakingSpace_81() const { return ___m_isNonBreakingSpace_81; }
	inline bool* get_address_of_m_isNonBreakingSpace_81() { return &___m_isNonBreakingSpace_81; }
	inline void set_m_isNonBreakingSpace_81(bool value)
	{
		___m_isNonBreakingSpace_81 = value;
	}

	inline static int32_t get_offset_of_m_isIgnoringAlignment_82() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_isIgnoringAlignment_82)); }
	inline bool get_m_isIgnoringAlignment_82() const { return ___m_isIgnoringAlignment_82; }
	inline bool* get_address_of_m_isIgnoringAlignment_82() { return &___m_isIgnoringAlignment_82; }
	inline void set_m_isIgnoringAlignment_82(bool value)
	{
		___m_isIgnoringAlignment_82 = value;
	}

	inline static int32_t get_offset_of_m_wordWrappingRatios_83() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_wordWrappingRatios_83)); }
	inline float get_m_wordWrappingRatios_83() const { return ___m_wordWrappingRatios_83; }
	inline float* get_address_of_m_wordWrappingRatios_83() { return &___m_wordWrappingRatios_83; }
	inline void set_m_wordWrappingRatios_83(float value)
	{
		___m_wordWrappingRatios_83 = value;
	}

	inline static int32_t get_offset_of_m_overflowMode_84() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_overflowMode_84)); }
	inline int32_t get_m_overflowMode_84() const { return ___m_overflowMode_84; }
	inline int32_t* get_address_of_m_overflowMode_84() { return &___m_overflowMode_84; }
	inline void set_m_overflowMode_84(int32_t value)
	{
		___m_overflowMode_84 = value;
	}

	inline static int32_t get_offset_of_m_isTextTruncated_85() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_isTextTruncated_85)); }
	inline bool get_m_isTextTruncated_85() const { return ___m_isTextTruncated_85; }
	inline bool* get_address_of_m_isTextTruncated_85() { return &___m_isTextTruncated_85; }
	inline void set_m_isTextTruncated_85(bool value)
	{
		___m_isTextTruncated_85 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_86() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_enableKerning_86)); }
	inline bool get_m_enableKerning_86() const { return ___m_enableKerning_86; }
	inline bool* get_address_of_m_enableKerning_86() { return &___m_enableKerning_86; }
	inline void set_m_enableKerning_86(bool value)
	{
		___m_enableKerning_86 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_87() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_enableExtraPadding_87)); }
	inline bool get_m_enableExtraPadding_87() const { return ___m_enableExtraPadding_87; }
	inline bool* get_address_of_m_enableExtraPadding_87() { return &___m_enableExtraPadding_87; }
	inline void set_m_enableExtraPadding_87(bool value)
	{
		___m_enableExtraPadding_87 = value;
	}

	inline static int32_t get_offset_of_checkPaddingRequired_88() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___checkPaddingRequired_88)); }
	inline bool get_checkPaddingRequired_88() const { return ___checkPaddingRequired_88; }
	inline bool* get_address_of_checkPaddingRequired_88() { return &___checkPaddingRequired_88; }
	inline void set_checkPaddingRequired_88(bool value)
	{
		___checkPaddingRequired_88 = value;
	}

	inline static int32_t get_offset_of_m_isRichText_89() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_isRichText_89)); }
	inline bool get_m_isRichText_89() const { return ___m_isRichText_89; }
	inline bool* get_address_of_m_isRichText_89() { return &___m_isRichText_89; }
	inline void set_m_isRichText_89(bool value)
	{
		___m_isRichText_89 = value;
	}

	inline static int32_t get_offset_of_m_parseCtrlCharacters_90() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_parseCtrlCharacters_90)); }
	inline bool get_m_parseCtrlCharacters_90() const { return ___m_parseCtrlCharacters_90; }
	inline bool* get_address_of_m_parseCtrlCharacters_90() { return &___m_parseCtrlCharacters_90; }
	inline void set_m_parseCtrlCharacters_90(bool value)
	{
		___m_parseCtrlCharacters_90 = value;
	}

	inline static int32_t get_offset_of_m_isOverlay_91() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_isOverlay_91)); }
	inline bool get_m_isOverlay_91() const { return ___m_isOverlay_91; }
	inline bool* get_address_of_m_isOverlay_91() { return &___m_isOverlay_91; }
	inline void set_m_isOverlay_91(bool value)
	{
		___m_isOverlay_91 = value;
	}

	inline static int32_t get_offset_of_m_isOrthographic_92() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_isOrthographic_92)); }
	inline bool get_m_isOrthographic_92() const { return ___m_isOrthographic_92; }
	inline bool* get_address_of_m_isOrthographic_92() { return &___m_isOrthographic_92; }
	inline void set_m_isOrthographic_92(bool value)
	{
		___m_isOrthographic_92 = value;
	}

	inline static int32_t get_offset_of_m_isCullingEnabled_93() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_isCullingEnabled_93)); }
	inline bool get_m_isCullingEnabled_93() const { return ___m_isCullingEnabled_93; }
	inline bool* get_address_of_m_isCullingEnabled_93() { return &___m_isCullingEnabled_93; }
	inline void set_m_isCullingEnabled_93(bool value)
	{
		___m_isCullingEnabled_93 = value;
	}

	inline static int32_t get_offset_of_m_ignoreCulling_94() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_ignoreCulling_94)); }
	inline bool get_m_ignoreCulling_94() const { return ___m_ignoreCulling_94; }
	inline bool* get_address_of_m_ignoreCulling_94() { return &___m_ignoreCulling_94; }
	inline void set_m_ignoreCulling_94(bool value)
	{
		___m_ignoreCulling_94 = value;
	}

	inline static int32_t get_offset_of_m_horizontalMapping_95() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_horizontalMapping_95)); }
	inline int32_t get_m_horizontalMapping_95() const { return ___m_horizontalMapping_95; }
	inline int32_t* get_address_of_m_horizontalMapping_95() { return &___m_horizontalMapping_95; }
	inline void set_m_horizontalMapping_95(int32_t value)
	{
		___m_horizontalMapping_95 = value;
	}

	inline static int32_t get_offset_of_m_verticalMapping_96() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_verticalMapping_96)); }
	inline int32_t get_m_verticalMapping_96() const { return ___m_verticalMapping_96; }
	inline int32_t* get_address_of_m_verticalMapping_96() { return &___m_verticalMapping_96; }
	inline void set_m_verticalMapping_96(int32_t value)
	{
		___m_verticalMapping_96 = value;
	}

	inline static int32_t get_offset_of_m_renderMode_97() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_renderMode_97)); }
	inline int32_t get_m_renderMode_97() const { return ___m_renderMode_97; }
	inline int32_t* get_address_of_m_renderMode_97() { return &___m_renderMode_97; }
	inline void set_m_renderMode_97(int32_t value)
	{
		___m_renderMode_97 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleCharacters_98() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_maxVisibleCharacters_98)); }
	inline int32_t get_m_maxVisibleCharacters_98() const { return ___m_maxVisibleCharacters_98; }
	inline int32_t* get_address_of_m_maxVisibleCharacters_98() { return &___m_maxVisibleCharacters_98; }
	inline void set_m_maxVisibleCharacters_98(int32_t value)
	{
		___m_maxVisibleCharacters_98 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleWords_99() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_maxVisibleWords_99)); }
	inline int32_t get_m_maxVisibleWords_99() const { return ___m_maxVisibleWords_99; }
	inline int32_t* get_address_of_m_maxVisibleWords_99() { return &___m_maxVisibleWords_99; }
	inline void set_m_maxVisibleWords_99(int32_t value)
	{
		___m_maxVisibleWords_99 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleLines_100() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_maxVisibleLines_100)); }
	inline int32_t get_m_maxVisibleLines_100() const { return ___m_maxVisibleLines_100; }
	inline int32_t* get_address_of_m_maxVisibleLines_100() { return &___m_maxVisibleLines_100; }
	inline void set_m_maxVisibleLines_100(int32_t value)
	{
		___m_maxVisibleLines_100 = value;
	}

	inline static int32_t get_offset_of_m_pageToDisplay_101() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_pageToDisplay_101)); }
	inline int32_t get_m_pageToDisplay_101() const { return ___m_pageToDisplay_101; }
	inline int32_t* get_address_of_m_pageToDisplay_101() { return &___m_pageToDisplay_101; }
	inline void set_m_pageToDisplay_101(int32_t value)
	{
		___m_pageToDisplay_101 = value;
	}

	inline static int32_t get_offset_of_m_isNewPage_102() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_isNewPage_102)); }
	inline bool get_m_isNewPage_102() const { return ___m_isNewPage_102; }
	inline bool* get_address_of_m_isNewPage_102() { return &___m_isNewPage_102; }
	inline void set_m_isNewPage_102(bool value)
	{
		___m_isNewPage_102 = value;
	}

	inline static int32_t get_offset_of_m_margin_103() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_margin_103)); }
	inline Vector4_t4282066567  get_m_margin_103() const { return ___m_margin_103; }
	inline Vector4_t4282066567 * get_address_of_m_margin_103() { return &___m_margin_103; }
	inline void set_m_margin_103(Vector4_t4282066567  value)
	{
		___m_margin_103 = value;
	}

	inline static int32_t get_offset_of_m_marginLeft_104() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_marginLeft_104)); }
	inline float get_m_marginLeft_104() const { return ___m_marginLeft_104; }
	inline float* get_address_of_m_marginLeft_104() { return &___m_marginLeft_104; }
	inline void set_m_marginLeft_104(float value)
	{
		___m_marginLeft_104 = value;
	}

	inline static int32_t get_offset_of_m_marginRight_105() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_marginRight_105)); }
	inline float get_m_marginRight_105() const { return ___m_marginRight_105; }
	inline float* get_address_of_m_marginRight_105() { return &___m_marginRight_105; }
	inline void set_m_marginRight_105(float value)
	{
		___m_marginRight_105 = value;
	}

	inline static int32_t get_offset_of_m_marginWidth_106() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_marginWidth_106)); }
	inline float get_m_marginWidth_106() const { return ___m_marginWidth_106; }
	inline float* get_address_of_m_marginWidth_106() { return &___m_marginWidth_106; }
	inline void set_m_marginWidth_106(float value)
	{
		___m_marginWidth_106 = value;
	}

	inline static int32_t get_offset_of_m_marginHeight_107() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_marginHeight_107)); }
	inline float get_m_marginHeight_107() const { return ___m_marginHeight_107; }
	inline float* get_address_of_m_marginHeight_107() { return &___m_marginHeight_107; }
	inline void set_m_marginHeight_107(float value)
	{
		___m_marginHeight_107 = value;
	}

	inline static int32_t get_offset_of_m_width_108() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_width_108)); }
	inline float get_m_width_108() const { return ___m_width_108; }
	inline float* get_address_of_m_width_108() { return &___m_width_108; }
	inline void set_m_width_108(float value)
	{
		___m_width_108 = value;
	}

	inline static int32_t get_offset_of_m_textInfo_109() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_textInfo_109)); }
	inline TMP_TextInfo_t270066265 * get_m_textInfo_109() const { return ___m_textInfo_109; }
	inline TMP_TextInfo_t270066265 ** get_address_of_m_textInfo_109() { return &___m_textInfo_109; }
	inline void set_m_textInfo_109(TMP_TextInfo_t270066265 * value)
	{
		___m_textInfo_109 = value;
		Il2CppCodeGenWriteBarrier(&___m_textInfo_109, value);
	}

	inline static int32_t get_offset_of_m_havePropertiesChanged_110() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_havePropertiesChanged_110)); }
	inline bool get_m_havePropertiesChanged_110() const { return ___m_havePropertiesChanged_110; }
	inline bool* get_address_of_m_havePropertiesChanged_110() { return &___m_havePropertiesChanged_110; }
	inline void set_m_havePropertiesChanged_110(bool value)
	{
		___m_havePropertiesChanged_110 = value;
	}

	inline static int32_t get_offset_of_m_transform_111() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_transform_111)); }
	inline Transform_t1659122786 * get_m_transform_111() const { return ___m_transform_111; }
	inline Transform_t1659122786 ** get_address_of_m_transform_111() { return &___m_transform_111; }
	inline void set_m_transform_111(Transform_t1659122786 * value)
	{
		___m_transform_111 = value;
		Il2CppCodeGenWriteBarrier(&___m_transform_111, value);
	}

	inline static int32_t get_offset_of_m_rectTransform_112() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_rectTransform_112)); }
	inline RectTransform_t972643934 * get_m_rectTransform_112() const { return ___m_rectTransform_112; }
	inline RectTransform_t972643934 ** get_address_of_m_rectTransform_112() { return &___m_rectTransform_112; }
	inline void set_m_rectTransform_112(RectTransform_t972643934 * value)
	{
		___m_rectTransform_112 = value;
		Il2CppCodeGenWriteBarrier(&___m_rectTransform_112, value);
	}

	inline static int32_t get_offset_of_m_mesh_113() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_mesh_113)); }
	inline Mesh_t4241756145 * get_m_mesh_113() const { return ___m_mesh_113; }
	inline Mesh_t4241756145 ** get_address_of_m_mesh_113() { return &___m_mesh_113; }
	inline void set_m_mesh_113(Mesh_t4241756145 * value)
	{
		___m_mesh_113 = value;
		Il2CppCodeGenWriteBarrier(&___m_mesh_113, value);
	}

	inline static int32_t get_offset_of_m_flexibleHeight_114() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_flexibleHeight_114)); }
	inline float get_m_flexibleHeight_114() const { return ___m_flexibleHeight_114; }
	inline float* get_address_of_m_flexibleHeight_114() { return &___m_flexibleHeight_114; }
	inline void set_m_flexibleHeight_114(float value)
	{
		___m_flexibleHeight_114 = value;
	}

	inline static int32_t get_offset_of_m_flexibleWidth_115() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_flexibleWidth_115)); }
	inline float get_m_flexibleWidth_115() const { return ___m_flexibleWidth_115; }
	inline float* get_address_of_m_flexibleWidth_115() { return &___m_flexibleWidth_115; }
	inline void set_m_flexibleWidth_115(float value)
	{
		___m_flexibleWidth_115 = value;
	}

	inline static int32_t get_offset_of_m_minHeight_116() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_minHeight_116)); }
	inline float get_m_minHeight_116() const { return ___m_minHeight_116; }
	inline float* get_address_of_m_minHeight_116() { return &___m_minHeight_116; }
	inline void set_m_minHeight_116(float value)
	{
		___m_minHeight_116 = value;
	}

	inline static int32_t get_offset_of_m_minWidth_117() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_minWidth_117)); }
	inline float get_m_minWidth_117() const { return ___m_minWidth_117; }
	inline float* get_address_of_m_minWidth_117() { return &___m_minWidth_117; }
	inline void set_m_minWidth_117(float value)
	{
		___m_minWidth_117 = value;
	}

	inline static int32_t get_offset_of_m_preferredWidth_118() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_preferredWidth_118)); }
	inline float get_m_preferredWidth_118() const { return ___m_preferredWidth_118; }
	inline float* get_address_of_m_preferredWidth_118() { return &___m_preferredWidth_118; }
	inline void set_m_preferredWidth_118(float value)
	{
		___m_preferredWidth_118 = value;
	}

	inline static int32_t get_offset_of_m_renderedWidth_119() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_renderedWidth_119)); }
	inline float get_m_renderedWidth_119() const { return ___m_renderedWidth_119; }
	inline float* get_address_of_m_renderedWidth_119() { return &___m_renderedWidth_119; }
	inline void set_m_renderedWidth_119(float value)
	{
		___m_renderedWidth_119 = value;
	}

	inline static int32_t get_offset_of_m_preferredHeight_120() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_preferredHeight_120)); }
	inline float get_m_preferredHeight_120() const { return ___m_preferredHeight_120; }
	inline float* get_address_of_m_preferredHeight_120() { return &___m_preferredHeight_120; }
	inline void set_m_preferredHeight_120(float value)
	{
		___m_preferredHeight_120 = value;
	}

	inline static int32_t get_offset_of_m_renderedHeight_121() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_renderedHeight_121)); }
	inline float get_m_renderedHeight_121() const { return ___m_renderedHeight_121; }
	inline float* get_address_of_m_renderedHeight_121() { return &___m_renderedHeight_121; }
	inline void set_m_renderedHeight_121(float value)
	{
		___m_renderedHeight_121 = value;
	}

	inline static int32_t get_offset_of_m_layoutPriority_122() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_layoutPriority_122)); }
	inline int32_t get_m_layoutPriority_122() const { return ___m_layoutPriority_122; }
	inline int32_t* get_address_of_m_layoutPriority_122() { return &___m_layoutPriority_122; }
	inline void set_m_layoutPriority_122(int32_t value)
	{
		___m_layoutPriority_122 = value;
	}

	inline static int32_t get_offset_of_m_isCalculateSizeRequired_123() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_isCalculateSizeRequired_123)); }
	inline bool get_m_isCalculateSizeRequired_123() const { return ___m_isCalculateSizeRequired_123; }
	inline bool* get_address_of_m_isCalculateSizeRequired_123() { return &___m_isCalculateSizeRequired_123; }
	inline void set_m_isCalculateSizeRequired_123(bool value)
	{
		___m_isCalculateSizeRequired_123 = value;
	}

	inline static int32_t get_offset_of_m_isLayoutDirty_124() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_isLayoutDirty_124)); }
	inline bool get_m_isLayoutDirty_124() const { return ___m_isLayoutDirty_124; }
	inline bool* get_address_of_m_isLayoutDirty_124() { return &___m_isLayoutDirty_124; }
	inline void set_m_isLayoutDirty_124(bool value)
	{
		___m_isLayoutDirty_124 = value;
	}

	inline static int32_t get_offset_of_m_verticesAlreadyDirty_125() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_verticesAlreadyDirty_125)); }
	inline bool get_m_verticesAlreadyDirty_125() const { return ___m_verticesAlreadyDirty_125; }
	inline bool* get_address_of_m_verticesAlreadyDirty_125() { return &___m_verticesAlreadyDirty_125; }
	inline void set_m_verticesAlreadyDirty_125(bool value)
	{
		___m_verticesAlreadyDirty_125 = value;
	}

	inline static int32_t get_offset_of_m_layoutAlreadyDirty_126() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_layoutAlreadyDirty_126)); }
	inline bool get_m_layoutAlreadyDirty_126() const { return ___m_layoutAlreadyDirty_126; }
	inline bool* get_address_of_m_layoutAlreadyDirty_126() { return &___m_layoutAlreadyDirty_126; }
	inline void set_m_layoutAlreadyDirty_126(bool value)
	{
		___m_layoutAlreadyDirty_126 = value;
	}

	inline static int32_t get_offset_of_m_isInputParsingRequired_127() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_isInputParsingRequired_127)); }
	inline bool get_m_isInputParsingRequired_127() const { return ___m_isInputParsingRequired_127; }
	inline bool* get_address_of_m_isInputParsingRequired_127() { return &___m_isInputParsingRequired_127; }
	inline void set_m_isInputParsingRequired_127(bool value)
	{
		___m_isInputParsingRequired_127 = value;
	}

	inline static int32_t get_offset_of_m_inputSource_128() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_inputSource_128)); }
	inline int32_t get_m_inputSource_128() const { return ___m_inputSource_128; }
	inline int32_t* get_address_of_m_inputSource_128() { return &___m_inputSource_128; }
	inline void set_m_inputSource_128(int32_t value)
	{
		___m_inputSource_128 = value;
	}

	inline static int32_t get_offset_of_old_text_129() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___old_text_129)); }
	inline String_t* get_old_text_129() const { return ___old_text_129; }
	inline String_t** get_address_of_old_text_129() { return &___old_text_129; }
	inline void set_old_text_129(String_t* value)
	{
		___old_text_129 = value;
		Il2CppCodeGenWriteBarrier(&___old_text_129, value);
	}

	inline static int32_t get_offset_of_old_arg0_130() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___old_arg0_130)); }
	inline float get_old_arg0_130() const { return ___old_arg0_130; }
	inline float* get_address_of_old_arg0_130() { return &___old_arg0_130; }
	inline void set_old_arg0_130(float value)
	{
		___old_arg0_130 = value;
	}

	inline static int32_t get_offset_of_old_arg1_131() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___old_arg1_131)); }
	inline float get_old_arg1_131() const { return ___old_arg1_131; }
	inline float* get_address_of_old_arg1_131() { return &___old_arg1_131; }
	inline void set_old_arg1_131(float value)
	{
		___old_arg1_131 = value;
	}

	inline static int32_t get_offset_of_old_arg2_132() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___old_arg2_132)); }
	inline float get_old_arg2_132() const { return ___old_arg2_132; }
	inline float* get_address_of_old_arg2_132() { return &___old_arg2_132; }
	inline void set_old_arg2_132(float value)
	{
		___old_arg2_132 = value;
	}

	inline static int32_t get_offset_of_m_fontScale_133() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_fontScale_133)); }
	inline float get_m_fontScale_133() const { return ___m_fontScale_133; }
	inline float* get_address_of_m_fontScale_133() { return &___m_fontScale_133; }
	inline void set_m_fontScale_133(float value)
	{
		___m_fontScale_133 = value;
	}

	inline static int32_t get_offset_of_m_htmlTag_134() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_htmlTag_134)); }
	inline CharU5BU5D_t3324145743* get_m_htmlTag_134() const { return ___m_htmlTag_134; }
	inline CharU5BU5D_t3324145743** get_address_of_m_htmlTag_134() { return &___m_htmlTag_134; }
	inline void set_m_htmlTag_134(CharU5BU5D_t3324145743* value)
	{
		___m_htmlTag_134 = value;
		Il2CppCodeGenWriteBarrier(&___m_htmlTag_134, value);
	}

	inline static int32_t get_offset_of_m_xmlAttribute_135() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_xmlAttribute_135)); }
	inline XML_TagAttributeU5BU5D_t3301714177* get_m_xmlAttribute_135() const { return ___m_xmlAttribute_135; }
	inline XML_TagAttributeU5BU5D_t3301714177** get_address_of_m_xmlAttribute_135() { return &___m_xmlAttribute_135; }
	inline void set_m_xmlAttribute_135(XML_TagAttributeU5BU5D_t3301714177* value)
	{
		___m_xmlAttribute_135 = value;
		Il2CppCodeGenWriteBarrier(&___m_xmlAttribute_135, value);
	}

	inline static int32_t get_offset_of_tag_LineIndent_136() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___tag_LineIndent_136)); }
	inline float get_tag_LineIndent_136() const { return ___tag_LineIndent_136; }
	inline float* get_address_of_tag_LineIndent_136() { return &___tag_LineIndent_136; }
	inline void set_tag_LineIndent_136(float value)
	{
		___tag_LineIndent_136 = value;
	}

	inline static int32_t get_offset_of_tag_Indent_137() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___tag_Indent_137)); }
	inline float get_tag_Indent_137() const { return ___tag_Indent_137; }
	inline float* get_address_of_tag_Indent_137() { return &___tag_Indent_137; }
	inline void set_tag_Indent_137(float value)
	{
		___tag_Indent_137 = value;
	}

	inline static int32_t get_offset_of_m_indentStack_138() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_indentStack_138)); }
	inline TMP_XmlTagStack_1_t1206294321  get_m_indentStack_138() const { return ___m_indentStack_138; }
	inline TMP_XmlTagStack_1_t1206294321 * get_address_of_m_indentStack_138() { return &___m_indentStack_138; }
	inline void set_m_indentStack_138(TMP_XmlTagStack_1_t1206294321  value)
	{
		___m_indentStack_138 = value;
	}

	inline static int32_t get_offset_of_tag_NoParsing_139() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___tag_NoParsing_139)); }
	inline bool get_tag_NoParsing_139() const { return ___tag_NoParsing_139; }
	inline bool* get_address_of_tag_NoParsing_139() { return &___tag_NoParsing_139; }
	inline void set_tag_NoParsing_139(bool value)
	{
		___tag_NoParsing_139 = value;
	}

	inline static int32_t get_offset_of_tag_LinkInfo_140() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___tag_LinkInfo_140)); }
	inline TMP_LinkInfo_t2467896998  get_tag_LinkInfo_140() const { return ___tag_LinkInfo_140; }
	inline TMP_LinkInfo_t2467896998 * get_address_of_tag_LinkInfo_140() { return &___tag_LinkInfo_140; }
	inline void set_tag_LinkInfo_140(TMP_LinkInfo_t2467896998  value)
	{
		___tag_LinkInfo_140 = value;
	}

	inline static int32_t get_offset_of_m_isParsingText_141() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_isParsingText_141)); }
	inline bool get_m_isParsingText_141() const { return ___m_isParsingText_141; }
	inline bool* get_address_of_m_isParsingText_141() { return &___m_isParsingText_141; }
	inline void set_m_isParsingText_141(bool value)
	{
		___m_isParsingText_141 = value;
	}

	inline static int32_t get_offset_of_m_char_buffer_142() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_char_buffer_142)); }
	inline Int32U5BU5D_t3230847821* get_m_char_buffer_142() const { return ___m_char_buffer_142; }
	inline Int32U5BU5D_t3230847821** get_address_of_m_char_buffer_142() { return &___m_char_buffer_142; }
	inline void set_m_char_buffer_142(Int32U5BU5D_t3230847821* value)
	{
		___m_char_buffer_142 = value;
		Il2CppCodeGenWriteBarrier(&___m_char_buffer_142, value);
	}

	inline static int32_t get_offset_of_m_internalCharacterInfo_143() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_internalCharacterInfo_143)); }
	inline TMP_CharacterInfoU5BU5D_t2688989516* get_m_internalCharacterInfo_143() const { return ___m_internalCharacterInfo_143; }
	inline TMP_CharacterInfoU5BU5D_t2688989516** get_address_of_m_internalCharacterInfo_143() { return &___m_internalCharacterInfo_143; }
	inline void set_m_internalCharacterInfo_143(TMP_CharacterInfoU5BU5D_t2688989516* value)
	{
		___m_internalCharacterInfo_143 = value;
		Il2CppCodeGenWriteBarrier(&___m_internalCharacterInfo_143, value);
	}

	inline static int32_t get_offset_of_m_input_CharArray_144() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_input_CharArray_144)); }
	inline CharU5BU5D_t3324145743* get_m_input_CharArray_144() const { return ___m_input_CharArray_144; }
	inline CharU5BU5D_t3324145743** get_address_of_m_input_CharArray_144() { return &___m_input_CharArray_144; }
	inline void set_m_input_CharArray_144(CharU5BU5D_t3324145743* value)
	{
		___m_input_CharArray_144 = value;
		Il2CppCodeGenWriteBarrier(&___m_input_CharArray_144, value);
	}

	inline static int32_t get_offset_of_m_charArray_Length_145() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_charArray_Length_145)); }
	inline int32_t get_m_charArray_Length_145() const { return ___m_charArray_Length_145; }
	inline int32_t* get_address_of_m_charArray_Length_145() { return &___m_charArray_Length_145; }
	inline void set_m_charArray_Length_145(int32_t value)
	{
		___m_charArray_Length_145 = value;
	}

	inline static int32_t get_offset_of_m_VisibleCharacters_146() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_VisibleCharacters_146)); }
	inline List_1_t4230808090 * get_m_VisibleCharacters_146() const { return ___m_VisibleCharacters_146; }
	inline List_1_t4230808090 ** get_address_of_m_VisibleCharacters_146() { return &___m_VisibleCharacters_146; }
	inline void set_m_VisibleCharacters_146(List_1_t4230808090 * value)
	{
		___m_VisibleCharacters_146 = value;
		Il2CppCodeGenWriteBarrier(&___m_VisibleCharacters_146, value);
	}

	inline static int32_t get_offset_of_m_settings_147() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_settings_147)); }
	inline TMP_Settings_t2708659969 * get_m_settings_147() const { return ___m_settings_147; }
	inline TMP_Settings_t2708659969 ** get_address_of_m_settings_147() { return &___m_settings_147; }
	inline void set_m_settings_147(TMP_Settings_t2708659969 * value)
	{
		___m_settings_147 = value;
		Il2CppCodeGenWriteBarrier(&___m_settings_147, value);
	}

	inline static int32_t get_offset_of_m_isNewTextObject_148() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_isNewTextObject_148)); }
	inline bool get_m_isNewTextObject_148() const { return ___m_isNewTextObject_148; }
	inline bool* get_address_of_m_isNewTextObject_148() { return &___m_isNewTextObject_148; }
	inline void set_m_isNewTextObject_148(bool value)
	{
		___m_isNewTextObject_148 = value;
	}

	inline static int32_t get_offset_of_m_warningsDisabled_149() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_warningsDisabled_149)); }
	inline bool get_m_warningsDisabled_149() const { return ___m_warningsDisabled_149; }
	inline bool* get_address_of_m_warningsDisabled_149() { return &___m_warningsDisabled_149; }
	inline void set_m_warningsDisabled_149(bool value)
	{
		___m_warningsDisabled_149 = value;
	}

	inline static int32_t get_offset_of_m_characterCount_150() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_characterCount_150)); }
	inline int32_t get_m_characterCount_150() const { return ___m_characterCount_150; }
	inline int32_t* get_address_of_m_characterCount_150() { return &___m_characterCount_150; }
	inline void set_m_characterCount_150(int32_t value)
	{
		___m_characterCount_150 = value;
	}

	inline static int32_t get_offset_of_m_visibleCharacterCount_151() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_visibleCharacterCount_151)); }
	inline int32_t get_m_visibleCharacterCount_151() const { return ___m_visibleCharacterCount_151; }
	inline int32_t* get_address_of_m_visibleCharacterCount_151() { return &___m_visibleCharacterCount_151; }
	inline void set_m_visibleCharacterCount_151(int32_t value)
	{
		___m_visibleCharacterCount_151 = value;
	}

	inline static int32_t get_offset_of_m_visibleSpriteCount_152() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_visibleSpriteCount_152)); }
	inline int32_t get_m_visibleSpriteCount_152() const { return ___m_visibleSpriteCount_152; }
	inline int32_t* get_address_of_m_visibleSpriteCount_152() { return &___m_visibleSpriteCount_152; }
	inline void set_m_visibleSpriteCount_152(int32_t value)
	{
		___m_visibleSpriteCount_152 = value;
	}

	inline static int32_t get_offset_of_m_firstCharacterOfLine_153() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_firstCharacterOfLine_153)); }
	inline int32_t get_m_firstCharacterOfLine_153() const { return ___m_firstCharacterOfLine_153; }
	inline int32_t* get_address_of_m_firstCharacterOfLine_153() { return &___m_firstCharacterOfLine_153; }
	inline void set_m_firstCharacterOfLine_153(int32_t value)
	{
		___m_firstCharacterOfLine_153 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacterOfLine_154() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_firstVisibleCharacterOfLine_154)); }
	inline int32_t get_m_firstVisibleCharacterOfLine_154() const { return ___m_firstVisibleCharacterOfLine_154; }
	inline int32_t* get_address_of_m_firstVisibleCharacterOfLine_154() { return &___m_firstVisibleCharacterOfLine_154; }
	inline void set_m_firstVisibleCharacterOfLine_154(int32_t value)
	{
		___m_firstVisibleCharacterOfLine_154 = value;
	}

	inline static int32_t get_offset_of_m_lastCharacterOfLine_155() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_lastCharacterOfLine_155)); }
	inline int32_t get_m_lastCharacterOfLine_155() const { return ___m_lastCharacterOfLine_155; }
	inline int32_t* get_address_of_m_lastCharacterOfLine_155() { return &___m_lastCharacterOfLine_155; }
	inline void set_m_lastCharacterOfLine_155(int32_t value)
	{
		___m_lastCharacterOfLine_155 = value;
	}

	inline static int32_t get_offset_of_m_lastVisibleCharacterOfLine_156() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_lastVisibleCharacterOfLine_156)); }
	inline int32_t get_m_lastVisibleCharacterOfLine_156() const { return ___m_lastVisibleCharacterOfLine_156; }
	inline int32_t* get_address_of_m_lastVisibleCharacterOfLine_156() { return &___m_lastVisibleCharacterOfLine_156; }
	inline void set_m_lastVisibleCharacterOfLine_156(int32_t value)
	{
		___m_lastVisibleCharacterOfLine_156 = value;
	}

	inline static int32_t get_offset_of_m_lineNumber_157() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_lineNumber_157)); }
	inline int32_t get_m_lineNumber_157() const { return ___m_lineNumber_157; }
	inline int32_t* get_address_of_m_lineNumber_157() { return &___m_lineNumber_157; }
	inline void set_m_lineNumber_157(int32_t value)
	{
		___m_lineNumber_157 = value;
	}

	inline static int32_t get_offset_of_m_pageNumber_158() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_pageNumber_158)); }
	inline int32_t get_m_pageNumber_158() const { return ___m_pageNumber_158; }
	inline int32_t* get_address_of_m_pageNumber_158() { return &___m_pageNumber_158; }
	inline void set_m_pageNumber_158(int32_t value)
	{
		___m_pageNumber_158 = value;
	}

	inline static int32_t get_offset_of_m_maxAscender_159() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_maxAscender_159)); }
	inline float get_m_maxAscender_159() const { return ___m_maxAscender_159; }
	inline float* get_address_of_m_maxAscender_159() { return &___m_maxAscender_159; }
	inline void set_m_maxAscender_159(float value)
	{
		___m_maxAscender_159 = value;
	}

	inline static int32_t get_offset_of_m_maxDescender_160() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_maxDescender_160)); }
	inline float get_m_maxDescender_160() const { return ___m_maxDescender_160; }
	inline float* get_address_of_m_maxDescender_160() { return &___m_maxDescender_160; }
	inline void set_m_maxDescender_160(float value)
	{
		___m_maxDescender_160 = value;
	}

	inline static int32_t get_offset_of_m_maxLineAscender_161() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_maxLineAscender_161)); }
	inline float get_m_maxLineAscender_161() const { return ___m_maxLineAscender_161; }
	inline float* get_address_of_m_maxLineAscender_161() { return &___m_maxLineAscender_161; }
	inline void set_m_maxLineAscender_161(float value)
	{
		___m_maxLineAscender_161 = value;
	}

	inline static int32_t get_offset_of_m_maxLineDescender_162() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_maxLineDescender_162)); }
	inline float get_m_maxLineDescender_162() const { return ___m_maxLineDescender_162; }
	inline float* get_address_of_m_maxLineDescender_162() { return &___m_maxLineDescender_162; }
	inline void set_m_maxLineDescender_162(float value)
	{
		___m_maxLineDescender_162 = value;
	}

	inline static int32_t get_offset_of_m_startOfLineAscender_163() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_startOfLineAscender_163)); }
	inline float get_m_startOfLineAscender_163() const { return ___m_startOfLineAscender_163; }
	inline float* get_address_of_m_startOfLineAscender_163() { return &___m_startOfLineAscender_163; }
	inline void set_m_startOfLineAscender_163(float value)
	{
		___m_startOfLineAscender_163 = value;
	}

	inline static int32_t get_offset_of_m_maxFontScale_164() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_maxFontScale_164)); }
	inline float get_m_maxFontScale_164() const { return ___m_maxFontScale_164; }
	inline float* get_address_of_m_maxFontScale_164() { return &___m_maxFontScale_164; }
	inline void set_m_maxFontScale_164(float value)
	{
		___m_maxFontScale_164 = value;
	}

	inline static int32_t get_offset_of_m_lineOffset_165() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_lineOffset_165)); }
	inline float get_m_lineOffset_165() const { return ___m_lineOffset_165; }
	inline float* get_address_of_m_lineOffset_165() { return &___m_lineOffset_165; }
	inline void set_m_lineOffset_165(float value)
	{
		___m_lineOffset_165 = value;
	}

	inline static int32_t get_offset_of_m_meshExtents_166() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_meshExtents_166)); }
	inline Extents_t2060714539  get_m_meshExtents_166() const { return ___m_meshExtents_166; }
	inline Extents_t2060714539 * get_address_of_m_meshExtents_166() { return &___m_meshExtents_166; }
	inline void set_m_meshExtents_166(Extents_t2060714539  value)
	{
		___m_meshExtents_166 = value;
	}

	inline static int32_t get_offset_of_m_htmlColor_167() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_htmlColor_167)); }
	inline Color32_t598853688  get_m_htmlColor_167() const { return ___m_htmlColor_167; }
	inline Color32_t598853688 * get_address_of_m_htmlColor_167() { return &___m_htmlColor_167; }
	inline void set_m_htmlColor_167(Color32_t598853688  value)
	{
		___m_htmlColor_167 = value;
	}

	inline static int32_t get_offset_of_m_colorStack_168() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_colorStack_168)); }
	inline TMP_XmlTagStack_1_t1808196333  get_m_colorStack_168() const { return ___m_colorStack_168; }
	inline TMP_XmlTagStack_1_t1808196333 * get_address_of_m_colorStack_168() { return &___m_colorStack_168; }
	inline void set_m_colorStack_168(TMP_XmlTagStack_1_t1808196333  value)
	{
		___m_colorStack_168 = value;
	}

	inline static int32_t get_offset_of_m_tabSpacing_169() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_tabSpacing_169)); }
	inline float get_m_tabSpacing_169() const { return ___m_tabSpacing_169; }
	inline float* get_address_of_m_tabSpacing_169() { return &___m_tabSpacing_169; }
	inline void set_m_tabSpacing_169(float value)
	{
		___m_tabSpacing_169 = value;
	}

	inline static int32_t get_offset_of_m_spacing_170() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_spacing_170)); }
	inline float get_m_spacing_170() const { return ___m_spacing_170; }
	inline float* get_address_of_m_spacing_170() { return &___m_spacing_170; }
	inline void set_m_spacing_170(float value)
	{
		___m_spacing_170 = value;
	}

	inline static int32_t get_offset_of_IsRectTransformDriven_171() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___IsRectTransformDriven_171)); }
	inline bool get_IsRectTransformDriven_171() const { return ___IsRectTransformDriven_171; }
	inline bool* get_address_of_IsRectTransformDriven_171() { return &___IsRectTransformDriven_171; }
	inline void set_IsRectTransformDriven_171(bool value)
	{
		___IsRectTransformDriven_171 = value;
	}

	inline static int32_t get_offset_of_m_styleStack_172() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_styleStack_172)); }
	inline TMP_XmlTagStack_1_t2363181145  get_m_styleStack_172() const { return ___m_styleStack_172; }
	inline TMP_XmlTagStack_1_t2363181145 * get_address_of_m_styleStack_172() { return &___m_styleStack_172; }
	inline void set_m_styleStack_172(TMP_XmlTagStack_1_t2363181145  value)
	{
		___m_styleStack_172 = value;
	}

	inline static int32_t get_offset_of_m_padding_173() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_padding_173)); }
	inline float get_m_padding_173() const { return ___m_padding_173; }
	inline float* get_address_of_m_padding_173() { return &___m_padding_173; }
	inline void set_m_padding_173(float value)
	{
		___m_padding_173 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffset_174() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_baselineOffset_174)); }
	inline float get_m_baselineOffset_174() const { return ___m_baselineOffset_174; }
	inline float* get_address_of_m_baselineOffset_174() { return &___m_baselineOffset_174; }
	inline void set_m_baselineOffset_174(float value)
	{
		___m_baselineOffset_174 = value;
	}

	inline static int32_t get_offset_of_m_xAdvance_175() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_xAdvance_175)); }
	inline float get_m_xAdvance_175() const { return ___m_xAdvance_175; }
	inline float* get_address_of_m_xAdvance_175() { return &___m_xAdvance_175; }
	inline void set_m_xAdvance_175(float value)
	{
		___m_xAdvance_175 = value;
	}

	inline static int32_t get_offset_of_m_textElementType_176() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_textElementType_176)); }
	inline int32_t get_m_textElementType_176() const { return ___m_textElementType_176; }
	inline int32_t* get_address_of_m_textElementType_176() { return &___m_textElementType_176; }
	inline void set_m_textElementType_176(int32_t value)
	{
		___m_textElementType_176 = value;
	}

	inline static int32_t get_offset_of_m_cached_TextElement_177() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_cached_TextElement_177)); }
	inline TMP_TextElement_t1232249705 * get_m_cached_TextElement_177() const { return ___m_cached_TextElement_177; }
	inline TMP_TextElement_t1232249705 ** get_address_of_m_cached_TextElement_177() { return &___m_cached_TextElement_177; }
	inline void set_m_cached_TextElement_177(TMP_TextElement_t1232249705 * value)
	{
		___m_cached_TextElement_177 = value;
		Il2CppCodeGenWriteBarrier(&___m_cached_TextElement_177, value);
	}

	inline static int32_t get_offset_of_m_cached_Underline_GlyphInfo_178() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_cached_Underline_GlyphInfo_178)); }
	inline TMP_Glyph_t1999257094 * get_m_cached_Underline_GlyphInfo_178() const { return ___m_cached_Underline_GlyphInfo_178; }
	inline TMP_Glyph_t1999257094 ** get_address_of_m_cached_Underline_GlyphInfo_178() { return &___m_cached_Underline_GlyphInfo_178; }
	inline void set_m_cached_Underline_GlyphInfo_178(TMP_Glyph_t1999257094 * value)
	{
		___m_cached_Underline_GlyphInfo_178 = value;
		Il2CppCodeGenWriteBarrier(&___m_cached_Underline_GlyphInfo_178, value);
	}

	inline static int32_t get_offset_of_m_spriteCount_179() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_spriteCount_179)); }
	inline int32_t get_m_spriteCount_179() const { return ___m_spriteCount_179; }
	inline int32_t* get_address_of_m_spriteCount_179() { return &___m_spriteCount_179; }
	inline void set_m_spriteCount_179(int32_t value)
	{
		___m_spriteCount_179 = value;
	}

	inline static int32_t get_offset_of_m_spriteIndex_180() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_spriteIndex_180)); }
	inline int32_t get_m_spriteIndex_180() const { return ___m_spriteIndex_180; }
	inline int32_t* get_address_of_m_spriteIndex_180() { return &___m_spriteIndex_180; }
	inline void set_m_spriteIndex_180(int32_t value)
	{
		___m_spriteIndex_180 = value;
	}

	inline static int32_t get_offset_of_m_inlineGraphics_181() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___m_inlineGraphics_181)); }
	inline InlineGraphicManager_t3583857972 * get_m_inlineGraphics_181() const { return ___m_inlineGraphics_181; }
	inline InlineGraphicManager_t3583857972 ** get_address_of_m_inlineGraphics_181() { return &___m_inlineGraphics_181; }
	inline void set_m_inlineGraphics_181(InlineGraphicManager_t3583857972 * value)
	{
		___m_inlineGraphics_181 = value;
		Il2CppCodeGenWriteBarrier(&___m_inlineGraphics_181, value);
	}

	inline static int32_t get_offset_of_k_Power_182() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___k_Power_182)); }
	inline SingleU5BU5D_t2316563989* get_k_Power_182() const { return ___k_Power_182; }
	inline SingleU5BU5D_t2316563989** get_address_of_k_Power_182() { return &___k_Power_182; }
	inline void set_k_Power_182(SingleU5BU5D_t2316563989* value)
	{
		___k_Power_182 = value;
		Il2CppCodeGenWriteBarrier(&___k_Power_182, value);
	}

	inline static int32_t get_offset_of_k_InfinityVector_183() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___k_InfinityVector_183)); }
	inline Vector2_t4282066565  get_k_InfinityVector_183() const { return ___k_InfinityVector_183; }
	inline Vector2_t4282066565 * get_address_of_k_InfinityVector_183() { return &___k_InfinityVector_183; }
	inline void set_k_InfinityVector_183(Vector2_t4282066565  value)
	{
		___k_InfinityVector_183 = value;
	}

	inline static int32_t get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_184() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659, ___U3CautoSizeTextContainerU3Ek__BackingField_184)); }
	inline bool get_U3CautoSizeTextContainerU3Ek__BackingField_184() const { return ___U3CautoSizeTextContainerU3Ek__BackingField_184; }
	inline bool* get_address_of_U3CautoSizeTextContainerU3Ek__BackingField_184() { return &___U3CautoSizeTextContainerU3Ek__BackingField_184; }
	inline void set_U3CautoSizeTextContainerU3Ek__BackingField_184(bool value)
	{
		___U3CautoSizeTextContainerU3Ek__BackingField_184 = value;
	}
};

struct TMP_Text_t980027659_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_Text::s_colorWhite
	Color32_t598853688  ___s_colorWhite_44;

public:
	inline static int32_t get_offset_of_s_colorWhite_44() { return static_cast<int32_t>(offsetof(TMP_Text_t980027659_StaticFields, ___s_colorWhite_44)); }
	inline Color32_t598853688  get_s_colorWhite_44() const { return ___s_colorWhite_44; }
	inline Color32_t598853688 * get_address_of_s_colorWhite_44() { return &___s_colorWhite_44; }
	inline void set_s_colorWhite_44(Color32_t598853688  value)
	{
		___s_colorWhite_44 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
