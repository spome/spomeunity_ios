﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PublicSpotMessageDB
struct PublicSpotMessageDB_t1853916122;
// PublicSpotMessage
struct PublicSpotMessage_t3890192988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PublicSpotMessage3890192988.h"

// System.Void PublicSpotMessageDB::.ctor()
extern "C"  void PublicSpotMessageDB__ctor_m520832209 (PublicSpotMessageDB_t1853916122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublicSpotMessageDB::.cctor()
extern "C"  void PublicSpotMessageDB__cctor_m2778800380 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PublicSpotMessageDB PublicSpotMessageDB::get_Instance()
extern "C"  PublicSpotMessageDB_t1853916122 * PublicSpotMessageDB_get_Instance_m2156621208 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublicSpotMessageDB::Awake()
extern "C"  void PublicSpotMessageDB_Awake_m758437428 (PublicSpotMessageDB_t1853916122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublicSpotMessageDB::AddPSMDB(PublicSpotMessage)
extern "C"  void PublicSpotMessageDB_AddPSMDB_m1227598172 (PublicSpotMessageDB_t1853916122 * __this, PublicSpotMessage_t3890192988 * ___messageid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PublicSpotMessage PublicSpotMessageDB::MessagesByIndex(System.Int32)
extern "C"  PublicSpotMessage_t3890192988 * PublicSpotMessageDB_MessagesByIndex_m3257673094 (PublicSpotMessageDB_t1853916122 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublicSpotMessageDB::AllDeletePSMDB()
extern "C"  void PublicSpotMessageDB_AllDeletePSMDB_m95139407 (PublicSpotMessageDB_t1853916122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
