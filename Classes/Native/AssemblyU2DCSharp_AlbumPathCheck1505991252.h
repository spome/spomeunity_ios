﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.InputField
struct InputField_t609046876;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t3603375195;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.UI.Toggle
struct Toggle_t110812896;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AlbumPathCheck
struct  AlbumPathCheck_t1505991252  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.InputField AlbumPathCheck::m_Title
	InputField_t609046876 * ___m_Title_2;
	// UnityEngine.UI.InputField AlbumPathCheck::m_Addr01
	InputField_t609046876 * ___m_Addr01_3;
	// UnityEngine.UI.InputField AlbumPathCheck::m_Addr02
	InputField_t609046876 * ___m_Addr02_4;
	// TMPro.TextMeshProUGUI AlbumPathCheck::m_Path
	TextMeshProUGUI_t3603375195 * ___m_Path_5;
	// UnityEngine.GameObject AlbumPathCheck::ProgressBar
	GameObject_t3674682005 * ___ProgressBar_6;
	// UnityEngine.UI.Image AlbumPathCheck::image
	Image_t538875265 * ___image_7;
	// UnityEngine.UI.Toggle AlbumPathCheck::ImageSelect
	Toggle_t110812896 * ___ImageSelect_8;
	// UnityEngine.UI.Toggle AlbumPathCheck::VideoSelect
	Toggle_t110812896 * ___VideoSelect_9;
	// System.String AlbumPathCheck::realfilename
	String_t* ___realfilename_10;
	// System.String AlbumPathCheck::tempfilename
	String_t* ___tempfilename_11;
	// System.Int32 AlbumPathCheck::_pref
	int32_t ____pref_12;
	// System.String AlbumPathCheck::lat
	String_t* ___lat_13;
	// System.String AlbumPathCheck::lon
	String_t* ___lon_14;
	// System.Int32 AlbumPathCheck::FileType
	int32_t ___FileType_15;
	// UnityEngine.GameObject AlbumPathCheck::addresspanel
	GameObject_t3674682005 * ___addresspanel_16;
	// UnityEngine.GameObject AlbumPathCheck::buttonspanel
	GameObject_t3674682005 * ___buttonspanel_17;

public:
	inline static int32_t get_offset_of_m_Title_2() { return static_cast<int32_t>(offsetof(AlbumPathCheck_t1505991252, ___m_Title_2)); }
	inline InputField_t609046876 * get_m_Title_2() const { return ___m_Title_2; }
	inline InputField_t609046876 ** get_address_of_m_Title_2() { return &___m_Title_2; }
	inline void set_m_Title_2(InputField_t609046876 * value)
	{
		___m_Title_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Title_2, value);
	}

	inline static int32_t get_offset_of_m_Addr01_3() { return static_cast<int32_t>(offsetof(AlbumPathCheck_t1505991252, ___m_Addr01_3)); }
	inline InputField_t609046876 * get_m_Addr01_3() const { return ___m_Addr01_3; }
	inline InputField_t609046876 ** get_address_of_m_Addr01_3() { return &___m_Addr01_3; }
	inline void set_m_Addr01_3(InputField_t609046876 * value)
	{
		___m_Addr01_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_Addr01_3, value);
	}

	inline static int32_t get_offset_of_m_Addr02_4() { return static_cast<int32_t>(offsetof(AlbumPathCheck_t1505991252, ___m_Addr02_4)); }
	inline InputField_t609046876 * get_m_Addr02_4() const { return ___m_Addr02_4; }
	inline InputField_t609046876 ** get_address_of_m_Addr02_4() { return &___m_Addr02_4; }
	inline void set_m_Addr02_4(InputField_t609046876 * value)
	{
		___m_Addr02_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_Addr02_4, value);
	}

	inline static int32_t get_offset_of_m_Path_5() { return static_cast<int32_t>(offsetof(AlbumPathCheck_t1505991252, ___m_Path_5)); }
	inline TextMeshProUGUI_t3603375195 * get_m_Path_5() const { return ___m_Path_5; }
	inline TextMeshProUGUI_t3603375195 ** get_address_of_m_Path_5() { return &___m_Path_5; }
	inline void set_m_Path_5(TextMeshProUGUI_t3603375195 * value)
	{
		___m_Path_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_Path_5, value);
	}

	inline static int32_t get_offset_of_ProgressBar_6() { return static_cast<int32_t>(offsetof(AlbumPathCheck_t1505991252, ___ProgressBar_6)); }
	inline GameObject_t3674682005 * get_ProgressBar_6() const { return ___ProgressBar_6; }
	inline GameObject_t3674682005 ** get_address_of_ProgressBar_6() { return &___ProgressBar_6; }
	inline void set_ProgressBar_6(GameObject_t3674682005 * value)
	{
		___ProgressBar_6 = value;
		Il2CppCodeGenWriteBarrier(&___ProgressBar_6, value);
	}

	inline static int32_t get_offset_of_image_7() { return static_cast<int32_t>(offsetof(AlbumPathCheck_t1505991252, ___image_7)); }
	inline Image_t538875265 * get_image_7() const { return ___image_7; }
	inline Image_t538875265 ** get_address_of_image_7() { return &___image_7; }
	inline void set_image_7(Image_t538875265 * value)
	{
		___image_7 = value;
		Il2CppCodeGenWriteBarrier(&___image_7, value);
	}

	inline static int32_t get_offset_of_ImageSelect_8() { return static_cast<int32_t>(offsetof(AlbumPathCheck_t1505991252, ___ImageSelect_8)); }
	inline Toggle_t110812896 * get_ImageSelect_8() const { return ___ImageSelect_8; }
	inline Toggle_t110812896 ** get_address_of_ImageSelect_8() { return &___ImageSelect_8; }
	inline void set_ImageSelect_8(Toggle_t110812896 * value)
	{
		___ImageSelect_8 = value;
		Il2CppCodeGenWriteBarrier(&___ImageSelect_8, value);
	}

	inline static int32_t get_offset_of_VideoSelect_9() { return static_cast<int32_t>(offsetof(AlbumPathCheck_t1505991252, ___VideoSelect_9)); }
	inline Toggle_t110812896 * get_VideoSelect_9() const { return ___VideoSelect_9; }
	inline Toggle_t110812896 ** get_address_of_VideoSelect_9() { return &___VideoSelect_9; }
	inline void set_VideoSelect_9(Toggle_t110812896 * value)
	{
		___VideoSelect_9 = value;
		Il2CppCodeGenWriteBarrier(&___VideoSelect_9, value);
	}

	inline static int32_t get_offset_of_realfilename_10() { return static_cast<int32_t>(offsetof(AlbumPathCheck_t1505991252, ___realfilename_10)); }
	inline String_t* get_realfilename_10() const { return ___realfilename_10; }
	inline String_t** get_address_of_realfilename_10() { return &___realfilename_10; }
	inline void set_realfilename_10(String_t* value)
	{
		___realfilename_10 = value;
		Il2CppCodeGenWriteBarrier(&___realfilename_10, value);
	}

	inline static int32_t get_offset_of_tempfilename_11() { return static_cast<int32_t>(offsetof(AlbumPathCheck_t1505991252, ___tempfilename_11)); }
	inline String_t* get_tempfilename_11() const { return ___tempfilename_11; }
	inline String_t** get_address_of_tempfilename_11() { return &___tempfilename_11; }
	inline void set_tempfilename_11(String_t* value)
	{
		___tempfilename_11 = value;
		Il2CppCodeGenWriteBarrier(&___tempfilename_11, value);
	}

	inline static int32_t get_offset_of__pref_12() { return static_cast<int32_t>(offsetof(AlbumPathCheck_t1505991252, ____pref_12)); }
	inline int32_t get__pref_12() const { return ____pref_12; }
	inline int32_t* get_address_of__pref_12() { return &____pref_12; }
	inline void set__pref_12(int32_t value)
	{
		____pref_12 = value;
	}

	inline static int32_t get_offset_of_lat_13() { return static_cast<int32_t>(offsetof(AlbumPathCheck_t1505991252, ___lat_13)); }
	inline String_t* get_lat_13() const { return ___lat_13; }
	inline String_t** get_address_of_lat_13() { return &___lat_13; }
	inline void set_lat_13(String_t* value)
	{
		___lat_13 = value;
		Il2CppCodeGenWriteBarrier(&___lat_13, value);
	}

	inline static int32_t get_offset_of_lon_14() { return static_cast<int32_t>(offsetof(AlbumPathCheck_t1505991252, ___lon_14)); }
	inline String_t* get_lon_14() const { return ___lon_14; }
	inline String_t** get_address_of_lon_14() { return &___lon_14; }
	inline void set_lon_14(String_t* value)
	{
		___lon_14 = value;
		Il2CppCodeGenWriteBarrier(&___lon_14, value);
	}

	inline static int32_t get_offset_of_FileType_15() { return static_cast<int32_t>(offsetof(AlbumPathCheck_t1505991252, ___FileType_15)); }
	inline int32_t get_FileType_15() const { return ___FileType_15; }
	inline int32_t* get_address_of_FileType_15() { return &___FileType_15; }
	inline void set_FileType_15(int32_t value)
	{
		___FileType_15 = value;
	}

	inline static int32_t get_offset_of_addresspanel_16() { return static_cast<int32_t>(offsetof(AlbumPathCheck_t1505991252, ___addresspanel_16)); }
	inline GameObject_t3674682005 * get_addresspanel_16() const { return ___addresspanel_16; }
	inline GameObject_t3674682005 ** get_address_of_addresspanel_16() { return &___addresspanel_16; }
	inline void set_addresspanel_16(GameObject_t3674682005 * value)
	{
		___addresspanel_16 = value;
		Il2CppCodeGenWriteBarrier(&___addresspanel_16, value);
	}

	inline static int32_t get_offset_of_buttonspanel_17() { return static_cast<int32_t>(offsetof(AlbumPathCheck_t1505991252, ___buttonspanel_17)); }
	inline GameObject_t3674682005 * get_buttonspanel_17() const { return ___buttonspanel_17; }
	inline GameObject_t3674682005 ** get_address_of_buttonspanel_17() { return &___buttonspanel_17; }
	inline void set_buttonspanel_17(GameObject_t3674682005 * value)
	{
		___buttonspanel_17 = value;
		Il2CppCodeGenWriteBarrier(&___buttonspanel_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
