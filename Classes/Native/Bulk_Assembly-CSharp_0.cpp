﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t3053238939;
// AddDropDownContents
struct AddDropDownContents_t2054805772;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;
// UnityEngine.UI.Dropdown
struct Dropdown_t4201779933;
// System.Object
struct Il2CppObject;
// AddDropDownContents/<Process>c__Iterator1
struct U3CProcessU3Ec__Iterator1_t1937986568;
// AlbumPathCheck
struct AlbumPathCheck_t1505991252;
// UnityEngine.WWW
struct WWW_t3134621005;
// AlbumPathCheck/<CheckProgress>c__Iterator4
struct U3CCheckProgressU3Ec__Iterator4_t1868153709;
// TMPro.TMP_Text
struct TMP_Text_t980027659;
// AlbumPathCheck/<Send>c__Iterator2
struct U3CSendU3Ec__Iterator2_t611604662;
// AlbumPathCheck/<UploadFileCo>c__Iterator3
struct U3CUploadFileCoU3Ec__Iterator3_t1598057078;
// AndroidBackBtn
struct AndroidBackBtn_t3275646630;
// AndroidWebview
struct AndroidWebview_t552345258;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;
// ApplicationChrome
struct ApplicationChrome_t3150366666;
// CurrentScreen
struct CurrentScreen_t3026574117;
// CurrentScreen/<JsonStart>c__Iterator5
struct U3CJsonStartU3Ec__Iterator5_t1185739994;
// CurrentScreen/<LoginCheckProgress>c__Iterator6
struct U3CLoginCheckProgressU3Ec__Iterator6_t4282206053;
// CurrentScreen/<MessageCountGetJsonStart>c__Iterator7
struct U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382;
// CurrentState
struct CurrentState_t1622142648;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// EmailChange
struct EmailChange_t3079654988;
// System.String[]
struct StringU5BU5D_t4054002952;
// EmailChange/<CheckProgress>c__Iterator9
struct U3CCheckProgressU3Ec__Iterator9_t1393611626;
// EmailChange/<RegisterDelete>c__IteratorA
struct U3CRegisterDeleteU3Ec__IteratorA_t1809913895;
// EmailChange/<RegisterSend>c__Iterator8
struct U3CRegisterSendU3Ec__Iterator8_t1720518241;
// EmailRecv
struct EmailRecv_t4124314498;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.Button
struct Button_t3896396478;
// EmailRecv/<GetEmailList>c__IteratorB
struct U3CGetEmailListU3Ec__IteratorB_t1941116220;
// EmailRecv/<Progress>c__IteratorC
struct U3CProgressU3Ec__IteratorC_t2850438612;
// EmailRecv/<TakeJson>c__AnonStorey22
struct U3CTakeJsonU3Ec__AnonStorey22_t1761236519;
// EmailRecv/<TakeJson>c__AnonStorey23
struct U3CTakeJsonU3Ec__AnonStorey23_t1761236520;
// ErrorMessage
struct ErrorMessage_t1367556351;
// ErrorMessage/<AndroidAlertDialog>c__AnonStorey24
struct U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// ErrorMessage/<AndroidAlertDialog>c__AnonStorey25
struct U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221;
// ErrorMessage/NegativeButtonListner
struct NegativeButtonListner_t2426404166;
// ErrorMessage/PositiveButtonListner
struct PositiveButtonListner_t2125458946;
// FPSCounter
struct FPSCounter_t1808341235;
// FPSCounter/<FPS>c__Iterator0
struct U3CFPSU3Ec__Iterator0_t1063757940;
// GPSManager
struct GPSManager_t1489257411;
// GPSManager/<OnApplicationPause>c__IteratorE
struct U3COnApplicationPauseU3Ec__IteratorE_t330808093;
// GPSManager/<Start>c__IteratorD
struct U3CStartU3Ec__IteratorD_t1827712127;
// GreatmanMessage
struct GreatmanMessage_t1817744250;
// GreatmanMessage/<CheckProgress>c__Iterator12
struct U3CCheckProgressU3Ec__Iterator12_t3998861986;
// GreatmanMessage/<ImageLoad>c__Iterator11
struct U3CImageLoadU3Ec__Iterator11_t890999725;
// UnityEngine.UI.Image
struct Image_t538875265;
// GreatmanMessage/<VideoDown>c__IteratorF
struct U3CVideoDownU3Ec__IteratorF_t830887869;
// GreatmanMessage/<VideoStart>c__Iterator10
struct U3CVideoStartU3Ec__Iterator10_t4263606752;
// iosWebView
struct iosWebView_t3524785996;
// WebViewObject
struct WebViewObject_t388577433;
// Loading
struct Loading_t2001303836;
// Loading/<Load>c__Iterator13
struct U3CLoadU3Ec__Iterator13_t1639883204;
// LoginCheck
struct LoginCheck_t219618111;
// Main
struct Main_t2390489;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t2276120119;
// MapUnityView
struct MapUnityView_t4158937566;
// MessageCheck
struct MessageCheck_t3146986849;
// MessageCheck/<CheckProgress>c__Iterator17
struct U3CCheckProgressU3Ec__Iterator17_t354264608;
// MessageCheck/<EmailInsertserver>c__Iterator18
struct U3CEmailInsertserverU3Ec__Iterator18_t4256762244;
// MessageCheck/<EmailInsertserverProgress>c__Iterator19
struct U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746;
// MessageCheck/<ImageLoad>c__Iterator16
struct U3CImageLoadU3Ec__Iterator16_t4085388203;
// MessageCheck/<MessageDeleteServerCheck>c__Iterator1B
struct U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789;
// MessageCheck/<MessageDeleteServerUpdate>c__Iterator1A
struct U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995;
// MessageCheck/<VideoDown>c__Iterator14
struct U3CVideoDownU3Ec__Iterator14_t3182108037;
// MessageCheck/<VideoStart>c__Iterator15
struct U3CVideoStartU3Ec__Iterator15_t210434316;
// MessageSee
struct MessageSee_t302730988;
// MessageSee/<JsonStart>c__Iterator1C
struct U3CJsonStartU3Ec__Iterator1C_t1966288966;
// MessageSee/<MessageSeeCheckProgress>c__Iterator1D
struct U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070;
// MessageSee/<MessageSeeRecvCheckProgress>c__Iterator1F
struct U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178;
// MessageSee/<RecvJsonStart>c__Iterator1E
struct U3CRecvJsonStartU3Ec__Iterator1E_t2718408706;
// MessageSee/<RecvTakeJson>c__AnonStorey27
struct U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148;
// MessageSee/<TakeJson>c__AnonStorey26
struct U3CTakeJsonU3Ec__AnonStorey26_t291817749;
// PrivateURL
struct PrivateURL_t117402892;
// ProgressBar.ProgressRadialBehaviour
struct ProgressRadialBehaviour_t2198524043;
// ProgressBar.Utils.FillerProperty
struct FillerProperty_t3769378557;
// ProgressBar.Utils.OnCompleteEvent
struct OnCompleteEvent_t4208438896;
// ProgressBar.Utils.ProgressValue
struct ProgressValue_t1899486002;
// PublicSpotMessage
struct PublicSpotMessage_t3890192988;
// PublicSpotMessageDB
struct PublicSpotMessageDB_t1853916122;
// SaveInput
struct SaveInput_t2167614509;
// ScreenDebug
struct ScreenDebug_t1599402279;
// SpotMessageGet
struct SpotMessageGet_t1944980625;
// SpotMessageGet/<GreatManCheckProgress>c__Iterator21
struct U3CGreatManCheckProgressU3Ec__Iterator21_t200358716;
// SpotMessageGet/<MessageGetJsonStart>c__Iterator20
struct U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382;
// SpotMessageGet/<MessageGetTakeJson>c__AnonStorey28
struct U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019;
// TextManager
struct TextManager_t1663192480;
// TMPro.Compute_DT_EventArgs
struct Compute_DT_EventArgs_t2335785766;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// TMPro.FaceInfo
struct FaceInfo_t3469866561;
// TMPro.FastAction
struct FastAction_t2355551432;
// System.Action
struct Action_t3771233898;
// TMPro.Glyph2D
struct Glyph2D_t3497109536;
// TMPro.InlineGraphic
struct InlineGraphic_t3606901041;
// UnityEngine.Texture
struct Texture_t2526458961;
// TMPro.InlineGraphicManager
struct InlineGraphicManager_t3583857972;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3053238933.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3053238933MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220439.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220439MethodDeclarations.h"
#include "AssemblyU2DCSharp_AddDropDownContents2054805772.h"
#include "AssemblyU2DCSharp_AddDropDownContents2054805772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_Int321153838500.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "AssemblyU2DCSharp_AddDropDownContents_U3CProcessU31937986568MethodDeclarations.h"
#include "AssemblyU2DCSharp_AddDropDownContents_U3CProcessU31937986568.h"
#include "mscorlib_System_IO_StringReader4061477668MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlDocument730752740MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNode856910923MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1553873098MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData185687546MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown4201779933MethodDeclarations.h"
#include "mscorlib_System_IO_StringReader4061477668.h"
#include "System_Xml_System_Xml_XmlNodeList991860617.h"
#include "System_Xml_System_Xml_XmlDocument730752740.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1553873098.h"
#include "System_Xml_System_Xml_XmlNode856910923.h"
#include "System_Xml_System_Xml_XmlNodeList991860617MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "System_Xml_System_Xml_XmlAttributeCollection3012627841.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap977482698MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap977482698.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData185687546.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown4201779933.h"
#include "UnityEngine_UnityEngine_WWW3134621005MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "AssemblyU2DCSharp_AlbumPathCheck1505991252.h"
#include "AssemblyU2DCSharp_AlbumPathCheck1505991252MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application2856536070MethodDeclarations.h"
#include "AssemblyU2DCSharp_ErrorMessage1367556351MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle110812896MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1885181538MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CtrlPlugins3380180629MethodDeclarations.h"
#include "AssemblyU2DCSharp_AndroidWebview552345258MethodDeclarations.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035.h"
#include "AssemblyU2DCSharp_TextManager_Texts2791099287.h"
#include "mscorlib_System_Enum2862688501MethodDeclarations.h"
#include "mscorlib_System_Enum2862688501.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle110812896.h"
#include "AssemblyU2DCSharp_AndroidWebview552345258.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField609046876MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField609046876.h"
#include "AssemblyU2DCSharp_TMPro_TMP_Text980027659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LocationService3853025142MethodDeclarations.h"
#include "AssemblyU2DCSharp_TMPro_TextMeshProUGUI3603375195.h"
#include "UnityEngine_UnityEngine_LocationService3853025142.h"
#include "AssemblyU2DCSharp_AlbumPathCheck_U3CSendU3Ec__Itera611604662MethodDeclarations.h"
#include "AssemblyU2DCSharp_AlbumPathCheck_U3CSendU3Ec__Itera611604662.h"
#include "AssemblyU2DCSharp_AlbumPathCheck_U3CUploadFileCoU31598057078MethodDeclarations.h"
#include "AssemblyU2DCSharp_AlbumPathCheck_U3CUploadFileCoU31598057078.h"
#include "AssemblyU2DCSharp_AlbumPathCheck_U3CCheckProgressU1868153709MethodDeclarations.h"
#include "AssemblyU2DCSharp_AlbumPathCheck_U3CCheckProgressU1868153709.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "AssemblyU2DCSharp_CurrentState1622142648MethodDeclarations.h"
#include "AssemblyU2DCSharp_CurrentState_PopupMessages2071685857.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image538875265MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "AssemblyU2DCSharp_TMPro_TMP_Text980027659.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image538875265.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279.h"
#include "UnityEngine_UnityEngine_WWWForm461342257MethodDeclarations.h"
#include "AssemblyU2DCSharp_PrivateURL117402892MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWForm461342257.h"
#include "AssemblyU2DCSharp_CurrentState_MemberStates3108678483.h"
#include "mscorlib_System_IO_FileInfo3233670074MethodDeclarations.h"
#include "LitJson_LitJson_JsonMapper863513565MethodDeclarations.h"
#include "LitJson_LitJson_JsonData1715015430MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718MethodDeclarations.h"
#include "mscorlib_System_GC1614687344MethodDeclarations.h"
#include "mscorlib_System_IO_FileInfo3233670074.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_Double3868226565.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte2862609660.h"
#include "LitJson_LitJson_JsonData1715015430.h"
#include "AssemblyU2DCSharp_AndroidBackBtn3275646630.h"
#include "AssemblyU2DCSharp_AndroidBackBtn3275646630MethodDeclarations.h"
#include "AssemblyU2DCSharp_CurrentScreen3026574117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"
#include "AssemblyU2DCSharp_CurrentState_States2558012761.h"
#include "AssemblyU2DCSharp_CurrentScreen3026574117.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass1816259147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject2362096582MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass1816259147.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject2362096582.h"
#include "AssemblyU2DCSharp_ApplicationChrome3150366666.h"
#include "AssemblyU2DCSharp_ApplicationChrome3150366666MethodDeclarations.h"
#include "AssemblyU2DCSharp_ApplicationChrome_States3850394631.h"
#include "AssemblyU2DCSharp_ApplicationChrome_States3850394631MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "AssemblyU2DCSharp_PublicSpotMessageDB1853916122MethodDeclarations.h"
#include "AssemblyU2DCSharp_PublicSpotMessageDB1853916122.h"
#include "AssemblyU2DCSharp_LoginCheck219618111MethodDeclarations.h"
#include "AssemblyU2DCSharp_iosWebView3524785996MethodDeclarations.h"
#include "AssemblyU2DCSharp_iosWebView3524785996.h"
#include "AssemblyU2DCSharp_CurrentScreen_U3CJsonStartU3Ec__1185739994MethodDeclarations.h"
#include "AssemblyU2DCSharp_CurrentScreen_U3CJsonStartU3Ec__1185739994.h"
#include "AssemblyU2DCSharp_CurrentScreen_U3CLoginCheckProgr4282206053MethodDeclarations.h"
#include "AssemblyU2DCSharp_CurrentScreen_U3CLoginCheckProgr4282206053.h"
#include "AssemblyU2DCSharp_CurrentState_LoginPopupMessages1285607942.h"
#include "AssemblyU2DCSharp_CurrentScreen_U3CMessageCountGet1453453382MethodDeclarations.h"
#include "AssemblyU2DCSharp_CurrentScreen_U3CMessageCountGet1453453382.h"
#include "AssemblyU2DCSharp_CurrentState_InquiryTitles1700261515.h"
#include "AssemblyU2DCSharp_CurrentScreen_LoginBtn1150883325.h"
#include "AssemblyU2DCSharp_CurrentScreen_LoginBtn1150883325MethodDeclarations.h"
#include "AssemblyU2DCSharp_CurrentScreen_MainBtn525776089.h"
#include "AssemblyU2DCSharp_CurrentScreen_MainBtn525776089MethodDeclarations.h"
#include "AssemblyU2DCSharp_CurrentScreen_Sumabo110Btn1225887993.h"
#include "AssemblyU2DCSharp_CurrentScreen_Sumabo110Btn1225887993MethodDeclarations.h"
#include "AssemblyU2DCSharp_CurrentScreen_SumabomenuBtn2605881168.h"
#include "AssemblyU2DCSharp_CurrentScreen_SumabomenuBtn2605881168MethodDeclarations.h"
#include "AssemblyU2DCSharp_CurrentState1622142648.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2595600883MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2595600883.h"
#include "AssemblyU2DCSharp_CurrentState_GPSPopupMessages960090149.h"
#include "AssemblyU2DCSharp_CurrentState_GPSPopupMessages960090149MethodDeclarations.h"
#include "AssemblyU2DCSharp_CurrentState_InquiryTitles1700261515MethodDeclarations.h"
#include "AssemblyU2DCSharp_CurrentState_LoginPopupMessages1285607942MethodDeclarations.h"
#include "AssemblyU2DCSharp_CurrentState_MemberStates3108678483MethodDeclarations.h"
#include "AssemblyU2DCSharp_CurrentState_PopupMessages2071685857MethodDeclarations.h"
#include "AssemblyU2DCSharp_CurrentState_States2558012761MethodDeclarations.h"
#include "AssemblyU2DCSharp_EmailChange3079654988.h"
#include "AssemblyU2DCSharp_EmailChange3079654988MethodDeclarations.h"
#include "AssemblyU2DCSharp_EmailChange_U3CRegisterSendU3Ec_1720518241MethodDeclarations.h"
#include "AssemblyU2DCSharp_EmailChange_U3CRegisterSendU3Ec_1720518241.h"
#include "AssemblyU2DCSharp_EmailChange_U3CCheckProgressU3Ec1393611626MethodDeclarations.h"
#include "AssemblyU2DCSharp_EmailChange_U3CCheckProgressU3Ec1393611626.h"
#include "AssemblyU2DCSharp_EmailChange_U3CRegisterDeleteU3E1809913895MethodDeclarations.h"
#include "AssemblyU2DCSharp_EmailChange_U3CRegisterDeleteU3E1809913895.h"
#include "AssemblyU2DCSharp_EmailRecv4124314498.h"
#include "AssemblyU2DCSharp_EmailRecv4124314498MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_EmailRecv_U3CGetEmailListU3Ec__I1941116220MethodDeclarations.h"
#include "AssemblyU2DCSharp_EmailRecv_U3CGetEmailListU3Ec__I1941116220.h"
#include "AssemblyU2DCSharp_EmailRecv_U3CProgressU3Ec__Itera2850438612MethodDeclarations.h"
#include "AssemblyU2DCSharp_EmailRecv_U3CProgressU3Ec__Itera2850438612.h"
#include "AssemblyU2DCSharp_EmailRecv_U3CTakeJsonU3Ec__AnonS1761236520MethodDeclarations.h"
#include "AssemblyU2DCSharp_EmailRecv_U3CTakeJsonU3Ec__AnonS1761236519MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform972643934MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button3896396478MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction594794173MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent1266085011MethodDeclarations.h"
#include "AssemblyU2DCSharp_EmailRecv_U3CTakeJsonU3Ec__AnonS1761236520.h"
#include "AssemblyU2DCSharp_EmailRecv_U3CTakeJsonU3Ec__AnonS1761236519.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button3896396478.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked2796375743.h"
#include "UnityEngine_UnityEngine_Events_UnityAction594794173.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3856946179.h"
#include "AssemblyU2DCSharp_ErrorMessage1367556351.h"
#include "AssemblyU2DCSharp_ErrorMessage_U3CAndroidAlertDial2424498220MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaRunnable1289602340MethodDeclarations.h"
#include "AssemblyU2DCSharp_ErrorMessage_U3CAndroidAlertDial2424498220.h"
#include "UnityEngine_UnityEngine_AndroidJavaRunnable1289602340.h"
#include "AssemblyU2DCSharp_ErrorMessage_U3CAndroidAlertDial2424498221MethodDeclarations.h"
#include "AssemblyU2DCSharp_ErrorMessage_U3CAndroidAlertDial2424498221.h"
#include "AssemblyU2DCSharp_ErrorMessage_PositiveButtonListn2125458946MethodDeclarations.h"
#include "AssemblyU2DCSharp_ErrorMessage_PositiveButtonListn2125458946.h"
#include "AssemblyU2DCSharp_ErrorMessage_NegativeButtonListn2426404166MethodDeclarations.h"
#include "AssemblyU2DCSharp_ErrorMessage_NegativeButtonListn2426404166.h"
#include "UnityEngine_UnityEngine_AndroidJavaProxy1828457281MethodDeclarations.h"
#include "AssemblyU2DCSharp_GPSManager1489257411MethodDeclarations.h"
#include "mscorlib_System_Double3868226565MethodDeclarations.h"
#include "AssemblyU2DCSharp_FPSCounter1808341235.h"
#include "AssemblyU2DCSharp_FPSCounter1808341235MethodDeclarations.h"
#include "AssemblyU2DCSharp_FPSCounter_U3CFPSU3Ec__Iterator01063757940MethodDeclarations.h"
#include "AssemblyU2DCSharp_FPSCounter_U3CFPSU3Ec__Iterator01063757940.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "AssemblyU2DCSharp_GPSManager1489257411.h"
#include "AssemblyU2DCSharp_GPSManager_U3CStartU3Ec__Iterato1827712127MethodDeclarations.h"
#include "AssemblyU2DCSharp_GPSManager_U3CStartU3Ec__Iterato1827712127.h"
#include "AssemblyU2DCSharp_GPSManager_U3COnApplicationPauseU330808093MethodDeclarations.h"
#include "AssemblyU2DCSharp_GPSManager_U3COnApplicationPauseU330808093.h"
#include "UnityEngine_UnityEngine_LocationInfo3215517959MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LocationInfo3215517959.h"
#include "mscorlib_System_Math2862914300MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocationState3286232508.h"
#include "UnityEngine_UnityEngine_LocationServiceStatus1153071304.h"
#include "AssemblyU2DCSharp_GreatmanMessage1817744250.h"
#include "AssemblyU2DCSharp_GreatmanMessage1817744250MethodDeclarations.h"
#include "AssemblyU2DCSharp_PublicSpotMessage3890192988.h"
#include "mscorlib_System_Char2862622538.h"
#include "mscorlib_System_Convert1363677321MethodDeclarations.h"
#include "AssemblyU2DCSharp_GreatmanMessage_U3CVideoDownU3Ec_830887869MethodDeclarations.h"
#include "AssemblyU2DCSharp_GreatmanMessage_U3CVideoDownU3Ec_830887869.h"
#include "AssemblyU2DCSharp_GreatmanMessage_U3CVideoStartU3E4263606752MethodDeclarations.h"
#include "AssemblyU2DCSharp_GreatmanMessage_U3CVideoStartU3E4263606752.h"
#include "AssemblyU2DCSharp_GreatmanMessage_U3CImageLoadU3Ec_890999725MethodDeclarations.h"
#include "AssemblyU2DCSharp_GreatmanMessage_U3CImageLoadU3Ec_890999725.h"
#include "AssemblyU2DCSharp_GreatmanMessage_U3CCheckProgress3998861986MethodDeclarations.h"
#include "AssemblyU2DCSharp_GreatmanMessage_U3CCheckProgress3998861986.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195MethodDeclarations.h"
#include "mscorlib_System_IO_File667612524MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite3199167241MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_TextureFormat4189619560.h"
#include "UnityEngine_UnityEngine_Texture2526458961MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"
#include "mscorlib_System_Action_1_gen403047693MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebViewObject388577432MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebViewObject388577432.h"
#include "mscorlib_System_Action_1_gen403047693.h"
#include "AssemblyU2DCSharp_Loading2001303836.h"
#include "AssemblyU2DCSharp_Loading2001303836MethodDeclarations.h"
#include "AssemblyU2DCSharp_Loading_U3CLoadU3Ec__Iterator131639883204MethodDeclarations.h"
#include "AssemblyU2DCSharp_Loading_U3CLoadU3Ec__Iterator131639883204.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2940962239MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103.h"
#include "AssemblyU2DCSharp_LocationState3286232508MethodDeclarations.h"
#include "AssemblyU2DCSharp_LoginCheck219618111.h"
#include "AssemblyU2DCSharp_Main2390489.h"
#include "AssemblyU2DCSharp_Main2390489MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste2276120119.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste2276120119MethodDeclarations.h"
#include "AssemblyU2DCSharp_MapUnityView4158937566.h"
#include "AssemblyU2DCSharp_MapUnityView4158937566MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageCheck3146986849.h"
#include "AssemblyU2DCSharp_MessageCheck3146986849MethodDeclarations.h"
#include "mscorlib_System_IO_DirectoryInfo89154617MethodDeclarations.h"
#include "mscorlib_System_IO_DirectoryInfo89154617.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_Messages3897517420.h"
#include "AssemblyU2DCSharp_MessageCheck_U3CVideoDownU3Ec__I3182108037MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageCheck_U3CVideoDownU3Ec__I3182108037.h"
#include "AssemblyU2DCSharp_MessageCheck_U3CVideoStartU3Ec__I210434316MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageCheck_U3CVideoStartU3Ec__I210434316.h"
#include "AssemblyU2DCSharp_MessageCheck_U3CImageLoadU3Ec__I4085388203MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageCheck_U3CImageLoadU3Ec__I4085388203.h"
#include "AssemblyU2DCSharp_MessageCheck_U3CCheckProgressU3Ec354264608MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageCheck_U3CCheckProgressU3Ec354264608.h"
#include "AssemblyU2DCSharp_MessageCheck_U3CEmailInsertserve4256762244MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageCheck_U3CEmailInsertserve4256762244.h"
#include "AssemblyU2DCSharp_MessageCheck_U3CEmailInsertserver943913746MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageCheck_U3CEmailInsertserver943913746.h"
#include "AssemblyU2DCSharp_MessageCheck_U3CMessageDeleteSer3671298995MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageCheck_U3CMessageDeleteSer3671298995.h"
#include "AssemblyU2DCSharp_MessageCheck_U3CMessageDeleteSer3224710789MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageCheck_U3CMessageDeleteSer3224710789.h"
#include "AssemblyU2DCSharp_Messages3897517420MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageSee302730988.h"
#include "AssemblyU2DCSharp_MessageSee302730988MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageSee_U3CJsonStartU3Ec__Ite1966288966MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageSee_U3CJsonStartU3Ec__Ite1966288966.h"
#include "AssemblyU2DCSharp_MessageSee_U3CMessageSeeCheckProg961994070MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageSee_U3CMessageSeeCheckProg961994070.h"
#include "AssemblyU2DCSharp_MessageSee_U3CTakeJsonU3Ec__AnonS291817749MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageSee_U3CTakeJsonU3Ec__AnonS291817749.h"
#include "AssemblyU2DCSharp_MessageSee_U3CRecvJsonStartU3Ec_2718408706MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageSee_U3CRecvJsonStartU3Ec_2718408706.h"
#include "AssemblyU2DCSharp_MessageSee_U3CMessageSeeRecvChec3146012178MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageSee_U3CMessageSeeRecvChec3146012178.h"
#include "AssemblyU2DCSharp_MessageSee_U3CRecvTakeJsonU3Ec__2132693148MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageSee_U3CRecvTakeJsonU3Ec__2132693148.h"
#include "AssemblyU2DCSharp_PrivateURL117402892.h"
#include "AssemblyU2DCSharp_ProgressBar_ProgressRadialBehavi2198524043.h"
#include "AssemblyU2DCSharp_ProgressBar_ProgressRadialBehavi2198524043MethodDeclarations.h"
#include "AssemblyU2DCSharp_ProgressBar_Utils_OnCompleteEven4208438896.h"
#include "AssemblyU2DCSharp_ProgressBar_Utils_FillerProperty3769378557.h"
#include "AssemblyU2DCSharp_ProgressBar_Utils_FillerProperty3769378557MethodDeclarations.h"
#include "AssemblyU2DCSharp_ProgressBar_Utils_OnCompleteEven4208438896MethodDeclarations.h"
#include "AssemblyU2DCSharp_ProgressBar_Utils_ProgressValue1899486002.h"
#include "AssemblyU2DCSharp_ProgressBar_Utils_ProgressValue1899486002MethodDeclarations.h"
#include "AssemblyU2DCSharp_PublicSpotMessage3890192988MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen963411244MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen963411244.h"
#include "AssemblyU2DCSharp_SaveInput2167614509.h"
#include "AssemblyU2DCSharp_SaveInput2167614509MethodDeclarations.h"
#include "mscorlib_System_Char2862622538MethodDeclarations.h"
#include "mscorlib_System_Globalization_UnicodeCategory1892815653.h"
#include "AssemblyU2DCSharp_ScreenDebug1599402279.h"
#include "AssemblyU2DCSharp_ScreenDebug1599402279MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resolution1578306928MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI3134605553MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin3371348110MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyleState1997423985MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resolution1578306928.h"
#include "UnityEngine_UnityEngine_GUISkin3371348110.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826.h"
#include "UnityEngine_UnityEngine_GUIStyleState1997423985.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_TextAnchor213922566.h"
#include "AssemblyU2DCSharp_SpotMessageGet1944980625.h"
#include "AssemblyU2DCSharp_SpotMessageGet1944980625MethodDeclarations.h"
#include "AssemblyU2DCSharp_SpotMessageGet_U3CMessageGetJson3614256382MethodDeclarations.h"
#include "AssemblyU2DCSharp_SpotMessageGet_U3CMessageGetJson3614256382.h"
#include "AssemblyU2DCSharp_SpotMessageGet_U3CGreatManCheckPr200358716MethodDeclarations.h"
#include "AssemblyU2DCSharp_SpotMessageGet_U3CGreatManCheckPr200358716.h"
#include "AssemblyU2DCSharp_SpotMessageGet_U3CMessageGetTake4134167019MethodDeclarations.h"
#include "AssemblyU2DCSharp_SpotMessageGet_U3CMessageGetTake4134167019.h"
#include "AssemblyU2DCSharp_TextManager1663192480.h"
#include "AssemblyU2DCSharp_TextManager1663192480MethodDeclarations.h"
#include "AssemblyU2DCSharp_TextManager_Texts2791099287MethodDeclarations.h"
#include "AssemblyU2DCSharp_TMPro_Compute_DistanceTransform_4075892261.h"
#include "AssemblyU2DCSharp_TMPro_Compute_DistanceTransform_4075892261MethodDeclarations.h"
#include "AssemblyU2DCSharp_TMPro_Compute_DT_EventArgs2335785766.h"
#include "AssemblyU2DCSharp_TMPro_Compute_DT_EventArgs2335785766MethodDeclarations.h"
#include "AssemblyU2DCSharp_TMPro_Extents2060714539.h"
#include "AssemblyU2DCSharp_TMPro_Extents2060714539MethodDeclarations.h"
#include "AssemblyU2DCSharp_TMPro_FaceInfo3469866561.h"
#include "AssemblyU2DCSharp_TMPro_FaceInfo3469866561MethodDeclarations.h"
#include "AssemblyU2DCSharp_TMPro_FastAction2355551432.h"
#include "AssemblyU2DCSharp_TMPro_FastAction2355551432MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_gen1645869067MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1797527366MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_gen1645869067.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1797527366.h"
#include "System_Core_System_Action3771233898.h"
#include "System_System_Collections_Generic_LinkedListNode_13387968605.h"
#include "System_System_Collections_Generic_LinkedListNode_13387968605MethodDeclarations.h"
#include "System_Core_System_Action3771233898MethodDeclarations.h"
#include "AssemblyU2DCSharp_TMPro_FontCreationSetting3395101892.h"
#include "AssemblyU2DCSharp_TMPro_FontCreationSetting3395101892MethodDeclarations.h"
#include "AssemblyU2DCSharp_TMPro_FontStyles3228051751.h"
#include "AssemblyU2DCSharp_TMPro_FontStyles3228051751MethodDeclarations.h"
#include "AssemblyU2DCSharp_TMPro_Glyph2D3497109536.h"
#include "AssemblyU2DCSharp_TMPro_Glyph2D3497109536MethodDeclarations.h"
#include "AssemblyU2DCSharp_TMPro_InlineGraphic3606901041.h"
#include "AssemblyU2DCSharp_TMPro_InlineGraphic3606901041MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic3186046376MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic836799438.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic836799438MethodDeclarations.h"
#include "AssemblyU2DCSharp_TMPro_InlineGraphicManager3583857972.h"
#include "AssemblyU2DCSharp_TMPro_InlineGraphicManager3583857972MethodDeclarations.h"
#include "AssemblyU2DCSharp_TMPro_TMP_SpriteAsset3955808645.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic3186046376.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Dropdown>()
#define GameObject_GetComponent_TisDropdown_t4201779933_m3155400077(__this, method) ((  Dropdown_t4201779933 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m900797242(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.UI.Dropdown>()
#define Component_GetComponentInChildren_TisDropdown_t4201779933_m3166666881(__this, method) ((  Dropdown_t4201779933 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m782999868_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponentInChildren_TisIl2CppObject_m782999868(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m782999868_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<TMPro.TMP_Text>()
#define GameObject_GetComponentInChildren_TisTMP_Text_t980027659_m2592787986(__this, method) ((  TMP_Text_t980027659 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m782999868_gshared)(__this, method)
// !!0 UnityEngine.AndroidJavaObject::GetStatic<System.Object>(System.String)
extern "C"  Il2CppObject * AndroidJavaObject_GetStatic_TisIl2CppObject_m3341062043_gshared (AndroidJavaObject_t2362096582 * __this, String_t* p0, const MethodInfo* method);
#define AndroidJavaObject_GetStatic_TisIl2CppObject_m3341062043(__this, p0, method) ((  Il2CppObject * (*) (AndroidJavaObject_t2362096582 *, String_t*, const MethodInfo*))AndroidJavaObject_GetStatic_TisIl2CppObject_m3341062043_gshared)(__this, p0, method)
// !!0 UnityEngine.AndroidJavaObject::GetStatic<UnityEngine.AndroidJavaObject>(System.String)
#define AndroidJavaObject_GetStatic_TisAndroidJavaObject_t2362096582_m524545334(__this, p0, method) ((  AndroidJavaObject_t2362096582 * (*) (AndroidJavaObject_t2362096582 *, String_t*, const MethodInfo*))AndroidJavaObject_GetStatic_TisIl2CppObject_m3341062043_gshared)(__this, p0, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3133387403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3133387403(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t3674682005_m3917608929(__this /* static, unused */, p0, method) ((  GameObject_t3674682005 * (*) (Il2CppObject * /* static, unused */, GameObject_t3674682005 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
#define GameObject_GetComponent_TisRectTransform_t972643934_m406276429(__this, method) ((  RectTransform_t972643934 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.UI.Text>()
#define GameObject_GetComponentInChildren_TisText_t9039225_m2924853261(__this, method) ((  Text_t9039225 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m782999868_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
#define GameObject_GetComponent_TisButton_t3896396478_m2812094092(__this, method) ((  Button_t3896396478 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m337943659_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m337943659(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<ErrorMessage>()
#define GameObject_AddComponent_TisErrorMessage_t1367556351_m3524973971(__this, method) ((  ErrorMessage_t1367556351 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.AndroidJavaObject::Call<System.Object>(System.String,System.Object[])
extern "C"  Il2CppObject * AndroidJavaObject_Call_TisIl2CppObject_m3039876358_gshared (AndroidJavaObject_t2362096582 * __this, String_t* p0, ObjectU5BU5D_t1108656482* p1, const MethodInfo* method);
#define AndroidJavaObject_Call_TisIl2CppObject_m3039876358(__this, p0, p1, method) ((  Il2CppObject * (*) (AndroidJavaObject_t2362096582 *, String_t*, ObjectU5BU5D_t1108656482*, const MethodInfo*))AndroidJavaObject_Call_TisIl2CppObject_m3039876358_gshared)(__this, p0, p1, method)
// !!0 UnityEngine.AndroidJavaObject::Call<UnityEngine.AndroidJavaObject>(System.String,System.Object[])
#define AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009(__this, p0, p1, method) ((  AndroidJavaObject_t2362096582 * (*) (AndroidJavaObject_t2362096582 *, String_t*, ObjectU5BU5D_t1108656482*, const MethodInfo*))AndroidJavaObject_Call_TisIl2CppObject_m3039876358_gshared)(__this, p0, p1, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<TMPro.TMP_Text>()
#define Component_GetComponent_TisTMP_Text_t980027659_m3910039766(__this, method) ((  TMP_Text_t980027659 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t9039225_m202917489(__this, method) ((  Text_t9039225 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t538875265_m3706520426(__this, method) ((  Image_t538875265 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<WebViewObject>()
#define GameObject_AddComponent_TisWebViewObject_t388577433_m1260926804(__this, method) ((  WebViewObject_t388577433 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.EventSystems.EventSystem>()
#define GameObject_GetComponent_TisEventSystem_t2276120119_m1517929633(__this, method) ((  EventSystem_t2276120119 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponentInParent_TisIl2CppObject_m1297875695(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInParent<TMPro.InlineGraphicManager>()
#define Component_GetComponentInParent_TisInlineGraphicManager_t3583857972_m3490019100(__this, method) ((  InlineGraphicManager_t3583857972 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t972643934_m1940403147(__this, method) ((  RectTransform_t972643934 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void <PrivateImplementationDetails>::.ctor()
extern "C"  void U3CPrivateImplementationDetailsU3E__ctor_m795736486 (U3CPrivateImplementationDetailsU3E_t3053238939 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$40
extern "C" void U24ArrayTypeU2440_t3379220439_marshal_pinvoke(const U24ArrayTypeU2440_t3379220439& unmarshaled, U24ArrayTypeU2440_t3379220439_marshaled_pinvoke& marshaled)
{
}
extern "C" void U24ArrayTypeU2440_t3379220439_marshal_pinvoke_back(const U24ArrayTypeU2440_t3379220439_marshaled_pinvoke& marshaled, U24ArrayTypeU2440_t3379220439& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$40
extern "C" void U24ArrayTypeU2440_t3379220439_marshal_pinvoke_cleanup(U24ArrayTypeU2440_t3379220439_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$40
extern "C" void U24ArrayTypeU2440_t3379220439_marshal_com(const U24ArrayTypeU2440_t3379220439& unmarshaled, U24ArrayTypeU2440_t3379220439_marshaled_com& marshaled)
{
}
extern "C" void U24ArrayTypeU2440_t3379220439_marshal_com_back(const U24ArrayTypeU2440_t3379220439_marshaled_com& marshaled, U24ArrayTypeU2440_t3379220439& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$40
extern "C" void U24ArrayTypeU2440_t3379220439_marshal_com_cleanup(U24ArrayTypeU2440_t3379220439_marshaled_com& marshaled)
{
}
// System.Void AddDropDownContents::.ctor()
extern Il2CppCodeGenString* _stringLiteral46214657;
extern const uint32_t AddDropDownContents__ctor_m2484709343_MetadataUsageId;
extern "C"  void AddDropDownContents__ctor_m2484709343 (AddDropDownContents_t2054805772 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddDropDownContents__ctor_m2484709343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_strName_2(_stringLiteral46214657);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AddDropDownContents::Start()
extern Il2CppCodeGenString* _stringLiteral3926596689;
extern const uint32_t AddDropDownContents_Start_m1431847135_MetadataUsageId;
extern "C"  void AddDropDownContents_Start_m1431847135 (AddDropDownContents_t2054805772 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddDropDownContents_Start_m1431847135_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral3926596689, /*hidden argument*/NULL);
		__this->set_DD_3(L_0);
		AddDropDownContents_JapanKenName_m1437829982(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AddDropDownContents::Changed(System.Int32)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t AddDropDownContents_Changed_m1971402626_MetadataUsageId;
extern "C"  void AddDropDownContents_Changed_m1971402626 (AddDropDownContents_t2054805772 * __this, int32_t ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddDropDownContents_Changed_m1971402626_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___result0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AddDropDownContents::JapanKenName()
extern "C"  void AddDropDownContents_JapanKenName_m1437829982 (AddDropDownContents_t2054805772 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = AddDropDownContents_Process_m1126075636(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator AddDropDownContents::Process()
extern Il2CppClass* U3CProcessU3Ec__Iterator1_t1937986568_il2cpp_TypeInfo_var;
extern const uint32_t AddDropDownContents_Process_m1126075636_MetadataUsageId;
extern "C"  Il2CppObject * AddDropDownContents_Process_m1126075636 (AddDropDownContents_t2054805772 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddDropDownContents_Process_m1126075636_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CProcessU3Ec__Iterator1_t1937986568 * V_0 = NULL;
	{
		U3CProcessU3Ec__Iterator1_t1937986568 * L_0 = (U3CProcessU3Ec__Iterator1_t1937986568 *)il2cpp_codegen_object_new(U3CProcessU3Ec__Iterator1_t1937986568_il2cpp_TypeInfo_var);
		U3CProcessU3Ec__Iterator1__ctor_m1320430179(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CProcessU3Ec__Iterator1_t1937986568 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_4(__this);
		U3CProcessU3Ec__Iterator1_t1937986568 * L_2 = V_0;
		return L_2;
	}
}
// System.Void AddDropDownContents::Interpret(System.String)
extern Il2CppClass* StringReader_t4061477668_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlDocument_t730752740_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1553873098_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNode_t856910923_il2cpp_TypeInfo_var;
extern Il2CppClass* OptionData_t185687546_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1043861288_MethodInfo_var;
extern const MethodInfo* List_1_Add_m738222104_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisDropdown_t4201779933_m3155400077_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral838851295;
extern Il2CppCodeGenString* _stringLiteral106068;
extern const uint32_t AddDropDownContents_Interpret_m2831212152_MetadataUsageId;
extern "C"  void AddDropDownContents_Interpret_m2831212152 (AddDropDownContents_t2054805772 * __this, String_t* ____strSource0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddDropDownContents_Interpret_m2831212152_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringReader_t4061477668 * V_0 = NULL;
	XmlNodeList_t991860617 * V_1 = NULL;
	XmlDocument_t730752740 * V_2 = NULL;
	List_1_t1553873098 * V_3 = NULL;
	XmlNode_t856910923 * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	XmlNode_t856910923 * V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	Il2CppObject * V_8 = NULL;
	Il2CppObject * V_9 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ____strSource0;
		StringReader_t4061477668 * L_1 = (StringReader_t4061477668 *)il2cpp_codegen_object_new(StringReader_t4061477668_il2cpp_TypeInfo_var);
		StringReader__ctor_m1181104909(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = (XmlNodeList_t991860617 *)NULL;
		XmlDocument_t730752740 * L_2 = (XmlDocument_t730752740 *)il2cpp_codegen_object_new(XmlDocument_t730752740_il2cpp_TypeInfo_var);
		XmlDocument__ctor_m467220425(L_2, /*hidden argument*/NULL);
		V_2 = L_2;
		XmlDocument_t730752740 * L_3 = V_2;
		StringReader_t4061477668 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.IO.StringReader::ReadToEnd() */, L_4);
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(57 /* System.Void System.Xml.XmlDocument::LoadXml(System.String) */, L_3, L_5);
		XmlDocument_t730752740 * L_6 = V_2;
		NullCheck(L_6);
		XmlNodeList_t991860617 * L_7 = XmlNode_SelectNodes_m3515755997(L_6, _stringLiteral838851295, /*hidden argument*/NULL);
		V_1 = L_7;
		List_1_t1553873098 * L_8 = (List_1_t1553873098 *)il2cpp_codegen_object_new(List_1_t1553873098_il2cpp_TypeInfo_var);
		List_1__ctor_m1043861288(L_8, /*hidden argument*/List_1__ctor_m1043861288_MethodInfo_var);
		V_3 = L_8;
		XmlNodeList_t991860617 * L_9 = V_1;
		NullCheck(L_9);
		Il2CppObject * L_10 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_9);
		V_5 = L_10;
	}

IL_0035:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00e4;
		}

IL_003a:
		{
			Il2CppObject * L_11 = V_5;
			NullCheck(L_11);
			Il2CppObject * L_12 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_11);
			V_4 = ((XmlNode_t856910923 *)CastclassClass(L_12, XmlNode_t856910923_il2cpp_TypeInfo_var));
			XmlNode_t856910923 * L_13 = V_4;
			NullCheck(L_13);
			String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(16 /* System.String System.Xml.XmlNode::get_Name() */, L_13);
			NullCheck(L_14);
			bool L_15 = String_Equals_m3541721061(L_14, _stringLiteral838851295, /*hidden argument*/NULL);
			if (!L_15)
			{
				goto IL_00e4;
			}
		}

IL_005e:
		{
			XmlNode_t856910923 * L_16 = V_4;
			NullCheck(L_16);
			bool L_17 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XmlNode::get_HasChildNodes() */, L_16);
			if (!L_17)
			{
				goto IL_00e4;
			}
		}

IL_006a:
		{
			XmlNode_t856910923 * L_18 = V_4;
			NullCheck(L_18);
			XmlNodeList_t991860617 * L_19 = VirtFuncInvoker0< XmlNodeList_t991860617 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_18);
			NullCheck(L_19);
			Il2CppObject * L_20 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_19);
			V_7 = L_20;
		}

IL_0078:
		try
		{ // begin try (depth: 2)
			{
				goto IL_00bd;
			}

IL_007d:
			{
				Il2CppObject * L_21 = V_7;
				NullCheck(L_21);
				Il2CppObject * L_22 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_21);
				V_6 = ((XmlNode_t856910923 *)CastclassClass(L_22, XmlNode_t856910923_il2cpp_TypeInfo_var));
				List_1_t1553873098 * L_23 = V_3;
				XmlNode_t856910923 * L_24 = V_6;
				NullCheck(L_24);
				XmlAttributeCollection_t3012627841 * L_25 = VirtFuncInvoker0< XmlAttributeCollection_t3012627841 * >::Invoke(6 /* System.Xml.XmlAttributeCollection System.Xml.XmlNode::get_Attributes() */, L_24);
				NullCheck(L_25);
				XmlNode_t856910923 * L_26 = VirtFuncInvoker1< XmlNode_t856910923 *, String_t* >::Invoke(7 /* System.Xml.XmlNode System.Xml.XmlNamedNodeMap::GetNamedItem(System.String) */, L_25, _stringLiteral106068);
				NullCheck(L_26);
				String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_26);
				OptionData_t185687546 * L_28 = (OptionData_t185687546 *)il2cpp_codegen_object_new(OptionData_t185687546_il2cpp_TypeInfo_var);
				OptionData__ctor_m3621517956(L_28, L_27, /*hidden argument*/NULL);
				NullCheck(L_23);
				List_1_Add_m738222104(L_23, L_28, /*hidden argument*/List_1_Add_m738222104_MethodInfo_var);
				GameObject_t3674682005 * L_29 = __this->get_DD_3();
				NullCheck(L_29);
				Dropdown_t4201779933 * L_30 = GameObject_GetComponent_TisDropdown_t4201779933_m3155400077(L_29, /*hidden argument*/GameObject_GetComponent_TisDropdown_t4201779933_m3155400077_MethodInfo_var);
				List_1_t1553873098 * L_31 = V_3;
				NullCheck(L_30);
				Dropdown_set_options_m4132251955(L_30, L_31, /*hidden argument*/NULL);
			}

IL_00bd:
			{
				Il2CppObject * L_32 = V_7;
				NullCheck(L_32);
				bool L_33 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_32);
				if (L_33)
				{
					goto IL_007d;
				}
			}

IL_00c9:
			{
				IL2CPP_LEAVE(0xE4, FINALLY_00ce);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
			goto FINALLY_00ce;
		}

FINALLY_00ce:
		{ // begin finally (depth: 2)
			{
				Il2CppObject * L_34 = V_7;
				V_8 = ((Il2CppObject *)IsInst(L_34, IDisposable_t1423340799_il2cpp_TypeInfo_var));
				Il2CppObject * L_35 = V_8;
				if (L_35)
				{
					goto IL_00dc;
				}
			}

IL_00db:
			{
				IL2CPP_END_FINALLY(206)
			}

IL_00dc:
			{
				Il2CppObject * L_36 = V_8;
				NullCheck(L_36);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_36);
				IL2CPP_END_FINALLY(206)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(206)
		{
			IL2CPP_JUMP_TBL(0xE4, IL_00e4)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
		}

IL_00e4:
		{
			Il2CppObject * L_37 = V_5;
			NullCheck(L_37);
			bool L_38 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_37);
			if (L_38)
			{
				goto IL_003a;
			}
		}

IL_00f0:
		{
			IL2CPP_LEAVE(0x10B, FINALLY_00f5);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00f5;
	}

FINALLY_00f5:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_39 = V_5;
			V_9 = ((Il2CppObject *)IsInst(L_39, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			Il2CppObject * L_40 = V_9;
			if (L_40)
			{
				goto IL_0103;
			}
		}

IL_0102:
		{
			IL2CPP_END_FINALLY(245)
		}

IL_0103:
		{
			Il2CppObject * L_41 = V_9;
			NullCheck(L_41);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_41);
			IL2CPP_END_FINALLY(245)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(245)
	{
		IL2CPP_JUMP_TBL(0x10B, IL_010b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_010b:
	{
		return;
	}
}
// System.Void AddDropDownContents/<Process>c__Iterator1::.ctor()
extern "C"  void U3CProcessU3Ec__Iterator1__ctor_m1320430179 (U3CProcessU3Ec__Iterator1_t1937986568 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object AddDropDownContents/<Process>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CProcessU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3058628367 (U3CProcessU3Ec__Iterator1_t1937986568 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object AddDropDownContents/<Process>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CProcessU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m269940899 (U3CProcessU3Ec__Iterator1_t1937986568 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean AddDropDownContents/<Process>c__Iterator1::MoveNext()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern const uint32_t U3CProcessU3Ec__Iterator1_MoveNext_m2969380273_MetadataUsageId;
extern "C"  bool U3CProcessU3Ec__Iterator1_MoveNext_m2969380273 (U3CProcessU3Ec__Iterator1_t1937986568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CProcessU3Ec__Iterator1_MoveNext_m2969380273_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0072;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_U3CstrPathU3E__0_0(L_2);
		String_t* L_3 = __this->get_U3CstrPathU3E__0_0();
		WWW_t3134621005 * L_4 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m1985874080(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__1_1(L_4);
		WWW_t3134621005 * L_5 = __this->get_U3CwwwU3E__1_1();
		__this->set_U24current_3(L_5);
		__this->set_U24PC_2(1);
		goto IL_0074;
	}

IL_0055:
	{
		AddDropDownContents_t2054805772 * L_6 = __this->get_U3CU3Ef__this_4();
		WWW_t3134621005 * L_7 = __this->get_U3CwwwU3E__1_1();
		NullCheck(L_7);
		String_t* L_8 = WWW_get_text_m4216049525(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		AddDropDownContents_Interpret_m2831212152(L_6, L_8, /*hidden argument*/NULL);
		__this->set_U24PC_2((-1));
	}

IL_0072:
	{
		return (bool)0;
	}

IL_0074:
	{
		return (bool)1;
	}
	// Dead block : IL_0076: ldloc.1
}
// System.Void AddDropDownContents/<Process>c__Iterator1::Dispose()
extern "C"  void U3CProcessU3Ec__Iterator1_Dispose_m1816332768 (U3CProcessU3Ec__Iterator1_t1937986568 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void AddDropDownContents/<Process>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CProcessU3Ec__Iterator1_Reset_m3261830416_MetadataUsageId;
extern "C"  void U3CProcessU3Ec__Iterator1_Reset_m3261830416 (U3CProcessU3Ec__Iterator1_t1937986568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CProcessU3Ec__Iterator1_Reset_m3261830416_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void AlbumPathCheck::.ctor()
extern "C"  void AlbumPathCheck__ctor_m2916651207 (AlbumPathCheck_t1505991252 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AlbumPathCheck::ChangeToggle(System.Int32)
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern Il2CppCodeGenString* _stringLiteral792782451;
extern const uint32_t AlbumPathCheck_ChangeToggle_m4061799826_MetadataUsageId;
extern "C"  void AlbumPathCheck_ChangeToggle_m4061799826 (AlbumPathCheck_t1505991252 * __this, int32_t ___num0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AlbumPathCheck_ChangeToggle_m4061799826_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = ((int32_t)3);
		Il2CppObject * L_2 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		int32_t L_4 = ((int32_t)2);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_3, _stringLiteral3660319847, L_6, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		int32_t L_7 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)8))))
		{
			goto IL_00fb;
		}
	}
	{
		int32_t L_8 = ___num0;
		if (L_8)
		{
			goto IL_0076;
		}
	}
	{
		Toggle_t110812896 * L_9 = __this->get_ImageSelect_8();
		NullCheck(L_9);
		bool L_10 = Toggle_get_isOn_m2105608497(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0076;
		}
	}
	{
		Toggle_t110812896 * L_11 = __this->get_ImageSelect_8();
		NullCheck(L_11);
		Selectable_set_interactable_m2686686419(L_11, (bool)1, /*hidden argument*/NULL);
		Toggle_t110812896 * L_12 = __this->get_VideoSelect_9();
		NullCheck(L_12);
		Selectable_set_interactable_m2686686419(L_12, (bool)0, /*hidden argument*/NULL);
		int32_t L_13 = ___num0;
		CtrlPlugins_doTakeAlbumAction_m2126393988(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		__this->set_FileType_15(1);
		goto IL_00f6;
	}

IL_0076:
	{
		int32_t L_14 = ___num0;
		if ((!(((uint32_t)L_14) == ((uint32_t)1))))
		{
			goto IL_00b7;
		}
	}
	{
		Toggle_t110812896 * L_15 = __this->get_VideoSelect_9();
		NullCheck(L_15);
		bool L_16 = Toggle_get_isOn_m2105608497(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00b7;
		}
	}
	{
		Toggle_t110812896 * L_17 = __this->get_ImageSelect_8();
		NullCheck(L_17);
		Selectable_set_interactable_m2686686419(L_17, (bool)0, /*hidden argument*/NULL);
		Toggle_t110812896 * L_18 = __this->get_VideoSelect_9();
		NullCheck(L_18);
		Selectable_set_interactable_m2686686419(L_18, (bool)1, /*hidden argument*/NULL);
		int32_t L_19 = ___num0;
		CtrlPlugins_doTakeAlbumAction_m2126393988(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		__this->set_FileType_15(2);
		goto IL_00f6;
	}

IL_00b7:
	{
		Toggle_t110812896 * L_20 = __this->get_ImageSelect_8();
		NullCheck(L_20);
		bool L_21 = Toggle_get_isOn_m2105608497(L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_00f6;
		}
	}
	{
		Toggle_t110812896 * L_22 = __this->get_VideoSelect_9();
		NullCheck(L_22);
		bool L_23 = Toggle_get_isOn_m2105608497(L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_00f6;
		}
	}
	{
		Toggle_t110812896 * L_24 = __this->get_ImageSelect_8();
		NullCheck(L_24);
		Selectable_set_interactable_m2686686419(L_24, (bool)1, /*hidden argument*/NULL);
		Toggle_t110812896 * L_25 = __this->get_VideoSelect_9();
		NullCheck(L_25);
		Selectable_set_interactable_m2686686419(L_25, (bool)1, /*hidden argument*/NULL);
		__this->set_FileType_15(0);
	}

IL_00f6:
	{
		goto IL_019a;
	}

IL_00fb:
	{
		int32_t L_26 = ___num0;
		if (L_26)
		{
			goto IL_012e;
		}
	}
	{
		Toggle_t110812896 * L_27 = __this->get_ImageSelect_8();
		NullCheck(L_27);
		bool L_28 = Toggle_get_isOn_m2105608497(L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_012e;
		}
	}
	{
		Toggle_t110812896 * L_29 = __this->get_ImageSelect_8();
		NullCheck(L_29);
		Selectable_set_interactable_m2686686419(L_29, (bool)1, /*hidden argument*/NULL);
		Toggle_t110812896 * L_30 = __this->get_VideoSelect_9();
		NullCheck(L_30);
		Selectable_set_interactable_m2686686419(L_30, (bool)0, /*hidden argument*/NULL);
		goto IL_019a;
	}

IL_012e:
	{
		int32_t L_31 = ___num0;
		if ((!(((uint32_t)L_31) == ((uint32_t)1))))
		{
			goto IL_0162;
		}
	}
	{
		Toggle_t110812896 * L_32 = __this->get_VideoSelect_9();
		NullCheck(L_32);
		bool L_33 = Toggle_get_isOn_m2105608497(L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0162;
		}
	}
	{
		Toggle_t110812896 * L_34 = __this->get_ImageSelect_8();
		NullCheck(L_34);
		Selectable_set_interactable_m2686686419(L_34, (bool)0, /*hidden argument*/NULL);
		Toggle_t110812896 * L_35 = __this->get_VideoSelect_9();
		NullCheck(L_35);
		Selectable_set_interactable_m2686686419(L_35, (bool)1, /*hidden argument*/NULL);
		goto IL_019a;
	}

IL_0162:
	{
		Toggle_t110812896 * L_36 = __this->get_ImageSelect_8();
		NullCheck(L_36);
		bool L_37 = Toggle_get_isOn_m2105608497(L_36, /*hidden argument*/NULL);
		if (L_37)
		{
			goto IL_019a;
		}
	}
	{
		Toggle_t110812896 * L_38 = __this->get_VideoSelect_9();
		NullCheck(L_38);
		bool L_39 = Toggle_get_isOn_m2105608497(L_38, /*hidden argument*/NULL);
		if (L_39)
		{
			goto IL_019a;
		}
	}
	{
		Toggle_t110812896 * L_40 = __this->get_ImageSelect_8();
		NullCheck(L_40);
		Selectable_set_interactable_m2686686419(L_40, (bool)1, /*hidden argument*/NULL);
		Toggle_t110812896 * L_41 = __this->get_VideoSelect_9();
		NullCheck(L_41);
		Selectable_set_interactable_m2686686419(L_41, (bool)1, /*hidden argument*/NULL);
	}

IL_019a:
	{
		int32_t L_42 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_42) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0291;
		}
	}
	{
		int32_t L_43 = ___num0;
		if (L_43)
		{
			goto IL_01fc;
		}
	}
	{
		Toggle_t110812896 * L_44 = __this->get_ImageSelect_8();
		NullCheck(L_44);
		bool L_45 = Toggle_get_isOn_m2105608497(L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_01fc;
		}
	}
	{
		Toggle_t110812896 * L_46 = __this->get_ImageSelect_8();
		NullCheck(L_46);
		Selectable_set_interactable_m2686686419(L_46, (bool)1, /*hidden argument*/NULL);
		Toggle_t110812896 * L_47 = __this->get_VideoSelect_9();
		NullCheck(L_47);
		Selectable_set_interactable_m2686686419(L_47, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_48 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_49 = Int32_ToString_m1286526384((&___num0), /*hidden argument*/NULL);
		NullCheck(L_48);
		AndroidWebview_Call_m335597645(L_48, _stringLiteral792782451, L_49, /*hidden argument*/NULL);
		int32_t L_50 = ___num0;
		CtrlPlugins_doTakeAlbumAction_m2126393988(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		__this->set_FileType_15(1);
		goto IL_028c;
	}

IL_01fc:
	{
		int32_t L_51 = ___num0;
		if ((!(((uint32_t)L_51) == ((uint32_t)1))))
		{
			goto IL_024d;
		}
	}
	{
		Toggle_t110812896 * L_52 = __this->get_VideoSelect_9();
		NullCheck(L_52);
		bool L_53 = Toggle_get_isOn_m2105608497(L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_024d;
		}
	}
	{
		Toggle_t110812896 * L_54 = __this->get_ImageSelect_8();
		NullCheck(L_54);
		Selectable_set_interactable_m2686686419(L_54, (bool)0, /*hidden argument*/NULL);
		Toggle_t110812896 * L_55 = __this->get_VideoSelect_9();
		NullCheck(L_55);
		Selectable_set_interactable_m2686686419(L_55, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_56 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_57 = Int32_ToString_m1286526384((&___num0), /*hidden argument*/NULL);
		NullCheck(L_56);
		AndroidWebview_Call_m335597645(L_56, _stringLiteral792782451, L_57, /*hidden argument*/NULL);
		__this->set_FileType_15(2);
		goto IL_028c;
	}

IL_024d:
	{
		Toggle_t110812896 * L_58 = __this->get_ImageSelect_8();
		NullCheck(L_58);
		bool L_59 = Toggle_get_isOn_m2105608497(L_58, /*hidden argument*/NULL);
		if (L_59)
		{
			goto IL_028c;
		}
	}
	{
		Toggle_t110812896 * L_60 = __this->get_VideoSelect_9();
		NullCheck(L_60);
		bool L_61 = Toggle_get_isOn_m2105608497(L_60, /*hidden argument*/NULL);
		if (L_61)
		{
			goto IL_028c;
		}
	}
	{
		Toggle_t110812896 * L_62 = __this->get_ImageSelect_8();
		NullCheck(L_62);
		Selectable_set_interactable_m2686686419(L_62, (bool)1, /*hidden argument*/NULL);
		Toggle_t110812896 * L_63 = __this->get_VideoSelect_9();
		NullCheck(L_63);
		Selectable_set_interactable_m2686686419(L_63, (bool)1, /*hidden argument*/NULL);
		__this->set_FileType_15(0);
	}

IL_028c:
	{
		goto IL_0330;
	}

IL_0291:
	{
		int32_t L_64 = ___num0;
		if (L_64)
		{
			goto IL_02c4;
		}
	}
	{
		Toggle_t110812896 * L_65 = __this->get_ImageSelect_8();
		NullCheck(L_65);
		bool L_66 = Toggle_get_isOn_m2105608497(L_65, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_02c4;
		}
	}
	{
		Toggle_t110812896 * L_67 = __this->get_ImageSelect_8();
		NullCheck(L_67);
		Selectable_set_interactable_m2686686419(L_67, (bool)1, /*hidden argument*/NULL);
		Toggle_t110812896 * L_68 = __this->get_VideoSelect_9();
		NullCheck(L_68);
		Selectable_set_interactable_m2686686419(L_68, (bool)0, /*hidden argument*/NULL);
		goto IL_0330;
	}

IL_02c4:
	{
		int32_t L_69 = ___num0;
		if ((!(((uint32_t)L_69) == ((uint32_t)1))))
		{
			goto IL_02f8;
		}
	}
	{
		Toggle_t110812896 * L_70 = __this->get_VideoSelect_9();
		NullCheck(L_70);
		bool L_71 = Toggle_get_isOn_m2105608497(L_70, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_02f8;
		}
	}
	{
		Toggle_t110812896 * L_72 = __this->get_ImageSelect_8();
		NullCheck(L_72);
		Selectable_set_interactable_m2686686419(L_72, (bool)0, /*hidden argument*/NULL);
		Toggle_t110812896 * L_73 = __this->get_VideoSelect_9();
		NullCheck(L_73);
		Selectable_set_interactable_m2686686419(L_73, (bool)1, /*hidden argument*/NULL);
		goto IL_0330;
	}

IL_02f8:
	{
		Toggle_t110812896 * L_74 = __this->get_ImageSelect_8();
		NullCheck(L_74);
		bool L_75 = Toggle_get_isOn_m2105608497(L_74, /*hidden argument*/NULL);
		if (L_75)
		{
			goto IL_0330;
		}
	}
	{
		Toggle_t110812896 * L_76 = __this->get_VideoSelect_9();
		NullCheck(L_76);
		bool L_77 = Toggle_get_isOn_m2105608497(L_76, /*hidden argument*/NULL);
		if (L_77)
		{
			goto IL_0330;
		}
	}
	{
		Toggle_t110812896 * L_78 = __this->get_ImageSelect_8();
		NullCheck(L_78);
		Selectable_set_interactable_m2686686419(L_78, (bool)1, /*hidden argument*/NULL);
		Toggle_t110812896 * L_79 = __this->get_VideoSelect_9();
		NullCheck(L_79);
		Selectable_set_interactable_m2686686419(L_79, (bool)1, /*hidden argument*/NULL);
	}

IL_0330:
	{
		return;
	}
}
// System.Void AlbumPathCheck::AndroidAlbumPath(System.String)
extern Il2CppCodeGenString* _stringLiteral3245250296;
extern const uint32_t AlbumPathCheck_AndroidAlbumPath_m3497750624_MetadataUsageId;
extern "C"  void AlbumPathCheck_AndroidAlbumPath_m3497750624 (AlbumPathCheck_t1505991252 * __this, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AlbumPathCheck_AndroidAlbumPath_m3497750624_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path0;
		AlbumPathCheck_UploadFile_m3385188612(__this, L_0, _stringLiteral3245250296, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AlbumPathCheck::AndroidAlbumFileName(System.String)
extern "C"  void AlbumPathCheck_AndroidAlbumFileName_m3922339870 (AlbumPathCheck_t1505991252 * __this, String_t* ___filename0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___filename0;
		__this->set_tempfilename_11(L_0);
		return;
	}
}
// System.Void AlbumPathCheck::OnValueChanged(System.Int32)
extern "C"  void AlbumPathCheck_OnValueChanged_m2658262640 (AlbumPathCheck_t1505991252 * __this, int32_t ___result0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___result0;
		__this->set__pref_12(L_0);
		return;
	}
}
// System.Void AlbumPathCheck::MessageSend()
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern Il2CppCodeGenString* _stringLiteral2211684265;
extern Il2CppCodeGenString* _stringLiteral387758323;
extern Il2CppCodeGenString* _stringLiteral4137715065;
extern Il2CppCodeGenString* _stringLiteral3533545053;
extern Il2CppCodeGenString* _stringLiteral399239137;
extern Il2CppCodeGenString* _stringLiteral2285666846;
extern Il2CppCodeGenString* _stringLiteral2136207780;
extern Il2CppCodeGenString* _stringLiteral2516828959;
extern Il2CppCodeGenString* _stringLiteral47602;
extern Il2CppCodeGenString* _stringLiteral3228181440;
extern Il2CppCodeGenString* _stringLiteral3556308;
extern const uint32_t AlbumPathCheck_MessageSend_m1793086292_MetadataUsageId;
extern "C"  void AlbumPathCheck_MessageSend_m1793086292 (AlbumPathCheck_t1505991252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AlbumPathCheck_MessageSend_m1793086292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_1 = ((int32_t)3);
		Il2CppObject * L_2 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		int32_t L_4 = ((int32_t)2);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_3, _stringLiteral3660319847, L_6, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral2211684265, _stringLiteral3660319847, /*hidden argument*/NULL);
		return;
	}

IL_003a:
	{
		InputField_t609046876 * L_7 = __this->get_m_Title_2();
		NullCheck(L_7);
		String_t* L_8 = InputField_get_text_m3972300732(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_10 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0084;
		}
	}
	{
		int32_t L_11 = ((int32_t)3);
		Il2CppObject * L_12 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_11);
		NullCheck((Enum_t2862688501 *)L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_12);
		int32_t L_14 = ((int32_t)2);
		Il2CppObject * L_15 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_14);
		NullCheck((Enum_t2862688501 *)L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_15);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_13, _stringLiteral387758323, L_16, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral4137715065, _stringLiteral387758323, /*hidden argument*/NULL);
		return;
	}

IL_0084:
	{
		String_t* L_17 = __this->get_tempfilename_11();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_19 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00a4;
		}
	}
	{
		String_t* L_20 = __this->get_tempfilename_11();
		if (L_20)
		{
			goto IL_00d4;
		}
	}

IL_00a4:
	{
		int32_t L_21 = ((int32_t)3);
		Il2CppObject * L_22 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_21);
		NullCheck((Enum_t2862688501 *)L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_22);
		int32_t L_24 = ((int32_t)2);
		Il2CppObject * L_25 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_24);
		NullCheck((Enum_t2862688501 *)L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_25);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_23, _stringLiteral3533545053, L_26, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral4137715065, _stringLiteral3533545053, /*hidden argument*/NULL);
		return;
	}

IL_00d4:
	{
		GameObject_t3674682005 * L_27 = __this->get_addresspanel_16();
		NullCheck(L_27);
		bool L_28 = GameObject_get_activeSelf_m3858025161(L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_01d6;
		}
	}
	{
		__this->set__pref_12(2);
		int32_t L_29 = __this->get__pref_12();
		if ((((int32_t)L_29) > ((int32_t)0)))
		{
			goto IL_0127;
		}
	}
	{
		int32_t L_30 = ((int32_t)3);
		Il2CppObject * L_31 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_30);
		NullCheck((Enum_t2862688501 *)L_31);
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_31);
		int32_t L_33 = ((int32_t)2);
		Il2CppObject * L_34 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_33);
		NullCheck((Enum_t2862688501 *)L_34);
		String_t* L_35 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_34);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_32, _stringLiteral399239137, L_35, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral4137715065, _stringLiteral399239137, /*hidden argument*/NULL);
		return;
	}

IL_0127:
	{
		InputField_t609046876 * L_36 = __this->get_m_Addr01_3();
		NullCheck(L_36);
		String_t* L_37 = InputField_get_text_m3972300732(L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_38 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_39 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0171;
		}
	}
	{
		int32_t L_40 = ((int32_t)3);
		Il2CppObject * L_41 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_40);
		NullCheck((Enum_t2862688501 *)L_41);
		String_t* L_42 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_41);
		int32_t L_43 = ((int32_t)2);
		Il2CppObject * L_44 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_43);
		NullCheck((Enum_t2862688501 *)L_44);
		String_t* L_45 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_44);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_42, _stringLiteral2285666846, L_45, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral4137715065, _stringLiteral2136207780, /*hidden argument*/NULL);
		return;
	}

IL_0171:
	{
		InputField_t609046876 * L_46 = __this->get_m_Addr02_4();
		NullCheck(L_46);
		String_t* L_47 = InputField_get_text_m3972300732(L_46, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_49 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_47, L_48, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_01bb;
		}
	}
	{
		int32_t L_50 = ((int32_t)3);
		Il2CppObject * L_51 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_50);
		NullCheck((Enum_t2862688501 *)L_51);
		String_t* L_52 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_51);
		int32_t L_53 = ((int32_t)2);
		Il2CppObject * L_54 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_53);
		NullCheck((Enum_t2862688501 *)L_54);
		String_t* L_55 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_54);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_52, _stringLiteral2516828959, L_55, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral4137715065, _stringLiteral2516828959, /*hidden argument*/NULL);
		return;
	}

IL_01bb:
	{
		__this->set_lat_13(_stringLiteral47602);
		__this->set_lon_14(_stringLiteral47602);
		goto IL_024c;
	}

IL_01d6:
	{
		GameObject_t3674682005 * L_56 = __this->get_buttonspanel_17();
		NullCheck(L_56);
		bool L_57 = GameObject_get_activeSelf_m3858025161(L_56, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_024c;
		}
	}
	{
		String_t* L_58 = __this->get_lat_13();
		if (!L_58)
		{
			goto IL_021c;
		}
	}
	{
		String_t* L_59 = __this->get_lat_13();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_60 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		if (L_60)
		{
			goto IL_021c;
		}
	}
	{
		String_t* L_61 = __this->get_lon_14();
		if (!L_61)
		{
			goto IL_021c;
		}
	}
	{
		String_t* L_62 = __this->get_lon_14();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_63 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_024c;
		}
	}

IL_021c:
	{
		int32_t L_64 = ((int32_t)3);
		Il2CppObject * L_65 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_64);
		NullCheck((Enum_t2862688501 *)L_65);
		String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_65);
		int32_t L_67 = ((int32_t)2);
		Il2CppObject * L_68 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_67);
		NullCheck((Enum_t2862688501 *)L_68);
		String_t* L_69 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_68);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_66, _stringLiteral3228181440, L_69, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral4137715065, _stringLiteral3228181440, /*hidden argument*/NULL);
		return;
	}

IL_024c:
	{
		InputField_t609046876 * L_70 = __this->get_m_Addr02_4();
		NullCheck(L_70);
		String_t* L_71 = InputField_get_text_m3972300732(L_70, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_0271;
		}
	}
	{
		InputField_t609046876 * L_72 = __this->get_m_Addr02_4();
		NullCheck(L_72);
		String_t* L_73 = InputField_get_text_m3972300732(L_72, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_74 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_73, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_0281;
		}
	}

IL_0271:
	{
		InputField_t609046876 * L_75 = __this->get_m_Addr02_4();
		NullCheck(L_75);
		InputField_set_text_m203843887(L_75, _stringLiteral3556308, /*hidden argument*/NULL);
	}

IL_0281:
	{
		InputField_t609046876 * L_76 = __this->get_m_Addr01_3();
		NullCheck(L_76);
		String_t* L_77 = InputField_get_text_m3972300732(L_76, /*hidden argument*/NULL);
		if (!L_77)
		{
			goto IL_02a6;
		}
	}
	{
		InputField_t609046876 * L_78 = __this->get_m_Addr01_3();
		NullCheck(L_78);
		String_t* L_79 = InputField_get_text_m3972300732(L_78, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_80 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_79, /*hidden argument*/NULL);
		if (!L_80)
		{
			goto IL_02b6;
		}
	}

IL_02a6:
	{
		InputField_t609046876 * L_81 = __this->get_m_Addr01_3();
		NullCheck(L_81);
		InputField_set_text_m203843887(L_81, _stringLiteral3556308, /*hidden argument*/NULL);
	}

IL_02b6:
	{
		InputField_t609046876 * L_82 = __this->get_m_Title_2();
		NullCheck(L_82);
		String_t* L_83 = InputField_get_text_m3972300732(L_82, /*hidden argument*/NULL);
		String_t* L_84 = __this->get_realfilename_10();
		int32_t L_85 = __this->get__pref_12();
		InputField_t609046876 * L_86 = __this->get_m_Addr01_3();
		NullCheck(L_86);
		String_t* L_87 = InputField_get_text_m3972300732(L_86, /*hidden argument*/NULL);
		InputField_t609046876 * L_88 = __this->get_m_Addr02_4();
		NullCheck(L_88);
		String_t* L_89 = InputField_get_text_m3972300732(L_88, /*hidden argument*/NULL);
		Il2CppObject * L_90 = AlbumPathCheck_Send_m1955910846(__this, L_83, L_84, L_85, L_87, L_89, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_90, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AlbumPathCheck::OnDisable()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t AlbumPathCheck_OnDisable_m135014830_MetadataUsageId;
extern "C"  void AlbumPathCheck_OnDisable_m135014830 (AlbumPathCheck_t1505991252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AlbumPathCheck_OnDisable_m135014830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InputField_t609046876 * L_0 = __this->get_m_Title_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_0);
		InputField_set_text_m203843887(L_0, L_1, /*hidden argument*/NULL);
		TextMeshProUGUI_t3603375195 * L_2 = __this->get_m_Path_5();
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_2);
		TMP_Text_set_text_m27929696(L_2, L_3, /*hidden argument*/NULL);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_tempfilename_11(L_4);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_realfilename_10(L_5);
		InputField_t609046876 * L_6 = __this->get_m_Addr01_3();
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_6);
		InputField_set_text_m203843887(L_6, L_7, /*hidden argument*/NULL);
		InputField_t609046876 * L_8 = __this->get_m_Addr02_4();
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_8);
		InputField_set_text_m203843887(L_8, L_9, /*hidden argument*/NULL);
		__this->set__pref_12(0);
		GameObject_t3674682005 * L_10 = __this->get_buttonspanel_17();
		NullCheck(L_10);
		GameObject_SetActive_m3538205401(L_10, (bool)1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_11 = __this->get_addresspanel_16();
		NullCheck(L_11);
		GameObject_SetActive_m3538205401(L_11, (bool)0, /*hidden argument*/NULL);
		Toggle_t110812896 * L_12 = __this->get_ImageSelect_8();
		NullCheck(L_12);
		Selectable_set_interactable_m2686686419(L_12, (bool)1, /*hidden argument*/NULL);
		Toggle_t110812896 * L_13 = __this->get_VideoSelect_9();
		NullCheck(L_13);
		Selectable_set_interactable_m2686686419(L_13, (bool)1, /*hidden argument*/NULL);
		Toggle_t110812896 * L_14 = __this->get_ImageSelect_8();
		NullCheck(L_14);
		Toggle_set_isOn_m3467664234(L_14, (bool)0, /*hidden argument*/NULL);
		Toggle_t110812896 * L_15 = __this->get_VideoSelect_9();
		NullCheck(L_15);
		Toggle_set_isOn_m3467664234(L_15, (bool)0, /*hidden argument*/NULL);
		__this->set_FileType_15(0);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_16 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		LocationService_Stop_m4216060557(L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator AlbumPathCheck::Send(System.String,System.String,System.Int32,System.String,System.String)
extern Il2CppClass* U3CSendU3Ec__Iterator2_t611604662_il2cpp_TypeInfo_var;
extern const uint32_t AlbumPathCheck_Send_m1955910846_MetadataUsageId;
extern "C"  Il2CppObject * AlbumPathCheck_Send_m1955910846 (AlbumPathCheck_t1505991252 * __this, String_t* ___title0, String_t* ___filename1, int32_t ___pref2, String_t* ___addr013, String_t* ___addr024, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AlbumPathCheck_Send_m1955910846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSendU3Ec__Iterator2_t611604662 * V_0 = NULL;
	{
		U3CSendU3Ec__Iterator2_t611604662 * L_0 = (U3CSendU3Ec__Iterator2_t611604662 *)il2cpp_codegen_object_new(U3CSendU3Ec__Iterator2_t611604662_il2cpp_TypeInfo_var);
		U3CSendU3Ec__Iterator2__ctor_m740422005(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSendU3Ec__Iterator2_t611604662 * L_1 = V_0;
		String_t* L_2 = ___title0;
		NullCheck(L_1);
		L_1->set_title_1(L_2);
		U3CSendU3Ec__Iterator2_t611604662 * L_3 = V_0;
		String_t* L_4 = ___filename1;
		NullCheck(L_3);
		L_3->set_filename_2(L_4);
		U3CSendU3Ec__Iterator2_t611604662 * L_5 = V_0;
		String_t* L_6 = ___addr013;
		NullCheck(L_5);
		L_5->set_addr01_3(L_6);
		U3CSendU3Ec__Iterator2_t611604662 * L_7 = V_0;
		String_t* L_8 = ___addr024;
		NullCheck(L_7);
		L_7->set_addr02_4(L_8);
		U3CSendU3Ec__Iterator2_t611604662 * L_9 = V_0;
		String_t* L_10 = ___title0;
		NullCheck(L_9);
		L_9->set_U3CU24U3Etitle_8(L_10);
		U3CSendU3Ec__Iterator2_t611604662 * L_11 = V_0;
		String_t* L_12 = ___filename1;
		NullCheck(L_11);
		L_11->set_U3CU24U3Efilename_9(L_12);
		U3CSendU3Ec__Iterator2_t611604662 * L_13 = V_0;
		String_t* L_14 = ___addr013;
		NullCheck(L_13);
		L_13->set_U3CU24U3Eaddr01_10(L_14);
		U3CSendU3Ec__Iterator2_t611604662 * L_15 = V_0;
		String_t* L_16 = ___addr024;
		NullCheck(L_15);
		L_15->set_U3CU24U3Eaddr02_11(L_16);
		U3CSendU3Ec__Iterator2_t611604662 * L_17 = V_0;
		NullCheck(L_17);
		L_17->set_U3CU3Ef__this_12(__this);
		U3CSendU3Ec__Iterator2_t611604662 * L_18 = V_0;
		return L_18;
	}
}
// System.Collections.IEnumerator AlbumPathCheck::UploadFileCo(System.String,System.String)
extern Il2CppClass* U3CUploadFileCoU3Ec__Iterator3_t1598057078_il2cpp_TypeInfo_var;
extern const uint32_t AlbumPathCheck_UploadFileCo_m1802983664_MetadataUsageId;
extern "C"  Il2CppObject * AlbumPathCheck_UploadFileCo_m1802983664 (AlbumPathCheck_t1505991252 * __this, String_t* ___localFileName0, String_t* ___uploadURL1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AlbumPathCheck_UploadFileCo_m1802983664_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CUploadFileCoU3Ec__Iterator3_t1598057078 * V_0 = NULL;
	{
		U3CUploadFileCoU3Ec__Iterator3_t1598057078 * L_0 = (U3CUploadFileCoU3Ec__Iterator3_t1598057078 *)il2cpp_codegen_object_new(U3CUploadFileCoU3Ec__Iterator3_t1598057078_il2cpp_TypeInfo_var);
		U3CUploadFileCoU3Ec__Iterator3__ctor_m2396942261(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CUploadFileCoU3Ec__Iterator3_t1598057078 * L_1 = V_0;
		String_t* L_2 = ___localFileName0;
		NullCheck(L_1);
		L_1->set_localFileName_0(L_2);
		U3CUploadFileCoU3Ec__Iterator3_t1598057078 * L_3 = V_0;
		String_t* L_4 = ___uploadURL1;
		NullCheck(L_3);
		L_3->set_uploadURL_5(L_4);
		U3CUploadFileCoU3Ec__Iterator3_t1598057078 * L_5 = V_0;
		String_t* L_6 = ___localFileName0;
		NullCheck(L_5);
		L_5->set_U3CU24U3ElocalFileName_10(L_6);
		U3CUploadFileCoU3Ec__Iterator3_t1598057078 * L_7 = V_0;
		String_t* L_8 = ___uploadURL1;
		NullCheck(L_7);
		L_7->set_U3CU24U3EuploadURL_11(L_8);
		U3CUploadFileCoU3Ec__Iterator3_t1598057078 * L_9 = V_0;
		NullCheck(L_9);
		L_9->set_U3CU3Ef__this_12(__this);
		U3CUploadFileCoU3Ec__Iterator3_t1598057078 * L_10 = V_0;
		return L_10;
	}
}
// System.Void AlbumPathCheck::UploadFile(System.String,System.String)
extern "C"  void AlbumPathCheck_UploadFile_m3385188612 (AlbumPathCheck_t1505991252 * __this, String_t* ___localFileName0, String_t* ___uploadURL1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___localFileName0;
		String_t* L_1 = ___uploadURL1;
		Il2CppObject * L_2 = AlbumPathCheck_UploadFileCo_m1802983664(__this, L_0, L_1, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator AlbumPathCheck::CheckProgress(UnityEngine.WWW)
extern Il2CppClass* U3CCheckProgressU3Ec__Iterator4_t1868153709_il2cpp_TypeInfo_var;
extern const uint32_t AlbumPathCheck_CheckProgress_m2259070848_MetadataUsageId;
extern "C"  Il2CppObject * AlbumPathCheck_CheckProgress_m2259070848 (AlbumPathCheck_t1505991252 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AlbumPathCheck_CheckProgress_m2259070848_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CCheckProgressU3Ec__Iterator4_t1868153709 * V_0 = NULL;
	{
		U3CCheckProgressU3Ec__Iterator4_t1868153709 * L_0 = (U3CCheckProgressU3Ec__Iterator4_t1868153709 *)il2cpp_codegen_object_new(U3CCheckProgressU3Ec__Iterator4_t1868153709_il2cpp_TypeInfo_var);
		U3CCheckProgressU3Ec__Iterator4__ctor_m3640393678(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCheckProgressU3Ec__Iterator4_t1868153709 * L_1 = V_0;
		WWW_t3134621005 * L_2 = ___www0;
		NullCheck(L_1);
		L_1->set_www_0(L_2);
		U3CCheckProgressU3Ec__Iterator4_t1868153709 * L_3 = V_0;
		WWW_t3134621005 * L_4 = ___www0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Ewww_3(L_4);
		U3CCheckProgressU3Ec__Iterator4_t1868153709 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_4(__this);
		U3CCheckProgressU3Ec__Iterator4_t1868153709 * L_6 = V_0;
		return L_6;
	}
}
// System.Void AlbumPathCheck::addressActive()
extern const MethodInfo* Component_GetComponentInChildren_TisDropdown_t4201779933_m3166666881_MethodInfo_var;
extern const uint32_t AlbumPathCheck_addressActive_m2345188255_MetadataUsageId;
extern "C"  void AlbumPathCheck_addressActive_m2345188255 (AlbumPathCheck_t1505991252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AlbumPathCheck_addressActive_m2345188255_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_buttonspanel_17();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_1 = __this->get_addresspanel_16();
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, (bool)1, /*hidden argument*/NULL);
		Dropdown_t4201779933 * L_2 = Component_GetComponentInChildren_TisDropdown_t4201779933_m3166666881(__this, /*hidden argument*/Component_GetComponentInChildren_TisDropdown_t4201779933_m3166666881_MethodInfo_var);
		NullCheck(L_2);
		Dropdown_set_value_m2635023165(L_2, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AlbumPathCheck::mapAtive()
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern Il2CppCodeGenString* _stringLiteral3692149211;
extern Il2CppCodeGenString* _stringLiteral4038742408;
extern const uint32_t AlbumPathCheck_mapAtive_m2160111366_MetadataUsageId;
extern "C"  void AlbumPathCheck_mapAtive_m2160111366 (AlbumPathCheck_t1505991252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AlbumPathCheck_mapAtive_m2160111366_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = ((int32_t)3);
		Il2CppObject * L_2 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		int32_t L_4 = ((int32_t)2);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_3, _stringLiteral3660319847, L_6, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral3692149211, _stringLiteral4038742408, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_PopupMessage_m2237488056(NULL /*static, unused*/, ((int32_t)10), /*hidden argument*/NULL);
		return;
	}
}
// System.Void AlbumPathCheck::mapClickGEO(System.String)
extern Il2CppCodeGenString* _stringLiteral47;
extern const uint32_t AlbumPathCheck_mapClickGEO_m1310738264_MetadataUsageId;
extern "C"  void AlbumPathCheck_mapClickGEO_m1310738264 (AlbumPathCheck_t1505991252 * __this, String_t* ___geo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AlbumPathCheck_mapClickGEO_m1310738264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___geo0;
		String_t* L_1 = ___geo0;
		NullCheck(L_1);
		int32_t L_2 = String_IndexOf_m1476794331(L_1, _stringLiteral47, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_3 = String_Substring_m675079568(L_0, 0, L_2, /*hidden argument*/NULL);
		__this->set_lat_13(L_3);
		String_t* L_4 = ___geo0;
		String_t* L_5 = ___geo0;
		NullCheck(L_5);
		int32_t L_6 = String_IndexOf_m1476794331(L_5, _stringLiteral47, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_7 = String_Substring_m2809233063(L_4, ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
		__this->set_lon_14(L_7);
		return;
	}
}
// System.Void AlbumPathCheck::mapLat(System.String)
extern "C"  void AlbumPathCheck_mapLat_m1375933602 (AlbumPathCheck_t1505991252 * __this, String_t* ___num0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___num0;
		__this->set_lat_13(L_0);
		return;
	}
}
// System.Void AlbumPathCheck::mapLon(System.String)
extern "C"  void AlbumPathCheck_mapLon_m1910637942 (AlbumPathCheck_t1505991252 * __this, String_t* ___num0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___num0;
		__this->set_lon_14(L_0);
		return;
	}
}
// System.Void AlbumPathCheck::OnEnable()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t AlbumPathCheck_OnEnable_m544320863_MetadataUsageId;
extern "C"  void AlbumPathCheck_OnEnable_m544320863 (AlbumPathCheck_t1505991252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AlbumPathCheck_OnEnable_m544320863_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_0 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		LocationService_Start_m1836061721(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AlbumPathCheck/<CheckProgress>c__Iterator4::.ctor()
extern "C"  void U3CCheckProgressU3Ec__Iterator4__ctor_m3640393678 (U3CCheckProgressU3Ec__Iterator4_t1868153709 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object AlbumPathCheck/<CheckProgress>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckProgressU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2241278094 (U3CCheckProgressU3Ec__Iterator4_t1868153709 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object AlbumPathCheck/<CheckProgress>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckProgressU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m3660058658 (U3CCheckProgressU3Ec__Iterator4_t1868153709 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean AlbumPathCheck/<CheckProgress>c__Iterator4::MoveNext()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentInChildren_TisTMP_Text_t980027659_m2592787986_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral37;
extern Il2CppCodeGenString* _stringLiteral2135970;
extern const uint32_t U3CCheckProgressU3Ec__Iterator4_MoveNext_m1977440846_MetadataUsageId;
extern "C"  bool U3CCheckProgressU3Ec__Iterator4_MoveNext_m1977440846 (U3CCheckProgressU3Ec__Iterator4_t1868153709 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckProgressU3Ec__Iterator4_MoveNext_m1977440846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_00af;
		}
		if (L_1 == 2)
		{
			goto IL_011a;
		}
	}
	{
		goto IL_0132;
	}

IL_0025:
	{
		AlbumPathCheck_t1505991252 * L_2 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = L_2->get_ProgressBar_6();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)1, /*hidden argument*/NULL);
		goto IL_00af;
	}

IL_003b:
	{
		AlbumPathCheck_t1505991252 * L_4 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = L_4->get_ProgressBar_6();
		NullCheck(L_5);
		TMP_Text_t980027659 * L_6 = GameObject_GetComponentInChildren_TisTMP_Text_t980027659_m2592787986(L_5, /*hidden argument*/GameObject_GetComponentInChildren_TisTMP_Text_t980027659_m2592787986_MethodInfo_var);
		WWW_t3134621005 * L_7 = __this->get_www_0();
		NullCheck(L_7);
		float L_8 = WWW_get_progress_m3186358572(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_9 = bankers_roundf(((float)((float)L_8*(float)(100.0f))));
		V_1 = L_9;
		String_t* L_10 = Single_ToString_m5736032((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m138640077(NULL /*static, unused*/, L_10, _stringLiteral37, /*hidden argument*/NULL);
		NullCheck(L_6);
		TMP_Text_set_text_m27929696(L_6, L_11, /*hidden argument*/NULL);
		AlbumPathCheck_t1505991252 * L_12 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_12);
		Image_t538875265 * L_13 = L_12->get_image_7();
		WWW_t3134621005 * L_14 = __this->get_www_0();
		NullCheck(L_14);
		float L_15 = WWW_get_progress_m3186358572(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Image_set_fillAmount_m1583793743(L_13, L_15, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_16 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_16, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_16);
		__this->set_U24PC_1(1);
		goto IL_0134;
	}

IL_00af:
	{
		WWW_t3134621005 * L_17 = __this->get_www_0();
		NullCheck(L_17);
		bool L_18 = WWW_get_isDone_m634060017(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_003b;
		}
	}
	{
		WWW_t3134621005 * L_19 = __this->get_www_0();
		NullCheck(L_19);
		bool L_20 = WWW_get_isDone_m634060017(L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_012b;
		}
	}
	{
		WWW_t3134621005 * L_21 = __this->get_www_0();
		NullCheck(L_21);
		float L_22 = WWW_get_uploadProgress_m1090826125(L_21, /*hidden argument*/NULL);
		if ((!(((float)L_22) >= ((float)(1.0f)))))
		{
			goto IL_012b;
		}
	}
	{
		AlbumPathCheck_t1505991252 * L_23 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_23);
		GameObject_t3674682005 * L_24 = L_23->get_ProgressBar_6();
		NullCheck(L_24);
		TMP_Text_t980027659 * L_25 = GameObject_GetComponentInChildren_TisTMP_Text_t980027659_m2592787986(L_24, /*hidden argument*/GameObject_GetComponentInChildren_TisTMP_Text_t980027659_m2592787986_MethodInfo_var);
		NullCheck(L_25);
		TMP_Text_set_text_m27929696(L_25, _stringLiteral2135970, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_26 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_26, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_26);
		__this->set_U24PC_1(2);
		goto IL_0134;
	}

IL_011a:
	{
		AlbumPathCheck_t1505991252 * L_27 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_27);
		GameObject_t3674682005 * L_28 = L_27->get_ProgressBar_6();
		NullCheck(L_28);
		GameObject_SetActive_m3538205401(L_28, (bool)0, /*hidden argument*/NULL);
	}

IL_012b:
	{
		__this->set_U24PC_1((-1));
	}

IL_0132:
	{
		return (bool)0;
	}

IL_0134:
	{
		return (bool)1;
	}
	// Dead block : IL_0136: ldloc.2
}
// System.Void AlbumPathCheck/<CheckProgress>c__Iterator4::Dispose()
extern "C"  void U3CCheckProgressU3Ec__Iterator4_Dispose_m2213228683 (U3CCheckProgressU3Ec__Iterator4_t1868153709 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void AlbumPathCheck/<CheckProgress>c__Iterator4::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckProgressU3Ec__Iterator4_Reset_m1286826619_MetadataUsageId;
extern "C"  void U3CCheckProgressU3Ec__Iterator4_Reset_m1286826619 (U3CCheckProgressU3Ec__Iterator4_t1868153709 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckProgressU3Ec__Iterator4_Reset_m1286826619_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void AlbumPathCheck/<Send>c__Iterator2::.ctor()
extern "C"  void U3CSendU3Ec__Iterator2__ctor_m740422005 (U3CSendU3Ec__Iterator2_t611604662 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object AlbumPathCheck/<Send>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3084669501 (U3CSendU3Ec__Iterator2_t611604662 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_7();
		return L_0;
	}
}
// System.Object AlbumPathCheck/<Send>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3874254801 (U3CSendU3Ec__Iterator2_t611604662 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_7();
		return L_0;
	}
}
// System.Boolean AlbumPathCheck/<Send>c__Iterator2::MoveNext()
extern Il2CppClass* WWWForm_t461342257_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2522905884;
extern Il2CppCodeGenString* _stringLiteral110371416;
extern Il2CppCodeGenString* _stringLiteral3575610;
extern Il2CppCodeGenString* _stringLiteral3402485746;
extern Il2CppCodeGenString* _stringLiteral473771429;
extern Il2CppCodeGenString* _stringLiteral3449379;
extern Il2CppCodeGenString* _stringLiteral2872469938;
extern Il2CppCodeGenString* _stringLiteral2872469939;
extern Il2CppCodeGenString* _stringLiteral2854988908;
extern Il2CppCodeGenString* _stringLiteral137365935;
extern Il2CppCodeGenString* _stringLiteral940536989;
extern Il2CppCodeGenString* _stringLiteral4137715065;
extern Il2CppCodeGenString* _stringLiteral3386855173;
extern Il2CppCodeGenString* _stringLiteral1979684883;
extern Il2CppCodeGenString* _stringLiteral940549841;
extern Il2CppCodeGenString* _stringLiteral349328388;
extern Il2CppCodeGenString* _stringLiteral3387253585;
extern const uint32_t U3CSendU3Ec__Iterator2_MoveNext_m2627385695_MetadataUsageId;
extern "C"  bool U3CSendU3Ec__Iterator2_MoveNext_m2627385695 (U3CSendU3Ec__Iterator2_t611604662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSendU3Ec__Iterator2_MoveNext_m2627385695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_017e;
		}
	}
	{
		goto IL_0212;
	}

IL_0021:
	{
		WWWForm_t461342257 * L_2 = (WWWForm_t461342257 *)il2cpp_codegen_object_new(WWWForm_t461342257_il2cpp_TypeInfo_var);
		WWWForm__ctor_m1417930174(L_2, /*hidden argument*/NULL);
		__this->set_U3CformU3E__0_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_3 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0042;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_4 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_005c;
		}
	}

IL_0042:
	{
		WWWForm_t461342257 * L_5 = __this->get_U3CformU3E__0_0();
		String_t* L_6 = PrivateURL_get_customerId_m4251176570(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		WWWForm_AddField_m2890504319(L_5, _stringLiteral2522905884, L_6, /*hidden argument*/NULL);
		goto IL_007b;
	}

IL_005c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_7 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_007b;
		}
	}
	{
		WWWForm_t461342257 * L_8 = __this->get_U3CformU3E__0_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_8);
		WWWForm_AddField_m2890504319(L_8, _stringLiteral2522905884, L_9, /*hidden argument*/NULL);
	}

IL_007b:
	{
		WWWForm_t461342257 * L_10 = __this->get_U3CformU3E__0_0();
		String_t* L_11 = __this->get_title_1();
		NullCheck(L_10);
		WWWForm_AddField_m2890504319(L_10, _stringLiteral110371416, L_11, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_12 = __this->get_U3CformU3E__0_0();
		AlbumPathCheck_t1505991252 * L_13 = __this->get_U3CU3Ef__this_12();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_FileType_15();
		NullCheck(L_12);
		WWWForm_AddField_m355604532(L_12, _stringLiteral3575610, L_14, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_15 = __this->get_U3CformU3E__0_0();
		NullCheck(L_15);
		WWWForm_AddField_m355604532(L_15, _stringLiteral3402485746, 1, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_16 = __this->get_U3CformU3E__0_0();
		String_t* L_17 = __this->get_filename_2();
		NullCheck(L_16);
		WWWForm_AddField_m2890504319(L_16, _stringLiteral473771429, L_17, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_18 = __this->get_U3CformU3E__0_0();
		AlbumPathCheck_t1505991252 * L_19 = __this->get_U3CU3Ef__this_12();
		NullCheck(L_19);
		int32_t L_20 = L_19->get__pref_12();
		NullCheck(L_18);
		WWWForm_AddField_m355604532(L_18, _stringLiteral3449379, L_20, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_21 = __this->get_U3CformU3E__0_0();
		String_t* L_22 = __this->get_addr01_3();
		NullCheck(L_21);
		WWWForm_AddField_m2890504319(L_21, _stringLiteral2872469938, L_22, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_23 = __this->get_U3CformU3E__0_0();
		String_t* L_24 = __this->get_addr02_4();
		NullCheck(L_23);
		WWWForm_AddField_m2890504319(L_23, _stringLiteral2872469939, L_24, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_25 = __this->get_U3CformU3E__0_0();
		AlbumPathCheck_t1505991252 * L_26 = __this->get_U3CU3Ef__this_12();
		NullCheck(L_26);
		String_t* L_27 = L_26->get_lat_13();
		NullCheck(L_25);
		WWWForm_AddField_m2890504319(L_25, _stringLiteral2854988908, L_27, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_28 = __this->get_U3CformU3E__0_0();
		AlbumPathCheck_t1505991252 * L_29 = __this->get_U3CU3Ef__this_12();
		NullCheck(L_29);
		String_t* L_30 = L_29->get_lon_14();
		NullCheck(L_28);
		WWWForm_AddField_m2890504319(L_28, _stringLiteral137365935, L_30, /*hidden argument*/NULL);
		String_t* L_31 = PrivateURL_get_RegistrationMessage_m4175169615(NULL /*static, unused*/, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_32 = __this->get_U3CformU3E__0_0();
		WWW_t3134621005 * L_33 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m3203953640(L_33, L_31, L_32, /*hidden argument*/NULL);
		__this->set_U3CwU3E__1_5(L_33);
		WWW_t3134621005 * L_34 = __this->get_U3CwU3E__1_5();
		__this->set_U24current_7(L_34);
		__this->set_U24PC_6(1);
		goto IL_0214;
	}

IL_017e:
	{
		WWW_t3134621005 * L_35 = __this->get_U3CwU3E__1_5();
		NullCheck(L_35);
		String_t* L_36 = WWW_get_error_m1787423074(L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_01d2;
		}
	}
	{
		WWW_t3134621005 * L_37 = __this->get_U3CwU3E__1_5();
		NullCheck(L_37);
		String_t* L_38 = WWW_get_error_m1787423074(L_37, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		int32_t L_39 = ((int32_t)2);
		Il2CppObject * L_40 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_39);
		NullCheck((Enum_t2862688501 *)L_40);
		String_t* L_41 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_40);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, _stringLiteral940536989, _stringLiteral4137715065, L_41, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral3386855173, _stringLiteral4137715065, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_PopupMessage_m2237488056(NULL /*static, unused*/, 6, /*hidden argument*/NULL);
		goto IL_020b;
	}

IL_01d2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1979684883, /*hidden argument*/NULL);
		int32_t L_42 = ((int32_t)2);
		Il2CppObject * L_43 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_42);
		NullCheck((Enum_t2862688501 *)L_43);
		String_t* L_44 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_43);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, _stringLiteral940549841, _stringLiteral349328388, L_44, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral3387253585, _stringLiteral1979684883, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_PopupMessage_m2237488056(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
	}

IL_020b:
	{
		__this->set_U24PC_6((-1));
	}

IL_0212:
	{
		return (bool)0;
	}

IL_0214:
	{
		return (bool)1;
	}
	// Dead block : IL_0216: ldloc.1
}
// System.Void AlbumPathCheck/<Send>c__Iterator2::Dispose()
extern "C"  void U3CSendU3Ec__Iterator2_Dispose_m2774226034 (U3CSendU3Ec__Iterator2_t611604662 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void AlbumPathCheck/<Send>c__Iterator2::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CSendU3Ec__Iterator2_Reset_m2681822242_MetadataUsageId;
extern "C"  void U3CSendU3Ec__Iterator2_Reset_m2681822242 (U3CSendU3Ec__Iterator2_t611604662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSendU3Ec__Iterator2_Reset_m2681822242_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void AlbumPathCheck/<UploadFileCo>c__Iterator3::.ctor()
extern "C"  void U3CUploadFileCoU3Ec__Iterator3__ctor_m2396942261 (U3CUploadFileCoU3Ec__Iterator3_t1598057078 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object AlbumPathCheck/<UploadFileCo>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CUploadFileCoU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m39084285 (U3CUploadFileCoU3Ec__Iterator3_t1598057078 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_9();
		return L_0;
	}
}
// System.Object AlbumPathCheck/<UploadFileCo>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CUploadFileCoU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m4074372753 (U3CUploadFileCoU3Ec__Iterator3_t1598057078 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_9();
		return L_0;
	}
}
// System.Boolean AlbumPathCheck/<UploadFileCo>c__Iterator3::MoveNext()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* FileInfo_t3233670074_il2cpp_TypeInfo_var;
extern Il2CppClass* WWWForm_t461342257_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3439929502;
extern Il2CppCodeGenString* _stringLiteral2206310032;
extern Il2CppCodeGenString* _stringLiteral973717120;
extern Il2CppCodeGenString* _stringLiteral3273937060;
extern Il2CppCodeGenString* _stringLiteral3688398002;
extern Il2CppCodeGenString* _stringLiteral2707813342;
extern Il2CppCodeGenString* _stringLiteral3753967799;
extern Il2CppCodeGenString* _stringLiteral2745405138;
extern Il2CppCodeGenString* _stringLiteral408897067;
extern Il2CppCodeGenString* _stringLiteral2522905884;
extern Il2CppCodeGenString* _stringLiteral110371416;
extern Il2CppCodeGenString* _stringLiteral2602996;
extern Il2CppCodeGenString* _stringLiteral3575610;
extern Il2CppCodeGenString* _stringLiteral3402485746;
extern Il2CppCodeGenString* _stringLiteral473771429;
extern Il2CppCodeGenString* _stringLiteral3449379;
extern Il2CppCodeGenString* _stringLiteral2872469938;
extern Il2CppCodeGenString* _stringLiteral2872469939;
extern Il2CppCodeGenString* _stringLiteral2854988908;
extern Il2CppCodeGenString* _stringLiteral47602;
extern Il2CppCodeGenString* _stringLiteral137365935;
extern Il2CppCodeGenString* _stringLiteral1427427650;
extern Il2CppCodeGenString* _stringLiteral1300438777;
extern Il2CppCodeGenString* _stringLiteral1006345793;
extern Il2CppCodeGenString* _stringLiteral1445788078;
extern Il2CppCodeGenString* _stringLiteral969084166;
extern Il2CppCodeGenString* _stringLiteral1476190065;
extern Il2CppCodeGenString* _stringLiteral1476190066;
extern Il2CppCodeGenString* _stringLiteral2398343773;
extern Il2CppCodeGenString* _stringLiteral1476190067;
extern const uint32_t U3CUploadFileCoU3Ec__Iterator3_MoveNext_m2820015903_MetadataUsageId;
extern "C"  bool U3CUploadFileCoU3Ec__Iterator3_MoveNext_m2820015903 (U3CUploadFileCoU3Ec__Iterator3_t1598057078 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CUploadFileCoU3Ec__Iterator3_MoveNext_m2820015903_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_8();
		V_0 = L_0;
		__this->set_U24PC_8((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0058;
		}
		if (L_1 == 2)
		{
			goto IL_02c7;
		}
	}
	{
		goto IL_049b;
	}

IL_0025:
	{
		String_t* L_2 = __this->get_localFileName_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3439929502, L_2, /*hidden argument*/NULL);
		WWW_t3134621005 * L_4 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m1985874080(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U3ClocalFileU3E__0_1(L_4);
		WWW_t3134621005 * L_5 = __this->get_U3ClocalFileU3E__0_1();
		__this->set_U24current_9(L_5);
		__this->set_U24PC_8(1);
		goto IL_049d;
	}

IL_0058:
	{
		WWW_t3134621005 * L_6 = __this->get_U3ClocalFileU3E__0_1();
		NullCheck(L_6);
		String_t* L_7 = WWW_get_error_m1787423074(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0077;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2206310032, /*hidden argument*/NULL);
		goto IL_00c5;
	}

IL_0077:
	{
		WWW_t3134621005 * L_8 = __this->get_U3ClocalFileU3E__0_1();
		NullCheck(L_8);
		String_t* L_9 = WWW_get_error_m1787423074(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral973717120, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_11 = ((int32_t)2);
		Il2CppObject * L_12 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_11);
		NullCheck((Enum_t2862688501 *)L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_12);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, _stringLiteral3273937060, _stringLiteral3688398002, L_13, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral2707813342, _stringLiteral3688398002, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_PopupMessage_m2237488056(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		goto IL_049b;
	}

IL_00c5:
	{
		String_t* L_14 = __this->get_localFileName_0();
		FileInfo_t3233670074 * L_15 = (FileInfo_t3233670074 *)il2cpp_codegen_object_new(FileInfo_t3233670074_il2cpp_TypeInfo_var);
		FileInfo__ctor_m2163658659(L_15, L_14, /*hidden argument*/NULL);
		__this->set_U3CfileinfoU3E__1_2(L_15);
		FileInfo_t3233670074 * L_16 = __this->get_U3CfileinfoU3E__1_2();
		NullCheck(L_16);
		int64_t L_17 = FileInfo_get_Length_m1498099825(L_16, /*hidden argument*/NULL);
		__this->set_U3CbyteCountU3E__2_3((((double)((double)L_17))));
		double L_18 = __this->get_U3CbyteCountU3E__2_3();
		if ((!(((double)L_18) >= ((double)(104857600.0)))))
		{
			goto IL_0135;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_19 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_19) == ((int32_t)2)))
		{
			goto IL_0135;
		}
	}
	{
		int32_t L_20 = ((int32_t)2);
		Il2CppObject * L_21 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_20);
		NullCheck((Enum_t2862688501 *)L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_21);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, _stringLiteral3753967799, _stringLiteral2745405138, L_22, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral408897067, _stringLiteral2745405138, /*hidden argument*/NULL);
		goto IL_049b;
	}

IL_0135:
	{
		WWWForm_t461342257 * L_23 = (WWWForm_t461342257 *)il2cpp_codegen_object_new(WWWForm_t461342257_il2cpp_TypeInfo_var);
		WWWForm__ctor_m1417930174(L_23, /*hidden argument*/NULL);
		__this->set_U3CformU3E__3_4(L_23);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_24 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_24) == ((int32_t)1)))
		{
			goto IL_0156;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_25 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)2))))
		{
			goto IL_0170;
		}
	}

IL_0156:
	{
		WWWForm_t461342257 * L_26 = __this->get_U3CformU3E__3_4();
		String_t* L_27 = PrivateURL_get_customerId_m4251176570(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_26);
		WWWForm_AddField_m2890504319(L_26, _stringLiteral2522905884, L_27, /*hidden argument*/NULL);
		goto IL_018f;
	}

IL_0170:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_28 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_018f;
		}
	}
	{
		WWWForm_t461342257 * L_29 = __this->get_U3CformU3E__3_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_29);
		WWWForm_AddField_m2890504319(L_29, _stringLiteral2522905884, L_30, /*hidden argument*/NULL);
	}

IL_018f:
	{
		WWWForm_t461342257 * L_31 = __this->get_U3CformU3E__3_4();
		NullCheck(L_31);
		WWWForm_AddField_m2890504319(L_31, _stringLiteral110371416, _stringLiteral2602996, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_32 = __this->get_U3CformU3E__3_4();
		NullCheck(L_32);
		WWWForm_AddField_m355604532(L_32, _stringLiteral3575610, 2, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_33 = __this->get_U3CformU3E__3_4();
		NullCheck(L_33);
		WWWForm_AddField_m355604532(L_33, _stringLiteral3402485746, 1, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_34 = __this->get_U3CformU3E__3_4();
		NullCheck(L_34);
		WWWForm_AddField_m2890504319(L_34, _stringLiteral473771429, _stringLiteral2602996, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_35 = __this->get_U3CformU3E__3_4();
		NullCheck(L_35);
		WWWForm_AddField_m355604532(L_35, _stringLiteral3449379, 0, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_36 = __this->get_U3CformU3E__3_4();
		NullCheck(L_36);
		WWWForm_AddField_m2890504319(L_36, _stringLiteral2872469938, _stringLiteral2602996, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_37 = __this->get_U3CformU3E__3_4();
		NullCheck(L_37);
		WWWForm_AddField_m2890504319(L_37, _stringLiteral2872469939, _stringLiteral2602996, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_38 = __this->get_U3CformU3E__3_4();
		NullCheck(L_38);
		WWWForm_AddField_m2890504319(L_38, _stringLiteral2854988908, _stringLiteral47602, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_39 = __this->get_U3CformU3E__3_4();
		NullCheck(L_39);
		WWWForm_AddField_m2890504319(L_39, _stringLiteral137365935, _stringLiteral47602, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_40 = __this->get_U3CformU3E__3_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_40);
		WWWForm_AddField_m2890504319(L_40, _stringLiteral1427427650, L_41, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_42 = __this->get_U3CformU3E__3_4();
		WWW_t3134621005 * L_43 = __this->get_U3ClocalFileU3E__0_1();
		NullCheck(L_43);
		ByteU5BU5D_t4260760469* L_44 = WWW_get_bytes_m2080623436(L_43, /*hidden argument*/NULL);
		AlbumPathCheck_t1505991252 * L_45 = __this->get_U3CU3Ef__this_12();
		NullCheck(L_45);
		String_t* L_46 = L_45->get_tempfilename_11();
		NullCheck(L_42);
		WWWForm_AddBinaryData_m3460911449(L_42, _stringLiteral1300438777, L_44, L_46, /*hidden argument*/NULL);
		String_t* L_47 = __this->get_uploadURL_5();
		WWWForm_t461342257 * L_48 = __this->get_U3CformU3E__3_4();
		WWW_t3134621005 * L_49 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m3203953640(L_49, L_47, L_48, /*hidden argument*/NULL);
		__this->set_U3CuploadU3E__4_6(L_49);
		AlbumPathCheck_t1505991252 * L_50 = __this->get_U3CU3Ef__this_12();
		AlbumPathCheck_t1505991252 * L_51 = __this->get_U3CU3Ef__this_12();
		WWW_t3134621005 * L_52 = __this->get_U3CuploadU3E__4_6();
		NullCheck(L_51);
		Il2CppObject * L_53 = AlbumPathCheck_CheckProgress_m2259070848(L_51, L_52, /*hidden argument*/NULL);
		NullCheck(L_50);
		MonoBehaviour_StartCoroutine_m2135303124(L_50, L_53, /*hidden argument*/NULL);
		WWW_t3134621005 * L_54 = __this->get_U3CuploadU3E__4_6();
		__this->set_U24current_9(L_54);
		__this->set_U24PC_8(2);
		goto IL_049d;
	}

IL_02c7:
	{
		WWW_t3134621005 * L_55 = __this->get_U3CuploadU3E__4_6();
		NullCheck(L_55);
		String_t* L_56 = WWW_get_error_m1787423074(L_55, /*hidden argument*/NULL);
		if (L_56)
		{
			goto IL_02e6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1006345793, /*hidden argument*/NULL);
		goto IL_032f;
	}

IL_02e6:
	{
		WWW_t3134621005 * L_57 = __this->get_U3CuploadU3E__4_6();
		NullCheck(L_57);
		String_t* L_58 = WWW_get_error_m1787423074(L_57, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_59 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1445788078, L_58, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		int32_t L_60 = ((int32_t)2);
		Il2CppObject * L_61 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_60);
		NullCheck((Enum_t2862688501 *)L_61);
		String_t* L_62 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_61);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, _stringLiteral3273937060, _stringLiteral969084166, L_62, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral2707813342, _stringLiteral1476190065, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_PopupMessage_m2237488056(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
	}

IL_032f:
	{
		WWW_t3134621005 * L_63 = __this->get_U3CuploadU3E__4_6();
		NullCheck(L_63);
		String_t* L_64 = WWW_get_text_m4216049525(L_63, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		JsonData_t1715015430 * L_65 = JsonMapper_ToObject_m3425498490(NULL /*static, unused*/, L_64, /*hidden argument*/NULL);
		__this->set_U3CEventJSU3E__5_7(L_65);
		JsonData_t1715015430 * L_66 = __this->get_U3CEventJSU3E__5_7();
		NullCheck(L_66);
		JsonData_t1715015430 * L_67 = JsonData_get_Item_m253158020(L_66, 1, /*hidden argument*/NULL);
		NullCheck(L_67);
		JsonData_t1715015430 * L_68 = JsonData_get_Item_m253158020(L_67, 0, /*hidden argument*/NULL);
		NullCheck(L_68);
		JsonData_t1715015430 * L_69 = JsonData_get_Item_m253158020(L_68, 0, /*hidden argument*/NULL);
		NullCheck(L_69);
		String_t* L_70 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_69);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t476798718_il2cpp_TypeInfo_var);
		bool L_71 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_70, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_044b;
		}
	}
	{
		AlbumPathCheck_t1505991252 * L_72 = __this->get_U3CU3Ef__this_12();
		JsonData_t1715015430 * L_73 = __this->get_U3CEventJSU3E__5_7();
		NullCheck(L_73);
		JsonData_t1715015430 * L_74 = JsonData_get_Item_m253158020(L_73, 1, /*hidden argument*/NULL);
		NullCheck(L_74);
		JsonData_t1715015430 * L_75 = JsonData_get_Item_m253158020(L_74, 1, /*hidden argument*/NULL);
		NullCheck(L_75);
		JsonData_t1715015430 * L_76 = JsonData_get_Item_m253158020(L_75, 6, /*hidden argument*/NULL);
		NullCheck(L_76);
		String_t* L_77 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_76);
		NullCheck(L_72);
		L_72->set_realfilename_10(L_77);
		AlbumPathCheck_t1505991252 * L_78 = __this->get_U3CU3Ef__this_12();
		NullCheck(L_78);
		String_t* L_79 = L_78->get_realfilename_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_80 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_79, /*hidden argument*/NULL);
		if (!L_80)
		{
			goto IL_03f7;
		}
	}
	{
		AlbumPathCheck_t1505991252 * L_81 = __this->get_U3CU3Ef__this_12();
		NullCheck(L_81);
		TextMeshProUGUI_t3603375195 * L_82 = L_81->get_m_Path_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_83 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_82);
		TMP_Text_set_text_m27929696(L_82, L_83, /*hidden argument*/NULL);
		int32_t L_84 = ((int32_t)2);
		Il2CppObject * L_85 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_84);
		NullCheck((Enum_t2862688501 *)L_85);
		String_t* L_86 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_85);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, _stringLiteral3273937060, _stringLiteral969084166, L_86, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral2707813342, _stringLiteral1476190066, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_PopupMessage_m2237488056(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		GC_Collect_m1459080321(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0446;
	}

IL_03f7:
	{
		AlbumPathCheck_t1505991252 * L_87 = __this->get_U3CU3Ef__this_12();
		NullCheck(L_87);
		TextMeshProUGUI_t3603375195 * L_88 = L_87->get_m_Path_5();
		AlbumPathCheck_t1505991252 * L_89 = __this->get_U3CU3Ef__this_12();
		NullCheck(L_89);
		String_t* L_90 = L_89->get_realfilename_10();
		NullCheck(L_88);
		TMP_Text_set_text_m27929696(L_88, L_90, /*hidden argument*/NULL);
		int32_t L_91 = ((int32_t)2);
		Il2CppObject * L_92 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_91);
		NullCheck((Enum_t2862688501 *)L_92);
		String_t* L_93 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_92);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, _stringLiteral3273937060, _stringLiteral2398343773, L_93, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral3273937060, _stringLiteral2398343773, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_PopupMessage_m2237488056(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		GC_Collect_m1459080321(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0446:
	{
		goto IL_0494;
	}

IL_044b:
	{
		AlbumPathCheck_t1505991252 * L_94 = __this->get_U3CU3Ef__this_12();
		NullCheck(L_94);
		TextMeshProUGUI_t3603375195 * L_95 = L_94->get_m_Path_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_96 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_95);
		TMP_Text_set_text_m27929696(L_95, L_96, /*hidden argument*/NULL);
		int32_t L_97 = ((int32_t)2);
		Il2CppObject * L_98 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_97);
		NullCheck((Enum_t2862688501 *)L_98);
		String_t* L_99 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_98);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, _stringLiteral3273937060, _stringLiteral969084166, L_99, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral2707813342, _stringLiteral1476190067, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_PopupMessage_m2237488056(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		GC_Collect_m1459080321(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0494:
	{
		__this->set_U24PC_8((-1));
	}

IL_049b:
	{
		return (bool)0;
	}

IL_049d:
	{
		return (bool)1;
	}
	// Dead block : IL_049f: ldloc.1
}
// System.Void AlbumPathCheck/<UploadFileCo>c__Iterator3::Dispose()
extern "C"  void U3CUploadFileCoU3Ec__Iterator3_Dispose_m1257325234 (U3CUploadFileCoU3Ec__Iterator3_t1598057078 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_8((-1));
		return;
	}
}
// System.Void AlbumPathCheck/<UploadFileCo>c__Iterator3::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CUploadFileCoU3Ec__Iterator3_Reset_m43375202_MetadataUsageId;
extern "C"  void U3CUploadFileCoU3Ec__Iterator3_Reset_m43375202 (U3CUploadFileCoU3Ec__Iterator3_t1598057078 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CUploadFileCoU3Ec__Iterator3_Reset_m43375202_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void AndroidBackBtn::.ctor()
extern "C"  void AndroidBackBtn__ctor_m323584181 (AndroidBackBtn_t3275646630 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AndroidBackBtn::Update()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentScreen_t3026574117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1014148;
extern Il2CppCodeGenString* _stringLiteral3281246972;
extern const uint32_t AndroidBackBtn_Update_m3168037112_MetadataUsageId;
extern "C"  void AndroidBackBtn_Update_m3168037112 (AndroidBackBtn_t3275646630 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidBackBtn_Update_m3168037112_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)278), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0097;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0088;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_2 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_3 = ((int32_t)2);
		Il2CppObject * L_4 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_3);
		NullCheck((Enum_t2862688501 *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_4);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m297139618(NULL /*static, unused*/, 1, _stringLiteral1014148, _stringLiteral3281246972, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_PopupMessage_m2237488056(NULL /*static, unused*/, ((int32_t)14), /*hidden argument*/NULL);
	}

IL_004c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_6 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)50)))))
		{
			goto IL_0059;
		}
	}
	{
		return;
	}

IL_0059:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_7 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_0079;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_8 = CurrentState_get_LoginState_m3052003788(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)20)))))
		{
			goto IL_0079;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		return;
	}

IL_0079:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentScreen_t3026574117_il2cpp_TypeInfo_var);
		CurrentScreen_t3026574117 * L_9 = CurrentScreen_get_Instance_m3161187310(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		CurrentScreen_BackButton_m2788205463(L_9, /*hidden argument*/NULL);
		goto IL_0097;
	}

IL_0088:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_10 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)319), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0097;
		}
	}

IL_0097:
	{
		return;
	}
}
// System.Void AndroidWebview::.ctor()
extern "C"  void AndroidWebview__ctor_m1655737649 (AndroidWebview_t552345258 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AndroidWebview::.cctor()
extern "C"  void AndroidWebview__cctor_m3601130652 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// AndroidWebview AndroidWebview::get_Instance()
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4033864337;
extern const uint32_t AndroidWebview_get_Instance_m3239679012_MetadataUsageId;
extern "C"  AndroidWebview_t552345258 * AndroidWebview_get_Instance_m3239679012 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidWebview_get_Instance_m3239679012_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_0 = ((AndroidWebview_t552345258_StaticFields*)AndroidWebview_t552345258_il2cpp_TypeInfo_var->static_fields)->get__instance_3();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral4033864337, /*hidden argument*/NULL);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_2 = ((AndroidWebview_t552345258_StaticFields*)AndroidWebview_t552345258_il2cpp_TypeInfo_var->static_fields)->get__instance_3();
		return L_2;
	}
}
// System.Void AndroidWebview::Awake()
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var;
extern const MethodInfo* AndroidJavaObject_GetStatic_TisAndroidJavaObject_t2362096582_m524545334_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2952365502;
extern Il2CppCodeGenString* _stringLiteral597527464;
extern const uint32_t AndroidWebview_Awake_m1893342868_MetadataUsageId;
extern "C"  void AndroidWebview_Awake_m1893342868 (AndroidWebview_t552345258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidWebview_Awake_m1893342868_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AndroidJavaClass_t1816259147 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		((AndroidWebview_t552345258_StaticFields*)AndroidWebview_t552345258_il2cpp_TypeInfo_var->static_fields)->set__instance_3(__this);
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_002e;
		}
	}
	{
		AndroidJavaClass_t1816259147 * L_1 = (AndroidJavaClass_t1816259147 *)il2cpp_codegen_object_new(AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m2757518396(L_1, _stringLiteral2952365502, /*hidden argument*/NULL);
		V_0 = L_1;
		AndroidJavaClass_t1816259147 * L_2 = V_0;
		NullCheck(L_2);
		AndroidJavaObject_t2362096582 * L_3 = AndroidJavaObject_GetStatic_TisAndroidJavaObject_t2362096582_m524545334(L_2, _stringLiteral597527464, /*hidden argument*/AndroidJavaObject_GetStatic_TisAndroidJavaObject_t2362096582_m524545334_MethodInfo_var);
		__this->set__activity_2(L_3);
	}

IL_002e:
	{
		return;
	}
}
// System.Void AndroidWebview::Call(System.String,System.String)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t AndroidWebview_Call_m335597645_MetadataUsageId;
extern "C"  void AndroidWebview_Call_m335597645 (AndroidWebview_t552345258 * __this, String_t* ___androidmethod0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidWebview_Call_m335597645_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0022;
		}
	}
	{
		AndroidJavaObject_t2362096582 * L_1 = __this->get__activity_2();
		String_t* L_2 = ___androidmethod0;
		ObjectU5BU5D_t1108656482* L_3 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_4 = ___value1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck(L_1);
		AndroidJavaObject_Call_m3151096341(L_1, L_2, L_3, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void AndroidWebview::Call(System.String)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t AndroidWebview_Call_m1677830609_MetadataUsageId;
extern "C"  void AndroidWebview_Call_m1677830609 (AndroidWebview_t552345258 * __this, String_t* ___androidmethod0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidWebview_Call_m1677830609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001e;
		}
	}
	{
		AndroidJavaObject_t2362096582 * L_1 = __this->get__activity_2();
		String_t* L_2 = ___androidmethod0;
		NullCheck(L_1);
		AndroidJavaObject_Call_m3151096341(L_1, L_2, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Void ApplicationChrome::.ctor()
extern "C"  void ApplicationChrome__ctor_m2477787361 (ApplicationChrome_t3150366666 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ApplicationChrome::.cctor()
extern Il2CppClass* ApplicationChrome_t3150366666_il2cpp_TypeInfo_var;
extern const uint32_t ApplicationChrome__cctor_m3314867948_MetadataUsageId;
extern "C"  void ApplicationChrome__cctor_m3314867948 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ApplicationChrome__cctor_m3314867948_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((ApplicationChrome_t3150366666_StaticFields*)ApplicationChrome_t3150366666_il2cpp_TypeInfo_var->static_fields)->set__statusBarColor_3(((int32_t)-16777216));
		((ApplicationChrome_t3150366666_StaticFields*)ApplicationChrome_t3150366666_il2cpp_TypeInfo_var->static_fields)->set__navigationBarColor_4(((int32_t)-16777216));
		ApplicationChrome_applyUIStates_m3276429891(NULL /*static, unused*/, /*hidden argument*/NULL);
		ApplicationChrome_applyUIColors_m1329407537(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ApplicationChrome::applyUIStates()
extern "C"  void ApplicationChrome_applyUIStates_m3276429891 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ApplicationChrome::applyUIColors()
extern "C"  void ApplicationChrome_applyUIColors_m1329407537 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// ApplicationChrome/States ApplicationChrome::get_navigationBarState()
extern Il2CppClass* ApplicationChrome_t3150366666_il2cpp_TypeInfo_var;
extern const uint32_t ApplicationChrome_get_navigationBarState_m2954615176_MetadataUsageId;
extern "C"  int32_t ApplicationChrome_get_navigationBarState_m2954615176 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ApplicationChrome_get_navigationBarState_m2954615176_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ApplicationChrome_t3150366666_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ApplicationChrome_t3150366666_StaticFields*)ApplicationChrome_t3150366666_il2cpp_TypeInfo_var->static_fields)->get__navigationBarState_2();
		return L_0;
	}
}
// System.Void ApplicationChrome::set_navigationBarState(ApplicationChrome/States)
extern Il2CppClass* ApplicationChrome_t3150366666_il2cpp_TypeInfo_var;
extern const uint32_t ApplicationChrome_set_navigationBarState_m1397346091_MetadataUsageId;
extern "C"  void ApplicationChrome_set_navigationBarState_m1397346091 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ApplicationChrome_set_navigationBarState_m1397346091_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ApplicationChrome_t3150366666_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ApplicationChrome_t3150366666_StaticFields*)ApplicationChrome_t3150366666_il2cpp_TypeInfo_var->static_fields)->get__navigationBarState_2();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ApplicationChrome_t3150366666_il2cpp_TypeInfo_var);
		((ApplicationChrome_t3150366666_StaticFields*)ApplicationChrome_t3150366666_il2cpp_TypeInfo_var->static_fields)->set__navigationBarState_2(L_2);
		ApplicationChrome_applyUIStates_m3276429891(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// ApplicationChrome/States ApplicationChrome::get_statusBarState()
extern Il2CppClass* ApplicationChrome_t3150366666_il2cpp_TypeInfo_var;
extern const uint32_t ApplicationChrome_get_statusBarState_m4269732966_MetadataUsageId;
extern "C"  int32_t ApplicationChrome_get_statusBarState_m4269732966 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ApplicationChrome_get_statusBarState_m4269732966_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ApplicationChrome_t3150366666_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ApplicationChrome_t3150366666_StaticFields*)ApplicationChrome_t3150366666_il2cpp_TypeInfo_var->static_fields)->get__statusBarState_1();
		return L_0;
	}
}
// System.Void ApplicationChrome::set_statusBarState(ApplicationChrome/States)
extern Il2CppClass* ApplicationChrome_t3150366666_il2cpp_TypeInfo_var;
extern const uint32_t ApplicationChrome_set_statusBarState_m3496748297_MetadataUsageId;
extern "C"  void ApplicationChrome_set_statusBarState_m3496748297 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ApplicationChrome_set_statusBarState_m3496748297_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ApplicationChrome_t3150366666_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ApplicationChrome_t3150366666_StaticFields*)ApplicationChrome_t3150366666_il2cpp_TypeInfo_var->static_fields)->get__statusBarState_1();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ApplicationChrome_t3150366666_il2cpp_TypeInfo_var);
		((ApplicationChrome_t3150366666_StaticFields*)ApplicationChrome_t3150366666_il2cpp_TypeInfo_var->static_fields)->set__statusBarState_1(L_2);
		ApplicationChrome_applyUIStates_m3276429891(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Boolean ApplicationChrome::get_dimmed()
extern Il2CppClass* ApplicationChrome_t3150366666_il2cpp_TypeInfo_var;
extern const uint32_t ApplicationChrome_get_dimmed_m317876092_MetadataUsageId;
extern "C"  bool ApplicationChrome_get_dimmed_m317876092 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ApplicationChrome_get_dimmed_m317876092_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ApplicationChrome_t3150366666_il2cpp_TypeInfo_var);
		bool L_0 = ((ApplicationChrome_t3150366666_StaticFields*)ApplicationChrome_t3150366666_il2cpp_TypeInfo_var->static_fields)->get__dimmed_7();
		return L_0;
	}
}
// System.Void ApplicationChrome::set_dimmed(System.Boolean)
extern Il2CppClass* ApplicationChrome_t3150366666_il2cpp_TypeInfo_var;
extern const uint32_t ApplicationChrome_set_dimmed_m476566427_MetadataUsageId;
extern "C"  void ApplicationChrome_set_dimmed_m476566427 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ApplicationChrome_set_dimmed_m476566427_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ApplicationChrome_t3150366666_il2cpp_TypeInfo_var);
		bool L_0 = ((ApplicationChrome_t3150366666_StaticFields*)ApplicationChrome_t3150366666_il2cpp_TypeInfo_var->static_fields)->get__dimmed_7();
		bool L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0016;
		}
	}
	{
		bool L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ApplicationChrome_t3150366666_il2cpp_TypeInfo_var);
		((ApplicationChrome_t3150366666_StaticFields*)ApplicationChrome_t3150366666_il2cpp_TypeInfo_var->static_fields)->set__dimmed_7(L_2);
		ApplicationChrome_applyUIStates_m3276429891(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.UInt32 ApplicationChrome::get_statusBarColor()
extern Il2CppClass* ApplicationChrome_t3150366666_il2cpp_TypeInfo_var;
extern const uint32_t ApplicationChrome_get_statusBarColor_m838480531_MetadataUsageId;
extern "C"  uint32_t ApplicationChrome_get_statusBarColor_m838480531 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ApplicationChrome_get_statusBarColor_m838480531_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ApplicationChrome_t3150366666_il2cpp_TypeInfo_var);
		uint32_t L_0 = ((ApplicationChrome_t3150366666_StaticFields*)ApplicationChrome_t3150366666_il2cpp_TypeInfo_var->static_fields)->get__statusBarColor_3();
		return L_0;
	}
}
// System.Void ApplicationChrome::set_statusBarColor(System.UInt32)
extern Il2CppClass* ApplicationChrome_t3150366666_il2cpp_TypeInfo_var;
extern const uint32_t ApplicationChrome_set_statusBarColor_m3773288632_MetadataUsageId;
extern "C"  void ApplicationChrome_set_statusBarColor_m3773288632 (Il2CppObject * __this /* static, unused */, uint32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ApplicationChrome_set_statusBarColor_m3773288632_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ApplicationChrome_t3150366666_il2cpp_TypeInfo_var);
		uint32_t L_0 = ((ApplicationChrome_t3150366666_StaticFields*)ApplicationChrome_t3150366666_il2cpp_TypeInfo_var->static_fields)->get__statusBarColor_3();
		uint32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001b;
		}
	}
	{
		uint32_t L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ApplicationChrome_t3150366666_il2cpp_TypeInfo_var);
		((ApplicationChrome_t3150366666_StaticFields*)ApplicationChrome_t3150366666_il2cpp_TypeInfo_var->static_fields)->set__statusBarColor_3(L_2);
		ApplicationChrome_applyUIColors_m1329407537(NULL /*static, unused*/, /*hidden argument*/NULL);
		ApplicationChrome_applyUIStates_m3276429891(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.UInt32 ApplicationChrome::get_navigationBarColor()
extern Il2CppClass* ApplicationChrome_t3150366666_il2cpp_TypeInfo_var;
extern const uint32_t ApplicationChrome_get_navigationBarColor_m3496849973_MetadataUsageId;
extern "C"  uint32_t ApplicationChrome_get_navigationBarColor_m3496849973 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ApplicationChrome_get_navigationBarColor_m3496849973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ApplicationChrome_t3150366666_il2cpp_TypeInfo_var);
		uint32_t L_0 = ((ApplicationChrome_t3150366666_StaticFields*)ApplicationChrome_t3150366666_il2cpp_TypeInfo_var->static_fields)->get__navigationBarColor_4();
		return L_0;
	}
}
// System.Void ApplicationChrome::set_navigationBarColor(System.UInt32)
extern Il2CppClass* ApplicationChrome_t3150366666_il2cpp_TypeInfo_var;
extern const uint32_t ApplicationChrome_set_navigationBarColor_m833562582_MetadataUsageId;
extern "C"  void ApplicationChrome_set_navigationBarColor_m833562582 (Il2CppObject * __this /* static, unused */, uint32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ApplicationChrome_set_navigationBarColor_m833562582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ApplicationChrome_t3150366666_il2cpp_TypeInfo_var);
		uint32_t L_0 = ((ApplicationChrome_t3150366666_StaticFields*)ApplicationChrome_t3150366666_il2cpp_TypeInfo_var->static_fields)->get__navigationBarColor_4();
		uint32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001b;
		}
	}
	{
		uint32_t L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ApplicationChrome_t3150366666_il2cpp_TypeInfo_var);
		((ApplicationChrome_t3150366666_StaticFields*)ApplicationChrome_t3150366666_il2cpp_TypeInfo_var->static_fields)->set__navigationBarColor_4(L_2);
		ApplicationChrome_applyUIColors_m1329407537(NULL /*static, unused*/, /*hidden argument*/NULL);
		ApplicationChrome_applyUIStates_m3276429891(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void CurrentScreen::.ctor()
extern "C"  void CurrentScreen__ctor_m1985495014 (CurrentScreen_t3026574117 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CurrentScreen::.cctor()
extern "C"  void CurrentScreen__cctor_m938707079 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// CurrentScreen CurrentScreen::get_Instance()
extern Il2CppClass* CurrentScreen_t3026574117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2035217420;
extern const uint32_t CurrentScreen_get_Instance_m3161187310_MetadataUsageId;
extern "C"  CurrentScreen_t3026574117 * CurrentScreen_get_Instance_m3161187310 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_get_Instance_m3161187310_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentScreen_t3026574117_il2cpp_TypeInfo_var);
		CurrentScreen_t3026574117 * L_0 = ((CurrentScreen_t3026574117_StaticFields*)CurrentScreen_t3026574117_il2cpp_TypeInfo_var->static_fields)->get__instance_6();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral2035217420, /*hidden argument*/NULL);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentScreen_t3026574117_il2cpp_TypeInfo_var);
		CurrentScreen_t3026574117 * L_2 = ((CurrentScreen_t3026574117_StaticFields*)CurrentScreen_t3026574117_il2cpp_TypeInfo_var->static_fields)->get__instance_6();
		return L_2;
	}
}
// System.Void CurrentScreen::Awake()
extern Il2CppClass* CurrentScreen_t3026574117_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_Awake_m2223100233_MetadataUsageId;
extern "C"  void CurrentScreen_Awake_m2223100233 (CurrentScreen_t3026574117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_Awake_m2223100233_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentScreen_t3026574117_il2cpp_TypeInfo_var);
		bool L_0 = ((CurrentScreen_t3026574117_StaticFields*)CurrentScreen_t3026574117_il2cpp_TypeInfo_var->static_fields)->get_FirstIn_5();
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentScreen_t3026574117_il2cpp_TypeInfo_var);
		((CurrentScreen_t3026574117_StaticFields*)CurrentScreen_t3026574117_il2cpp_TypeInfo_var->static_fields)->set__instance_6(__this);
		GameObjectU5BU5D_t2662109048* L_1 = __this->get_m_Display_2();
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		int32_t L_2 = 0;
		GameObject_t3674682005 * L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, 0, L_3, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_4 = __this->get_m_Display_2();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		int32_t L_5 = 1;
		GameObject_t3674682005 * L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, 1, L_6, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_7 = __this->get_m_Display_2();
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		int32_t L_8 = 2;
		GameObject_t3674682005 * L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, 2, L_9, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_10 = __this->get_m_Display_2();
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		int32_t L_11 = 3;
		GameObject_t3674682005 * L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, 3, L_12, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_13 = __this->get_m_Display_2();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		int32_t L_14 = 4;
		GameObject_t3674682005 * L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, 4, L_15, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_16 = __this->get_m_Display_2();
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 5);
		int32_t L_17 = 5;
		GameObject_t3674682005 * L_18 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, 5, L_18, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_19 = __this->get_m_Display_2();
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 5);
		int32_t L_20 = 5;
		GameObject_t3674682005 * L_21 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)50), L_21, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_22 = __this->get_m_Display_2();
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 6);
		int32_t L_23 = 6;
		GameObject_t3674682005 * L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, 6, L_24, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_25 = __this->get_m_Display_2();
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 7);
		int32_t L_26 = 7;
		GameObject_t3674682005 * L_27 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, 7, L_27, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_28 = __this->get_m_Display_2();
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 8);
		int32_t L_29 = 8;
		GameObject_t3674682005 * L_30 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, 8, L_30, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_31 = __this->get_m_Display_2();
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, ((int32_t)9));
		int32_t L_32 = ((int32_t)9);
		GameObject_t3674682005 * L_33 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)9), L_33, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_34 = __this->get_m_Display_2();
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)10));
		int32_t L_35 = ((int32_t)10);
		GameObject_t3674682005 * L_36 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)10), L_36, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_37 = __this->get_m_Display_2();
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, ((int32_t)11));
		int32_t L_38 = ((int32_t)11);
		GameObject_t3674682005 * L_39 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)11), L_39, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_40 = __this->get_m_Display_2();
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)12));
		int32_t L_41 = ((int32_t)12);
		GameObject_t3674682005 * L_42 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)12), L_42, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_43 = __this->get_m_Display_2();
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, ((int32_t)13));
		int32_t L_44 = ((int32_t)13);
		GameObject_t3674682005 * L_45 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)13), L_45, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_46 = __this->get_m_Display_2();
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)14));
		int32_t L_47 = ((int32_t)14);
		GameObject_t3674682005 * L_48 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)14), L_48, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_49 = __this->get_m_Display_2();
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, ((int32_t)15));
		int32_t L_50 = ((int32_t)15);
		GameObject_t3674682005 * L_51 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)15), L_51, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_52 = __this->get_m_Display_2();
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)16));
		int32_t L_53 = ((int32_t)16);
		GameObject_t3674682005 * L_54 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_53));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)16), L_54, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_55 = __this->get_m_Display_2();
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, ((int32_t)17));
		int32_t L_56 = ((int32_t)17);
		GameObject_t3674682005 * L_57 = (L_55)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)17), L_57, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_58 = __this->get_m_Display_2();
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)18));
		int32_t L_59 = ((int32_t)18);
		GameObject_t3674682005 * L_60 = (L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_59));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)18), L_60, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_61 = __this->get_m_Display_2();
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, ((int32_t)19));
		int32_t L_62 = ((int32_t)19);
		GameObject_t3674682005 * L_63 = (L_61)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)19), L_63, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_64 = __this->get_m_Display_2();
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, ((int32_t)20));
		int32_t L_65 = ((int32_t)20);
		GameObject_t3674682005 * L_66 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_65));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)20), L_66, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_67 = __this->get_m_Display_2();
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, ((int32_t)21));
		int32_t L_68 = ((int32_t)21);
		GameObject_t3674682005 * L_69 = (L_67)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)21), L_69, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_70 = __this->get_m_Display_2();
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, ((int32_t)22));
		int32_t L_71 = ((int32_t)22);
		GameObject_t3674682005 * L_72 = (L_70)->GetAt(static_cast<il2cpp_array_size_t>(L_71));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)22), L_72, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_73 = __this->get_m_Display_2();
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, ((int32_t)23));
		int32_t L_74 = ((int32_t)23);
		GameObject_t3674682005 * L_75 = (L_73)->GetAt(static_cast<il2cpp_array_size_t>(L_74));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)23), L_75, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_76 = __this->get_m_Display_2();
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, ((int32_t)24));
		int32_t L_77 = ((int32_t)24);
		GameObject_t3674682005 * L_78 = (L_76)->GetAt(static_cast<il2cpp_array_size_t>(L_77));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)24), L_78, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_79 = __this->get_m_Display_2();
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, ((int32_t)25));
		int32_t L_80 = ((int32_t)25);
		GameObject_t3674682005 * L_81 = (L_79)->GetAt(static_cast<il2cpp_array_size_t>(L_80));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)25), L_81, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_82 = __this->get_m_Display_2();
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, ((int32_t)26));
		int32_t L_83 = ((int32_t)26);
		GameObject_t3674682005 * L_84 = (L_82)->GetAt(static_cast<il2cpp_array_size_t>(L_83));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)26), L_84, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_85 = __this->get_m_Display_2();
		NullCheck(L_85);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_85, ((int32_t)27));
		int32_t L_86 = ((int32_t)27);
		GameObject_t3674682005 * L_87 = (L_85)->GetAt(static_cast<il2cpp_array_size_t>(L_86));
		CurrentState_AddScreenState_m2795282826(NULL /*static, unused*/, ((int32_t)27), L_87, /*hidden argument*/NULL);
		((CurrentScreen_t3026574117_StaticFields*)CurrentScreen_t3026574117_il2cpp_TypeInfo_var->static_fields)->set_FirstIn_5((bool)1);
		return;
	}
}
// System.Void CurrentScreen::BackButton()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* PublicSpotMessageDB_t1853916122_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_BackButton_m2788205463_MetadataUsageId;
extern "C"  void CurrentScreen_BackButton_m2788205463 (CurrentScreen_t3026574117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_BackButton_m2788205463_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_1 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		CurrentScreen_VisibleFalseState_m2603065973(__this, L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_2 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 0)
		{
			goto IL_00f0;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 1)
		{
			goto IL_00fc;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 2)
		{
			goto IL_011e;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 3)
		{
			goto IL_014e;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 4)
		{
			goto IL_0136;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 5)
		{
			goto IL_012a;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 6)
		{
			goto IL_015a;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 7)
		{
			goto IL_0166;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 8)
		{
			goto IL_0172;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 9)
		{
			goto IL_017e;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 10)
		{
			goto IL_018b;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 11)
		{
			goto IL_01a1;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 12)
		{
			goto IL_01ae;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 13)
		{
			goto IL_01bb;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 14)
		{
			goto IL_01c8;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 15)
		{
			goto IL_01d4;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 16)
		{
			goto IL_01e1;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 17)
		{
			goto IL_01ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 18)
		{
			goto IL_01fb;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 19)
		{
			goto IL_0208;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 20)
		{
			goto IL_0256;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 21)
		{
			goto IL_0215;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 22)
		{
			goto IL_0222;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 23)
		{
			goto IL_022f;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 24)
		{
			goto IL_023c;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 25)
		{
			goto IL_0249;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 26)
		{
			goto IL_027c;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 27)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 28)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 29)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 30)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 31)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 32)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 33)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 34)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 35)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 36)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 37)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 38)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 39)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 40)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 41)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 42)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 43)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 44)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 45)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 46)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 47)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 48)
		{
			goto IL_0289;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 49)
		{
			goto IL_0142;
		}
	}
	{
		goto IL_0289;
	}

IL_00f0:
	{
		CurrentScreen_State_m3173002564(__this, 0, /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_4 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0112;
		}
	}
	{
		CurrentScreen_State_m3173002564(__this, 4, /*hidden argument*/NULL);
		goto IL_0119;
	}

IL_0112:
	{
		CurrentScreen_State_m3173002564(__this, 1, /*hidden argument*/NULL);
	}

IL_0119:
	{
		goto IL_028e;
	}

IL_011e:
	{
		CurrentScreen_State_m3173002564(__this, 2, /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_012a:
	{
		CurrentScreen_State_m3173002564(__this, 2, /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_0136:
	{
		CurrentScreen_State_m3173002564(__this, 2, /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_0142:
	{
		CurrentScreen_State_m3173002564(__this, 5, /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_014e:
	{
		CurrentScreen_State_m3173002564(__this, 1, /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_015a:
	{
		CurrentScreen_State_m3173002564(__this, 6, /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_0166:
	{
		CurrentScreen_State_m3173002564(__this, 6, /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_0172:
	{
		CurrentScreen_State_m3173002564(__this, 6, /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_017e:
	{
		CurrentScreen_State_m3173002564(__this, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_018b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PublicSpotMessageDB_t1853916122_il2cpp_TypeInfo_var);
		PublicSpotMessageDB_t1853916122 * L_5 = PublicSpotMessageDB_get_Instance_m2156621208(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		PublicSpotMessageDB_AllDeletePSMDB_m95139407(L_5, /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, 2, /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_01a1:
	{
		CurrentScreen_State_m3173002564(__this, ((int32_t)11), /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_01ae:
	{
		CurrentScreen_State_m3173002564(__this, ((int32_t)11), /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_01bb:
	{
		CurrentScreen_State_m3173002564(__this, ((int32_t)11), /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_01c8:
	{
		CurrentScreen_State_m3173002564(__this, 2, /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_01d4:
	{
		CurrentScreen_State_m3173002564(__this, ((int32_t)15), /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_01e1:
	{
		CurrentScreen_State_m3173002564(__this, ((int32_t)16), /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_01ee:
	{
		CurrentScreen_State_m3173002564(__this, ((int32_t)20), /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_01fb:
	{
		CurrentScreen_State_m3173002564(__this, ((int32_t)20), /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_0208:
	{
		CurrentScreen_State_m3173002564(__this, ((int32_t)27), /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_0215:
	{
		CurrentScreen_State_m3173002564(__this, ((int32_t)20), /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_0222:
	{
		CurrentScreen_State_m3173002564(__this, ((int32_t)20), /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_022f:
	{
		CurrentScreen_State_m3173002564(__this, ((int32_t)20), /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_023c:
	{
		CurrentScreen_State_m3173002564(__this, ((int32_t)20), /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_0249:
	{
		CurrentScreen_State_m3173002564(__this, ((int32_t)20), /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_0256:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_6 = CurrentState_get_LoginState_m3052003788(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_026c;
		}
	}
	{
		CurrentScreen_State_m3173002564(__this, 4, /*hidden argument*/NULL);
		goto IL_0277;
	}

IL_026c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_7 = CurrentState_get_LoginState_m3052003788(NULL /*static, unused*/, /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, L_7, /*hidden argument*/NULL);
	}

IL_0277:
	{
		goto IL_028e;
	}

IL_027c:
	{
		CurrentScreen_State_m3173002564(__this, ((int32_t)50), /*hidden argument*/NULL);
		goto IL_028e;
	}

IL_0289:
	{
		goto IL_028e;
	}

IL_028e:
	{
		return;
	}
}
// System.Void CurrentScreen::State(CurrentState/States)
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_State_m3173002564_MetadataUsageId;
extern "C"  void CurrentScreen_State_m3173002564 (CurrentScreen_t3026574117 * __this, int32_t ___Current0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_State_m3173002564_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___Current0;
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		GameObject_t3674682005 * L_1 = CurrentState_GetScreenState_m3808002068(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, (bool)1, /*hidden argument*/NULL);
		int32_t L_2 = ___Current0;
		CurrentState_set_State_m3490075940(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CurrentScreen::VisibleFalseState(CurrentState/States)
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_VisibleFalseState_m2603065973_MetadataUsageId;
extern "C"  void CurrentScreen_VisibleFalseState_m2603065973 (CurrentScreen_t3026574117 * __this, int32_t ___Current0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_VisibleFalseState_m2603065973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___Current0;
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		GameObject_t3674682005 * L_1 = CurrentState_GetScreenState_m3808002068(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CurrentScreen::LoginButton()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_LoginButton_m1187927039_MetadataUsageId;
extern "C"  void CurrentScreen_LoginButton_m1187927039 (CurrentScreen_t3026574117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_LoginButton_m1187927039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		CurrentScreen_VisibleFalseState_m2603065973(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, 4, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean CurrentScreen::Loggied()
extern Il2CppClass* LoginCheck_t219618111_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_Loggied_m1683951613_MetadataUsageId;
extern "C"  bool CurrentScreen_Loggied_m1683951613 (CurrentScreen_t3026574117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_Loggied_m1683951613_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		IL2CPP_RUNTIME_CLASS_INIT(LoginCheck_t219618111_il2cpp_TypeInfo_var);
		int32_t L_0 = LoginCheck_get_LoginResult_m2321846537(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0015;
	}

IL_0013:
	{
		V_0 = (bool)0;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void CurrentScreen::MainButton(System.Int32)
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppClass* iosWebView_t3524785996_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern Il2CppCodeGenString* _stringLiteral2523543316;
extern Il2CppCodeGenString* _stringLiteral3646559839;
extern const uint32_t CurrentScreen_MainButton_m841552442_MetadataUsageId;
extern "C"  void CurrentScreen_MainButton_m841552442 (CurrentScreen_t3026574117 * __this, int32_t ___btn0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_MainButton_m841552442_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___btn0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		goto IL_00d7;
	}

IL_0014:
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, 0, /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, 1, /*hidden argument*/NULL);
		goto IL_00dc;
	}

IL_0027:
	{
		int32_t L_3 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if (L_4 == 0)
		{
			goto IL_0044;
		}
		if (L_4 == 1)
		{
			goto IL_006f;
		}
		if (L_4 == 2)
		{
			goto IL_009e;
		}
	}
	{
		goto IL_00cd;
	}

IL_0044:
	{
		int32_t L_5 = ((int32_t)3);
		Il2CppObject * L_6 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_5);
		NullCheck((Enum_t2862688501 *)L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_6);
		int32_t L_8 = ((int32_t)2);
		Il2CppObject * L_9 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_8);
		NullCheck((Enum_t2862688501 *)L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_9);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_7, _stringLiteral3660319847, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_PopupMessage_m2237488056(NULL /*static, unused*/, 8, /*hidden argument*/NULL);
		goto IL_00d2;
	}

IL_006f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_11 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		AndroidWebview_Call_m335597645(L_11, _stringLiteral2523543316, _stringLiteral3646559839, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iosWebView_t3524785996_il2cpp_TypeInfo_var);
		iosWebView_t3524785996 * L_12 = iosWebView_get_Instance_m3613006564(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		iosWebView_Act_m2518930019(L_12, _stringLiteral3646559839, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_PopupMessage_m2237488056(NULL /*static, unused*/, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_00d2;
	}

IL_009e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_13 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		AndroidWebview_Call_m335597645(L_13, _stringLiteral2523543316, _stringLiteral3646559839, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iosWebView_t3524785996_il2cpp_TypeInfo_var);
		iosWebView_t3524785996 * L_14 = iosWebView_get_Instance_m3613006564(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		iosWebView_Act_m2518930019(L_14, _stringLiteral3646559839, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_PopupMessage_m2237488056(NULL /*static, unused*/, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_00d2;
	}

IL_00cd:
	{
		goto IL_00d2;
	}

IL_00d2:
	{
		goto IL_00dc;
	}

IL_00d7:
	{
		goto IL_00dc;
	}

IL_00dc:
	{
		return;
	}
}
// System.Void CurrentScreen::OKButton()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_OKButton_m2045106860_MetadataUsageId;
extern "C"  void CurrentScreen_OKButton_m2045106860 (CurrentScreen_t3026574117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_OKButton_m2045106860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		bool L_0 = CurrentScreen_Loggied_m1683951613(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		CurrentScreen_State_m3173002564(__this, 2, /*hidden argument*/NULL);
		goto IL_002b;
	}

IL_0024:
	{
		CurrentScreen_State_m3173002564(__this, 4, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void CurrentScreen::SumabomenuButton(System.Int32)
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppClass* iosWebView_t3524785996_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern Il2CppCodeGenString* _stringLiteral2523543316;
extern Il2CppCodeGenString* _stringLiteral1281893213;
extern const uint32_t CurrentScreen_SumabomenuButton_m2687570531_MetadataUsageId;
extern "C"  void CurrentScreen_SumabomenuButton_m2687570531 (CurrentScreen_t3026574117 * __this, int32_t ___btn0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_SumabomenuButton_m2687570531_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___btn0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0069;
		}
		if (L_1 == 2)
		{
			goto IL_00ad;
		}
		if (L_1 == 3)
		{
			goto IL_00c8;
		}
		if (L_1 == 4)
		{
			goto IL_0172;
		}
		if (L_1 == 5)
		{
			goto IL_01b0;
		}
	}
	{
		goto IL_0203;
	}

IL_0025:
	{
		int32_t L_2 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_3 = ((int32_t)3);
		Il2CppObject * L_4 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_3);
		NullCheck((Enum_t2862688501 *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_4);
		int32_t L_6 = ((int32_t)2);
		Il2CppObject * L_7 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_6);
		NullCheck((Enum_t2862688501 *)L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_7);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_5, _stringLiteral3660319847, L_8, /*hidden argument*/NULL);
		return;
	}

IL_0050:
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, 2, /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, 5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		goto IL_0208;
	}

IL_0069:
	{
		int32_t L_9 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0094;
		}
	}
	{
		int32_t L_10 = ((int32_t)3);
		Il2CppObject * L_11 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_10);
		NullCheck((Enum_t2862688501 *)L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_11);
		int32_t L_13 = ((int32_t)2);
		Il2CppObject * L_14 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_13);
		NullCheck((Enum_t2862688501 *)L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_12, _stringLiteral3660319847, L_15, /*hidden argument*/NULL);
		return;
	}

IL_0094:
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, 2, /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, 6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, 6, /*hidden argument*/NULL);
		goto IL_0208;
	}

IL_00ad:
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, 2, /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, ((int32_t)11), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, ((int32_t)11), /*hidden argument*/NULL);
		goto IL_0208;
	}

IL_00c8:
	{
		int32_t L_16 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_16;
		int32_t L_17 = V_1;
		if (L_17 == 0)
		{
			goto IL_00e5;
		}
		if (L_17 == 1)
		{
			goto IL_010a;
		}
		if (L_17 == 2)
		{
			goto IL_0139;
		}
	}
	{
		goto IL_0168;
	}

IL_00e5:
	{
		int32_t L_18 = ((int32_t)3);
		Il2CppObject * L_19 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_18);
		NullCheck((Enum_t2862688501 *)L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_19);
		int32_t L_21 = ((int32_t)2);
		Il2CppObject * L_22 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_21);
		NullCheck((Enum_t2862688501 *)L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_22);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_20, _stringLiteral3660319847, L_23, /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_010a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_24 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_24);
		AndroidWebview_Call_m335597645(L_24, _stringLiteral2523543316, _stringLiteral1281893213, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iosWebView_t3524785996_il2cpp_TypeInfo_var);
		iosWebView_t3524785996 * L_25 = iosWebView_get_Instance_m3613006564(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_25);
		iosWebView_Act_m2518930019(L_25, _stringLiteral1281893213, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, ((int32_t)15), /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_0139:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_26 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_26);
		AndroidWebview_Call_m335597645(L_26, _stringLiteral2523543316, _stringLiteral1281893213, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iosWebView_t3524785996_il2cpp_TypeInfo_var);
		iosWebView_t3524785996 * L_27 = iosWebView_get_Instance_m3613006564(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_27);
		iosWebView_Act_m2518930019(L_27, _stringLiteral1281893213, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, ((int32_t)15), /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_0168:
	{
		goto IL_016d;
	}

IL_016d:
	{
		goto IL_0208;
	}

IL_0172:
	{
		int32_t L_28 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_019d;
		}
	}
	{
		int32_t L_29 = ((int32_t)3);
		Il2CppObject * L_30 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_29);
		NullCheck((Enum_t2862688501 *)L_30);
		String_t* L_31 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_30);
		int32_t L_32 = ((int32_t)2);
		Il2CppObject * L_33 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_32);
		NullCheck((Enum_t2862688501 *)L_33);
		String_t* L_34 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_33);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_31, _stringLiteral3660319847, L_34, /*hidden argument*/NULL);
		return;
	}

IL_019d:
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, 2, /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, 3, /*hidden argument*/NULL);
		goto IL_0208;
	}

IL_01b0:
	{
		int32_t L_35 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_35)
		{
			goto IL_01db;
		}
	}
	{
		int32_t L_36 = ((int32_t)3);
		Il2CppObject * L_37 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_36);
		NullCheck((Enum_t2862688501 *)L_37);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_37);
		int32_t L_39 = ((int32_t)2);
		Il2CppObject * L_40 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_39);
		NullCheck((Enum_t2862688501 *)L_40);
		String_t* L_41 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_40);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_38, _stringLiteral3660319847, L_41, /*hidden argument*/NULL);
		return;
	}

IL_01db:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_42 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_43 = PrivateURL_get_MemberShipChange_m2995936519(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_42);
		AndroidWebview_Call_m335597645(L_42, _stringLiteral2523543316, L_43, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iosWebView_t3524785996_il2cpp_TypeInfo_var);
		iosWebView_t3524785996 * L_44 = iosWebView_get_Instance_m3613006564(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_45 = PrivateURL_get_MemberShipChange_m2995936519(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_44);
		iosWebView_Act_m2518930019(L_44, L_45, /*hidden argument*/NULL);
		goto IL_0208;
	}

IL_0203:
	{
		goto IL_0208;
	}

IL_0208:
	{
		return;
	}
}
// System.Void CurrentScreen::PremiumserviceRegist()
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppClass* iosWebView_t3524785996_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2523543316;
extern Il2CppCodeGenString* _stringLiteral3811534965;
extern const uint32_t CurrentScreen_PremiumserviceRegist_m1864671026_MetadataUsageId;
extern "C"  void CurrentScreen_PremiumserviceRegist_m1864671026 (CurrentScreen_t3026574117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_PremiumserviceRegist_m1864671026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_0 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		AndroidWebview_Call_m335597645(L_0, _stringLiteral2523543316, _stringLiteral3811534965, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iosWebView_t3524785996_il2cpp_TypeInfo_var);
		iosWebView_t3524785996 * L_1 = iosWebView_get_Instance_m3613006564(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		iosWebView_Act_m2518930019(L_1, _stringLiteral3811534965, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator CurrentScreen::JsonStart()
extern Il2CppClass* U3CJsonStartU3Ec__Iterator5_t1185739994_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_JsonStart_m140421542_MetadataUsageId;
extern "C"  Il2CppObject * CurrentScreen_JsonStart_m140421542 (CurrentScreen_t3026574117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_JsonStart_m140421542_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CJsonStartU3Ec__Iterator5_t1185739994 * V_0 = NULL;
	{
		U3CJsonStartU3Ec__Iterator5_t1185739994 * L_0 = (U3CJsonStartU3Ec__Iterator5_t1185739994 *)il2cpp_codegen_object_new(U3CJsonStartU3Ec__Iterator5_t1185739994_il2cpp_TypeInfo_var);
		U3CJsonStartU3Ec__Iterator5__ctor_m3081243089(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CJsonStartU3Ec__Iterator5_t1185739994 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CJsonStartU3Ec__Iterator5_t1185739994 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator CurrentScreen::LoginCheckProgress(UnityEngine.WWW)
extern Il2CppClass* U3CLoginCheckProgressU3Ec__Iterator6_t4282206053_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_LoginCheckProgress_m1704831024_MetadataUsageId;
extern "C"  Il2CppObject * CurrentScreen_LoginCheckProgress_m1704831024 (CurrentScreen_t3026574117 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_LoginCheckProgress_m1704831024_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 * V_0 = NULL;
	{
		U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 * L_0 = (U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 *)il2cpp_codegen_object_new(U3CLoginCheckProgressU3Ec__Iterator6_t4282206053_il2cpp_TypeInfo_var);
		U3CLoginCheckProgressU3Ec__Iterator6__ctor_m2835064534(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 * L_1 = V_0;
		WWW_t3134621005 * L_2 = ___www0;
		NullCheck(L_1);
		L_1->set_www_0(L_2);
		U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 * L_3 = V_0;
		WWW_t3134621005 * L_4 = ___www0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Ewww_3(L_4);
		U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 * L_5 = V_0;
		return L_5;
	}
}
// System.Void CurrentScreen::TakeJson(System.String)
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* LoginCheck_t219618111_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2760346223;
extern Il2CppCodeGenString* _stringLiteral3738031314;
extern Il2CppCodeGenString* _stringLiteral2084199307;
extern Il2CppCodeGenString* _stringLiteral219618111;
extern Il2CppCodeGenString* _stringLiteral2982240411;
extern Il2CppCodeGenString* _stringLiteral2444407869;
extern Il2CppCodeGenString* _stringLiteral2542803558;
extern Il2CppCodeGenString* _stringLiteral1964198090;
extern Il2CppCodeGenString* _stringLiteral73596745;
extern Il2CppCodeGenString* _stringLiteral2619578343;
extern Il2CppCodeGenString* _stringLiteral2622298;
extern const uint32_t CurrentScreen_TakeJson_m27493301_MetadataUsageId;
extern "C"  void CurrentScreen_TakeJson_m27493301 (CurrentScreen_t3026574117 * __this, String_t* ___js0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_TakeJson_m27493301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JsonData_t1715015430 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___js0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		JsonData_t1715015430 * L_1 = JsonMapper_ToObject_m3425498490(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JsonData_t1715015430 * L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		JsonData_t1715015430 * L_3 = V_0;
		NullCheck(L_3);
		JsonData_t1715015430 * L_4 = JsonData_get_Item_m253158020(L_3, 1, /*hidden argument*/NULL);
		NullCheck(L_4);
		JsonData_t1715015430 * L_5 = JsonData_get_Item_m4009629743(L_4, _stringLiteral2760346223, /*hidden argument*/NULL);
		NullCheck(L_5);
		JsonData_t1715015430 * L_6 = JsonData_get_Item_m4009629743(L_5, _stringLiteral3738031314, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t476798718_il2cpp_TypeInfo_var);
		bool L_8 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0096;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginPopupMessage_m2175582148(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		CtrlPlugins_Login_m1267639130(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		int32_t L_9 = ((int32_t)3);
		Il2CppObject * L_10 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_9);
		NullCheck((Enum_t2862688501 *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_10);
		JsonData_t1715015430 * L_12 = V_0;
		NullCheck(L_12);
		JsonData_t1715015430 * L_13 = JsonData_get_Item_m253158020(L_12, 1, /*hidden argument*/NULL);
		NullCheck(L_13);
		JsonData_t1715015430 * L_14 = JsonData_get_Item_m4009629743(L_13, _stringLiteral2760346223, /*hidden argument*/NULL);
		NullCheck(L_14);
		JsonData_t1715015430 * L_15 = JsonData_get_Item_m4009629743(L_14, _stringLiteral2084199307, /*hidden argument*/NULL);
		NullCheck(L_15);
		JsonData_t1715015430 * L_16 = JsonData_get_Item_m253158020(L_15, 0, /*hidden argument*/NULL);
		NullCheck(L_16);
		JsonData_t1715015430 * L_17 = JsonData_get_Item_m253158020(L_16, 0, /*hidden argument*/NULL);
		NullCheck(L_17);
		JsonData_t1715015430 * L_18 = JsonData_get_Item_m253158020(L_17, 1, /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_18);
		int32_t L_20 = ((int32_t)2);
		Il2CppObject * L_21 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_20);
		NullCheck((Enum_t2862688501 *)L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_21);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_11, L_19, L_22, /*hidden argument*/NULL);
		goto IL_0250;
	}

IL_0096:
	{
		JsonData_t1715015430 * L_23 = V_0;
		NullCheck(L_23);
		JsonData_t1715015430 * L_24 = JsonData_get_Item_m4009629743(L_23, _stringLiteral219618111, /*hidden argument*/NULL);
		NullCheck(L_24);
		JsonData_t1715015430 * L_25 = JsonData_get_Item_m4009629743(L_24, _stringLiteral2982240411, /*hidden argument*/NULL);
		NullCheck(L_25);
		JsonData_t1715015430 * L_26 = JsonData_get_Item_m4009629743(L_25, _stringLiteral2444407869, /*hidden argument*/NULL);
		NullCheck(L_26);
		String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_26);
		int32_t L_28 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_020b;
		}
	}
	{
		JsonData_t1715015430 * L_29 = V_0;
		NullCheck(L_29);
		JsonData_t1715015430 * L_30 = JsonData_get_Item_m4009629743(L_29, _stringLiteral219618111, /*hidden argument*/NULL);
		NullCheck(L_30);
		JsonData_t1715015430 * L_31 = JsonData_get_Item_m4009629743(L_30, _stringLiteral2982240411, /*hidden argument*/NULL);
		NullCheck(L_31);
		JsonData_t1715015430 * L_32 = JsonData_get_Item_m4009629743(L_31, _stringLiteral2542803558, /*hidden argument*/NULL);
		NullCheck(L_32);
		String_t* L_33 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_32);
		PrivateURL_set_customerId_m1347654551(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		InputField_t609046876 * L_34 = __this->get_m_UserLoginEmail_4();
		NullCheck(L_34);
		String_t* L_35 = InputField_get_text_m3972300732(L_34, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_37 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0111;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral1964198090, /*hidden argument*/NULL);
	}

IL_0111:
	{
		InputField_t609046876 * L_38 = __this->get_m_UserLoginEmail_4();
		NullCheck(L_38);
		String_t* L_39 = InputField_get_text_m3972300732(L_38, /*hidden argument*/NULL);
		PrivateURL_set_userloginemail_m704658226(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginPopupMessage_m2175582148(NULL /*static, unused*/, 4, /*hidden argument*/NULL);
		JsonData_t1715015430 * L_40 = V_0;
		NullCheck(L_40);
		JsonData_t1715015430 * L_41 = JsonData_get_Item_m4009629743(L_40, _stringLiteral219618111, /*hidden argument*/NULL);
		NullCheck(L_41);
		JsonData_t1715015430 * L_42 = JsonData_get_Item_m4009629743(L_41, _stringLiteral2982240411, /*hidden argument*/NULL);
		NullCheck(L_42);
		JsonData_t1715015430 * L_43 = JsonData_get_Item_m4009629743(L_42, _stringLiteral2619578343, /*hidden argument*/NULL);
		NullCheck(L_43);
		String_t* L_44 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_43);
		int32_t L_45 = ((int32_t)2);
		Il2CppObject * L_46 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_45);
		NullCheck((Enum_t2862688501 *)L_46);
		String_t* L_47 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_46);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, _stringLiteral73596745, L_44, L_47, /*hidden argument*/NULL);
		JsonData_t1715015430 * L_48 = V_0;
		NullCheck(L_48);
		JsonData_t1715015430 * L_49 = JsonData_get_Item_m4009629743(L_48, _stringLiteral219618111, /*hidden argument*/NULL);
		NullCheck(L_49);
		JsonData_t1715015430 * L_50 = JsonData_get_Item_m4009629743(L_49, _stringLiteral2982240411, /*hidden argument*/NULL);
		NullCheck(L_50);
		JsonData_t1715015430 * L_51 = JsonData_get_Item_m4009629743(L_50, _stringLiteral2444407869, /*hidden argument*/NULL);
		NullCheck(L_51);
		String_t* L_52 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_51);
		int32_t L_53 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LoginCheck_t219618111_il2cpp_TypeInfo_var);
		LoginCheck_set_LoginResult_m2617684276(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		JsonData_t1715015430 * L_54 = V_0;
		NullCheck(L_54);
		JsonData_t1715015430 * L_55 = JsonData_get_Item_m4009629743(L_54, _stringLiteral219618111, /*hidden argument*/NULL);
		NullCheck(L_55);
		JsonData_t1715015430 * L_56 = JsonData_get_Item_m4009629743(L_55, _stringLiteral2982240411, /*hidden argument*/NULL);
		NullCheck(L_56);
		JsonData_t1715015430 * L_57 = JsonData_get_Item_m4009629743(L_56, _stringLiteral2622298, /*hidden argument*/NULL);
		NullCheck(L_57);
		String_t* L_58 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_57);
		int32_t L_59 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_58, /*hidden argument*/NULL);
		CurrentState_set_MemberState_m2378785892(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		CurrentScreen_VisibleFalseState_m2603065973(__this, 4, /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, 2, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_01f9;
	}

IL_01d1:
	{
		GameObjectU5BU5D_t2662109048* L_60 = __this->get_m_MenuButton_3();
		int32_t L_61 = V_1;
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)((int32_t)4+(int32_t)L_61)));
		int32_t L_62 = ((int32_t)((int32_t)4+(int32_t)L_61));
		GameObject_t3674682005 * L_63 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		NullCheck(L_63);
		bool L_64 = GameObject_get_activeSelf_m3858025161(L_63, /*hidden argument*/NULL);
		if (L_64)
		{
			goto IL_01f5;
		}
	}
	{
		GameObjectU5BU5D_t2662109048* L_65 = __this->get_m_MenuButton_3();
		int32_t L_66 = V_1;
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, ((int32_t)((int32_t)4+(int32_t)L_66)));
		int32_t L_67 = ((int32_t)((int32_t)4+(int32_t)L_66));
		GameObject_t3674682005 * L_68 = (L_65)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		NullCheck(L_68);
		GameObject_SetActive_m3538205401(L_68, (bool)1, /*hidden argument*/NULL);
	}

IL_01f5:
	{
		int32_t L_69 = V_1;
		V_1 = ((int32_t)((int32_t)L_69+(int32_t)1));
	}

IL_01f9:
	{
		int32_t L_70 = V_1;
		if ((((int32_t)L_70) < ((int32_t)2)))
		{
			goto IL_01d1;
		}
	}
	{
		CtrlPlugins_Login_m1267639130(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		goto IL_0250;
	}

IL_020b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginPopupMessage_m2175582148(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		int32_t L_71 = ((int32_t)3);
		Il2CppObject * L_72 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_71);
		NullCheck((Enum_t2862688501 *)L_72);
		String_t* L_73 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_72);
		JsonData_t1715015430 * L_74 = V_0;
		NullCheck(L_74);
		JsonData_t1715015430 * L_75 = JsonData_get_Item_m4009629743(L_74, _stringLiteral219618111, /*hidden argument*/NULL);
		NullCheck(L_75);
		JsonData_t1715015430 * L_76 = JsonData_get_Item_m4009629743(L_75, _stringLiteral2982240411, /*hidden argument*/NULL);
		NullCheck(L_76);
		JsonData_t1715015430 * L_77 = JsonData_get_Item_m4009629743(L_76, _stringLiteral2619578343, /*hidden argument*/NULL);
		NullCheck(L_77);
		String_t* L_78 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_77);
		int32_t L_79 = ((int32_t)2);
		Il2CppObject * L_80 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_79);
		NullCheck((Enum_t2862688501 *)L_80);
		String_t* L_81 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_80);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_73, L_78, L_81, /*hidden argument*/NULL);
	}

IL_0250:
	{
		return;
	}
}
// System.Void CurrentScreen::LoginScreenButton(System.Int32)
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppClass* iosWebView_t3524785996_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* LoginCheck_t219618111_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern Il2CppCodeGenString* _stringLiteral2523543316;
extern Il2CppCodeGenString* _stringLiteral2506161958;
extern Il2CppCodeGenString* _stringLiteral2462281341;
extern const uint32_t CurrentScreen_LoginScreenButton_m4268466268_MetadataUsageId;
extern "C"  void CurrentScreen_LoginScreenButton_m4268466268 (CurrentScreen_t3026574117 * __this, int32_t ___btn0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_LoginScreenButton_m4268466268_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___btn0;
		V_1 = L_0;
		int32_t L_1 = V_1;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_008d;
		}
		if (L_1 == 2)
		{
			goto IL_0129;
		}
		if (L_1 == 3)
		{
			goto IL_01c5;
		}
	}
	{
		goto IL_0206;
	}

IL_001d:
	{
		int32_t L_2 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = V_2;
		if (L_3 == 0)
		{
			goto IL_003a;
		}
		if (L_3 == 1)
		{
			goto IL_005f;
		}
		if (L_3 == 2)
		{
			goto IL_0071;
		}
	}
	{
		goto IL_0083;
	}

IL_003a:
	{
		int32_t L_4 = ((int32_t)3);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		int32_t L_7 = ((int32_t)2);
		Il2CppObject * L_8 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_7);
		NullCheck((Enum_t2862688501 *)L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_6, _stringLiteral3660319847, L_9, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_005f:
	{
		Il2CppObject * L_10 = CurrentScreen_JsonStart_m140421542(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_10, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_0071:
	{
		Il2CppObject * L_11 = CurrentScreen_JsonStart_m140421542(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_11, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_0083:
	{
		goto IL_0088;
	}

IL_0088:
	{
		goto IL_020b;
	}

IL_008d:
	{
		int32_t L_12 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_12;
		int32_t L_13 = V_2;
		if (L_13 == 0)
		{
			goto IL_00aa;
		}
		if (L_13 == 1)
		{
			goto IL_00cf;
		}
		if (L_13 == 2)
		{
			goto IL_00f7;
		}
	}
	{
		goto IL_011f;
	}

IL_00aa:
	{
		int32_t L_14 = ((int32_t)3);
		Il2CppObject * L_15 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_14);
		NullCheck((Enum_t2862688501 *)L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_15);
		int32_t L_17 = ((int32_t)2);
		Il2CppObject * L_18 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_17);
		NullCheck((Enum_t2862688501 *)L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_18);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_16, _stringLiteral3660319847, L_19, /*hidden argument*/NULL);
		goto IL_0124;
	}

IL_00cf:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_20 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		AndroidWebview_Call_m335597645(L_20, _stringLiteral2523543316, _stringLiteral2506161958, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iosWebView_t3524785996_il2cpp_TypeInfo_var);
		iosWebView_t3524785996 * L_21 = iosWebView_get_Instance_m3613006564(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		iosWebView_Act_m2518930019(L_21, _stringLiteral2506161958, /*hidden argument*/NULL);
		goto IL_0124;
	}

IL_00f7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_22 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_22);
		AndroidWebview_Call_m335597645(L_22, _stringLiteral2523543316, _stringLiteral2506161958, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iosWebView_t3524785996_il2cpp_TypeInfo_var);
		iosWebView_t3524785996 * L_23 = iosWebView_get_Instance_m3613006564(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_23);
		iosWebView_Act_m2518930019(L_23, _stringLiteral2506161958, /*hidden argument*/NULL);
		goto IL_0124;
	}

IL_011f:
	{
		goto IL_0124;
	}

IL_0124:
	{
		goto IL_020b;
	}

IL_0129:
	{
		int32_t L_24 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_24;
		int32_t L_25 = V_2;
		if (L_25 == 0)
		{
			goto IL_0146;
		}
		if (L_25 == 1)
		{
			goto IL_016b;
		}
		if (L_25 == 2)
		{
			goto IL_0193;
		}
	}
	{
		goto IL_01bb;
	}

IL_0146:
	{
		int32_t L_26 = ((int32_t)3);
		Il2CppObject * L_27 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_26);
		NullCheck((Enum_t2862688501 *)L_27);
		String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_27);
		int32_t L_29 = ((int32_t)2);
		Il2CppObject * L_30 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_29);
		NullCheck((Enum_t2862688501 *)L_30);
		String_t* L_31 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_30);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_28, _stringLiteral3660319847, L_31, /*hidden argument*/NULL);
		goto IL_01c0;
	}

IL_016b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_32 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_32);
		AndroidWebview_Call_m335597645(L_32, _stringLiteral2523543316, _stringLiteral2462281341, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iosWebView_t3524785996_il2cpp_TypeInfo_var);
		iosWebView_t3524785996 * L_33 = iosWebView_get_Instance_m3613006564(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_33);
		iosWebView_Act_m2518930019(L_33, _stringLiteral2462281341, /*hidden argument*/NULL);
		goto IL_01c0;
	}

IL_0193:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_34 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_34);
		AndroidWebview_Call_m335597645(L_34, _stringLiteral2523543316, _stringLiteral2462281341, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iosWebView_t3524785996_il2cpp_TypeInfo_var);
		iosWebView_t3524785996 * L_35 = iosWebView_get_Instance_m3613006564(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_35);
		iosWebView_Act_m2518930019(L_35, _stringLiteral2462281341, /*hidden argument*/NULL);
		goto IL_01c0;
	}

IL_01bb:
	{
		goto IL_01c0;
	}

IL_01c0:
	{
		goto IL_020b;
	}

IL_01c5:
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, 4, /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, 2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_MemberState_m2378785892(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LoginCheck_t219618111_il2cpp_TypeInfo_var);
		LoginCheck_set_LoginResult_m2617684276(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_01fa;
	}

IL_01e6:
	{
		GameObjectU5BU5D_t2662109048* L_36 = __this->get_m_MenuButton_3();
		int32_t L_37 = V_0;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)((int32_t)4+(int32_t)L_37)));
		int32_t L_38 = ((int32_t)((int32_t)4+(int32_t)L_37));
		GameObject_t3674682005 * L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		NullCheck(L_39);
		GameObject_SetActive_m3538205401(L_39, (bool)0, /*hidden argument*/NULL);
		int32_t L_40 = V_0;
		V_0 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_01fa:
	{
		int32_t L_41 = V_0;
		if ((((int32_t)L_41) < ((int32_t)2)))
		{
			goto IL_01e6;
		}
	}
	{
		goto IL_020b;
	}

IL_0206:
	{
		goto IL_020b;
	}

IL_020b:
	{
		return;
	}
}
// System.Collections.IEnumerator CurrentScreen::MessageCountGetJsonStart()
extern Il2CppClass* U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_MessageCountGetJsonStart_m2449123042_MetadataUsageId;
extern "C"  Il2CppObject * CurrentScreen_MessageCountGetJsonStart_m2449123042 (CurrentScreen_t3026574117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_MessageCountGetJsonStart_m2449123042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382 * V_0 = NULL;
	{
		U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382 * L_0 = (U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382 *)il2cpp_codegen_object_new(U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382_il2cpp_TypeInfo_var);
		U3CMessageCountGetJsonStartU3Ec__Iterator7__ctor_m3694852373(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_4(__this);
		U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382 * L_2 = V_0;
		return L_2;
	}
}
// System.Void CurrentScreen::MessageCountGetTakeJson(System.String)
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral65298671;
extern Il2CppCodeGenString* _stringLiteral2619578343;
extern Il2CppCodeGenString* _stringLiteral4223277962;
extern Il2CppCodeGenString* _stringLiteral2158449843;
extern Il2CppCodeGenString* _stringLiteral12303728;
extern Il2CppCodeGenString* _stringLiteral2139735762;
extern const uint32_t CurrentScreen_MessageCountGetTakeJson_m2221092993_MetadataUsageId;
extern "C"  void CurrentScreen_MessageCountGetTakeJson_m2221092993 (CurrentScreen_t3026574117 * __this, String_t* ___js0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_MessageCountGetTakeJson_m2221092993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JsonData_t1715015430 * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		String_t* L_0 = ___js0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		JsonData_t1715015430 * L_1 = JsonMapper_ToObject_m3425498490(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JsonData_t1715015430 * L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		JsonData_t1715015430 * L_3 = V_0;
		NullCheck(L_3);
		JsonData_t1715015430 * L_4 = JsonData_get_Item_m253158020(L_3, 1, /*hidden argument*/NULL);
		NullCheck(L_4);
		JsonData_t1715015430 * L_5 = JsonData_get_Item_m253158020(L_4, 0, /*hidden argument*/NULL);
		NullCheck(L_5);
		JsonData_t1715015430 * L_6 = JsonData_get_Item_m253158020(L_5, 0, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t476798718_il2cpp_TypeInfo_var);
		bool L_8 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		bool L_9 = V_1;
		if (!L_9)
		{
			goto IL_00dd;
		}
	}
	{
		JsonData_t1715015430 * L_10 = V_0;
		NullCheck(L_10);
		JsonData_t1715015430 * L_11 = JsonData_get_Item_m253158020(L_10, 1, /*hidden argument*/NULL);
		NullCheck(L_11);
		JsonData_t1715015430 * L_12 = JsonData_get_Item_m4009629743(L_11, _stringLiteral65298671, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_12);
		int32_t L_14 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		int32_t L_15 = V_2;
		if ((((int32_t)L_15) < ((int32_t)1)))
		{
			goto IL_007a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_16 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_17 = ((int32_t)2);
		Il2CppObject * L_18 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_17);
		NullCheck((Enum_t2862688501 *)L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_18);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, _stringLiteral2619578343, _stringLiteral4223277962, L_19, /*hidden argument*/NULL);
		return;
	}

IL_007a:
	{
		int32_t L_20 = V_2;
		if ((((int32_t)L_20) < ((int32_t)((int32_t)10))))
		{
			goto IL_00a8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_21 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_00a8;
		}
	}
	{
		int32_t L_22 = ((int32_t)2);
		Il2CppObject * L_23 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_22);
		NullCheck((Enum_t2862688501 *)L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_23);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, _stringLiteral2619578343, _stringLiteral4223277962, L_24, /*hidden argument*/NULL);
		return;
	}

IL_00a8:
	{
		int32_t L_25 = V_2;
		V_3 = ((int32_t)((int32_t)((int32_t)10)-(int32_t)L_25));
		String_t* L_26 = Int32_ToString_m1286526384((&V_3), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2158449843, L_26, _stringLiteral12303728, /*hidden argument*/NULL);
		int32_t L_28 = ((int32_t)2);
		Il2CppObject * L_29 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_28);
		NullCheck((Enum_t2862688501 *)L_29);
		String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_29);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, _stringLiteral2619578343, L_27, L_30, /*hidden argument*/NULL);
		goto IL_00f7;
	}

IL_00dd:
	{
		int32_t L_31 = ((int32_t)2);
		Il2CppObject * L_32 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_31);
		NullCheck((Enum_t2862688501 *)L_32);
		String_t* L_33 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_32);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, _stringLiteral2619578343, _stringLiteral2139735762, L_33, /*hidden argument*/NULL);
	}

IL_00f7:
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, 6, /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, 7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CurrentScreen::MessageWriteButton()
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern const uint32_t CurrentScreen_MessageWriteButton_m3529376968_MetadataUsageId;
extern "C"  void CurrentScreen_MessageWriteButton_m3529376968 (CurrentScreen_t3026574117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_MessageWriteButton_m3529376968_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = ((int32_t)3);
		Il2CppObject * L_2 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		int32_t L_4 = ((int32_t)2);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_3, _stringLiteral3660319847, L_6, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		int32_t L_7 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0049;
		}
	}
	{
		Il2CppObject * L_8 = CurrentScreen_MessageCountGetJsonStart_m2449123042(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_8, /*hidden argument*/NULL);
		goto IL_005d;
	}

IL_0049:
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, 6, /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, 7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void CurrentScreen::MemberServiceWriteButton()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_MemberServiceWriteButton_m2401017044_MetadataUsageId;
extern "C"  void CurrentScreen_MemberServiceWriteButton_m2401017044 (CurrentScreen_t3026574117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_MemberServiceWriteButton_m2401017044_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, 8, /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, ((int32_t)9), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, ((int32_t)9), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CurrentScreen::MemberServiceMemberButton()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_MemberServiceMemberButton_m1543428235_MetadataUsageId;
extern "C"  void CurrentScreen_MemberServiceMemberButton_m1543428235 (CurrentScreen_t3026574117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_MemberServiceMemberButton_m1543428235_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, 6, /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, 7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CurrentScreen::MessageRegistrationButton()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_MessageRegistrationButton_m3324319414_MetadataUsageId;
extern "C"  void CurrentScreen_MessageRegistrationButton_m3324319414 (CurrentScreen_t3026574117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_MessageRegistrationButton_m3324319414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, ((int32_t)9), /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, ((int32_t)10), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, ((int32_t)10), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CurrentScreen::Sumabo110Button()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_Sumabo110Button_m1567156227_MetadataUsageId;
extern "C"  void CurrentScreen_Sumabo110Button_m1567156227 (CurrentScreen_t3026574117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_Sumabo110Button_m1567156227_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, ((int32_t)15), /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, ((int32_t)16), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, ((int32_t)16), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CurrentScreen::Sumabo1101Button(System.Int32)
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_Sumabo1101Button_m2428242981_MetadataUsageId;
extern "C"  void CurrentScreen_Sumabo1101Button_m2428242981 (CurrentScreen_t3026574117 * __this, int32_t ___btn0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_Sumabo1101Button_m2428242981_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, ((int32_t)16), /*hidden argument*/NULL);
		int32_t L_0 = ___btn0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0030;
		}
		if (L_1 == 2)
		{
			goto IL_003b;
		}
		if (L_1 == 3)
		{
			goto IL_0046;
		}
	}
	{
		goto IL_0051;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_InquiryTitle_m2236206948(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_InquiryTitle_m2236206948(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_003b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_InquiryTitle_m2236206948(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_InquiryTitle_m2236206948(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_0051:
	{
		goto IL_0056;
	}

IL_0056:
	{
		CurrentScreen_State_m3173002564(__this, ((int32_t)17), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, ((int32_t)17), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CurrentScreen::Sumabo1102Button()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_Sumabo1102Button_m2394907605_MetadataUsageId;
extern "C"  void CurrentScreen_Sumabo1102Button_m2394907605 (CurrentScreen_t3026574117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_Sumabo1102Button_m2394907605_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, ((int32_t)17), /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, ((int32_t)18), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, ((int32_t)18), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CurrentScreen::AccountForgetButton()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_AccountForgetButton_m1160876816_MetadataUsageId;
extern "C"  void CurrentScreen_AccountForgetButton_m1160876816 (CurrentScreen_t3026574117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_AccountForgetButton_m1160876816_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, ((int32_t)21), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_InquiryTitle_m2236206948(NULL /*static, unused*/, 5, /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, ((int32_t)18), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CurrentScreen::EmailRegister()
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern const uint32_t CurrentScreen_EmailRegister_m1311987875_MetadataUsageId;
extern "C"  void CurrentScreen_EmailRegister_m1311987875 (CurrentScreen_t3026574117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_EmailRegister_m1311987875_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = ((int32_t)3);
		Il2CppObject * L_2 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		int32_t L_4 = ((int32_t)2);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_3, _stringLiteral3660319847, L_6, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, ((int32_t)27), /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, ((int32_t)20), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CurrentScreen::onWindowFocusChanged(System.String)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentScreen_onWindowFocusChanged_m3383064281_MetadataUsageId;
extern "C"  void CurrentScreen_onWindowFocusChanged_m3383064281 (CurrentScreen_t3026574117 * __this, String_t* ___hasFocus0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentScreen_onWindowFocusChanged_m3383064281_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		String_t* L_0 = ___hasFocus0;
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t476798718_il2cpp_TypeInfo_var);
		bool L_1 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_0043;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_3 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)7))))
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_4 = CurrentState_get_PopupMessage_m3375877057(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)7))))
		{
			goto IL_0043;
		}
	}
	{
		CurrentScreen_VisibleFalseState_m2603065973(__this, 7, /*hidden argument*/NULL);
		CurrentScreen_State_m3173002564(__this, ((int32_t)50), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		CurrentState_set_PopupMessage_m2237488056(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
// System.Void CurrentScreen/<JsonStart>c__Iterator5::.ctor()
extern "C"  void U3CJsonStartU3Ec__Iterator5__ctor_m3081243089 (U3CJsonStartU3Ec__Iterator5_t1185739994 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object CurrentScreen/<JsonStart>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CJsonStartU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1881566689 (U3CJsonStartU3Ec__Iterator5_t1185739994 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object CurrentScreen/<JsonStart>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CJsonStartU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m747262837 (U3CJsonStartU3Ec__Iterator5_t1185739994 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean CurrentScreen/<JsonStart>c__Iterator5::MoveNext()
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t U3CJsonStartU3Ec__Iterator5_MoveNext_m2900864387_MetadataUsageId;
extern "C"  bool U3CJsonStartU3Ec__Iterator5_MoveNext_m2900864387 (U3CJsonStartU3Ec__Iterator5_t1185739994 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CJsonStartU3Ec__Iterator5_MoveNext_m2900864387_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0066;
		}
	}
	{
		goto IL_00b7;
	}

IL_0021:
	{
		String_t* L_2 = PrivateURL_get_Login_m301514730(NULL /*static, unused*/, /*hidden argument*/NULL);
		WWW_t3134621005 * L_3 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m1985874080(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_0(L_3);
		CurrentScreen_t3026574117 * L_4 = __this->get_U3CU3Ef__this_3();
		CurrentScreen_t3026574117 * L_5 = __this->get_U3CU3Ef__this_3();
		WWW_t3134621005 * L_6 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_5);
		Il2CppObject * L_7 = CurrentScreen_LoginCheckProgress_m1704831024(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		MonoBehaviour_StartCoroutine_m2135303124(L_4, L_7, /*hidden argument*/NULL);
		WWW_t3134621005 * L_8 = __this->get_U3CwwwU3E__0_0();
		__this->set_U24current_2(L_8);
		__this->set_U24PC_1(1);
		goto IL_00b9;
	}

IL_0066:
	{
		WWW_t3134621005 * L_9 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_9);
		String_t* L_10 = WWW_get_error_m1787423074(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0091;
		}
	}
	{
		CurrentScreen_t3026574117 * L_11 = __this->get_U3CU3Ef__this_3();
		WWW_t3134621005 * L_12 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_12);
		String_t* L_13 = WWW_get_text_m4216049525(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		CurrentScreen_TakeJson_m27493301(L_11, L_13, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_0091:
	{
		WWW_t3134621005 * L_14 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_14);
		String_t* L_15 = WWW_get_error_m1787423074(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_16 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		AndroidWebview_Call_m1677830609(L_16, _stringLiteral3202370, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		__this->set_U24PC_1((-1));
	}

IL_00b7:
	{
		return (bool)0;
	}

IL_00b9:
	{
		return (bool)1;
	}
	// Dead block : IL_00bb: ldloc.1
}
// System.Void CurrentScreen/<JsonStart>c__Iterator5::Dispose()
extern "C"  void U3CJsonStartU3Ec__Iterator5_Dispose_m1740424654 (U3CJsonStartU3Ec__Iterator5_t1185739994 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void CurrentScreen/<JsonStart>c__Iterator5::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CJsonStartU3Ec__Iterator5_Reset_m727676030_MetadataUsageId;
extern "C"  void U3CJsonStartU3Ec__Iterator5_Reset_m727676030 (U3CJsonStartU3Ec__Iterator5_t1185739994 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CJsonStartU3Ec__Iterator5_Reset_m727676030_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void CurrentScreen/<LoginCheckProgress>c__Iterator6::.ctor()
extern "C"  void U3CLoginCheckProgressU3Ec__Iterator6__ctor_m2835064534 (U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object CurrentScreen/<LoginCheckProgress>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoginCheckProgressU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2942237702 (U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object CurrentScreen/<LoginCheckProgress>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoginCheckProgressU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m3106261914 (U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean CurrentScreen/<LoginCheckProgress>c__Iterator6::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3529469;
extern Il2CppCodeGenString* _stringLiteral2659940787;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t U3CLoginCheckProgressU3Ec__Iterator6_MoveNext_m3935108166_MetadataUsageId;
extern "C"  bool U3CLoginCheckProgressU3Ec__Iterator6_MoveNext_m3935108166 (U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoginCheckProgressU3Ec__Iterator6_MoveNext_m3935108166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0046;
		}
		if (L_1 == 2)
		{
			goto IL_00ab;
		}
	}
	{
		goto IL_00c1;
	}

IL_0025:
	{
		goto IL_005a;
	}

IL_002a:
	{
		WaitForSeconds_t1615819279 * L_2 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_2, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_2);
		__this->set_U24PC_1(1);
		goto IL_00c3;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_3 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		AndroidWebview_Call_m335597645(L_3, _stringLiteral3529469, _stringLiteral2659940787, /*hidden argument*/NULL);
	}

IL_005a:
	{
		WWW_t3134621005 * L_4 = __this->get_www_0();
		NullCheck(L_4);
		bool L_5 = WWW_get_isDone_m634060017(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002a;
		}
	}
	{
		WWW_t3134621005 * L_6 = __this->get_www_0();
		NullCheck(L_6);
		bool L_7 = WWW_get_isDone_m634060017(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00ba;
		}
	}
	{
		WWW_t3134621005 * L_8 = __this->get_www_0();
		NullCheck(L_8);
		float L_9 = WWW_get_progress_m3186358572(L_8, /*hidden argument*/NULL);
		if ((!(((float)L_9) >= ((float)(1.0f)))))
		{
			goto IL_00ba;
		}
	}
	{
		WaitForSeconds_t1615819279 * L_10 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_10, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_10);
		__this->set_U24PC_1(2);
		goto IL_00c3;
	}

IL_00ab:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_11 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		AndroidWebview_Call_m1677830609(L_11, _stringLiteral3202370, /*hidden argument*/NULL);
	}

IL_00ba:
	{
		__this->set_U24PC_1((-1));
	}

IL_00c1:
	{
		return (bool)0;
	}

IL_00c3:
	{
		return (bool)1;
	}
	// Dead block : IL_00c5: ldloc.1
}
// System.Void CurrentScreen/<LoginCheckProgress>c__Iterator6::Dispose()
extern "C"  void U3CLoginCheckProgressU3Ec__Iterator6_Dispose_m1386034579 (U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void CurrentScreen/<LoginCheckProgress>c__Iterator6::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoginCheckProgressU3Ec__Iterator6_Reset_m481497475_MetadataUsageId;
extern "C"  void U3CLoginCheckProgressU3Ec__Iterator6_Reset_m481497475 (U3CLoginCheckProgressU3Ec__Iterator6_t4282206053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoginCheckProgressU3Ec__Iterator6_Reset_m481497475_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void CurrentScreen/<MessageCountGetJsonStart>c__Iterator7::.ctor()
extern "C"  void U3CMessageCountGetJsonStartU3Ec__Iterator7__ctor_m3694852373 (U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object CurrentScreen/<MessageCountGetJsonStart>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMessageCountGetJsonStartU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2167314279 (U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object CurrentScreen/<MessageCountGetJsonStart>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMessageCountGetJsonStartU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m2704427259 (U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean CurrentScreen/<MessageCountGetJsonStart>c__Iterator7::MoveNext()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t U3CMessageCountGetJsonStartU3Ec__Iterator7_MoveNext_m2945644199_MetadataUsageId;
extern "C"  bool U3CMessageCountGetJsonStartU3Ec__Iterator7_MoveNext_m2945644199 (U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMessageCountGetJsonStartU3Ec__Iterator7_MoveNext_m2945644199_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0090;
		}
	}
	{
		goto IL_00d8;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_U3CurlU3E__0_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_3 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_4 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0051;
		}
	}

IL_0041:
	{
		String_t* L_5 = PrivateURL_get_MessageCount_m59418249(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CurlU3E__0_0(L_5);
		goto IL_0067;
	}

IL_0051:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_6 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_0067;
		}
	}
	{
		String_t* L_7 = PrivateURL_get_MemberMessageCount_m606779331(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CurlU3E__0_0(L_7);
	}

IL_0067:
	{
		String_t* L_8 = __this->get_U3CurlU3E__0_0();
		WWW_t3134621005 * L_9 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m1985874080(L_9, L_8, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__1_1(L_9);
		WWW_t3134621005 * L_10 = __this->get_U3CwwwU3E__1_1();
		__this->set_U24current_3(L_10);
		__this->set_U24PC_2(1);
		goto IL_00da;
	}

IL_0090:
	{
		WWW_t3134621005 * L_11 = __this->get_U3CwwwU3E__1_1();
		NullCheck(L_11);
		String_t* L_12 = WWW_get_error_m1787423074(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_00bb;
		}
	}
	{
		CurrentScreen_t3026574117 * L_13 = __this->get_U3CU3Ef__this_4();
		WWW_t3134621005 * L_14 = __this->get_U3CwwwU3E__1_1();
		NullCheck(L_14);
		String_t* L_15 = WWW_get_text_m4216049525(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		CurrentScreen_MessageCountGetTakeJson_m2221092993(L_13, L_15, /*hidden argument*/NULL);
		goto IL_00d1;
	}

IL_00bb:
	{
		WWW_t3134621005 * L_16 = __this->get_U3CwwwU3E__1_1();
		NullCheck(L_16);
		String_t* L_17 = WWW_get_error_m1787423074(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		CtrlPlugins_Login_m1267639130(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
	}

IL_00d1:
	{
		__this->set_U24PC_2((-1));
	}

IL_00d8:
	{
		return (bool)0;
	}

IL_00da:
	{
		return (bool)1;
	}
	// Dead block : IL_00dc: ldloc.1
}
// System.Void CurrentScreen/<MessageCountGetJsonStart>c__Iterator7::Dispose()
extern "C"  void U3CMessageCountGetJsonStartU3Ec__Iterator7_Dispose_m3008427026 (U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void CurrentScreen/<MessageCountGetJsonStart>c__Iterator7::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CMessageCountGetJsonStartU3Ec__Iterator7_Reset_m1341285314_MetadataUsageId;
extern "C"  void U3CMessageCountGetJsonStartU3Ec__Iterator7_Reset_m1341285314 (U3CMessageCountGetJsonStartU3Ec__Iterator7_t1453453382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMessageCountGetJsonStartU3Ec__Iterator7_Reset_m1341285314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void CurrentState::.ctor()
extern "C"  void CurrentState__ctor_m1475154659 (CurrentState_t1622142648 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CurrentState::.cctor()
extern Il2CppClass* Dictionary_2_t2595600883_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2759097677_MethodInfo_var;
extern const uint32_t CurrentState__cctor_m2298025258_MetadataUsageId;
extern "C"  void CurrentState__cctor_m2298025258 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentState__cctor_m2298025258_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2595600883 * L_0 = (Dictionary_2_t2595600883 *)il2cpp_codegen_object_new(Dictionary_2_t2595600883_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2759097677(L_0, /*hidden argument*/Dictionary_2__ctor_m2759097677_MethodInfo_var);
		((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->set__ScreenState_7(L_0);
		return;
	}
}
// CurrentState/States CurrentState::get_State()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentState_get_State_m3296155471_MetadataUsageId;
extern "C"  int32_t CurrentState_get_State_m3296155471 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentState_get_State_m3296155471_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__State_0();
		return L_0;
	}
}
// System.Void CurrentState::set_State(CurrentState/States)
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentState_set_State_m3490075940_MetadataUsageId;
extern "C"  void CurrentState_set_State_m3490075940 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentState_set_State_m3490075940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__State_0();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->set__State_0(L_2);
	}

IL_0011:
	{
		return;
	}
}
// CurrentState/States CurrentState::get_LoginState()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentState_get_LoginState_m3052003788_MetadataUsageId;
extern "C"  int32_t CurrentState_get_LoginState_m3052003788 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentState_get_LoginState_m3052003788_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__LoginState_1();
		return L_0;
	}
}
// System.Void CurrentState::set_LoginState(CurrentState/States)
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentState_set_LoginState_m3940368275_MetadataUsageId;
extern "C"  void CurrentState_set_LoginState_m3940368275 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentState_set_LoginState_m3940368275_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__LoginState_1();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->set__LoginState_1(L_2);
	}

IL_0011:
	{
		return;
	}
}
// CurrentState/InquiryTitles CurrentState::get_InquiryTitle()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentState_get_InquiryTitle_m3036927725_MetadataUsageId;
extern "C"  int32_t CurrentState_get_InquiryTitle_m3036927725 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentState_get_InquiryTitle_m3036927725_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__InquiryTitle_2();
		return L_0;
	}
}
// System.Void CurrentState::set_InquiryTitle(CurrentState/InquiryTitles)
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentState_set_InquiryTitle_m2236206948_MetadataUsageId;
extern "C"  void CurrentState_set_InquiryTitle_m2236206948 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentState_set_InquiryTitle_m2236206948_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__InquiryTitle_2();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->set__InquiryTitle_2(L_2);
	}

IL_0011:
	{
		return;
	}
}
// CurrentState/GPSPopupMessages CurrentState::get_GPSPopupMessage()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentState_get_GPSPopupMessage_m652265359_MetadataUsageId;
extern "C"  int32_t CurrentState_get_GPSPopupMessage_m652265359 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentState_get_GPSPopupMessage_m652265359_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__GPSPopupMessage_3();
		return L_0;
	}
}
// System.Void CurrentState::set_GPSPopupMessage(CurrentState/GPSPopupMessages)
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentState_set_GPSPopupMessage_m1603270948_MetadataUsageId;
extern "C"  void CurrentState_set_GPSPopupMessage_m1603270948 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentState_set_GPSPopupMessage_m1603270948_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__GPSPopupMessage_3();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->set__GPSPopupMessage_3(L_2);
	}

IL_0011:
	{
		return;
	}
}
// CurrentState/LoginPopupMessages CurrentState::get_LoginPopupMessage()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentState_get_LoginPopupMessage_m464967087_MetadataUsageId;
extern "C"  int32_t CurrentState_get_LoginPopupMessage_m464967087 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentState_get_LoginPopupMessage_m464967087_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__LoginPopupMessage_4();
		return L_0;
	}
}
// System.Void CurrentState::set_LoginPopupMessage(CurrentState/LoginPopupMessages)
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentState_set_LoginPopupMessage_m2175582148_MetadataUsageId;
extern "C"  void CurrentState_set_LoginPopupMessage_m2175582148 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentState_set_LoginPopupMessage_m2175582148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__LoginPopupMessage_4();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->set__LoginPopupMessage_4(L_2);
	}

IL_0011:
	{
		return;
	}
}
// CurrentState/PopupMessages CurrentState::get_PopupMessage()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentState_get_PopupMessage_m3375877057_MetadataUsageId;
extern "C"  int32_t CurrentState_get_PopupMessage_m3375877057 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentState_get_PopupMessage_m3375877057_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__PopupMessage_5();
		return L_0;
	}
}
// System.Void CurrentState::set_PopupMessage(CurrentState/PopupMessages)
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentState_set_PopupMessage_m2237488056_MetadataUsageId;
extern "C"  void CurrentState_set_PopupMessage_m2237488056 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentState_set_PopupMessage_m2237488056_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__PopupMessage_5();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->set__PopupMessage_5(L_2);
	}

IL_0011:
	{
		return;
	}
}
// System.Void CurrentState::AddScreenState(CurrentState/States,UnityEngine.GameObject)
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m3388190864_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m3523611577_MethodInfo_var;
extern const uint32_t CurrentState_AddScreenState_m2795282826_MetadataUsageId;
extern "C"  void CurrentState_AddScreenState_m2795282826 (Il2CppObject * __this /* static, unused */, int32_t ___key0, GameObject_t3674682005 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentState_AddScreenState_m2795282826_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		Dictionary_2_t2595600883 * L_0 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__ScreenState_7();
		int32_t L_1 = ___key0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m3388190864(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m3388190864_MethodInfo_var);
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		Dictionary_2_t2595600883 * L_3 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__ScreenState_7();
		int32_t L_4 = ___key0;
		GameObject_t3674682005 * L_5 = ___value1;
		NullCheck(L_3);
		Dictionary_2_Add_m3523611577(L_3, L_4, L_5, /*hidden argument*/Dictionary_2_Add_m3523611577_MethodInfo_var);
	}

IL_001c:
	{
		return;
	}
}
// UnityEngine.GameObject CurrentState::GetScreenState(CurrentState/States)
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m3388190864_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m555163959_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral534877004;
extern const uint32_t CurrentState_GetScreenState_m3808002068_MetadataUsageId;
extern "C"  GameObject_t3674682005 * CurrentState_GetScreenState_m3808002068 (Il2CppObject * __this /* static, unused */, int32_t ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentState_GetScreenState_m3808002068_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		Dictionary_2_t2595600883 * L_0 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__ScreenState_7();
		int32_t L_1 = ___key0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m3388190864(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m3388190864_MethodInfo_var);
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral534877004, /*hidden argument*/NULL);
		return (GameObject_t3674682005 *)NULL;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		Dictionary_2_t2595600883 * L_3 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__ScreenState_7();
		int32_t L_4 = ___key0;
		NullCheck(L_3);
		GameObject_t3674682005 * L_5 = Dictionary_2_get_Item_m555163959(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m555163959_MethodInfo_var);
		return L_5;
	}
}
// CurrentState/MemberStates CurrentState::get_MemberState()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentState_get_MemberState_m3859431375_MetadataUsageId;
extern "C"  int32_t CurrentState_get_MemberState_m3859431375 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentState_get_MemberState_m3859431375_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__MemberState_6();
		return L_0;
	}
}
// System.Void CurrentState::set_MemberState(CurrentState/MemberStates)
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t CurrentState_set_MemberState_m2378785892_MetadataUsageId;
extern "C"  void CurrentState_set_MemberState_m2378785892 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentState_set_MemberState_m2378785892_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = ((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->get__MemberState_6();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		((CurrentState_t1622142648_StaticFields*)CurrentState_t1622142648_il2cpp_TypeInfo_var->static_fields)->set__MemberState_6(L_2);
	}

IL_0011:
	{
		return;
	}
}
// System.Void EmailChange::.ctor()
extern "C"  void EmailChange__ctor_m1263600287 (EmailChange_t3079654988 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EmailChange::EmailButtonClick(System.String[])
extern "C"  void EmailChange_EmailButtonClick_m1046917441 (EmailChange_t3079654988 * __this, StringU5BU5D_t4054002952* ___value0, const MethodInfo* method)
{
	{
		InputField_t609046876 * L_0 = __this->get_Name_2();
		StringU5BU5D_t4054002952* L_1 = ___value0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		int32_t L_2 = 0;
		String_t* L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_0);
		InputField_set_text_m203843887(L_0, L_3, /*hidden argument*/NULL);
		InputField_t609046876 * L_4 = __this->get_Email_3();
		StringU5BU5D_t4054002952* L_5 = ___value0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		int32_t L_6 = 1;
		String_t* L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_4);
		InputField_set_text_m203843887(L_4, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EmailChange::register()
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern const uint32_t EmailChange_register_m913772168_MetadataUsageId;
extern "C"  void EmailChange_register_m913772168 (EmailChange_t3079654988 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmailChange_register_m913772168_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = ((int32_t)3);
		Il2CppObject * L_2 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		int32_t L_4 = ((int32_t)2);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_3, _stringLiteral3660319847, L_6, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		Il2CppObject * L_7 = EmailChange_RegisterSend_m3888352648(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator EmailChange::RegisterSend()
extern Il2CppClass* U3CRegisterSendU3Ec__Iterator8_t1720518241_il2cpp_TypeInfo_var;
extern const uint32_t EmailChange_RegisterSend_m3888352648_MetadataUsageId;
extern "C"  Il2CppObject * EmailChange_RegisterSend_m3888352648 (EmailChange_t3079654988 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmailChange_RegisterSend_m3888352648_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CRegisterSendU3Ec__Iterator8_t1720518241 * V_0 = NULL;
	{
		U3CRegisterSendU3Ec__Iterator8_t1720518241 * L_0 = (U3CRegisterSendU3Ec__Iterator8_t1720518241 *)il2cpp_codegen_object_new(U3CRegisterSendU3Ec__Iterator8_t1720518241_il2cpp_TypeInfo_var);
		U3CRegisterSendU3Ec__Iterator8__ctor_m4271807322(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRegisterSendU3Ec__Iterator8_t1720518241 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_4(__this);
		U3CRegisterSendU3Ec__Iterator8_t1720518241 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator EmailChange::CheckProgress(UnityEngine.WWW)
extern Il2CppClass* U3CCheckProgressU3Ec__Iterator9_t1393611626_il2cpp_TypeInfo_var;
extern const uint32_t EmailChange_CheckProgress_m3793964824_MetadataUsageId;
extern "C"  Il2CppObject * EmailChange_CheckProgress_m3793964824 (EmailChange_t3079654988 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmailChange_CheckProgress_m3793964824_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CCheckProgressU3Ec__Iterator9_t1393611626 * V_0 = NULL;
	{
		U3CCheckProgressU3Ec__Iterator9_t1393611626 * L_0 = (U3CCheckProgressU3Ec__Iterator9_t1393611626 *)il2cpp_codegen_object_new(U3CCheckProgressU3Ec__Iterator9_t1393611626_il2cpp_TypeInfo_var);
		U3CCheckProgressU3Ec__Iterator9__ctor_m4146656065(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCheckProgressU3Ec__Iterator9_t1393611626 * L_1 = V_0;
		WWW_t3134621005 * L_2 = ___www0;
		NullCheck(L_1);
		L_1->set_www_0(L_2);
		U3CCheckProgressU3Ec__Iterator9_t1393611626 * L_3 = V_0;
		WWW_t3134621005 * L_4 = ___www0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Ewww_3(L_4);
		U3CCheckProgressU3Ec__Iterator9_t1393611626 * L_5 = V_0;
		return L_5;
	}
}
// System.Void EmailChange::EmailDelete()
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern const uint32_t EmailChange_EmailDelete_m1830258020_MetadataUsageId;
extern "C"  void EmailChange_EmailDelete_m1830258020 (EmailChange_t3079654988 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmailChange_EmailDelete_m1830258020_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = ((int32_t)3);
		Il2CppObject * L_2 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		int32_t L_4 = ((int32_t)2);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_3, _stringLiteral3660319847, L_6, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		Il2CppObject * L_7 = EmailChange_RegisterDelete_m3953951531(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator EmailChange::RegisterDelete()
extern Il2CppClass* U3CRegisterDeleteU3Ec__IteratorA_t1809913895_il2cpp_TypeInfo_var;
extern const uint32_t EmailChange_RegisterDelete_m3953951531_MetadataUsageId;
extern "C"  Il2CppObject * EmailChange_RegisterDelete_m3953951531 (EmailChange_t3079654988 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmailChange_RegisterDelete_m3953951531_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CRegisterDeleteU3Ec__IteratorA_t1809913895 * V_0 = NULL;
	{
		U3CRegisterDeleteU3Ec__IteratorA_t1809913895 * L_0 = (U3CRegisterDeleteU3Ec__IteratorA_t1809913895 *)il2cpp_codegen_object_new(U3CRegisterDeleteU3Ec__IteratorA_t1809913895_il2cpp_TypeInfo_var);
		U3CRegisterDeleteU3Ec__IteratorA__ctor_m4184638036(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRegisterDeleteU3Ec__IteratorA_t1809913895 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_4(__this);
		U3CRegisterDeleteU3Ec__IteratorA_t1809913895 * L_2 = V_0;
		return L_2;
	}
}
// System.Void EmailChange/<CheckProgress>c__Iterator9::.ctor()
extern "C"  void U3CCheckProgressU3Ec__Iterator9__ctor_m4146656065 (U3CCheckProgressU3Ec__Iterator9_t1393611626 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object EmailChange/<CheckProgress>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckProgressU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1084189617 (U3CCheckProgressU3Ec__Iterator9_t1393611626 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object EmailChange/<CheckProgress>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckProgressU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m2715349317 (U3CCheckProgressU3Ec__Iterator9_t1393611626 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean EmailChange/<CheckProgress>c__Iterator9::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3529469;
extern Il2CppCodeGenString* _stringLiteral1239105089;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t U3CCheckProgressU3Ec__Iterator9_MoveNext_m1071401747_MetadataUsageId;
extern "C"  bool U3CCheckProgressU3Ec__Iterator9_MoveNext_m1071401747 (U3CCheckProgressU3Ec__Iterator9_t1393611626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckProgressU3Ec__Iterator9_MoveNext_m1071401747_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0046;
		}
		if (L_1 == 2)
		{
			goto IL_00ab;
		}
	}
	{
		goto IL_00c1;
	}

IL_0025:
	{
		goto IL_005a;
	}

IL_002a:
	{
		WaitForSeconds_t1615819279 * L_2 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_2, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_2);
		__this->set_U24PC_1(1);
		goto IL_00c3;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_3 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		AndroidWebview_Call_m335597645(L_3, _stringLiteral3529469, _stringLiteral1239105089, /*hidden argument*/NULL);
	}

IL_005a:
	{
		WWW_t3134621005 * L_4 = __this->get_www_0();
		NullCheck(L_4);
		bool L_5 = WWW_get_isDone_m634060017(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002a;
		}
	}
	{
		WWW_t3134621005 * L_6 = __this->get_www_0();
		NullCheck(L_6);
		bool L_7 = WWW_get_isDone_m634060017(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00ba;
		}
	}
	{
		WWW_t3134621005 * L_8 = __this->get_www_0();
		NullCheck(L_8);
		float L_9 = WWW_get_uploadProgress_m1090826125(L_8, /*hidden argument*/NULL);
		if ((!(((float)L_9) >= ((float)(1.0f)))))
		{
			goto IL_00ba;
		}
	}
	{
		WaitForSeconds_t1615819279 * L_10 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_10, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_10);
		__this->set_U24PC_1(2);
		goto IL_00c3;
	}

IL_00ab:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_11 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		AndroidWebview_Call_m1677830609(L_11, _stringLiteral3202370, /*hidden argument*/NULL);
	}

IL_00ba:
	{
		__this->set_U24PC_1((-1));
	}

IL_00c1:
	{
		return (bool)0;
	}

IL_00c3:
	{
		return (bool)1;
	}
	// Dead block : IL_00c5: ldloc.1
}
// System.Void EmailChange/<CheckProgress>c__Iterator9::Dispose()
extern "C"  void U3CCheckProgressU3Ec__Iterator9_Dispose_m3400078142 (U3CCheckProgressU3Ec__Iterator9_t1393611626 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void EmailChange/<CheckProgress>c__Iterator9::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckProgressU3Ec__Iterator9_Reset_m1793089006_MetadataUsageId;
extern "C"  void U3CCheckProgressU3Ec__Iterator9_Reset_m1793089006 (U3CCheckProgressU3Ec__Iterator9_t1393611626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckProgressU3Ec__Iterator9_Reset_m1793089006_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void EmailChange/<RegisterDelete>c__IteratorA::.ctor()
extern "C"  void U3CRegisterDeleteU3Ec__IteratorA__ctor_m4184638036 (U3CRegisterDeleteU3Ec__IteratorA_t1809913895 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object EmailChange/<RegisterDelete>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRegisterDeleteU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m677748488 (U3CRegisterDeleteU3Ec__IteratorA_t1809913895 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object EmailChange/<RegisterDelete>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRegisterDeleteU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m159822492 (U3CRegisterDeleteU3Ec__IteratorA_t1809913895 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean EmailChange/<RegisterDelete>c__IteratorA::MoveNext()
extern Il2CppClass* WWWForm_t461342257_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentScreen_t3026574117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral110371416;
extern Il2CppCodeGenString* _stringLiteral2522905884;
extern Il2CppCodeGenString* _stringLiteral2604245075;
extern Il2CppCodeGenString* _stringLiteral96619420;
extern Il2CppCodeGenString* _stringLiteral485954921;
extern Il2CppCodeGenString* _stringLiteral665309472;
extern Il2CppCodeGenString* _stringLiteral2740204476;
extern Il2CppCodeGenString* _stringLiteral1979684883;
extern Il2CppCodeGenString* _stringLiteral665322324;
extern Il2CppCodeGenString* _stringLiteral50086220;
extern Il2CppCodeGenString* _stringLiteral3416034720;
extern const uint32_t U3CRegisterDeleteU3Ec__IteratorA_MoveNext_m1790893832_MetadataUsageId;
extern "C"  bool U3CRegisterDeleteU3Ec__IteratorA_MoveNext_m1790893832 (U3CRegisterDeleteU3Ec__IteratorA_t1809913895 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CRegisterDeleteU3Ec__IteratorA_MoveNext_m1790893832_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00e1;
		}
	}
	{
		goto IL_018d;
	}

IL_0021:
	{
		WWWForm_t461342257 * L_2 = (WWWForm_t461342257 *)il2cpp_codegen_object_new(WWWForm_t461342257_il2cpp_TypeInfo_var);
		WWWForm__ctor_m1417930174(L_2, /*hidden argument*/NULL);
		__this->set_U3CformU3E__0_0(L_2);
		WWWForm_t461342257 * L_3 = __this->get_U3CformU3E__0_0();
		EmailChange_t3079654988 * L_4 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_4);
		InputField_t609046876 * L_5 = L_4->get_Name_2();
		NullCheck(L_5);
		String_t* L_6 = InputField_get_text_m3972300732(L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		WWWForm_AddField_m2890504319(L_3, _stringLiteral110371416, L_6, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_7 = __this->get_U3CformU3E__0_0();
		String_t* L_8 = PrivateURL_get_customerId_m4251176570(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		WWWForm_AddField_m2890504319(L_7, _stringLiteral2522905884, L_8, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_9 = __this->get_U3CformU3E__0_0();
		String_t* L_10 = PrivateURL_get_selectmessageid_m1725662023(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		WWWForm_AddField_m2890504319(L_9, _stringLiteral2604245075, L_10, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_11 = __this->get_U3CformU3E__0_0();
		EmailChange_t3079654988 * L_12 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_12);
		InputField_t609046876 * L_13 = L_12->get_Email_3();
		NullCheck(L_13);
		String_t* L_14 = InputField_get_text_m3972300732(L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		WWWForm_AddField_m2890504319(L_11, _stringLiteral96619420, L_14, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_15 = __this->get_U3CformU3E__0_0();
		WWW_t3134621005 * L_16 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m3203953640(L_16, _stringLiteral485954921, L_15, /*hidden argument*/NULL);
		__this->set_U3CwU3E__1_1(L_16);
		EmailChange_t3079654988 * L_17 = __this->get_U3CU3Ef__this_4();
		EmailChange_t3079654988 * L_18 = __this->get_U3CU3Ef__this_4();
		WWW_t3134621005 * L_19 = __this->get_U3CwU3E__1_1();
		NullCheck(L_18);
		Il2CppObject * L_20 = EmailChange_CheckProgress_m3793964824(L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		MonoBehaviour_StartCoroutine_m2135303124(L_17, L_20, /*hidden argument*/NULL);
		WWW_t3134621005 * L_21 = __this->get_U3CwU3E__1_1();
		__this->set_U24current_3(L_21);
		__this->set_U24PC_2(1);
		goto IL_018f;
	}

IL_00e1:
	{
		WWW_t3134621005 * L_22 = __this->get_U3CwU3E__1_1();
		NullCheck(L_22);
		String_t* L_23 = WWW_get_error_m1787423074(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0120;
		}
	}
	{
		WWW_t3134621005 * L_24 = __this->get_U3CwU3E__1_1();
		NullCheck(L_24);
		String_t* L_25 = WWW_get_error_m1787423074(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		int32_t L_26 = ((int32_t)2);
		Il2CppObject * L_27 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_26);
		NullCheck((Enum_t2862688501 *)L_27);
		String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_27);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, _stringLiteral665309472, _stringLiteral2740204476, L_28, /*hidden argument*/NULL);
		goto IL_016e;
	}

IL_0120:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1979684883, /*hidden argument*/NULL);
		EmailChange_t3079654988 * L_29 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_29);
		InputField_t609046876 * L_30 = L_29->get_Name_2();
		NullCheck(L_30);
		String_t* L_31 = InputField_get_text_m3972300732(L_30, /*hidden argument*/NULL);
		EmailChange_t3079654988 * L_32 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_32);
		InputField_t609046876 * L_33 = L_32->get_Email_3();
		NullCheck(L_33);
		String_t* L_34 = InputField_get_text_m3972300732(L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral50086220, L_31, _stringLiteral3416034720, L_34, /*hidden argument*/NULL);
		int32_t L_36 = ((int32_t)2);
		Il2CppObject * L_37 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_36);
		NullCheck((Enum_t2862688501 *)L_37);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_37);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, _stringLiteral665322324, L_35, L_38, /*hidden argument*/NULL);
	}

IL_016e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentScreen_t3026574117_il2cpp_TypeInfo_var);
		CurrentScreen_t3026574117 * L_39 = CurrentScreen_get_Instance_m3161187310(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_39);
		CurrentScreen_VisibleFalseState_m2603065973(L_39, ((int32_t)18), /*hidden argument*/NULL);
		CurrentScreen_t3026574117 * L_40 = CurrentScreen_get_Instance_m3161187310(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_40);
		CurrentScreen_State_m3173002564(L_40, ((int32_t)20), /*hidden argument*/NULL);
		__this->set_U24PC_2((-1));
	}

IL_018d:
	{
		return (bool)0;
	}

IL_018f:
	{
		return (bool)1;
	}
	// Dead block : IL_0191: ldloc.1
}
// System.Void EmailChange/<RegisterDelete>c__IteratorA::Dispose()
extern "C"  void U3CRegisterDeleteU3Ec__IteratorA_Dispose_m1246046609 (U3CRegisterDeleteU3Ec__IteratorA_t1809913895 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void EmailChange/<RegisterDelete>c__IteratorA::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CRegisterDeleteU3Ec__IteratorA_Reset_m1831070977_MetadataUsageId;
extern "C"  void U3CRegisterDeleteU3Ec__IteratorA_Reset_m1831070977 (U3CRegisterDeleteU3Ec__IteratorA_t1809913895 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CRegisterDeleteU3Ec__IteratorA_Reset_m1831070977_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void EmailChange/<RegisterSend>c__Iterator8::.ctor()
extern "C"  void U3CRegisterSendU3Ec__Iterator8__ctor_m4271807322 (U3CRegisterSendU3Ec__Iterator8_t1720518241 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object EmailChange/<RegisterSend>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRegisterSendU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2177685250 (U3CRegisterSendU3Ec__Iterator8_t1720518241 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object EmailChange/<RegisterSend>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRegisterSendU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1363222166 (U3CRegisterSendU3Ec__Iterator8_t1720518241 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean EmailChange/<RegisterSend>c__Iterator8::MoveNext()
extern Il2CppClass* WWWForm_t461342257_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentScreen_t3026574117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral110371416;
extern Il2CppCodeGenString* _stringLiteral2522905884;
extern Il2CppCodeGenString* _stringLiteral2604245075;
extern Il2CppCodeGenString* _stringLiteral96619420;
extern Il2CppCodeGenString* _stringLiteral982567047;
extern Il2CppCodeGenString* _stringLiteral705088145;
extern Il2CppCodeGenString* _stringLiteral812946541;
extern Il2CppCodeGenString* _stringLiteral1979684883;
extern Il2CppCodeGenString* _stringLiteral705100997;
extern Il2CppCodeGenString* _stringLiteral888669090;
extern Il2CppCodeGenString* _stringLiteral3416034720;
extern const uint32_t U3CRegisterSendU3Ec__Iterator8_MoveNext_m2568781122_MetadataUsageId;
extern "C"  bool U3CRegisterSendU3Ec__Iterator8_MoveNext_m2568781122 (U3CRegisterSendU3Ec__Iterator8_t1720518241 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CRegisterSendU3Ec__Iterator8_MoveNext_m2568781122_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00e1;
		}
	}
	{
		goto IL_018d;
	}

IL_0021:
	{
		WWWForm_t461342257 * L_2 = (WWWForm_t461342257 *)il2cpp_codegen_object_new(WWWForm_t461342257_il2cpp_TypeInfo_var);
		WWWForm__ctor_m1417930174(L_2, /*hidden argument*/NULL);
		__this->set_U3CformU3E__0_0(L_2);
		WWWForm_t461342257 * L_3 = __this->get_U3CformU3E__0_0();
		EmailChange_t3079654988 * L_4 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_4);
		InputField_t609046876 * L_5 = L_4->get_Name_2();
		NullCheck(L_5);
		String_t* L_6 = InputField_get_text_m3972300732(L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		WWWForm_AddField_m2890504319(L_3, _stringLiteral110371416, L_6, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_7 = __this->get_U3CformU3E__0_0();
		String_t* L_8 = PrivateURL_get_customerId_m4251176570(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		WWWForm_AddField_m2890504319(L_7, _stringLiteral2522905884, L_8, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_9 = __this->get_U3CformU3E__0_0();
		String_t* L_10 = PrivateURL_get_selectmessageid_m1725662023(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		WWWForm_AddField_m2890504319(L_9, _stringLiteral2604245075, L_10, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_11 = __this->get_U3CformU3E__0_0();
		EmailChange_t3079654988 * L_12 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_12);
		InputField_t609046876 * L_13 = L_12->get_Email_3();
		NullCheck(L_13);
		String_t* L_14 = InputField_get_text_m3972300732(L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		WWWForm_AddField_m2890504319(L_11, _stringLiteral96619420, L_14, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_15 = __this->get_U3CformU3E__0_0();
		WWW_t3134621005 * L_16 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m3203953640(L_16, _stringLiteral982567047, L_15, /*hidden argument*/NULL);
		__this->set_U3CwU3E__1_1(L_16);
		EmailChange_t3079654988 * L_17 = __this->get_U3CU3Ef__this_4();
		EmailChange_t3079654988 * L_18 = __this->get_U3CU3Ef__this_4();
		WWW_t3134621005 * L_19 = __this->get_U3CwU3E__1_1();
		NullCheck(L_18);
		Il2CppObject * L_20 = EmailChange_CheckProgress_m3793964824(L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		MonoBehaviour_StartCoroutine_m2135303124(L_17, L_20, /*hidden argument*/NULL);
		WWW_t3134621005 * L_21 = __this->get_U3CwU3E__1_1();
		__this->set_U24current_3(L_21);
		__this->set_U24PC_2(1);
		goto IL_018f;
	}

IL_00e1:
	{
		WWW_t3134621005 * L_22 = __this->get_U3CwU3E__1_1();
		NullCheck(L_22);
		String_t* L_23 = WWW_get_error_m1787423074(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0120;
		}
	}
	{
		WWW_t3134621005 * L_24 = __this->get_U3CwU3E__1_1();
		NullCheck(L_24);
		String_t* L_25 = WWW_get_error_m1787423074(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		int32_t L_26 = ((int32_t)2);
		Il2CppObject * L_27 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_26);
		NullCheck((Enum_t2862688501 *)L_27);
		String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_27);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, _stringLiteral705088145, _stringLiteral812946541, L_28, /*hidden argument*/NULL);
		goto IL_016e;
	}

IL_0120:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1979684883, /*hidden argument*/NULL);
		EmailChange_t3079654988 * L_29 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_29);
		InputField_t609046876 * L_30 = L_29->get_Name_2();
		NullCheck(L_30);
		String_t* L_31 = InputField_get_text_m3972300732(L_30, /*hidden argument*/NULL);
		EmailChange_t3079654988 * L_32 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_32);
		InputField_t609046876 * L_33 = L_32->get_Email_3();
		NullCheck(L_33);
		String_t* L_34 = InputField_get_text_m3972300732(L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral888669090, L_31, _stringLiteral3416034720, L_34, /*hidden argument*/NULL);
		int32_t L_36 = ((int32_t)2);
		Il2CppObject * L_37 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_36);
		NullCheck((Enum_t2862688501 *)L_37);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_37);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, _stringLiteral705100997, L_35, L_38, /*hidden argument*/NULL);
	}

IL_016e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentScreen_t3026574117_il2cpp_TypeInfo_var);
		CurrentScreen_t3026574117 * L_39 = CurrentScreen_get_Instance_m3161187310(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_39);
		CurrentScreen_VisibleFalseState_m2603065973(L_39, ((int32_t)18), /*hidden argument*/NULL);
		CurrentScreen_t3026574117 * L_40 = CurrentScreen_get_Instance_m3161187310(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_40);
		CurrentScreen_State_m3173002564(L_40, ((int32_t)20), /*hidden argument*/NULL);
		__this->set_U24PC_2((-1));
	}

IL_018d:
	{
		return (bool)0;
	}

IL_018f:
	{
		return (bool)1;
	}
	// Dead block : IL_0191: ldloc.1
}
// System.Void EmailChange/<RegisterSend>c__Iterator8::Dispose()
extern "C"  void U3CRegisterSendU3Ec__Iterator8_Dispose_m3411351831 (U3CRegisterSendU3Ec__Iterator8_t1720518241 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void EmailChange/<RegisterSend>c__Iterator8::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CRegisterSendU3Ec__Iterator8_Reset_m1918240263_MetadataUsageId;
extern "C"  void U3CRegisterSendU3Ec__Iterator8_Reset_m1918240263 (U3CRegisterSendU3Ec__Iterator8_t1720518241 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CRegisterSendU3Ec__Iterator8_Reset_m1918240263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void EmailRecv::.ctor()
extern "C"  void EmailRecv__ctor_m183426089 (EmailRecv_t4124314498 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EmailRecv::OnEnable()
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2001146706;
extern const uint32_t EmailRecv_OnEnable_m3319795389_MetadataUsageId;
extern "C"  void EmailRecv_OnEnable_m3319795389 (EmailRecv_t4124314498 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmailRecv_OnEnable_m3319795389_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		GameObject_t3674682005 * L_0 = __this->get_m_Parent_3();
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = GameObject_get_transform_m1278640159(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = Transform_get_childCount_m2107810675(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) < ((int32_t)1)))
		{
			goto IL_0061;
		}
	}
	{
		V_0 = 0;
		goto IL_004b;
	}

IL_001d:
	{
		GameObject_t3674682005 * L_3 = __this->get_m_Parent_3();
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2001146706, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1659122786 * L_9 = Transform_FindChild_m2149912886(L_4, L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_t3674682005 * L_10 = Component_get_gameObject_m1170635899(L_9, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004b:
	{
		int32_t L_12 = V_0;
		GameObject_t3674682005 * L_13 = __this->get_m_Parent_3();
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = GameObject_get_transform_m1278640159(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = Transform_get_childCount_m2107810675(L_14, /*hidden argument*/NULL);
		if ((((int32_t)L_12) < ((int32_t)L_15)))
		{
			goto IL_001d;
		}
	}

IL_0061:
	{
		Il2CppObject * L_16 = EmailRecv_GetEmailList_m4085222231(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator EmailRecv::GetEmailList()
extern Il2CppClass* U3CGetEmailListU3Ec__IteratorB_t1941116220_il2cpp_TypeInfo_var;
extern const uint32_t EmailRecv_GetEmailList_m4085222231_MetadataUsageId;
extern "C"  Il2CppObject * EmailRecv_GetEmailList_m4085222231 (EmailRecv_t4124314498 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmailRecv_GetEmailList_m4085222231_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetEmailListU3Ec__IteratorB_t1941116220 * V_0 = NULL;
	{
		U3CGetEmailListU3Ec__IteratorB_t1941116220 * L_0 = (U3CGetEmailListU3Ec__IteratorB_t1941116220 *)il2cpp_codegen_object_new(U3CGetEmailListU3Ec__IteratorB_t1941116220_il2cpp_TypeInfo_var);
		U3CGetEmailListU3Ec__IteratorB__ctor_m844904671(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetEmailListU3Ec__IteratorB_t1941116220 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEmailListU3Ec__IteratorB_t1941116220 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator EmailRecv::Progress(UnityEngine.WWW)
extern Il2CppClass* U3CProgressU3Ec__IteratorC_t2850438612_il2cpp_TypeInfo_var;
extern const uint32_t EmailRecv_Progress_m4217691602_MetadataUsageId;
extern "C"  Il2CppObject * EmailRecv_Progress_m4217691602 (EmailRecv_t4124314498 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmailRecv_Progress_m4217691602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CProgressU3Ec__IteratorC_t2850438612 * V_0 = NULL;
	{
		U3CProgressU3Ec__IteratorC_t2850438612 * L_0 = (U3CProgressU3Ec__IteratorC_t2850438612 *)il2cpp_codegen_object_new(U3CProgressU3Ec__IteratorC_t2850438612_il2cpp_TypeInfo_var);
		U3CProgressU3Ec__IteratorC__ctor_m551946055(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CProgressU3Ec__IteratorC_t2850438612 * L_1 = V_0;
		WWW_t3134621005 * L_2 = ___www0;
		NullCheck(L_1);
		L_1->set_www_0(L_2);
		U3CProgressU3Ec__IteratorC_t2850438612 * L_3 = V_0;
		WWW_t3134621005 * L_4 = ___www0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Ewww_3(L_4);
		U3CProgressU3Ec__IteratorC_t2850438612 * L_5 = V_0;
		return L_5;
	}
}
// System.Void EmailRecv::TakeJson(System.String)
extern Il2CppClass* U3CTakeJsonU3Ec__AnonStorey23_t1761236520_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CTakeJsonU3Ec__AnonStorey22_t1761236519_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_t594794173_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponentInChildren_TisText_t9039225_m2924853261_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var;
extern const MethodInfo* U3CTakeJsonU3Ec__AnonStorey22_U3CU3Em__0_m3879286213_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2760346223;
extern Il2CppCodeGenString* _stringLiteral3738031314;
extern Il2CppCodeGenString* _stringLiteral3286045045;
extern Il2CppCodeGenString* _stringLiteral2001146706;
extern Il2CppCodeGenString* _stringLiteral3792159859;
extern Il2CppCodeGenString* _stringLiteral2289459;
extern Il2CppCodeGenString* _stringLiteral110371416;
extern Il2CppCodeGenString* _stringLiteral96619420;
extern const uint32_t EmailRecv_TakeJson_m1240939000_MetadataUsageId;
extern "C"  void EmailRecv_TakeJson_m1240939000 (EmailRecv_t4124314498 * __this, String_t* ___js0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmailRecv_TakeJson_m1240939000_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t3674682005 * V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * V_4 = NULL;
	U3CTakeJsonU3Ec__AnonStorey22_t1761236519 * V_5 = NULL;
	{
		U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * L_0 = (U3CTakeJsonU3Ec__AnonStorey23_t1761236520 *)il2cpp_codegen_object_new(U3CTakeJsonU3Ec__AnonStorey23_t1761236520_il2cpp_TypeInfo_var);
		U3CTakeJsonU3Ec__AnonStorey23__ctor_m1870355011(L_0, /*hidden argument*/NULL);
		V_4 = L_0;
		U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * L_1 = V_4;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * L_2 = V_4;
		String_t* L_3 = ___js0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		JsonData_t1715015430 * L_4 = JsonMapper_ToObject_m3425498490(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_EventJS_0(L_4);
		U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * L_5 = V_4;
		NullCheck(L_5);
		JsonData_t1715015430 * L_6 = L_5->get_EventJS_0();
		if (L_6)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * L_7 = V_4;
		NullCheck(L_7);
		JsonData_t1715015430 * L_8 = L_7->get_EventJS_0();
		NullCheck(L_8);
		JsonData_t1715015430 * L_9 = JsonData_get_Item_m253158020(L_8, 1, /*hidden argument*/NULL);
		NullCheck(L_9);
		JsonData_t1715015430 * L_10 = JsonData_get_Item_m4009629743(L_9, _stringLiteral2760346223, /*hidden argument*/NULL);
		NullCheck(L_10);
		JsonData_t1715015430 * L_11 = JsonData_get_Item_m4009629743(L_10, _stringLiteral3738031314, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_11);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t476798718_il2cpp_TypeInfo_var);
		bool L_13 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_02ae;
		}
	}
	{
		V_0 = 0;
		goto IL_0288;
	}

IL_0060:
	{
		int32_t L_14 = V_0;
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3286045045, L_16, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_18 = GameObject_Find_m332785498(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		GameObject_t3674682005 * L_19 = V_1;
		bool L_20 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_19, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0284;
		}
	}
	{
		U3CTakeJsonU3Ec__AnonStorey22_t1761236519 * L_21 = (U3CTakeJsonU3Ec__AnonStorey22_t1761236519 *)il2cpp_codegen_object_new(U3CTakeJsonU3Ec__AnonStorey22_t1761236519_il2cpp_TypeInfo_var);
		U3CTakeJsonU3Ec__AnonStorey22__ctor_m2066868516(L_21, /*hidden argument*/NULL);
		V_5 = L_21;
		U3CTakeJsonU3Ec__AnonStorey22_t1761236519 * L_22 = V_5;
		U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * L_23 = V_4;
		NullCheck(L_22);
		L_22->set_U3CU3Ef__refU2435_1(L_23);
		U3CTakeJsonU3Ec__AnonStorey22_t1761236519 * L_24 = V_5;
		NullCheck(L_24);
		L_24->set_U3CU3Ef__this_2(__this);
		U3CTakeJsonU3Ec__AnonStorey22_t1761236519 * L_25 = V_5;
		GameObject_t3674682005 * L_26 = __this->get_m_Button_2();
		GameObject_t3674682005 * L_27 = Object_Instantiate_TisGameObject_t3674682005_m3917608929(NULL /*static, unused*/, L_26, /*hidden argument*/Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var);
		NullCheck(L_25);
		L_25->set_Btn_0(L_27);
		U3CTakeJsonU3Ec__AnonStorey22_t1761236519 * L_28 = V_5;
		NullCheck(L_28);
		GameObject_t3674682005 * L_29 = L_28->get_Btn_0();
		int32_t L_30 = V_0;
		int32_t L_31 = L_30;
		Il2CppObject * L_32 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_31);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2001146706, L_32, /*hidden argument*/NULL);
		NullCheck(L_29);
		Object_set_name_m1123518500(L_29, L_33, /*hidden argument*/NULL);
		U3CTakeJsonU3Ec__AnonStorey22_t1761236519 * L_34 = V_5;
		NullCheck(L_34);
		GameObject_t3674682005 * L_35 = L_34->get_Btn_0();
		NullCheck(L_35);
		Transform_t1659122786 * L_36 = GameObject_get_transform_m1278640159(L_35, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_37 = __this->get_m_Parent_3();
		NullCheck(L_37);
		Transform_t1659122786 * L_38 = GameObject_get_transform_m1278640159(L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_SetParent_m3449663462(L_36, L_38, /*hidden argument*/NULL);
		U3CTakeJsonU3Ec__AnonStorey22_t1761236519 * L_39 = V_5;
		NullCheck(L_39);
		GameObject_t3674682005 * L_40 = L_39->get_Btn_0();
		NullCheck(L_40);
		Transform_t1659122786 * L_41 = GameObject_get_transform_m1278640159(L_40, /*hidden argument*/NULL);
		int32_t L_42 = V_0;
		Vector3_t4282066566  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Vector3__ctor_m2926210380(&L_43, (0.0f), ((float)((float)(-110.5f)+(float)(((float)((float)((int32_t)((int32_t)((int32_t)-540)*(int32_t)L_42))))))), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_41);
		Transform_set_localPosition_m3504330903(L_41, L_43, /*hidden argument*/NULL);
		U3CTakeJsonU3Ec__AnonStorey22_t1761236519 * L_44 = V_5;
		NullCheck(L_44);
		GameObject_t3674682005 * L_45 = L_44->get_Btn_0();
		NullCheck(L_45);
		Transform_t1659122786 * L_46 = GameObject_get_transform_m1278640159(L_45, /*hidden argument*/NULL);
		Vector3_t4282066566  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Vector3__ctor_m2926210380(&L_47, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_46);
		Transform_set_localScale_m310756934(L_46, L_47, /*hidden argument*/NULL);
		U3CTakeJsonU3Ec__AnonStorey22_t1761236519 * L_48 = V_5;
		NullCheck(L_48);
		GameObject_t3674682005 * L_49 = L_48->get_Btn_0();
		NullCheck(L_49);
		RectTransform_t972643934 * L_50 = GameObject_GetComponent_TisRectTransform_t972643934_m406276429(L_49, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var);
		Vector2_t4282066565  L_51 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_50);
		RectTransform_set_offsetMax_m677885815(L_50, L_51, /*hidden argument*/NULL);
		U3CTakeJsonU3Ec__AnonStorey22_t1761236519 * L_52 = V_5;
		NullCheck(L_52);
		GameObject_t3674682005 * L_53 = L_52->get_Btn_0();
		NullCheck(L_53);
		RectTransform_t972643934 * L_54 = GameObject_GetComponent_TisRectTransform_t972643934_m406276429(L_53, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var);
		Vector2_t4282066565  L_55;
		memset(&L_55, 0, sizeof(L_55));
		Vector2__ctor_m1517109030(&L_55, (0.0f), (221.0f), /*hidden argument*/NULL);
		NullCheck(L_54);
		RectTransform_set_sizeDelta_m1223846609(L_54, L_55, /*hidden argument*/NULL);
		String_t* L_56 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_2 = L_56;
		String_t* L_57 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_3 = L_57;
		U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * L_58 = V_4;
		NullCheck(L_58);
		JsonData_t1715015430 * L_59 = L_58->get_EventJS_0();
		NullCheck(L_59);
		JsonData_t1715015430 * L_60 = JsonData_get_Item_m4009629743(L_59, _stringLiteral3792159859, /*hidden argument*/NULL);
		NullCheck(L_60);
		JsonData_t1715015430 * L_61 = JsonData_get_Item_m4009629743(L_60, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_62 = V_0;
		NullCheck(L_61);
		JsonData_t1715015430 * L_63 = JsonData_get_Item_m253158020(L_61, L_62, /*hidden argument*/NULL);
		NullCheck(L_63);
		JsonData_t1715015430 * L_64 = JsonData_get_Item_m4009629743(L_63, _stringLiteral110371416, /*hidden argument*/NULL);
		NullCheck(L_64);
		String_t* L_65 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_64);
		bool L_66 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		if (L_66)
		{
			goto IL_01e4;
		}
	}
	{
		U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * L_67 = V_4;
		NullCheck(L_67);
		JsonData_t1715015430 * L_68 = L_67->get_EventJS_0();
		NullCheck(L_68);
		JsonData_t1715015430 * L_69 = JsonData_get_Item_m4009629743(L_68, _stringLiteral3792159859, /*hidden argument*/NULL);
		NullCheck(L_69);
		JsonData_t1715015430 * L_70 = JsonData_get_Item_m4009629743(L_69, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_71 = V_0;
		NullCheck(L_70);
		JsonData_t1715015430 * L_72 = JsonData_get_Item_m253158020(L_70, L_71, /*hidden argument*/NULL);
		NullCheck(L_72);
		JsonData_t1715015430 * L_73 = JsonData_get_Item_m4009629743(L_72, _stringLiteral110371416, /*hidden argument*/NULL);
		NullCheck(L_73);
		String_t* L_74 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_73);
		V_2 = L_74;
	}

IL_01e4:
	{
		U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * L_75 = V_4;
		NullCheck(L_75);
		JsonData_t1715015430 * L_76 = L_75->get_EventJS_0();
		NullCheck(L_76);
		JsonData_t1715015430 * L_77 = JsonData_get_Item_m4009629743(L_76, _stringLiteral3792159859, /*hidden argument*/NULL);
		NullCheck(L_77);
		JsonData_t1715015430 * L_78 = JsonData_get_Item_m4009629743(L_77, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_79 = V_0;
		NullCheck(L_78);
		JsonData_t1715015430 * L_80 = JsonData_get_Item_m253158020(L_78, L_79, /*hidden argument*/NULL);
		NullCheck(L_80);
		JsonData_t1715015430 * L_81 = JsonData_get_Item_m4009629743(L_80, _stringLiteral96619420, /*hidden argument*/NULL);
		NullCheck(L_81);
		String_t* L_82 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_81);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_83 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_82, /*hidden argument*/NULL);
		if (L_83)
		{
			goto IL_024f;
		}
	}
	{
		U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * L_84 = V_4;
		NullCheck(L_84);
		JsonData_t1715015430 * L_85 = L_84->get_EventJS_0();
		NullCheck(L_85);
		JsonData_t1715015430 * L_86 = JsonData_get_Item_m4009629743(L_85, _stringLiteral3792159859, /*hidden argument*/NULL);
		NullCheck(L_86);
		JsonData_t1715015430 * L_87 = JsonData_get_Item_m4009629743(L_86, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_88 = V_0;
		NullCheck(L_87);
		JsonData_t1715015430 * L_89 = JsonData_get_Item_m253158020(L_87, L_88, /*hidden argument*/NULL);
		NullCheck(L_89);
		JsonData_t1715015430 * L_90 = JsonData_get_Item_m4009629743(L_89, _stringLiteral96619420, /*hidden argument*/NULL);
		NullCheck(L_90);
		String_t* L_91 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_90);
		V_3 = L_91;
	}

IL_024f:
	{
		U3CTakeJsonU3Ec__AnonStorey22_t1761236519 * L_92 = V_5;
		NullCheck(L_92);
		GameObject_t3674682005 * L_93 = L_92->get_Btn_0();
		NullCheck(L_93);
		Text_t9039225 * L_94 = GameObject_GetComponentInChildren_TisText_t9039225_m2924853261(L_93, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t9039225_m2924853261_MethodInfo_var);
		String_t* L_95 = V_2;
		NullCheck(L_94);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_94, L_95);
		U3CTakeJsonU3Ec__AnonStorey22_t1761236519 * L_96 = V_5;
		NullCheck(L_96);
		GameObject_t3674682005 * L_97 = L_96->get_Btn_0();
		NullCheck(L_97);
		Button_t3896396478 * L_98 = GameObject_GetComponent_TisButton_t3896396478_m2812094092(L_97, /*hidden argument*/GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var);
		NullCheck(L_98);
		ButtonClickedEvent_t2796375743 * L_99 = Button_get_onClick_m1145127631(L_98, /*hidden argument*/NULL);
		U3CTakeJsonU3Ec__AnonStorey22_t1761236519 * L_100 = V_5;
		IntPtr_t L_101;
		L_101.set_m_value_0((void*)(void*)U3CTakeJsonU3Ec__AnonStorey22_U3CU3Em__0_m3879286213_MethodInfo_var);
		UnityAction_t594794173 * L_102 = (UnityAction_t594794173 *)il2cpp_codegen_object_new(UnityAction_t594794173_il2cpp_TypeInfo_var);
		UnityAction__ctor_m4130179243(L_102, L_100, L_101, /*hidden argument*/NULL);
		NullCheck(L_99);
		UnityEvent_AddListener_m4099140869(L_99, L_102, /*hidden argument*/NULL);
	}

IL_0284:
	{
		int32_t L_103 = V_0;
		V_0 = ((int32_t)((int32_t)L_103+(int32_t)1));
	}

IL_0288:
	{
		int32_t L_104 = V_0;
		U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * L_105 = V_4;
		NullCheck(L_105);
		JsonData_t1715015430 * L_106 = L_105->get_EventJS_0();
		NullCheck(L_106);
		JsonData_t1715015430 * L_107 = JsonData_get_Item_m4009629743(L_106, _stringLiteral3792159859, /*hidden argument*/NULL);
		NullCheck(L_107);
		JsonData_t1715015430 * L_108 = JsonData_get_Item_m4009629743(L_107, _stringLiteral2289459, /*hidden argument*/NULL);
		NullCheck(L_108);
		int32_t L_109 = JsonData_get_Count_m412158079(L_108, /*hidden argument*/NULL);
		if ((((int32_t)L_104) < ((int32_t)L_109)))
		{
			goto IL_0060;
		}
	}

IL_02ae:
	{
		return;
	}
}
// System.Void EmailRecv/<GetEmailList>c__IteratorB::.ctor()
extern "C"  void U3CGetEmailListU3Ec__IteratorB__ctor_m844904671 (U3CGetEmailListU3Ec__IteratorB_t1941116220 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object EmailRecv/<GetEmailList>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetEmailListU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2337549277 (U3CGetEmailListU3Ec__IteratorB_t1941116220 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object EmailRecv/<GetEmailList>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEmailListU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m3616246129 (U3CGetEmailListU3Ec__IteratorB_t1941116220 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean EmailRecv/<GetEmailList>c__IteratorB::MoveNext()
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t U3CGetEmailListU3Ec__IteratorB_MoveNext_m646779549_MetadataUsageId;
extern "C"  bool U3CGetEmailListU3Ec__IteratorB_MoveNext_m646779549 (U3CGetEmailListU3Ec__IteratorB_t1941116220 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEmailListU3Ec__IteratorB_MoveNext_m646779549_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0066;
		}
	}
	{
		goto IL_00b7;
	}

IL_0021:
	{
		String_t* L_2 = PrivateURL_get_EmailRecvUpdate_m298749708(NULL /*static, unused*/, /*hidden argument*/NULL);
		WWW_t3134621005 * L_3 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m1985874080(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_0(L_3);
		EmailRecv_t4124314498 * L_4 = __this->get_U3CU3Ef__this_3();
		EmailRecv_t4124314498 * L_5 = __this->get_U3CU3Ef__this_3();
		WWW_t3134621005 * L_6 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_5);
		Il2CppObject * L_7 = EmailRecv_Progress_m4217691602(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		MonoBehaviour_StartCoroutine_m2135303124(L_4, L_7, /*hidden argument*/NULL);
		WWW_t3134621005 * L_8 = __this->get_U3CwwwU3E__0_0();
		__this->set_U24current_2(L_8);
		__this->set_U24PC_1(1);
		goto IL_00b9;
	}

IL_0066:
	{
		WWW_t3134621005 * L_9 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_9);
		String_t* L_10 = WWW_get_error_m1787423074(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0091;
		}
	}
	{
		EmailRecv_t4124314498 * L_11 = __this->get_U3CU3Ef__this_3();
		WWW_t3134621005 * L_12 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_12);
		String_t* L_13 = WWW_get_text_m4216049525(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		EmailRecv_TakeJson_m1240939000(L_11, L_13, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_0091:
	{
		WWW_t3134621005 * L_14 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_14);
		String_t* L_15 = WWW_get_error_m1787423074(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_16 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		AndroidWebview_Call_m1677830609(L_16, _stringLiteral3202370, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		__this->set_U24PC_1((-1));
	}

IL_00b7:
	{
		return (bool)0;
	}

IL_00b9:
	{
		return (bool)1;
	}
	// Dead block : IL_00bb: ldloc.1
}
// System.Void EmailRecv/<GetEmailList>c__IteratorB::Dispose()
extern "C"  void U3CGetEmailListU3Ec__IteratorB_Dispose_m102852956 (U3CGetEmailListU3Ec__IteratorB_t1941116220 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void EmailRecv/<GetEmailList>c__IteratorB::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEmailListU3Ec__IteratorB_Reset_m2786304908_MetadataUsageId;
extern "C"  void U3CGetEmailListU3Ec__IteratorB_Reset_m2786304908 (U3CGetEmailListU3Ec__IteratorB_t1941116220 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEmailListU3Ec__IteratorB_Reset_m2786304908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void EmailRecv/<Progress>c__IteratorC::.ctor()
extern "C"  void U3CProgressU3Ec__IteratorC__ctor_m551946055 (U3CProgressU3Ec__IteratorC_t2850438612 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object EmailRecv/<Progress>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CProgressU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m197824757 (U3CProgressU3Ec__IteratorC_t2850438612 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object EmailRecv/<Progress>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CProgressU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m4024376969 (U3CProgressU3Ec__IteratorC_t2850438612 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean EmailRecv/<Progress>c__IteratorC::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3529469;
extern Il2CppCodeGenString* _stringLiteral1800556493;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t U3CProgressU3Ec__IteratorC_MoveNext_m833045813_MetadataUsageId;
extern "C"  bool U3CProgressU3Ec__IteratorC_MoveNext_m833045813 (U3CProgressU3Ec__IteratorC_t2850438612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CProgressU3Ec__IteratorC_MoveNext_m833045813_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0046;
		}
		if (L_1 == 2)
		{
			goto IL_00ab;
		}
	}
	{
		goto IL_00c1;
	}

IL_0025:
	{
		goto IL_005a;
	}

IL_002a:
	{
		WaitForSeconds_t1615819279 * L_2 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_2, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_2);
		__this->set_U24PC_1(1);
		goto IL_00c3;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_3 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		AndroidWebview_Call_m335597645(L_3, _stringLiteral3529469, _stringLiteral1800556493, /*hidden argument*/NULL);
	}

IL_005a:
	{
		WWW_t3134621005 * L_4 = __this->get_www_0();
		NullCheck(L_4);
		bool L_5 = WWW_get_isDone_m634060017(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002a;
		}
	}
	{
		WWW_t3134621005 * L_6 = __this->get_www_0();
		NullCheck(L_6);
		bool L_7 = WWW_get_isDone_m634060017(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00ba;
		}
	}
	{
		WWW_t3134621005 * L_8 = __this->get_www_0();
		NullCheck(L_8);
		float L_9 = WWW_get_progress_m3186358572(L_8, /*hidden argument*/NULL);
		if ((!(((float)L_9) >= ((float)(1.0f)))))
		{
			goto IL_00ba;
		}
	}
	{
		WaitForSeconds_t1615819279 * L_10 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_10, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_10);
		__this->set_U24PC_1(2);
		goto IL_00c3;
	}

IL_00ab:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_11 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		AndroidWebview_Call_m1677830609(L_11, _stringLiteral3202370, /*hidden argument*/NULL);
	}

IL_00ba:
	{
		__this->set_U24PC_1((-1));
	}

IL_00c1:
	{
		return (bool)0;
	}

IL_00c3:
	{
		return (bool)1;
	}
	// Dead block : IL_00c5: ldloc.1
}
// System.Void EmailRecv/<Progress>c__IteratorC::Dispose()
extern "C"  void U3CProgressU3Ec__IteratorC_Dispose_m2037464516 (U3CProgressU3Ec__IteratorC_t2850438612 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void EmailRecv/<Progress>c__IteratorC::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CProgressU3Ec__IteratorC_Reset_m2493346292_MetadataUsageId;
extern "C"  void U3CProgressU3Ec__IteratorC_Reset_m2493346292 (U3CProgressU3Ec__IteratorC_t2850438612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CProgressU3Ec__IteratorC_Reset_m2493346292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void EmailRecv/<TakeJson>c__AnonStorey22::.ctor()
extern "C"  void U3CTakeJsonU3Ec__AnonStorey22__ctor_m2066868516 (U3CTakeJsonU3Ec__AnonStorey22_t1761236519 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EmailRecv/<TakeJson>c__AnonStorey22::<>m__0()
extern Il2CppClass* CurrentScreen_t3026574117_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral624758289;
extern Il2CppCodeGenString* _stringLiteral3792159859;
extern Il2CppCodeGenString* _stringLiteral2289459;
extern Il2CppCodeGenString* _stringLiteral110371416;
extern Il2CppCodeGenString* _stringLiteral96619420;
extern Il2CppCodeGenString* _stringLiteral2943542362;
extern const uint32_t U3CTakeJsonU3Ec__AnonStorey22_U3CU3Em__0_m3879286213_MetadataUsageId;
extern "C"  void U3CTakeJsonU3Ec__AnonStorey22_U3CU3Em__0_m3879286213 (U3CTakeJsonU3Ec__AnonStorey22_t1761236519 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTakeJsonU3Ec__AnonStorey22_U3CU3Em__0_m3879286213_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	StringU5BU5D_t4054002952* V_4 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentScreen_t3026574117_il2cpp_TypeInfo_var);
		CurrentScreen_t3026574117 * L_0 = CurrentScreen_get_Instance_m3161187310(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		CurrentScreen_VisibleFalseState_m2603065973(L_0, ((int32_t)20), /*hidden argument*/NULL);
		CurrentScreen_t3026574117 * L_1 = CurrentScreen_get_Instance_m3161187310(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		CurrentScreen_State_m3173002564(L_1, ((int32_t)18), /*hidden argument*/NULL);
		EmailRecv_t4124314498 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = L_2->get_m_TargetGO_4();
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0048;
		}
	}
	{
		EmailRecv_t4124314498 * L_5 = __this->get_U3CU3Ef__this_2();
		GameObject_t3674682005 * L_6 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral624758289, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_t3674682005 * L_7 = GameObject_get_gameObject_m1966529385(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->set_m_TargetGO_4(L_7);
	}

IL_0048:
	{
		GameObject_t3674682005 * L_8 = __this->get_Btn_0();
		NullCheck(L_8);
		Button_t3896396478 * L_9 = GameObject_GetComponent_TisButton_t3896396478_m2812094092(L_8, /*hidden argument*/GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var);
		NullCheck(L_9);
		String_t* L_10 = Object_get_name_m3709440845(L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		String_t* L_11 = V_0;
		NullCheck(L_11);
		String_t* L_12 = String_Substring_m2809233063(L_11, 6, /*hidden argument*/NULL);
		int32_t L_13 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * L_14 = __this->get_U3CU3Ef__refU2435_1();
		NullCheck(L_14);
		JsonData_t1715015430 * L_15 = L_14->get_EventJS_0();
		NullCheck(L_15);
		JsonData_t1715015430 * L_16 = JsonData_get_Item_m4009629743(L_15, _stringLiteral3792159859, /*hidden argument*/NULL);
		NullCheck(L_16);
		JsonData_t1715015430 * L_17 = JsonData_get_Item_m4009629743(L_16, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_18 = V_1;
		NullCheck(L_17);
		JsonData_t1715015430 * L_19 = JsonData_get_Item_m253158020(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		JsonData_t1715015430 * L_20 = JsonData_get_Item_m4009629743(L_19, _stringLiteral110371416, /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_20);
		V_2 = L_21;
		U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * L_22 = __this->get_U3CU3Ef__refU2435_1();
		NullCheck(L_22);
		JsonData_t1715015430 * L_23 = L_22->get_EventJS_0();
		NullCheck(L_23);
		JsonData_t1715015430 * L_24 = JsonData_get_Item_m4009629743(L_23, _stringLiteral3792159859, /*hidden argument*/NULL);
		NullCheck(L_24);
		JsonData_t1715015430 * L_25 = JsonData_get_Item_m4009629743(L_24, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_26 = V_1;
		NullCheck(L_25);
		JsonData_t1715015430 * L_27 = JsonData_get_Item_m253158020(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		JsonData_t1715015430 * L_28 = JsonData_get_Item_m4009629743(L_27, _stringLiteral96619420, /*hidden argument*/NULL);
		NullCheck(L_28);
		String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_28);
		V_3 = L_29;
		StringU5BU5D_t4054002952* L_30 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)2));
		String_t* L_31 = V_2;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		ArrayElementTypeCheck (L_30, L_31);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_31);
		StringU5BU5D_t4054002952* L_32 = L_30;
		String_t* L_33 = V_3;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 1);
		ArrayElementTypeCheck (L_32, L_33);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_33);
		V_4 = L_32;
		EmailRecv_t4124314498 * L_34 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_34);
		GameObject_t3674682005 * L_35 = L_34->get_m_TargetGO_4();
		StringU5BU5D_t4054002952* L_36 = V_4;
		NullCheck(L_35);
		GameObject_SendMessage_m423373689(L_35, _stringLiteral2943542362, (Il2CppObject *)(Il2CppObject *)L_36, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EmailRecv/<TakeJson>c__AnonStorey23::.ctor()
extern "C"  void U3CTakeJsonU3Ec__AnonStorey23__ctor_m1870355011 (U3CTakeJsonU3Ec__AnonStorey23_t1761236520 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ErrorMessage::.ctor()
extern "C"  void ErrorMessage__ctor_m3943981180 (ErrorMessage_t1367556351 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ErrorMessage::.cctor()
extern "C"  void ErrorMessage__cctor_m1522236081 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ErrorMessage::Start()
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisErrorMessage_t1367556351_m3524973971_MethodInfo_var;
extern const MethodInfo* AndroidJavaObject_GetStatic_TisAndroidJavaObject_t2362096582_m524545334_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2952365502;
extern Il2CppCodeGenString* _stringLiteral597527464;
extern const uint32_t ErrorMessage_Start_m2891118972_MetadataUsageId;
extern "C"  void ErrorMessage_Start_m2891118972 (ErrorMessage_t1367556351 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ErrorMessage_Start_m2891118972_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AndroidJavaClass_t1816259147 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_t1367556351 * L_0 = ((ErrorMessage_t1367556351_StaticFields*)ErrorMessage_t1367556351_il2cpp_TypeInfo_var->static_fields)->get_EM_2();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		GameObject_t3674682005 * L_2 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		ErrorMessage_t1367556351 * L_3 = GameObject_AddComponent_TisErrorMessage_t1367556351_m3524973971(L_2, /*hidden argument*/GameObject_AddComponent_TisErrorMessage_t1367556351_m3524973971_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		((ErrorMessage_t1367556351_StaticFields*)ErrorMessage_t1367556351_il2cpp_TypeInfo_var->static_fields)->set_EM_2(L_3);
	}

IL_0020:
	{
		int32_t L_4 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0047;
		}
	}
	{
		AndroidJavaClass_t1816259147 * L_5 = (AndroidJavaClass_t1816259147 *)il2cpp_codegen_object_new(AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m2757518396(L_5, _stringLiteral2952365502, /*hidden argument*/NULL);
		V_0 = L_5;
		AndroidJavaClass_t1816259147 * L_6 = V_0;
		NullCheck(L_6);
		AndroidJavaObject_t2362096582 * L_7 = AndroidJavaObject_GetStatic_TisAndroidJavaObject_t2362096582_m524545334(L_6, _stringLiteral597527464, /*hidden argument*/AndroidJavaObject_GetStatic_TisAndroidJavaObject_t2362096582_m524545334_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		((ErrorMessage_t1367556351_StaticFields*)ErrorMessage_t1367556351_il2cpp_TypeInfo_var->static_fields)->set_activity_4(L_7);
	}

IL_0047:
	{
		return;
	}
}
// System.Boolean ErrorMessage::get_PopupON()
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern const uint32_t ErrorMessage_get_PopupON_m360300232_MetadataUsageId;
extern "C"  bool ErrorMessage_get_PopupON_m360300232 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ErrorMessage_get_PopupON_m360300232_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		bool L_0 = ((ErrorMessage_t1367556351_StaticFields*)ErrorMessage_t1367556351_il2cpp_TypeInfo_var->static_fields)->get__PopupON_3();
		return L_0;
	}
}
// System.Void ErrorMessage::AndroidAlertDialog(System.String,System.String,System.String)
extern Il2CppClass* U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaRunnable_t1289602340_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CAndroidAlertDialogU3Ec__AnonStorey24_U3CU3Em__1_m3418336283_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2455245224;
extern const uint32_t ErrorMessage_AndroidAlertDialog_m3506572413_MetadataUsageId;
extern "C"  void ErrorMessage_AndroidAlertDialog_m3506572413 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___button2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ErrorMessage_AndroidAlertDialog_m3506572413_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220 * V_0 = NULL;
	{
		U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220 * L_0 = (U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220 *)il2cpp_codegen_object_new(U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220_il2cpp_TypeInfo_var);
		U3CAndroidAlertDialogU3Ec__AnonStorey24__ctor_m527978479(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220 * L_1 = V_0;
		String_t* L_2 = ___title0;
		NullCheck(L_1);
		L_1->set_title_0(L_2);
		U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220 * L_3 = V_0;
		String_t* L_4 = ___message1;
		NullCheck(L_3);
		L_3->set_message_1(L_4);
		U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220 * L_5 = V_0;
		String_t* L_6 = ___button2;
		NullCheck(L_5);
		L_5->set_button_2(L_6);
		int32_t L_7 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0051;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		((ErrorMessage_t1367556351_StaticFields*)ErrorMessage_t1367556351_il2cpp_TypeInfo_var->static_fields)->set__PopupON_3((bool)1);
		AndroidJavaObject_t2362096582 * L_8 = ((ErrorMessage_t1367556351_StaticFields*)ErrorMessage_t1367556351_il2cpp_TypeInfo_var->static_fields)->get_activity_4();
		ObjectU5BU5D_t1108656482* L_9 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220 * L_10 = V_0;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)U3CAndroidAlertDialogU3Ec__AnonStorey24_U3CU3Em__1_m3418336283_MethodInfo_var);
		AndroidJavaRunnable_t1289602340 * L_12 = (AndroidJavaRunnable_t1289602340 *)il2cpp_codegen_object_new(AndroidJavaRunnable_t1289602340_il2cpp_TypeInfo_var);
		AndroidJavaRunnable__ctor_m1140889495(L_12, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_12);
		NullCheck(L_8);
		AndroidJavaObject_Call_m3151096341(L_8, _stringLiteral2455245224, L_9, /*hidden argument*/NULL);
	}

IL_0051:
	{
		return;
	}
}
// System.Void ErrorMessage::AndroidAlertDialog(System.Int32,System.String,System.String,System.String)
extern Il2CppClass* U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaRunnable_t1289602340_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CAndroidAlertDialogU3Ec__AnonStorey25_U3CU3Em__2_m1621385885_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2455245224;
extern const uint32_t ErrorMessage_AndroidAlertDialog_m297139618_MetadataUsageId;
extern "C"  void ErrorMessage_AndroidAlertDialog_m297139618 (Il2CppObject * __this /* static, unused */, int32_t ___index0, String_t* ___title1, String_t* ___message2, String_t* ___button3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ErrorMessage_AndroidAlertDialog_m297139618_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221 * V_0 = NULL;
	{
		U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221 * L_0 = (U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221 *)il2cpp_codegen_object_new(U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221_il2cpp_TypeInfo_var);
		U3CAndroidAlertDialogU3Ec__AnonStorey25__ctor_m331464974(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221 * L_1 = V_0;
		String_t* L_2 = ___title1;
		NullCheck(L_1);
		L_1->set_title_0(L_2);
		U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221 * L_3 = V_0;
		String_t* L_4 = ___message2;
		NullCheck(L_3);
		L_3->set_message_1(L_4);
		U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221 * L_5 = V_0;
		int32_t L_6 = ___index0;
		NullCheck(L_5);
		L_5->set_index_2(L_6);
		U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221 * L_7 = V_0;
		String_t* L_8 = ___button3;
		NullCheck(L_7);
		L_7->set_button_3(L_8);
		int32_t L_9 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0058;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		((ErrorMessage_t1367556351_StaticFields*)ErrorMessage_t1367556351_il2cpp_TypeInfo_var->static_fields)->set__PopupON_3((bool)1);
		AndroidJavaObject_t2362096582 * L_10 = ((ErrorMessage_t1367556351_StaticFields*)ErrorMessage_t1367556351_il2cpp_TypeInfo_var->static_fields)->get_activity_4();
		ObjectU5BU5D_t1108656482* L_11 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221 * L_12 = V_0;
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)U3CAndroidAlertDialogU3Ec__AnonStorey25_U3CU3Em__2_m1621385885_MethodInfo_var);
		AndroidJavaRunnable_t1289602340 * L_14 = (AndroidJavaRunnable_t1289602340 *)il2cpp_codegen_object_new(AndroidJavaRunnable_t1289602340_il2cpp_TypeInfo_var);
		AndroidJavaRunnable__ctor_m1140889495(L_14, L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_14);
		NullCheck(L_10);
		AndroidJavaObject_Call_m3151096341(L_10, _stringLiteral2455245224, L_11, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void ErrorMessage/<AndroidAlertDialog>c__AnonStorey24::.ctor()
extern "C"  void U3CAndroidAlertDialogU3Ec__AnonStorey24__ctor_m527978479 (U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ErrorMessage/<AndroidAlertDialog>c__AnonStorey24::<>m__1()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var;
extern Il2CppClass* PositiveButtonListner_t2125458946_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const MethodInfo* AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2283373231;
extern Il2CppCodeGenString* _stringLiteral1405084438;
extern Il2CppCodeGenString* _stringLiteral3928590661;
extern Il2CppCodeGenString* _stringLiteral843105165;
extern Il2CppCodeGenString* _stringLiteral2942673148;
extern Il2CppCodeGenString* _stringLiteral3529469;
extern Il2CppCodeGenString* _stringLiteral3148187872;
extern const uint32_t U3CAndroidAlertDialogU3Ec__AnonStorey24_U3CU3Em__1_m3418336283_MetadataUsageId;
extern "C"  void U3CAndroidAlertDialogU3Ec__AnonStorey24_U3CU3Em__1_m3418336283 (U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAndroidAlertDialogU3Ec__AnonStorey24_U3CU3Em__1_m3418336283_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AndroidJavaObject_t2362096582 * V_0 = NULL;
	AndroidJavaObject_t2362096582 * V_1 = NULL;
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		AndroidJavaObject_t2362096582 * L_1 = ((ErrorMessage_t1367556351_StaticFields*)ErrorMessage_t1367556351_il2cpp_TypeInfo_var->static_fields)->get_activity_4();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		AndroidJavaObject_t2362096582 * L_2 = (AndroidJavaObject_t2362096582 *)il2cpp_codegen_object_new(AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var);
		AndroidJavaObject__ctor_m219376901(L_2, _stringLiteral2283373231, L_0, /*hidden argument*/NULL);
		V_0 = L_2;
		AndroidJavaObject_t2362096582 * L_3 = V_0;
		ObjectU5BU5D_t1108656482* L_4 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_5 = __this->get_title_0();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		NullCheck(L_3);
		AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009(L_3, _stringLiteral1405084438, L_4, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009_MethodInfo_var);
		AndroidJavaObject_t2362096582 * L_6 = V_0;
		ObjectU5BU5D_t1108656482* L_7 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_8 = __this->get_message_1();
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_8);
		NullCheck(L_6);
		AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009(L_6, _stringLiteral3928590661, L_7, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009_MethodInfo_var);
		AndroidJavaObject_t2362096582 * L_9 = V_0;
		ObjectU5BU5D_t1108656482* L_10 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		String_t* L_11 = __this->get_button_2();
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_10;
		ErrorMessage_t1367556351 * L_13 = ((ErrorMessage_t1367556351_StaticFields*)ErrorMessage_t1367556351_il2cpp_TypeInfo_var->static_fields)->get_EM_2();
		PositiveButtonListner_t2125458946 * L_14 = (PositiveButtonListner_t2125458946 *)il2cpp_codegen_object_new(PositiveButtonListner_t2125458946_il2cpp_TypeInfo_var);
		PositiveButtonListner__ctor_m3471104954(L_14, L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_14);
		NullCheck(L_9);
		AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009(L_9, _stringLiteral843105165, L_12, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009_MethodInfo_var);
		AndroidJavaObject_t2362096582 * L_15 = V_0;
		NullCheck(L_15);
		AndroidJavaObject_t2362096582 * L_16 = AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009(L_15, _stringLiteral2942673148, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009_MethodInfo_var);
		V_1 = L_16;
		AndroidJavaObject_t2362096582 * L_17 = V_1;
		NullCheck(L_17);
		AndroidJavaObject_Call_m3151096341(L_17, _stringLiteral3529469, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		AndroidJavaObject_t2362096582 * L_18 = V_1;
		ObjectU5BU5D_t1108656482* L_19 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		bool L_20 = ((bool)0);
		Il2CppObject * L_21 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 0);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_21);
		NullCheck(L_18);
		AndroidJavaObject_Call_m3151096341(L_18, _stringLiteral3148187872, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ErrorMessage/<AndroidAlertDialog>c__AnonStorey25::.ctor()
extern "C"  void U3CAndroidAlertDialogU3Ec__AnonStorey25__ctor_m331464974 (U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ErrorMessage/<AndroidAlertDialog>c__AnonStorey25::<>m__2()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var;
extern Il2CppClass* NegativeButtonListner_t2426404166_il2cpp_TypeInfo_var;
extern Il2CppClass* PositiveButtonListner_t2125458946_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const MethodInfo* AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2283373231;
extern Il2CppCodeGenString* _stringLiteral1405084438;
extern Il2CppCodeGenString* _stringLiteral3928590661;
extern Il2CppCodeGenString* _stringLiteral435001545;
extern Il2CppCodeGenString* _stringLiteral3303336493;
extern Il2CppCodeGenString* _stringLiteral12269512;
extern Il2CppCodeGenString* _stringLiteral843105165;
extern Il2CppCodeGenString* _stringLiteral2942673148;
extern Il2CppCodeGenString* _stringLiteral3529469;
extern Il2CppCodeGenString* _stringLiteral3148187872;
extern const uint32_t U3CAndroidAlertDialogU3Ec__AnonStorey25_U3CU3Em__2_m1621385885_MetadataUsageId;
extern "C"  void U3CAndroidAlertDialogU3Ec__AnonStorey25_U3CU3Em__2_m1621385885 (U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAndroidAlertDialogU3Ec__AnonStorey25_U3CU3Em__2_m1621385885_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AndroidJavaObject_t2362096582 * V_0 = NULL;
	AndroidJavaObject_t2362096582 * V_1 = NULL;
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		AndroidJavaObject_t2362096582 * L_1 = ((ErrorMessage_t1367556351_StaticFields*)ErrorMessage_t1367556351_il2cpp_TypeInfo_var->static_fields)->get_activity_4();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		AndroidJavaObject_t2362096582 * L_2 = (AndroidJavaObject_t2362096582 *)il2cpp_codegen_object_new(AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var);
		AndroidJavaObject__ctor_m219376901(L_2, _stringLiteral2283373231, L_0, /*hidden argument*/NULL);
		V_0 = L_2;
		AndroidJavaObject_t2362096582 * L_3 = V_0;
		ObjectU5BU5D_t1108656482* L_4 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_5 = __this->get_title_0();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		NullCheck(L_3);
		AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009(L_3, _stringLiteral1405084438, L_4, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009_MethodInfo_var);
		AndroidJavaObject_t2362096582 * L_6 = V_0;
		ObjectU5BU5D_t1108656482* L_7 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_8 = __this->get_message_1();
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_8);
		NullCheck(L_6);
		AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009(L_6, _stringLiteral3928590661, L_7, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009_MethodInfo_var);
		int32_t L_9 = __this->get_index_2();
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_0087;
		}
	}
	{
		AndroidJavaObject_t2362096582 * L_10 = V_0;
		ObjectU5BU5D_t1108656482* L_11 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, _stringLiteral3303336493);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3303336493);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_t1367556351 * L_13 = ((ErrorMessage_t1367556351_StaticFields*)ErrorMessage_t1367556351_il2cpp_TypeInfo_var->static_fields)->get_EM_2();
		NegativeButtonListner_t2426404166 * L_14 = (NegativeButtonListner_t2426404166 *)il2cpp_codegen_object_new(NegativeButtonListner_t2426404166_il2cpp_TypeInfo_var);
		NegativeButtonListner__ctor_m3832135414(L_14, L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_14);
		NullCheck(L_10);
		AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009(L_10, _stringLiteral435001545, L_12, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009_MethodInfo_var);
		goto IL_00ba;
	}

IL_0087:
	{
		int32_t L_15 = __this->get_index_2();
		if ((!(((uint32_t)L_15) == ((uint32_t)2))))
		{
			goto IL_00ba;
		}
	}
	{
		AndroidJavaObject_t2362096582 * L_16 = V_0;
		ObjectU5BU5D_t1108656482* L_17 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		ArrayElementTypeCheck (L_17, _stringLiteral12269512);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral12269512);
		ObjectU5BU5D_t1108656482* L_18 = L_17;
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_t1367556351 * L_19 = ((ErrorMessage_t1367556351_StaticFields*)ErrorMessage_t1367556351_il2cpp_TypeInfo_var->static_fields)->get_EM_2();
		NegativeButtonListner_t2426404166 * L_20 = (NegativeButtonListner_t2426404166 *)il2cpp_codegen_object_new(NegativeButtonListner_t2426404166_il2cpp_TypeInfo_var);
		NegativeButtonListner__ctor_m3832135414(L_20, L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 1);
		ArrayElementTypeCheck (L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_20);
		NullCheck(L_16);
		AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009(L_16, _stringLiteral435001545, L_18, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009_MethodInfo_var);
	}

IL_00ba:
	{
		AndroidJavaObject_t2362096582 * L_21 = V_0;
		ObjectU5BU5D_t1108656482* L_22 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		String_t* L_23 = __this->get_button_3();
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 0);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_23);
		ObjectU5BU5D_t1108656482* L_24 = L_22;
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_t1367556351 * L_25 = ((ErrorMessage_t1367556351_StaticFields*)ErrorMessage_t1367556351_il2cpp_TypeInfo_var->static_fields)->get_EM_2();
		PositiveButtonListner_t2125458946 * L_26 = (PositiveButtonListner_t2125458946 *)il2cpp_codegen_object_new(PositiveButtonListner_t2125458946_il2cpp_TypeInfo_var);
		PositiveButtonListner__ctor_m3471104954(L_26, L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 1);
		ArrayElementTypeCheck (L_24, L_26);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_26);
		NullCheck(L_21);
		AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009(L_21, _stringLiteral843105165, L_24, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009_MethodInfo_var);
		AndroidJavaObject_t2362096582 * L_27 = V_0;
		NullCheck(L_27);
		AndroidJavaObject_t2362096582 * L_28 = AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009(L_27, _stringLiteral2942673148, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009_MethodInfo_var);
		V_1 = L_28;
		AndroidJavaObject_t2362096582 * L_29 = V_1;
		NullCheck(L_29);
		AndroidJavaObject_Call_m3151096341(L_29, _stringLiteral3529469, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		AndroidJavaObject_t2362096582 * L_30 = V_1;
		ObjectU5BU5D_t1108656482* L_31 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		bool L_32 = ((bool)0);
		Il2CppObject * L_33 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_32);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 0);
		ArrayElementTypeCheck (L_31, L_33);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_33);
		NullCheck(L_30);
		AndroidJavaObject_Call_m3151096341(L_30, _stringLiteral3148187872, L_31, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ErrorMessage/NegativeButtonListner::.ctor(ErrorMessage)
extern Il2CppCodeGenString* _stringLiteral3619134134;
extern const uint32_t NegativeButtonListner__ctor_m3832135414_MetadataUsageId;
extern "C"  void NegativeButtonListner__ctor_m3832135414 (NegativeButtonListner_t2426404166 * __this, ErrorMessage_t1367556351 * ___d0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NegativeButtonListner__ctor_m3832135414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AndroidJavaProxy__ctor_m2087176370(__this, _stringLiteral3619134134, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ErrorMessage/NegativeButtonListner::onClick(UnityEngine.AndroidJavaObject,System.Int32)
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1671672458;
extern const uint32_t NegativeButtonListner_onClick_m1966294682_MetadataUsageId;
extern "C"  void NegativeButtonListner_onClick_m1966294682 (NegativeButtonListner_t2426404166 * __this, AndroidJavaObject_t2362096582 * ___obj0, int32_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NegativeButtonListner_onClick_m1966294682_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_1 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		AndroidJavaObject_t2362096582 * L_2 = ___obj0;
		NullCheck(L_2);
		AndroidJavaObject_Call_m3151096341(L_2, _stringLiteral1671672458, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_3 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_0044;
		}
	}
	{
		AndroidJavaObject_t2362096582 * L_4 = ___obj0;
		NullCheck(L_4);
		AndroidJavaObject_Call_m3151096341(L_4, _stringLiteral1671672458, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_0044:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		((ErrorMessage_t1367556351_StaticFields*)ErrorMessage_t1367556351_il2cpp_TypeInfo_var->static_fields)->set__PopupON_3((bool)0);
		return;
	}
}
// System.Void ErrorMessage/PositiveButtonListner::.ctor(ErrorMessage)
extern Il2CppCodeGenString* _stringLiteral3619134134;
extern const uint32_t PositiveButtonListner__ctor_m3471104954_MetadataUsageId;
extern "C"  void PositiveButtonListner__ctor_m3471104954 (PositiveButtonListner_t2125458946 * __this, ErrorMessage_t1367556351 * ___d0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PositiveButtonListner__ctor_m3471104954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AndroidJavaProxy__ctor_m2087176370(__this, _stringLiteral3619134134, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ErrorMessage/PositiveButtonListner::onClick(UnityEngine.AndroidJavaObject,System.Int32)
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1565688087;
extern Il2CppCodeGenString* _stringLiteral1474523267;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral3146986849;
extern Il2CppCodeGenString* _stringLiteral1490037686;
extern const uint32_t PositiveButtonListner_onClick_m2775208798_MetadataUsageId;
extern "C"  void PositiveButtonListner_onClick_m2775208798 (PositiveButtonListner_t2125458946 * __this, AndroidJavaObject_t2362096582 * ___obj0, int32_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PositiveButtonListner_onClick_m2775208798_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	double V_1 = 0.0;
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_00ca;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_1 = CurrentState_get_GPSPopupMessage_m652265359(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		AndroidJavaObject_t2362096582 * L_2 = ((ErrorMessage_t1367556351_StaticFields*)ErrorMessage_t1367556351_il2cpp_TypeInfo_var->static_fields)->get_activity_4();
		NullCheck(L_2);
		AndroidJavaObject_Call_m3151096341(L_2, _stringLiteral1565688087, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_3 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0047;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_4 = CurrentState_get_PopupMessage_m3375877057(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_0047;
		}
	}
	{
		Application_Quit_m1187862186(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0047:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_5 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)7))))
		{
			goto IL_0097;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_6 = CurrentState_get_PopupMessage_m3375877057(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0097;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_7 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		double L_8 = GPSManager_get__latitude_m2672992213(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_8;
		String_t* L_9 = Double_ToString_m3380246633((&V_0), /*hidden argument*/NULL);
		double L_10 = GPSManager_get__longitude_m385823368(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_10;
		String_t* L_11 = Double_ToString_m3380246633((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m1825781833(NULL /*static, unused*/, L_9, _stringLiteral47, L_11, /*hidden argument*/NULL);
		NullCheck(L_7);
		AndroidWebview_Call_m335597645(L_7, _stringLiteral1474523267, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_PopupMessage_m2237488056(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_0097:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_13 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_00c4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_14 = CurrentState_get_PopupMessage_m3375877057(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_00c4;
		}
	}
	{
		GameObject_t3674682005 * L_15 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3146986849, /*hidden argument*/NULL);
		NullCheck(L_15);
		GameObject_SendMessage_m3923684423(L_15, _stringLiteral1490037686, 1, /*hidden argument*/NULL);
	}

IL_00c4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		((ErrorMessage_t1367556351_StaticFields*)ErrorMessage_t1367556351_il2cpp_TypeInfo_var->static_fields)->set__PopupON_3((bool)0);
	}

IL_00ca:
	{
		return;
	}
}
// System.Void FPSCounter::.ctor()
extern "C"  void FPSCounter__ctor_m645128456 (FPSCounter_t1808341235 * __this, const MethodInfo* method)
{
	{
		__this->set_frequency_2((0.5f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 FPSCounter::get_FramesPerSec()
extern "C"  int32_t FPSCounter_get_FramesPerSec_m2683539469 (FPSCounter_t1808341235 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CFramesPerSecU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void FPSCounter::set_FramesPerSec(System.Int32)
extern "C"  void FPSCounter_set_FramesPerSec_m1406923972 (FPSCounter_t1808341235 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CFramesPerSecU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void FPSCounter::Start()
extern "C"  void FPSCounter_Start_m3887233544 (FPSCounter_t1808341235 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = FPSCounter_FPS_m1661540551(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator FPSCounter::FPS()
extern Il2CppClass* U3CFPSU3Ec__Iterator0_t1063757940_il2cpp_TypeInfo_var;
extern const uint32_t FPSCounter_FPS_m1661540551_MetadataUsageId;
extern "C"  Il2CppObject * FPSCounter_FPS_m1661540551 (FPSCounter_t1808341235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FPSCounter_FPS_m1661540551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CFPSU3Ec__Iterator0_t1063757940 * V_0 = NULL;
	{
		U3CFPSU3Ec__Iterator0_t1063757940 * L_0 = (U3CFPSU3Ec__Iterator0_t1063757940 *)il2cpp_codegen_object_new(U3CFPSU3Ec__Iterator0_t1063757940_il2cpp_TypeInfo_var);
		U3CFPSU3Ec__Iterator0__ctor_m463297703(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFPSU3Ec__Iterator0_t1063757940 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_6(__this);
		U3CFPSU3Ec__Iterator0_t1063757940 * L_2 = V_0;
		return L_2;
	}
}
// System.Void FPSCounter/<FPS>c__Iterator0::.ctor()
extern "C"  void U3CFPSU3Ec__Iterator0__ctor_m463297703 (U3CFPSU3Ec__Iterator0_t1063757940 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object FPSCounter/<FPS>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFPSU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m713234965 (U3CFPSU3Ec__Iterator0_t1063757940 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object FPSCounter/<FPS>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFPSU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3054364585 (U3CFPSU3Ec__Iterator0_t1063757940 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Boolean FPSCounter/<FPS>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTMP_Text_t980027659_m3910039766_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1054921;
extern const uint32_t U3CFPSU3Ec__Iterator0_MoveNext_m2845568469_MetadataUsageId;
extern "C"  bool U3CFPSU3Ec__Iterator0_MoveNext_m2845568469 (U3CFPSU3Ec__Iterator0_t1063757940 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFPSU3Ec__Iterator0_MoveNext_m2845568469_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_005e;
		}
	}
	{
		goto IL_00d9;
	}

IL_0021:
	{
		goto IL_00cd;
	}

IL_0026:
	{
		int32_t L_2 = Time_get_frameCount_m3434184975(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3ClastFrameCountU3E__0_0(L_2);
		float L_3 = Time_get_realtimeSinceStartup_m2972554983(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3ClastTimeU3E__1_1(L_3);
		FPSCounter_t1808341235 * L_4 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_4);
		float L_5 = L_4->get_frequency_2();
		WaitForSeconds_t1615819279 * L_6 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_6, L_5, /*hidden argument*/NULL);
		__this->set_U24current_5(L_6);
		__this->set_U24PC_4(1);
		goto IL_00db;
	}

IL_005e:
	{
		float L_7 = Time_get_realtimeSinceStartup_m2972554983(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = __this->get_U3ClastTimeU3E__1_1();
		__this->set_U3CtimeSpanU3E__2_2(((float)((float)L_7-(float)L_8)));
		int32_t L_9 = Time_get_frameCount_m3434184975(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_U3ClastFrameCountU3E__0_0();
		__this->set_U3CframeCountU3E__3_3(((int32_t)((int32_t)L_9-(int32_t)L_10)));
		FPSCounter_t1808341235 * L_11 = __this->get_U3CU3Ef__this_6();
		int32_t L_12 = __this->get_U3CframeCountU3E__3_3();
		float L_13 = __this->get_U3CtimeSpanU3E__2_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_14 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)(((float)((float)L_12)))/(float)L_13)), /*hidden argument*/NULL);
		NullCheck(L_11);
		FPSCounter_set_FramesPerSec_m1406923972(L_11, L_14, /*hidden argument*/NULL);
		FPSCounter_t1808341235 * L_15 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_15);
		TMP_Text_t980027659 * L_16 = Component_GetComponent_TisTMP_Text_t980027659_m3910039766(L_15, /*hidden argument*/Component_GetComponent_TisTMP_Text_t980027659_m3910039766_MethodInfo_var);
		FPSCounter_t1808341235 * L_17 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_17);
		int32_t L_18 = FPSCounter_get_FramesPerSec_m2683539469(L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		String_t* L_19 = Int32_ToString_m1286526384((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m138640077(NULL /*static, unused*/, L_19, _stringLiteral1054921, /*hidden argument*/NULL);
		NullCheck(L_16);
		TMP_Text_set_text_m27929696(L_16, L_20, /*hidden argument*/NULL);
	}

IL_00cd:
	{
		goto IL_0026;
	}
	// Dead block : IL_00d2: ldarg.0

IL_00d9:
	{
		return (bool)0;
	}

IL_00db:
	{
		return (bool)1;
	}
}
// System.Void FPSCounter/<FPS>c__Iterator0::Dispose()
extern "C"  void U3CFPSU3Ec__Iterator0_Dispose_m2745744164 (U3CFPSU3Ec__Iterator0_t1063757940 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void FPSCounter/<FPS>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CFPSU3Ec__Iterator0_Reset_m2404697940_MetadataUsageId;
extern "C"  void U3CFPSU3Ec__Iterator0_Reset_m2404697940 (U3CFPSU3Ec__Iterator0_t1063757940 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFPSU3Ec__Iterator0_Reset_m2404697940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GPSManager::.ctor()
extern "C"  void GPSManager__ctor_m739500088 (GPSManager_t1489257411 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Double GPSManager::get__latitude()
extern Il2CppClass* GPSManager_t1489257411_il2cpp_TypeInfo_var;
extern const uint32_t GPSManager_get__latitude_m2672992213_MetadataUsageId;
extern "C"  double GPSManager_get__latitude_m2672992213 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPSManager_get__latitude_m2672992213_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->get_latitude_4();
		return L_0;
	}
}
// System.Double GPSManager::get__longitude()
extern Il2CppClass* GPSManager_t1489257411_il2cpp_TypeInfo_var;
extern const uint32_t GPSManager_get__longitude_m385823368_MetadataUsageId;
extern "C"  double GPSManager_get__longitude_m385823368 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPSManager_get__longitude_m385823368_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->get_longitude_5();
		return L_0;
	}
}
// System.Double GPSManager::get__distance()
extern Il2CppClass* GPSManager_t1489257411_il2cpp_TypeInfo_var;
extern const uint32_t GPSManager_get__distance_m1549713694_MetadataUsageId;
extern "C"  double GPSManager_get__distance_m1549713694 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPSManager_get__distance_m1549713694_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->get_distance_6();
		return L_0;
	}
}
// System.Double GPSManager::get__TargetLatitude()
extern Il2CppClass* GPSManager_t1489257411_il2cpp_TypeInfo_var;
extern const uint32_t GPSManager_get__TargetLatitude_m846758502_MetadataUsageId;
extern "C"  double GPSManager_get__TargetLatitude_m846758502 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPSManager_get__TargetLatitude_m846758502_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->get_TargetLatitude_7();
		return L_0;
	}
}
// System.Void GPSManager::set__TargetLatitude(System.Double)
extern Il2CppClass* GPSManager_t1489257411_il2cpp_TypeInfo_var;
extern const uint32_t GPSManager_set__TargetLatitude_m470540013_MetadataUsageId;
extern "C"  void GPSManager_set__TargetLatitude_m470540013 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPSManager_set__TargetLatitude_m470540013_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->get_TargetLatitude_7();
		double L_1 = ___value0;
		if ((((double)L_0) == ((double)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		double L_2 = ___value0;
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_TargetLatitude_7(L_2);
	}

IL_0011:
	{
		return;
	}
}
// System.Double GPSManager::get__TargetLongitude()
extern Il2CppClass* GPSManager_t1489257411_il2cpp_TypeInfo_var;
extern const uint32_t GPSManager_get__TargetLongitude_m3902120471_MetadataUsageId;
extern "C"  double GPSManager_get__TargetLongitude_m3902120471 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPSManager_get__TargetLongitude_m3902120471_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->get_TargetLongitude_8();
		return L_0;
	}
}
// System.Void GPSManager::set__TargetLongitude(System.Double)
extern Il2CppClass* GPSManager_t1489257411_il2cpp_TypeInfo_var;
extern const uint32_t GPSManager_set__TargetLongitude_m2588667994_MetadataUsageId;
extern "C"  void GPSManager_set__TargetLongitude_m2588667994 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPSManager_set__TargetLongitude_m2588667994_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->get_TargetLongitude_8();
		double L_1 = ___value0;
		if ((((double)L_0) == ((double)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		double L_2 = ___value0;
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_TargetLongitude_8(L_2);
	}

IL_0011:
	{
		return;
	}
}
// System.Collections.IEnumerator GPSManager::Start()
extern Il2CppClass* U3CStartU3Ec__IteratorD_t1827712127_il2cpp_TypeInfo_var;
extern const uint32_t GPSManager_Start_m3100476912_MetadataUsageId;
extern "C"  Il2CppObject * GPSManager_Start_m3100476912 (GPSManager_t1489257411 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPSManager_Start_m3100476912_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CStartU3Ec__IteratorD_t1827712127 * V_0 = NULL;
	{
		U3CStartU3Ec__IteratorD_t1827712127 * L_0 = (U3CStartU3Ec__IteratorD_t1827712127 *)il2cpp_codegen_object_new(U3CStartU3Ec__IteratorD_t1827712127_il2cpp_TypeInfo_var);
		U3CStartU3Ec__IteratorD__ctor_m142249724(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__IteratorD_t1827712127 * L_1 = V_0;
		return L_1;
	}
}
// System.Collections.IEnumerator GPSManager::OnApplicationPause(System.Boolean)
extern Il2CppClass* U3COnApplicationPauseU3Ec__IteratorE_t330808093_il2cpp_TypeInfo_var;
extern const uint32_t GPSManager_OnApplicationPause_m827286544_MetadataUsageId;
extern "C"  Il2CppObject * GPSManager_OnApplicationPause_m827286544 (GPSManager_t1489257411 * __this, bool ___pauseState0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPSManager_OnApplicationPause_m827286544_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3COnApplicationPauseU3Ec__IteratorE_t330808093 * V_0 = NULL;
	{
		U3COnApplicationPauseU3Ec__IteratorE_t330808093 * L_0 = (U3COnApplicationPauseU3Ec__IteratorE_t330808093 *)il2cpp_codegen_object_new(U3COnApplicationPauseU3Ec__IteratorE_t330808093_il2cpp_TypeInfo_var);
		U3COnApplicationPauseU3Ec__IteratorE__ctor_m3397538030(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3COnApplicationPauseU3Ec__IteratorE_t330808093 * L_1 = V_0;
		bool L_2 = ___pauseState0;
		NullCheck(L_1);
		L_1->set_pauseState_0(L_2);
		U3COnApplicationPauseU3Ec__IteratorE_t330808093 * L_3 = V_0;
		bool L_4 = ___pauseState0;
		NullCheck(L_3);
		L_3->set_U3CU24U3EpauseState_4(L_4);
		U3COnApplicationPauseU3Ec__IteratorE_t330808093 * L_5 = V_0;
		return L_5;
	}
}
// System.Single GPSManager::Haversine(System.Single&,System.Single&)
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t GPSManager_Haversine_m430118693_MetadataUsageId;
extern "C"  float GPSManager_Haversine_m430118693 (GPSManager_t1489257411 * __this, float* ___lastLatitude0, float* ___lastLongitude1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPSManager_Haversine_m430118693_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	LocationInfo_t3215517959  V_6;
	memset(&V_6, 0, sizeof(V_6));
	LocationInfo_t3215517959  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_0 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		LocationInfo_t3215517959  L_1 = LocationService_get_lastData_m2812407679(L_0, /*hidden argument*/NULL);
		V_6 = L_1;
		float L_2 = LocationInfo_get_latitude_m1803811355((&V_6), /*hidden argument*/NULL);
		V_0 = L_2;
		LocationService_t3853025142 * L_3 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		LocationInfo_t3215517959  L_4 = LocationService_get_lastData_m2812407679(L_3, /*hidden argument*/NULL);
		V_7 = L_4;
		float L_5 = LocationInfo_get_longitude_m3505987842((&V_7), /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = V_0;
		float* L_7 = ___lastLatitude0;
		V_2 = ((float)((float)((float)((float)L_6-(float)(*((float*)L_7))))*(float)(0.0174532924f)));
		float L_8 = V_1;
		float* L_9 = ___lastLongitude1;
		V_3 = ((float)((float)((float)((float)L_8-(float)(*((float*)L_9))))*(float)(0.0174532924f)));
		float L_10 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_11 = sinf(((float)((float)L_10/(float)(2.0f))));
		float L_12 = powf(L_11, (2.0f));
		float* L_13 = ___lastLatitude0;
		float L_14 = cosf(((float)((float)(*((float*)L_13))*(float)(0.0174532924f))));
		float L_15 = V_0;
		float L_16 = cosf(((float)((float)L_15*(float)(0.0174532924f))));
		float L_17 = V_3;
		float L_18 = sinf(((float)((float)L_17/(float)(2.0f))));
		float L_19 = powf(L_18, (2.0f));
		V_4 = ((float)((float)L_12+(float)((float)((float)((float)((float)L_14*(float)L_16))*(float)L_19))));
		float* L_20 = ___lastLatitude0;
		float L_21 = V_0;
		*((float*)(L_20)) = (float)L_21;
		float* L_22 = ___lastLongitude1;
		float L_23 = V_1;
		*((float*)(L_22)) = (float)L_23;
		float L_24 = V_4;
		float L_25 = sqrtf(L_24);
		float L_26 = V_4;
		float L_27 = sqrtf(((float)((float)(1.0f)-(float)L_26)));
		float L_28 = atan2f(L_25, L_27);
		V_5 = ((float)((float)(2.0f)*(float)L_28));
		float L_29 = V_5;
		return ((float)((float)(6371.0f)*(float)L_29));
	}
}
// System.Double GPSManager::Distance(System.Double,System.Double,System.Double,System.Double)
extern "C"  double GPSManager_Distance_m4256732498 (GPSManager_t1489257411 * __this, double ___lat10, double ___lon11, double ___lat22, double ___lon23, const MethodInfo* method)
{
	double V_0 = 0.0;
	double V_1 = 0.0;
	{
		double L_0 = ___lon11;
		double L_1 = ___lon23;
		V_0 = ((double)((double)L_0-(double)L_1));
		double L_2 = ___lat10;
		double L_3 = GPSManager_deg2rad_m276033958(__this, L_2, /*hidden argument*/NULL);
		double L_4 = sin(L_3);
		double L_5 = ___lat22;
		double L_6 = GPSManager_deg2rad_m276033958(__this, L_5, /*hidden argument*/NULL);
		double L_7 = sin(L_6);
		double L_8 = ___lat10;
		double L_9 = GPSManager_deg2rad_m276033958(__this, L_8, /*hidden argument*/NULL);
		double L_10 = cos(L_9);
		double L_11 = ___lat22;
		double L_12 = GPSManager_deg2rad_m276033958(__this, L_11, /*hidden argument*/NULL);
		double L_13 = cos(L_12);
		double L_14 = V_0;
		double L_15 = GPSManager_deg2rad_m276033958(__this, L_14, /*hidden argument*/NULL);
		double L_16 = cos(L_15);
		V_1 = ((double)((double)((double)((double)L_4*(double)L_7))+(double)((double)((double)((double)((double)L_10*(double)L_13))*(double)L_16))));
		double L_17 = V_1;
		double L_18 = acos(L_17);
		V_1 = L_18;
		double L_19 = V_1;
		double L_20 = GPSManager_rad2deg_m2849866022(__this, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		double L_21 = V_1;
		V_1 = ((double)((double)((double)((double)L_21*(double)(60.0)))*(double)(1.1515)));
		double L_22 = V_1;
		V_1 = ((double)((double)L_22*(double)(1.609344)));
		double L_23 = V_1;
		V_1 = ((double)((double)L_23*(double)(1000.0)));
		double L_24 = V_1;
		double L_25 = Math_Round_m2351753873(NULL /*static, unused*/, L_24, 2, /*hidden argument*/NULL);
		V_1 = L_25;
		double L_26 = V_1;
		return L_26;
	}
}
// System.Double GPSManager::deg2rad(System.Double)
extern "C"  double GPSManager_deg2rad_m276033958 (GPSManager_t1489257411 * __this, double ___deg0, const MethodInfo* method)
{
	{
		double L_0 = ___deg0;
		return (((double)((double)((double)((double)((double)((double)L_0*(double)(3.1415926535897931)))/(double)(180.0))))));
	}
}
// System.Double GPSManager::rad2deg(System.Double)
extern "C"  double GPSManager_rad2deg_m2849866022 (GPSManager_t1489257411 * __this, double ___rad0, const MethodInfo* method)
{
	{
		double L_0 = ___rad0;
		return (((double)((double)((double)((double)((double)((double)L_0*(double)(180.0)))/(double)(3.1415926535897931))))));
	}
}
// System.Void GPSManager::Update()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* GPSManager_t1489257411_il2cpp_TypeInfo_var;
extern const uint32_t GPSManager_Update_m3176528341_MetadataUsageId;
extern "C"  void GPSManager_Update_m3176528341 (GPSManager_t1489257411 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPSManager_Update_m3176528341_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	LocationInfo_t3215517959  V_1;
	memset(&V_1, 0, sizeof(V_1));
	LocationInfo_t3215517959  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)((int32_t)50))))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_1 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)((int32_t)27))))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_2 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)12))))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_3 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_00fa;
		}
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_4 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = LocationService_get_isEnabledByUser_m185518765(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_6 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		LocationService_Stop_m4216060557(L_6, /*hidden argument*/NULL);
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_state_3(0);
		goto IL_00bf;
	}

IL_0053:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_7 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = LocationService_get_status_m435232686(L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)1))))
		{
			goto IL_006e;
		}
	}
	{
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_state_3(4);
		goto IL_00bf;
	}

IL_006e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_9 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = LocationService_get_status_m435232686(L_9, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_10) == ((uint32_t)3))))
		{
			goto IL_0089;
		}
	}
	{
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_state_3(2);
		goto IL_00bf;
	}

IL_0089:
	{
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_state_3(3);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_11 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		LocationInfo_t3215517959  L_12 = LocationService_get_lastData_m2812407679(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		float L_13 = LocationInfo_get_latitude_m1803811355((&V_1), /*hidden argument*/NULL);
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_latitude_4((((double)((double)L_13))));
		LocationService_t3853025142 * L_14 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		LocationInfo_t3215517959  L_15 = LocationService_get_lastData_m2812407679(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		float L_16 = LocationInfo_get_longitude_m3505987842((&V_2), /*hidden argument*/NULL);
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_longitude_5((((double)((double)L_16))));
	}

IL_00bf:
	{
		int32_t L_17 = ((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->get_state_3();
		if ((!(((uint32_t)L_17) == ((uint32_t)3))))
		{
			goto IL_00fa;
		}
	}
	{
		double L_18 = ((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->get_latitude_4();
		double L_19 = ((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->get_longitude_5();
		double L_20 = ((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->get_TargetLatitude_7();
		double L_21 = ((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->get_TargetLongitude_8();
		double L_22 = GPSManager_Distance_m4256732498(__this, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		V_0 = L_22;
		double L_23 = V_0;
		if ((!(((double)L_23) > ((double)(0.0)))))
		{
			goto IL_00fa;
		}
	}
	{
		double L_24 = V_0;
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_distance_6(L_24);
	}

IL_00fa:
	{
		return;
	}
}
// System.Void GPSManager::FixedUpdate()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* GPSManager_t1489257411_il2cpp_TypeInfo_var;
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4137760208;
extern Il2CppCodeGenString* _stringLiteral3995696013;
extern const uint32_t GPSManager_FixedUpdate_m1846719411_MetadataUsageId;
extern "C"  void GPSManager_FixedUpdate_m1846719411 (GPSManager_t1489257411 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPSManager_FixedUpdate_m1846719411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)((int32_t)50))))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_1 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)((int32_t)27))))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_2 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)12))))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_3 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_00e1;
		}
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		bool L_4 = ErrorMessage_get_PopupON_m360300232(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003a;
		}
	}
	{
		return;
	}

IL_003a:
	{
		int32_t L_5 = ((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->get_state_3();
		V_0 = L_5;
		int32_t L_6 = V_0;
		if (L_6 == 0)
		{
			goto IL_0075;
		}
		if (L_6 == 1)
		{
			goto IL_00b6;
		}
		if (L_6 == 2)
		{
			goto IL_008b;
		}
		if (L_6 == 3)
		{
			goto IL_006a;
		}
		if (L_6 == 4)
		{
			goto IL_005f;
		}
	}
	{
		goto IL_00e1;
	}

IL_005f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_GPSPopupMessage_m1603270948(NULL /*static, unused*/, 5, /*hidden argument*/NULL);
		goto IL_00e1;
	}

IL_006a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_GPSPopupMessage_m1603270948(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		goto IL_00e1;
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		bool L_7 = ErrorMessage_get_PopupON_m360300232(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0080;
		}
	}
	{
		return;
	}

IL_0080:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_GPSPopupMessage_m1603270948(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		goto IL_00e1;
	}

IL_008b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_GPSPopupMessage_m1603270948(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		int32_t L_8 = ((int32_t)3);
		Il2CppObject * L_9 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_8);
		NullCheck((Enum_t2862688501 *)L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_9);
		int32_t L_11 = ((int32_t)2);
		Il2CppObject * L_12 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_11);
		NullCheck((Enum_t2862688501 *)L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_12);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_10, _stringLiteral4137760208, L_13, /*hidden argument*/NULL);
		goto IL_00e1;
	}

IL_00b6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_GPSPopupMessage_m1603270948(NULL /*static, unused*/, 4, /*hidden argument*/NULL);
		int32_t L_14 = ((int32_t)3);
		Il2CppObject * L_15 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_14);
		NullCheck((Enum_t2862688501 *)L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_15);
		int32_t L_17 = ((int32_t)2);
		Il2CppObject * L_18 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_17);
		NullCheck((Enum_t2862688501 *)L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_18);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_16, _stringLiteral3995696013, L_19, /*hidden argument*/NULL);
		goto IL_00e1;
	}

IL_00e1:
	{
		return;
	}
}
// System.Void GPSManager/<OnApplicationPause>c__IteratorE::.ctor()
extern "C"  void U3COnApplicationPauseU3Ec__IteratorE__ctor_m3397538030 (U3COnApplicationPauseU3Ec__IteratorE_t330808093 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GPSManager/<OnApplicationPause>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3COnApplicationPauseU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1809486500 (U3COnApplicationPauseU3Ec__IteratorE_t330808093 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object GPSManager/<OnApplicationPause>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3COnApplicationPauseU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m3694026296 (U3COnApplicationPauseU3Ec__IteratorE_t330808093 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean GPSManager/<OnApplicationPause>c__IteratorE::MoveNext()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* GPSManager_t1489257411_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const uint32_t U3COnApplicationPauseU3Ec__IteratorE_MoveNext_m4234488902_MetadataUsageId;
extern "C"  bool U3COnApplicationPauseU3Ec__IteratorE_MoveNext_m4234488902 (U3COnApplicationPauseU3Ec__IteratorE_t330808093 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3COnApplicationPauseU3Ec__IteratorE_MoveNext_m4234488902_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	LocationInfo_t3215517959  V_1;
	memset(&V_1, 0, sizeof(V_1));
	LocationInfo_t3215517959  V_2;
	memset(&V_2, 0, sizeof(V_2));
	bool V_3 = false;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0074;
		}
	}
	{
		goto IL_010c;
	}

IL_0021:
	{
		bool L_2 = __this->get_pauseState_0();
		if (!L_2)
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_3 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		LocationService_Stop_m4216060557(L_3, /*hidden argument*/NULL);
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_state_3(0);
		goto IL_0105;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_4 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		LocationService_Start_m1836061721(L_4, /*hidden argument*/NULL);
		__this->set_U3CwaitTimeU3E__0_1(((int32_t)15));
		goto IL_0082;
	}

IL_0058:
	{
		WaitForSeconds_t1615819279 * L_5 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_5, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_3(L_5);
		__this->set_U24PC_2(1);
		goto IL_010e;
	}

IL_0074:
	{
		int32_t L_6 = __this->get_U3CwaitTimeU3E__0_1();
		__this->set_U3CwaitTimeU3E__0_1(((int32_t)((int32_t)L_6-(int32_t)1)));
	}

IL_0082:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_7 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = LocationService_get_status_m435232686(L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)1))))
		{
			goto IL_009e;
		}
	}
	{
		int32_t L_9 = __this->get_U3CwaitTimeU3E__0_1();
		if ((((int32_t)L_9) > ((int32_t)0)))
		{
			goto IL_0058;
		}
	}

IL_009e:
	{
		int32_t L_10 = __this->get_U3CwaitTimeU3E__0_1();
		if (L_10)
		{
			goto IL_00b4;
		}
	}
	{
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_state_3(1);
		goto IL_0105;
	}

IL_00b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_11 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = LocationService_get_status_m435232686(L_11, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)3))))
		{
			goto IL_00cf;
		}
	}
	{
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_state_3(2);
		goto IL_0105;
	}

IL_00cf:
	{
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_state_3(3);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_13 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		LocationInfo_t3215517959  L_14 = LocationService_get_lastData_m2812407679(L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		float L_15 = LocationInfo_get_latitude_m1803811355((&V_1), /*hidden argument*/NULL);
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_latitude_4((((double)((double)L_15))));
		LocationService_t3853025142 * L_16 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		LocationInfo_t3215517959  L_17 = LocationService_get_lastData_m2812407679(L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		float L_18 = LocationInfo_get_longitude_m3505987842((&V_2), /*hidden argument*/NULL);
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_longitude_5((((double)((double)L_18))));
	}

IL_0105:
	{
		__this->set_U24PC_2((-1));
	}

IL_010c:
	{
		return (bool)0;
	}

IL_010e:
	{
		return (bool)1;
	}
	// Dead block : IL_0110: ldloc.3
}
// System.Void GPSManager/<OnApplicationPause>c__IteratorE::Dispose()
extern "C"  void U3COnApplicationPauseU3Ec__IteratorE_Dispose_m757184939 (U3COnApplicationPauseU3Ec__IteratorE_t330808093 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void GPSManager/<OnApplicationPause>c__IteratorE::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3COnApplicationPauseU3Ec__IteratorE_Reset_m1043970971_MetadataUsageId;
extern "C"  void U3COnApplicationPauseU3Ec__IteratorE_Reset_m1043970971 (U3COnApplicationPauseU3Ec__IteratorE_t330808093 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3COnApplicationPauseU3Ec__IteratorE_Reset_m1043970971_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GPSManager/<Start>c__IteratorD::.ctor()
extern "C"  void U3CStartU3Ec__IteratorD__ctor_m142249724 (U3CStartU3Ec__IteratorD_t1827712127 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GPSManager/<Start>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m925952416 (U3CStartU3Ec__IteratorD_t1827712127 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object GPSManager/<Start>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3853135156 (U3CStartU3Ec__IteratorD_t1827712127 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean GPSManager/<Start>c__IteratorD::MoveNext()
extern Il2CppClass* GPSManager_t1489257411_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__IteratorD_MoveNext_m547373664_MetadataUsageId;
extern "C"  bool U3CStartU3Ec__IteratorD_MoveNext_m547373664 (U3CStartU3Ec__IteratorD_t1827712127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__IteratorD_MoveNext_m547373664_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0069;
		}
	}
	{
		goto IL_0070;
	}

IL_0021:
	{
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_state_3(0);
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_latitude_4((0.0));
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_longitude_5((0.0));
		((GPSManager_t1489257411_StaticFields*)GPSManager_t1489257411_il2cpp_TypeInfo_var->static_fields)->set_distance_6((0.0));
		bool L_2 = ((bool)0);
		Il2CppObject * L_3 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_2);
		__this->set_U24current_1(L_3);
		__this->set_U24PC_0(1);
		goto IL_0072;
	}

IL_0069:
	{
		__this->set_U24PC_0((-1));
	}

IL_0070:
	{
		return (bool)0;
	}

IL_0072:
	{
		return (bool)1;
	}
	// Dead block : IL_0074: ldloc.1
}
// System.Void GPSManager/<Start>c__IteratorD::Dispose()
extern "C"  void U3CStartU3Ec__IteratorD_Dispose_m3456281657 (U3CStartU3Ec__IteratorD_t1827712127 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void GPSManager/<Start>c__IteratorD::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__IteratorD_Reset_m2083649961_MetadataUsageId;
extern "C"  void U3CStartU3Ec__IteratorD_Reset_m2083649961 (U3CStartU3Ec__IteratorD_t1827712127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__IteratorD_Reset_m2083649961_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GreatmanMessage::.ctor()
extern "C"  void GreatmanMessage__ctor_m1606712625 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GreatmanMessage::Awake()
extern Il2CppCodeGenString* _stringLiteral76880356;
extern const uint32_t GreatmanMessage_Awake_m1844317844_MetadataUsageId;
extern "C"  void GreatmanMessage_Awake_m1844317844 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GreatmanMessage_Awake_m1844317844_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral76880356, /*hidden argument*/NULL);
		__this->set_panel_18(L_0);
		return;
	}
}
// System.Void GreatmanMessage::PSMButtonClick(System.Int32)
extern Il2CppClass* PublicSpotMessageDB_t1853916122_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral53138620;
extern Il2CppCodeGenString* _stringLiteral453008043;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral1478659;
extern Il2CppCodeGenString* _stringLiteral3439929502;
extern Il2CppCodeGenString* _stringLiteral1475827;
extern const uint32_t GreatmanMessage_PSMButtonClick_m1735655312_MetadataUsageId;
extern "C"  void GreatmanMessage_PSMButtonClick_m1735655312 (GreatmanMessage_t1817744250 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GreatmanMessage_PSMButtonClick_m1735655312_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		Text_t9039225 * L_0 = __this->get_BodyText_16();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		GameObject_t3674682005 * L_2 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral53138620, /*hidden argument*/NULL);
		NullCheck(L_2);
		Text_t9039225 * L_3 = GameObject_GetComponent_TisText_t9039225_m202917489(L_2, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		__this->set_BodyText_16(L_3);
	}

IL_0026:
	{
		Text_t9039225 * L_4 = __this->get_TitleText_17();
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004c;
		}
	}
	{
		GameObject_t3674682005 * L_6 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral453008043, /*hidden argument*/NULL);
		NullCheck(L_6);
		Text_t9039225 * L_7 = GameObject_GetComponent_TisText_t9039225_m202917489(L_6, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		__this->set_TitleText_17(L_7);
	}

IL_004c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PublicSpotMessageDB_t1853916122_il2cpp_TypeInfo_var);
		PublicSpotMessageDB_t1853916122 * L_8 = PublicSpotMessageDB_get_Instance_m2156621208(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_9 = ___index0;
		NullCheck(L_8);
		PublicSpotMessage_t3890192988 * L_10 = PublicSpotMessageDB_MessagesByIndex_m3257673094(L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = L_10->get_title_2();
		__this->set__title_2(L_11);
		PublicSpotMessageDB_t1853916122 * L_12 = PublicSpotMessageDB_get_Instance_m2156621208(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_13 = ___index0;
		NullCheck(L_12);
		PublicSpotMessage_t3890192988 * L_14 = PublicSpotMessageDB_MessagesByIndex_m3257673094(L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15 = L_14->get_realfilename_3();
		__this->set__realfilename_3(L_15);
		PublicSpotMessageDB_t1853916122 * L_16 = PublicSpotMessageDB_get_Instance_m2156621208(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_17 = ___index0;
		NullCheck(L_16);
		PublicSpotMessage_t3890192988 * L_18 = PublicSpotMessageDB_MessagesByIndex_m3257673094(L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_19 = L_18->get_pref_4();
		__this->set__pref_4(L_19);
		PublicSpotMessageDB_t1853916122 * L_20 = PublicSpotMessageDB_get_Instance_m2156621208(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_21 = ___index0;
		NullCheck(L_20);
		PublicSpotMessage_t3890192988 * L_22 = PublicSpotMessageDB_MessagesByIndex_m3257673094(L_20, L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_23 = L_22->get_addr01_5();
		__this->set__addr01_5(L_23);
		PublicSpotMessageDB_t1853916122 * L_24 = PublicSpotMessageDB_get_Instance_m2156621208(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_25 = ___index0;
		NullCheck(L_24);
		PublicSpotMessage_t3890192988 * L_26 = PublicSpotMessageDB_MessagesByIndex_m3257673094(L_24, L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		String_t* L_27 = L_26->get_addr02_6();
		__this->set__addr02_6(L_27);
		PublicSpotMessageDB_t1853916122 * L_28 = PublicSpotMessageDB_get_Instance_m2156621208(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_29 = ___index0;
		NullCheck(L_28);
		PublicSpotMessage_t3890192988 * L_30 = PublicSpotMessageDB_MessagesByIndex_m3257673094(L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		String_t* L_31 = L_30->get_latitude_7();
		__this->set__latitude_7(L_31);
		PublicSpotMessageDB_t1853916122 * L_32 = PublicSpotMessageDB_get_Instance_m2156621208(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_33 = ___index0;
		NullCheck(L_32);
		PublicSpotMessage_t3890192988 * L_34 = PublicSpotMessageDB_MessagesByIndex_m3257673094(L_32, L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		String_t* L_35 = L_34->get_longtitude_8();
		__this->set__longtitude_8(L_35);
		PublicSpotMessageDB_t1853916122 * L_36 = PublicSpotMessageDB_get_Instance_m2156621208(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_37 = ___index0;
		NullCheck(L_36);
		PublicSpotMessage_t3890192988 * L_38 = PublicSpotMessageDB_MessagesByIndex_m3257673094(L_36, L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		String_t* L_39 = L_38->get_type_1();
		__this->set__type_9(L_39);
		String_t* L_40 = __this->get__title_2();
		NullCheck(L_40);
		String_t* L_41 = String_Replace_m3369701083(L_40, ((int32_t)47), ((int32_t)95), /*hidden argument*/NULL);
		V_0 = L_41;
		String_t* L_42 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m2933632197(NULL /*static, unused*/, L_42, _stringLiteral47, L_43, _stringLiteral1478659, /*hidden argument*/NULL);
		__this->set__video_localpath_10(L_44);
		StringU5BU5D_t4054002952* L_45 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 0);
		ArrayElementTypeCheck (L_45, _stringLiteral3439929502);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3439929502);
		StringU5BU5D_t4054002952* L_46 = L_45;
		String_t* L_47 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, 1);
		ArrayElementTypeCheck (L_46, L_47);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_47);
		StringU5BU5D_t4054002952* L_48 = L_46;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 2);
		ArrayElementTypeCheck (L_48, _stringLiteral47);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral47);
		StringU5BU5D_t4054002952* L_49 = L_48;
		String_t* L_50 = V_0;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, 3);
		ArrayElementTypeCheck (L_49, L_50);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_50);
		StringU5BU5D_t4054002952* L_51 = L_49;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, 4);
		ArrayElementTypeCheck (L_51, _stringLiteral1478659);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1478659);
		String_t* L_52 = String_Concat_m21867311(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		__this->set__video_localpath2_11(L_52);
		String_t* L_53 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_54 = V_0;
		String_t* L_55 = String_Concat_m2933632197(NULL /*static, unused*/, L_53, _stringLiteral47, L_54, _stringLiteral1475827, /*hidden argument*/NULL);
		__this->set__image_localpath_12(L_55);
		String_t* L_56 = __this->get__title_2();
		String_t* L_57 = __this->get__realfilename_3();
		String_t* L_58 = __this->get__pref_4();
		String_t* L_59 = __this->get__addr01_5();
		String_t* L_60 = __this->get__addr02_6();
		String_t* L_61 = __this->get__latitude_7();
		String_t* L_62 = __this->get__longtitude_8();
		GreatmanMessage_MessageSet_m2814655900(__this, L_56, L_57, L_58, L_59, L_60, L_61, L_62, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GreatmanMessage::MessageSet(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4276625758;
extern Il2CppCodeGenString* _stringLiteral2936184020;
extern const uint32_t GreatmanMessage_MessageSet_m2814655900_MetadataUsageId;
extern "C"  void GreatmanMessage_MessageSet_m2814655900 (GreatmanMessage_t1817744250 * __this, String_t* ___title0, String_t* ___realfilename1, String_t* ___pref2, String_t* ___addr013, String_t* ___addr024, String_t* ___latitude5, String_t* ___longtitude6, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GreatmanMessage_MessageSet_m2814655900_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Text_t9039225 * L_0 = __this->get_TitleText_17();
		String_t* L_1 = ___title0;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_1);
		Text_t9039225 * L_2 = __this->get_BodyText_16();
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, _stringLiteral4276625758);
		String_t* L_3 = ___latitude5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_5 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005b;
		}
	}
	{
		String_t* L_6 = ___longtitude6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_8 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005b;
		}
	}
	{
		String_t* L_9 = ___latitude5;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		double L_10 = Convert_ToDouble_m1719184653(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		GPSManager_set__TargetLatitude_m470540013(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		String_t* L_11 = ___longtitude6;
		double L_12 = Convert_ToDouble_m1719184653(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		GPSManager_set__TargetLongitude_m2588667994(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		goto IL_006b;
	}

IL_005b:
	{
		Text_t9039225 * L_13 = __this->get_CurrentDistance_21();
		NullCheck(L_13);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_13, _stringLiteral2936184020);
	}

IL_006b:
	{
		return;
	}
}
// System.Void GreatmanMessage::VideoPlay()
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern const uint32_t GreatmanMessage_VideoPlay_m364876254_MetadataUsageId;
extern "C"  void GreatmanMessage_VideoPlay_m364876254 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GreatmanMessage_VideoPlay_m364876254_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = ((int32_t)3);
		Il2CppObject * L_2 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		int32_t L_4 = ((int32_t)2);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_3, _stringLiteral3660319847, L_6, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		Il2CppObject * L_7 = GreatmanMessage_VideoDown_m4134881812(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator GreatmanMessage::VideoDown()
extern Il2CppClass* U3CVideoDownU3Ec__IteratorF_t830887869_il2cpp_TypeInfo_var;
extern const uint32_t GreatmanMessage_VideoDown_m4134881812_MetadataUsageId;
extern "C"  Il2CppObject * GreatmanMessage_VideoDown_m4134881812 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GreatmanMessage_VideoDown_m4134881812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CVideoDownU3Ec__IteratorF_t830887869 * V_0 = NULL;
	{
		U3CVideoDownU3Ec__IteratorF_t830887869 * L_0 = (U3CVideoDownU3Ec__IteratorF_t830887869 *)il2cpp_codegen_object_new(U3CVideoDownU3Ec__IteratorF_t830887869_il2cpp_TypeInfo_var);
		U3CVideoDownU3Ec__IteratorF__ctor_m2658638926(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CVideoDownU3Ec__IteratorF_t830887869 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_4(__this);
		U3CVideoDownU3Ec__IteratorF_t830887869 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator GreatmanMessage::VideoStart()
extern Il2CppClass* U3CVideoStartU3Ec__Iterator10_t4263606752_il2cpp_TypeInfo_var;
extern const uint32_t GreatmanMessage_VideoStart_m4177958418_MetadataUsageId;
extern "C"  Il2CppObject * GreatmanMessage_VideoStart_m4177958418 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GreatmanMessage_VideoStart_m4177958418_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CVideoStartU3Ec__Iterator10_t4263606752 * V_0 = NULL;
	{
		U3CVideoStartU3Ec__Iterator10_t4263606752 * L_0 = (U3CVideoStartU3Ec__Iterator10_t4263606752 *)il2cpp_codegen_object_new(U3CVideoStartU3Ec__Iterator10_t4263606752_il2cpp_TypeInfo_var);
		U3CVideoStartU3Ec__Iterator10__ctor_m2435258251(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CVideoStartU3Ec__Iterator10_t4263606752 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_4(__this);
		U3CVideoStartU3Ec__Iterator10_t4263606752 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator GreatmanMessage::ImageLoad()
extern Il2CppClass* U3CImageLoadU3Ec__Iterator11_t890999725_il2cpp_TypeInfo_var;
extern const uint32_t GreatmanMessage_ImageLoad_m1200707128_MetadataUsageId;
extern "C"  Il2CppObject * GreatmanMessage_ImageLoad_m1200707128 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GreatmanMessage_ImageLoad_m1200707128_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CImageLoadU3Ec__Iterator11_t890999725 * V_0 = NULL;
	{
		U3CImageLoadU3Ec__Iterator11_t890999725 * L_0 = (U3CImageLoadU3Ec__Iterator11_t890999725 *)il2cpp_codegen_object_new(U3CImageLoadU3Ec__Iterator11_t890999725_il2cpp_TypeInfo_var);
		U3CImageLoadU3Ec__Iterator11__ctor_m423468430(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CImageLoadU3Ec__Iterator11_t890999725 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_4(__this);
		U3CImageLoadU3Ec__Iterator11_t890999725 * L_2 = V_0;
		return L_2;
	}
}
// System.Void GreatmanMessage::ImageVisible()
extern "C"  void GreatmanMessage_ImageVisible_m2348391626 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = __this->get__Sprite_14();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator GreatmanMessage::CheckProgress(UnityEngine.WWW)
extern Il2CppClass* U3CCheckProgressU3Ec__Iterator12_t3998861986_il2cpp_TypeInfo_var;
extern const uint32_t GreatmanMessage_CheckProgress_m1738077254_MetadataUsageId;
extern "C"  Il2CppObject * GreatmanMessage_CheckProgress_m1738077254 (GreatmanMessage_t1817744250 * __this, WWW_t3134621005 * ____www0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GreatmanMessage_CheckProgress_m1738077254_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CCheckProgressU3Ec__Iterator12_t3998861986 * V_0 = NULL;
	{
		U3CCheckProgressU3Ec__Iterator12_t3998861986 * L_0 = (U3CCheckProgressU3Ec__Iterator12_t3998861986 *)il2cpp_codegen_object_new(U3CCheckProgressU3Ec__Iterator12_t3998861986_il2cpp_TypeInfo_var);
		U3CCheckProgressU3Ec__Iterator12__ctor_m2740756025(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCheckProgressU3Ec__Iterator12_t3998861986 * L_1 = V_0;
		WWW_t3134621005 * L_2 = ____www0;
		NullCheck(L_1);
		L_1->set__www_0(L_2);
		U3CCheckProgressU3Ec__Iterator12_t3998861986 * L_3 = V_0;
		WWW_t3134621005 * L_4 = ____www0;
		NullCheck(L_3);
		L_3->set_U3CU24U3E_www_3(L_4);
		U3CCheckProgressU3Ec__Iterator12_t3998861986 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_4(__this);
		U3CCheckProgressU3Ec__Iterator12_t3998861986 * L_6 = V_0;
		return L_6;
	}
}
// System.Void GreatmanMessage::MapView()
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern Il2CppCodeGenString* _stringLiteral1474523267;
extern Il2CppCodeGenString* _stringLiteral47;
extern const uint32_t GreatmanMessage_MapView_m2793212080_MetadataUsageId;
extern "C"  void GreatmanMessage_MapView_m2793212080 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GreatmanMessage_MapView_m2793212080_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		int32_t L_0 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = ((int32_t)3);
		Il2CppObject * L_2 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		int32_t L_4 = ((int32_t)2);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_3, _stringLiteral3660319847, L_6, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_7 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = __this->get__latitude_7();
		String_t* L_9 = __this->get__longtitude_8();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1825781833(NULL /*static, unused*/, L_8, _stringLiteral47, L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		AndroidWebview_Call_m335597645(L_7, _stringLiteral1474523267, L_10, /*hidden argument*/NULL);
		String_t* L_11 = __this->get__latitude_7();
		float L_12 = Single_Parse_m3022284664(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		String_t* L_13 = __this->get__longtitude_8();
		float L_14 = Single_Parse_m3022284664(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		float L_15 = V_0;
		float L_16 = V_1;
		CtrlPlugins_Open_m1700334148(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GreatmanMessage::OnEnable()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2674321230;
extern const uint32_t GreatmanMessage_OnEnable_m236875957_MetadataUsageId;
extern "C"  void GreatmanMessage_OnEnable_m236875957 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GreatmanMessage_OnEnable_m236875957_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set__messagecheck_23((bool)1);
		__this->set_timer_27((0.0f));
		__this->set_waitingTime_28(3);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_0 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		LocationService_Start_m1836061721(L_0, /*hidden argument*/NULL);
		Text_t9039225 * L_1 = __this->get_CurrentDistance_21();
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, _stringLiteral2674321230);
		Button_t3896396478 * L_2 = __this->get_MessageSeeBtn_22();
		NullCheck(L_2);
		Selectable_set_interactable_m2686686419(L_2, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GreatmanMessage::OnDisable()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GreatmanMessage_OnDisable_m3489124632_MetadataUsageId;
extern "C"  void GreatmanMessage_OnDisable_m3489124632 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GreatmanMessage_OnDisable_m3489124632_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, ((int32_t)12), /*hidden argument*/NULL);
		__this->set__messagecheck_23((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_0 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		LocationService_Stop_m4216060557(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GreatmanMessage::Update()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral12256356;
extern Il2CppCodeGenString* _stringLiteral3426;
extern Il2CppCodeGenString* _stringLiteral109;
extern Il2CppCodeGenString* _stringLiteral2605310289;
extern Il2CppCodeGenString* _stringLiteral2590475406;
extern const uint32_t GreatmanMessage_Update_m4290313212_MetadataUsageId;
extern "C"  void GreatmanMessage_Update_m4290313212 (GreatmanMessage_t1817744250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GreatmanMessage_Update_m4290313212_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0253;
		}
	}
	{
		float L_1 = __this->get_timer_27();
		float L_2 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timer_27(((float)((float)L_1+(float)L_2)));
		float L_3 = __this->get_timer_27();
		int32_t L_4 = __this->get_waitingTime_28();
		if ((!(((float)L_3) > ((float)(((float)((float)L_4)))))))
		{
			goto IL_0253;
		}
	}
	{
		bool L_5 = __this->get__messagecheck_23();
		if (!L_5)
		{
			goto IL_0248;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_6 = CurrentState_get_GPSPopupMessage_m652265359(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_0248;
		}
	}
	{
		String_t* L_7 = __this->get__latitude_7();
		__this->set_lat_25(L_7);
		String_t* L_8 = __this->get__longtitude_8();
		__this->set_lon_26(L_8);
		String_t* L_9 = __this->get_lat_25();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0081;
		}
	}
	{
		GPSManager_set__TargetLatitude_m470540013(NULL /*static, unused*/, (0.0), /*hidden argument*/NULL);
		goto IL_0091;
	}

IL_0081:
	{
		String_t* L_11 = __this->get_lat_25();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		double L_12 = Convert_ToDouble_m1719184653(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		GPSManager_set__TargetLatitude_m470540013(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
	}

IL_0091:
	{
		String_t* L_13 = __this->get_lon_26();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00b4;
		}
	}
	{
		GPSManager_set__TargetLongitude_m2588667994(NULL /*static, unused*/, (0.0), /*hidden argument*/NULL);
		goto IL_00c4;
	}

IL_00b4:
	{
		String_t* L_15 = __this->get_lon_26();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		double L_16 = Convert_ToDouble_m1719184653(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		GPSManager_set__TargetLongitude_m2588667994(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
	}

IL_00c4:
	{
		double L_17 = GPSManager_get__distance_m1549713694(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((double)L_17) > ((double)(10000.0)))))
		{
			goto IL_0140;
		}
	}
	{
		Button_t3896396478 * L_18 = __this->get_MessageSeeBtn_22();
		NullCheck(L_18);
		Selectable_set_interactable_m2686686419(L_18, (bool)0, /*hidden argument*/NULL);
		double L_19 = GPSManager_get__distance_m1549713694(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__distancedouble_24(L_19);
		double L_20 = __this->get__distancedouble_24();
		__this->set__distancedouble_24(((double)((double)L_20/(double)(1000.0))));
		double L_21 = __this->get__distancedouble_24();
		double L_22 = Math_Round_m2351753873(NULL /*static, unused*/, L_21, 2, /*hidden argument*/NULL);
		__this->set__distancedouble_24(L_22);
		Text_t9039225 * L_23 = __this->get_CurrentDistance_21();
		double* L_24 = __this->get_address_of__distancedouble_24();
		String_t* L_25 = Double_ToString_m3380246633(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral12256356, L_25, _stringLiteral3426, /*hidden argument*/NULL);
		NullCheck(L_23);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_23, L_26);
		goto IL_0202;
	}

IL_0140:
	{
		double L_27 = GPSManager_get__distance_m1549713694(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((double)L_27) <= ((double)(10000.0)))))
		{
			goto IL_0202;
		}
	}
	{
		double L_28 = GPSManager_get__distance_m1549713694(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__distancedouble_24(L_28);
		double L_29 = __this->get__distancedouble_24();
		double L_30 = Math_Round_m2351753873(NULL /*static, unused*/, L_29, 2, /*hidden argument*/NULL);
		__this->set__distancedouble_24(L_30);
		double L_31 = GPSManager_get__distance_m1549713694(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((double)L_31) > ((double)(1000.0)))))
		{
			goto IL_01ad;
		}
	}
	{
		Text_t9039225 * L_32 = __this->get_CurrentDistance_21();
		double* L_33 = __this->get_address_of__distancedouble_24();
		String_t* L_34 = Double_ToString_m3380246633(L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral12256356, L_34, _stringLiteral3426, /*hidden argument*/NULL);
		NullCheck(L_32);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_32, L_35);
		goto IL_01d2;
	}

IL_01ad:
	{
		Text_t9039225 * L_36 = __this->get_CurrentDistance_21();
		double* L_37 = __this->get_address_of__distancedouble_24();
		String_t* L_38 = Double_ToString_m3380246633(L_37, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral12256356, L_38, _stringLiteral109, /*hidden argument*/NULL);
		NullCheck(L_36);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_36, L_39);
	}

IL_01d2:
	{
		double L_40 = GPSManager_get__distance_m1549713694(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((double)L_40) < ((double)(20.0)))))
		{
			goto IL_01f6;
		}
	}
	{
		Button_t3896396478 * L_41 = __this->get_MessageSeeBtn_22();
		NullCheck(L_41);
		Selectable_set_interactable_m2686686419(L_41, (bool)1, /*hidden argument*/NULL);
		goto IL_0202;
	}

IL_01f6:
	{
		Button_t3896396478 * L_42 = __this->get_MessageSeeBtn_22();
		NullCheck(L_42);
		Selectable_set_interactable_m2686686419(L_42, (bool)0, /*hidden argument*/NULL);
	}

IL_0202:
	{
		ObjectU5BU5D_t1108656482* L_43 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 0);
		ArrayElementTypeCheck (L_43, _stringLiteral2605310289);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2605310289);
		ObjectU5BU5D_t1108656482* L_44 = L_43;
		double L_45 = GPSManager_get__latitude_m2672992213(NULL /*static, unused*/, /*hidden argument*/NULL);
		double L_46 = L_45;
		Il2CppObject * L_47 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 1);
		ArrayElementTypeCheck (L_44, L_47);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_47);
		ObjectU5BU5D_t1108656482* L_48 = L_44;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 2);
		ArrayElementTypeCheck (L_48, _stringLiteral2590475406);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral2590475406);
		ObjectU5BU5D_t1108656482* L_49 = L_48;
		double L_50 = GPSManager_get__longitude_m385823368(NULL /*static, unused*/, /*hidden argument*/NULL);
		double L_51 = L_50;
		Il2CppObject * L_52 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_51);
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, 3);
		ArrayElementTypeCheck (L_49, L_52);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_52);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_53 = String_Concat_m3016520001(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Button_t3896396478 * L_54 = __this->get_MessageSeeBtn_22();
		NullCheck(L_54);
		Selectable_set_interactable_m2686686419(L_54, (bool)1, /*hidden argument*/NULL);
	}

IL_0248:
	{
		__this->set_timer_27((0.0f));
	}

IL_0253:
	{
		return;
	}
}
// System.Void GreatmanMessage/<CheckProgress>c__Iterator12::.ctor()
extern "C"  void U3CCheckProgressU3Ec__Iterator12__ctor_m2740756025 (U3CCheckProgressU3Ec__Iterator12_t3998861986 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GreatmanMessage/<CheckProgress>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckProgressU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2315462083 (U3CCheckProgressU3Ec__Iterator12_t3998861986 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object GreatmanMessage/<CheckProgress>c__Iterator12::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckProgressU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m4081520471 (U3CCheckProgressU3Ec__Iterator12_t3998861986 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean GreatmanMessage/<CheckProgress>c__Iterator12::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentInChildren_TisTMP_Text_t980027659_m2592787986_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral37;
extern Il2CppCodeGenString* _stringLiteral2135970;
extern const uint32_t U3CCheckProgressU3Ec__Iterator12_MoveNext_m1965523715_MetadataUsageId;
extern "C"  bool U3CCheckProgressU3Ec__Iterator12_MoveNext_m1965523715 (U3CCheckProgressU3Ec__Iterator12_t3998861986 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckProgressU3Ec__Iterator12_MoveNext_m1965523715_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0057;
		}
		if (L_1 == 2)
		{
			goto IL_011a;
		}
	}
	{
		goto IL_0121;
	}

IL_0025:
	{
		GreatmanMessage_t1817744250 * L_2 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = L_2->get_ProgressBar_19();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)1, /*hidden argument*/NULL);
		goto IL_00af;
	}

IL_003b:
	{
		WaitForSeconds_t1615819279 * L_4 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_4, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_4);
		__this->set_U24PC_1(1);
		goto IL_0123;
	}

IL_0057:
	{
		GreatmanMessage_t1817744250 * L_5 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = L_5->get_ProgressBar_19();
		NullCheck(L_6);
		TMP_Text_t980027659 * L_7 = GameObject_GetComponentInChildren_TisTMP_Text_t980027659_m2592787986(L_6, /*hidden argument*/GameObject_GetComponentInChildren_TisTMP_Text_t980027659_m2592787986_MethodInfo_var);
		WWW_t3134621005 * L_8 = __this->get__www_0();
		NullCheck(L_8);
		float L_9 = WWW_get_progress_m3186358572(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_10 = bankers_roundf(((float)((float)L_9*(float)(100.0f))));
		V_1 = L_10;
		String_t* L_11 = Single_ToString_m5736032((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m138640077(NULL /*static, unused*/, L_11, _stringLiteral37, /*hidden argument*/NULL);
		NullCheck(L_7);
		TMP_Text_set_text_m27929696(L_7, L_12, /*hidden argument*/NULL);
		GreatmanMessage_t1817744250 * L_13 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_13);
		Image_t538875265 * L_14 = L_13->get_image_20();
		WWW_t3134621005 * L_15 = __this->get__www_0();
		NullCheck(L_15);
		float L_16 = WWW_get_progress_m3186358572(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		Image_set_fillAmount_m1583793743(L_14, L_16, /*hidden argument*/NULL);
	}

IL_00af:
	{
		WWW_t3134621005 * L_17 = __this->get__www_0();
		NullCheck(L_17);
		bool L_18 = WWW_get_isDone_m634060017(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_003b;
		}
	}
	{
		WWW_t3134621005 * L_19 = __this->get__www_0();
		NullCheck(L_19);
		bool L_20 = WWW_get_isDone_m634060017(L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_011a;
		}
	}
	{
		WWW_t3134621005 * L_21 = __this->get__www_0();
		NullCheck(L_21);
		float L_22 = WWW_get_progress_m3186358572(L_21, /*hidden argument*/NULL);
		if ((!(((float)L_22) >= ((float)(1.0f)))))
		{
			goto IL_011a;
		}
	}
	{
		GreatmanMessage_t1817744250 * L_23 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_23);
		GameObject_t3674682005 * L_24 = L_23->get_ProgressBar_19();
		NullCheck(L_24);
		TMP_Text_t980027659 * L_25 = GameObject_GetComponentInChildren_TisTMP_Text_t980027659_m2592787986(L_24, /*hidden argument*/GameObject_GetComponentInChildren_TisTMP_Text_t980027659_m2592787986_MethodInfo_var);
		NullCheck(L_25);
		TMP_Text_set_text_m27929696(L_25, _stringLiteral2135970, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_26 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_26, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_26);
		__this->set_U24PC_1(2);
		goto IL_0123;
	}

IL_011a:
	{
		__this->set_U24PC_1((-1));
	}

IL_0121:
	{
		return (bool)0;
	}

IL_0123:
	{
		return (bool)1;
	}
	// Dead block : IL_0125: ldloc.2
}
// System.Void GreatmanMessage/<CheckProgress>c__Iterator12::Dispose()
extern "C"  void U3CCheckProgressU3Ec__Iterator12_Dispose_m949870646 (U3CCheckProgressU3Ec__Iterator12_t3998861986 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void GreatmanMessage/<CheckProgress>c__Iterator12::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckProgressU3Ec__Iterator12_Reset_m387188966_MetadataUsageId;
extern "C"  void U3CCheckProgressU3Ec__Iterator12_Reset_m387188966 (U3CCheckProgressU3Ec__Iterator12_t3998861986 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckProgressU3Ec__Iterator12_Reset_m387188966_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GreatmanMessage/<ImageLoad>c__Iterator11::.ctor()
extern "C"  void U3CImageLoadU3Ec__Iterator11__ctor_m423468430 (U3CImageLoadU3Ec__Iterator11_t890999725 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GreatmanMessage/<ImageLoad>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CImageLoadU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2580157454 (U3CImageLoadU3Ec__Iterator11_t890999725 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object GreatmanMessage/<ImageLoad>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CImageLoadU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m3952210850 (U3CImageLoadU3Ec__Iterator11_t890999725 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean GreatmanMessage/<ImageLoad>c__Iterator11::MoveNext()
extern Il2CppClass* Texture2D_t3884108195_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisImage_t538875265_m3706520426_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3529469;
extern Il2CppCodeGenString* _stringLiteral1800556493;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t U3CImageLoadU3Ec__Iterator11_MoveNext_m4188946830_MetadataUsageId;
extern "C"  bool U3CImageLoadU3Ec__Iterator11_MoveNext_m4188946830 (U3CImageLoadU3Ec__Iterator11_t890999725 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CImageLoadU3Ec__Iterator11_MoveNext_m4188946830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0073;
		}
	}
	{
		goto IL_0188;
	}

IL_0021:
	{
		Texture2D_t3884108195 * L_2 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3705883154(L_2, 1, 1, ((int32_t)34), (bool)0, /*hidden argument*/NULL);
		__this->set_U3CtexU3E__0_0(L_2);
		GreatmanMessage_t1817744250 * L_3 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_3);
		String_t* L_4 = L_3->get__image_localpath_12();
		ByteU5BU5D_t4260760469* L_5 = File_ReadAllBytes_m621899937(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_U3CbinaryImageDataU3E__1_1(L_5);
		goto IL_0073;
	}

IL_004c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_6 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		AndroidWebview_Call_m335597645(L_6, _stringLiteral3529469, _stringLiteral1800556493, /*hidden argument*/NULL);
		__this->set_U24current_3(NULL);
		__this->set_U24PC_2(1);
		goto IL_018a;
	}

IL_0073:
	{
		Texture2D_t3884108195 * L_7 = __this->get_U3CtexU3E__0_0();
		ByteU5BU5D_t4260760469* L_8 = __this->get_U3CbinaryImageDataU3E__1_1();
		NullCheck(L_7);
		bool L_9 = Texture2D_LoadImage_m2186196036(L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_10 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		AndroidWebview_Call_m1677830609(L_10, _stringLiteral3202370, /*hidden argument*/NULL);
		GreatmanMessage_t1817744250 * L_11 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_11);
		GameObject_t3674682005 * L_12 = L_11->get__Sprite_14();
		NullCheck(L_12);
		GameObject_SetActive_m3538205401(L_12, (bool)1, /*hidden argument*/NULL);
		GreatmanMessage_t1817744250 * L_13 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_13);
		RectTransform_t972643934 * L_14 = L_13->get_imagesize_15();
		NullCheck(L_14);
		Image_t538875265 * L_15 = Component_GetComponent_TisImage_t538875265_m3706520426(L_14, /*hidden argument*/Component_GetComponent_TisImage_t538875265_m3706520426_MethodInfo_var);
		Texture2D_t3884108195 * L_16 = __this->get_U3CtexU3E__0_0();
		Texture2D_t3884108195 * L_17 = __this->get_U3CtexU3E__0_0();
		NullCheck(L_17);
		int32_t L_18 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_17);
		Texture2D_t3884108195 * L_19 = __this->get_U3CtexU3E__0_0();
		NullCheck(L_19);
		int32_t L_20 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_19);
		Rect_t4241904616  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Rect__ctor_m3291325233(&L_21, (0.0f), (0.0f), (((float)((float)L_18))), (((float)((float)L_20))), /*hidden argument*/NULL);
		Vector2_t4282066565  L_22 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		Sprite_t3199167241 * L_23 = Sprite_Create_m278903054(NULL /*static, unused*/, L_16, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_15);
		Image_set_sprite_m572551402(L_15, L_23, /*hidden argument*/NULL);
		int32_t L_24 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_25 = __this->get_U3CtexU3E__0_0();
		NullCheck(L_25);
		int32_t L_26 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_25);
		if ((((int32_t)L_24) >= ((int32_t)L_26)))
		{
			goto IL_0148;
		}
	}
	{
		GreatmanMessage_t1817744250 * L_27 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_27);
		RectTransform_t972643934 * L_28 = L_27->get_imagesize_15();
		Texture2D_t3884108195 * L_29 = __this->get_U3CtexU3E__0_0();
		NullCheck(L_29);
		int32_t L_30 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_29);
		Texture2D_t3884108195 * L_31 = __this->get_U3CtexU3E__0_0();
		NullCheck(L_31);
		int32_t L_32 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_31);
		Vector2_t4282066565  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector2__ctor_m1517109030(&L_33, ((float)((float)(((float)((float)L_30)))*(float)(0.75f))), ((float)((float)(((float)((float)L_32)))*(float)(0.75f))), /*hidden argument*/NULL);
		NullCheck(L_28);
		RectTransform_set_sizeDelta_m1223846609(L_28, L_33, /*hidden argument*/NULL);
		goto IL_0181;
	}

IL_0148:
	{
		GreatmanMessage_t1817744250 * L_34 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_34);
		RectTransform_t972643934 * L_35 = L_34->get_imagesize_15();
		Texture2D_t3884108195 * L_36 = __this->get_U3CtexU3E__0_0();
		NullCheck(L_36);
		int32_t L_37 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_36);
		Texture2D_t3884108195 * L_38 = __this->get_U3CtexU3E__0_0();
		NullCheck(L_38);
		int32_t L_39 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_38);
		Vector2_t4282066565  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Vector2__ctor_m1517109030(&L_40, ((float)((float)(((float)((float)L_37)))*(float)(0.75f))), ((float)((float)(((float)((float)L_39)))*(float)(0.75f))), /*hidden argument*/NULL);
		NullCheck(L_35);
		RectTransform_set_sizeDelta_m1223846609(L_35, L_40, /*hidden argument*/NULL);
	}

IL_0181:
	{
		__this->set_U24PC_2((-1));
	}

IL_0188:
	{
		return (bool)0;
	}

IL_018a:
	{
		return (bool)1;
	}
	// Dead block : IL_018c: ldloc.1
}
// System.Void GreatmanMessage/<ImageLoad>c__Iterator11::Dispose()
extern "C"  void U3CImageLoadU3Ec__Iterator11_Dispose_m3124518475 (U3CImageLoadU3Ec__Iterator11_t890999725 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void GreatmanMessage/<ImageLoad>c__Iterator11::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CImageLoadU3Ec__Iterator11_Reset_m2364868667_MetadataUsageId;
extern "C"  void U3CImageLoadU3Ec__Iterator11_Reset_m2364868667 (U3CImageLoadU3Ec__Iterator11_t890999725 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CImageLoadU3Ec__Iterator11_Reset_m2364868667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GreatmanMessage/<VideoDown>c__IteratorF::.ctor()
extern "C"  void U3CVideoDownU3Ec__IteratorF__ctor_m2658638926 (U3CVideoDownU3Ec__IteratorF_t830887869 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GreatmanMessage/<VideoDown>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CVideoDownU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m135123140 (U3CVideoDownU3Ec__IteratorF_t830887869 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object GreatmanMessage/<VideoDown>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CVideoDownU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m2380389464 (U3CVideoDownU3Ec__IteratorF_t830887869 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean GreatmanMessage/<VideoDown>c__IteratorF::MoveNext()
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1511312033;
extern Il2CppCodeGenString* _stringLiteral1819179829;
extern Il2CppCodeGenString* _stringLiteral2220296047;
extern const uint32_t U3CVideoDownU3Ec__IteratorF_MoveNext_m4205243110_MetadataUsageId;
extern "C"  bool U3CVideoDownU3Ec__IteratorF_MoveNext_m4205243110 (U3CVideoDownU3Ec__IteratorF_t830887869 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CVideoDownU3Ec__IteratorF_MoveNext_m4205243110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_009b;
		}
	}
	{
		goto IL_0135;
	}

IL_0021:
	{
		GreatmanMessage_t1817744250 * L_2 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_2);
		String_t* L_3 = L_2->get__realfilename_3();
		WWW_t3134621005 * L_4 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m1985874080(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_0(L_4);
		GreatmanMessage_t1817744250 * L_5 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_5);
		String_t* L_6 = L_5->get__realfilename_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1511312033, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		GreatmanMessage_t1817744250 * L_8 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_8);
		String_t* L_9 = L_8->get__video_localpath_10();
		bool L_10 = File_Exists_m1326262381(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0117;
		}
	}
	{
		GreatmanMessage_t1817744250 * L_11 = __this->get_U3CU3Ef__this_4();
		GreatmanMessage_t1817744250 * L_12 = __this->get_U3CU3Ef__this_4();
		WWW_t3134621005 * L_13 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_12);
		Il2CppObject * L_14 = GreatmanMessage_CheckProgress_m1738077254(L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		MonoBehaviour_StartCoroutine_m2135303124(L_11, L_14, /*hidden argument*/NULL);
		WWW_t3134621005 * L_15 = __this->get_U3CwwwU3E__0_0();
		__this->set_U24current_3(L_15);
		__this->set_U24PC_2(1);
		goto IL_0137;
	}

IL_009b:
	{
		WWW_t3134621005 * L_16 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_16);
		String_t* L_17 = WWW_get_error_m1787423074(L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00d7;
		}
	}
	{
		WWW_t3134621005 * L_18 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_18);
		ByteU5BU5D_t4260760469* L_19 = WWW_get_bytes_m2080623436(L_18, /*hidden argument*/NULL);
		__this->set_U3CbytesU3E__1_1(L_19);
		GreatmanMessage_t1817744250 * L_20 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_20);
		String_t* L_21 = L_20->get__video_localpath_10();
		ByteU5BU5D_t4260760469* L_22 = __this->get_U3CbytesU3E__1_1();
		File_WriteAllBytes_m2419938065(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		goto IL_0117;
	}

IL_00d7:
	{
		GreatmanMessage_t1817744250 * L_23 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_23);
		GameObject_t3674682005 * L_24 = L_23->get_ProgressBar_19();
		NullCheck(L_24);
		GameObject_SetActive_m3538205401(L_24, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral1819179829, /*hidden argument*/NULL);
		int32_t L_25 = ((int32_t)3);
		Il2CppObject * L_26 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_25);
		NullCheck((Enum_t2862688501 *)L_26);
		String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_26);
		int32_t L_28 = ((int32_t)2);
		Il2CppObject * L_29 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_28);
		NullCheck((Enum_t2862688501 *)L_29);
		String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_29);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_27, _stringLiteral2220296047, L_30, /*hidden argument*/NULL);
		goto IL_0135;
	}

IL_0117:
	{
		GreatmanMessage_t1817744250 * L_31 = __this->get_U3CU3Ef__this_4();
		GreatmanMessage_t1817744250 * L_32 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_32);
		Il2CppObject * L_33 = GreatmanMessage_VideoStart_m4177958418(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		MonoBehaviour_StartCoroutine_m2135303124(L_31, L_33, /*hidden argument*/NULL);
		__this->set_U24PC_2((-1));
	}

IL_0135:
	{
		return (bool)0;
	}

IL_0137:
	{
		return (bool)1;
	}
	// Dead block : IL_0139: ldloc.1
}
// System.Void GreatmanMessage/<VideoDown>c__IteratorF::Dispose()
extern "C"  void U3CVideoDownU3Ec__IteratorF_Dispose_m3639717131 (U3CVideoDownU3Ec__IteratorF_t830887869 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void GreatmanMessage/<VideoDown>c__IteratorF::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CVideoDownU3Ec__IteratorF_Reset_m305071867_MetadataUsageId;
extern "C"  void U3CVideoDownU3Ec__IteratorF_Reset_m305071867 (U3CVideoDownU3Ec__IteratorF_t830887869 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CVideoDownU3Ec__IteratorF_Reset_m305071867_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GreatmanMessage/<VideoStart>c__Iterator10::.ctor()
extern "C"  void U3CVideoStartU3Ec__Iterator10__ctor_m2435258251 (U3CVideoStartU3Ec__Iterator10_t4263606752 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GreatmanMessage/<VideoStart>c__Iterator10::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CVideoStartU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2678190823 (U3CVideoStartU3Ec__Iterator10_t4263606752 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object GreatmanMessage/<VideoStart>c__Iterator10::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CVideoStartU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m2093786235 (U3CVideoStartU3Ec__Iterator10_t4263606752 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean GreatmanMessage/<VideoStart>c__Iterator10::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral49;
extern Il2CppCodeGenString* _stringLiteral50;
extern Il2CppCodeGenString* _stringLiteral51;
extern const uint32_t U3CVideoStartU3Ec__Iterator10_MoveNext_m1830373257_MetadataUsageId;
extern "C"  bool U3CVideoStartU3Ec__Iterator10_MoveNext_m1830373257 (U3CVideoStartU3Ec__Iterator10_t4263606752 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CVideoStartU3Ec__Iterator10_MoveNext_m1830373257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_0145;
	}

IL_0021:
	{
		WaitForSeconds_t1615819279 * L_2 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_2, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_3(L_2);
		__this->set_U24PC_2(1);
		goto IL_0147;
	}

IL_003d:
	{
		GreatmanMessage_t1817744250 * L_3 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_3);
		String_t* L_4 = L_3->get__type_9();
		NullCheck(L_4);
		bool L_5 = String_Equals_m3541721061(L_4, _stringLiteral49, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_007f;
		}
	}
	{
		GreatmanMessage_t1817744250 * L_6 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_6);
		GameObject_t3674682005 * L_7 = L_6->get_ProgressBar_19();
		NullCheck(L_7);
		GameObject_SetActive_m3538205401(L_7, (bool)0, /*hidden argument*/NULL);
		GreatmanMessage_t1817744250 * L_8 = __this->get_U3CU3Ef__this_4();
		GreatmanMessage_t1817744250 * L_9 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_9);
		Il2CppObject * L_10 = GreatmanMessage_ImageLoad_m1200707128(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		MonoBehaviour_StartCoroutine_m2135303124(L_8, L_10, /*hidden argument*/NULL);
	}

IL_007f:
	{
		GreatmanMessage_t1817744250 * L_11 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_11);
		String_t* L_12 = L_11->get__type_9();
		NullCheck(L_12);
		bool L_13 = String_Equals_m3541721061(L_12, _stringLiteral50, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00cb;
		}
	}
	{
		GreatmanMessage_t1817744250 * L_14 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_14);
		GameObject_t3674682005 * L_15 = L_14->get_ProgressBar_19();
		NullCheck(L_15);
		GameObject_SetActive_m3538205401(L_15, (bool)0, /*hidden argument*/NULL);
		GreatmanMessage_t1817744250 * L_16 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_16);
		String_t* L_17 = L_16->get__video_localpath_10();
		CtrlPlugins_isWikitude_video_m3097634024(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		GreatmanMessage_t1817744250 * L_18 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_18);
		GameObject_t3674682005 * L_19 = L_18->get_panel_18();
		NullCheck(L_19);
		GameObject_SetActive_m3538205401(L_19, (bool)1, /*hidden argument*/NULL);
	}

IL_00cb:
	{
		GreatmanMessage_t1817744250 * L_20 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_20);
		String_t* L_21 = L_20->get__type_9();
		NullCheck(L_21);
		bool L_22 = String_Equals_m3541721061(L_21, _stringLiteral51, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_013e;
		}
	}
	{
		GreatmanMessage_t1817744250 * L_23 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_23);
		GameObject_t3674682005 * L_24 = L_23->get_ProgressBar_19();
		NullCheck(L_24);
		GameObject_SetActive_m3538205401(L_24, (bool)0, /*hidden argument*/NULL);
		GreatmanMessage_t1817744250 * L_25 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_25);
		String_t* L_26 = L_25->get__latitude_7();
		float L_27 = Single_Parse_m3022284664(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		__this->set_U3Clatitude_wikitudeU3E__0_0(L_27);
		GreatmanMessage_t1817744250 * L_28 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_28);
		String_t* L_29 = L_28->get__longtitude_8();
		float L_30 = Single_Parse_m3022284664(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		__this->set_U3Clongtitude_wikitudeU3E__1_1(L_30);
		GreatmanMessage_t1817744250 * L_31 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_31);
		String_t* L_32 = L_31->get__video_localpath_10();
		float L_33 = __this->get_U3Clatitude_wikitudeU3E__0_0();
		float L_34 = __this->get_U3Clongtitude_wikitudeU3E__1_1();
		CtrlPlugins_isWikitude_m2708272430(NULL /*static, unused*/, L_32, L_33, L_34, /*hidden argument*/NULL);
	}

IL_013e:
	{
		__this->set_U24PC_2((-1));
	}

IL_0145:
	{
		return (bool)0;
	}

IL_0147:
	{
		return (bool)1;
	}
	// Dead block : IL_0149: ldloc.1
}
// System.Void GreatmanMessage/<VideoStart>c__Iterator10::Dispose()
extern "C"  void U3CVideoStartU3Ec__Iterator10_Dispose_m3719253256 (U3CVideoStartU3Ec__Iterator10_t4263606752 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void GreatmanMessage/<VideoStart>c__Iterator10::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CVideoStartU3Ec__Iterator10_Reset_m81691192_MetadataUsageId;
extern "C"  void U3CVideoStartU3Ec__Iterator10_Reset_m81691192 (U3CVideoStartU3Ec__Iterator10_t4263606752 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CVideoStartU3Ec__Iterator10_Reset_m81691192_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void iosWebView::.ctor()
extern "C"  void iosWebView__ctor_m2508658383 (iosWebView_t3524785996 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void iosWebView::.cctor()
extern "C"  void iosWebView__cctor_m4271869630 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// iosWebView iosWebView::get_Instance()
extern Il2CppClass* iosWebView_t3524785996_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4033864337;
extern const uint32_t iosWebView_get_Instance_m3613006564_MetadataUsageId;
extern "C"  iosWebView_t3524785996 * iosWebView_get_Instance_m3613006564 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iosWebView_get_Instance_m3613006564_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(iosWebView_t3524785996_il2cpp_TypeInfo_var);
		iosWebView_t3524785996 * L_0 = ((iosWebView_t3524785996_StaticFields*)iosWebView_t3524785996_il2cpp_TypeInfo_var->static_fields)->get__instance_4();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral4033864337, /*hidden argument*/NULL);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(iosWebView_t3524785996_il2cpp_TypeInfo_var);
		iosWebView_t3524785996 * L_2 = ((iosWebView_t3524785996_StaticFields*)iosWebView_t3524785996_il2cpp_TypeInfo_var->static_fields)->get__instance_4();
		return L_2;
	}
}
// System.Void iosWebView::Awake()
extern Il2CppClass* iosWebView_t3524785996_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral76880356;
extern Il2CppCodeGenString* _stringLiteral4288499021;
extern const uint32_t iosWebView_Awake_m2746263602_MetadataUsageId;
extern "C"  void iosWebView_Awake_m2746263602 (iosWebView_t3524785996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iosWebView_Awake_m2746263602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(iosWebView_t3524785996_il2cpp_TypeInfo_var);
		((iosWebView_t3524785996_StaticFields*)iosWebView_t3524785996_il2cpp_TypeInfo_var->static_fields)->set__instance_4(__this);
		GameObject_t3674682005 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral76880356, /*hidden argument*/NULL);
		__this->set_panel_5(L_0);
		GameObject_t3674682005 * L_1 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral4288499021, /*hidden argument*/NULL);
		__this->set_webBackButton_6(L_1);
		GameObject_t3674682005 * L_2 = __this->get_webBackButton_6();
		NullCheck(L_2);
		GameObject_SetActive_m3538205401(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_3 = __this->get_panel_5();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void iosWebView::Act(System.String)
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern Il2CppClass* iosWebView_t3524785996_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t403047693_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisWebViewObject_t388577433_m1260926804_MethodInfo_var;
extern const MethodInfo* iosWebView_U3CActU3Em__6_m1814597860_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3125221195_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral388577432;
extern const uint32_t iosWebView_Act_m2518930019_MetadataUsageId;
extern "C"  void iosWebView_Act_m2518930019 (iosWebView_t3524785996 * __this, String_t* ___url0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iosWebView_Act_m2518930019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WebViewObject_t388577433 * G_B2_0 = NULL;
	WebViewObject_t388577433 * G_B1_0 = NULL;
	{
		GameObject_t3674682005 * L_0 = __this->get_panel_5();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_1 = __this->get_webBackButton_6();
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, (bool)1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_2 = (GameObject_t3674682005 *)il2cpp_codegen_object_new(GameObject_t3674682005_il2cpp_TypeInfo_var);
		GameObject__ctor_m3920833606(L_2, _stringLiteral388577432, /*hidden argument*/NULL);
		NullCheck(L_2);
		WebViewObject_t388577433 * L_3 = GameObject_AddComponent_TisWebViewObject_t388577433_m1260926804(L_2, /*hidden argument*/GameObject_AddComponent_TisWebViewObject_t388577433_m1260926804_MethodInfo_var);
		__this->set_webViewObject_3(L_3);
		WebViewObject_t388577433 * L_4 = __this->get_webViewObject_3();
		IL2CPP_RUNTIME_CLASS_INIT(iosWebView_t3524785996_il2cpp_TypeInfo_var);
		Action_1_t403047693 * L_5 = ((iosWebView_t3524785996_StaticFields*)iosWebView_t3524785996_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_7();
		G_B1_0 = L_4;
		if (L_5)
		{
			G_B2_0 = L_4;
			goto IL_004b;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)iosWebView_U3CActU3Em__6_m1814597860_MethodInfo_var);
		Action_1_t403047693 * L_7 = (Action_1_t403047693 *)il2cpp_codegen_object_new(Action_1_t403047693_il2cpp_TypeInfo_var);
		Action_1__ctor_m3125221195(L_7, NULL, L_6, /*hidden argument*/Action_1__ctor_m3125221195_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(iosWebView_t3524785996_il2cpp_TypeInfo_var);
		((iosWebView_t3524785996_StaticFields*)iosWebView_t3524785996_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache5_7(L_7);
		G_B2_0 = G_B1_0;
	}

IL_004b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(iosWebView_t3524785996_il2cpp_TypeInfo_var);
		Action_1_t403047693 * L_8 = ((iosWebView_t3524785996_StaticFields*)iosWebView_t3524785996_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_7();
		NullCheck(G_B2_0);
		WebViewObject_Init_m700700128(G_B2_0, L_8, (bool)0, /*hidden argument*/NULL);
		WebViewObject_t388577433 * L_9 = __this->get_webViewObject_3();
		String_t* L_10 = ___url0;
		NullCheck(L_9);
		WebViewObject_LoadURL_m3283687400(L_9, L_10, /*hidden argument*/NULL);
		WebViewObject_t388577433 * L_11 = __this->get_webViewObject_3();
		NullCheck(L_11);
		WebViewObject_SetMargins_m3805796466(L_11, 0, 0, 0, ((int32_t)105), /*hidden argument*/NULL);
		WebViewObject_t388577433 * L_12 = __this->get_webViewObject_3();
		NullCheck(L_12);
		WebViewObject_SetVisibility_m927614396(L_12, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void iosWebView::WebBack()
extern Il2CppCodeGenString* _stringLiteral388577432;
extern const uint32_t iosWebView_WebBack_m3641098376_MetadataUsageId;
extern "C"  void iosWebView_WebBack_m3641098376 (iosWebView_t3524785996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iosWebView_WebBack_m3641098376_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		GameObject_t3674682005 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral388577432, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t3674682005 * L_1 = V_0;
		Object_Destroy_m176400816(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_2 = __this->get_panel_5();
		NullCheck(L_2);
		GameObject_SetActive_m3538205401(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_3 = __this->get_webBackButton_6();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void iosWebView::<Act>m__6(System.String)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t iosWebView_U3CActU3Em__6_m1814597860_MetadataUsageId;
extern "C"  void iosWebView_U3CActU3Em__6_m1814597860 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iosWebView_U3CActU3Em__6_m1814597860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___msg0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Loading::.ctor()
extern "C"  void Loading__ctor_m3414177231 (Loading_t2001303836 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Loading::Start()
extern Il2CppClass* AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var;
extern const MethodInfo* AndroidJavaObject_GetStatic_TisAndroidJavaObject_t2362096582_m524545334_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2952365502;
extern Il2CppCodeGenString* _stringLiteral597527464;
extern const uint32_t Loading_Start_m2361315023_MetadataUsageId;
extern "C"  void Loading_Start_m2361315023 (Loading_t2001303836 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Loading_Start_m2361315023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AndroidJavaClass_t1816259147 * V_0 = NULL;
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0028;
		}
	}
	{
		AndroidJavaClass_t1816259147 * L_1 = (AndroidJavaClass_t1816259147 *)il2cpp_codegen_object_new(AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m2757518396(L_1, _stringLiteral2952365502, /*hidden argument*/NULL);
		V_0 = L_1;
		AndroidJavaClass_t1816259147 * L_2 = V_0;
		NullCheck(L_2);
		AndroidJavaObject_t2362096582 * L_3 = AndroidJavaObject_GetStatic_TisAndroidJavaObject_t2362096582_m524545334(L_2, _stringLiteral597527464, /*hidden argument*/AndroidJavaObject_GetStatic_TisAndroidJavaObject_t2362096582_m524545334_MethodInfo_var);
		__this->set__activity_2(L_3);
	}

IL_0028:
	{
		Il2CppObject * L_4 = Loading_Load_m1257196979(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Loading::Load()
extern Il2CppClass* U3CLoadU3Ec__Iterator13_t1639883204_il2cpp_TypeInfo_var;
extern const uint32_t Loading_Load_m1257196979_MetadataUsageId;
extern "C"  Il2CppObject * Loading_Load_m1257196979 (Loading_t2001303836 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Loading_Load_m1257196979_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoadU3Ec__Iterator13_t1639883204 * V_0 = NULL;
	{
		U3CLoadU3Ec__Iterator13_t1639883204 * L_0 = (U3CLoadU3Ec__Iterator13_t1639883204 *)il2cpp_codegen_object_new(U3CLoadU3Ec__Iterator13_t1639883204_il2cpp_TypeInfo_var);
		U3CLoadU3Ec__Iterator13__ctor_m1834599975(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadU3Ec__Iterator13_t1639883204 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_4(__this);
		U3CLoadU3Ec__Iterator13_t1639883204 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Loading/<Load>c__Iterator13::.ctor()
extern "C"  void U3CLoadU3Ec__Iterator13__ctor_m1834599975 (U3CLoadU3Ec__Iterator13_t1639883204 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Loading/<Load>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2542970123 (U3CLoadU3Ec__Iterator13_t1639883204 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object Loading/<Load>c__Iterator13::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m3406700191 (U3CLoadU3Ec__Iterator13_t1639883204 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean Loading/<Load>c__Iterator13::MoveNext()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3343801;
extern Il2CppCodeGenString* _stringLiteral3529469;
extern Il2CppCodeGenString* _stringLiteral1800556493;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t U3CLoadU3Ec__Iterator13_MoveNext_m87464045_MetadataUsageId;
extern "C"  bool U3CLoadU3Ec__Iterator13_MoveNext_m87464045 (U3CLoadU3Ec__Iterator13_t1639883204 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadU3Ec__Iterator13_MoveNext_m87464045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_007c;
		}
		if (L_1 == 2)
		{
			goto IL_00c9;
		}
	}
	{
		goto IL_00e0;
	}

IL_0025:
	{
		int32_t L_2 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_009c;
		}
	}
	{
		AsyncOperation_t3699081103 * L_3 = SceneManager_LoadSceneAsync_m1034954248(NULL /*static, unused*/, _stringLiteral3343801, /*hidden argument*/NULL);
		__this->set_U3CasyncU3E__0_0(L_3);
		Loading_t2001303836 * L_4 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_4);
		AndroidJavaObject_t2362096582 * L_5 = L_4->get__activity_2();
		ObjectU5BU5D_t1108656482* L_6 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, _stringLiteral1800556493);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1800556493);
		NullCheck(L_5);
		AndroidJavaObject_Call_m3151096341(L_5, _stringLiteral3529469, L_6, /*hidden argument*/NULL);
		AsyncOperation_t3699081103 * L_7 = __this->get_U3CasyncU3E__0_0();
		__this->set_U24current_3(L_7);
		__this->set_U24PC_2(1);
		goto IL_00e2;
	}

IL_007c:
	{
		Loading_t2001303836 * L_8 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_8);
		AndroidJavaObject_t2362096582 * L_9 = L_8->get__activity_2();
		NullCheck(L_9);
		AndroidJavaObject_Call_m3151096341(L_9, _stringLiteral3202370, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		goto IL_00d9;
	}

IL_009c:
	{
		AsyncOperation_t3699081103 * L_10 = SceneManager_LoadSceneAsync_m1034954248(NULL /*static, unused*/, _stringLiteral3343801, /*hidden argument*/NULL);
		__this->set_U3CasyncU3E__1_1(L_10);
		goto IL_00c9;
	}

IL_00b1:
	{
		bool L_11 = ((bool)1);
		Il2CppObject * L_12 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_11);
		__this->set_U24current_3(L_12);
		__this->set_U24PC_2(2);
		goto IL_00e2;
	}

IL_00c9:
	{
		AsyncOperation_t3699081103 * L_13 = __this->get_U3CasyncU3E__1_1();
		NullCheck(L_13);
		bool L_14 = AsyncOperation_get_isDone_m2747591837(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00b1;
		}
	}

IL_00d9:
	{
		__this->set_U24PC_2((-1));
	}

IL_00e0:
	{
		return (bool)0;
	}

IL_00e2:
	{
		return (bool)1;
	}
	// Dead block : IL_00e4: ldloc.1
}
// System.Void Loading/<Load>c__Iterator13::Dispose()
extern "C"  void U3CLoadU3Ec__Iterator13_Dispose_m2012267684 (U3CLoadU3Ec__Iterator13_t1639883204 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void Loading/<Load>c__Iterator13::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadU3Ec__Iterator13_Reset_m3776000212_MetadataUsageId;
extern "C"  void U3CLoadU3Ec__Iterator13_Reset_m3776000212 (U3CLoadU3Ec__Iterator13_t1639883204 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadU3Ec__Iterator13_Reset_m3776000212_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void LoginCheck::.ctor()
extern "C"  void LoginCheck__ctor_m899003452 (LoginCheck_t219618111 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LoginCheck::.cctor()
extern Il2CppClass* LoginCheck_t219618111_il2cpp_TypeInfo_var;
extern const uint32_t LoginCheck__cctor_m1617207025_MetadataUsageId;
extern "C"  void LoginCheck__cctor_m1617207025 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LoginCheck__cctor_m1617207025_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((LoginCheck_t219618111_StaticFields*)LoginCheck_t219618111_il2cpp_TypeInfo_var->static_fields)->set__LoginResult_4(2);
		return;
	}
}
// System.Int32 LoginCheck::get_LoginResult()
extern Il2CppClass* LoginCheck_t219618111_il2cpp_TypeInfo_var;
extern const uint32_t LoginCheck_get_LoginResult_m2321846537_MetadataUsageId;
extern "C"  int32_t LoginCheck_get_LoginResult_m2321846537 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LoginCheck_get_LoginResult_m2321846537_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LoginCheck_t219618111_il2cpp_TypeInfo_var);
		int32_t L_0 = ((LoginCheck_t219618111_StaticFields*)LoginCheck_t219618111_il2cpp_TypeInfo_var->static_fields)->get__LoginResult_4();
		return L_0;
	}
}
// System.Void LoginCheck::set_LoginResult(System.Int32)
extern Il2CppClass* LoginCheck_t219618111_il2cpp_TypeInfo_var;
extern const uint32_t LoginCheck_set_LoginResult_m2617684276_MetadataUsageId;
extern "C"  void LoginCheck_set_LoginResult_m2617684276 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LoginCheck_set_LoginResult_m2617684276_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LoginCheck_t219618111_il2cpp_TypeInfo_var);
		int32_t L_0 = ((LoginCheck_t219618111_StaticFields*)LoginCheck_t219618111_il2cpp_TypeInfo_var->static_fields)->get__LoginResult_4();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(LoginCheck_t219618111_il2cpp_TypeInfo_var);
		((LoginCheck_t219618111_StaticFields*)LoginCheck_t219618111_il2cpp_TypeInfo_var->static_fields)->set__LoginResult_4(L_2);
	}

IL_0011:
	{
		return;
	}
}
// System.Void LoginCheck::Awake()
extern "C"  void LoginCheck_Awake_m1136608671 (LoginCheck_t219618111 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LoginCheck::Update()
extern "C"  void LoginCheck_Update_m3826165329 (LoginCheck_t219618111 * __this, const MethodInfo* method)
{
	{
		InputField_t609046876 * L_0 = __this->get_m_ID_2();
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m3972300732(L_0, /*hidden argument*/NULL);
		PrivateURL_set_ID_m2762899157(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		InputField_t609046876 * L_2 = __this->get_m_PW_3();
		NullCheck(L_2);
		String_t* L_3 = InputField_get_text_m3972300732(L_2, /*hidden argument*/NULL);
		PrivateURL_set_PW_m2535917673(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main::.ctor()
extern "C"  void Main__ctor_m2928940258 (Main_t2390489 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main::Awake()
extern Il2CppClass* ApplicationChrome_t3150366666_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisEventSystem_t2276120119_m1517929633_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1779670345;
extern const uint32_t Main_Awake_m3166545477_MetadataUsageId;
extern "C"  void Main_Awake_m3166545477 (Main_t2390489 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Main_Awake_m3166545477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ApplicationChrome_t3150366666_il2cpp_TypeInfo_var);
		ApplicationChrome_set_statusBarState_m3496748297(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_State_m3490075940(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		EventSystem_t2276120119 * L_0 = __this->get_m_event_2();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		GameObject_t3674682005 * L_2 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral1779670345, /*hidden argument*/NULL);
		NullCheck(L_2);
		EventSystem_t2276120119 * L_3 = GameObject_GetComponent_TisEventSystem_t2276120119_m1517929633(L_2, /*hidden argument*/GameObject_GetComponent_TisEventSystem_t2276120119_m1517929633_MethodInfo_var);
		__this->set_m_event_2(L_3);
	}

IL_0038:
	{
		return;
	}
}
// System.Void Main::Start()
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t Main_Start_m1876078050_MetadataUsageId;
extern "C"  void Main_Start_m1876078050 (Main_t2390489 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Main_Start_m1876078050_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_1 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		AndroidWebview_Call_m1677830609(L_1, _stringLiteral3202370, /*hidden argument*/NULL);
		EventSystem_t2276120119 * L_2 = __this->get_m_event_2();
		float L_3 = Screen_get_dpi_m3780069159(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		EventSystem_set_pixelDragThreshold_m3094844391(L_2, (((int32_t)((int32_t)((float)((float)((float)((float)(0.5f)*(float)L_3))/(float)(2.54f)))))), /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void MapUnityView::.ctor()
extern "C"  void MapUnityView__ctor_m2422723197 (MapUnityView_t4158937566 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MapUnityView::.cctor()
extern "C"  void MapUnityView__cctor_m1607878864 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MapUnityView::Awake()
extern Il2CppCodeGenString* _stringLiteral76880356;
extern Il2CppCodeGenString* _stringLiteral3023715669;
extern Il2CppCodeGenString* _stringLiteral2306211395;
extern const uint32_t MapUnityView_Awake_m2660328416_MetadataUsageId;
extern "C"  void MapUnityView_Awake_m2660328416 (MapUnityView_t4158937566 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MapUnityView_Awake_m2660328416_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral76880356, /*hidden argument*/NULL);
		__this->set_panel_2(L_0);
		GameObject_t3674682005 * L_1 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3023715669, /*hidden argument*/NULL);
		__this->set_mapBackButton_3(L_1);
		GameObject_t3674682005 * L_2 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2306211395, /*hidden argument*/NULL);
		__this->set_messageChekMapBackButton_4(L_2);
		GameObject_t3674682005 * L_3 = __this->get_mapBackButton_3();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_4 = __this->get_messageChekMapBackButton_4();
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MapUnityView::Start()
extern "C"  void MapUnityView_Start_m1369860989 (MapUnityView_t4158937566 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MapUnityView::MapOpen()
extern "C"  void MapUnityView_MapOpen_m806408545 (MapUnityView_t4158937566 * __this, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = __this->get_panel_2();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_1 = __this->get_mapBackButton_3();
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MapUnityView::MapClose()
extern "C"  void MapUnityView_MapClose_m1358645027 (MapUnityView_t4158937566 * __this, const MethodInfo* method)
{
	{
		CtrlPlugins_Close_m2985654872(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_0 = __this->get_panel_2();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_1 = __this->get_mapBackButton_3();
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MapUnityView::MeChekOpen()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1492158093;
extern const uint32_t MapUnityView_MeChekOpen_m3898353108_MetadataUsageId;
extern "C"  void MapUnityView_MeChekOpen_m3898353108 (MapUnityView_t4158937566 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MapUnityView_MeChekOpen_m3898353108_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1492158093, /*hidden argument*/NULL);
		CtrlPlugins_isMKMapAction_m1390583882(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_0 = __this->get_panel_2();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MapUnityView::MeChekClose()
extern "C"  void MapUnityView_MeChekClose_m2719645968 (MapUnityView_t4158937566 * __this, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = __this->get_panel_2();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MessageCheck::.ctor()
extern "C"  void MessageCheck__ctor_m3855891034 (MessageCheck_t3146986849 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MessageCheck::Awake()
extern Il2CppCodeGenString* _stringLiteral76880356;
extern const uint32_t MessageCheck_Awake_m4093496253_MetadataUsageId;
extern "C"  void MessageCheck_Awake_m4093496253 (MessageCheck_t3146986849 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_Awake_m4093496253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral76880356, /*hidden argument*/NULL);
		__this->set_panel_2(L_0);
		return;
	}
}
// System.Void MessageCheck::Start()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DirectoryInfo_t89154617_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral41830;
extern const uint32_t MessageCheck_Start_m2803028826_MetadataUsageId;
extern "C"  void MessageCheck_Start_m2803028826 (MessageCheck_t3146986849 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_Start_m2803028826_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DirectoryInfo_t89154617 * V_0 = NULL;
	FileInfoU5BU5D_t3358688287* V_1 = NULL;
	FileInfo_t3233670074 * V_2 = NULL;
	FileInfoU5BU5D_t3358688287* V_3 = NULL;
	int32_t V_4 = 0;
	{
		String_t* L_0 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, L_0, _stringLiteral47, /*hidden argument*/NULL);
		DirectoryInfo_t89154617 * L_2 = (DirectoryInfo_t89154617 *)il2cpp_codegen_object_new(DirectoryInfo_t89154617_il2cpp_TypeInfo_var);
		DirectoryInfo__ctor_m4029233824(L_2, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		DirectoryInfo_t89154617 * L_3 = V_0;
		NullCheck(L_3);
		FileInfoU5BU5D_t3358688287* L_4 = DirectoryInfo_GetFiles_m2151933877(L_3, _stringLiteral41830, /*hidden argument*/NULL);
		V_1 = L_4;
		FileInfoU5BU5D_t3358688287* L_5 = V_1;
		V_3 = L_5;
		V_4 = 0;
		goto IL_0050;
	}

IL_002b:
	{
		FileInfoU5BU5D_t3358688287* L_6 = V_3;
		int32_t L_7 = V_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		FileInfo_t3233670074 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_2 = L_9;
		String_t* L_10 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		FileInfo_t3233670074 * L_11 = V_2;
		NullCheck(L_11);
		String_t* L_12 = FileInfo_get_Name_m3553897244(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1825781833(NULL /*static, unused*/, L_10, _stringLiteral47, L_12, /*hidden argument*/NULL);
		File_Delete_m760984832(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_4;
		V_4 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0050:
	{
		int32_t L_15 = V_4;
		FileInfoU5BU5D_t3358688287* L_16 = V_3;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		__this->set_timer_26((0.0f));
		__this->set_waitingTime_27(3);
		return;
	}
}
// System.Void MessageCheck::ButtonClick(System.Int32)
extern Il2CppClass* MessageCheck_t3146986849_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentScreen_t3026574117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral47602;
extern Il2CppCodeGenString* _stringLiteral2674321230;
extern const uint32_t MessageCheck_ButtonClick_m2729009791_MetadataUsageId;
extern "C"  void MessageCheck_ButtonClick_m2729009791 (MessageCheck_t3146986849 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_ButtonClick_m2729009791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		MessagesU5BU5D_t3583122277* L_0 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get_m_Messages_6();
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		MessagesU5BU5D_t3583122277* L_1 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get_m_Messages_6();
		NullCheck(L_1);
		int32_t L_2 = ___index0;
		if ((((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))-(int32_t)1))) >= ((int32_t)L_2)))
		{
			goto IL_0022;
		}
	}
	{
		__this->set__messagecheck_17((bool)0);
		return;
	}

IL_0022:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_3 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)50)))))
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_4 = ___index0;
		((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->set__index_25(L_4);
		__this->set__messagecheck_17((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentScreen_t3026574117_il2cpp_TypeInfo_var);
		CurrentScreen_t3026574117 * L_5 = CurrentScreen_get_Instance_m3161187310(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		CurrentScreen_VisibleFalseState_m2603065973(L_5, ((int32_t)50), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, ((int32_t)50), /*hidden argument*/NULL);
		goto IL_0072;
	}

IL_0053:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_6 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)20)))))
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentScreen_t3026574117_il2cpp_TypeInfo_var);
		CurrentScreen_t3026574117 * L_7 = CurrentScreen_get_Instance_m3161187310(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		CurrentScreen_VisibleFalseState_m2603065973(L_7, ((int32_t)20), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, ((int32_t)20), /*hidden argument*/NULL);
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentScreen_t3026574117_il2cpp_TypeInfo_var);
		CurrentScreen_t3026574117 * L_8 = CurrentScreen_get_Instance_m3161187310(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		CurrentScreen_State_m3173002564(L_8, ((int32_t)27), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_9 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)2))))
		{
			goto IL_009a;
		}
	}
	{
		Button_t3896396478 * L_10 = __this->get_EmailInsertBtn_10();
		NullCheck(L_10);
		Selectable_set_interactable_m2686686419(L_10, (bool)1, /*hidden argument*/NULL);
		goto IL_00cc;
	}

IL_009a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_11 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_00b5;
		}
	}
	{
		Button_t3896396478 * L_12 = __this->get_EmailInsertBtn_10();
		NullCheck(L_12);
		Selectable_set_interactable_m2686686419(L_12, (bool)0, /*hidden argument*/NULL);
		goto IL_00cc;
	}

IL_00b5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_13 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)1))))
		{
			goto IL_00cc;
		}
	}
	{
		Button_t3896396478 * L_14 = __this->get_EmailInsertBtn_10();
		NullCheck(L_14);
		Selectable_set_interactable_m2686686419(L_14, (bool)1, /*hidden argument*/NULL);
	}

IL_00cc:
	{
		MessagesU5BU5D_t3583122277* L_15 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get_m_Messages_6();
		int32_t L_16 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get__index_25();
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = ((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))->get_MessageCategory_9();
		if ((!(((uint32_t)L_17) == ((uint32_t)1))))
		{
			goto IL_010d;
		}
	}
	{
		Button_t3896396478 * L_18 = __this->get_EmailInsertBtn_10();
		NullCheck(L_18);
		GameObject_t3674682005 * L_19 = Component_get_gameObject_m1170635899(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		GameObject_SetActive_m3538205401(L_19, (bool)0, /*hidden argument*/NULL);
		Button_t3896396478 * L_20 = __this->get__MessageDelete_16();
		NullCheck(L_20);
		GameObject_t3674682005 * L_21 = Component_get_gameObject_m1170635899(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		GameObject_SetActive_m3538205401(L_21, (bool)0, /*hidden argument*/NULL);
		goto IL_012f;
	}

IL_010d:
	{
		Button_t3896396478 * L_22 = __this->get_EmailInsertBtn_10();
		NullCheck(L_22);
		GameObject_t3674682005 * L_23 = Component_get_gameObject_m1170635899(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		GameObject_SetActive_m3538205401(L_23, (bool)1, /*hidden argument*/NULL);
		Button_t3896396478 * L_24 = __this->get__MessageDelete_16();
		NullCheck(L_24);
		GameObject_t3674682005 * L_25 = Component_get_gameObject_m1170635899(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		GameObject_SetActive_m3538205401(L_25, (bool)1, /*hidden argument*/NULL);
	}

IL_012f:
	{
		Button_t3896396478 * L_26 = __this->get_MessageSeeBtn_9();
		NullCheck(L_26);
		Selectable_set_interactable_m2686686419(L_26, (bool)0, /*hidden argument*/NULL);
		Text_t9039225 * L_27 = __this->get_Title_7();
		MessagesU5BU5D_t3583122277* L_28 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get_m_Messages_6();
		int32_t L_29 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get__index_25();
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		String_t* L_30 = ((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_29)))->get_Title_0();
		NullCheck(L_27);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_27, L_30);
		MessagesU5BU5D_t3583122277* L_31 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get_m_Messages_6();
		int32_t L_32 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get__index_25();
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		String_t* L_33 = ((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_32)))->get_type_1();
		__this->set_filetype_24(L_33);
		MessagesU5BU5D_t3583122277* L_34 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get_m_Messages_6();
		int32_t L_35 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get__index_25();
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
		String_t* L_36 = ((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_35)))->get_videourl_5();
		__this->set__videourl_19(L_36);
		MessagesU5BU5D_t3583122277* L_37 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get_m_Messages_6();
		int32_t L_38 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get__index_25();
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_38);
		String_t* L_39 = ((L_37)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38)))->get_videoname_6();
		__this->set__videoname_20(L_39);
		MessagesU5BU5D_t3583122277* L_40 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get_m_Messages_6();
		int32_t L_41 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get__index_25();
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, L_41);
		String_t* L_42 = ((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_41)))->get_lat_3();
		__this->set_lat_21(L_42);
		MessagesU5BU5D_t3583122277* L_43 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get_m_Messages_6();
		int32_t L_44 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get__index_25();
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		String_t* L_45 = ((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44)))->get_lon_4();
		__this->set_lon_22(L_45);
		MessagesU5BU5D_t3583122277* L_46 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get_m_Messages_6();
		int32_t L_47 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get__index_25();
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, L_47);
		String_t* L_48 = ((L_46)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_47)))->get_messageid_8();
		String_t* L_49 = L_48;
		V_0 = L_49;
		__this->set__messageid_23(L_49);
		String_t* L_50 = V_0;
		PrivateURL_set_selectmessageid_m2533335980(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		String_t* L_51 = __this->get_lat_21();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_52 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_0219;
		}
	}
	{
		__this->set_lat_21(_stringLiteral47602);
	}

IL_0219:
	{
		String_t* L_53 = __this->get_lon_22();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_54 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_0234;
		}
	}
	{
		__this->set_lon_22(_stringLiteral47602);
	}

IL_0234:
	{
		String_t* L_55 = __this->get_lat_21();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		double L_56 = Convert_ToDouble_m1719184653(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
		GPSManager_set__TargetLatitude_m470540013(NULL /*static, unused*/, L_56, /*hidden argument*/NULL);
		String_t* L_57 = __this->get_lon_22();
		double L_58 = Convert_ToDouble_m1719184653(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		GPSManager_set__TargetLongitude_m2588667994(NULL /*static, unused*/, L_58, /*hidden argument*/NULL);
		Text_t9039225 * L_59 = __this->get_CurrentDistance_8();
		NullCheck(L_59);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_59, _stringLiteral2674321230);
		GameObject_t3674682005 * L_60 = __this->get__Sprite_15();
		NullCheck(L_60);
		GameObject_SetActive_m3538205401(L_60, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MessageCheck::VideoPlay()
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern const uint32_t MessageCheck_VideoPlay_m1709871751_MetadataUsageId;
extern "C"  void MessageCheck_VideoPlay_m1709871751 (MessageCheck_t3146986849 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_VideoPlay_m1709871751_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = ((int32_t)3);
		Il2CppObject * L_2 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		int32_t L_4 = ((int32_t)2);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_3, _stringLiteral3660319847, L_6, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		Il2CppObject * L_7 = MessageCheck_VideoDown_m622930733(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MessageCheck::MapView()
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern Il2CppCodeGenString* _stringLiteral1474523267;
extern Il2CppCodeGenString* _stringLiteral47;
extern const uint32_t MessageCheck_MapView_m3885113241_MetadataUsageId;
extern "C"  void MessageCheck_MapView_m3885113241 (MessageCheck_t3146986849 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_MapView_m3885113241_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		int32_t L_0 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = ((int32_t)3);
		Il2CppObject * L_2 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		int32_t L_4 = ((int32_t)2);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_3, _stringLiteral3660319847, L_6, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_7 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = __this->get_lat_21();
		String_t* L_9 = __this->get_lon_22();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1825781833(NULL /*static, unused*/, L_8, _stringLiteral47, L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		AndroidWebview_Call_m335597645(L_7, _stringLiteral1474523267, L_10, /*hidden argument*/NULL);
		String_t* L_11 = __this->get_lat_21();
		float L_12 = Single_Parse_m3022284664(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		String_t* L_13 = __this->get_lon_22();
		float L_14 = Single_Parse_m3022284664(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		float L_15 = V_0;
		float L_16 = V_1;
		CtrlPlugins_Open_m1700334148(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator MessageCheck::VideoDown()
extern Il2CppClass* U3CVideoDownU3Ec__Iterator14_t3182108037_il2cpp_TypeInfo_var;
extern const uint32_t MessageCheck_VideoDown_m622930733_MetadataUsageId;
extern "C"  Il2CppObject * MessageCheck_VideoDown_m622930733 (MessageCheck_t3146986849 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_VideoDown_m622930733_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CVideoDownU3Ec__Iterator14_t3182108037 * V_0 = NULL;
	{
		U3CVideoDownU3Ec__Iterator14_t3182108037 * L_0 = (U3CVideoDownU3Ec__Iterator14_t3182108037 *)il2cpp_codegen_object_new(U3CVideoDownU3Ec__Iterator14_t3182108037_il2cpp_TypeInfo_var);
		U3CVideoDownU3Ec__Iterator14__ctor_m3844436358(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CVideoDownU3Ec__Iterator14_t3182108037 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_5(__this);
		U3CVideoDownU3Ec__Iterator14_t3182108037 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator MessageCheck::VideoStart(System.Int32,System.String)
extern Il2CppClass* U3CVideoStartU3Ec__Iterator15_t210434316_il2cpp_TypeInfo_var;
extern const uint32_t MessageCheck_VideoStart_m2620798630_MetadataUsageId;
extern "C"  Il2CppObject * MessageCheck_VideoStart_m2620798630 (MessageCheck_t3146986849 * __this, int32_t ___num0, String_t* ___filecategory1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_VideoStart_m2620798630_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CVideoStartU3Ec__Iterator15_t210434316 * V_0 = NULL;
	{
		U3CVideoStartU3Ec__Iterator15_t210434316 * L_0 = (U3CVideoStartU3Ec__Iterator15_t210434316 *)il2cpp_codegen_object_new(U3CVideoStartU3Ec__Iterator15_t210434316_il2cpp_TypeInfo_var);
		U3CVideoStartU3Ec__Iterator15__ctor_m1597995279(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CVideoStartU3Ec__Iterator15_t210434316 * L_1 = V_0;
		int32_t L_2 = ___num0;
		NullCheck(L_1);
		L_1->set_num_0(L_2);
		U3CVideoStartU3Ec__Iterator15_t210434316 * L_3 = V_0;
		String_t* L_4 = ___filecategory1;
		NullCheck(L_3);
		L_3->set_filecategory_3(L_4);
		U3CVideoStartU3Ec__Iterator15_t210434316 * L_5 = V_0;
		int32_t L_6 = ___num0;
		NullCheck(L_5);
		L_5->set_U3CU24U3Enum_7(L_6);
		U3CVideoStartU3Ec__Iterator15_t210434316 * L_7 = V_0;
		String_t* L_8 = ___filecategory1;
		NullCheck(L_7);
		L_7->set_U3CU24U3Efilecategory_8(L_8);
		U3CVideoStartU3Ec__Iterator15_t210434316 * L_9 = V_0;
		NullCheck(L_9);
		L_9->set_U3CU3Ef__this_9(__this);
		U3CVideoStartU3Ec__Iterator15_t210434316 * L_10 = V_0;
		return L_10;
	}
}
// System.Collections.IEnumerator MessageCheck::ImageLoad(System.String)
extern Il2CppClass* U3CImageLoadU3Ec__Iterator16_t4085388203_il2cpp_TypeInfo_var;
extern const uint32_t MessageCheck_ImageLoad_m1220711249_MetadataUsageId;
extern "C"  Il2CppObject * MessageCheck_ImageLoad_m1220711249 (MessageCheck_t3146986849 * __this, String_t* ___file0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_ImageLoad_m1220711249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CImageLoadU3Ec__Iterator16_t4085388203 * V_0 = NULL;
	{
		U3CImageLoadU3Ec__Iterator16_t4085388203 * L_0 = (U3CImageLoadU3Ec__Iterator16_t4085388203 *)il2cpp_codegen_object_new(U3CImageLoadU3Ec__Iterator16_t4085388203_il2cpp_TypeInfo_var);
		U3CImageLoadU3Ec__Iterator16__ctor_m3047818784(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CImageLoadU3Ec__Iterator16_t4085388203 * L_1 = V_0;
		String_t* L_2 = ___file0;
		NullCheck(L_1);
		L_1->set_file_1(L_2);
		U3CImageLoadU3Ec__Iterator16_t4085388203 * L_3 = V_0;
		String_t* L_4 = ___file0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Efile_5(L_4);
		U3CImageLoadU3Ec__Iterator16_t4085388203 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_6(__this);
		U3CImageLoadU3Ec__Iterator16_t4085388203 * L_6 = V_0;
		return L_6;
	}
}
// System.Void MessageCheck::ImageVisible()
extern "C"  void MessageCheck_ImageVisible_m3359338369 (MessageCheck_t3146986849 * __this, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = __this->get__Sprite_15();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator MessageCheck::CheckProgress(UnityEngine.WWW)
extern Il2CppClass* U3CCheckProgressU3Ec__Iterator17_t354264608_il2cpp_TypeInfo_var;
extern const uint32_t MessageCheck_CheckProgress_m2269471373_MetadataUsageId;
extern "C"  Il2CppObject * MessageCheck_CheckProgress_m2269471373 (MessageCheck_t3146986849 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_CheckProgress_m2269471373_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CCheckProgressU3Ec__Iterator17_t354264608 * V_0 = NULL;
	{
		U3CCheckProgressU3Ec__Iterator17_t354264608 * L_0 = (U3CCheckProgressU3Ec__Iterator17_t354264608 *)il2cpp_codegen_object_new(U3CCheckProgressU3Ec__Iterator17_t354264608_il2cpp_TypeInfo_var);
		U3CCheckProgressU3Ec__Iterator17__ctor_m1199092555(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCheckProgressU3Ec__Iterator17_t354264608 * L_1 = V_0;
		WWW_t3134621005 * L_2 = ___www0;
		NullCheck(L_1);
		L_1->set_www_0(L_2);
		U3CCheckProgressU3Ec__Iterator17_t354264608 * L_3 = V_0;
		WWW_t3134621005 * L_4 = ___www0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Ewww_3(L_4);
		U3CCheckProgressU3Ec__Iterator17_t354264608 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_4(__this);
		U3CCheckProgressU3Ec__Iterator17_t354264608 * L_6 = V_0;
		return L_6;
	}
}
// System.Void MessageCheck::emailInsert()
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern Il2CppCodeGenString* _stringLiteral383510407;
extern Il2CppCodeGenString* _stringLiteral3030424555;
extern Il2CppCodeGenString* _stringLiteral4137715065;
extern Il2CppCodeGenString* _stringLiteral3199232591;
extern const uint32_t MessageCheck_emailInsert_m3021129357_MetadataUsageId;
extern "C"  void MessageCheck_emailInsert_m3021129357 (MessageCheck_t3146986849 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_emailInsert_m3021129357_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_1 = ((int32_t)3);
		Il2CppObject * L_2 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		int32_t L_4 = ((int32_t)2);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_3, _stringLiteral3660319847, L_6, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral383510407, _stringLiteral3660319847, /*hidden argument*/NULL);
		return;
	}

IL_003a:
	{
		InputField_t609046876 * L_7 = __this->get_name_input_12();
		NullCheck(L_7);
		String_t* L_8 = InputField_get_text_m3972300732(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_10 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0084;
		}
	}
	{
		int32_t L_11 = ((int32_t)3);
		Il2CppObject * L_12 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_11);
		NullCheck((Enum_t2862688501 *)L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_12);
		int32_t L_14 = ((int32_t)2);
		Il2CppObject * L_15 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_14);
		NullCheck((Enum_t2862688501 *)L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_15);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_13, _stringLiteral3030424555, L_16, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral4137715065, _stringLiteral3030424555, /*hidden argument*/NULL);
		return;
	}

IL_0084:
	{
		InputField_t609046876 * L_17 = __this->get_email_input_11();
		NullCheck(L_17);
		String_t* L_18 = InputField_get_text_m3972300732(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_20 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00ce;
		}
	}
	{
		int32_t L_21 = ((int32_t)3);
		Il2CppObject * L_22 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_21);
		NullCheck((Enum_t2862688501 *)L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_22);
		int32_t L_24 = ((int32_t)2);
		Il2CppObject * L_25 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_24);
		NullCheck((Enum_t2862688501 *)L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_25);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_23, _stringLiteral3199232591, L_26, /*hidden argument*/NULL);
		CtrlPlugins_Alert_ios_m486937204(NULL /*static, unused*/, _stringLiteral4137715065, _stringLiteral3199232591, /*hidden argument*/NULL);
		return;
	}

IL_00ce:
	{
		InputField_t609046876 * L_27 = __this->get_name_input_12();
		NullCheck(L_27);
		String_t* L_28 = InputField_get_text_m3972300732(L_27, /*hidden argument*/NULL);
		InputField_t609046876 * L_29 = __this->get_email_input_11();
		NullCheck(L_29);
		String_t* L_30 = InputField_get_text_m3972300732(L_29, /*hidden argument*/NULL);
		String_t* L_31 = PrivateURL_get_customerId_m4251176570(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_32 = __this->get__messageid_23();
		Il2CppObject * L_33 = MessageCheck_EmailInsertserver_m687391374(__this, L_28, L_30, L_31, L_32, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_33, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator MessageCheck::EmailInsertserver(System.String,System.String,System.String,System.String)
extern Il2CppClass* U3CEmailInsertserverU3Ec__Iterator18_t4256762244_il2cpp_TypeInfo_var;
extern const uint32_t MessageCheck_EmailInsertserver_m687391374_MetadataUsageId;
extern "C"  Il2CppObject * MessageCheck_EmailInsertserver_m687391374 (MessageCheck_t3146986849 * __this, String_t* ___title0, String_t* ___email1, String_t* ___customid2, String_t* ___messageid3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_EmailInsertserver_m687391374_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * V_0 = NULL;
	{
		U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * L_0 = (U3CEmailInsertserverU3Ec__Iterator18_t4256762244 *)il2cpp_codegen_object_new(U3CEmailInsertserverU3Ec__Iterator18_t4256762244_il2cpp_TypeInfo_var);
		U3CEmailInsertserverU3Ec__Iterator18__ctor_m3927714919(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * L_1 = V_0;
		String_t* L_2 = ___title0;
		NullCheck(L_1);
		L_1->set_title_1(L_2);
		U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * L_3 = V_0;
		String_t* L_4 = ___email1;
		NullCheck(L_3);
		L_3->set_email_2(L_4);
		U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * L_5 = V_0;
		String_t* L_6 = ___customid2;
		NullCheck(L_5);
		L_5->set_customid_3(L_6);
		U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * L_7 = V_0;
		String_t* L_8 = ___messageid3;
		NullCheck(L_7);
		L_7->set_messageid_4(L_8);
		U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * L_9 = V_0;
		String_t* L_10 = ___title0;
		NullCheck(L_9);
		L_9->set_U3CU24U3Etitle_8(L_10);
		U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * L_11 = V_0;
		String_t* L_12 = ___email1;
		NullCheck(L_11);
		L_11->set_U3CU24U3Eemail_9(L_12);
		U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * L_13 = V_0;
		String_t* L_14 = ___customid2;
		NullCheck(L_13);
		L_13->set_U3CU24U3Ecustomid_10(L_14);
		U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * L_15 = V_0;
		String_t* L_16 = ___messageid3;
		NullCheck(L_15);
		L_15->set_U3CU24U3Emessageid_11(L_16);
		U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * L_17 = V_0;
		NullCheck(L_17);
		L_17->set_U3CU3Ef__this_12(__this);
		U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * L_18 = V_0;
		return L_18;
	}
}
// System.Collections.IEnumerator MessageCheck::EmailInsertserverProgress(UnityEngine.WWW)
extern Il2CppClass* U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746_il2cpp_TypeInfo_var;
extern const uint32_t MessageCheck_EmailInsertserverProgress_m3801537309_MetadataUsageId;
extern "C"  Il2CppObject * MessageCheck_EmailInsertserverProgress_m3801537309 (MessageCheck_t3146986849 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_EmailInsertserverProgress_m3801537309_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 * V_0 = NULL;
	{
		U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 * L_0 = (U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 *)il2cpp_codegen_object_new(U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746_il2cpp_TypeInfo_var);
		U3CEmailInsertserverProgressU3Ec__Iterator19__ctor_m3795497625(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 * L_1 = V_0;
		WWW_t3134621005 * L_2 = ___www0;
		NullCheck(L_1);
		L_1->set_www_0(L_2);
		U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 * L_3 = V_0;
		WWW_t3134621005 * L_4 = ___www0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Ewww_3(L_4);
		U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 * L_5 = V_0;
		return L_5;
	}
}
// System.Void MessageCheck::EmailInsertserverTakeJson(System.String)
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentScreen_t3026574117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2760346223;
extern Il2CppCodeGenString* _stringLiteral3738031314;
extern Il2CppCodeGenString* _stringLiteral719357562;
extern Il2CppCodeGenString* _stringLiteral15742870;
extern const uint32_t MessageCheck_EmailInsertserverTakeJson_m3006726659_MetadataUsageId;
extern "C"  void MessageCheck_EmailInsertserverTakeJson_m3006726659 (MessageCheck_t3146986849 * __this, String_t* ___js0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_EmailInsertserverTakeJson_m3006726659_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JsonData_t1715015430 * V_0 = NULL;
	{
		String_t* L_0 = ___js0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		JsonData_t1715015430 * L_1 = JsonMapper_ToObject_m3425498490(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___js0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		JsonData_t1715015430 * L_3 = V_0;
		NullCheck(L_3);
		JsonData_t1715015430 * L_4 = JsonData_get_Item_m253158020(L_3, 1, /*hidden argument*/NULL);
		NullCheck(L_4);
		JsonData_t1715015430 * L_5 = JsonData_get_Item_m4009629743(L_4, _stringLiteral2760346223, /*hidden argument*/NULL);
		NullCheck(L_5);
		JsonData_t1715015430 * L_6 = JsonData_get_Item_m4009629743(L_5, _stringLiteral3738031314, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t476798718_il2cpp_TypeInfo_var);
		bool L_8 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0095;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentScreen_t3026574117_il2cpp_TypeInfo_var);
		CurrentScreen_t3026574117 * L_9 = CurrentScreen_get_Instance_m3161187310(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		CurrentScreen_VisibleFalseState_m2603065973(L_9, ((int32_t)20), /*hidden argument*/NULL);
		CurrentScreen_t3026574117 * L_10 = CurrentScreen_get_Instance_m3161187310(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		CurrentScreen_State_m3173002564(L_10, ((int32_t)19), /*hidden argument*/NULL);
		Text_t9039225 * L_11 = __this->get__name_13();
		InputField_t609046876 * L_12 = __this->get_name_input_12();
		NullCheck(L_12);
		String_t* L_13 = InputField_get_text_m3972300732(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral719357562, L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, L_14);
		Text_t9039225 * L_15 = __this->get__email_14();
		InputField_t609046876 * L_16 = __this->get_email_input_11();
		NullCheck(L_16);
		String_t* L_17 = InputField_get_text_m3972300732(L_16, /*hidden argument*/NULL);
		String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral15742870, L_17, /*hidden argument*/NULL);
		NullCheck(L_15);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_15, L_18);
		goto IL_00db;
	}

IL_0095:
	{
		int32_t L_19 = ((int32_t)3);
		Il2CppObject * L_20 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_19);
		NullCheck((Enum_t2862688501 *)L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_20);
		JsonData_t1715015430 * L_22 = V_0;
		NullCheck(L_22);
		JsonData_t1715015430 * L_23 = JsonData_get_Item_m253158020(L_22, 1, /*hidden argument*/NULL);
		NullCheck(L_23);
		JsonData_t1715015430 * L_24 = JsonData_get_Item_m253158020(L_23, 0, /*hidden argument*/NULL);
		NullCheck(L_24);
		JsonData_t1715015430 * L_25 = JsonData_get_Item_m253158020(L_24, 1, /*hidden argument*/NULL);
		NullCheck(L_25);
		JsonData_t1715015430 * L_26 = JsonData_get_Item_m253158020(L_25, 0, /*hidden argument*/NULL);
		NullCheck(L_26);
		JsonData_t1715015430 * L_27 = JsonData_get_Item_m253158020(L_26, 0, /*hidden argument*/NULL);
		NullCheck(L_27);
		JsonData_t1715015430 * L_28 = JsonData_get_Item_m253158020(L_27, 1, /*hidden argument*/NULL);
		NullCheck(L_28);
		String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_28);
		int32_t L_30 = ((int32_t)2);
		Il2CppObject * L_31 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_30);
		NullCheck((Enum_t2862688501 *)L_31);
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_31);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_21, L_29, L_32, /*hidden argument*/NULL);
		return;
	}

IL_00db:
	{
		InputField_t609046876 * L_33 = __this->get_name_input_12();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_33);
		InputField_set_text_m203843887(L_33, L_34, /*hidden argument*/NULL);
		InputField_t609046876 * L_35 = __this->get_email_input_11();
		String_t* L_36 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_35);
		InputField_set_text_m203843887(L_35, L_36, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MessageCheck::MessageDeleteBtn()
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2365905129;
extern Il2CppCodeGenString* _stringLiteral253701288;
extern Il2CppCodeGenString* _stringLiteral396725;
extern const uint32_t MessageCheck_MessageDeleteBtn_m1972104212_MetadataUsageId;
extern "C"  void MessageCheck_MessageDeleteBtn_m1972104212 (MessageCheck_t3146986849 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_MessageDeleteBtn_m1972104212_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m297139618(NULL /*static, unused*/, 2, _stringLiteral2365905129, _stringLiteral253701288, _stringLiteral396725, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_PopupMessage_m2237488056(NULL /*static, unused*/, ((int32_t)11), /*hidden argument*/NULL);
		goto IL_0040;
	}

IL_002d:
	{
		MessageCheck_MessageDeleteCheck_m1649034752(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = MessageCheck_MessageDeleteServerUpdate_m1372336334(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_1, /*hidden argument*/NULL);
	}

IL_0040:
	{
		return;
	}
}
// System.Collections.IEnumerator MessageCheck::MessageDeleteServerUpdate()
extern Il2CppClass* U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995_il2cpp_TypeInfo_var;
extern const uint32_t MessageCheck_MessageDeleteServerUpdate_m1372336334_MetadataUsageId;
extern "C"  Il2CppObject * MessageCheck_MessageDeleteServerUpdate_m1372336334 (MessageCheck_t3146986849 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_MessageDeleteServerUpdate_m1372336334_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995 * V_0 = NULL;
	{
		U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995 * L_0 = (U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995 *)il2cpp_codegen_object_new(U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995_il2cpp_TypeInfo_var);
		U3CMessageDeleteServerUpdateU3Ec__Iterator1A__ctor_m1594179352(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995 * L_1 = V_0;
		return L_1;
	}
}
// System.Void MessageCheck::MessageDeleteCheck()
extern "C"  void MessageCheck_MessageDeleteCheck_m1649034752 (MessageCheck_t3146986849 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = MessageCheck_MessageDeleteServerCheck_m3514863173(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator MessageCheck::MessageDeleteServerCheck()
extern Il2CppClass* U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789_il2cpp_TypeInfo_var;
extern const uint32_t MessageCheck_MessageDeleteServerCheck_m3514863173_MetadataUsageId;
extern "C"  Il2CppObject * MessageCheck_MessageDeleteServerCheck_m3514863173 (MessageCheck_t3146986849 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_MessageDeleteServerCheck_m3514863173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789 * V_0 = NULL;
	{
		U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789 * L_0 = (U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789 *)il2cpp_codegen_object_new(U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789_il2cpp_TypeInfo_var);
		U3CMessageDeleteServerCheckU3Ec__Iterator1B__ctor_m1870680502(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789 * L_2 = V_0;
		return L_2;
	}
}
// System.Void MessageCheck::MessageDeleteTakeJson(System.String)
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2760346223;
extern Il2CppCodeGenString* _stringLiteral3738031314;
extern Il2CppCodeGenString* _stringLiteral4174987542;
extern Il2CppCodeGenString* _stringLiteral2276424892;
extern Il2CppCodeGenString* _stringLiteral4119486321;
extern const uint32_t MessageCheck_MessageDeleteTakeJson_m2005989801_MetadataUsageId;
extern "C"  void MessageCheck_MessageDeleteTakeJson_m2005989801 (MessageCheck_t3146986849 * __this, String_t* ___js0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_MessageDeleteTakeJson_m2005989801_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JsonData_t1715015430 * V_0 = NULL;
	{
		String_t* L_0 = ___js0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		JsonData_t1715015430 * L_1 = JsonMapper_ToObject_m3425498490(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___js0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		Il2CppObject * L_3 = MessageCheck_MessageDeleteServerUpdate_m1372336334(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_3, /*hidden argument*/NULL);
		JsonData_t1715015430 * L_4 = V_0;
		NullCheck(L_4);
		JsonData_t1715015430 * L_5 = JsonData_get_Item_m253158020(L_4, 1, /*hidden argument*/NULL);
		NullCheck(L_5);
		JsonData_t1715015430 * L_6 = JsonData_get_Item_m4009629743(L_5, _stringLiteral2760346223, /*hidden argument*/NULL);
		NullCheck(L_6);
		JsonData_t1715015430 * L_7 = JsonData_get_Item_m4009629743(L_6, _stringLiteral3738031314, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_7);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t476798718_il2cpp_TypeInfo_var);
		bool L_9 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_005e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_10 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		AndroidWebview_Call_m335597645(L_10, _stringLiteral4174987542, _stringLiteral2276424892, /*hidden argument*/NULL);
		goto IL_0072;
	}

IL_005e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_11 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		AndroidWebview_Call_m335597645(L_11, _stringLiteral4174987542, _stringLiteral4119486321, /*hidden argument*/NULL);
	}

IL_0072:
	{
		return;
	}
}
// System.Void MessageCheck::ArView()
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3451549691;
extern const uint32_t MessageCheck_ArView_m1885089376_MetadataUsageId;
extern "C"  void MessageCheck_ArView_m1885089376 (MessageCheck_t3146986849 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_ArView_m1885089376_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_0 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		AndroidWebview_Call_m1677830609(L_0, _stringLiteral3451549691, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MessageCheck::Update()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* MessageCheck_t3146986849_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral12256356;
extern Il2CppCodeGenString* _stringLiteral3426;
extern Il2CppCodeGenString* _stringLiteral109;
extern const uint32_t MessageCheck_Update_m1000399859_MetadataUsageId;
extern "C"  void MessageCheck_Update_m1000399859 (MessageCheck_t3146986849 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageCheck_Update_m1000399859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0090;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)278), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		goto IL_0090;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0081;
		}
	}
	{
		GameObject_t3674682005 * L_3 = __this->get_ProgressBar_3();
		NullCheck(L_3);
		bool L_4 = GameObject_get_activeSelf_m3858025161(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003d;
		}
	}
	{
		return;
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_5 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)20)))))
		{
			goto IL_0054;
		}
	}
	{
		int32_t L_6 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get__index_25();
		MessageCheck_ButtonClick_m2729009791(__this, L_6, /*hidden argument*/NULL);
	}

IL_0054:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_7 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_007c;
		}
	}
	{
		GameObject_t3674682005 * L_8 = __this->get__Sprite_15();
		NullCheck(L_8);
		bool L_9 = GameObject_get_activeSelf_m3858025161(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007c;
		}
	}
	{
		GameObject_t3674682005 * L_10 = __this->get__Sprite_15();
		NullCheck(L_10);
		GameObject_SetActive_m3538205401(L_10, (bool)0, /*hidden argument*/NULL);
	}

IL_007c:
	{
		goto IL_0090;
	}

IL_0081:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_11 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)319), /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0090;
		}
	}

IL_0090:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_12 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_02c5;
		}
	}
	{
		float L_13 = __this->get_timer_26();
		float L_14 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timer_26(((float)((float)L_13+(float)L_14)));
		float L_15 = __this->get_timer_26();
		int32_t L_16 = __this->get_waitingTime_27();
		if ((!(((float)L_15) > ((float)(((float)((float)L_16)))))))
		{
			goto IL_02c5;
		}
	}
	{
		bool L_17 = __this->get__messagecheck_17();
		if (!L_17)
		{
			goto IL_02ae;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_18 = CurrentState_get_GPSPopupMessage_m652265359(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_18) == ((uint32_t)1))))
		{
			goto IL_02ae;
		}
	}
	{
		MessagesU5BU5D_t3583122277* L_19 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get_m_Messages_6();
		int32_t L_20 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get__index_25();
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		String_t* L_21 = ((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_20)))->get_lat_3();
		__this->set_lat_21(L_21);
		MessagesU5BU5D_t3583122277* L_22 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get_m_Messages_6();
		int32_t L_23 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get__index_25();
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		String_t* L_24 = ((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23)))->get_lon_4();
		__this->set_lon_22(L_24);
		String_t* L_25 = __this->get_lat_21();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_26 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_012d;
		}
	}
	{
		GPSManager_set__TargetLatitude_m470540013(NULL /*static, unused*/, (0.0), /*hidden argument*/NULL);
		goto IL_013d;
	}

IL_012d:
	{
		String_t* L_27 = __this->get_lat_21();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		double L_28 = Convert_ToDouble_m1719184653(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		GPSManager_set__TargetLatitude_m470540013(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
	}

IL_013d:
	{
		String_t* L_29 = __this->get_lon_22();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_30 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0160;
		}
	}
	{
		GPSManager_set__TargetLongitude_m2588667994(NULL /*static, unused*/, (0.0), /*hidden argument*/NULL);
		goto IL_0170;
	}

IL_0160:
	{
		String_t* L_31 = __this->get_lon_22();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		double L_32 = Convert_ToDouble_m1719184653(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		GPSManager_set__TargetLongitude_m2588667994(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
	}

IL_0170:
	{
		double L_33 = GPSManager_get__distance_m1549713694(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((double)L_33) > ((double)(10000.0)))))
		{
			goto IL_01ec;
		}
	}
	{
		Button_t3896396478 * L_34 = __this->get_MessageSeeBtn_9();
		NullCheck(L_34);
		Selectable_set_interactable_m2686686419(L_34, (bool)0, /*hidden argument*/NULL);
		double L_35 = GPSManager_get__distance_m1549713694(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__distancedouble_18(L_35);
		double L_36 = __this->get__distancedouble_18();
		__this->set__distancedouble_18(((double)((double)L_36/(double)(1000.0))));
		double L_37 = __this->get__distancedouble_18();
		double L_38 = Math_Round_m2351753873(NULL /*static, unused*/, L_37, 2, /*hidden argument*/NULL);
		__this->set__distancedouble_18(L_38);
		Text_t9039225 * L_39 = __this->get_CurrentDistance_8();
		double* L_40 = __this->get_address_of__distancedouble_18();
		String_t* L_41 = Double_ToString_m3380246633(L_40, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_42 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral12256356, L_41, _stringLiteral3426, /*hidden argument*/NULL);
		NullCheck(L_39);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_39, L_42);
		goto IL_02ae;
	}

IL_01ec:
	{
		double L_43 = GPSManager_get__distance_m1549713694(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((double)L_43) <= ((double)(10000.0)))))
		{
			goto IL_02ae;
		}
	}
	{
		double L_44 = GPSManager_get__distance_m1549713694(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__distancedouble_18(L_44);
		double L_45 = __this->get__distancedouble_18();
		double L_46 = Math_Round_m2351753873(NULL /*static, unused*/, L_45, 2, /*hidden argument*/NULL);
		__this->set__distancedouble_18(L_46);
		double L_47 = GPSManager_get__distance_m1549713694(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((double)L_47) > ((double)(1000.0)))))
		{
			goto IL_0259;
		}
	}
	{
		Text_t9039225 * L_48 = __this->get_CurrentDistance_8();
		double* L_49 = __this->get_address_of__distancedouble_18();
		String_t* L_50 = Double_ToString_m3380246633(L_49, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_51 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral12256356, L_50, _stringLiteral3426, /*hidden argument*/NULL);
		NullCheck(L_48);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_48, L_51);
		goto IL_027e;
	}

IL_0259:
	{
		Text_t9039225 * L_52 = __this->get_CurrentDistance_8();
		double* L_53 = __this->get_address_of__distancedouble_18();
		String_t* L_54 = Double_ToString_m3380246633(L_53, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_55 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral12256356, L_54, _stringLiteral109, /*hidden argument*/NULL);
		NullCheck(L_52);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_52, L_55);
	}

IL_027e:
	{
		double L_56 = GPSManager_get__distance_m1549713694(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((double)L_56) < ((double)(20.0)))))
		{
			goto IL_02a2;
		}
	}
	{
		Button_t3896396478 * L_57 = __this->get_MessageSeeBtn_9();
		NullCheck(L_57);
		Selectable_set_interactable_m2686686419(L_57, (bool)1, /*hidden argument*/NULL);
		goto IL_02ae;
	}

IL_02a2:
	{
		Button_t3896396478 * L_58 = __this->get_MessageSeeBtn_9();
		NullCheck(L_58);
		Selectable_set_interactable_m2686686419(L_58, (bool)0, /*hidden argument*/NULL);
	}

IL_02ae:
	{
		Button_t3896396478 * L_59 = __this->get_MessageSeeBtn_9();
		NullCheck(L_59);
		Selectable_set_interactable_m2686686419(L_59, (bool)1, /*hidden argument*/NULL);
		__this->set_timer_26((0.0f));
	}

IL_02c5:
	{
		return;
	}
}
// System.Void MessageCheck/<CheckProgress>c__Iterator17::.ctor()
extern "C"  void U3CCheckProgressU3Ec__Iterator17__ctor_m1199092555 (U3CCheckProgressU3Ec__Iterator17_t354264608 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object MessageCheck/<CheckProgress>c__Iterator17::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckProgressU3Ec__Iterator17_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4097453863 (U3CCheckProgressU3Ec__Iterator17_t354264608 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object MessageCheck/<CheckProgress>c__Iterator17::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckProgressU3Ec__Iterator17_System_Collections_IEnumerator_get_Current_m400908475 (U3CCheckProgressU3Ec__Iterator17_t354264608 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean MessageCheck/<CheckProgress>c__Iterator17::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentInChildren_TisTMP_Text_t980027659_m2592787986_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral37;
extern Il2CppCodeGenString* _stringLiteral2135970;
extern const uint32_t U3CCheckProgressU3Ec__Iterator17_MoveNext_m267719625_MetadataUsageId;
extern "C"  bool U3CCheckProgressU3Ec__Iterator17_MoveNext_m267719625 (U3CCheckProgressU3Ec__Iterator17_t354264608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckProgressU3Ec__Iterator17_MoveNext_m267719625_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0057;
		}
		if (L_1 == 2)
		{
			goto IL_011a;
		}
	}
	{
		goto IL_0132;
	}

IL_0025:
	{
		MessageCheck_t3146986849 * L_2 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = L_2->get_ProgressBar_3();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)1, /*hidden argument*/NULL);
		goto IL_00af;
	}

IL_003b:
	{
		WaitForSeconds_t1615819279 * L_4 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_4, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_4);
		__this->set_U24PC_1(1);
		goto IL_0134;
	}

IL_0057:
	{
		MessageCheck_t3146986849 * L_5 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = L_5->get_ProgressBar_3();
		NullCheck(L_6);
		TMP_Text_t980027659 * L_7 = GameObject_GetComponentInChildren_TisTMP_Text_t980027659_m2592787986(L_6, /*hidden argument*/GameObject_GetComponentInChildren_TisTMP_Text_t980027659_m2592787986_MethodInfo_var);
		WWW_t3134621005 * L_8 = __this->get_www_0();
		NullCheck(L_8);
		float L_9 = WWW_get_progress_m3186358572(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_10 = bankers_roundf(((float)((float)L_9*(float)(100.0f))));
		V_1 = L_10;
		String_t* L_11 = Single_ToString_m5736032((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m138640077(NULL /*static, unused*/, L_11, _stringLiteral37, /*hidden argument*/NULL);
		NullCheck(L_7);
		TMP_Text_set_text_m27929696(L_7, L_12, /*hidden argument*/NULL);
		MessageCheck_t3146986849 * L_13 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_13);
		Image_t538875265 * L_14 = L_13->get_image_4();
		WWW_t3134621005 * L_15 = __this->get_www_0();
		NullCheck(L_15);
		float L_16 = WWW_get_progress_m3186358572(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		Image_set_fillAmount_m1583793743(L_14, L_16, /*hidden argument*/NULL);
	}

IL_00af:
	{
		WWW_t3134621005 * L_17 = __this->get_www_0();
		NullCheck(L_17);
		bool L_18 = WWW_get_isDone_m634060017(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_003b;
		}
	}
	{
		WWW_t3134621005 * L_19 = __this->get_www_0();
		NullCheck(L_19);
		bool L_20 = WWW_get_isDone_m634060017(L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_012b;
		}
	}
	{
		WWW_t3134621005 * L_21 = __this->get_www_0();
		NullCheck(L_21);
		float L_22 = WWW_get_progress_m3186358572(L_21, /*hidden argument*/NULL);
		if ((!(((float)L_22) >= ((float)(1.0f)))))
		{
			goto IL_012b;
		}
	}
	{
		MessageCheck_t3146986849 * L_23 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_23);
		GameObject_t3674682005 * L_24 = L_23->get_ProgressBar_3();
		NullCheck(L_24);
		TMP_Text_t980027659 * L_25 = GameObject_GetComponentInChildren_TisTMP_Text_t980027659_m2592787986(L_24, /*hidden argument*/GameObject_GetComponentInChildren_TisTMP_Text_t980027659_m2592787986_MethodInfo_var);
		NullCheck(L_25);
		TMP_Text_set_text_m27929696(L_25, _stringLiteral2135970, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_26 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_26, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_26);
		__this->set_U24PC_1(2);
		goto IL_0134;
	}

IL_011a:
	{
		MessageCheck_t3146986849 * L_27 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_27);
		GameObject_t3674682005 * L_28 = L_27->get_ProgressBar_3();
		NullCheck(L_28);
		GameObject_SetActive_m3538205401(L_28, (bool)0, /*hidden argument*/NULL);
	}

IL_012b:
	{
		__this->set_U24PC_1((-1));
	}

IL_0132:
	{
		return (bool)0;
	}

IL_0134:
	{
		return (bool)1;
	}
	// Dead block : IL_0136: ldloc.2
}
// System.Void MessageCheck/<CheckProgress>c__Iterator17::Dispose()
extern "C"  void U3CCheckProgressU3Ec__Iterator17_Dispose_m1174993096 (U3CCheckProgressU3Ec__Iterator17_t354264608 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void MessageCheck/<CheckProgress>c__Iterator17::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckProgressU3Ec__Iterator17_Reset_m3140492792_MetadataUsageId;
extern "C"  void U3CCheckProgressU3Ec__Iterator17_Reset_m3140492792 (U3CCheckProgressU3Ec__Iterator17_t354264608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckProgressU3Ec__Iterator17_Reset_m3140492792_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MessageCheck/<EmailInsertserver>c__Iterator18::.ctor()
extern "C"  void U3CEmailInsertserverU3Ec__Iterator18__ctor_m3927714919 (U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object MessageCheck/<EmailInsertserver>c__Iterator18::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEmailInsertserverU3Ec__Iterator18_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m705126923 (U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_7();
		return L_0;
	}
}
// System.Object MessageCheck/<EmailInsertserver>c__Iterator18::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEmailInsertserverU3Ec__Iterator18_System_Collections_IEnumerator_get_Current_m3499940767 (U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_7();
		return L_0;
	}
}
// System.Boolean MessageCheck/<EmailInsertserver>c__Iterator18::MoveNext()
extern Il2CppClass* WWWForm_t461342257_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral110371416;
extern Il2CppCodeGenString* _stringLiteral96619420;
extern Il2CppCodeGenString* _stringLiteral2522905884;
extern Il2CppCodeGenString* _stringLiteral2604245075;
extern Il2CppCodeGenString* _stringLiteral637620855;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t U3CEmailInsertserverU3Ec__Iterator18_MoveNext_m86372141_MetadataUsageId;
extern "C"  bool U3CEmailInsertserverU3Ec__Iterator18_MoveNext_m86372141 (U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEmailInsertserverU3Ec__Iterator18_MoveNext_m86372141_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00cf;
		}
	}
	{
		goto IL_0146;
	}

IL_0021:
	{
		WWWForm_t461342257 * L_2 = (WWWForm_t461342257 *)il2cpp_codegen_object_new(WWWForm_t461342257_il2cpp_TypeInfo_var);
		WWWForm__ctor_m1417930174(L_2, /*hidden argument*/NULL);
		__this->set_U3CformU3E__0_0(L_2);
		WWWForm_t461342257 * L_3 = __this->get_U3CformU3E__0_0();
		String_t* L_4 = __this->get_title_1();
		NullCheck(L_3);
		WWWForm_AddField_m2890504319(L_3, _stringLiteral110371416, L_4, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_5 = __this->get_U3CformU3E__0_0();
		String_t* L_6 = __this->get_email_2();
		NullCheck(L_5);
		WWWForm_AddField_m2890504319(L_5, _stringLiteral96619420, L_6, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_7 = __this->get_U3CformU3E__0_0();
		String_t* L_8 = __this->get_customid_3();
		NullCheck(L_7);
		WWWForm_AddField_m2890504319(L_7, _stringLiteral2522905884, L_8, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_9 = __this->get_U3CformU3E__0_0();
		String_t* L_10 = __this->get_messageid_4();
		NullCheck(L_9);
		WWWForm_AddField_m2890504319(L_9, _stringLiteral2604245075, L_10, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_11 = __this->get_U3CformU3E__0_0();
		WWW_t3134621005 * L_12 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m3203953640(L_12, _stringLiteral637620855, L_11, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__1_5(L_12);
		MessageCheck_t3146986849 * L_13 = __this->get_U3CU3Ef__this_12();
		MessageCheck_t3146986849 * L_14 = __this->get_U3CU3Ef__this_12();
		WWW_t3134621005 * L_15 = __this->get_U3CwwwU3E__1_5();
		NullCheck(L_14);
		Il2CppObject * L_16 = MessageCheck_EmailInsertserverProgress_m3801537309(L_14, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		MonoBehaviour_StartCoroutine_m2135303124(L_13, L_16, /*hidden argument*/NULL);
		WWW_t3134621005 * L_17 = __this->get_U3CwwwU3E__1_5();
		__this->set_U24current_7(L_17);
		__this->set_U24PC_6(1);
		goto IL_0148;
	}

IL_00cf:
	{
		WWW_t3134621005 * L_18 = __this->get_U3CwwwU3E__1_5();
		NullCheck(L_18);
		String_t* L_19 = WWW_get_error_m1787423074(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00fa;
		}
	}
	{
		MessageCheck_t3146986849 * L_20 = __this->get_U3CU3Ef__this_12();
		WWW_t3134621005 * L_21 = __this->get_U3CwwwU3E__1_5();
		NullCheck(L_21);
		String_t* L_22 = WWW_get_text_m4216049525(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		MessageCheck_EmailInsertserverTakeJson_m3006726659(L_20, L_22, /*hidden argument*/NULL);
		goto IL_013f;
	}

IL_00fa:
	{
		WWW_t3134621005 * L_23 = __this->get_U3CwwwU3E__1_5();
		NullCheck(L_23);
		String_t* L_24 = WWW_get_error_m1787423074(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_25 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_25);
		AndroidWebview_Call_m1677830609(L_25, _stringLiteral3202370, /*hidden argument*/NULL);
		int32_t L_26 = ((int32_t)3);
		Il2CppObject * L_27 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_26);
		NullCheck((Enum_t2862688501 *)L_27);
		String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_27);
		WWW_t3134621005 * L_29 = __this->get_U3CwwwU3E__1_5();
		NullCheck(L_29);
		String_t* L_30 = WWW_get_error_m1787423074(L_29, /*hidden argument*/NULL);
		int32_t L_31 = ((int32_t)2);
		Il2CppObject * L_32 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_31);
		NullCheck((Enum_t2862688501 *)L_32);
		String_t* L_33 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_32);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_28, L_30, L_33, /*hidden argument*/NULL);
	}

IL_013f:
	{
		__this->set_U24PC_6((-1));
	}

IL_0146:
	{
		return (bool)0;
	}

IL_0148:
	{
		return (bool)1;
	}
	// Dead block : IL_014a: ldloc.1
}
// System.Void MessageCheck/<EmailInsertserver>c__Iterator18::Dispose()
extern "C"  void U3CEmailInsertserverU3Ec__Iterator18_Dispose_m3451034340 (U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void MessageCheck/<EmailInsertserver>c__Iterator18::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CEmailInsertserverU3Ec__Iterator18_Reset_m1574147860_MetadataUsageId;
extern "C"  void U3CEmailInsertserverU3Ec__Iterator18_Reset_m1574147860 (U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEmailInsertserverU3Ec__Iterator18_Reset_m1574147860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MessageCheck/<EmailInsertserverProgress>c__Iterator19::.ctor()
extern "C"  void U3CEmailInsertserverProgressU3Ec__Iterator19__ctor_m3795497625 (U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object MessageCheck/<EmailInsertserverProgress>c__Iterator19::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEmailInsertserverProgressU3Ec__Iterator19_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m599915545 (U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object MessageCheck/<EmailInsertserverProgress>c__Iterator19::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEmailInsertserverProgressU3Ec__Iterator19_System_Collections_IEnumerator_get_Current_m3434697133 (U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean MessageCheck/<EmailInsertserverProgress>c__Iterator19::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3529469;
extern Il2CppCodeGenString* _stringLiteral1800556493;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t U3CEmailInsertserverProgressU3Ec__Iterator19_MoveNext_m1712183227_MetadataUsageId;
extern "C"  bool U3CEmailInsertserverProgressU3Ec__Iterator19_MoveNext_m1712183227 (U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEmailInsertserverProgressU3Ec__Iterator19_MoveNext_m1712183227_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0041;
		}
		if (L_1 == 2)
		{
			goto IL_0081;
		}
	}
	{
		goto IL_0097;
	}

IL_0025:
	{
		WaitForSeconds_t1615819279 * L_2 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_2, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_2);
		__this->set_U24PC_1(1);
		goto IL_0099;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_3 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		AndroidWebview_Call_m335597645(L_3, _stringLiteral3529469, _stringLiteral1800556493, /*hidden argument*/NULL);
		WWW_t3134621005 * L_4 = __this->get_www_0();
		NullCheck(L_4);
		bool L_5 = WWW_get_isDone_m634060017(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0025;
		}
	}
	{
		WaitForSeconds_t1615819279 * L_6 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_6, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(2);
		goto IL_0099;
	}

IL_0081:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_7 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		AndroidWebview_Call_m1677830609(L_7, _stringLiteral3202370, /*hidden argument*/NULL);
		__this->set_U24PC_1((-1));
	}

IL_0097:
	{
		return (bool)0;
	}

IL_0099:
	{
		return (bool)1;
	}
	// Dead block : IL_009b: ldloc.1
}
// System.Void MessageCheck/<EmailInsertserverProgress>c__Iterator19::Dispose()
extern "C"  void U3CEmailInsertserverProgressU3Ec__Iterator19_Dispose_m944266390 (U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void MessageCheck/<EmailInsertserverProgress>c__Iterator19::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CEmailInsertserverProgressU3Ec__Iterator19_Reset_m1441930566_MetadataUsageId;
extern "C"  void U3CEmailInsertserverProgressU3Ec__Iterator19_Reset_m1441930566 (U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEmailInsertserverProgressU3Ec__Iterator19_Reset_m1441930566_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MessageCheck/<ImageLoad>c__Iterator16::.ctor()
extern "C"  void U3CImageLoadU3Ec__Iterator16__ctor_m3047818784 (U3CImageLoadU3Ec__Iterator16_t4085388203 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object MessageCheck/<ImageLoad>c__Iterator16::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CImageLoadU3Ec__Iterator16_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3487958258 (U3CImageLoadU3Ec__Iterator16_t4085388203 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object MessageCheck/<ImageLoad>c__Iterator16::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CImageLoadU3Ec__Iterator16_System_Collections_IEnumerator_get_Current_m3855887494 (U3CImageLoadU3Ec__Iterator16_t4085388203 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Boolean MessageCheck/<ImageLoad>c__Iterator16::MoveNext()
extern Il2CppClass* Texture2D_t3884108195_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisImage_t538875265_m3706520426_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral3529469;
extern Il2CppCodeGenString* _stringLiteral1800556493;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t U3CImageLoadU3Ec__Iterator16_MoveNext_m3589806804_MetadataUsageId;
extern "C"  bool U3CImageLoadU3Ec__Iterator16_MoveNext_m3589806804 (U3CImageLoadU3Ec__Iterator16_t4085388203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CImageLoadU3Ec__Iterator16_MoveNext_m3589806804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0088;
		}
	}
	{
		goto IL_019d;
	}

IL_0021:
	{
		Texture2D_t3884108195 * L_2 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3705883154(L_2, 1, 1, ((int32_t)34), (bool)0, /*hidden argument*/NULL);
		__this->set_U3CtexU3E__0_0(L_2);
		String_t* L_3 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		MessageCheck_t3146986849 * L_4 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_4);
		String_t* L_5 = L_4->get__videoname_20();
		String_t* L_6 = __this->get_file_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2933632197(NULL /*static, unused*/, L_3, _stringLiteral47, L_5, L_6, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_8 = File_ReadAllBytes_m621899937(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		__this->set_U3CbinaryImageDataU3E__1_2(L_8);
		goto IL_0088;
	}

IL_0061:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_9 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		AndroidWebview_Call_m335597645(L_9, _stringLiteral3529469, _stringLiteral1800556493, /*hidden argument*/NULL);
		__this->set_U24current_4(NULL);
		__this->set_U24PC_3(1);
		goto IL_019f;
	}

IL_0088:
	{
		Texture2D_t3884108195 * L_10 = __this->get_U3CtexU3E__0_0();
		ByteU5BU5D_t4260760469* L_11 = __this->get_U3CbinaryImageDataU3E__1_2();
		NullCheck(L_10);
		bool L_12 = Texture2D_LoadImage_m2186196036(L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0061;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_13 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		AndroidWebview_Call_m1677830609(L_13, _stringLiteral3202370, /*hidden argument*/NULL);
		MessageCheck_t3146986849 * L_14 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_14);
		GameObject_t3674682005 * L_15 = L_14->get__Sprite_15();
		NullCheck(L_15);
		GameObject_SetActive_m3538205401(L_15, (bool)1, /*hidden argument*/NULL);
		MessageCheck_t3146986849 * L_16 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_16);
		RectTransform_t972643934 * L_17 = L_16->get_imagesize_5();
		NullCheck(L_17);
		Image_t538875265 * L_18 = Component_GetComponent_TisImage_t538875265_m3706520426(L_17, /*hidden argument*/Component_GetComponent_TisImage_t538875265_m3706520426_MethodInfo_var);
		Texture2D_t3884108195 * L_19 = __this->get_U3CtexU3E__0_0();
		Texture2D_t3884108195 * L_20 = __this->get_U3CtexU3E__0_0();
		NullCheck(L_20);
		int32_t L_21 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_20);
		Texture2D_t3884108195 * L_22 = __this->get_U3CtexU3E__0_0();
		NullCheck(L_22);
		int32_t L_23 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_22);
		Rect_t4241904616  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m3291325233(&L_24, (0.0f), (0.0f), (((float)((float)L_21))), (((float)((float)L_23))), /*hidden argument*/NULL);
		Vector2_t4282066565  L_25 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		Sprite_t3199167241 * L_26 = Sprite_Create_m278903054(NULL /*static, unused*/, L_19, L_24, L_25, /*hidden argument*/NULL);
		NullCheck(L_18);
		Image_set_sprite_m572551402(L_18, L_26, /*hidden argument*/NULL);
		int32_t L_27 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_28 = __this->get_U3CtexU3E__0_0();
		NullCheck(L_28);
		int32_t L_29 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_28);
		if ((((int32_t)L_27) >= ((int32_t)L_29)))
		{
			goto IL_015d;
		}
	}
	{
		MessageCheck_t3146986849 * L_30 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_30);
		RectTransform_t972643934 * L_31 = L_30->get_imagesize_5();
		Texture2D_t3884108195 * L_32 = __this->get_U3CtexU3E__0_0();
		NullCheck(L_32);
		int32_t L_33 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_32);
		Texture2D_t3884108195 * L_34 = __this->get_U3CtexU3E__0_0();
		NullCheck(L_34);
		int32_t L_35 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_34);
		Vector2_t4282066565  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Vector2__ctor_m1517109030(&L_36, ((float)((float)(((float)((float)L_33)))*(float)(0.75f))), ((float)((float)(((float)((float)L_35)))*(float)(0.75f))), /*hidden argument*/NULL);
		NullCheck(L_31);
		RectTransform_set_sizeDelta_m1223846609(L_31, L_36, /*hidden argument*/NULL);
		goto IL_0196;
	}

IL_015d:
	{
		MessageCheck_t3146986849 * L_37 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_37);
		RectTransform_t972643934 * L_38 = L_37->get_imagesize_5();
		Texture2D_t3884108195 * L_39 = __this->get_U3CtexU3E__0_0();
		NullCheck(L_39);
		int32_t L_40 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_39);
		Texture2D_t3884108195 * L_41 = __this->get_U3CtexU3E__0_0();
		NullCheck(L_41);
		int32_t L_42 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_41);
		Vector2_t4282066565  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Vector2__ctor_m1517109030(&L_43, ((float)((float)(((float)((float)L_40)))*(float)(0.75f))), ((float)((float)(((float)((float)L_42)))*(float)(0.75f))), /*hidden argument*/NULL);
		NullCheck(L_38);
		RectTransform_set_sizeDelta_m1223846609(L_38, L_43, /*hidden argument*/NULL);
	}

IL_0196:
	{
		__this->set_U24PC_3((-1));
	}

IL_019d:
	{
		return (bool)0;
	}

IL_019f:
	{
		return (bool)1;
	}
	// Dead block : IL_01a1: ldloc.1
}
// System.Void MessageCheck/<ImageLoad>c__Iterator16::Dispose()
extern "C"  void U3CImageLoadU3Ec__Iterator16_Dispose_m3979405917 (U3CImageLoadU3Ec__Iterator16_t4085388203 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void MessageCheck/<ImageLoad>c__Iterator16::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CImageLoadU3Ec__Iterator16_Reset_m694251725_MetadataUsageId;
extern "C"  void U3CImageLoadU3Ec__Iterator16_Reset_m694251725 (U3CImageLoadU3Ec__Iterator16_t4085388203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CImageLoadU3Ec__Iterator16_Reset_m694251725_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MessageCheck/<MessageDeleteServerCheck>c__Iterator1B::.ctor()
extern "C"  void U3CMessageDeleteServerCheckU3Ec__Iterator1B__ctor_m1870680502 (U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object MessageCheck/<MessageDeleteServerCheck>c__Iterator1B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMessageDeleteServerCheckU3Ec__Iterator1B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3453673830 (U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object MessageCheck/<MessageDeleteServerCheck>c__Iterator1B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMessageDeleteServerCheckU3Ec__Iterator1B_System_Collections_IEnumerator_get_Current_m3218632954 (U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean MessageCheck/<MessageDeleteServerCheck>c__Iterator1B::MoveNext()
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4174987542;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t U3CMessageDeleteServerCheckU3Ec__Iterator1B_MoveNext_m3262631526_MetadataUsageId;
extern "C"  bool U3CMessageDeleteServerCheckU3Ec__Iterator1B_MoveNext_m3262631526 (U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMessageDeleteServerCheckU3Ec__Iterator1B_MoveNext_m3262631526_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0029;
		}
		if (L_1 == 1)
		{
			goto IL_006e;
		}
		if (L_1 == 2)
		{
			goto IL_009a;
		}
		if (L_1 == 3)
		{
			goto IL_00eb;
		}
	}
	{
		goto IL_0101;
	}

IL_0029:
	{
		String_t* L_2 = PrivateURL_get_MessageOneDelete_m1855499307(NULL /*static, unused*/, /*hidden argument*/NULL);
		WWW_t3134621005 * L_3 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m1985874080(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_0(L_3);
		MessageCheck_t3146986849 * L_4 = __this->get_U3CU3Ef__this_3();
		MessageCheck_t3146986849 * L_5 = __this->get_U3CU3Ef__this_3();
		WWW_t3134621005 * L_6 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_5);
		Il2CppObject * L_7 = MessageCheck_EmailInsertserverProgress_m3801537309(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		MonoBehaviour_StartCoroutine_m2135303124(L_4, L_7, /*hidden argument*/NULL);
		WWW_t3134621005 * L_8 = __this->get_U3CwwwU3E__0_0();
		__this->set_U24current_2(L_8);
		__this->set_U24PC_1(1);
		goto IL_0103;
	}

IL_006e:
	{
		WWW_t3134621005 * L_9 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_9);
		String_t* L_10 = WWW_get_error_m1787423074(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_00b5;
		}
	}
	{
		WaitForSeconds_t1615819279 * L_11 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_11, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_11);
		__this->set_U24PC_1(2);
		goto IL_0103;
	}

IL_009a:
	{
		MessageCheck_t3146986849 * L_12 = __this->get_U3CU3Ef__this_3();
		WWW_t3134621005 * L_13 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_13);
		String_t* L_14 = WWW_get_text_m4216049525(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		MessageCheck_MessageDeleteTakeJson_m2005989801(L_12, L_14, /*hidden argument*/NULL);
		goto IL_00fa;
	}

IL_00b5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_15 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		WWW_t3134621005 * L_16 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_16);
		String_t* L_17 = WWW_get_error_m1787423074(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		AndroidWebview_Call_m335597645(L_15, _stringLiteral4174987542, L_17, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_18 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_18, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_18);
		__this->set_U24PC_1(3);
		goto IL_0103;
	}

IL_00eb:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_19 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		AndroidWebview_Call_m1677830609(L_19, _stringLiteral3202370, /*hidden argument*/NULL);
	}

IL_00fa:
	{
		__this->set_U24PC_1((-1));
	}

IL_0101:
	{
		return (bool)0;
	}

IL_0103:
	{
		return (bool)1;
	}
	// Dead block : IL_0105: ldloc.1
}
// System.Void MessageCheck/<MessageDeleteServerCheck>c__Iterator1B::Dispose()
extern "C"  void U3CMessageDeleteServerCheckU3Ec__Iterator1B_Dispose_m2325915763 (U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void MessageCheck/<MessageDeleteServerCheck>c__Iterator1B::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CMessageDeleteServerCheckU3Ec__Iterator1B_Reset_m3812080739_MetadataUsageId;
extern "C"  void U3CMessageDeleteServerCheckU3Ec__Iterator1B_Reset_m3812080739 (U3CMessageDeleteServerCheckU3Ec__Iterator1B_t3224710789 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMessageDeleteServerCheckU3Ec__Iterator1B_Reset_m3812080739_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MessageCheck/<MessageDeleteServerUpdate>c__Iterator1A::.ctor()
extern "C"  void U3CMessageDeleteServerUpdateU3Ec__Iterator1A__ctor_m1594179352 (U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object MessageCheck/<MessageDeleteServerUpdate>c__Iterator1A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMessageDeleteServerUpdateU3Ec__Iterator1A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m236634362 (U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object MessageCheck/<MessageDeleteServerUpdate>c__Iterator1A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMessageDeleteServerUpdateU3Ec__Iterator1A_System_Collections_IEnumerator_get_Current_m3687970958 (U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean MessageCheck/<MessageDeleteServerUpdate>c__Iterator1A::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentScreen_t3026574117_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t U3CMessageDeleteServerUpdateU3Ec__Iterator1A_MoveNext_m2095154908_MetadataUsageId;
extern "C"  bool U3CMessageDeleteServerUpdateU3Ec__Iterator1A_MoveNext_m2095154908 (U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMessageDeleteServerUpdateU3Ec__Iterator1A_MoveNext_m2095154908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_0069;
	}

IL_0021:
	{
		WaitForSeconds_t1615819279 * L_2 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_2, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		__this->set_U24PC_0(1);
		goto IL_006b;
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentScreen_t3026574117_il2cpp_TypeInfo_var);
		CurrentScreen_t3026574117 * L_3 = CurrentScreen_get_Instance_m3161187310(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		CurrentScreen_VisibleFalseState_m2603065973(L_3, ((int32_t)27), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		CurrentScreen_t3026574117 * L_4 = CurrentScreen_get_Instance_m3161187310(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		CurrentScreen_State_m3173002564(L_4, ((int32_t)50), /*hidden argument*/NULL);
		CurrentState_set_PopupMessage_m2237488056(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_0069:
	{
		return (bool)0;
	}

IL_006b:
	{
		return (bool)1;
	}
	// Dead block : IL_006d: ldloc.1
}
// System.Void MessageCheck/<MessageDeleteServerUpdate>c__Iterator1A::Dispose()
extern "C"  void U3CMessageDeleteServerUpdateU3Ec__Iterator1A_Dispose_m2896282965 (U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void MessageCheck/<MessageDeleteServerUpdate>c__Iterator1A::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CMessageDeleteServerUpdateU3Ec__Iterator1A_Reset_m3535579589_MetadataUsageId;
extern "C"  void U3CMessageDeleteServerUpdateU3Ec__Iterator1A_Reset_m3535579589 (U3CMessageDeleteServerUpdateU3Ec__Iterator1A_t3671298995 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMessageDeleteServerUpdateU3Ec__Iterator1A_Reset_m3535579589_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MessageCheck/<VideoDown>c__Iterator14::.ctor()
extern "C"  void U3CVideoDownU3Ec__Iterator14__ctor_m3844436358 (U3CVideoDownU3Ec__Iterator14_t3182108037 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object MessageCheck/<VideoDown>c__Iterator14::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CVideoDownU3Ec__Iterator14_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3156995404 (U3CVideoDownU3Ec__Iterator14_t3182108037 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object MessageCheck/<VideoDown>c__Iterator14::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CVideoDownU3Ec__Iterator14_System_Collections_IEnumerator_get_Current_m2778335456 (U3CVideoDownU3Ec__Iterator14_t3182108037 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Boolean MessageCheck/<VideoDown>c__Iterator14::MoveNext()
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral46;
extern Il2CppCodeGenString* _stringLiteral49;
extern Il2CppCodeGenString* _stringLiteral50;
extern Il2CppCodeGenString* _stringLiteral51;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral1819179829;
extern Il2CppCodeGenString* _stringLiteral2220296047;
extern const uint32_t U3CVideoDownU3Ec__Iterator14_MoveNext_m1634676142_MetadataUsageId;
extern "C"  bool U3CVideoDownU3Ec__Iterator14_MoveNext_m1634676142 (U3CVideoDownU3Ec__Iterator14_t3182108037 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CVideoDownU3Ec__Iterator14_MoveNext_m1634676142_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0191;
		}
	}
	{
		goto IL_027f;
	}

IL_0021:
	{
		MessageCheck_t3146986849 * L_2 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_2);
		String_t* L_3 = L_2->get__videourl_19();
		WWW_t3134621005 * L_4 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m1985874080(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_0(L_4);
		MessageCheck_t3146986849 * L_5 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_5);
		String_t* L_6 = L_5->get__videourl_19();
		MessageCheck_t3146986849 * L_7 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_7);
		String_t* L_8 = L_7->get__videourl_19();
		NullCheck(L_8);
		int32_t L_9 = String_LastIndexOf_m2747144337(L_8, _stringLiteral46, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_10 = String_Substring_m2809233063(L_6, ((int32_t)((int32_t)L_9+(int32_t)1)), /*hidden argument*/NULL);
		__this->set_U3CtempU3E__1_1(L_10);
		MessageCheck_t3146986849 * L_11 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_11);
		String_t* L_12 = L_11->get_filetype_24();
		NullCheck(L_12);
		bool L_13 = String_Equals_m3541721061(L_12, _stringLiteral49, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00a0;
		}
	}
	{
		MessageCheck_t3146986849 * L_14 = __this->get_U3CU3Ef__this_5();
		String_t* L_15 = __this->get_U3CtempU3E__1_1();
		NullCheck(L_14);
		L_14->set_filenamecategory_28(L_15);
		MessageCheck_t3146986849 * L_16 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_16);
		L_16->set_category_29(1);
		goto IL_0113;
	}

IL_00a0:
	{
		MessageCheck_t3146986849 * L_17 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_17);
		String_t* L_18 = L_17->get_filetype_24();
		NullCheck(L_18);
		bool L_19 = String_Equals_m3541721061(L_18, _stringLiteral50, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00dc;
		}
	}
	{
		MessageCheck_t3146986849 * L_20 = __this->get_U3CU3Ef__this_5();
		String_t* L_21 = __this->get_U3CtempU3E__1_1();
		NullCheck(L_20);
		L_20->set_filenamecategory_28(L_21);
		MessageCheck_t3146986849 * L_22 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_22);
		L_22->set_category_29(2);
		goto IL_0113;
	}

IL_00dc:
	{
		MessageCheck_t3146986849 * L_23 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_23);
		String_t* L_24 = L_23->get_filetype_24();
		NullCheck(L_24);
		bool L_25 = String_Equals_m3541721061(L_24, _stringLiteral51, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0113;
		}
	}
	{
		MessageCheck_t3146986849 * L_26 = __this->get_U3CU3Ef__this_5();
		String_t* L_27 = __this->get_U3CtempU3E__1_1();
		NullCheck(L_26);
		L_26->set_filenamecategory_28(L_27);
		MessageCheck_t3146986849 * L_28 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_28);
		L_28->set_category_29(3);
	}

IL_0113:
	{
		StringU5BU5D_t4054002952* L_29 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		String_t* L_30 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 0);
		ArrayElementTypeCheck (L_29, L_30);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_30);
		StringU5BU5D_t4054002952* L_31 = L_29;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 1);
		ArrayElementTypeCheck (L_31, _stringLiteral47);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral47);
		StringU5BU5D_t4054002952* L_32 = L_31;
		MessageCheck_t3146986849 * L_33 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_33);
		String_t* L_34 = L_33->get__videoname_20();
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 2);
		ArrayElementTypeCheck (L_32, L_34);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_34);
		StringU5BU5D_t4054002952* L_35 = L_32;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 3);
		ArrayElementTypeCheck (L_35, _stringLiteral46);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral46);
		StringU5BU5D_t4054002952* L_36 = L_35;
		MessageCheck_t3146986849 * L_37 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_37);
		String_t* L_38 = L_37->get_filenamecategory_28();
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 4);
		ArrayElementTypeCheck (L_36, L_38);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_38);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m21867311(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		bool L_40 = File_Exists_m1326262381(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_0241;
		}
	}
	{
		MessageCheck_t3146986849 * L_41 = __this->get_U3CU3Ef__this_5();
		MessageCheck_t3146986849 * L_42 = __this->get_U3CU3Ef__this_5();
		WWW_t3134621005 * L_43 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_42);
		Il2CppObject * L_44 = MessageCheck_CheckProgress_m2269471373(L_42, L_43, /*hidden argument*/NULL);
		NullCheck(L_41);
		MonoBehaviour_StartCoroutine_m2135303124(L_41, L_44, /*hidden argument*/NULL);
		WWW_t3134621005 * L_45 = __this->get_U3CwwwU3E__0_0();
		__this->set_U24current_4(L_45);
		__this->set_U24PC_3(1);
		goto IL_0281;
	}

IL_0191:
	{
		WWW_t3134621005 * L_46 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_46);
		String_t* L_47 = WWW_get_error_m1787423074(L_46, /*hidden argument*/NULL);
		if (L_47)
		{
			goto IL_0201;
		}
	}
	{
		WWW_t3134621005 * L_48 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_48);
		ByteU5BU5D_t4260760469* L_49 = WWW_get_bytes_m2080623436(L_48, /*hidden argument*/NULL);
		__this->set_U3CbytesU3E__2_2(L_49);
		StringU5BU5D_t4054002952* L_50 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		String_t* L_51 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, 0);
		ArrayElementTypeCheck (L_50, L_51);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_51);
		StringU5BU5D_t4054002952* L_52 = L_50;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 1);
		ArrayElementTypeCheck (L_52, _stringLiteral47);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral47);
		StringU5BU5D_t4054002952* L_53 = L_52;
		MessageCheck_t3146986849 * L_54 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_54);
		String_t* L_55 = L_54->get__videoname_20();
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 2);
		ArrayElementTypeCheck (L_53, L_55);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_55);
		StringU5BU5D_t4054002952* L_56 = L_53;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 3);
		ArrayElementTypeCheck (L_56, _stringLiteral46);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral46);
		StringU5BU5D_t4054002952* L_57 = L_56;
		MessageCheck_t3146986849 * L_58 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_58);
		String_t* L_59 = L_58->get_filenamecategory_28();
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, 4);
		ArrayElementTypeCheck (L_57, L_59);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_59);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_60 = String_Concat_m21867311(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_61 = __this->get_U3CbytesU3E__2_2();
		File_WriteAllBytes_m2419938065(NULL /*static, unused*/, L_60, L_61, /*hidden argument*/NULL);
		goto IL_0241;
	}

IL_0201:
	{
		MessageCheck_t3146986849 * L_62 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_62);
		GameObject_t3674682005 * L_63 = L_62->get_ProgressBar_3();
		NullCheck(L_63);
		GameObject_SetActive_m3538205401(L_63, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral1819179829, /*hidden argument*/NULL);
		int32_t L_64 = ((int32_t)3);
		Il2CppObject * L_65 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_64);
		NullCheck((Enum_t2862688501 *)L_65);
		String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_65);
		int32_t L_67 = ((int32_t)2);
		Il2CppObject * L_68 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_67);
		NullCheck((Enum_t2862688501 *)L_68);
		String_t* L_69 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_68);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_66, _stringLiteral2220296047, L_69, /*hidden argument*/NULL);
		goto IL_027f;
	}

IL_0241:
	{
		MessageCheck_t3146986849 * L_70 = __this->get_U3CU3Ef__this_5();
		MessageCheck_t3146986849 * L_71 = __this->get_U3CU3Ef__this_5();
		MessageCheck_t3146986849 * L_72 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_72);
		int32_t L_73 = L_72->get_category_29();
		MessageCheck_t3146986849 * L_74 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_74);
		String_t* L_75 = L_74->get_filenamecategory_28();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_76 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral46, L_75, /*hidden argument*/NULL);
		NullCheck(L_71);
		Il2CppObject * L_77 = MessageCheck_VideoStart_m2620798630(L_71, L_73, L_76, /*hidden argument*/NULL);
		NullCheck(L_70);
		MonoBehaviour_StartCoroutine_m2135303124(L_70, L_77, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_027f:
	{
		return (bool)0;
	}

IL_0281:
	{
		return (bool)1;
	}
	// Dead block : IL_0283: ldloc.1
}
// System.Void MessageCheck/<VideoDown>c__Iterator14::Dispose()
extern "C"  void U3CVideoDownU3Ec__Iterator14_Dispose_m729748547 (U3CVideoDownU3Ec__Iterator14_t3182108037 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void MessageCheck/<VideoDown>c__Iterator14::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CVideoDownU3Ec__Iterator14_Reset_m1490869299_MetadataUsageId;
extern "C"  void U3CVideoDownU3Ec__Iterator14_Reset_m1490869299 (U3CVideoDownU3Ec__Iterator14_t3182108037 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CVideoDownU3Ec__Iterator14_Reset_m1490869299_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MessageCheck/<VideoStart>c__Iterator15::.ctor()
extern "C"  void U3CVideoStartU3Ec__Iterator15__ctor_m1597995279 (U3CVideoStartU3Ec__Iterator15_t210434316 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object MessageCheck/<VideoStart>c__Iterator15::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CVideoStartU3Ec__Iterator15_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1995219053 (U3CVideoStartU3Ec__Iterator15_t210434316 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Object MessageCheck/<VideoStart>c__Iterator15::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CVideoStartU3Ec__Iterator15_System_Collections_IEnumerator_get_Current_m4114554369 (U3CVideoStartU3Ec__Iterator15_t210434316 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Boolean MessageCheck/<VideoStart>c__Iterator15::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral47;
extern const uint32_t U3CVideoStartU3Ec__Iterator15_MoveNext_m1497679725_MetadataUsageId;
extern "C"  bool U3CVideoStartU3Ec__Iterator15_MoveNext_m1497679725 (U3CVideoStartU3Ec__Iterator15_t210434316 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CVideoStartU3Ec__Iterator15_MoveNext_m1497679725_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_015e;
	}

IL_0021:
	{
		WaitForSeconds_t1615819279 * L_2 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_2, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_6(L_2);
		__this->set_U24PC_5(1);
		goto IL_0160;
	}

IL_003d:
	{
		int32_t L_3 = __this->get_num_0();
		if ((!(((uint32_t)L_3) == ((uint32_t)3))))
		{
			goto IL_00c2;
		}
	}
	{
		MessageCheck_t3146986849 * L_4 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_4);
		String_t* L_5 = L_4->get_lat_21();
		float L_6 = Single_Parse_m3022284664(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_U3Clatitude_wikitudeU3E__0_1(L_6);
		MessageCheck_t3146986849 * L_7 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_7);
		String_t* L_8 = L_7->get_lon_22();
		float L_9 = Single_Parse_m3022284664(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->set_U3Clongtitude_wikitudeU3E__1_2(L_9);
		String_t* L_10 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		MessageCheck_t3146986849 * L_11 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_11);
		String_t* L_12 = L_11->get__videoname_20();
		String_t* L_13 = __this->get_filecategory_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m2933632197(NULL /*static, unused*/, L_10, _stringLiteral47, L_12, L_13, /*hidden argument*/NULL);
		__this->set_U3C_video_localpathU3E__2_4(L_14);
		String_t* L_15 = __this->get_U3C_video_localpathU3E__2_4();
		float L_16 = __this->get_U3Clatitude_wikitudeU3E__0_1();
		float L_17 = __this->get_U3Clongtitude_wikitudeU3E__1_2();
		CtrlPlugins_isWikitude_m2708272430(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		String_t* L_18 = __this->get_U3C_video_localpathU3E__2_4();
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		goto IL_0157;
	}

IL_00c2:
	{
		int32_t L_19 = __this->get_num_0();
		if ((!(((uint32_t)L_19) == ((uint32_t)2))))
		{
			goto IL_012e;
		}
	}
	{
		String_t* L_20 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		MessageCheck_t3146986849 * L_21 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_21);
		String_t* L_22 = L_21->get__videoname_20();
		String_t* L_23 = __this->get_filecategory_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Concat_m2933632197(NULL /*static, unused*/, L_20, _stringLiteral47, L_22, L_23, /*hidden argument*/NULL);
		CtrlPlugins_isWikitude_video_m3097634024(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		String_t* L_25 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		MessageCheck_t3146986849 * L_26 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_26);
		String_t* L_27 = L_26->get__videoname_20();
		String_t* L_28 = __this->get_filecategory_3();
		String_t* L_29 = String_Concat_m2933632197(NULL /*static, unused*/, L_25, _stringLiteral47, L_27, L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		MessageCheck_t3146986849 * L_30 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_30);
		GameObject_t3674682005 * L_31 = L_30->get_panel_2();
		NullCheck(L_31);
		GameObject_SetActive_m3538205401(L_31, (bool)1, /*hidden argument*/NULL);
		goto IL_0157;
	}

IL_012e:
	{
		int32_t L_32 = __this->get_num_0();
		if ((!(((uint32_t)L_32) == ((uint32_t)1))))
		{
			goto IL_0157;
		}
	}
	{
		MessageCheck_t3146986849 * L_33 = __this->get_U3CU3Ef__this_9();
		MessageCheck_t3146986849 * L_34 = __this->get_U3CU3Ef__this_9();
		String_t* L_35 = __this->get_filecategory_3();
		NullCheck(L_34);
		Il2CppObject * L_36 = MessageCheck_ImageLoad_m1220711249(L_34, L_35, /*hidden argument*/NULL);
		NullCheck(L_33);
		MonoBehaviour_StartCoroutine_m2135303124(L_33, L_36, /*hidden argument*/NULL);
	}

IL_0157:
	{
		__this->set_U24PC_5((-1));
	}

IL_015e:
	{
		return (bool)0;
	}

IL_0160:
	{
		return (bool)1;
	}
	// Dead block : IL_0162: ldloc.1
}
// System.Void MessageCheck/<VideoStart>c__Iterator15::Dispose()
extern "C"  void U3CVideoStartU3Ec__Iterator15_Dispose_m2268421516 (U3CVideoStartU3Ec__Iterator15_t210434316 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void MessageCheck/<VideoStart>c__Iterator15::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CVideoStartU3Ec__Iterator15_Reset_m3539395516_MetadataUsageId;
extern "C"  void U3CVideoStartU3Ec__Iterator15_Reset_m3539395516 (U3CVideoStartU3Ec__Iterator15_t210434316 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CVideoStartU3Ec__Iterator15_Reset_m3539395516_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Messages::_Message(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void Messages__Message_m2086894250 (Messages_t3897517420 * __this, int32_t ____messagecategory0, String_t* ____title1, String_t* ____type2, String_t* ____address3, String_t* ____lat4, String_t* ____lon5, String_t* ____videourl6, String_t* ____videoname7, String_t* ____createdata8, String_t* ____messageid9, const MethodInfo* method)
{
	{
		String_t* L_0 = ____title1;
		__this->set_Title_0(L_0);
		String_t* L_1 = ____type2;
		__this->set_type_1(L_1);
		String_t* L_2 = ____address3;
		__this->set_address_2(L_2);
		String_t* L_3 = ____lat4;
		__this->set_lat_3(L_3);
		String_t* L_4 = ____lon5;
		__this->set_lon_4(L_4);
		String_t* L_5 = ____videourl6;
		__this->set_videourl_5(L_5);
		String_t* L_6 = ____videoname7;
		__this->set_videoname_6(L_6);
		String_t* L_7 = ____createdata8;
		__this->set_createdata_7(L_7);
		String_t* L_8 = ____messageid9;
		__this->set_messageid_8(L_8);
		int32_t L_9 = ____messagecategory0;
		__this->set_MessageCategory_9(L_9);
		return;
	}
}
extern "C"  void Messages__Message_m2086894250_AdjustorThunk (Il2CppObject * __this, int32_t ____messagecategory0, String_t* ____title1, String_t* ____type2, String_t* ____address3, String_t* ____lat4, String_t* ____lon5, String_t* ____videourl6, String_t* ____videoname7, String_t* ____createdata8, String_t* ____messageid9, const MethodInfo* method)
{
	Messages_t3897517420 * _thisAdjusted = reinterpret_cast<Messages_t3897517420 *>(__this + 1);
	Messages__Message_m2086894250(_thisAdjusted, ____messagecategory0, ____title1, ____type2, ____address3, ____lat4, ____lon5, ____videourl6, ____videoname7, ____createdata8, ____messageid9, method);
}
// Conversion methods for marshalling of: Messages
extern "C" void Messages_t3897517420_marshal_pinvoke(const Messages_t3897517420& unmarshaled, Messages_t3897517420_marshaled_pinvoke& marshaled)
{
	marshaled.___Title_0 = il2cpp_codegen_marshal_string(unmarshaled.get_Title_0());
	marshaled.___type_1 = il2cpp_codegen_marshal_string(unmarshaled.get_type_1());
	marshaled.___address_2 = il2cpp_codegen_marshal_string(unmarshaled.get_address_2());
	marshaled.___lat_3 = il2cpp_codegen_marshal_string(unmarshaled.get_lat_3());
	marshaled.___lon_4 = il2cpp_codegen_marshal_string(unmarshaled.get_lon_4());
	marshaled.___videourl_5 = il2cpp_codegen_marshal_string(unmarshaled.get_videourl_5());
	marshaled.___videoname_6 = il2cpp_codegen_marshal_string(unmarshaled.get_videoname_6());
	marshaled.___createdata_7 = il2cpp_codegen_marshal_string(unmarshaled.get_createdata_7());
	marshaled.___messageid_8 = il2cpp_codegen_marshal_string(unmarshaled.get_messageid_8());
	marshaled.___MessageCategory_9 = unmarshaled.get_MessageCategory_9();
}
extern "C" void Messages_t3897517420_marshal_pinvoke_back(const Messages_t3897517420_marshaled_pinvoke& marshaled, Messages_t3897517420& unmarshaled)
{
	unmarshaled.set_Title_0(il2cpp_codegen_marshal_string_result(marshaled.___Title_0));
	unmarshaled.set_type_1(il2cpp_codegen_marshal_string_result(marshaled.___type_1));
	unmarshaled.set_address_2(il2cpp_codegen_marshal_string_result(marshaled.___address_2));
	unmarshaled.set_lat_3(il2cpp_codegen_marshal_string_result(marshaled.___lat_3));
	unmarshaled.set_lon_4(il2cpp_codegen_marshal_string_result(marshaled.___lon_4));
	unmarshaled.set_videourl_5(il2cpp_codegen_marshal_string_result(marshaled.___videourl_5));
	unmarshaled.set_videoname_6(il2cpp_codegen_marshal_string_result(marshaled.___videoname_6));
	unmarshaled.set_createdata_7(il2cpp_codegen_marshal_string_result(marshaled.___createdata_7));
	unmarshaled.set_messageid_8(il2cpp_codegen_marshal_string_result(marshaled.___messageid_8));
	int32_t unmarshaled_MessageCategory_temp_9 = 0;
	unmarshaled_MessageCategory_temp_9 = marshaled.___MessageCategory_9;
	unmarshaled.set_MessageCategory_9(unmarshaled_MessageCategory_temp_9);
}
// Conversion method for clean up from marshalling of: Messages
extern "C" void Messages_t3897517420_marshal_pinvoke_cleanup(Messages_t3897517420_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___Title_0);
	marshaled.___Title_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___type_1);
	marshaled.___type_1 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___address_2);
	marshaled.___address_2 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___lat_3);
	marshaled.___lat_3 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___lon_4);
	marshaled.___lon_4 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___videourl_5);
	marshaled.___videourl_5 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___videoname_6);
	marshaled.___videoname_6 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___createdata_7);
	marshaled.___createdata_7 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___messageid_8);
	marshaled.___messageid_8 = NULL;
}
// Conversion methods for marshalling of: Messages
extern "C" void Messages_t3897517420_marshal_com(const Messages_t3897517420& unmarshaled, Messages_t3897517420_marshaled_com& marshaled)
{
	marshaled.___Title_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_Title_0());
	marshaled.___type_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_type_1());
	marshaled.___address_2 = il2cpp_codegen_marshal_bstring(unmarshaled.get_address_2());
	marshaled.___lat_3 = il2cpp_codegen_marshal_bstring(unmarshaled.get_lat_3());
	marshaled.___lon_4 = il2cpp_codegen_marshal_bstring(unmarshaled.get_lon_4());
	marshaled.___videourl_5 = il2cpp_codegen_marshal_bstring(unmarshaled.get_videourl_5());
	marshaled.___videoname_6 = il2cpp_codegen_marshal_bstring(unmarshaled.get_videoname_6());
	marshaled.___createdata_7 = il2cpp_codegen_marshal_bstring(unmarshaled.get_createdata_7());
	marshaled.___messageid_8 = il2cpp_codegen_marshal_bstring(unmarshaled.get_messageid_8());
	marshaled.___MessageCategory_9 = unmarshaled.get_MessageCategory_9();
}
extern "C" void Messages_t3897517420_marshal_com_back(const Messages_t3897517420_marshaled_com& marshaled, Messages_t3897517420& unmarshaled)
{
	unmarshaled.set_Title_0(il2cpp_codegen_marshal_bstring_result(marshaled.___Title_0));
	unmarshaled.set_type_1(il2cpp_codegen_marshal_bstring_result(marshaled.___type_1));
	unmarshaled.set_address_2(il2cpp_codegen_marshal_bstring_result(marshaled.___address_2));
	unmarshaled.set_lat_3(il2cpp_codegen_marshal_bstring_result(marshaled.___lat_3));
	unmarshaled.set_lon_4(il2cpp_codegen_marshal_bstring_result(marshaled.___lon_4));
	unmarshaled.set_videourl_5(il2cpp_codegen_marshal_bstring_result(marshaled.___videourl_5));
	unmarshaled.set_videoname_6(il2cpp_codegen_marshal_bstring_result(marshaled.___videoname_6));
	unmarshaled.set_createdata_7(il2cpp_codegen_marshal_bstring_result(marshaled.___createdata_7));
	unmarshaled.set_messageid_8(il2cpp_codegen_marshal_bstring_result(marshaled.___messageid_8));
	int32_t unmarshaled_MessageCategory_temp_9 = 0;
	unmarshaled_MessageCategory_temp_9 = marshaled.___MessageCategory_9;
	unmarshaled.set_MessageCategory_9(unmarshaled_MessageCategory_temp_9);
}
// Conversion method for clean up from marshalling of: Messages
extern "C" void Messages_t3897517420_marshal_com_cleanup(Messages_t3897517420_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___Title_0);
	marshaled.___Title_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___type_1);
	marshaled.___type_1 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___address_2);
	marshaled.___address_2 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___lat_3);
	marshaled.___lat_3 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___lon_4);
	marshaled.___lon_4 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___videourl_5);
	marshaled.___videourl_5 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___videoname_6);
	marshaled.___videoname_6 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___createdata_7);
	marshaled.___createdata_7 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___messageid_8);
	marshaled.___messageid_8 = NULL;
}
// System.Void MessageSee::.ctor()
extern Il2CppClass* CurrentScreen_t3026574117_il2cpp_TypeInfo_var;
extern const uint32_t MessageSee__ctor_m978080559_MetadataUsageId;
extern "C"  void MessageSee__ctor_m978080559 (MessageSee_t302730988 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageSee__ctor_m978080559_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentScreen_t3026574117_il2cpp_TypeInfo_var);
		CurrentScreen__ctor_m1985495014(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator MessageSee::JsonStart(CurrentState/MemberStates)
extern Il2CppClass* U3CJsonStartU3Ec__Iterator1C_t1966288966_il2cpp_TypeInfo_var;
extern const uint32_t MessageSee_JsonStart_m1847667520_MetadataUsageId;
extern "C"  Il2CppObject * MessageSee_JsonStart_m1847667520 (MessageSee_t302730988 * __this, int32_t ___MemberState0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageSee_JsonStart_m1847667520_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CJsonStartU3Ec__Iterator1C_t1966288966 * V_0 = NULL;
	{
		U3CJsonStartU3Ec__Iterator1C_t1966288966 * L_0 = (U3CJsonStartU3Ec__Iterator1C_t1966288966 *)il2cpp_codegen_object_new(U3CJsonStartU3Ec__Iterator1C_t1966288966_il2cpp_TypeInfo_var);
		U3CJsonStartU3Ec__Iterator1C__ctor_m3442672101(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CJsonStartU3Ec__Iterator1C_t1966288966 * L_1 = V_0;
		int32_t L_2 = ___MemberState0;
		NullCheck(L_1);
		L_1->set_MemberState_1(L_2);
		U3CJsonStartU3Ec__Iterator1C_t1966288966 * L_3 = V_0;
		int32_t L_4 = ___MemberState0;
		NullCheck(L_3);
		L_3->set_U3CU24U3EMemberState_5(L_4);
		U3CJsonStartU3Ec__Iterator1C_t1966288966 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_6(__this);
		U3CJsonStartU3Ec__Iterator1C_t1966288966 * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator MessageSee::MessageSeeCheckProgress(UnityEngine.WWW)
extern Il2CppClass* U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070_il2cpp_TypeInfo_var;
extern const uint32_t MessageSee_MessageSeeCheckProgress_m1726939908_MetadataUsageId;
extern "C"  Il2CppObject * MessageSee_MessageSeeCheckProgress_m1726939908 (MessageSee_t302730988 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageSee_MessageSeeCheckProgress_m1726939908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 * V_0 = NULL;
	{
		U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 * L_0 = (U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 *)il2cpp_codegen_object_new(U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070_il2cpp_TypeInfo_var);
		U3CMessageSeeCheckProgressU3Ec__Iterator1D__ctor_m1954795733(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 * L_1 = V_0;
		WWW_t3134621005 * L_2 = ___www0;
		NullCheck(L_1);
		L_1->set_www_0(L_2);
		U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 * L_3 = V_0;
		WWW_t3134621005 * L_4 = ___www0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Ewww_3(L_4);
		U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 * L_5 = V_0;
		return L_5;
	}
}
// System.Void MessageSee::TakeJson(System.String)
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* MessagesU5BU5D_t3583122277_il2cpp_TypeInfo_var;
extern Il2CppClass* MessageCheck_t3146986849_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CTakeJsonU3Ec__AnonStorey26_t291817749_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_t594794173_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponentInChildren_TisText_t9039225_m2924853261_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var;
extern const MethodInfo* U3CTakeJsonU3Ec__AnonStorey26_U3CU3Em__3_m681730374_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2001146706;
extern Il2CppCodeGenString* _stringLiteral3897517420;
extern Il2CppCodeGenString* _stringLiteral2289459;
extern Il2CppCodeGenString* _stringLiteral2604245075;
extern Il2CppCodeGenString* _stringLiteral110371416;
extern Il2CppCodeGenString* _stringLiteral3575610;
extern Il2CppCodeGenString* _stringLiteral2872469938;
extern Il2CppCodeGenString* _stringLiteral2854988908;
extern Il2CppCodeGenString* _stringLiteral137365935;
extern Il2CppCodeGenString* _stringLiteral473771429;
extern Il2CppCodeGenString* _stringLiteral3800909073;
extern Il2CppCodeGenString* _stringLiteral10;
extern const uint32_t MessageSee_TakeJson_m4186039550_MetadataUsageId;
extern "C"  void MessageSee_TakeJson_m4186039550 (MessageSee_t302730988 * __this, String_t* ___js0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageSee_TakeJson_m4186039550_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JsonData_t1715015430 * V_0 = NULL;
	int32_t V_1 = 0;
	GameObject_t3674682005 * V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	U3CTakeJsonU3Ec__AnonStorey26_t291817749 * V_11 = NULL;
	{
		String_t* L_0 = ___js0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		JsonData_t1715015430 * L_1 = JsonMapper_ToObject_m3425498490(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___js0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		JsonData_t1715015430 * L_3 = V_0;
		NullCheck(L_3);
		JsonData_t1715015430 * L_4 = JsonData_get_Item_m253158020(L_3, 1, /*hidden argument*/NULL);
		NullCheck(L_4);
		JsonData_t1715015430 * L_5 = JsonData_get_Item_m253158020(L_4, 0, /*hidden argument*/NULL);
		NullCheck(L_5);
		JsonData_t1715015430 * L_6 = JsonData_get_Item_m253158020(L_5, 0, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t476798718_il2cpp_TypeInfo_var);
		bool L_8 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_04de;
		}
	}
	{
		JsonData_t1715015430 * L_9 = V_0;
		NullCheck(L_9);
		JsonData_t1715015430 * L_10 = JsonData_get_Item_m253158020(L_9, 1, /*hidden argument*/NULL);
		NullCheck(L_10);
		JsonData_t1715015430 * L_11 = JsonData_get_Item_m253158020(L_10, 1, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = JsonData_get_Count_m412158079(L_11, /*hidden argument*/NULL);
		((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->set_m_Messages_6(((MessagesU5BU5D_t3583122277*)SZArrayNew(MessagesU5BU5D_t3583122277_il2cpp_TypeInfo_var, (uint32_t)L_12)));
		V_1 = 0;
		goto IL_04c6;
	}

IL_0053:
	{
		int32_t L_13 = V_1;
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2001146706, L_15, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_17 = GameObject_Find_m332785498(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		GameObject_t3674682005 * L_18 = V_2;
		bool L_19 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_18, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_04c2;
		}
	}
	{
		U3CTakeJsonU3Ec__AnonStorey26_t291817749 * L_20 = (U3CTakeJsonU3Ec__AnonStorey26_t291817749 *)il2cpp_codegen_object_new(U3CTakeJsonU3Ec__AnonStorey26_t291817749_il2cpp_TypeInfo_var);
		U3CTakeJsonU3Ec__AnonStorey26__ctor_m3626289446(L_20, /*hidden argument*/NULL);
		V_11 = L_20;
		U3CTakeJsonU3Ec__AnonStorey26_t291817749 * L_21 = V_11;
		NullCheck(L_21);
		L_21->set_U3CU3Ef__this_1(__this);
		U3CTakeJsonU3Ec__AnonStorey26_t291817749 * L_22 = V_11;
		GameObject_t3674682005 * L_23 = __this->get_m_Button_15();
		GameObject_t3674682005 * L_24 = Object_Instantiate_TisGameObject_t3674682005_m3917608929(NULL /*static, unused*/, L_23, /*hidden argument*/Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var);
		NullCheck(L_22);
		L_22->set_Btn_0(L_24);
		U3CTakeJsonU3Ec__AnonStorey26_t291817749 * L_25 = V_11;
		NullCheck(L_25);
		GameObject_t3674682005 * L_26 = L_25->get_Btn_0();
		int32_t L_27 = V_1;
		int32_t L_28 = L_27;
		Il2CppObject * L_29 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_28);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2001146706, L_29, /*hidden argument*/NULL);
		NullCheck(L_26);
		Object_set_name_m1123518500(L_26, L_30, /*hidden argument*/NULL);
		U3CTakeJsonU3Ec__AnonStorey26_t291817749 * L_31 = V_11;
		NullCheck(L_31);
		GameObject_t3674682005 * L_32 = L_31->get_Btn_0();
		NullCheck(L_32);
		Transform_t1659122786 * L_33 = GameObject_get_transform_m1278640159(L_32, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_34 = __this->get_m_Parent_16();
		NullCheck(L_34);
		Transform_t1659122786 * L_35 = GameObject_get_transform_m1278640159(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		Transform_SetParent_m3449663462(L_33, L_35, /*hidden argument*/NULL);
		U3CTakeJsonU3Ec__AnonStorey26_t291817749 * L_36 = V_11;
		NullCheck(L_36);
		GameObject_t3674682005 * L_37 = L_36->get_Btn_0();
		NullCheck(L_37);
		Transform_t1659122786 * L_38 = GameObject_get_transform_m1278640159(L_37, /*hidden argument*/NULL);
		int32_t L_39 = V_1;
		Vector3_t4282066566  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Vector3__ctor_m2926210380(&L_40, (0.0f), ((float)((float)(-110.5f)+(float)(((float)((float)((int32_t)((int32_t)((int32_t)-540)*(int32_t)L_39))))))), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_38);
		Transform_set_localPosition_m3504330903(L_38, L_40, /*hidden argument*/NULL);
		U3CTakeJsonU3Ec__AnonStorey26_t291817749 * L_41 = V_11;
		NullCheck(L_41);
		GameObject_t3674682005 * L_42 = L_41->get_Btn_0();
		NullCheck(L_42);
		Transform_t1659122786 * L_43 = GameObject_get_transform_m1278640159(L_42, /*hidden argument*/NULL);
		Vector3_t4282066566  L_44;
		memset(&L_44, 0, sizeof(L_44));
		Vector3__ctor_m2926210380(&L_44, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_43);
		Transform_set_localScale_m310756934(L_43, L_44, /*hidden argument*/NULL);
		U3CTakeJsonU3Ec__AnonStorey26_t291817749 * L_45 = V_11;
		NullCheck(L_45);
		GameObject_t3674682005 * L_46 = L_45->get_Btn_0();
		NullCheck(L_46);
		RectTransform_t972643934 * L_47 = GameObject_GetComponent_TisRectTransform_t972643934_m406276429(L_46, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var);
		Vector2_t4282066565  L_48 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_47);
		RectTransform_set_offsetMax_m677885815(L_47, L_48, /*hidden argument*/NULL);
		U3CTakeJsonU3Ec__AnonStorey26_t291817749 * L_49 = V_11;
		NullCheck(L_49);
		GameObject_t3674682005 * L_50 = L_49->get_Btn_0();
		NullCheck(L_50);
		RectTransform_t972643934 * L_51 = GameObject_GetComponent_TisRectTransform_t972643934_m406276429(L_50, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var);
		Vector2_t4282066565  L_52;
		memset(&L_52, 0, sizeof(L_52));
		Vector2__ctor_m1517109030(&L_52, (0.0f), (221.0f), /*hidden argument*/NULL);
		NullCheck(L_51);
		RectTransform_set_sizeDelta_m1223846609(L_51, L_52, /*hidden argument*/NULL);
		String_t* L_53 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_3 = L_53;
		String_t* L_54 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_4 = L_54;
		String_t* L_55 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_5 = L_55;
		String_t* L_56 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_6 = L_56;
		String_t* L_57 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_7 = L_57;
		String_t* L_58 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_8 = L_58;
		String_t* L_59 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_9 = L_59;
		String_t* L_60 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_10 = L_60;
		JsonData_t1715015430 * L_61 = V_0;
		NullCheck(L_61);
		JsonData_t1715015430 * L_62 = JsonData_get_Item_m4009629743(L_61, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_62);
		JsonData_t1715015430 * L_63 = JsonData_get_Item_m4009629743(L_62, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_64 = V_1;
		NullCheck(L_63);
		JsonData_t1715015430 * L_65 = JsonData_get_Item_m253158020(L_63, L_64, /*hidden argument*/NULL);
		NullCheck(L_65);
		JsonData_t1715015430 * L_66 = JsonData_get_Item_m4009629743(L_65, _stringLiteral2604245075, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_01e4;
		}
	}
	{
		JsonData_t1715015430 * L_67 = V_0;
		NullCheck(L_67);
		JsonData_t1715015430 * L_68 = JsonData_get_Item_m4009629743(L_67, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_68);
		JsonData_t1715015430 * L_69 = JsonData_get_Item_m4009629743(L_68, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_70 = V_1;
		NullCheck(L_69);
		JsonData_t1715015430 * L_71 = JsonData_get_Item_m253158020(L_69, L_70, /*hidden argument*/NULL);
		NullCheck(L_71);
		JsonData_t1715015430 * L_72 = JsonData_get_Item_m4009629743(L_71, _stringLiteral2604245075, /*hidden argument*/NULL);
		NullCheck(L_72);
		String_t* L_73 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_72);
		V_10 = L_73;
	}

IL_01e4:
	{
		JsonData_t1715015430 * L_74 = V_0;
		NullCheck(L_74);
		JsonData_t1715015430 * L_75 = JsonData_get_Item_m4009629743(L_74, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_75);
		JsonData_t1715015430 * L_76 = JsonData_get_Item_m4009629743(L_75, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_77 = V_1;
		NullCheck(L_76);
		JsonData_t1715015430 * L_78 = JsonData_get_Item_m253158020(L_76, L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		JsonData_t1715015430 * L_79 = JsonData_get_Item_m4009629743(L_78, _stringLiteral110371416, /*hidden argument*/NULL);
		NullCheck(L_79);
		String_t* L_80 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_79);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_81 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_80, /*hidden argument*/NULL);
		if (L_81)
		{
			goto IL_0243;
		}
	}
	{
		JsonData_t1715015430 * L_82 = V_0;
		NullCheck(L_82);
		JsonData_t1715015430 * L_83 = JsonData_get_Item_m4009629743(L_82, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_83);
		JsonData_t1715015430 * L_84 = JsonData_get_Item_m4009629743(L_83, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_85 = V_1;
		NullCheck(L_84);
		JsonData_t1715015430 * L_86 = JsonData_get_Item_m253158020(L_84, L_85, /*hidden argument*/NULL);
		NullCheck(L_86);
		JsonData_t1715015430 * L_87 = JsonData_get_Item_m4009629743(L_86, _stringLiteral110371416, /*hidden argument*/NULL);
		NullCheck(L_87);
		String_t* L_88 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_87);
		V_3 = L_88;
	}

IL_0243:
	{
		JsonData_t1715015430 * L_89 = V_0;
		NullCheck(L_89);
		JsonData_t1715015430 * L_90 = JsonData_get_Item_m4009629743(L_89, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_90);
		JsonData_t1715015430 * L_91 = JsonData_get_Item_m4009629743(L_90, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_92 = V_1;
		NullCheck(L_91);
		JsonData_t1715015430 * L_93 = JsonData_get_Item_m253158020(L_91, L_92, /*hidden argument*/NULL);
		NullCheck(L_93);
		JsonData_t1715015430 * L_94 = JsonData_get_Item_m4009629743(L_93, _stringLiteral3575610, /*hidden argument*/NULL);
		NullCheck(L_94);
		String_t* L_95 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_94);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_96 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_95, /*hidden argument*/NULL);
		if (L_96)
		{
			goto IL_02a3;
		}
	}
	{
		JsonData_t1715015430 * L_97 = V_0;
		NullCheck(L_97);
		JsonData_t1715015430 * L_98 = JsonData_get_Item_m4009629743(L_97, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_98);
		JsonData_t1715015430 * L_99 = JsonData_get_Item_m4009629743(L_98, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_100 = V_1;
		NullCheck(L_99);
		JsonData_t1715015430 * L_101 = JsonData_get_Item_m253158020(L_99, L_100, /*hidden argument*/NULL);
		NullCheck(L_101);
		JsonData_t1715015430 * L_102 = JsonData_get_Item_m4009629743(L_101, _stringLiteral3575610, /*hidden argument*/NULL);
		NullCheck(L_102);
		String_t* L_103 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_102);
		V_4 = L_103;
	}

IL_02a3:
	{
		JsonData_t1715015430 * L_104 = V_0;
		NullCheck(L_104);
		JsonData_t1715015430 * L_105 = JsonData_get_Item_m4009629743(L_104, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_105);
		JsonData_t1715015430 * L_106 = JsonData_get_Item_m4009629743(L_105, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_107 = V_1;
		NullCheck(L_106);
		JsonData_t1715015430 * L_108 = JsonData_get_Item_m253158020(L_106, L_107, /*hidden argument*/NULL);
		NullCheck(L_108);
		JsonData_t1715015430 * L_109 = JsonData_get_Item_m4009629743(L_108, _stringLiteral2872469938, /*hidden argument*/NULL);
		if (!L_109)
		{
			goto IL_02f9;
		}
	}
	{
		JsonData_t1715015430 * L_110 = V_0;
		NullCheck(L_110);
		JsonData_t1715015430 * L_111 = JsonData_get_Item_m4009629743(L_110, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_111);
		JsonData_t1715015430 * L_112 = JsonData_get_Item_m4009629743(L_111, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_113 = V_1;
		NullCheck(L_112);
		JsonData_t1715015430 * L_114 = JsonData_get_Item_m253158020(L_112, L_113, /*hidden argument*/NULL);
		NullCheck(L_114);
		JsonData_t1715015430 * L_115 = JsonData_get_Item_m4009629743(L_114, _stringLiteral2872469938, /*hidden argument*/NULL);
		NullCheck(L_115);
		String_t* L_116 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_115);
		V_5 = L_116;
	}

IL_02f9:
	{
		JsonData_t1715015430 * L_117 = V_0;
		NullCheck(L_117);
		JsonData_t1715015430 * L_118 = JsonData_get_Item_m4009629743(L_117, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_118);
		JsonData_t1715015430 * L_119 = JsonData_get_Item_m4009629743(L_118, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_120 = V_1;
		NullCheck(L_119);
		JsonData_t1715015430 * L_121 = JsonData_get_Item_m253158020(L_119, L_120, /*hidden argument*/NULL);
		NullCheck(L_121);
		JsonData_t1715015430 * L_122 = JsonData_get_Item_m4009629743(L_121, _stringLiteral2854988908, /*hidden argument*/NULL);
		if (!L_122)
		{
			goto IL_034f;
		}
	}
	{
		JsonData_t1715015430 * L_123 = V_0;
		NullCheck(L_123);
		JsonData_t1715015430 * L_124 = JsonData_get_Item_m4009629743(L_123, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_124);
		JsonData_t1715015430 * L_125 = JsonData_get_Item_m4009629743(L_124, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_126 = V_1;
		NullCheck(L_125);
		JsonData_t1715015430 * L_127 = JsonData_get_Item_m253158020(L_125, L_126, /*hidden argument*/NULL);
		NullCheck(L_127);
		JsonData_t1715015430 * L_128 = JsonData_get_Item_m4009629743(L_127, _stringLiteral2854988908, /*hidden argument*/NULL);
		NullCheck(L_128);
		String_t* L_129 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_128);
		V_6 = L_129;
	}

IL_034f:
	{
		JsonData_t1715015430 * L_130 = V_0;
		NullCheck(L_130);
		JsonData_t1715015430 * L_131 = JsonData_get_Item_m4009629743(L_130, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_131);
		JsonData_t1715015430 * L_132 = JsonData_get_Item_m4009629743(L_131, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_133 = V_1;
		NullCheck(L_132);
		JsonData_t1715015430 * L_134 = JsonData_get_Item_m253158020(L_132, L_133, /*hidden argument*/NULL);
		NullCheck(L_134);
		JsonData_t1715015430 * L_135 = JsonData_get_Item_m4009629743(L_134, _stringLiteral137365935, /*hidden argument*/NULL);
		if (!L_135)
		{
			goto IL_03a5;
		}
	}
	{
		JsonData_t1715015430 * L_136 = V_0;
		NullCheck(L_136);
		JsonData_t1715015430 * L_137 = JsonData_get_Item_m4009629743(L_136, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_137);
		JsonData_t1715015430 * L_138 = JsonData_get_Item_m4009629743(L_137, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_139 = V_1;
		NullCheck(L_138);
		JsonData_t1715015430 * L_140 = JsonData_get_Item_m253158020(L_138, L_139, /*hidden argument*/NULL);
		NullCheck(L_140);
		JsonData_t1715015430 * L_141 = JsonData_get_Item_m4009629743(L_140, _stringLiteral137365935, /*hidden argument*/NULL);
		NullCheck(L_141);
		String_t* L_142 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_141);
		V_7 = L_142;
	}

IL_03a5:
	{
		JsonData_t1715015430 * L_143 = V_0;
		NullCheck(L_143);
		JsonData_t1715015430 * L_144 = JsonData_get_Item_m4009629743(L_143, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_144);
		JsonData_t1715015430 * L_145 = JsonData_get_Item_m4009629743(L_144, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_146 = V_1;
		NullCheck(L_145);
		JsonData_t1715015430 * L_147 = JsonData_get_Item_m253158020(L_145, L_146, /*hidden argument*/NULL);
		NullCheck(L_147);
		JsonData_t1715015430 * L_148 = JsonData_get_Item_m4009629743(L_147, _stringLiteral473771429, /*hidden argument*/NULL);
		NullCheck(L_148);
		String_t* L_149 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_148);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_150 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_149, /*hidden argument*/NULL);
		if (L_150)
		{
			goto IL_0405;
		}
	}
	{
		JsonData_t1715015430 * L_151 = V_0;
		NullCheck(L_151);
		JsonData_t1715015430 * L_152 = JsonData_get_Item_m4009629743(L_151, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_152);
		JsonData_t1715015430 * L_153 = JsonData_get_Item_m4009629743(L_152, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_154 = V_1;
		NullCheck(L_153);
		JsonData_t1715015430 * L_155 = JsonData_get_Item_m253158020(L_153, L_154, /*hidden argument*/NULL);
		NullCheck(L_155);
		JsonData_t1715015430 * L_156 = JsonData_get_Item_m4009629743(L_155, _stringLiteral473771429, /*hidden argument*/NULL);
		NullCheck(L_156);
		String_t* L_157 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_156);
		V_8 = L_157;
	}

IL_0405:
	{
		JsonData_t1715015430 * L_158 = V_0;
		NullCheck(L_158);
		JsonData_t1715015430 * L_159 = JsonData_get_Item_m4009629743(L_158, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_159);
		JsonData_t1715015430 * L_160 = JsonData_get_Item_m4009629743(L_159, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_161 = V_1;
		NullCheck(L_160);
		JsonData_t1715015430 * L_162 = JsonData_get_Item_m253158020(L_160, L_161, /*hidden argument*/NULL);
		NullCheck(L_162);
		JsonData_t1715015430 * L_163 = JsonData_get_Item_m4009629743(L_162, _stringLiteral3800909073, /*hidden argument*/NULL);
		if (!L_163)
		{
			goto IL_045b;
		}
	}
	{
		JsonData_t1715015430 * L_164 = V_0;
		NullCheck(L_164);
		JsonData_t1715015430 * L_165 = JsonData_get_Item_m4009629743(L_164, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_165);
		JsonData_t1715015430 * L_166 = JsonData_get_Item_m4009629743(L_165, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_167 = V_1;
		NullCheck(L_166);
		JsonData_t1715015430 * L_168 = JsonData_get_Item_m253158020(L_166, L_167, /*hidden argument*/NULL);
		NullCheck(L_168);
		JsonData_t1715015430 * L_169 = JsonData_get_Item_m4009629743(L_168, _stringLiteral3800909073, /*hidden argument*/NULL);
		NullCheck(L_169);
		String_t* L_170 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_169);
		V_9 = L_170;
	}

IL_045b:
	{
		MessagesU5BU5D_t3583122277* L_171 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get_m_Messages_6();
		int32_t L_172 = V_1;
		NullCheck(L_171);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_171, L_172);
		int32_t L_173 = __this->get_categorynum_12();
		String_t* L_174 = V_3;
		String_t* L_175 = V_4;
		String_t* L_176 = V_5;
		String_t* L_177 = V_6;
		String_t* L_178 = V_7;
		String_t* L_179 = V_8;
		String_t* L_180 = V_3;
		String_t* L_181 = V_9;
		String_t* L_182 = V_10;
		Messages__Message_m2086894250(((L_171)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_172))), L_173, L_174, L_175, L_176, L_177, L_178, L_179, L_180, L_181, L_182, /*hidden argument*/NULL);
		U3CTakeJsonU3Ec__AnonStorey26_t291817749 * L_183 = V_11;
		NullCheck(L_183);
		GameObject_t3674682005 * L_184 = L_183->get_Btn_0();
		NullCheck(L_184);
		Text_t9039225 * L_185 = GameObject_GetComponentInChildren_TisText_t9039225_m2924853261(L_184, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t9039225_m2924853261_MethodInfo_var);
		String_t* L_186 = V_3;
		String_t* L_187 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_188 = String_Concat_m1825781833(NULL /*static, unused*/, L_186, _stringLiteral10, L_187, /*hidden argument*/NULL);
		NullCheck(L_185);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_185, L_188);
		U3CTakeJsonU3Ec__AnonStorey26_t291817749 * L_189 = V_11;
		NullCheck(L_189);
		GameObject_t3674682005 * L_190 = L_189->get_Btn_0();
		NullCheck(L_190);
		Button_t3896396478 * L_191 = GameObject_GetComponent_TisButton_t3896396478_m2812094092(L_190, /*hidden argument*/GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var);
		NullCheck(L_191);
		ButtonClickedEvent_t2796375743 * L_192 = Button_get_onClick_m1145127631(L_191, /*hidden argument*/NULL);
		U3CTakeJsonU3Ec__AnonStorey26_t291817749 * L_193 = V_11;
		IntPtr_t L_194;
		L_194.set_m_value_0((void*)(void*)U3CTakeJsonU3Ec__AnonStorey26_U3CU3Em__3_m681730374_MethodInfo_var);
		UnityAction_t594794173 * L_195 = (UnityAction_t594794173 *)il2cpp_codegen_object_new(UnityAction_t594794173_il2cpp_TypeInfo_var);
		UnityAction__ctor_m4130179243(L_195, L_193, L_194, /*hidden argument*/NULL);
		NullCheck(L_192);
		UnityEvent_AddListener_m4099140869(L_192, L_195, /*hidden argument*/NULL);
	}

IL_04c2:
	{
		int32_t L_196 = V_1;
		V_1 = ((int32_t)((int32_t)L_196+(int32_t)1));
	}

IL_04c6:
	{
		int32_t L_197 = V_1;
		JsonData_t1715015430 * L_198 = V_0;
		NullCheck(L_198);
		JsonData_t1715015430 * L_199 = JsonData_get_Item_m253158020(L_198, 1, /*hidden argument*/NULL);
		NullCheck(L_199);
		JsonData_t1715015430 * L_200 = JsonData_get_Item_m253158020(L_199, 1, /*hidden argument*/NULL);
		NullCheck(L_200);
		int32_t L_201 = JsonData_get_Count_m412158079(L_200, /*hidden argument*/NULL);
		if ((((int32_t)L_197) < ((int32_t)L_201)))
		{
			goto IL_0053;
		}
	}

IL_04de:
	{
		return;
	}
}
// System.Collections.IEnumerator MessageSee::RecvJsonStart()
extern Il2CppClass* U3CRecvJsonStartU3Ec__Iterator1E_t2718408706_il2cpp_TypeInfo_var;
extern const uint32_t MessageSee_RecvJsonStart_m2967129369_MetadataUsageId;
extern "C"  Il2CppObject * MessageSee_RecvJsonStart_m2967129369 (MessageSee_t302730988 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageSee_RecvJsonStart_m2967129369_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CRecvJsonStartU3Ec__Iterator1E_t2718408706 * V_0 = NULL;
	{
		U3CRecvJsonStartU3Ec__Iterator1E_t2718408706 * L_0 = (U3CRecvJsonStartU3Ec__Iterator1E_t2718408706 *)il2cpp_codegen_object_new(U3CRecvJsonStartU3Ec__Iterator1E_t2718408706_il2cpp_TypeInfo_var);
		U3CRecvJsonStartU3Ec__Iterator1E__ctor_m2930954665(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRecvJsonStartU3Ec__Iterator1E_t2718408706 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CRecvJsonStartU3Ec__Iterator1E_t2718408706 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator MessageSee::MessageSeeRecvCheckProgress(UnityEngine.WWW)
extern Il2CppClass* U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178_il2cpp_TypeInfo_var;
extern const uint32_t MessageSee_MessageSeeRecvCheckProgress_m1648991882_MetadataUsageId;
extern "C"  Il2CppObject * MessageSee_MessageSeeRecvCheckProgress_m1648991882 (MessageSee_t302730988 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageSee_MessageSeeRecvCheckProgress_m1648991882_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 * V_0 = NULL;
	{
		U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 * L_0 = (U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 *)il2cpp_codegen_object_new(U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178_il2cpp_TypeInfo_var);
		U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F__ctor_m2471475097(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 * L_1 = V_0;
		WWW_t3134621005 * L_2 = ___www0;
		NullCheck(L_1);
		L_1->set_www_0(L_2);
		U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 * L_3 = V_0;
		WWW_t3134621005 * L_4 = ___www0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Ewww_3(L_4);
		U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 * L_5 = V_0;
		return L_5;
	}
}
// System.Void MessageSee::RecvTakeJson(System.String)
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* MessagesU5BU5D_t3583122277_il2cpp_TypeInfo_var;
extern Il2CppClass* MessageCheck_t3146986849_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_t594794173_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponentInChildren_TisText_t9039225_m2924853261_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var;
extern const MethodInfo* U3CRecvTakeJsonU3Ec__AnonStorey27_U3CU3Em__4_m787397582_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2001146706;
extern Il2CppCodeGenString* _stringLiteral3897517420;
extern Il2CppCodeGenString* _stringLiteral2289459;
extern Il2CppCodeGenString* _stringLiteral2604245075;
extern Il2CppCodeGenString* _stringLiteral110371416;
extern Il2CppCodeGenString* _stringLiteral3575610;
extern Il2CppCodeGenString* _stringLiteral2872469938;
extern Il2CppCodeGenString* _stringLiteral2854988908;
extern Il2CppCodeGenString* _stringLiteral137365935;
extern Il2CppCodeGenString* _stringLiteral473771429;
extern Il2CppCodeGenString* _stringLiteral3800909073;
extern Il2CppCodeGenString* _stringLiteral10;
extern const uint32_t MessageSee_RecvTakeJson_m3430226488_MetadataUsageId;
extern "C"  void MessageSee_RecvTakeJson_m3430226488 (MessageSee_t302730988 * __this, String_t* ___js0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageSee_RecvTakeJson_m3430226488_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JsonData_t1715015430 * V_0 = NULL;
	int32_t V_1 = 0;
	GameObject_t3674682005 * V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148 * V_11 = NULL;
	{
		String_t* L_0 = ___js0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		JsonData_t1715015430 * L_1 = JsonMapper_ToObject_m3425498490(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___js0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		JsonData_t1715015430 * L_3 = V_0;
		NullCheck(L_3);
		JsonData_t1715015430 * L_4 = JsonData_get_Item_m253158020(L_3, 1, /*hidden argument*/NULL);
		NullCheck(L_4);
		JsonData_t1715015430 * L_5 = JsonData_get_Item_m253158020(L_4, 0, /*hidden argument*/NULL);
		NullCheck(L_5);
		JsonData_t1715015430 * L_6 = JsonData_get_Item_m253158020(L_5, 0, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t476798718_il2cpp_TypeInfo_var);
		bool L_8 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_04de;
		}
	}
	{
		JsonData_t1715015430 * L_9 = V_0;
		NullCheck(L_9);
		JsonData_t1715015430 * L_10 = JsonData_get_Item_m253158020(L_9, 1, /*hidden argument*/NULL);
		NullCheck(L_10);
		JsonData_t1715015430 * L_11 = JsonData_get_Item_m253158020(L_10, 1, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = JsonData_get_Count_m412158079(L_11, /*hidden argument*/NULL);
		((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->set_m_Messages_6(((MessagesU5BU5D_t3583122277*)SZArrayNew(MessagesU5BU5D_t3583122277_il2cpp_TypeInfo_var, (uint32_t)L_12)));
		V_1 = 0;
		goto IL_04c6;
	}

IL_0053:
	{
		int32_t L_13 = V_1;
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2001146706, L_15, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_17 = GameObject_Find_m332785498(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		GameObject_t3674682005 * L_18 = V_2;
		bool L_19 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_18, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_04c2;
		}
	}
	{
		U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148 * L_20 = (U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148 *)il2cpp_codegen_object_new(U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148_il2cpp_TypeInfo_var);
		U3CRecvTakeJsonU3Ec__AnonStorey27__ctor_m4045340031(L_20, /*hidden argument*/NULL);
		V_11 = L_20;
		U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148 * L_21 = V_11;
		NullCheck(L_21);
		L_21->set_U3CU3Ef__this_1(__this);
		U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148 * L_22 = V_11;
		GameObject_t3674682005 * L_23 = __this->get_m_Button_15();
		GameObject_t3674682005 * L_24 = Object_Instantiate_TisGameObject_t3674682005_m3917608929(NULL /*static, unused*/, L_23, /*hidden argument*/Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var);
		NullCheck(L_22);
		L_22->set_Btn_0(L_24);
		U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148 * L_25 = V_11;
		NullCheck(L_25);
		GameObject_t3674682005 * L_26 = L_25->get_Btn_0();
		int32_t L_27 = V_1;
		int32_t L_28 = L_27;
		Il2CppObject * L_29 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_28);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2001146706, L_29, /*hidden argument*/NULL);
		NullCheck(L_26);
		Object_set_name_m1123518500(L_26, L_30, /*hidden argument*/NULL);
		U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148 * L_31 = V_11;
		NullCheck(L_31);
		GameObject_t3674682005 * L_32 = L_31->get_Btn_0();
		NullCheck(L_32);
		Transform_t1659122786 * L_33 = GameObject_get_transform_m1278640159(L_32, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_34 = __this->get_m_Parent_16();
		NullCheck(L_34);
		Transform_t1659122786 * L_35 = GameObject_get_transform_m1278640159(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		Transform_SetParent_m3449663462(L_33, L_35, /*hidden argument*/NULL);
		U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148 * L_36 = V_11;
		NullCheck(L_36);
		GameObject_t3674682005 * L_37 = L_36->get_Btn_0();
		NullCheck(L_37);
		Transform_t1659122786 * L_38 = GameObject_get_transform_m1278640159(L_37, /*hidden argument*/NULL);
		int32_t L_39 = V_1;
		Vector3_t4282066566  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Vector3__ctor_m2926210380(&L_40, (0.0f), ((float)((float)(-110.5f)+(float)(((float)((float)((int32_t)((int32_t)((int32_t)-540)*(int32_t)L_39))))))), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_38);
		Transform_set_localPosition_m3504330903(L_38, L_40, /*hidden argument*/NULL);
		U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148 * L_41 = V_11;
		NullCheck(L_41);
		GameObject_t3674682005 * L_42 = L_41->get_Btn_0();
		NullCheck(L_42);
		Transform_t1659122786 * L_43 = GameObject_get_transform_m1278640159(L_42, /*hidden argument*/NULL);
		Vector3_t4282066566  L_44;
		memset(&L_44, 0, sizeof(L_44));
		Vector3__ctor_m2926210380(&L_44, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_43);
		Transform_set_localScale_m310756934(L_43, L_44, /*hidden argument*/NULL);
		U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148 * L_45 = V_11;
		NullCheck(L_45);
		GameObject_t3674682005 * L_46 = L_45->get_Btn_0();
		NullCheck(L_46);
		RectTransform_t972643934 * L_47 = GameObject_GetComponent_TisRectTransform_t972643934_m406276429(L_46, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var);
		Vector2_t4282066565  L_48 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_47);
		RectTransform_set_offsetMax_m677885815(L_47, L_48, /*hidden argument*/NULL);
		U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148 * L_49 = V_11;
		NullCheck(L_49);
		GameObject_t3674682005 * L_50 = L_49->get_Btn_0();
		NullCheck(L_50);
		RectTransform_t972643934 * L_51 = GameObject_GetComponent_TisRectTransform_t972643934_m406276429(L_50, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var);
		Vector2_t4282066565  L_52;
		memset(&L_52, 0, sizeof(L_52));
		Vector2__ctor_m1517109030(&L_52, (0.0f), (221.0f), /*hidden argument*/NULL);
		NullCheck(L_51);
		RectTransform_set_sizeDelta_m1223846609(L_51, L_52, /*hidden argument*/NULL);
		String_t* L_53 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_3 = L_53;
		String_t* L_54 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_4 = L_54;
		String_t* L_55 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_5 = L_55;
		String_t* L_56 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_6 = L_56;
		String_t* L_57 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_7 = L_57;
		String_t* L_58 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_8 = L_58;
		String_t* L_59 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_9 = L_59;
		String_t* L_60 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_10 = L_60;
		JsonData_t1715015430 * L_61 = V_0;
		NullCheck(L_61);
		JsonData_t1715015430 * L_62 = JsonData_get_Item_m4009629743(L_61, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_62);
		JsonData_t1715015430 * L_63 = JsonData_get_Item_m4009629743(L_62, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_64 = V_1;
		NullCheck(L_63);
		JsonData_t1715015430 * L_65 = JsonData_get_Item_m253158020(L_63, L_64, /*hidden argument*/NULL);
		NullCheck(L_65);
		JsonData_t1715015430 * L_66 = JsonData_get_Item_m4009629743(L_65, _stringLiteral2604245075, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_01e4;
		}
	}
	{
		JsonData_t1715015430 * L_67 = V_0;
		NullCheck(L_67);
		JsonData_t1715015430 * L_68 = JsonData_get_Item_m4009629743(L_67, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_68);
		JsonData_t1715015430 * L_69 = JsonData_get_Item_m4009629743(L_68, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_70 = V_1;
		NullCheck(L_69);
		JsonData_t1715015430 * L_71 = JsonData_get_Item_m253158020(L_69, L_70, /*hidden argument*/NULL);
		NullCheck(L_71);
		JsonData_t1715015430 * L_72 = JsonData_get_Item_m4009629743(L_71, _stringLiteral2604245075, /*hidden argument*/NULL);
		NullCheck(L_72);
		String_t* L_73 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_72);
		V_10 = L_73;
	}

IL_01e4:
	{
		JsonData_t1715015430 * L_74 = V_0;
		NullCheck(L_74);
		JsonData_t1715015430 * L_75 = JsonData_get_Item_m4009629743(L_74, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_75);
		JsonData_t1715015430 * L_76 = JsonData_get_Item_m4009629743(L_75, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_77 = V_1;
		NullCheck(L_76);
		JsonData_t1715015430 * L_78 = JsonData_get_Item_m253158020(L_76, L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		JsonData_t1715015430 * L_79 = JsonData_get_Item_m4009629743(L_78, _stringLiteral110371416, /*hidden argument*/NULL);
		NullCheck(L_79);
		String_t* L_80 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_79);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_81 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_80, /*hidden argument*/NULL);
		if (L_81)
		{
			goto IL_0243;
		}
	}
	{
		JsonData_t1715015430 * L_82 = V_0;
		NullCheck(L_82);
		JsonData_t1715015430 * L_83 = JsonData_get_Item_m4009629743(L_82, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_83);
		JsonData_t1715015430 * L_84 = JsonData_get_Item_m4009629743(L_83, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_85 = V_1;
		NullCheck(L_84);
		JsonData_t1715015430 * L_86 = JsonData_get_Item_m253158020(L_84, L_85, /*hidden argument*/NULL);
		NullCheck(L_86);
		JsonData_t1715015430 * L_87 = JsonData_get_Item_m4009629743(L_86, _stringLiteral110371416, /*hidden argument*/NULL);
		NullCheck(L_87);
		String_t* L_88 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_87);
		V_3 = L_88;
	}

IL_0243:
	{
		JsonData_t1715015430 * L_89 = V_0;
		NullCheck(L_89);
		JsonData_t1715015430 * L_90 = JsonData_get_Item_m4009629743(L_89, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_90);
		JsonData_t1715015430 * L_91 = JsonData_get_Item_m4009629743(L_90, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_92 = V_1;
		NullCheck(L_91);
		JsonData_t1715015430 * L_93 = JsonData_get_Item_m253158020(L_91, L_92, /*hidden argument*/NULL);
		NullCheck(L_93);
		JsonData_t1715015430 * L_94 = JsonData_get_Item_m4009629743(L_93, _stringLiteral3575610, /*hidden argument*/NULL);
		NullCheck(L_94);
		String_t* L_95 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_94);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_96 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_95, /*hidden argument*/NULL);
		if (L_96)
		{
			goto IL_02a3;
		}
	}
	{
		JsonData_t1715015430 * L_97 = V_0;
		NullCheck(L_97);
		JsonData_t1715015430 * L_98 = JsonData_get_Item_m4009629743(L_97, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_98);
		JsonData_t1715015430 * L_99 = JsonData_get_Item_m4009629743(L_98, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_100 = V_1;
		NullCheck(L_99);
		JsonData_t1715015430 * L_101 = JsonData_get_Item_m253158020(L_99, L_100, /*hidden argument*/NULL);
		NullCheck(L_101);
		JsonData_t1715015430 * L_102 = JsonData_get_Item_m4009629743(L_101, _stringLiteral3575610, /*hidden argument*/NULL);
		NullCheck(L_102);
		String_t* L_103 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_102);
		V_4 = L_103;
	}

IL_02a3:
	{
		JsonData_t1715015430 * L_104 = V_0;
		NullCheck(L_104);
		JsonData_t1715015430 * L_105 = JsonData_get_Item_m4009629743(L_104, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_105);
		JsonData_t1715015430 * L_106 = JsonData_get_Item_m4009629743(L_105, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_107 = V_1;
		NullCheck(L_106);
		JsonData_t1715015430 * L_108 = JsonData_get_Item_m253158020(L_106, L_107, /*hidden argument*/NULL);
		NullCheck(L_108);
		JsonData_t1715015430 * L_109 = JsonData_get_Item_m4009629743(L_108, _stringLiteral2872469938, /*hidden argument*/NULL);
		if (!L_109)
		{
			goto IL_02f9;
		}
	}
	{
		JsonData_t1715015430 * L_110 = V_0;
		NullCheck(L_110);
		JsonData_t1715015430 * L_111 = JsonData_get_Item_m4009629743(L_110, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_111);
		JsonData_t1715015430 * L_112 = JsonData_get_Item_m4009629743(L_111, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_113 = V_1;
		NullCheck(L_112);
		JsonData_t1715015430 * L_114 = JsonData_get_Item_m253158020(L_112, L_113, /*hidden argument*/NULL);
		NullCheck(L_114);
		JsonData_t1715015430 * L_115 = JsonData_get_Item_m4009629743(L_114, _stringLiteral2872469938, /*hidden argument*/NULL);
		NullCheck(L_115);
		String_t* L_116 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_115);
		V_5 = L_116;
	}

IL_02f9:
	{
		JsonData_t1715015430 * L_117 = V_0;
		NullCheck(L_117);
		JsonData_t1715015430 * L_118 = JsonData_get_Item_m4009629743(L_117, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_118);
		JsonData_t1715015430 * L_119 = JsonData_get_Item_m4009629743(L_118, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_120 = V_1;
		NullCheck(L_119);
		JsonData_t1715015430 * L_121 = JsonData_get_Item_m253158020(L_119, L_120, /*hidden argument*/NULL);
		NullCheck(L_121);
		JsonData_t1715015430 * L_122 = JsonData_get_Item_m4009629743(L_121, _stringLiteral2854988908, /*hidden argument*/NULL);
		if (!L_122)
		{
			goto IL_034f;
		}
	}
	{
		JsonData_t1715015430 * L_123 = V_0;
		NullCheck(L_123);
		JsonData_t1715015430 * L_124 = JsonData_get_Item_m4009629743(L_123, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_124);
		JsonData_t1715015430 * L_125 = JsonData_get_Item_m4009629743(L_124, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_126 = V_1;
		NullCheck(L_125);
		JsonData_t1715015430 * L_127 = JsonData_get_Item_m253158020(L_125, L_126, /*hidden argument*/NULL);
		NullCheck(L_127);
		JsonData_t1715015430 * L_128 = JsonData_get_Item_m4009629743(L_127, _stringLiteral2854988908, /*hidden argument*/NULL);
		NullCheck(L_128);
		String_t* L_129 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_128);
		V_6 = L_129;
	}

IL_034f:
	{
		JsonData_t1715015430 * L_130 = V_0;
		NullCheck(L_130);
		JsonData_t1715015430 * L_131 = JsonData_get_Item_m4009629743(L_130, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_131);
		JsonData_t1715015430 * L_132 = JsonData_get_Item_m4009629743(L_131, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_133 = V_1;
		NullCheck(L_132);
		JsonData_t1715015430 * L_134 = JsonData_get_Item_m253158020(L_132, L_133, /*hidden argument*/NULL);
		NullCheck(L_134);
		JsonData_t1715015430 * L_135 = JsonData_get_Item_m4009629743(L_134, _stringLiteral137365935, /*hidden argument*/NULL);
		if (!L_135)
		{
			goto IL_03a5;
		}
	}
	{
		JsonData_t1715015430 * L_136 = V_0;
		NullCheck(L_136);
		JsonData_t1715015430 * L_137 = JsonData_get_Item_m4009629743(L_136, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_137);
		JsonData_t1715015430 * L_138 = JsonData_get_Item_m4009629743(L_137, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_139 = V_1;
		NullCheck(L_138);
		JsonData_t1715015430 * L_140 = JsonData_get_Item_m253158020(L_138, L_139, /*hidden argument*/NULL);
		NullCheck(L_140);
		JsonData_t1715015430 * L_141 = JsonData_get_Item_m4009629743(L_140, _stringLiteral137365935, /*hidden argument*/NULL);
		NullCheck(L_141);
		String_t* L_142 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_141);
		V_7 = L_142;
	}

IL_03a5:
	{
		JsonData_t1715015430 * L_143 = V_0;
		NullCheck(L_143);
		JsonData_t1715015430 * L_144 = JsonData_get_Item_m4009629743(L_143, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_144);
		JsonData_t1715015430 * L_145 = JsonData_get_Item_m4009629743(L_144, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_146 = V_1;
		NullCheck(L_145);
		JsonData_t1715015430 * L_147 = JsonData_get_Item_m253158020(L_145, L_146, /*hidden argument*/NULL);
		NullCheck(L_147);
		JsonData_t1715015430 * L_148 = JsonData_get_Item_m4009629743(L_147, _stringLiteral473771429, /*hidden argument*/NULL);
		NullCheck(L_148);
		String_t* L_149 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_148);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_150 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_149, /*hidden argument*/NULL);
		if (L_150)
		{
			goto IL_0405;
		}
	}
	{
		JsonData_t1715015430 * L_151 = V_0;
		NullCheck(L_151);
		JsonData_t1715015430 * L_152 = JsonData_get_Item_m4009629743(L_151, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_152);
		JsonData_t1715015430 * L_153 = JsonData_get_Item_m4009629743(L_152, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_154 = V_1;
		NullCheck(L_153);
		JsonData_t1715015430 * L_155 = JsonData_get_Item_m253158020(L_153, L_154, /*hidden argument*/NULL);
		NullCheck(L_155);
		JsonData_t1715015430 * L_156 = JsonData_get_Item_m4009629743(L_155, _stringLiteral473771429, /*hidden argument*/NULL);
		NullCheck(L_156);
		String_t* L_157 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_156);
		V_8 = L_157;
	}

IL_0405:
	{
		JsonData_t1715015430 * L_158 = V_0;
		NullCheck(L_158);
		JsonData_t1715015430 * L_159 = JsonData_get_Item_m4009629743(L_158, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_159);
		JsonData_t1715015430 * L_160 = JsonData_get_Item_m4009629743(L_159, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_161 = V_1;
		NullCheck(L_160);
		JsonData_t1715015430 * L_162 = JsonData_get_Item_m253158020(L_160, L_161, /*hidden argument*/NULL);
		NullCheck(L_162);
		JsonData_t1715015430 * L_163 = JsonData_get_Item_m4009629743(L_162, _stringLiteral3800909073, /*hidden argument*/NULL);
		if (!L_163)
		{
			goto IL_045b;
		}
	}
	{
		JsonData_t1715015430 * L_164 = V_0;
		NullCheck(L_164);
		JsonData_t1715015430 * L_165 = JsonData_get_Item_m4009629743(L_164, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_165);
		JsonData_t1715015430 * L_166 = JsonData_get_Item_m4009629743(L_165, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_167 = V_1;
		NullCheck(L_166);
		JsonData_t1715015430 * L_168 = JsonData_get_Item_m253158020(L_166, L_167, /*hidden argument*/NULL);
		NullCheck(L_168);
		JsonData_t1715015430 * L_169 = JsonData_get_Item_m4009629743(L_168, _stringLiteral3800909073, /*hidden argument*/NULL);
		NullCheck(L_169);
		String_t* L_170 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_169);
		V_9 = L_170;
	}

IL_045b:
	{
		MessagesU5BU5D_t3583122277* L_171 = ((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->get_m_Messages_6();
		int32_t L_172 = V_1;
		NullCheck(L_171);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_171, L_172);
		int32_t L_173 = __this->get_categorynum_12();
		String_t* L_174 = V_3;
		String_t* L_175 = V_4;
		String_t* L_176 = V_5;
		String_t* L_177 = V_6;
		String_t* L_178 = V_7;
		String_t* L_179 = V_8;
		String_t* L_180 = V_3;
		String_t* L_181 = V_9;
		String_t* L_182 = V_10;
		Messages__Message_m2086894250(((L_171)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_172))), L_173, L_174, L_175, L_176, L_177, L_178, L_179, L_180, L_181, L_182, /*hidden argument*/NULL);
		U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148 * L_183 = V_11;
		NullCheck(L_183);
		GameObject_t3674682005 * L_184 = L_183->get_Btn_0();
		NullCheck(L_184);
		Text_t9039225 * L_185 = GameObject_GetComponentInChildren_TisText_t9039225_m2924853261(L_184, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t9039225_m2924853261_MethodInfo_var);
		String_t* L_186 = V_3;
		String_t* L_187 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_188 = String_Concat_m1825781833(NULL /*static, unused*/, L_186, _stringLiteral10, L_187, /*hidden argument*/NULL);
		NullCheck(L_185);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_185, L_188);
		U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148 * L_189 = V_11;
		NullCheck(L_189);
		GameObject_t3674682005 * L_190 = L_189->get_Btn_0();
		NullCheck(L_190);
		Button_t3896396478 * L_191 = GameObject_GetComponent_TisButton_t3896396478_m2812094092(L_190, /*hidden argument*/GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var);
		NullCheck(L_191);
		ButtonClickedEvent_t2796375743 * L_192 = Button_get_onClick_m1145127631(L_191, /*hidden argument*/NULL);
		U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148 * L_193 = V_11;
		IntPtr_t L_194;
		L_194.set_m_value_0((void*)(void*)U3CRecvTakeJsonU3Ec__AnonStorey27_U3CU3Em__4_m787397582_MethodInfo_var);
		UnityAction_t594794173 * L_195 = (UnityAction_t594794173 *)il2cpp_codegen_object_new(UnityAction_t594794173_il2cpp_TypeInfo_var);
		UnityAction__ctor_m4130179243(L_195, L_193, L_194, /*hidden argument*/NULL);
		NullCheck(L_192);
		UnityEvent_AddListener_m4099140869(L_192, L_195, /*hidden argument*/NULL);
	}

IL_04c2:
	{
		int32_t L_196 = V_1;
		V_1 = ((int32_t)((int32_t)L_196+(int32_t)1));
	}

IL_04c6:
	{
		int32_t L_197 = V_1;
		JsonData_t1715015430 * L_198 = V_0;
		NullCheck(L_198);
		JsonData_t1715015430 * L_199 = JsonData_get_Item_m253158020(L_198, 1, /*hidden argument*/NULL);
		NullCheck(L_199);
		JsonData_t1715015430 * L_200 = JsonData_get_Item_m253158020(L_199, 1, /*hidden argument*/NULL);
		NullCheck(L_200);
		int32_t L_201 = JsonData_get_Count_m412158079(L_200, /*hidden argument*/NULL);
		if ((((int32_t)L_197) < ((int32_t)L_201)))
		{
			goto IL_0053;
		}
	}

IL_04de:
	{
		return;
	}
}
// System.Void MessageSee::OnEnable()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MessageCheck_t3146986849_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3146986849;
extern Il2CppCodeGenString* _stringLiteral3218270073;
extern Il2CppCodeGenString* _stringLiteral2001146706;
extern const uint32_t MessageSee_OnEnable_m3011375607_MetadataUsageId;
extern "C"  void MessageSee_OnEnable_m3011375607 (MessageSee_t302730988 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageSee_OnEnable_m3011375607_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		GameObject_t3674682005 * L_0 = __this->get_btn_7();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		GameObject_t3674682005 * L_2 = __this->get_m_Text_13();
		__this->set_btn_7(L_2);
	}

IL_001d:
	{
		Text_t9039225 * L_3 = __this->get_tm_8();
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		GameObject_t3674682005 * L_5 = __this->get_m_Text_13();
		NullCheck(L_5);
		Text_t9039225 * L_6 = GameObject_GetComponent_TisText_t9039225_m202917489(L_5, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		__this->set_tm_8(L_6);
	}

IL_003f:
	{
		GameObject_t3674682005 * L_7 = __this->get_btns_9();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005c;
		}
	}
	{
		GameObject_t3674682005 * L_9 = __this->get_m_GOBtn_14();
		__this->set_btns_9(L_9);
	}

IL_005c:
	{
		GameObject_t3674682005 * L_10 = __this->get_mc_10();
		bool L_11 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_10, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0082;
		}
	}
	{
		GameObject_t3674682005 * L_12 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3146986849, /*hidden argument*/NULL);
		NullCheck(L_12);
		GameObject_t3674682005 * L_13 = GameObject_get_gameObject_m1966529385(L_12, /*hidden argument*/NULL);
		__this->set_mc_10(L_13);
	}

IL_0082:
	{
		Button_t3896396478 * L_14 = __this->get_recvbtn_11();
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00ad;
		}
	}
	{
		GameObject_t3674682005 * L_16 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3218270073, /*hidden argument*/NULL);
		NullCheck(L_16);
		GameObject_t3674682005 * L_17 = GameObject_get_gameObject_m1966529385(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Button_t3896396478 * L_18 = GameObject_GetComponent_TisButton_t3896396478_m2812094092(L_17, /*hidden argument*/GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var);
		__this->set_recvbtn_11(L_18);
	}

IL_00ad:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_19 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_19) == ((uint32_t)2))))
		{
			goto IL_00c9;
		}
	}
	{
		Button_t3896396478 * L_20 = __this->get_recvbtn_11();
		NullCheck(L_20);
		Selectable_set_interactable_m2686686419(L_20, (bool)1, /*hidden argument*/NULL);
		goto IL_00fb;
	}

IL_00c9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_21 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_00e4;
		}
	}
	{
		Button_t3896396478 * L_22 = __this->get_recvbtn_11();
		NullCheck(L_22);
		Selectable_set_interactable_m2686686419(L_22, (bool)0, /*hidden argument*/NULL);
		goto IL_00fb;
	}

IL_00e4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_23 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)1))))
		{
			goto IL_00fb;
		}
	}
	{
		Button_t3896396478 * L_24 = __this->get_recvbtn_11();
		NullCheck(L_24);
		Selectable_set_interactable_m2686686419(L_24, (bool)1, /*hidden argument*/NULL);
	}

IL_00fb:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_25 = CurrentState_get_LoginState_m3052003788(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)7))))
		{
			goto IL_010d;
		}
	}
	{
		MessageSee_MessageseeButton_m1046720708(__this, 0, /*hidden argument*/NULL);
	}

IL_010d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_26 = CurrentState_get_LoginState_m3052003788(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_26) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_0187;
		}
	}
	{
		GameObject_t3674682005 * L_27 = __this->get_m_Parent_16();
		NullCheck(L_27);
		Transform_t1659122786 * L_28 = GameObject_get_transform_m1278640159(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		int32_t L_29 = Transform_get_childCount_m2107810675(L_28, /*hidden argument*/NULL);
		if ((((int32_t)L_29) < ((int32_t)1)))
		{
			goto IL_017a;
		}
	}
	{
		V_0 = 0;
		goto IL_0164;
	}

IL_0136:
	{
		GameObject_t3674682005 * L_30 = __this->get_m_Parent_16();
		NullCheck(L_30);
		Transform_t1659122786 * L_31 = GameObject_get_transform_m1278640159(L_30, /*hidden argument*/NULL);
		int32_t L_32 = V_0;
		int32_t L_33 = L_32;
		Il2CppObject * L_34 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_33);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2001146706, L_34, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_t1659122786 * L_36 = Transform_FindChild_m2149912886(L_31, L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		GameObject_t3674682005 * L_37 = Component_get_gameObject_m1170635899(L_36, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		int32_t L_38 = V_0;
		V_0 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_0164:
	{
		int32_t L_39 = V_0;
		GameObject_t3674682005 * L_40 = __this->get_m_Parent_16();
		NullCheck(L_40);
		Transform_t1659122786 * L_41 = GameObject_get_transform_m1278640159(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		int32_t L_42 = Transform_get_childCount_m2107810675(L_41, /*hidden argument*/NULL);
		if ((((int32_t)L_39) < ((int32_t)L_42)))
		{
			goto IL_0136;
		}
	}

IL_017a:
	{
		((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->set_m_Messages_6((MessagesU5BU5D_t3583122277*)NULL);
		MessageSee_MessageseeButton_m1046720708(__this, 0, /*hidden argument*/NULL);
	}

IL_0187:
	{
		return;
	}
}
// System.Void MessageSee::MessageGet()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t MessageSee_MessageGet_m1172803396_MetadataUsageId;
extern "C"  void MessageSee_MessageGet_m1172803396 (MessageSee_t302730988 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageSee_MessageGet_m1172803396_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = CurrentState_get_MemberState_m3859431375(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = MessageSee_JsonStart_m1847667520(__this, L_0, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MessageSee::MessageRecv()
extern "C"  void MessageSee_MessageRecv_m2311656090 (MessageSee_t302730988 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = MessageSee_RecvJsonStart_m2967129369(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MessageSee::MessageseeButton(System.Int32)
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern const uint32_t MessageSee_MessageseeButton_m1046720708_MetadataUsageId;
extern "C"  void MessageSee_MessageseeButton_m1046720708 (MessageSee_t302730988 * __this, int32_t ___num0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageSee_MessageseeButton_m1046720708_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = ((int32_t)3);
		Il2CppObject * L_2 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		int32_t L_4 = ((int32_t)2);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_3, _stringLiteral3660319847, L_6, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_7 = CurrentState_get_LoginState_m3052003788(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)7))))
		{
			goto IL_003b;
		}
	}
	{
		goto IL_0052;
	}

IL_003b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_8 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		LocationService_Start_m1836061721(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_State_m3490075940(NULL /*static, unused*/, ((int32_t)50), /*hidden argument*/NULL);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, 5, /*hidden argument*/NULL);
	}

IL_0052:
	{
		Text_t9039225 * L_9 = __this->get_tm_8();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_9, L_10);
		GameObject_t3674682005 * L_11 = __this->get_btn_7();
		NullCheck(L_11);
		GameObject_SetActive_m3538205401(L_11, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_12 = __this->get_btns_9();
		NullCheck(L_12);
		GameObject_SetActive_m3538205401(L_12, (bool)1, /*hidden argument*/NULL);
		int32_t L_13 = ___num0;
		if (L_13)
		{
			goto IL_00a8;
		}
	}
	{
		TMP_Text_t980027659 * L_14 = __this->get_m_TitleText_17();
		int32_t L_15 = ((int32_t)0);
		Il2CppObject * L_16 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_15);
		NullCheck((Enum_t2862688501 *)L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_16);
		NullCheck(L_14);
		TMP_Text_set_text_m27929696(L_14, L_17, /*hidden argument*/NULL);
		MessageSee_MessageGet_m1172803396(__this, /*hidden argument*/NULL);
		int32_t L_18 = ___num0;
		__this->set_categorynum_12(L_18);
		goto IL_00d2;
	}

IL_00a8:
	{
		int32_t L_19 = ___num0;
		if ((!(((uint32_t)L_19) == ((uint32_t)1))))
		{
			goto IL_00d2;
		}
	}
	{
		TMP_Text_t980027659 * L_20 = __this->get_m_TitleText_17();
		int32_t L_21 = ((int32_t)1);
		Il2CppObject * L_22 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_21);
		NullCheck((Enum_t2862688501 *)L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_22);
		NullCheck(L_20);
		TMP_Text_set_text_m27929696(L_20, L_23, /*hidden argument*/NULL);
		MessageSee_MessageRecv_m2311656090(__this, /*hidden argument*/NULL);
		int32_t L_24 = ___num0;
		__this->set_categorynum_12(L_24);
	}

IL_00d2:
	{
		return;
	}
}
// System.Void MessageSee::MessageseeBackButton()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MessageCheck_t3146986849_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1969446703;
extern Il2CppCodeGenString* _stringLiteral2001146706;
extern const uint32_t MessageSee_MessageseeBackButton_m2729165306_MetadataUsageId;
extern "C"  void MessageSee_MessageseeBackButton_m2729165306 (MessageSee_t302730988 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageSee_MessageseeBackButton_m2729165306_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_0 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)50)))))
		{
			goto IL_00b7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_1 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		LocationService_Stop_m4216060557(L_1, /*hidden argument*/NULL);
		Text_t9039225 * L_2 = __this->get_tm_8();
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, _stringLiteral1969446703);
		GameObject_t3674682005 * L_3 = __this->get_btn_7();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_4 = __this->get_btns_9();
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_5 = __this->get_m_Parent_16();
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = GameObject_get_transform_m1278640159(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = Transform_get_childCount_m2107810675(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) < ((int32_t)1)))
		{
			goto IL_009f;
		}
	}
	{
		V_0 = 0;
		goto IL_0089;
	}

IL_005b:
	{
		GameObject_t3674682005 * L_8 = __this->get_m_Parent_16();
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = GameObject_get_transform_m1278640159(L_8, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2001146706, L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t1659122786 * L_14 = Transform_FindChild_m2149912886(L_9, L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		GameObject_t3674682005 * L_15 = Component_get_gameObject_m1170635899(L_14, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0089:
	{
		int32_t L_17 = V_0;
		GameObject_t3674682005 * L_18 = __this->get_m_Parent_16();
		NullCheck(L_18);
		Transform_t1659122786 * L_19 = GameObject_get_transform_m1278640159(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = Transform_get_childCount_m2107810675(L_19, /*hidden argument*/NULL);
		if ((((int32_t)L_17) < ((int32_t)L_20)))
		{
			goto IL_005b;
		}
	}

IL_009f:
	{
		((MessageCheck_t3146986849_StaticFields*)MessageCheck_t3146986849_il2cpp_TypeInfo_var->static_fields)->set_m_Messages_6((MessagesU5BU5D_t3583122277*)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_State_m3490075940(NULL /*static, unused*/, 5, /*hidden argument*/NULL);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, ((int32_t)50), /*hidden argument*/NULL);
		goto IL_00bd;
	}

IL_00b7:
	{
		CurrentScreen_BackButton_m2788205463(__this, /*hidden argument*/NULL);
	}

IL_00bd:
	{
		return;
	}
}
// System.Void MessageSee::Update()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t MessageSee_Update_m1982588350_MetadataUsageId;
extern "C"  void MessageSee_Update_m1982588350 (MessageSee_t302730988 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageSee_Update_m1982588350_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)278), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		goto IL_0052;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_3 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)50)))))
		{
			goto IL_003e;
		}
	}
	{
		MessageSee_MessageseeBackButton_m2729165306(__this, /*hidden argument*/NULL);
	}

IL_003e:
	{
		goto IL_0052;
	}

IL_0043:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)319), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0052;
		}
	}

IL_0052:
	{
		return;
	}
}
// System.Void MessageSee/<JsonStart>c__Iterator1C::.ctor()
extern "C"  void U3CJsonStartU3Ec__Iterator1C__ctor_m3442672101 (U3CJsonStartU3Ec__Iterator1C_t1966288966 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object MessageSee/<JsonStart>c__Iterator1C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CJsonStartU3Ec__Iterator1C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2251434253 (U3CJsonStartU3Ec__Iterator1C_t1966288966 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object MessageSee/<JsonStart>c__Iterator1C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CJsonStartU3Ec__Iterator1C_System_Collections_IEnumerator_get_Current_m2028594849 (U3CJsonStartU3Ec__Iterator1C_t1966288966 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Boolean MessageSee/<JsonStart>c__Iterator1C::MoveNext()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t U3CJsonStartU3Ec__Iterator1C_MoveNext_m1423986671_MetadataUsageId;
extern "C"  bool U3CJsonStartU3Ec__Iterator1C_MoveNext_m1423986671 (U3CJsonStartU3Ec__Iterator1C_t1966288966 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CJsonStartU3Ec__Iterator1C_MoveNext_m1423986671_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00c0;
		}
	}
	{
		goto IL_0137;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_U3CUrlU3E__0_0(L_2);
		int32_t L_3 = __this->get_MemberState_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_0048;
		}
	}
	{
		String_t* L_4 = PrivateURL_get_Message_m2273489160(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CUrlU3E__0_0(L_4);
		goto IL_007a;
	}

IL_0048:
	{
		int32_t L_5 = __this->get_MemberState_1();
		if (L_5)
		{
			goto IL_0063;
		}
	}
	{
		String_t* L_6 = PrivateURL_get_DeviceMessage_m3117622098(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CUrlU3E__0_0(L_6);
		goto IL_007a;
	}

IL_0063:
	{
		int32_t L_7 = __this->get_MemberState_1();
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_007a;
		}
	}
	{
		String_t* L_8 = PrivateURL_get_Message_m2273489160(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CUrlU3E__0_0(L_8);
	}

IL_007a:
	{
		String_t* L_9 = __this->get_U3CUrlU3E__0_0();
		WWW_t3134621005 * L_10 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m1985874080(L_10, L_9, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__1_2(L_10);
		MessageSee_t302730988 * L_11 = __this->get_U3CU3Ef__this_6();
		MessageSee_t302730988 * L_12 = __this->get_U3CU3Ef__this_6();
		WWW_t3134621005 * L_13 = __this->get_U3CwwwU3E__1_2();
		NullCheck(L_12);
		Il2CppObject * L_14 = MessageSee_MessageSeeCheckProgress_m1726939908(L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		MonoBehaviour_StartCoroutine_m2135303124(L_11, L_14, /*hidden argument*/NULL);
		WWW_t3134621005 * L_15 = __this->get_U3CwwwU3E__1_2();
		__this->set_U24current_4(L_15);
		__this->set_U24PC_3(1);
		goto IL_0139;
	}

IL_00c0:
	{
		WWW_t3134621005 * L_16 = __this->get_U3CwwwU3E__1_2();
		NullCheck(L_16);
		String_t* L_17 = WWW_get_error_m1787423074(L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00eb;
		}
	}
	{
		MessageSee_t302730988 * L_18 = __this->get_U3CU3Ef__this_6();
		WWW_t3134621005 * L_19 = __this->get_U3CwwwU3E__1_2();
		NullCheck(L_19);
		String_t* L_20 = WWW_get_text_m4216049525(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		MessageSee_TakeJson_m4186039550(L_18, L_20, /*hidden argument*/NULL);
		goto IL_0130;
	}

IL_00eb:
	{
		WWW_t3134621005 * L_21 = __this->get_U3CwwwU3E__1_2();
		NullCheck(L_21);
		String_t* L_22 = WWW_get_error_m1787423074(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_23 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_23);
		AndroidWebview_Call_m1677830609(L_23, _stringLiteral3202370, /*hidden argument*/NULL);
		int32_t L_24 = ((int32_t)3);
		Il2CppObject * L_25 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_24);
		NullCheck((Enum_t2862688501 *)L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_25);
		WWW_t3134621005 * L_27 = __this->get_U3CwwwU3E__1_2();
		NullCheck(L_27);
		String_t* L_28 = WWW_get_error_m1787423074(L_27, /*hidden argument*/NULL);
		int32_t L_29 = ((int32_t)2);
		Il2CppObject * L_30 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_29);
		NullCheck((Enum_t2862688501 *)L_30);
		String_t* L_31 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_30);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_26, L_28, L_31, /*hidden argument*/NULL);
	}

IL_0130:
	{
		__this->set_U24PC_3((-1));
	}

IL_0137:
	{
		return (bool)0;
	}

IL_0139:
	{
		return (bool)1;
	}
	// Dead block : IL_013b: ldloc.1
}
// System.Void MessageSee/<JsonStart>c__Iterator1C::Dispose()
extern "C"  void U3CJsonStartU3Ec__Iterator1C_Dispose_m1181354210 (U3CJsonStartU3Ec__Iterator1C_t1966288966 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void MessageSee/<JsonStart>c__Iterator1C::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CJsonStartU3Ec__Iterator1C_Reset_m1089105042_MetadataUsageId;
extern "C"  void U3CJsonStartU3Ec__Iterator1C_Reset_m1089105042 (U3CJsonStartU3Ec__Iterator1C_t1966288966 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CJsonStartU3Ec__Iterator1C_Reset_m1089105042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MessageSee/<MessageSeeCheckProgress>c__Iterator1D::.ctor()
extern "C"  void U3CMessageSeeCheckProgressU3Ec__Iterator1D__ctor_m1954795733 (U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object MessageSee/<MessageSeeCheckProgress>c__Iterator1D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMessageSeeCheckProgressU3Ec__Iterator1D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m717658333 (U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object MessageSee/<MessageSeeCheckProgress>c__Iterator1D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMessageSeeCheckProgressU3Ec__Iterator1D_System_Collections_IEnumerator_get_Current_m2071459441 (U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean MessageSee/<MessageSeeCheckProgress>c__Iterator1D::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3529469;
extern Il2CppCodeGenString* _stringLiteral1800556493;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t U3CMessageSeeCheckProgressU3Ec__Iterator1D_MoveNext_m3457615359_MetadataUsageId;
extern "C"  bool U3CMessageSeeCheckProgressU3Ec__Iterator1D_MoveNext_m3457615359 (U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMessageSeeCheckProgressU3Ec__Iterator1D_MoveNext_m3457615359_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0041;
		}
		if (L_1 == 2)
		{
			goto IL_0081;
		}
	}
	{
		goto IL_0097;
	}

IL_0025:
	{
		WaitForSeconds_t1615819279 * L_2 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_2, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_2);
		__this->set_U24PC_1(1);
		goto IL_0099;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_3 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		AndroidWebview_Call_m335597645(L_3, _stringLiteral3529469, _stringLiteral1800556493, /*hidden argument*/NULL);
		WWW_t3134621005 * L_4 = __this->get_www_0();
		NullCheck(L_4);
		bool L_5 = WWW_get_isDone_m634060017(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0025;
		}
	}
	{
		WaitForSeconds_t1615819279 * L_6 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_6, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(2);
		goto IL_0099;
	}

IL_0081:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_7 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		AndroidWebview_Call_m1677830609(L_7, _stringLiteral3202370, /*hidden argument*/NULL);
		__this->set_U24PC_1((-1));
	}

IL_0097:
	{
		return (bool)0;
	}

IL_0099:
	{
		return (bool)1;
	}
	// Dead block : IL_009b: ldloc.1
}
// System.Void MessageSee/<MessageSeeCheckProgress>c__Iterator1D::Dispose()
extern "C"  void U3CMessageSeeCheckProgressU3Ec__Iterator1D_Dispose_m1556274130 (U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void MessageSee/<MessageSeeCheckProgress>c__Iterator1D::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CMessageSeeCheckProgressU3Ec__Iterator1D_Reset_m3896195970_MetadataUsageId;
extern "C"  void U3CMessageSeeCheckProgressU3Ec__Iterator1D_Reset_m3896195970 (U3CMessageSeeCheckProgressU3Ec__Iterator1D_t961994070 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMessageSeeCheckProgressU3Ec__Iterator1D_Reset_m3896195970_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MessageSee/<MessageSeeRecvCheckProgress>c__Iterator1F::.ctor()
extern "C"  void U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F__ctor_m2471475097 (U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object MessageSee/<MessageSeeRecvCheckProgress>c__Iterator1F::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4282783513 (U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object MessageSee/<MessageSeeRecvCheckProgress>c__Iterator1F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_System_Collections_IEnumerator_get_Current_m835939501 (U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean MessageSee/<MessageSeeRecvCheckProgress>c__Iterator1F::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3529469;
extern Il2CppCodeGenString* _stringLiteral1800556493;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_MoveNext_m2736698043_MetadataUsageId;
extern "C"  bool U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_MoveNext_m2736698043 (U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_MoveNext_m2736698043_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0046;
		}
		if (L_1 == 2)
		{
			goto IL_00ab;
		}
	}
	{
		goto IL_00c1;
	}

IL_0025:
	{
		goto IL_005a;
	}

IL_002a:
	{
		WaitForSeconds_t1615819279 * L_2 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_2, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_2);
		__this->set_U24PC_1(1);
		goto IL_00c3;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_3 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		AndroidWebview_Call_m335597645(L_3, _stringLiteral3529469, _stringLiteral1800556493, /*hidden argument*/NULL);
	}

IL_005a:
	{
		WWW_t3134621005 * L_4 = __this->get_www_0();
		NullCheck(L_4);
		bool L_5 = WWW_get_isDone_m634060017(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002a;
		}
	}
	{
		WWW_t3134621005 * L_6 = __this->get_www_0();
		NullCheck(L_6);
		bool L_7 = WWW_get_isDone_m634060017(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00ba;
		}
	}
	{
		WWW_t3134621005 * L_8 = __this->get_www_0();
		NullCheck(L_8);
		float L_9 = WWW_get_progress_m3186358572(L_8, /*hidden argument*/NULL);
		if ((!(((float)L_9) >= ((float)(1.0f)))))
		{
			goto IL_00ba;
		}
	}
	{
		WaitForSeconds_t1615819279 * L_10 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_10, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_10);
		__this->set_U24PC_1(2);
		goto IL_00c3;
	}

IL_00ab:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_11 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		AndroidWebview_Call_m1677830609(L_11, _stringLiteral3202370, /*hidden argument*/NULL);
	}

IL_00ba:
	{
		__this->set_U24PC_1((-1));
	}

IL_00c1:
	{
		return (bool)0;
	}

IL_00c3:
	{
		return (bool)1;
	}
	// Dead block : IL_00c5: ldloc.1
}
// System.Void MessageSee/<MessageSeeRecvCheckProgress>c__Iterator1F::Dispose()
extern "C"  void U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_Dispose_m4163903894 (U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void MessageSee/<MessageSeeRecvCheckProgress>c__Iterator1F::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_Reset_m117908038_MetadataUsageId;
extern "C"  void U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_Reset_m117908038 (U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_t3146012178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMessageSeeRecvCheckProgressU3Ec__Iterator1F_Reset_m117908038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MessageSee/<RecvJsonStart>c__Iterator1E::.ctor()
extern "C"  void U3CRecvJsonStartU3Ec__Iterator1E__ctor_m2930954665 (U3CRecvJsonStartU3Ec__Iterator1E_t2718408706 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object MessageSee/<RecvJsonStart>c__Iterator1E::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRecvJsonStartU3Ec__Iterator1E_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m531611209 (U3CRecvJsonStartU3Ec__Iterator1E_t2718408706 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object MessageSee/<RecvJsonStart>c__Iterator1E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRecvJsonStartU3Ec__Iterator1E_System_Collections_IEnumerator_get_Current_m1230074845 (U3CRecvJsonStartU3Ec__Iterator1E_t2718408706 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean MessageSee/<RecvJsonStart>c__Iterator1E::MoveNext()
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t U3CRecvJsonStartU3Ec__Iterator1E_MoveNext_m3570201515_MetadataUsageId;
extern "C"  bool U3CRecvJsonStartU3Ec__Iterator1E_MoveNext_m3570201515 (U3CRecvJsonStartU3Ec__Iterator1E_t2718408706 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CRecvJsonStartU3Ec__Iterator1E_MoveNext_m3570201515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0066;
		}
	}
	{
		goto IL_00b7;
	}

IL_0021:
	{
		String_t* L_2 = PrivateURL_get_MessageRecv_m5945870(NULL /*static, unused*/, /*hidden argument*/NULL);
		WWW_t3134621005 * L_3 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m1985874080(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_0(L_3);
		MessageSee_t302730988 * L_4 = __this->get_U3CU3Ef__this_3();
		MessageSee_t302730988 * L_5 = __this->get_U3CU3Ef__this_3();
		WWW_t3134621005 * L_6 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_5);
		Il2CppObject * L_7 = MessageSee_MessageSeeRecvCheckProgress_m1648991882(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		MonoBehaviour_StartCoroutine_m2135303124(L_4, L_7, /*hidden argument*/NULL);
		WWW_t3134621005 * L_8 = __this->get_U3CwwwU3E__0_0();
		__this->set_U24current_2(L_8);
		__this->set_U24PC_1(1);
		goto IL_00b9;
	}

IL_0066:
	{
		WWW_t3134621005 * L_9 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_9);
		String_t* L_10 = WWW_get_error_m1787423074(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0091;
		}
	}
	{
		MessageSee_t302730988 * L_11 = __this->get_U3CU3Ef__this_3();
		WWW_t3134621005 * L_12 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_12);
		String_t* L_13 = WWW_get_text_m4216049525(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		MessageSee_RecvTakeJson_m3430226488(L_11, L_13, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_0091:
	{
		WWW_t3134621005 * L_14 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_14);
		String_t* L_15 = WWW_get_error_m1787423074(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_16 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		AndroidWebview_Call_m1677830609(L_16, _stringLiteral3202370, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		__this->set_U24PC_1((-1));
	}

IL_00b7:
	{
		return (bool)0;
	}

IL_00b9:
	{
		return (bool)1;
	}
	// Dead block : IL_00bb: ldloc.1
}
// System.Void MessageSee/<RecvJsonStart>c__Iterator1E::Dispose()
extern "C"  void U3CRecvJsonStartU3Ec__Iterator1E_Dispose_m3342137254 (U3CRecvJsonStartU3Ec__Iterator1E_t2718408706 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void MessageSee/<RecvJsonStart>c__Iterator1E::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CRecvJsonStartU3Ec__Iterator1E_Reset_m577387606_MetadataUsageId;
extern "C"  void U3CRecvJsonStartU3Ec__Iterator1E_Reset_m577387606 (U3CRecvJsonStartU3Ec__Iterator1E_t2718408706 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CRecvJsonStartU3Ec__Iterator1E_Reset_m577387606_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MessageSee/<RecvTakeJson>c__AnonStorey27::.ctor()
extern "C"  void U3CRecvTakeJsonU3Ec__AnonStorey27__ctor_m4045340031 (U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MessageSee/<RecvTakeJson>c__AnonStorey27::<>m__4()
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern Il2CppCodeGenString* _stringLiteral4177331542;
extern const uint32_t U3CRecvTakeJsonU3Ec__AnonStorey27_U3CU3Em__4_m787397582_MetadataUsageId;
extern "C"  void U3CRecvTakeJsonU3Ec__AnonStorey27_U3CU3Em__4_m787397582 (U3CRecvTakeJsonU3Ec__AnonStorey27_t2132693148 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CRecvTakeJsonU3Ec__AnonStorey27_U3CU3Em__4_m787397582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = ((int32_t)3);
		Il2CppObject * L_2 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		int32_t L_4 = ((int32_t)2);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_3, _stringLiteral3660319847, L_6, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_7 = __this->get_Btn_0();
		NullCheck(L_7);
		Button_t3896396478 * L_8 = GameObject_GetComponent_TisButton_t3896396478_m2812094092(L_7, /*hidden argument*/GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var);
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_m3709440845(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		String_t* L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = String_Substring_m2809233063(L_10, 6, /*hidden argument*/NULL);
		int32_t L_12 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		MessageSee_t302730988 * L_13 = __this->get_U3CU3Ef__this_1();
		NullCheck(L_13);
		GameObject_t3674682005 * L_14 = L_13->get_mc_10();
		int32_t L_15 = V_1;
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_14);
		GameObject_SendMessage_m423373689(L_14, _stringLiteral4177331542, L_17, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MessageSee/<TakeJson>c__AnonStorey26::.ctor()
extern "C"  void U3CTakeJsonU3Ec__AnonStorey26__ctor_m3626289446 (U3CTakeJsonU3Ec__AnonStorey26_t291817749 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MessageSee/<TakeJson>c__AnonStorey26::<>m__3()
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern Il2CppCodeGenString* _stringLiteral4177331542;
extern const uint32_t U3CTakeJsonU3Ec__AnonStorey26_U3CU3Em__3_m681730374_MetadataUsageId;
extern "C"  void U3CTakeJsonU3Ec__AnonStorey26_U3CU3Em__3_m681730374 (U3CTakeJsonU3Ec__AnonStorey26_t291817749 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTakeJsonU3Ec__AnonStorey26_U3CU3Em__3_m681730374_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = ((int32_t)3);
		Il2CppObject * L_2 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		int32_t L_4 = ((int32_t)2);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_3, _stringLiteral3660319847, L_6, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_7 = __this->get_Btn_0();
		NullCheck(L_7);
		Button_t3896396478 * L_8 = GameObject_GetComponent_TisButton_t3896396478_m2812094092(L_7, /*hidden argument*/GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var);
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_m3709440845(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		String_t* L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = String_Substring_m2809233063(L_10, 6, /*hidden argument*/NULL);
		int32_t L_12 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		MessageSee_t302730988 * L_13 = __this->get_U3CU3Ef__this_1();
		NullCheck(L_13);
		GameObject_t3674682005 * L_14 = L_13->get_mc_10();
		int32_t L_15 = V_1;
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_14);
		GameObject_SendMessage_m423373689(L_14, _stringLiteral4177331542, L_17, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PrivateURL::.ctor()
extern "C"  void PrivateURL__ctor_m3158841615 (PrivateURL_t117402892 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PrivateURL::get_ID()
extern Il2CppClass* PrivateURL_t117402892_il2cpp_TypeInfo_var;
extern const uint32_t PrivateURL_get_ID_m659887484_MetadataUsageId;
extern "C"  String_t* PrivateURL_get_ID_m659887484 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_get_ID_m659887484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ((PrivateURL_t117402892_StaticFields*)PrivateURL_t117402892_il2cpp_TypeInfo_var->static_fields)->get__id_22();
		return L_0;
	}
}
// System.Void PrivateURL::set_ID(System.String)
extern Il2CppClass* PrivateURL_t117402892_il2cpp_TypeInfo_var;
extern const uint32_t PrivateURL_set_ID_m2762899157_MetadataUsageId;
extern "C"  void PrivateURL_set_ID_m2762899157 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_set_ID_m2762899157_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value0;
		((PrivateURL_t117402892_StaticFields*)PrivateURL_t117402892_il2cpp_TypeInfo_var->static_fields)->set__id_22(L_0);
		return;
	}
}
// System.String PrivateURL::get_PW()
extern Il2CppClass* PrivateURL_t117402892_il2cpp_TypeInfo_var;
extern const uint32_t PrivateURL_get_PW_m660114280_MetadataUsageId;
extern "C"  String_t* PrivateURL_get_PW_m660114280 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_get_PW_m660114280_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ((PrivateURL_t117402892_StaticFields*)PrivateURL_t117402892_il2cpp_TypeInfo_var->static_fields)->get__pw_23();
		return L_0;
	}
}
// System.Void PrivateURL::set_PW(System.String)
extern Il2CppClass* PrivateURL_t117402892_il2cpp_TypeInfo_var;
extern const uint32_t PrivateURL_set_PW_m2535917673_MetadataUsageId;
extern "C"  void PrivateURL_set_PW_m2535917673 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_set_PW_m2535917673_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value0;
		((PrivateURL_t117402892_StaticFields*)PrivateURL_t117402892_il2cpp_TypeInfo_var->static_fields)->set__pw_23(L_0);
		return;
	}
}
// System.String PrivateURL::get_customerId()
extern Il2CppClass* PrivateURL_t117402892_il2cpp_TypeInfo_var;
extern const uint32_t PrivateURL_get_customerId_m4251176570_MetadataUsageId;
extern "C"  String_t* PrivateURL_get_customerId_m4251176570 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_get_customerId_m4251176570_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ((PrivateURL_t117402892_StaticFields*)PrivateURL_t117402892_il2cpp_TypeInfo_var->static_fields)->get__customerId_25();
		return L_0;
	}
}
// System.Void PrivateURL::set_customerId(System.String)
extern Il2CppClass* PrivateURL_t117402892_il2cpp_TypeInfo_var;
extern const uint32_t PrivateURL_set_customerId_m1347654551_MetadataUsageId;
extern "C"  void PrivateURL_set_customerId_m1347654551 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_set_customerId_m1347654551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value0;
		((PrivateURL_t117402892_StaticFields*)PrivateURL_t117402892_il2cpp_TypeInfo_var->static_fields)->set__customerId_25(L_0);
		return;
	}
}
// System.String PrivateURL::get_userloginemail()
extern Il2CppClass* PrivateURL_t117402892_il2cpp_TypeInfo_var;
extern const uint32_t PrivateURL_get_userloginemail_m1497216895_MetadataUsageId;
extern "C"  String_t* PrivateURL_get_userloginemail_m1497216895 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_get_userloginemail_m1497216895_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ((PrivateURL_t117402892_StaticFields*)PrivateURL_t117402892_il2cpp_TypeInfo_var->static_fields)->get__userloginemail_26();
		return L_0;
	}
}
// System.Void PrivateURL::set_userloginemail(System.String)
extern Il2CppClass* PrivateURL_t117402892_il2cpp_TypeInfo_var;
extern const uint32_t PrivateURL_set_userloginemail_m704658226_MetadataUsageId;
extern "C"  void PrivateURL_set_userloginemail_m704658226 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_set_userloginemail_m704658226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value0;
		((PrivateURL_t117402892_StaticFields*)PrivateURL_t117402892_il2cpp_TypeInfo_var->static_fields)->set__userloginemail_26(L_0);
		return;
	}
}
// System.String PrivateURL::get_selectmessageid()
extern Il2CppClass* PrivateURL_t117402892_il2cpp_TypeInfo_var;
extern const uint32_t PrivateURL_get_selectmessageid_m1725662023_MetadataUsageId;
extern "C"  String_t* PrivateURL_get_selectmessageid_m1725662023 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_get_selectmessageid_m1725662023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ((PrivateURL_t117402892_StaticFields*)PrivateURL_t117402892_il2cpp_TypeInfo_var->static_fields)->get__selectmessageid_27();
		return L_0;
	}
}
// System.Void PrivateURL::set_selectmessageid(System.String)
extern Il2CppClass* PrivateURL_t117402892_il2cpp_TypeInfo_var;
extern const uint32_t PrivateURL_set_selectmessageid_m2533335980_MetadataUsageId;
extern "C"  void PrivateURL_set_selectmessageid_m2533335980 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_set_selectmessageid_m2533335980_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value0;
		((PrivateURL_t117402892_StaticFields*)PrivateURL_t117402892_il2cpp_TypeInfo_var->static_fields)->set__selectmessageid_27(L_0);
		return;
	}
}
// System.String PrivateURL::get_Login()
extern Il2CppClass* PrivateURL_t117402892_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1978716728;
extern Il2CppCodeGenString* _stringLiteral2123597107;
extern const uint32_t PrivateURL_get_Login_m301514730_MetadataUsageId;
extern "C"  String_t* PrivateURL_get_Login_m301514730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_get_Login_m301514730_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ((PrivateURL_t117402892_StaticFields*)PrivateURL_t117402892_il2cpp_TypeInfo_var->static_fields)->get__id_22();
		String_t* L_1 = ((PrivateURL_t117402892_StaticFields*)PrivateURL_t117402892_il2cpp_TypeInfo_var->static_fields)->get__pw_23();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral1978716728, L_0, _stringLiteral2123597107, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String PrivateURL::get_Message()
extern Il2CppClass* PrivateURL_t117402892_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral234188291;
extern const uint32_t PrivateURL_get_Message_m2273489160_MetadataUsageId;
extern "C"  String_t* PrivateURL_get_Message_m2273489160 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_get_Message_m2273489160_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ((PrivateURL_t117402892_StaticFields*)PrivateURL_t117402892_il2cpp_TypeInfo_var->static_fields)->get__customerId_25();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral234188291, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String PrivateURL::get_MessageOneDelete()
extern Il2CppClass* PrivateURL_t117402892_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1829665941;
extern const uint32_t PrivateURL_get_MessageOneDelete_m1855499307_MetadataUsageId;
extern "C"  String_t* PrivateURL_get_MessageOneDelete_m1855499307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_get_MessageOneDelete_m1855499307_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ((PrivateURL_t117402892_StaticFields*)PrivateURL_t117402892_il2cpp_TypeInfo_var->static_fields)->get__selectmessageid_27();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1829665941, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String PrivateURL::get_MessageRecv()
extern Il2CppClass* PrivateURL_t117402892_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral112767970;
extern const uint32_t PrivateURL_get_MessageRecv_m5945870_MetadataUsageId;
extern "C"  String_t* PrivateURL_get_MessageRecv_m5945870 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_get_MessageRecv_m5945870_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ((PrivateURL_t117402892_StaticFields*)PrivateURL_t117402892_il2cpp_TypeInfo_var->static_fields)->get__userloginemail_26();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral112767970, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String PrivateURL::get_DeviceMessage()
extern Il2CppCodeGenString* _stringLiteral3201798085;
extern const uint32_t PrivateURL_get_DeviceMessage_m3117622098_MetadataUsageId;
extern "C"  String_t* PrivateURL_get_DeviceMessage_m3117622098 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_get_DeviceMessage_m3117622098_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral3201798085;
	}
}
// System.String PrivateURL::get_RegistrationMessage()
extern Il2CppCodeGenString* _stringLiteral959102673;
extern const uint32_t PrivateURL_get_RegistrationMessage_m4175169615_MetadataUsageId;
extern "C"  String_t* PrivateURL_get_RegistrationMessage_m4175169615 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_get_RegistrationMessage_m4175169615_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral959102673;
	}
}
// System.String PrivateURL::get_MessageCount()
extern Il2CppCodeGenString* _stringLiteral1379830946;
extern const uint32_t PrivateURL_get_MessageCount_m59418249_MetadataUsageId;
extern "C"  String_t* PrivateURL_get_MessageCount_m59418249 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_get_MessageCount_m59418249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral1379830946;
	}
}
// System.String PrivateURL::get_MemberMessageCount()
extern Il2CppClass* PrivateURL_t117402892_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2174723565;
extern const uint32_t PrivateURL_get_MemberMessageCount_m606779331_MetadataUsageId;
extern "C"  String_t* PrivateURL_get_MemberMessageCount_m606779331 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_get_MemberMessageCount_m606779331_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ((PrivateURL_t117402892_StaticFields*)PrivateURL_t117402892_il2cpp_TypeInfo_var->static_fields)->get__customerId_25();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2174723565, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String PrivateURL::get_MemberShipChange()
extern Il2CppCodeGenString* _stringLiteral369130085;
extern const uint32_t PrivateURL_get_MemberShipChange_m2995936519_MetadataUsageId;
extern "C"  String_t* PrivateURL_get_MemberShipChange_m2995936519 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_get_MemberShipChange_m2995936519_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral369130085;
	}
}
// System.String PrivateURL::get_EmailRecvUpdate()
extern Il2CppClass* PrivateURL_t117402892_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1549764803;
extern const uint32_t PrivateURL_get_EmailRecvUpdate_m298749708_MetadataUsageId;
extern "C"  String_t* PrivateURL_get_EmailRecvUpdate_m298749708 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrivateURL_get_EmailRecvUpdate_m298749708_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ((PrivateURL_t117402892_StaticFields*)PrivateURL_t117402892_il2cpp_TypeInfo_var->static_fields)->get__selectmessageid_27();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1549764803, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void ProgressBar.ProgressRadialBehaviour::.ctor()
extern "C"  void ProgressRadialBehaviour__ctor_m4118557090 (ProgressRadialBehaviour_t2198524043 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ProgressBar.ProgressRadialBehaviour::get_Value()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t ProgressRadialBehaviour_get_Value_m3819232596_MetadataUsageId;
extern "C"  float ProgressRadialBehaviour_get_Value_m3819232596 (ProgressRadialBehaviour_t2198524043 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ProgressRadialBehaviour_get_Value_m3819232596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_m_Value_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_1 = bankers_roundf(((float)((float)L_0*(float)(100.0f))));
		return L_1;
	}
}
// System.Void ProgressBar.ProgressRadialBehaviour::set_Value(System.Single)
extern "C"  void ProgressRadialBehaviour_set_Value_m4154902679 (ProgressRadialBehaviour_t2198524043 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		ProgressRadialBehaviour_SetFillerSizeAsPercentage_m2120422124(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ProgressBar.ProgressRadialBehaviour::get_TransitoryValue()
extern "C"  float ProgressRadialBehaviour_get_TransitoryValue_m1525230865 (ProgressRadialBehaviour_t2198524043 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_U3CTransitoryValueU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void ProgressBar.ProgressRadialBehaviour::set_TransitoryValue(System.Single)
extern "C"  void ProgressRadialBehaviour_set_TransitoryValue_m4230546426 (ProgressRadialBehaviour_t2198524043 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CTransitoryValueU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Boolean ProgressBar.ProgressRadialBehaviour::get_isDone()
extern "C"  bool ProgressRadialBehaviour_get_isDone_m3339697635 (ProgressRadialBehaviour_t2198524043 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Value_3();
		return (bool)((((float)L_0) == ((float)(1.0f)))? 1 : 0);
	}
}
// System.Boolean ProgressBar.ProgressRadialBehaviour::get_isPaused()
extern "C"  bool ProgressRadialBehaviour_get_isPaused_m1222803599 (ProgressRadialBehaviour_t2198524043 * __this, const MethodInfo* method)
{
	{
		float L_0 = ProgressRadialBehaviour_get_TransitoryValue_m1525230865(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_m_Value_3();
		return (bool)((((float)L_0) == ((float)L_1))? 1 : 0);
	}
}
// System.Void ProgressBar.ProgressRadialBehaviour::Start()
extern const MethodInfo* Component_GetComponent_TisImage_t538875265_m3706520426_MethodInfo_var;
extern const uint32_t ProgressRadialBehaviour_Start_m3065694882_MetadataUsageId;
extern "C"  void ProgressRadialBehaviour_Start_m3065694882 (ProgressRadialBehaviour_t2198524043 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ProgressRadialBehaviour_Start_m3065694882_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Image_t538875265 * L_0 = Component_GetComponent_TisImage_t538875265_m3706520426(__this, /*hidden argument*/Component_GetComponent_TisImage_t538875265_m3706520426_MethodInfo_var);
		__this->set_m_Fill_2(L_0);
		__this->set_m_Value_3((0.0f));
		ProgressRadialBehaviour_SetFillerSize_m1120266744(__this, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ProgressBar.ProgressRadialBehaviour::Update()
extern "C"  void ProgressRadialBehaviour_Update_m553113003 (ProgressRadialBehaviour_t2198524043 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ProgressRadialBehaviour_get_TransitoryValue_m1525230865(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_m_Value_3();
		if ((((float)L_0) == ((float)L_1)))
		{
			goto IL_0114;
		}
	}
	{
		float L_2 = __this->get_m_Value_3();
		float L_3 = ProgressRadialBehaviour_get_TransitoryValue_m1525230865(__this, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_2-(float)L_3));
		float L_4 = V_0;
		if ((!(((float)L_4) > ((float)(0.0f)))))
		{
			goto IL_0065;
		}
	}
	{
		float L_5 = ProgressRadialBehaviour_get_TransitoryValue_m1525230865(__this, /*hidden argument*/NULL);
		float L_6 = __this->get_ProgressSpeed_5();
		float L_7 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		ProgressRadialBehaviour_set_TransitoryValue_m4230546426(__this, ((float)((float)L_5+(float)((float)((float)L_6*(float)L_7)))), /*hidden argument*/NULL);
		float L_8 = ProgressRadialBehaviour_get_TransitoryValue_m1525230865(__this, /*hidden argument*/NULL);
		float L_9 = __this->get_m_Value_3();
		if ((!(((float)L_8) >= ((float)L_9))))
		{
			goto IL_0060;
		}
	}
	{
		float L_10 = __this->get_m_Value_3();
		ProgressRadialBehaviour_set_TransitoryValue_m4230546426(__this, L_10, /*hidden argument*/NULL);
	}

IL_0060:
	{
		goto IL_00a6;
	}

IL_0065:
	{
		float L_11 = V_0;
		if ((!(((float)L_11) < ((float)(0.0f)))))
		{
			goto IL_00a6;
		}
	}
	{
		float L_12 = ProgressRadialBehaviour_get_TransitoryValue_m1525230865(__this, /*hidden argument*/NULL);
		float L_13 = __this->get_ProgressSpeed_5();
		float L_14 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		ProgressRadialBehaviour_set_TransitoryValue_m4230546426(__this, ((float)((float)L_12-(float)((float)((float)L_13*(float)L_14)))), /*hidden argument*/NULL);
		float L_15 = ProgressRadialBehaviour_get_TransitoryValue_m1525230865(__this, /*hidden argument*/NULL);
		float L_16 = __this->get_m_Value_3();
		if ((!(((float)L_15) <= ((float)L_16))))
		{
			goto IL_00a6;
		}
	}
	{
		float L_17 = __this->get_m_Value_3();
		ProgressRadialBehaviour_set_TransitoryValue_m4230546426(__this, L_17, /*hidden argument*/NULL);
	}

IL_00a6:
	{
		float L_18 = ProgressRadialBehaviour_get_TransitoryValue_m1525230865(__this, /*hidden argument*/NULL);
		if ((!(((float)L_18) >= ((float)(1.0f)))))
		{
			goto IL_00c6;
		}
	}
	{
		ProgressRadialBehaviour_set_TransitoryValue_m4230546426(__this, (1.0f), /*hidden argument*/NULL);
		goto IL_00e1;
	}

IL_00c6:
	{
		float L_19 = ProgressRadialBehaviour_get_TransitoryValue_m1525230865(__this, /*hidden argument*/NULL);
		if ((!(((float)L_19) < ((float)(0.0f)))))
		{
			goto IL_00e1;
		}
	}
	{
		ProgressRadialBehaviour_set_TransitoryValue_m4230546426(__this, (0.0f), /*hidden argument*/NULL);
	}

IL_00e1:
	{
		float L_20 = ProgressRadialBehaviour_get_TransitoryValue_m1525230865(__this, /*hidden argument*/NULL);
		ProgressRadialBehaviour_SetFillerSize_m1120266744(__this, L_20, /*hidden argument*/NULL);
		bool L_21 = __this->get_TriggerOnComplete_6();
		if (!L_21)
		{
			goto IL_0114;
		}
	}
	{
		bool L_22 = ProgressRadialBehaviour_get_isPaused_m1222803599(__this, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0114;
		}
	}
	{
		bool L_23 = ProgressRadialBehaviour_get_isDone_m3339697635(__this, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0114;
		}
	}
	{
		ProgressRadialBehaviour_OnComplete_m3171670234(__this, /*hidden argument*/NULL);
	}

IL_0114:
	{
		return;
	}
}
// System.Void ProgressBar.ProgressRadialBehaviour::SetFillerSize(System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1029;
extern const uint32_t ProgressRadialBehaviour_SetFillerSize_m1120266744_MetadataUsageId;
extern "C"  void ProgressRadialBehaviour_SetFillerSize_m1120266744 (ProgressRadialBehaviour_t2198524043 * __this, float ___fill0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ProgressRadialBehaviour_SetFillerSize_m1120266744_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		TextMeshProUGUI_t3603375195 * L_0 = __this->get_m_AttachedText_4();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		TextMeshProUGUI_t3603375195 * L_2 = __this->get_m_AttachedText_4();
		float L_3 = ___fill0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_3*(float)(100.0f))), /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = Int32_ToString_m1286526384((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m138640077(NULL /*static, unused*/, L_5, _stringLiteral1029, /*hidden argument*/NULL);
		NullCheck(L_2);
		TMP_Text_set_text_m27929696(L_2, L_6, /*hidden argument*/NULL);
	}

IL_0039:
	{
		Image_t538875265 * L_7 = __this->get_m_Fill_2();
		float L_8 = ___fill0;
		NullCheck(L_7);
		Image_set_fillAmount_m1583793743(L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ProgressBar.ProgressRadialBehaviour::SetFillerSizeAsPercentage(System.Single)
extern "C"  void ProgressRadialBehaviour_SetFillerSizeAsPercentage_m2120422124 (ProgressRadialBehaviour_t2198524043 * __this, float ___Percent0, const MethodInfo* method)
{
	{
		float L_0 = ___Percent0;
		__this->set_m_Value_3(((float)((float)L_0/(float)(100.0f))));
		float L_1 = __this->get_m_Value_3();
		if ((!(((float)L_1) < ((float)(0.0f)))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_m_Value_3((0.0f));
		goto IL_0048;
	}

IL_002d:
	{
		float L_2 = __this->get_m_Value_3();
		if ((!(((float)L_2) > ((float)(1.0f)))))
		{
			goto IL_0048;
		}
	}
	{
		__this->set_m_Value_3((1.0f));
	}

IL_0048:
	{
		return;
	}
}
// System.Void ProgressBar.ProgressRadialBehaviour::OnComplete()
extern "C"  void ProgressRadialBehaviour_OnComplete_m3171670234 (ProgressRadialBehaviour_t2198524043 * __this, const MethodInfo* method)
{
	{
		OnCompleteEvent_t4208438896 * L_0 = __this->get_OnCompleteMethods_7();
		NullCheck(L_0);
		UnityEvent_Invoke_m2672830205(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ProgressBar.ProgressRadialBehaviour::IncrementValue(System.Single)
extern "C"  void ProgressRadialBehaviour_IncrementValue_m1811676999 (ProgressRadialBehaviour_t2198524043 * __this, float ___inc0, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Value_3();
		float L_1 = ___inc0;
		__this->set_m_Value_3(((float)((float)L_0+(float)((float)((float)L_1/(float)(100.0f))))));
		float L_2 = __this->get_m_Value_3();
		if ((!(((float)L_2) > ((float)(1.0f)))))
		{
			goto IL_002f;
		}
	}
	{
		__this->set_m_Value_3((1.0f));
	}

IL_002f:
	{
		return;
	}
}
// System.Void ProgressBar.ProgressRadialBehaviour::DecrementValue(System.Single)
extern "C"  void ProgressRadialBehaviour_DecrementValue_m638797419 (ProgressRadialBehaviour_t2198524043 * __this, float ___dec0, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Value_3();
		float L_1 = ___dec0;
		__this->set_m_Value_3(((float)((float)L_0-(float)((float)((float)L_1/(float)(100.0f))))));
		float L_2 = __this->get_m_Value_3();
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_002f;
		}
	}
	{
		__this->set_m_Value_3((0.0f));
	}

IL_002f:
	{
		return;
	}
}
// System.Void ProgressBar.Utils.FillerProperty::.ctor(System.Single,System.Single)
extern "C"  void FillerProperty__ctor_m955200959 (FillerProperty_t3769378557 * __this, float ___Min0, float ___Max1, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		float L_0 = ___Min0;
		__this->set_MinWidth_1(L_0);
		float L_1 = ___Max1;
		__this->set_MaxWidth_0(L_1);
		return;
	}
}
// System.Void ProgressBar.Utils.OnCompleteEvent::.ctor()
extern "C"  void OnCompleteEvent__ctor_m3920728494 (OnCompleteEvent_t4208438896 * __this, const MethodInfo* method)
{
	{
		UnityEvent__ctor_m1715209183(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ProgressBar.Utils.ProgressValue::.ctor(System.Single,System.Single)
extern "C"  void ProgressValue__ctor_m3142496420 (ProgressValue_t1899486002 * __this, float ___value0, float ___MaxValue1, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		float L_0 = ___value0;
		__this->set_m_Value_0(L_0);
		float L_1 = ___MaxValue1;
		__this->set_m_MaxValue_1(L_1);
		return;
	}
}
// System.Void ProgressBar.Utils.ProgressValue::Set(System.Single)
extern "C"  void ProgressValue_Set_m692521151 (ProgressValue_t1899486002 * __this, float ___newValue0, const MethodInfo* method)
{
	{
		float L_0 = ___newValue0;
		__this->set_m_Value_0(L_0);
		return;
	}
}
// System.Single ProgressBar.Utils.ProgressValue::get_AsFloat()
extern "C"  float ProgressValue_get_AsFloat_m4073483383 (ProgressValue_t1899486002 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Value_0();
		return L_0;
	}
}
// System.Int32 ProgressBar.Utils.ProgressValue::get_AsInt()
extern "C"  int32_t ProgressValue_get_AsInt_m2535567884 (ProgressValue_t1899486002 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Value_0();
		return (((int32_t)((int32_t)L_0)));
	}
}
// System.Single ProgressBar.Utils.ProgressValue::get_Normalized()
extern "C"  float ProgressValue_get_Normalized_m3855071756 (ProgressValue_t1899486002 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Value_0();
		float L_1 = __this->get_m_MaxValue_1();
		return ((float)((float)L_0/(float)L_1));
	}
}
// System.Single ProgressBar.Utils.ProgressValue::get_PercentAsFloat()
extern "C"  float ProgressValue_get_PercentAsFloat_m2405012186 (ProgressValue_t1899486002 * __this, const MethodInfo* method)
{
	{
		float L_0 = ProgressValue_get_Normalized_m3855071756(__this, /*hidden argument*/NULL);
		return ((float)((float)L_0*(float)(100.0f)));
	}
}
// System.Single ProgressBar.Utils.ProgressValue::get_PercentAsInt()
extern "C"  float ProgressValue_get_PercentAsInt_m4094716653 (ProgressValue_t1899486002 * __this, const MethodInfo* method)
{
	{
		float L_0 = ProgressValue_get_PercentAsFloat_m2405012186(__this, /*hidden argument*/NULL);
		return (((float)((float)(((int32_t)((int32_t)L_0))))));
	}
}
// System.Void PublicSpotMessage::.ctor(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PublicSpotMessage__ctor_m1772971704 (PublicSpotMessage_t3890192988 * __this, int32_t ___messageindex0, String_t* ___message_id1, String_t* ___type2, String_t* ___title3, String_t* ___realfilename4, String_t* ___pref5, String_t* ___addr016, String_t* ___addr027, String_t* ___latitude8, String_t* ___longtitude9, String_t* ___create_data10, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___messageindex0;
		__this->set_messageindex_10(L_0);
		String_t* L_1 = ___message_id1;
		__this->set_message_id_0(L_1);
		String_t* L_2 = ___type2;
		__this->set_type_1(L_2);
		String_t* L_3 = ___title3;
		__this->set_title_2(L_3);
		String_t* L_4 = ___realfilename4;
		__this->set_realfilename_3(L_4);
		String_t* L_5 = ___pref5;
		__this->set_pref_4(L_5);
		String_t* L_6 = ___addr016;
		__this->set_addr01_5(L_6);
		String_t* L_7 = ___addr027;
		__this->set_addr02_6(L_7);
		String_t* L_8 = ___latitude8;
		__this->set_latitude_7(L_8);
		String_t* L_9 = ___longtitude9;
		__this->set_longtitude_8(L_9);
		String_t* L_10 = ___create_data10;
		__this->set_create_data_9(L_10);
		return;
	}
}
// System.Void PublicSpotMessageDB::.ctor()
extern Il2CppClass* List_1_t963411244_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2228207227_MethodInfo_var;
extern const uint32_t PublicSpotMessageDB__ctor_m520832209_MetadataUsageId;
extern "C"  void PublicSpotMessageDB__ctor_m520832209 (PublicSpotMessageDB_t1853916122 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PublicSpotMessageDB__ctor_m520832209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t963411244 * L_0 = (List_1_t963411244 *)il2cpp_codegen_object_new(List_1_t963411244_il2cpp_TypeInfo_var);
		List_1__ctor_m2228207227(L_0, /*hidden argument*/List_1__ctor_m2228207227_MethodInfo_var);
		__this->set_m_PSMDB_3(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PublicSpotMessageDB::.cctor()
extern "C"  void PublicSpotMessageDB__cctor_m2778800380 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// PublicSpotMessageDB PublicSpotMessageDB::get_Instance()
extern Il2CppClass* PublicSpotMessageDB_t1853916122_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral916723649;
extern const uint32_t PublicSpotMessageDB_get_Instance_m2156621208_MetadataUsageId;
extern "C"  PublicSpotMessageDB_t1853916122 * PublicSpotMessageDB_get_Instance_m2156621208 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PublicSpotMessageDB_get_Instance_m2156621208_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PublicSpotMessageDB_t1853916122_il2cpp_TypeInfo_var);
		PublicSpotMessageDB_t1853916122 * L_0 = ((PublicSpotMessageDB_t1853916122_StaticFields*)PublicSpotMessageDB_t1853916122_il2cpp_TypeInfo_var->static_fields)->get__instance_2();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral916723649, /*hidden argument*/NULL);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PublicSpotMessageDB_t1853916122_il2cpp_TypeInfo_var);
		PublicSpotMessageDB_t1853916122 * L_2 = ((PublicSpotMessageDB_t1853916122_StaticFields*)PublicSpotMessageDB_t1853916122_il2cpp_TypeInfo_var->static_fields)->get__instance_2();
		return L_2;
	}
}
// System.Void PublicSpotMessageDB::Awake()
extern Il2CppClass* PublicSpotMessageDB_t1853916122_il2cpp_TypeInfo_var;
extern const uint32_t PublicSpotMessageDB_Awake_m758437428_MetadataUsageId;
extern "C"  void PublicSpotMessageDB_Awake_m758437428 (PublicSpotMessageDB_t1853916122 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PublicSpotMessageDB_Awake_m758437428_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PublicSpotMessageDB_t1853916122_il2cpp_TypeInfo_var);
		((PublicSpotMessageDB_t1853916122_StaticFields*)PublicSpotMessageDB_t1853916122_il2cpp_TypeInfo_var->static_fields)->set__instance_2(__this);
		return;
	}
}
// System.Void PublicSpotMessageDB::AddPSMDB(PublicSpotMessage)
extern const MethodInfo* List_1_Add_m1922568043_MethodInfo_var;
extern const uint32_t PublicSpotMessageDB_AddPSMDB_m1227598172_MetadataUsageId;
extern "C"  void PublicSpotMessageDB_AddPSMDB_m1227598172 (PublicSpotMessageDB_t1853916122 * __this, PublicSpotMessage_t3890192988 * ___messageid0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PublicSpotMessageDB_AddPSMDB_m1227598172_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t963411244 * L_0 = __this->get_m_PSMDB_3();
		PublicSpotMessage_t3890192988 * L_1 = ___messageid0;
		NullCheck(L_0);
		List_1_Add_m1922568043(L_0, L_1, /*hidden argument*/List_1_Add_m1922568043_MethodInfo_var);
		return;
	}
}
// PublicSpotMessage PublicSpotMessageDB::MessagesByIndex(System.Int32)
extern const MethodInfo* List_1_get_Item_m1179376068_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2973917361_MethodInfo_var;
extern const uint32_t PublicSpotMessageDB_MessagesByIndex_m3257673094_MetadataUsageId;
extern "C"  PublicSpotMessage_t3890192988 * PublicSpotMessageDB_MessagesByIndex_m3257673094 (PublicSpotMessageDB_t1853916122 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PublicSpotMessageDB_MessagesByIndex_m3257673094_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_002f;
	}

IL_0007:
	{
		List_1_t963411244 * L_0 = __this->get_m_PSMDB_3();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PublicSpotMessage_t3890192988 * L_2 = List_1_get_Item_m1179376068(L_0, L_1, /*hidden argument*/List_1_get_Item_m1179376068_MethodInfo_var);
		NullCheck(L_2);
		int32_t L_3 = L_2->get_messageindex_10();
		int32_t L_4 = ___index0;
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_002b;
		}
	}
	{
		List_1_t963411244 * L_5 = __this->get_m_PSMDB_3();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		PublicSpotMessage_t3890192988 * L_7 = List_1_get_Item_m1179376068(L_5, L_6, /*hidden argument*/List_1_get_Item_m1179376068_MethodInfo_var);
		return L_7;
	}

IL_002b:
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_9 = V_0;
		List_1_t963411244 * L_10 = __this->get_m_PSMDB_3();
		NullCheck(L_10);
		int32_t L_11 = List_1_get_Count_m2973917361(L_10, /*hidden argument*/List_1_get_Count_m2973917361_MethodInfo_var);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_0007;
		}
	}
	{
		return (PublicSpotMessage_t3890192988 *)NULL;
	}
}
// System.Void PublicSpotMessageDB::AllDeletePSMDB()
extern const MethodInfo* List_1_get_Count_m2973917361_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m3929307814_MethodInfo_var;
extern const uint32_t PublicSpotMessageDB_AllDeletePSMDB_m95139407_MetadataUsageId;
extern "C"  void PublicSpotMessageDB_AllDeletePSMDB_m95139407 (PublicSpotMessageDB_t1853916122 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PublicSpotMessageDB_AllDeletePSMDB_m95139407_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t963411244 * L_0 = __this->get_m_PSMDB_3();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m2973917361(L_0, /*hidden argument*/List_1_get_Count_m2973917361_MethodInfo_var);
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		List_1_t963411244 * L_2 = __this->get_m_PSMDB_3();
		NullCheck(L_2);
		List_1_Clear_m3929307814(L_2, /*hidden argument*/List_1_Clear_m3929307814_MethodInfo_var);
		return;
	}
}
// System.Void SaveInput::.ctor()
extern "C"  void SaveInput__ctor_m899828702 (SaveInput_t2167614509 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SaveInput::SaveText()
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern const uint32_t SaveInput_SaveText_m650321136_MetadataUsageId;
extern "C"  void SaveInput_SaveText_m650321136 (SaveInput_t2167614509 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SaveInput_SaveText_m650321136_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InputField_t609046876 * L_0 = __this->get_inputField_3();
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m3972300732(L_0, /*hidden argument*/NULL);
		bool L_2 = SaveInput_isContainother_m1480183402(__this, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_3 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)((int32_t)20))))
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_4 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)18)))))
		{
			goto IL_003b;
		}
	}

IL_002e:
	{
		InputField_t609046876 * L_5 = __this->get_inputField_3();
		NullCheck(L_5);
		InputField_set_characterLimit_m1004148585(L_5, ((int32_t)15), /*hidden argument*/NULL);
	}

IL_003b:
	{
		goto IL_0065;
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_6 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)((int32_t)20))))
		{
			goto IL_0058;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_7 = CurrentState_get_State_m3296155471(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)18)))))
		{
			goto IL_0065;
		}
	}

IL_0058:
	{
		InputField_t609046876 * L_8 = __this->get_inputField_3();
		NullCheck(L_8);
		InputField_set_characterLimit_m1004148585(L_8, ((int32_t)30), /*hidden argument*/NULL);
	}

IL_0065:
	{
		InputField_t609046876 * L_9 = __this->get_inputField_3();
		NullCheck(L_9);
		String_t* L_10 = InputField_get_text_m3972300732(L_9, /*hidden argument*/NULL);
		__this->set_str_2(L_10);
		Text_t9039225 * L_11 = __this->get_text_4();
		String_t* L_12 = __this->get_str_2();
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, L_12);
		return;
	}
}
// System.Boolean SaveInput::isContainother(System.String)
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern const uint32_t SaveInput_isContainother_m1480183402_MetadataUsageId;
extern "C"  bool SaveInput_isContainother_m1480183402 (SaveInput_t2167614509 * __this, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SaveInput_isContainother_m1480183402_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CharU5BU5D_t3324145743* V_0 = NULL;
	Il2CppChar V_1 = 0x0;
	CharU5BU5D_t3324145743* V_2 = NULL;
	int32_t V_3 = 0;
	{
		String_t* L_0 = ___s0;
		NullCheck(L_0);
		CharU5BU5D_t3324145743* L_1 = String_ToCharArray_m1208288742(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		CharU5BU5D_t3324145743* L_2 = V_0;
		V_2 = L_2;
		V_3 = 0;
		goto IL_0026;
	}

IL_0010:
	{
		CharU5BU5D_t3324145743* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		uint16_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = L_6;
		Il2CppChar L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		int32_t L_8 = Char_GetUnicodeCategory_m494586991(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)4))))
		{
			goto IL_0022;
		}
	}
	{
		return (bool)1;
	}

IL_0022:
	{
		int32_t L_9 = V_3;
		V_3 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0026:
	{
		int32_t L_10 = V_3;
		CharU5BU5D_t3324145743* L_11 = V_2;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void ScreenDebug::.ctor()
extern "C"  void ScreenDebug__ctor_m1292232228 (ScreenDebug_t1599402279 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScreenDebug::OnGUI()
extern Il2CppClass* ScreenDebug_t1599402279_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* GPSPopupMessages_t960090149_il2cpp_TypeInfo_var;
extern const uint32_t ScreenDebug_OnGUI_m787630878_MetadataUsageId;
extern "C"  void ScreenDebug_OnGUI_m787630878 (ScreenDebug_t1599402279 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScreenDebug_OnGUI_m787630878_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Resolution_t1578306928  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		float L_0 = Screen_get_dpi_m3780069159(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0025;
		}
	}
	{
		float L_1 = Screen_get_dpi_m3780069159(NULL /*static, unused*/, /*hidden argument*/NULL);
		((ScreenDebug_t1599402279_StaticFields*)ScreenDebug_t1599402279_il2cpp_TypeInfo_var->static_fields)->set_SCREEN_DENSITY_2((((int32_t)((int32_t)((float)((float)L_1/(float)(160.0f)))))));
		goto IL_003d;
	}

IL_0025:
	{
		Resolution_t1578306928  L_2 = Screen_get_currentResolution_m2532370351(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_2;
		int32_t L_3 = Resolution_get_height_m2314404467((&V_3), /*hidden argument*/NULL);
		((ScreenDebug_t1599402279_StaticFields*)ScreenDebug_t1599402279_il2cpp_TypeInfo_var->static_fields)->set_SCREEN_DENSITY_2(((int32_t)((int32_t)L_3/(int32_t)((int32_t)600))));
	}

IL_003d:
	{
		int32_t L_4 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect__ctor_m3291325233((&V_0), (40.0f), (20.0f), (((float)((float)((int32_t)((int32_t)L_4-(int32_t)((int32_t)80)))))), (((float)((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)40)))))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUISkin_t3371348110 * L_6 = GUI_get_skin_m4001454842(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIStyle_t2990928826 * L_7 = GUISkin_get_box_m3284280323(L_6, /*hidden argument*/NULL);
		int32_t L_8 = ((ScreenDebug_t1599402279_StaticFields*)ScreenDebug_t1599402279_il2cpp_TypeInfo_var->static_fields)->get_SCREEN_DENSITY_2();
		NullCheck(L_7);
		GUIStyle_set_fontSize_m3621764235(L_7, ((int32_t)((int32_t)((int32_t)32)*(int32_t)L_8)), /*hidden argument*/NULL);
		GUISkin_t3371348110 * L_9 = GUI_get_skin_m4001454842(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		GUIStyle_t2990928826 * L_10 = GUISkin_get_box_m3284280323(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		GUIStyleState_t1997423985 * L_11 = GUIStyle_get_normal_m42729964(L_10, /*hidden argument*/NULL);
		Color_t4194546905  L_12 = Color_get_red_m4288945411(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		GUIStyleState_set_textColor_m3058467057(L_11, L_12, /*hidden argument*/NULL);
		float L_13 = Rect_get_height_m2154960823((&V_0), /*hidden argument*/NULL);
		V_1 = ((float)((float)L_13/(float)(7.0f)));
		float L_14 = Rect_get_x_m982385354((&V_0), /*hidden argument*/NULL);
		float L_15 = Rect_get_y_m982386315((&V_0), /*hidden argument*/NULL);
		float L_16 = Rect_get_height_m2154960823((&V_0), /*hidden argument*/NULL);
		float L_17 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		float L_18 = V_1;
		Rect__ctor_m3291325233((&V_2), ((float)((float)L_14+(float)(40.0f))), ((float)((float)L_15+(float)((float)((float)L_16/(float)(2.0f))))), ((float)((float)L_17-(float)(80.0f))), ((float)((float)L_18*(float)(2.0f))), /*hidden argument*/NULL);
		GUISkin_t3371348110 * L_19 = GUI_get_skin_m4001454842(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		GUIStyle_t2990928826 * L_20 = GUISkin_get_label_m3553254700(L_19, /*hidden argument*/NULL);
		int32_t L_21 = ((ScreenDebug_t1599402279_StaticFields*)ScreenDebug_t1599402279_il2cpp_TypeInfo_var->static_fields)->get_SCREEN_DENSITY_2();
		NullCheck(L_20);
		GUIStyle_set_fontSize_m3621764235(L_20, ((int32_t)((int32_t)((int32_t)40)*(int32_t)L_21)), /*hidden argument*/NULL);
		GUISkin_t3371348110 * L_22 = GUI_get_skin_m4001454842(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_22);
		GUIStyle_t2990928826 * L_23 = GUISkin_get_label_m3553254700(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		GUIStyle_set_alignment_m4252900834(L_23, 7, /*hidden argument*/NULL);
		GUISkin_t3371348110 * L_24 = GUI_get_skin_m4001454842(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_24);
		GUIStyle_t2990928826 * L_25 = GUISkin_get_label_m3553254700(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		GUIStyleState_t1997423985 * L_26 = GUIStyle_get_normal_m42729964(L_25, /*hidden argument*/NULL);
		Color_t4194546905  L_27 = Color_get_red_m4288945411(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_26);
		GUIStyleState_set_textColor_m3058467057(L_26, L_27, /*hidden argument*/NULL);
		Rect_t4241904616  L_28 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		int32_t L_29 = CurrentState_get_GPSPopupMessage_m652265359(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_30 = L_29;
		Il2CppObject * L_31 = Box(GPSPopupMessages_t960090149_il2cpp_TypeInfo_var, &L_30);
		NullCheck((Enum_t2862688501 *)L_31);
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_31);
		GUI_Label_m1483857617(NULL /*static, unused*/, L_28, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpotMessageGet::.ctor()
extern "C"  void SpotMessageGet__ctor_m1288590122 (SpotMessageGet_t1944980625 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpotMessageGet::OnEnable()
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern const uint32_t SpotMessageGet_OnEnable_m2042211356_MetadataUsageId;
extern "C"  void SpotMessageGet_OnEnable_m2042211356 (SpotMessageGet_t1944980625 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpotMessageGet_OnEnable_m2042211356_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = ((int32_t)3);
		Il2CppObject * L_2 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		int32_t L_4 = ((int32_t)2);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_3, _stringLiteral3660319847, L_6, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		Il2CppObject * L_7 = SpotMessageGet_MessageGetJsonStart_m827736907(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator SpotMessageGet::MessageGetJsonStart()
extern Il2CppClass* U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382_il2cpp_TypeInfo_var;
extern const uint32_t SpotMessageGet_MessageGetJsonStart_m827736907_MetadataUsageId;
extern "C"  Il2CppObject * SpotMessageGet_MessageGetJsonStart_m827736907 (SpotMessageGet_t1944980625 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpotMessageGet_MessageGetJsonStart_m827736907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382 * V_0 = NULL;
	{
		U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382 * L_0 = (U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382 *)il2cpp_codegen_object_new(U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382_il2cpp_TypeInfo_var);
		U3CMessageGetJsonStartU3Ec__Iterator20__ctor_m1229495853(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator SpotMessageGet::GreatManCheckProgress(UnityEngine.WWW)
extern Il2CppClass* U3CGreatManCheckProgressU3Ec__Iterator21_t200358716_il2cpp_TypeInfo_var;
extern const uint32_t SpotMessageGet_GreatManCheckProgress_m3593636778_MetadataUsageId;
extern "C"  Il2CppObject * SpotMessageGet_GreatManCheckProgress_m3593636778 (SpotMessageGet_t1944980625 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpotMessageGet_GreatManCheckProgress_m3593636778_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 * V_0 = NULL;
	{
		U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 * L_0 = (U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 *)il2cpp_codegen_object_new(U3CGreatManCheckProgressU3Ec__Iterator21_t200358716_il2cpp_TypeInfo_var);
		U3CGreatManCheckProgressU3Ec__Iterator21__ctor_m1852840879(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 * L_1 = V_0;
		WWW_t3134621005 * L_2 = ___www0;
		NullCheck(L_1);
		L_1->set_www_0(L_2);
		U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 * L_3 = V_0;
		WWW_t3134621005 * L_4 = ___www0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Ewww_3(L_4);
		U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 * L_5 = V_0;
		return L_5;
	}
}
// System.Void SpotMessageGet::MessageGetTakeJson(System.String)
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019_il2cpp_TypeInfo_var;
extern Il2CppClass* PublicSpotMessageDB_t1853916122_il2cpp_TypeInfo_var;
extern Il2CppClass* PublicSpotMessage_t3890192988_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_t594794173_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponentInChildren_TisText_t9039225_m2924853261_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var;
extern const MethodInfo* U3CMessageGetTakeJsonU3Ec__AnonStorey28_U3CU3Em__5_m3275208734_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3897517420;
extern Il2CppCodeGenString* _stringLiteral2760346223;
extern Il2CppCodeGenString* _stringLiteral3738031314;
extern Il2CppCodeGenString* _stringLiteral3680721943;
extern Il2CppCodeGenString* _stringLiteral2289459;
extern Il2CppCodeGenString* _stringLiteral2604245075;
extern Il2CppCodeGenString* _stringLiteral3575610;
extern Il2CppCodeGenString* _stringLiteral110371416;
extern Il2CppCodeGenString* _stringLiteral473771429;
extern Il2CppCodeGenString* _stringLiteral3449379;
extern Il2CppCodeGenString* _stringLiteral2872469938;
extern Il2CppCodeGenString* _stringLiteral2872469939;
extern Il2CppCodeGenString* _stringLiteral2854988908;
extern Il2CppCodeGenString* _stringLiteral137365935;
extern Il2CppCodeGenString* _stringLiteral3800909073;
extern const uint32_t SpotMessageGet_MessageGetTakeJson_m763779946_MetadataUsageId;
extern "C"  void SpotMessageGet_MessageGetTakeJson_m763779946 (SpotMessageGet_t1944980625 * __this, String_t* ___js0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpotMessageGet_MessageGetTakeJson_m763779946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JsonData_t1715015430 * V_0 = NULL;
	int32_t V_1 = 0;
	GameObject_t3674682005 * V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019 * V_13 = NULL;
	{
		String_t* L_0 = ___js0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		JsonData_t1715015430 * L_1 = JsonMapper_ToObject_m3425498490(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JsonData_t1715015430 * L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		JsonData_t1715015430 * L_3 = V_0;
		NullCheck(L_3);
		JsonData_t1715015430 * L_4 = JsonData_get_Item_m4009629743(L_3, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_4);
		JsonData_t1715015430 * L_5 = JsonData_get_Item_m4009629743(L_4, _stringLiteral2760346223, /*hidden argument*/NULL);
		NullCheck(L_5);
		JsonData_t1715015430 * L_6 = JsonData_get_Item_m4009629743(L_5, _stringLiteral3738031314, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t476798718_il2cpp_TypeInfo_var);
		bool L_8 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_055c;
		}
	}
	{
		V_1 = 0;
		goto IL_053c;
	}

IL_0043:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3680721943, L_11, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_13 = GameObject_Find_m332785498(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		GameObject_t3674682005 * L_14 = V_2;
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0538;
		}
	}
	{
		U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019 * L_16 = (U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019 *)il2cpp_codegen_object_new(U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019_il2cpp_TypeInfo_var);
		U3CMessageGetTakeJsonU3Ec__AnonStorey28__ctor_m3432855312(L_16, /*hidden argument*/NULL);
		V_13 = L_16;
		U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019 * L_17 = V_13;
		GameObject_t3674682005 * L_18 = __this->get_m_Button_2();
		GameObject_t3674682005 * L_19 = Object_Instantiate_TisGameObject_t3674682005_m3917608929(NULL /*static, unused*/, L_18, /*hidden argument*/Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var);
		NullCheck(L_17);
		L_17->set_Btn_0(L_19);
		U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019 * L_20 = V_13;
		NullCheck(L_20);
		GameObject_t3674682005 * L_21 = L_20->get_Btn_0();
		int32_t L_22 = V_1;
		int32_t L_23 = L_22;
		Il2CppObject * L_24 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_23);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3680721943, L_24, /*hidden argument*/NULL);
		NullCheck(L_21);
		Object_set_name_m1123518500(L_21, L_25, /*hidden argument*/NULL);
		U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019 * L_26 = V_13;
		NullCheck(L_26);
		GameObject_t3674682005 * L_27 = L_26->get_Btn_0();
		NullCheck(L_27);
		Transform_t1659122786 * L_28 = GameObject_get_transform_m1278640159(L_27, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_29 = __this->get_m_Parent_3();
		NullCheck(L_29);
		Transform_t1659122786 * L_30 = GameObject_get_transform_m1278640159(L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_SetParent_m3449663462(L_28, L_30, /*hidden argument*/NULL);
		U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019 * L_31 = V_13;
		NullCheck(L_31);
		GameObject_t3674682005 * L_32 = L_31->get_Btn_0();
		NullCheck(L_32);
		Transform_t1659122786 * L_33 = GameObject_get_transform_m1278640159(L_32, /*hidden argument*/NULL);
		int32_t L_34 = V_1;
		Vector3_t4282066566  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Vector3__ctor_m2926210380(&L_35, (0.0f), ((float)((float)(-110.5f)+(float)(((float)((float)((int32_t)((int32_t)((int32_t)-540)*(int32_t)L_34))))))), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_33);
		Transform_set_localPosition_m3504330903(L_33, L_35, /*hidden argument*/NULL);
		U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019 * L_36 = V_13;
		NullCheck(L_36);
		GameObject_t3674682005 * L_37 = L_36->get_Btn_0();
		NullCheck(L_37);
		Transform_t1659122786 * L_38 = GameObject_get_transform_m1278640159(L_37, /*hidden argument*/NULL);
		Vector3_t4282066566  L_39;
		memset(&L_39, 0, sizeof(L_39));
		Vector3__ctor_m2926210380(&L_39, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_38);
		Transform_set_localScale_m310756934(L_38, L_39, /*hidden argument*/NULL);
		U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019 * L_40 = V_13;
		NullCheck(L_40);
		GameObject_t3674682005 * L_41 = L_40->get_Btn_0();
		NullCheck(L_41);
		RectTransform_t972643934 * L_42 = GameObject_GetComponent_TisRectTransform_t972643934_m406276429(L_41, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var);
		Vector2_t4282066565  L_43 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_42);
		RectTransform_set_offsetMax_m677885815(L_42, L_43, /*hidden argument*/NULL);
		U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019 * L_44 = V_13;
		NullCheck(L_44);
		GameObject_t3674682005 * L_45 = L_44->get_Btn_0();
		NullCheck(L_45);
		RectTransform_t972643934 * L_46 = GameObject_GetComponent_TisRectTransform_t972643934_m406276429(L_45, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var);
		Vector2_t4282066565  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Vector2__ctor_m1517109030(&L_47, (0.0f), (221.0f), /*hidden argument*/NULL);
		NullCheck(L_46);
		RectTransform_set_sizeDelta_m1223846609(L_46, L_47, /*hidden argument*/NULL);
		String_t* L_48 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_3 = L_48;
		String_t* L_49 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_4 = L_49;
		String_t* L_50 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_5 = L_50;
		String_t* L_51 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_6 = L_51;
		String_t* L_52 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_7 = L_52;
		String_t* L_53 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_8 = L_53;
		String_t* L_54 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_9 = L_54;
		String_t* L_55 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_10 = L_55;
		String_t* L_56 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_11 = L_56;
		String_t* L_57 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_12 = L_57;
		JsonData_t1715015430 * L_58 = V_0;
		NullCheck(L_58);
		JsonData_t1715015430 * L_59 = JsonData_get_Item_m4009629743(L_58, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_59);
		JsonData_t1715015430 * L_60 = JsonData_get_Item_m4009629743(L_59, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_61 = V_1;
		NullCheck(L_60);
		JsonData_t1715015430 * L_62 = JsonData_get_Item_m253158020(L_60, L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		JsonData_t1715015430 * L_63 = JsonData_get_Item_m4009629743(L_62, _stringLiteral2604245075, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_01d9;
		}
	}
	{
		JsonData_t1715015430 * L_64 = V_0;
		NullCheck(L_64);
		JsonData_t1715015430 * L_65 = JsonData_get_Item_m4009629743(L_64, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_65);
		JsonData_t1715015430 * L_66 = JsonData_get_Item_m4009629743(L_65, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_67 = V_1;
		NullCheck(L_66);
		JsonData_t1715015430 * L_68 = JsonData_get_Item_m253158020(L_66, L_67, /*hidden argument*/NULL);
		NullCheck(L_68);
		JsonData_t1715015430 * L_69 = JsonData_get_Item_m4009629743(L_68, _stringLiteral2604245075, /*hidden argument*/NULL);
		NullCheck(L_69);
		String_t* L_70 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_69);
		V_3 = L_70;
	}

IL_01d9:
	{
		JsonData_t1715015430 * L_71 = V_0;
		NullCheck(L_71);
		JsonData_t1715015430 * L_72 = JsonData_get_Item_m4009629743(L_71, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_72);
		JsonData_t1715015430 * L_73 = JsonData_get_Item_m4009629743(L_72, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_74 = V_1;
		NullCheck(L_73);
		JsonData_t1715015430 * L_75 = JsonData_get_Item_m253158020(L_73, L_74, /*hidden argument*/NULL);
		NullCheck(L_75);
		JsonData_t1715015430 * L_76 = JsonData_get_Item_m4009629743(L_75, _stringLiteral3575610, /*hidden argument*/NULL);
		if (!L_76)
		{
			goto IL_022f;
		}
	}
	{
		JsonData_t1715015430 * L_77 = V_0;
		NullCheck(L_77);
		JsonData_t1715015430 * L_78 = JsonData_get_Item_m4009629743(L_77, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_78);
		JsonData_t1715015430 * L_79 = JsonData_get_Item_m4009629743(L_78, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_80 = V_1;
		NullCheck(L_79);
		JsonData_t1715015430 * L_81 = JsonData_get_Item_m253158020(L_79, L_80, /*hidden argument*/NULL);
		NullCheck(L_81);
		JsonData_t1715015430 * L_82 = JsonData_get_Item_m4009629743(L_81, _stringLiteral3575610, /*hidden argument*/NULL);
		NullCheck(L_82);
		String_t* L_83 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_82);
		V_4 = L_83;
	}

IL_022f:
	{
		JsonData_t1715015430 * L_84 = V_0;
		NullCheck(L_84);
		JsonData_t1715015430 * L_85 = JsonData_get_Item_m4009629743(L_84, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_85);
		JsonData_t1715015430 * L_86 = JsonData_get_Item_m4009629743(L_85, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_87 = V_1;
		NullCheck(L_86);
		JsonData_t1715015430 * L_88 = JsonData_get_Item_m253158020(L_86, L_87, /*hidden argument*/NULL);
		NullCheck(L_88);
		JsonData_t1715015430 * L_89 = JsonData_get_Item_m4009629743(L_88, _stringLiteral110371416, /*hidden argument*/NULL);
		if (!L_89)
		{
			goto IL_0285;
		}
	}
	{
		JsonData_t1715015430 * L_90 = V_0;
		NullCheck(L_90);
		JsonData_t1715015430 * L_91 = JsonData_get_Item_m4009629743(L_90, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_91);
		JsonData_t1715015430 * L_92 = JsonData_get_Item_m4009629743(L_91, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_93 = V_1;
		NullCheck(L_92);
		JsonData_t1715015430 * L_94 = JsonData_get_Item_m253158020(L_92, L_93, /*hidden argument*/NULL);
		NullCheck(L_94);
		JsonData_t1715015430 * L_95 = JsonData_get_Item_m4009629743(L_94, _stringLiteral110371416, /*hidden argument*/NULL);
		NullCheck(L_95);
		String_t* L_96 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_95);
		V_5 = L_96;
	}

IL_0285:
	{
		JsonData_t1715015430 * L_97 = V_0;
		NullCheck(L_97);
		JsonData_t1715015430 * L_98 = JsonData_get_Item_m4009629743(L_97, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_98);
		JsonData_t1715015430 * L_99 = JsonData_get_Item_m4009629743(L_98, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_100 = V_1;
		NullCheck(L_99);
		JsonData_t1715015430 * L_101 = JsonData_get_Item_m253158020(L_99, L_100, /*hidden argument*/NULL);
		NullCheck(L_101);
		JsonData_t1715015430 * L_102 = JsonData_get_Item_m4009629743(L_101, _stringLiteral473771429, /*hidden argument*/NULL);
		if (!L_102)
		{
			goto IL_02db;
		}
	}
	{
		JsonData_t1715015430 * L_103 = V_0;
		NullCheck(L_103);
		JsonData_t1715015430 * L_104 = JsonData_get_Item_m4009629743(L_103, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_104);
		JsonData_t1715015430 * L_105 = JsonData_get_Item_m4009629743(L_104, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_106 = V_1;
		NullCheck(L_105);
		JsonData_t1715015430 * L_107 = JsonData_get_Item_m253158020(L_105, L_106, /*hidden argument*/NULL);
		NullCheck(L_107);
		JsonData_t1715015430 * L_108 = JsonData_get_Item_m4009629743(L_107, _stringLiteral473771429, /*hidden argument*/NULL);
		NullCheck(L_108);
		String_t* L_109 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_108);
		V_6 = L_109;
	}

IL_02db:
	{
		JsonData_t1715015430 * L_110 = V_0;
		NullCheck(L_110);
		JsonData_t1715015430 * L_111 = JsonData_get_Item_m4009629743(L_110, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_111);
		JsonData_t1715015430 * L_112 = JsonData_get_Item_m4009629743(L_111, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_113 = V_1;
		NullCheck(L_112);
		JsonData_t1715015430 * L_114 = JsonData_get_Item_m253158020(L_112, L_113, /*hidden argument*/NULL);
		NullCheck(L_114);
		JsonData_t1715015430 * L_115 = JsonData_get_Item_m4009629743(L_114, _stringLiteral3449379, /*hidden argument*/NULL);
		if (!L_115)
		{
			goto IL_0331;
		}
	}
	{
		JsonData_t1715015430 * L_116 = V_0;
		NullCheck(L_116);
		JsonData_t1715015430 * L_117 = JsonData_get_Item_m4009629743(L_116, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_117);
		JsonData_t1715015430 * L_118 = JsonData_get_Item_m4009629743(L_117, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_119 = V_1;
		NullCheck(L_118);
		JsonData_t1715015430 * L_120 = JsonData_get_Item_m253158020(L_118, L_119, /*hidden argument*/NULL);
		NullCheck(L_120);
		JsonData_t1715015430 * L_121 = JsonData_get_Item_m4009629743(L_120, _stringLiteral3449379, /*hidden argument*/NULL);
		NullCheck(L_121);
		String_t* L_122 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_121);
		V_7 = L_122;
	}

IL_0331:
	{
		JsonData_t1715015430 * L_123 = V_0;
		NullCheck(L_123);
		JsonData_t1715015430 * L_124 = JsonData_get_Item_m4009629743(L_123, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_124);
		JsonData_t1715015430 * L_125 = JsonData_get_Item_m4009629743(L_124, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_126 = V_1;
		NullCheck(L_125);
		JsonData_t1715015430 * L_127 = JsonData_get_Item_m253158020(L_125, L_126, /*hidden argument*/NULL);
		NullCheck(L_127);
		JsonData_t1715015430 * L_128 = JsonData_get_Item_m4009629743(L_127, _stringLiteral2872469938, /*hidden argument*/NULL);
		if (!L_128)
		{
			goto IL_0387;
		}
	}
	{
		JsonData_t1715015430 * L_129 = V_0;
		NullCheck(L_129);
		JsonData_t1715015430 * L_130 = JsonData_get_Item_m4009629743(L_129, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_130);
		JsonData_t1715015430 * L_131 = JsonData_get_Item_m4009629743(L_130, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_132 = V_1;
		NullCheck(L_131);
		JsonData_t1715015430 * L_133 = JsonData_get_Item_m253158020(L_131, L_132, /*hidden argument*/NULL);
		NullCheck(L_133);
		JsonData_t1715015430 * L_134 = JsonData_get_Item_m4009629743(L_133, _stringLiteral2872469938, /*hidden argument*/NULL);
		NullCheck(L_134);
		String_t* L_135 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_134);
		V_8 = L_135;
	}

IL_0387:
	{
		JsonData_t1715015430 * L_136 = V_0;
		NullCheck(L_136);
		JsonData_t1715015430 * L_137 = JsonData_get_Item_m4009629743(L_136, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_137);
		JsonData_t1715015430 * L_138 = JsonData_get_Item_m4009629743(L_137, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_139 = V_1;
		NullCheck(L_138);
		JsonData_t1715015430 * L_140 = JsonData_get_Item_m253158020(L_138, L_139, /*hidden argument*/NULL);
		NullCheck(L_140);
		JsonData_t1715015430 * L_141 = JsonData_get_Item_m4009629743(L_140, _stringLiteral2872469939, /*hidden argument*/NULL);
		if (!L_141)
		{
			goto IL_03dd;
		}
	}
	{
		JsonData_t1715015430 * L_142 = V_0;
		NullCheck(L_142);
		JsonData_t1715015430 * L_143 = JsonData_get_Item_m4009629743(L_142, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_143);
		JsonData_t1715015430 * L_144 = JsonData_get_Item_m4009629743(L_143, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_145 = V_1;
		NullCheck(L_144);
		JsonData_t1715015430 * L_146 = JsonData_get_Item_m253158020(L_144, L_145, /*hidden argument*/NULL);
		NullCheck(L_146);
		JsonData_t1715015430 * L_147 = JsonData_get_Item_m4009629743(L_146, _stringLiteral2872469939, /*hidden argument*/NULL);
		NullCheck(L_147);
		String_t* L_148 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_147);
		V_9 = L_148;
	}

IL_03dd:
	{
		JsonData_t1715015430 * L_149 = V_0;
		NullCheck(L_149);
		JsonData_t1715015430 * L_150 = JsonData_get_Item_m4009629743(L_149, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_150);
		JsonData_t1715015430 * L_151 = JsonData_get_Item_m4009629743(L_150, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_152 = V_1;
		NullCheck(L_151);
		JsonData_t1715015430 * L_153 = JsonData_get_Item_m253158020(L_151, L_152, /*hidden argument*/NULL);
		NullCheck(L_153);
		JsonData_t1715015430 * L_154 = JsonData_get_Item_m4009629743(L_153, _stringLiteral2854988908, /*hidden argument*/NULL);
		if (!L_154)
		{
			goto IL_0433;
		}
	}
	{
		JsonData_t1715015430 * L_155 = V_0;
		NullCheck(L_155);
		JsonData_t1715015430 * L_156 = JsonData_get_Item_m4009629743(L_155, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_156);
		JsonData_t1715015430 * L_157 = JsonData_get_Item_m4009629743(L_156, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_158 = V_1;
		NullCheck(L_157);
		JsonData_t1715015430 * L_159 = JsonData_get_Item_m253158020(L_157, L_158, /*hidden argument*/NULL);
		NullCheck(L_159);
		JsonData_t1715015430 * L_160 = JsonData_get_Item_m4009629743(L_159, _stringLiteral2854988908, /*hidden argument*/NULL);
		NullCheck(L_160);
		String_t* L_161 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_160);
		V_10 = L_161;
	}

IL_0433:
	{
		JsonData_t1715015430 * L_162 = V_0;
		NullCheck(L_162);
		JsonData_t1715015430 * L_163 = JsonData_get_Item_m4009629743(L_162, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_163);
		JsonData_t1715015430 * L_164 = JsonData_get_Item_m4009629743(L_163, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_165 = V_1;
		NullCheck(L_164);
		JsonData_t1715015430 * L_166 = JsonData_get_Item_m253158020(L_164, L_165, /*hidden argument*/NULL);
		NullCheck(L_166);
		JsonData_t1715015430 * L_167 = JsonData_get_Item_m4009629743(L_166, _stringLiteral137365935, /*hidden argument*/NULL);
		if (!L_167)
		{
			goto IL_0489;
		}
	}
	{
		JsonData_t1715015430 * L_168 = V_0;
		NullCheck(L_168);
		JsonData_t1715015430 * L_169 = JsonData_get_Item_m4009629743(L_168, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_169);
		JsonData_t1715015430 * L_170 = JsonData_get_Item_m4009629743(L_169, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_171 = V_1;
		NullCheck(L_170);
		JsonData_t1715015430 * L_172 = JsonData_get_Item_m253158020(L_170, L_171, /*hidden argument*/NULL);
		NullCheck(L_172);
		JsonData_t1715015430 * L_173 = JsonData_get_Item_m4009629743(L_172, _stringLiteral137365935, /*hidden argument*/NULL);
		NullCheck(L_173);
		String_t* L_174 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_173);
		V_11 = L_174;
	}

IL_0489:
	{
		JsonData_t1715015430 * L_175 = V_0;
		NullCheck(L_175);
		JsonData_t1715015430 * L_176 = JsonData_get_Item_m4009629743(L_175, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_176);
		JsonData_t1715015430 * L_177 = JsonData_get_Item_m4009629743(L_176, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_178 = V_1;
		NullCheck(L_177);
		JsonData_t1715015430 * L_179 = JsonData_get_Item_m253158020(L_177, L_178, /*hidden argument*/NULL);
		NullCheck(L_179);
		JsonData_t1715015430 * L_180 = JsonData_get_Item_m4009629743(L_179, _stringLiteral3800909073, /*hidden argument*/NULL);
		if (!L_180)
		{
			goto IL_04df;
		}
	}
	{
		JsonData_t1715015430 * L_181 = V_0;
		NullCheck(L_181);
		JsonData_t1715015430 * L_182 = JsonData_get_Item_m4009629743(L_181, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_182);
		JsonData_t1715015430 * L_183 = JsonData_get_Item_m4009629743(L_182, _stringLiteral2289459, /*hidden argument*/NULL);
		int32_t L_184 = V_1;
		NullCheck(L_183);
		JsonData_t1715015430 * L_185 = JsonData_get_Item_m253158020(L_183, L_184, /*hidden argument*/NULL);
		NullCheck(L_185);
		JsonData_t1715015430 * L_186 = JsonData_get_Item_m4009629743(L_185, _stringLiteral3800909073, /*hidden argument*/NULL);
		NullCheck(L_186);
		String_t* L_187 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String LitJson.JsonData::ToString() */, L_186);
		V_12 = L_187;
	}

IL_04df:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PublicSpotMessageDB_t1853916122_il2cpp_TypeInfo_var);
		PublicSpotMessageDB_t1853916122 * L_188 = PublicSpotMessageDB_get_Instance_m2156621208(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_189 = V_1;
		String_t* L_190 = V_3;
		String_t* L_191 = V_4;
		String_t* L_192 = V_5;
		String_t* L_193 = V_6;
		String_t* L_194 = V_7;
		String_t* L_195 = V_8;
		String_t* L_196 = V_9;
		String_t* L_197 = V_10;
		String_t* L_198 = V_11;
		String_t* L_199 = V_12;
		PublicSpotMessage_t3890192988 * L_200 = (PublicSpotMessage_t3890192988 *)il2cpp_codegen_object_new(PublicSpotMessage_t3890192988_il2cpp_TypeInfo_var);
		PublicSpotMessage__ctor_m1772971704(L_200, L_189, L_190, L_191, L_192, L_193, L_194, L_195, L_196, L_197, L_198, L_199, /*hidden argument*/NULL);
		NullCheck(L_188);
		PublicSpotMessageDB_AddPSMDB_m1227598172(L_188, L_200, /*hidden argument*/NULL);
		U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019 * L_201 = V_13;
		NullCheck(L_201);
		GameObject_t3674682005 * L_202 = L_201->get_Btn_0();
		NullCheck(L_202);
		Text_t9039225 * L_203 = GameObject_GetComponentInChildren_TisText_t9039225_m2924853261(L_202, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t9039225_m2924853261_MethodInfo_var);
		String_t* L_204 = V_5;
		NullCheck(L_203);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_203, L_204);
		U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019 * L_205 = V_13;
		NullCheck(L_205);
		GameObject_t3674682005 * L_206 = L_205->get_Btn_0();
		NullCheck(L_206);
		Button_t3896396478 * L_207 = GameObject_GetComponent_TisButton_t3896396478_m2812094092(L_206, /*hidden argument*/GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var);
		NullCheck(L_207);
		ButtonClickedEvent_t2796375743 * L_208 = Button_get_onClick_m1145127631(L_207, /*hidden argument*/NULL);
		U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019 * L_209 = V_13;
		IntPtr_t L_210;
		L_210.set_m_value_0((void*)(void*)U3CMessageGetTakeJsonU3Ec__AnonStorey28_U3CU3Em__5_m3275208734_MethodInfo_var);
		UnityAction_t594794173 * L_211 = (UnityAction_t594794173 *)il2cpp_codegen_object_new(UnityAction_t594794173_il2cpp_TypeInfo_var);
		UnityAction__ctor_m4130179243(L_211, L_209, L_210, /*hidden argument*/NULL);
		NullCheck(L_208);
		UnityEvent_AddListener_m4099140869(L_208, L_211, /*hidden argument*/NULL);
	}

IL_0538:
	{
		int32_t L_212 = V_1;
		V_1 = ((int32_t)((int32_t)L_212+(int32_t)1));
	}

IL_053c:
	{
		int32_t L_213 = V_1;
		JsonData_t1715015430 * L_214 = V_0;
		NullCheck(L_214);
		JsonData_t1715015430 * L_215 = JsonData_get_Item_m4009629743(L_214, _stringLiteral3897517420, /*hidden argument*/NULL);
		NullCheck(L_215);
		JsonData_t1715015430 * L_216 = JsonData_get_Item_m4009629743(L_215, _stringLiteral2289459, /*hidden argument*/NULL);
		NullCheck(L_216);
		int32_t L_217 = JsonData_get_Count_m412158079(L_216, /*hidden argument*/NULL);
		if ((((int32_t)L_213) < ((int32_t)L_217)))
		{
			goto IL_0043;
		}
	}

IL_055c:
	{
		return;
	}
}
// System.Void SpotMessageGet::OnDisable()
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3680721943;
extern const uint32_t SpotMessageGet_OnDisable_m3619947153_MetadataUsageId;
extern "C"  void SpotMessageGet_OnDisable_m3619947153 (SpotMessageGet_t1944980625 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SpotMessageGet_OnDisable_m3619947153_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		GameObject_t3674682005 * L_0 = __this->get_m_Parent_3();
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = GameObject_get_transform_m1278640159(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = Transform_get_childCount_m2107810675(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) < ((int32_t)1)))
		{
			goto IL_0061;
		}
	}
	{
		V_0 = 0;
		goto IL_004b;
	}

IL_001d:
	{
		GameObject_t3674682005 * L_3 = __this->get_m_Parent_3();
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3680721943, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1659122786 * L_9 = Transform_FindChild_m2149912886(L_4, L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_t3674682005 * L_10 = Component_get_gameObject_m1170635899(L_9, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004b:
	{
		int32_t L_12 = V_0;
		GameObject_t3674682005 * L_13 = __this->get_m_Parent_3();
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = GameObject_get_transform_m1278640159(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = Transform_get_childCount_m2107810675(L_14, /*hidden argument*/NULL);
		if ((((int32_t)L_12) < ((int32_t)L_15)))
		{
			goto IL_001d;
		}
	}

IL_0061:
	{
		return;
	}
}
// System.Void SpotMessageGet/<GreatManCheckProgress>c__Iterator21::.ctor()
extern "C"  void U3CGreatManCheckProgressU3Ec__Iterator21__ctor_m1852840879 (U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SpotMessageGet/<GreatManCheckProgress>c__Iterator21::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGreatManCheckProgressU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1988715395 (U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object SpotMessageGet/<GreatManCheckProgress>c__Iterator21::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGreatManCheckProgressU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m2306919703 (U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean SpotMessageGet/<GreatManCheckProgress>c__Iterator21::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3529469;
extern Il2CppCodeGenString* _stringLiteral1800556493;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t U3CGreatManCheckProgressU3Ec__Iterator21_MoveNext_m2769637861_MetadataUsageId;
extern "C"  bool U3CGreatManCheckProgressU3Ec__Iterator21_MoveNext_m2769637861 (U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGreatManCheckProgressU3Ec__Iterator21_MoveNext_m2769637861_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0041;
		}
		if (L_1 == 2)
		{
			goto IL_0081;
		}
	}
	{
		goto IL_0097;
	}

IL_0025:
	{
		WaitForSeconds_t1615819279 * L_2 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_2, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_2);
		__this->set_U24PC_1(1);
		goto IL_0099;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_3 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		AndroidWebview_Call_m335597645(L_3, _stringLiteral3529469, _stringLiteral1800556493, /*hidden argument*/NULL);
		WWW_t3134621005 * L_4 = __this->get_www_0();
		NullCheck(L_4);
		bool L_5 = WWW_get_isDone_m634060017(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0025;
		}
	}
	{
		WaitForSeconds_t1615819279 * L_6 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_6, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(2);
		goto IL_0099;
	}

IL_0081:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_7 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		AndroidWebview_Call_m1677830609(L_7, _stringLiteral3202370, /*hidden argument*/NULL);
		__this->set_U24PC_1((-1));
	}

IL_0097:
	{
		return (bool)0;
	}

IL_0099:
	{
		return (bool)1;
	}
	// Dead block : IL_009b: ldloc.1
}
// System.Void SpotMessageGet/<GreatManCheckProgress>c__Iterator21::Dispose()
extern "C"  void U3CGreatManCheckProgressU3Ec__Iterator21_Dispose_m2361907244 (U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void SpotMessageGet/<GreatManCheckProgress>c__Iterator21::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGreatManCheckProgressU3Ec__Iterator21_Reset_m3794241116_MetadataUsageId;
extern "C"  void U3CGreatManCheckProgressU3Ec__Iterator21_Reset_m3794241116 (U3CGreatManCheckProgressU3Ec__Iterator21_t200358716 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGreatManCheckProgressU3Ec__Iterator21_Reset_m3794241116_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SpotMessageGet/<MessageGetJsonStart>c__Iterator20::.ctor()
extern "C"  void U3CMessageGetJsonStartU3Ec__Iterator20__ctor_m1229495853 (U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SpotMessageGet/<MessageGetJsonStart>c__Iterator20::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMessageGetJsonStartU3Ec__Iterator20_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3327881861 (U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object SpotMessageGet/<MessageGetJsonStart>c__Iterator20::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMessageGetJsonStartU3Ec__Iterator20_System_Collections_IEnumerator_get_Current_m2902794265 (U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean SpotMessageGet/<MessageGetJsonStart>c__Iterator20::MoveNext()
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidWebview_t552345258_il2cpp_TypeInfo_var;
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3711296027;
extern Il2CppCodeGenString* _stringLiteral3202370;
extern const uint32_t U3CMessageGetJsonStartU3Ec__Iterator20_MoveNext_m4029356455_MetadataUsageId;
extern "C"  bool U3CMessageGetJsonStartU3Ec__Iterator20_MoveNext_m4029356455 (U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMessageGetJsonStartU3Ec__Iterator20_MoveNext_m4029356455_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0066;
		}
	}
	{
		goto IL_00dd;
	}

IL_0021:
	{
		WWW_t3134621005 * L_2 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m1985874080(L_2, _stringLiteral3711296027, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_0(L_2);
		SpotMessageGet_t1944980625 * L_3 = __this->get_U3CU3Ef__this_3();
		SpotMessageGet_t1944980625 * L_4 = __this->get_U3CU3Ef__this_3();
		WWW_t3134621005 * L_5 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_4);
		Il2CppObject * L_6 = SpotMessageGet_GreatManCheckProgress_m3593636778(L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		MonoBehaviour_StartCoroutine_m2135303124(L_3, L_6, /*hidden argument*/NULL);
		WWW_t3134621005 * L_7 = __this->get_U3CwwwU3E__0_0();
		__this->set_U24current_2(L_7);
		__this->set_U24PC_1(1);
		goto IL_00df;
	}

IL_0066:
	{
		WWW_t3134621005 * L_8 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_8);
		String_t* L_9 = WWW_get_error_m1787423074(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0091;
		}
	}
	{
		SpotMessageGet_t1944980625 * L_10 = __this->get_U3CU3Ef__this_3();
		WWW_t3134621005 * L_11 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_11);
		String_t* L_12 = WWW_get_text_m4216049525(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		SpotMessageGet_MessageGetTakeJson_m763779946(L_10, L_12, /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_0091:
	{
		WWW_t3134621005 * L_13 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_13);
		String_t* L_14 = WWW_get_error_m1787423074(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AndroidWebview_t552345258_il2cpp_TypeInfo_var);
		AndroidWebview_t552345258 * L_15 = AndroidWebview_get_Instance_m3239679012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		AndroidWebview_Call_m1677830609(L_15, _stringLiteral3202370, /*hidden argument*/NULL);
		int32_t L_16 = ((int32_t)3);
		Il2CppObject * L_17 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_16);
		NullCheck((Enum_t2862688501 *)L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_17);
		WWW_t3134621005 * L_19 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_19);
		String_t* L_20 = WWW_get_error_m1787423074(L_19, /*hidden argument*/NULL);
		int32_t L_21 = ((int32_t)2);
		Il2CppObject * L_22 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_21);
		NullCheck((Enum_t2862688501 *)L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_22);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_18, L_20, L_23, /*hidden argument*/NULL);
	}

IL_00d6:
	{
		__this->set_U24PC_1((-1));
	}

IL_00dd:
	{
		return (bool)0;
	}

IL_00df:
	{
		return (bool)1;
	}
	// Dead block : IL_00e1: ldloc.1
}
// System.Void SpotMessageGet/<MessageGetJsonStart>c__Iterator20::Dispose()
extern "C"  void U3CMessageGetJsonStartU3Ec__Iterator20_Dispose_m327791402 (U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void SpotMessageGet/<MessageGetJsonStart>c__Iterator20::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CMessageGetJsonStartU3Ec__Iterator20_Reset_m3170896090_MetadataUsageId;
extern "C"  void U3CMessageGetJsonStartU3Ec__Iterator20_Reset_m3170896090 (U3CMessageGetJsonStartU3Ec__Iterator20_t3614256382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMessageGetJsonStartU3Ec__Iterator20_Reset_m3170896090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SpotMessageGet/<MessageGetTakeJson>c__AnonStorey28::.ctor()
extern "C"  void U3CMessageGetTakeJsonU3Ec__AnonStorey28__ctor_m3432855312 (U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpotMessageGet/<MessageGetTakeJson>c__AnonStorey28::<>m__5()
extern Il2CppClass* Texts_t2791099287_il2cpp_TypeInfo_var;
extern Il2CppClass* ErrorMessage_t1367556351_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentScreen_t3026574117_il2cpp_TypeInfo_var;
extern Il2CppClass* CurrentState_t1622142648_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660319847;
extern Il2CppCodeGenString* _stringLiteral181257642;
extern Il2CppCodeGenString* _stringLiteral859168876;
extern const uint32_t U3CMessageGetTakeJsonU3Ec__AnonStorey28_U3CU3Em__5_m3275208734_MetadataUsageId;
extern "C"  void U3CMessageGetTakeJsonU3Ec__AnonStorey28_U3CU3Em__5_m3275208734 (U3CMessageGetTakeJsonU3Ec__AnonStorey28_t4134167019 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMessageGetTakeJsonU3Ec__AnonStorey28_U3CU3Em__5_m3275208734_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = Application_get_internetReachability_m379356375(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = ((int32_t)3);
		Il2CppObject * L_2 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		int32_t L_4 = ((int32_t)2);
		Il2CppObject * L_5 = Box(Texts_t2791099287_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(ErrorMessage_t1367556351_il2cpp_TypeInfo_var);
		ErrorMessage_AndroidAlertDialog_m3506572413(NULL /*static, unused*/, L_3, _stringLiteral3660319847, L_6, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_7 = __this->get_Btn_0();
		NullCheck(L_7);
		Button_t3896396478 * L_8 = GameObject_GetComponent_TisButton_t3896396478_m2812094092(L_7, /*hidden argument*/GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var);
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_m3709440845(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		String_t* L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = String_Substring_m2809233063(L_10, ((int32_t)17), /*hidden argument*/NULL);
		int32_t L_12 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		IL2CPP_RUNTIME_CLASS_INIT(CurrentScreen_t3026574117_il2cpp_TypeInfo_var);
		CurrentScreen_t3026574117 * L_13 = CurrentScreen_get_Instance_m3161187310(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		CurrentScreen_VisibleFalseState_m2603065973(L_13, ((int32_t)11), /*hidden argument*/NULL);
		CurrentScreen_t3026574117 * L_14 = CurrentScreen_get_Instance_m3161187310(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		CurrentScreen_State_m3173002564(L_14, ((int32_t)12), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CurrentState_t1622142648_il2cpp_TypeInfo_var);
		CurrentState_set_LoginState_m3940368275(NULL /*static, unused*/, ((int32_t)11), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_15 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral181257642, /*hidden argument*/NULL);
		int32_t L_16 = V_1;
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_15);
		GameObject_SendMessage_m423373689(L_15, _stringLiteral859168876, L_18, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TextManager::.ctor()
extern "C"  void TextManager__ctor_m456557003 (TextManager_t1663192480 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TMPro.Compute_DT_EventArgs::.ctor(TMPro.Compute_DistanceTransform_EventTypes,System.Single)
extern "C"  void Compute_DT_EventArgs__ctor_m3947918093 (Compute_DT_EventArgs_t2335785766 * __this, int32_t ___type0, float ___progress1, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___type0;
		__this->set_EventType_0(L_0);
		float L_1 = ___progress1;
		__this->set_ProgressPercentage_1(L_1);
		return;
	}
}
// System.Void TMPro.Compute_DT_EventArgs::.ctor(TMPro.Compute_DistanceTransform_EventTypes,UnityEngine.Color[])
extern "C"  void Compute_DT_EventArgs__ctor_m3727474598 (Compute_DT_EventArgs_t2335785766 * __this, int32_t ___type0, ColorU5BU5D_t2441545636* ___colors1, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___type0;
		__this->set_EventType_0(L_0);
		ColorU5BU5D_t2441545636* L_1 = ___colors1;
		__this->set_Colors_2(L_1);
		return;
	}
}
// System.Void TMPro.Extents::.ctor(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Extents__ctor_m1813869138 (Extents_t2060714539 * __this, Vector2_t4282066565  ___min0, Vector2_t4282066565  ___max1, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = ___min0;
		__this->set_min_0(L_0);
		Vector2_t4282066565  L_1 = ___max1;
		__this->set_max_1(L_1);
		return;
	}
}
extern "C"  void Extents__ctor_m1813869138_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___min0, Vector2_t4282066565  ___max1, const MethodInfo* method)
{
	Extents_t2060714539 * _thisAdjusted = reinterpret_cast<Extents_t2060714539 *>(__this + 1);
	Extents__ctor_m1813869138(_thisAdjusted, ___min0, ___max1, method);
}
// System.String TMPro.Extents::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral74345914;
extern Il2CppCodeGenString* _stringLiteral3212;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral2464730965;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t Extents_ToString_m3250151057_MetadataUsageId;
extern "C"  String_t* Extents_ToString_m3250151057 (Extents_t2060714539 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Extents_ToString_m3250151057_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral74345914);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral74345914);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Vector2_t4282066565 * L_2 = __this->get_address_of_min_0();
		float* L_3 = L_2->get_address_of_x_1();
		String_t* L_4 = Single_ToString_m639595682(L_3, _stringLiteral3212, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_4);
		StringU5BU5D_t4054002952* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral1396);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_6 = L_5;
		Vector2_t4282066565 * L_7 = __this->get_address_of_min_0();
		float* L_8 = L_7->get_address_of_y_2();
		String_t* L_9 = Single_ToString_m639595682(L_8, _stringLiteral3212, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_9);
		StringU5BU5D_t4054002952* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral2464730965);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral2464730965);
		StringU5BU5D_t4054002952* L_11 = L_10;
		Vector2_t4282066565 * L_12 = __this->get_address_of_max_1();
		float* L_13 = L_12->get_address_of_x_1();
		String_t* L_14 = Single_ToString_m639595682(L_13, _stringLiteral3212, /*hidden argument*/NULL);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_14);
		StringU5BU5D_t4054002952* L_15 = L_11;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 6);
		ArrayElementTypeCheck (L_15, _stringLiteral1396);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_16 = L_15;
		Vector2_t4282066565 * L_17 = __this->get_address_of_max_1();
		float* L_18 = L_17->get_address_of_y_2();
		String_t* L_19 = Single_ToString_m639595682(L_18, _stringLiteral3212, /*hidden argument*/NULL);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 7);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)L_19);
		StringU5BU5D_t4054002952* L_20 = L_16;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 8);
		ArrayElementTypeCheck (L_20, _stringLiteral41);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m21867311(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		V_0 = L_21;
		String_t* L_22 = V_0;
		return L_22;
	}
}
extern "C"  String_t* Extents_ToString_m3250151057_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Extents_t2060714539 * _thisAdjusted = reinterpret_cast<Extents_t2060714539 *>(__this + 1);
	return Extents_ToString_m3250151057(_thisAdjusted, method);
}
// Conversion methods for marshalling of: TMPro.Extents
extern "C" void Extents_t2060714539_marshal_pinvoke(const Extents_t2060714539& unmarshaled, Extents_t2060714539_marshaled_pinvoke& marshaled)
{
	Vector2_t4282066565_marshal_pinvoke(unmarshaled.get_min_0(), marshaled.___min_0);
	Vector2_t4282066565_marshal_pinvoke(unmarshaled.get_max_1(), marshaled.___max_1);
}
extern "C" void Extents_t2060714539_marshal_pinvoke_back(const Extents_t2060714539_marshaled_pinvoke& marshaled, Extents_t2060714539& unmarshaled)
{
	Vector2_t4282066565  unmarshaled_min_temp_0;
	memset(&unmarshaled_min_temp_0, 0, sizeof(unmarshaled_min_temp_0));
	Vector2_t4282066565_marshal_pinvoke_back(marshaled.___min_0, unmarshaled_min_temp_0);
	unmarshaled.set_min_0(unmarshaled_min_temp_0);
	Vector2_t4282066565  unmarshaled_max_temp_1;
	memset(&unmarshaled_max_temp_1, 0, sizeof(unmarshaled_max_temp_1));
	Vector2_t4282066565_marshal_pinvoke_back(marshaled.___max_1, unmarshaled_max_temp_1);
	unmarshaled.set_max_1(unmarshaled_max_temp_1);
}
// Conversion method for clean up from marshalling of: TMPro.Extents
extern "C" void Extents_t2060714539_marshal_pinvoke_cleanup(Extents_t2060714539_marshaled_pinvoke& marshaled)
{
	Vector2_t4282066565_marshal_pinvoke_cleanup(marshaled.___min_0);
	Vector2_t4282066565_marshal_pinvoke_cleanup(marshaled.___max_1);
}
// Conversion methods for marshalling of: TMPro.Extents
extern "C" void Extents_t2060714539_marshal_com(const Extents_t2060714539& unmarshaled, Extents_t2060714539_marshaled_com& marshaled)
{
	Vector2_t4282066565_marshal_com(unmarshaled.get_min_0(), marshaled.___min_0);
	Vector2_t4282066565_marshal_com(unmarshaled.get_max_1(), marshaled.___max_1);
}
extern "C" void Extents_t2060714539_marshal_com_back(const Extents_t2060714539_marshaled_com& marshaled, Extents_t2060714539& unmarshaled)
{
	Vector2_t4282066565  unmarshaled_min_temp_0;
	memset(&unmarshaled_min_temp_0, 0, sizeof(unmarshaled_min_temp_0));
	Vector2_t4282066565_marshal_com_back(marshaled.___min_0, unmarshaled_min_temp_0);
	unmarshaled.set_min_0(unmarshaled_min_temp_0);
	Vector2_t4282066565  unmarshaled_max_temp_1;
	memset(&unmarshaled_max_temp_1, 0, sizeof(unmarshaled_max_temp_1));
	Vector2_t4282066565_marshal_com_back(marshaled.___max_1, unmarshaled_max_temp_1);
	unmarshaled.set_max_1(unmarshaled_max_temp_1);
}
// Conversion method for clean up from marshalling of: TMPro.Extents
extern "C" void Extents_t2060714539_marshal_com_cleanup(Extents_t2060714539_marshaled_com& marshaled)
{
	Vector2_t4282066565_marshal_com_cleanup(marshaled.___min_0);
	Vector2_t4282066565_marshal_com_cleanup(marshaled.___max_1);
}
// System.Void TMPro.FaceInfo::.ctor()
extern "C"  void FaceInfo__ctor_m3175481910 (FaceInfo_t3469866561 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TMPro.FastAction::.ctor()
extern Il2CppClass* LinkedList_1_t1645869067_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1797527366_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1__ctor_m1519450093_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m4280543573_MethodInfo_var;
extern const uint32_t FastAction__ctor_m1456332175_MetadataUsageId;
extern "C"  void FastAction__ctor_m1456332175 (FastAction_t2355551432 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FastAction__ctor_m1456332175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t1645869067 * L_0 = (LinkedList_1_t1645869067 *)il2cpp_codegen_object_new(LinkedList_1_t1645869067_il2cpp_TypeInfo_var);
		LinkedList_1__ctor_m1519450093(L_0, /*hidden argument*/LinkedList_1__ctor_m1519450093_MethodInfo_var);
		__this->set_delegates_0(L_0);
		Dictionary_2_t1797527366 * L_1 = (Dictionary_2_t1797527366 *)il2cpp_codegen_object_new(Dictionary_2_t1797527366_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4280543573(L_1, /*hidden argument*/Dictionary_2__ctor_m4280543573_MethodInfo_var);
		__this->set_lookup_1(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TMPro.FastAction::Add(System.Action)
extern const MethodInfo* Dictionary_2_ContainsKey_m1382572440_MethodInfo_var;
extern const MethodInfo* LinkedList_1_AddLast_m340215404_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m1472319942_MethodInfo_var;
extern const uint32_t FastAction_Add_m4206295599_MetadataUsageId;
extern "C"  void FastAction_Add_m4206295599 (FastAction_t2355551432 * __this, Action_t3771233898 * ___rhs0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FastAction_Add_m4206295599_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1797527366 * L_0 = __this->get_lookup_1();
		Action_t3771233898 * L_1 = ___rhs0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m1382572440(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m1382572440_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Dictionary_2_t1797527366 * L_3 = __this->get_lookup_1();
		Action_t3771233898 * L_4 = ___rhs0;
		LinkedList_1_t1645869067 * L_5 = __this->get_delegates_0();
		Action_t3771233898 * L_6 = ___rhs0;
		NullCheck(L_5);
		LinkedListNode_1_t3387968605 * L_7 = LinkedList_1_AddLast_m340215404(L_5, L_6, /*hidden argument*/LinkedList_1_AddLast_m340215404_MethodInfo_var);
		NullCheck(L_3);
		Dictionary_2_set_Item_m1472319942(L_3, L_4, L_7, /*hidden argument*/Dictionary_2_set_Item_m1472319942_MethodInfo_var);
		return;
	}
}
// System.Void TMPro.FastAction::Remove(System.Action)
extern const MethodInfo* Dictionary_2_TryGetValue_m444534244_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Remove_m2805505616_MethodInfo_var;
extern const MethodInfo* LinkedList_1_Remove_m1958035851_MethodInfo_var;
extern const uint32_t FastAction_Remove_m1406368196_MetadataUsageId;
extern "C"  void FastAction_Remove_m1406368196 (FastAction_t2355551432 * __this, Action_t3771233898 * ___rhs0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FastAction_Remove_m1406368196_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedListNode_1_t3387968605 * V_0 = NULL;
	{
		Dictionary_2_t1797527366 * L_0 = __this->get_lookup_1();
		Action_t3771233898 * L_1 = ___rhs0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m444534244(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m444534244_MethodInfo_var);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Dictionary_2_t1797527366 * L_3 = __this->get_lookup_1();
		Action_t3771233898 * L_4 = ___rhs0;
		NullCheck(L_3);
		Dictionary_2_Remove_m2805505616(L_3, L_4, /*hidden argument*/Dictionary_2_Remove_m2805505616_MethodInfo_var);
		LinkedList_1_t1645869067 * L_5 = __this->get_delegates_0();
		LinkedListNode_1_t3387968605 * L_6 = V_0;
		NullCheck(L_5);
		LinkedList_1_Remove_m1958035851(L_5, L_6, /*hidden argument*/LinkedList_1_Remove_m1958035851_MethodInfo_var);
	}

IL_002c:
	{
		return;
	}
}
// System.Void TMPro.FastAction::Call()
extern const MethodInfo* LinkedList_1_get_First_m1999564331_MethodInfo_var;
extern const MethodInfo* LinkedListNode_1_get_Value_m1983866631_MethodInfo_var;
extern const MethodInfo* LinkedListNode_1_get_Next_m848486936_MethodInfo_var;
extern const uint32_t FastAction_Call_m2862857395_MetadataUsageId;
extern "C"  void FastAction_Call_m2862857395 (FastAction_t2355551432 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FastAction_Call_m2862857395_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedListNode_1_t3387968605 * V_0 = NULL;
	{
		LinkedList_1_t1645869067 * L_0 = __this->get_delegates_0();
		NullCheck(L_0);
		LinkedListNode_1_t3387968605 * L_1 = LinkedList_1_get_First_m1999564331(L_0, /*hidden argument*/LinkedList_1_get_First_m1999564331_MethodInfo_var);
		V_0 = L_1;
		goto IL_0023;
	}

IL_0011:
	{
		LinkedListNode_1_t3387968605 * L_2 = V_0;
		NullCheck(L_2);
		Action_t3771233898 * L_3 = LinkedListNode_1_get_Value_m1983866631(L_2, /*hidden argument*/LinkedListNode_1_get_Value_m1983866631_MethodInfo_var);
		NullCheck(L_3);
		Action_Invoke_m1445970038(L_3, /*hidden argument*/NULL);
		LinkedListNode_1_t3387968605 * L_4 = V_0;
		NullCheck(L_4);
		LinkedListNode_1_t3387968605 * L_5 = LinkedListNode_1_get_Next_m848486936(L_4, /*hidden argument*/LinkedListNode_1_get_Next_m848486936_MethodInfo_var);
		V_0 = L_5;
	}

IL_0023:
	{
		LinkedListNode_1_t3387968605 * L_6 = V_0;
		if (L_6)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}
}
// Conversion methods for marshalling of: TMPro.FontCreationSetting
extern "C" void FontCreationSetting_t3395101892_marshal_pinvoke(const FontCreationSetting_t3395101892& unmarshaled, FontCreationSetting_t3395101892_marshaled_pinvoke& marshaled)
{
	marshaled.___fontSourcePath_0 = il2cpp_codegen_marshal_string(unmarshaled.get_fontSourcePath_0());
	marshaled.___fontSizingMode_1 = unmarshaled.get_fontSizingMode_1();
	marshaled.___fontSize_2 = unmarshaled.get_fontSize_2();
	marshaled.___fontPadding_3 = unmarshaled.get_fontPadding_3();
	marshaled.___fontPackingMode_4 = unmarshaled.get_fontPackingMode_4();
	marshaled.___fontAtlasWidth_5 = unmarshaled.get_fontAtlasWidth_5();
	marshaled.___fontAtlasHeight_6 = unmarshaled.get_fontAtlasHeight_6();
	marshaled.___fontCharacterSet_7 = unmarshaled.get_fontCharacterSet_7();
	marshaled.___fontStyle_8 = unmarshaled.get_fontStyle_8();
	marshaled.___fontStlyeModifier_9 = unmarshaled.get_fontStlyeModifier_9();
	marshaled.___fontRenderMode_10 = unmarshaled.get_fontRenderMode_10();
	marshaled.___fontKerning_11 = unmarshaled.get_fontKerning_11();
}
extern "C" void FontCreationSetting_t3395101892_marshal_pinvoke_back(const FontCreationSetting_t3395101892_marshaled_pinvoke& marshaled, FontCreationSetting_t3395101892& unmarshaled)
{
	unmarshaled.set_fontSourcePath_0(il2cpp_codegen_marshal_string_result(marshaled.___fontSourcePath_0));
	int32_t unmarshaled_fontSizingMode_temp_1 = 0;
	unmarshaled_fontSizingMode_temp_1 = marshaled.___fontSizingMode_1;
	unmarshaled.set_fontSizingMode_1(unmarshaled_fontSizingMode_temp_1);
	int32_t unmarshaled_fontSize_temp_2 = 0;
	unmarshaled_fontSize_temp_2 = marshaled.___fontSize_2;
	unmarshaled.set_fontSize_2(unmarshaled_fontSize_temp_2);
	int32_t unmarshaled_fontPadding_temp_3 = 0;
	unmarshaled_fontPadding_temp_3 = marshaled.___fontPadding_3;
	unmarshaled.set_fontPadding_3(unmarshaled_fontPadding_temp_3);
	int32_t unmarshaled_fontPackingMode_temp_4 = 0;
	unmarshaled_fontPackingMode_temp_4 = marshaled.___fontPackingMode_4;
	unmarshaled.set_fontPackingMode_4(unmarshaled_fontPackingMode_temp_4);
	int32_t unmarshaled_fontAtlasWidth_temp_5 = 0;
	unmarshaled_fontAtlasWidth_temp_5 = marshaled.___fontAtlasWidth_5;
	unmarshaled.set_fontAtlasWidth_5(unmarshaled_fontAtlasWidth_temp_5);
	int32_t unmarshaled_fontAtlasHeight_temp_6 = 0;
	unmarshaled_fontAtlasHeight_temp_6 = marshaled.___fontAtlasHeight_6;
	unmarshaled.set_fontAtlasHeight_6(unmarshaled_fontAtlasHeight_temp_6);
	int32_t unmarshaled_fontCharacterSet_temp_7 = 0;
	unmarshaled_fontCharacterSet_temp_7 = marshaled.___fontCharacterSet_7;
	unmarshaled.set_fontCharacterSet_7(unmarshaled_fontCharacterSet_temp_7);
	int32_t unmarshaled_fontStyle_temp_8 = 0;
	unmarshaled_fontStyle_temp_8 = marshaled.___fontStyle_8;
	unmarshaled.set_fontStyle_8(unmarshaled_fontStyle_temp_8);
	float unmarshaled_fontStlyeModifier_temp_9 = 0.0f;
	unmarshaled_fontStlyeModifier_temp_9 = marshaled.___fontStlyeModifier_9;
	unmarshaled.set_fontStlyeModifier_9(unmarshaled_fontStlyeModifier_temp_9);
	int32_t unmarshaled_fontRenderMode_temp_10 = 0;
	unmarshaled_fontRenderMode_temp_10 = marshaled.___fontRenderMode_10;
	unmarshaled.set_fontRenderMode_10(unmarshaled_fontRenderMode_temp_10);
	bool unmarshaled_fontKerning_temp_11 = false;
	unmarshaled_fontKerning_temp_11 = marshaled.___fontKerning_11;
	unmarshaled.set_fontKerning_11(unmarshaled_fontKerning_temp_11);
}
// Conversion method for clean up from marshalling of: TMPro.FontCreationSetting
extern "C" void FontCreationSetting_t3395101892_marshal_pinvoke_cleanup(FontCreationSetting_t3395101892_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___fontSourcePath_0);
	marshaled.___fontSourcePath_0 = NULL;
}
// Conversion methods for marshalling of: TMPro.FontCreationSetting
extern "C" void FontCreationSetting_t3395101892_marshal_com(const FontCreationSetting_t3395101892& unmarshaled, FontCreationSetting_t3395101892_marshaled_com& marshaled)
{
	marshaled.___fontSourcePath_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_fontSourcePath_0());
	marshaled.___fontSizingMode_1 = unmarshaled.get_fontSizingMode_1();
	marshaled.___fontSize_2 = unmarshaled.get_fontSize_2();
	marshaled.___fontPadding_3 = unmarshaled.get_fontPadding_3();
	marshaled.___fontPackingMode_4 = unmarshaled.get_fontPackingMode_4();
	marshaled.___fontAtlasWidth_5 = unmarshaled.get_fontAtlasWidth_5();
	marshaled.___fontAtlasHeight_6 = unmarshaled.get_fontAtlasHeight_6();
	marshaled.___fontCharacterSet_7 = unmarshaled.get_fontCharacterSet_7();
	marshaled.___fontStyle_8 = unmarshaled.get_fontStyle_8();
	marshaled.___fontStlyeModifier_9 = unmarshaled.get_fontStlyeModifier_9();
	marshaled.___fontRenderMode_10 = unmarshaled.get_fontRenderMode_10();
	marshaled.___fontKerning_11 = unmarshaled.get_fontKerning_11();
}
extern "C" void FontCreationSetting_t3395101892_marshal_com_back(const FontCreationSetting_t3395101892_marshaled_com& marshaled, FontCreationSetting_t3395101892& unmarshaled)
{
	unmarshaled.set_fontSourcePath_0(il2cpp_codegen_marshal_bstring_result(marshaled.___fontSourcePath_0));
	int32_t unmarshaled_fontSizingMode_temp_1 = 0;
	unmarshaled_fontSizingMode_temp_1 = marshaled.___fontSizingMode_1;
	unmarshaled.set_fontSizingMode_1(unmarshaled_fontSizingMode_temp_1);
	int32_t unmarshaled_fontSize_temp_2 = 0;
	unmarshaled_fontSize_temp_2 = marshaled.___fontSize_2;
	unmarshaled.set_fontSize_2(unmarshaled_fontSize_temp_2);
	int32_t unmarshaled_fontPadding_temp_3 = 0;
	unmarshaled_fontPadding_temp_3 = marshaled.___fontPadding_3;
	unmarshaled.set_fontPadding_3(unmarshaled_fontPadding_temp_3);
	int32_t unmarshaled_fontPackingMode_temp_4 = 0;
	unmarshaled_fontPackingMode_temp_4 = marshaled.___fontPackingMode_4;
	unmarshaled.set_fontPackingMode_4(unmarshaled_fontPackingMode_temp_4);
	int32_t unmarshaled_fontAtlasWidth_temp_5 = 0;
	unmarshaled_fontAtlasWidth_temp_5 = marshaled.___fontAtlasWidth_5;
	unmarshaled.set_fontAtlasWidth_5(unmarshaled_fontAtlasWidth_temp_5);
	int32_t unmarshaled_fontAtlasHeight_temp_6 = 0;
	unmarshaled_fontAtlasHeight_temp_6 = marshaled.___fontAtlasHeight_6;
	unmarshaled.set_fontAtlasHeight_6(unmarshaled_fontAtlasHeight_temp_6);
	int32_t unmarshaled_fontCharacterSet_temp_7 = 0;
	unmarshaled_fontCharacterSet_temp_7 = marshaled.___fontCharacterSet_7;
	unmarshaled.set_fontCharacterSet_7(unmarshaled_fontCharacterSet_temp_7);
	int32_t unmarshaled_fontStyle_temp_8 = 0;
	unmarshaled_fontStyle_temp_8 = marshaled.___fontStyle_8;
	unmarshaled.set_fontStyle_8(unmarshaled_fontStyle_temp_8);
	float unmarshaled_fontStlyeModifier_temp_9 = 0.0f;
	unmarshaled_fontStlyeModifier_temp_9 = marshaled.___fontStlyeModifier_9;
	unmarshaled.set_fontStlyeModifier_9(unmarshaled_fontStlyeModifier_temp_9);
	int32_t unmarshaled_fontRenderMode_temp_10 = 0;
	unmarshaled_fontRenderMode_temp_10 = marshaled.___fontRenderMode_10;
	unmarshaled.set_fontRenderMode_10(unmarshaled_fontRenderMode_temp_10);
	bool unmarshaled_fontKerning_temp_11 = false;
	unmarshaled_fontKerning_temp_11 = marshaled.___fontKerning_11;
	unmarshaled.set_fontKerning_11(unmarshaled_fontKerning_temp_11);
}
// Conversion method for clean up from marshalling of: TMPro.FontCreationSetting
extern "C" void FontCreationSetting_t3395101892_marshal_com_cleanup(FontCreationSetting_t3395101892_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___fontSourcePath_0);
	marshaled.___fontSourcePath_0 = NULL;
}
// System.Void TMPro.Glyph2D::.ctor()
extern "C"  void Glyph2D__ctor_m844861447 (Glyph2D_t3497109536 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TMPro.InlineGraphic::.ctor()
extern "C"  void InlineGraphic__ctor_m1035377686 (InlineGraphic_t3606901041 * __this, const MethodInfo* method)
{
	{
		MaskableGraphic__ctor_m3514233785(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Texture TMPro.InlineGraphic::get_mainTexture()
extern Il2CppClass* Graphic_t836799438_il2cpp_TypeInfo_var;
extern const uint32_t InlineGraphic_get_mainTexture_m2725741276_MetadataUsageId;
extern "C"  Texture_t2526458961 * InlineGraphic_get_mainTexture_m2725741276 (InlineGraphic_t3606901041 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InlineGraphic_get_mainTexture_m2725741276_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture_t2526458961 * L_0 = __this->get_texture_28();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Graphic_t836799438_il2cpp_TypeInfo_var);
		Texture2D_t3884108195 * L_2 = ((Graphic_t836799438_StaticFields*)Graphic_t836799438_il2cpp_TypeInfo_var->static_fields)->get_s_WhiteTexture_3();
		return L_2;
	}

IL_0017:
	{
		Texture_t2526458961 * L_3 = __this->get_texture_28();
		return L_3;
	}
}
// System.Void TMPro.InlineGraphic::Awake()
extern const MethodInfo* Component_GetComponentInParent_TisInlineGraphicManager_t3583857972_m3490019100_MethodInfo_var;
extern const uint32_t InlineGraphic_Awake_m1272982905_MetadataUsageId;
extern "C"  void InlineGraphic_Awake_m1272982905 (InlineGraphic_t3606901041 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InlineGraphic_Awake_m1272982905_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InlineGraphicManager_t3583857972 * L_0 = Component_GetComponentInParent_TisInlineGraphicManager_t3583857972_m3490019100(__this, /*hidden argument*/Component_GetComponentInParent_TisInlineGraphicManager_t3583857972_m3490019100_MethodInfo_var);
		__this->set_m_manager_29(L_0);
		return;
	}
}
// System.Void TMPro.InlineGraphic::OnEnable()
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var;
extern const uint32_t InlineGraphic_OnEnable_m553102256_MetadataUsageId;
extern "C"  void InlineGraphic_OnEnable_m553102256 (InlineGraphic_t3606901041 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InlineGraphic_OnEnable_m553102256_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t972643934 * L_0 = __this->get_m_RectTransform_30();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		GameObject_t3674682005 * L_2 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		RectTransform_t972643934 * L_3 = GameObject_GetComponent_TisRectTransform_t972643934_m406276429(L_2, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var);
		__this->set_m_RectTransform_30(L_3);
	}

IL_0022:
	{
		InlineGraphicManager_t3583857972 * L_4 = __this->get_m_manager_29();
		bool L_5 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005f;
		}
	}
	{
		InlineGraphicManager_t3583857972 * L_6 = __this->get_m_manager_29();
		NullCheck(L_6);
		TMP_SpriteAsset_t3955808645 * L_7 = InlineGraphicManager_get_spriteAsset_m630756793(L_6, /*hidden argument*/NULL);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005f;
		}
	}
	{
		InlineGraphicManager_t3583857972 * L_9 = __this->get_m_manager_29();
		NullCheck(L_9);
		TMP_SpriteAsset_t3955808645 * L_10 = InlineGraphicManager_get_spriteAsset_m630756793(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Texture_t2526458961 * L_11 = L_10->get_spriteSheet_2();
		__this->set_texture_28(L_11);
	}

IL_005f:
	{
		return;
	}
}
// System.Void TMPro.InlineGraphic::OnDisable()
extern "C"  void InlineGraphic_OnDisable_m407238013 (InlineGraphic_t3606901041 * __this, const MethodInfo* method)
{
	{
		MaskableGraphic_OnDisable_m2667299744(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TMPro.InlineGraphic::OnTransformParentChanged()
extern "C"  void InlineGraphic_OnTransformParentChanged_m3639723531 (InlineGraphic_t3606901041 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void TMPro.InlineGraphic::OnRectTransformDimensionsChange()
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRectTransform_t972643934_m1940403147_MethodInfo_var;
extern const uint32_t InlineGraphic_OnRectTransformDimensionsChange_m514316346_MetadataUsageId;
extern "C"  void InlineGraphic_OnRectTransformDimensionsChange_m514316346 (InlineGraphic_t3606901041 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InlineGraphic_OnRectTransformDimensionsChange_m514316346_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t972643934 * L_0 = __this->get_m_RectTransform_30();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		GameObject_t3674682005 * L_2 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		RectTransform_t972643934 * L_3 = GameObject_GetComponent_TisRectTransform_t972643934_m406276429(L_2, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var);
		__this->set_m_RectTransform_30(L_3);
	}

IL_0022:
	{
		RectTransform_t972643934 * L_4 = __this->get_m_ParentRectTransform_31();
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0049;
		}
	}
	{
		RectTransform_t972643934 * L_6 = __this->get_m_RectTransform_30();
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = Transform_get_parent_m2236876972(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		RectTransform_t972643934 * L_8 = Component_GetComponent_TisRectTransform_t972643934_m1940403147(L_7, /*hidden argument*/Component_GetComponent_TisRectTransform_t972643934_m1940403147_MethodInfo_var);
		__this->set_m_ParentRectTransform_31(L_8);
	}

IL_0049:
	{
		RectTransform_t972643934 * L_9 = __this->get_m_RectTransform_30();
		NullCheck(L_9);
		Vector2_t4282066565  L_10 = RectTransform_get_pivot_m3785570595(L_9, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_11 = __this->get_m_ParentRectTransform_31();
		NullCheck(L_11);
		Vector2_t4282066565  L_12 = RectTransform_get_pivot_m3785570595(L_11, /*hidden argument*/NULL);
		bool L_13 = Vector2_op_Inequality_m1638984867(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		RectTransform_t972643934 * L_14 = __this->get_m_RectTransform_30();
		RectTransform_t972643934 * L_15 = __this->get_m_ParentRectTransform_31();
		NullCheck(L_15);
		Vector2_t4282066565  L_16 = RectTransform_get_pivot_m3785570595(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		RectTransform_set_pivot_m457344806(L_14, L_16, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Void TMPro.InlineGraphic::UpdateMaterial()
extern "C"  void InlineGraphic_UpdateMaterial_m505296702 (InlineGraphic_t3606901041 * __this, const MethodInfo* method)
{
	{
		Graphic_UpdateMaterial_m1564277409(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TMPro.InlineGraphic::UpdateGeometry()
extern "C"  void InlineGraphic_UpdateGeometry_m1068970953 (InlineGraphic_t3606901041 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
