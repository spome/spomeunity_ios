﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Mesh
struct Mesh_t4241756145;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t701588350;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
struct Vector3_t4282066566_marshaled_pinvoke;
struct Vector4_t4282066567_marshaled_pinvoke;
struct Vector2_t4282066565_marshaled_pinvoke;
struct Color32_t598853688_marshaled_pinvoke;
struct Vector3_t4282066566_marshaled_com;
struct Vector4_t4282066567_marshaled_com;
struct Vector2_t4282066565_marshaled_com;
struct Color32_t598853688_marshaled_com;

#include "mscorlib_System_ValueType1744280289.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MeshInfo
struct  TMP_MeshInfo_t801067801 
{
public:
	// UnityEngine.Mesh TMPro.TMP_MeshInfo::mesh
	Mesh_t4241756145 * ___mesh_3;
	// System.Int32 TMPro.TMP_MeshInfo::vertexCount
	int32_t ___vertexCount_4;
	// UnityEngine.Vector3[] TMPro.TMP_MeshInfo::vertices
	Vector3U5BU5D_t215400611* ___vertices_5;
	// UnityEngine.Vector3[] TMPro.TMP_MeshInfo::normals
	Vector3U5BU5D_t215400611* ___normals_6;
	// UnityEngine.Vector4[] TMPro.TMP_MeshInfo::tangents
	Vector4U5BU5D_t701588350* ___tangents_7;
	// UnityEngine.Vector2[] TMPro.TMP_MeshInfo::uvs0
	Vector2U5BU5D_t4024180168* ___uvs0_8;
	// UnityEngine.Vector2[] TMPro.TMP_MeshInfo::uvs2
	Vector2U5BU5D_t4024180168* ___uvs2_9;
	// UnityEngine.Vector2[] TMPro.TMP_MeshInfo::uvs4
	Vector2U5BU5D_t4024180168* ___uvs4_10;
	// UnityEngine.Color32[] TMPro.TMP_MeshInfo::colors32
	Color32U5BU5D_t2960766953* ___colors32_11;
	// System.Int32[] TMPro.TMP_MeshInfo::triangles
	Int32U5BU5D_t3230847821* ___triangles_12;

public:
	inline static int32_t get_offset_of_mesh_3() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t801067801, ___mesh_3)); }
	inline Mesh_t4241756145 * get_mesh_3() const { return ___mesh_3; }
	inline Mesh_t4241756145 ** get_address_of_mesh_3() { return &___mesh_3; }
	inline void set_mesh_3(Mesh_t4241756145 * value)
	{
		___mesh_3 = value;
		Il2CppCodeGenWriteBarrier(&___mesh_3, value);
	}

	inline static int32_t get_offset_of_vertexCount_4() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t801067801, ___vertexCount_4)); }
	inline int32_t get_vertexCount_4() const { return ___vertexCount_4; }
	inline int32_t* get_address_of_vertexCount_4() { return &___vertexCount_4; }
	inline void set_vertexCount_4(int32_t value)
	{
		___vertexCount_4 = value;
	}

	inline static int32_t get_offset_of_vertices_5() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t801067801, ___vertices_5)); }
	inline Vector3U5BU5D_t215400611* get_vertices_5() const { return ___vertices_5; }
	inline Vector3U5BU5D_t215400611** get_address_of_vertices_5() { return &___vertices_5; }
	inline void set_vertices_5(Vector3U5BU5D_t215400611* value)
	{
		___vertices_5 = value;
		Il2CppCodeGenWriteBarrier(&___vertices_5, value);
	}

	inline static int32_t get_offset_of_normals_6() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t801067801, ___normals_6)); }
	inline Vector3U5BU5D_t215400611* get_normals_6() const { return ___normals_6; }
	inline Vector3U5BU5D_t215400611** get_address_of_normals_6() { return &___normals_6; }
	inline void set_normals_6(Vector3U5BU5D_t215400611* value)
	{
		___normals_6 = value;
		Il2CppCodeGenWriteBarrier(&___normals_6, value);
	}

	inline static int32_t get_offset_of_tangents_7() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t801067801, ___tangents_7)); }
	inline Vector4U5BU5D_t701588350* get_tangents_7() const { return ___tangents_7; }
	inline Vector4U5BU5D_t701588350** get_address_of_tangents_7() { return &___tangents_7; }
	inline void set_tangents_7(Vector4U5BU5D_t701588350* value)
	{
		___tangents_7 = value;
		Il2CppCodeGenWriteBarrier(&___tangents_7, value);
	}

	inline static int32_t get_offset_of_uvs0_8() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t801067801, ___uvs0_8)); }
	inline Vector2U5BU5D_t4024180168* get_uvs0_8() const { return ___uvs0_8; }
	inline Vector2U5BU5D_t4024180168** get_address_of_uvs0_8() { return &___uvs0_8; }
	inline void set_uvs0_8(Vector2U5BU5D_t4024180168* value)
	{
		___uvs0_8 = value;
		Il2CppCodeGenWriteBarrier(&___uvs0_8, value);
	}

	inline static int32_t get_offset_of_uvs2_9() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t801067801, ___uvs2_9)); }
	inline Vector2U5BU5D_t4024180168* get_uvs2_9() const { return ___uvs2_9; }
	inline Vector2U5BU5D_t4024180168** get_address_of_uvs2_9() { return &___uvs2_9; }
	inline void set_uvs2_9(Vector2U5BU5D_t4024180168* value)
	{
		___uvs2_9 = value;
		Il2CppCodeGenWriteBarrier(&___uvs2_9, value);
	}

	inline static int32_t get_offset_of_uvs4_10() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t801067801, ___uvs4_10)); }
	inline Vector2U5BU5D_t4024180168* get_uvs4_10() const { return ___uvs4_10; }
	inline Vector2U5BU5D_t4024180168** get_address_of_uvs4_10() { return &___uvs4_10; }
	inline void set_uvs4_10(Vector2U5BU5D_t4024180168* value)
	{
		___uvs4_10 = value;
		Il2CppCodeGenWriteBarrier(&___uvs4_10, value);
	}

	inline static int32_t get_offset_of_colors32_11() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t801067801, ___colors32_11)); }
	inline Color32U5BU5D_t2960766953* get_colors32_11() const { return ___colors32_11; }
	inline Color32U5BU5D_t2960766953** get_address_of_colors32_11() { return &___colors32_11; }
	inline void set_colors32_11(Color32U5BU5D_t2960766953* value)
	{
		___colors32_11 = value;
		Il2CppCodeGenWriteBarrier(&___colors32_11, value);
	}

	inline static int32_t get_offset_of_triangles_12() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t801067801, ___triangles_12)); }
	inline Int32U5BU5D_t3230847821* get_triangles_12() const { return ___triangles_12; }
	inline Int32U5BU5D_t3230847821** get_address_of_triangles_12() { return &___triangles_12; }
	inline void set_triangles_12(Int32U5BU5D_t3230847821* value)
	{
		___triangles_12 = value;
		Il2CppCodeGenWriteBarrier(&___triangles_12, value);
	}
};

struct TMP_MeshInfo_t801067801_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_MeshInfo::s_DefaultColor
	Color32_t598853688  ___s_DefaultColor_0;
	// UnityEngine.Vector3 TMPro.TMP_MeshInfo::s_DefaultNormal
	Vector3_t4282066566  ___s_DefaultNormal_1;
	// UnityEngine.Vector4 TMPro.TMP_MeshInfo::s_DefaultTangent
	Vector4_t4282066567  ___s_DefaultTangent_2;

public:
	inline static int32_t get_offset_of_s_DefaultColor_0() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t801067801_StaticFields, ___s_DefaultColor_0)); }
	inline Color32_t598853688  get_s_DefaultColor_0() const { return ___s_DefaultColor_0; }
	inline Color32_t598853688 * get_address_of_s_DefaultColor_0() { return &___s_DefaultColor_0; }
	inline void set_s_DefaultColor_0(Color32_t598853688  value)
	{
		___s_DefaultColor_0 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_1() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t801067801_StaticFields, ___s_DefaultNormal_1)); }
	inline Vector3_t4282066566  get_s_DefaultNormal_1() const { return ___s_DefaultNormal_1; }
	inline Vector3_t4282066566 * get_address_of_s_DefaultNormal_1() { return &___s_DefaultNormal_1; }
	inline void set_s_DefaultNormal_1(Vector3_t4282066566  value)
	{
		___s_DefaultNormal_1 = value;
	}

	inline static int32_t get_offset_of_s_DefaultTangent_2() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t801067801_StaticFields, ___s_DefaultTangent_2)); }
	inline Vector4_t4282066567  get_s_DefaultTangent_2() const { return ___s_DefaultTangent_2; }
	inline Vector4_t4282066567 * get_address_of_s_DefaultTangent_2() { return &___s_DefaultTangent_2; }
	inline void set_s_DefaultTangent_2(Vector4_t4282066567  value)
	{
		___s_DefaultTangent_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: TMPro.TMP_MeshInfo
struct TMP_MeshInfo_t801067801_marshaled_pinvoke
{
	Mesh_t4241756145 * ___mesh_3;
	int32_t ___vertexCount_4;
	Vector3_t4282066566_marshaled_pinvoke* ___vertices_5;
	Vector3_t4282066566_marshaled_pinvoke* ___normals_6;
	Vector4_t4282066567_marshaled_pinvoke* ___tangents_7;
	Vector2_t4282066565_marshaled_pinvoke* ___uvs0_8;
	Vector2_t4282066565_marshaled_pinvoke* ___uvs2_9;
	Vector2_t4282066565_marshaled_pinvoke* ___uvs4_10;
	Color32_t598853688_marshaled_pinvoke* ___colors32_11;
	int32_t* ___triangles_12;
};
// Native definition for marshalling of: TMPro.TMP_MeshInfo
struct TMP_MeshInfo_t801067801_marshaled_com
{
	Mesh_t4241756145 * ___mesh_3;
	int32_t ___vertexCount_4;
	Vector3_t4282066566_marshaled_com* ___vertices_5;
	Vector3_t4282066566_marshaled_com* ___normals_6;
	Vector4_t4282066567_marshaled_com* ___tangents_7;
	Vector2_t4282066565_marshaled_com* ___uvs0_8;
	Vector2_t4282066565_marshaled_com* ___uvs2_9;
	Vector2_t4282066565_marshaled_com* ___uvs4_10;
	Color32_t598853688_marshaled_com* ___colors32_11;
	int32_t* ___triangles_12;
};
