﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.InlineGraphicManager/<GetSpriteIndexByHashCode>c__AnonStorey29
struct U3CGetSpriteIndexByHashCodeU3Ec__AnonStorey29_t1993853662;
// TMPro.TMP_Sprite
struct TMP_Sprite_t3889423907;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_TMP_Sprite3889423907.h"

// System.Void TMPro.InlineGraphicManager/<GetSpriteIndexByHashCode>c__AnonStorey29::.ctor()
extern "C"  void U3CGetSpriteIndexByHashCodeU3Ec__AnonStorey29__ctor_m3026082173 (U3CGetSpriteIndexByHashCodeU3Ec__AnonStorey29_t1993853662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.InlineGraphicManager/<GetSpriteIndexByHashCode>c__AnonStorey29::<>m__7(TMPro.TMP_Sprite)
extern "C"  bool U3CGetSpriteIndexByHashCodeU3Ec__AnonStorey29_U3CU3Em__7_m2019819392 (U3CGetSpriteIndexByHashCodeU3Ec__AnonStorey29_t1993853662 * __this, TMP_Sprite_t3889423907 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
