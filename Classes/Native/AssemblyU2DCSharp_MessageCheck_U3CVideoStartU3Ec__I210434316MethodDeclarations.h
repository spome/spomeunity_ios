﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageCheck/<VideoStart>c__Iterator15
struct U3CVideoStartU3Ec__Iterator15_t210434316;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageCheck/<VideoStart>c__Iterator15::.ctor()
extern "C"  void U3CVideoStartU3Ec__Iterator15__ctor_m1597995279 (U3CVideoStartU3Ec__Iterator15_t210434316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageCheck/<VideoStart>c__Iterator15::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CVideoStartU3Ec__Iterator15_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1995219053 (U3CVideoStartU3Ec__Iterator15_t210434316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageCheck/<VideoStart>c__Iterator15::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CVideoStartU3Ec__Iterator15_System_Collections_IEnumerator_get_Current_m4114554369 (U3CVideoStartU3Ec__Iterator15_t210434316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MessageCheck/<VideoStart>c__Iterator15::MoveNext()
extern "C"  bool U3CVideoStartU3Ec__Iterator15_MoveNext_m1497679725 (U3CVideoStartU3Ec__Iterator15_t210434316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck/<VideoStart>c__Iterator15::Dispose()
extern "C"  void U3CVideoStartU3Ec__Iterator15_Dispose_m2268421516 (U3CVideoStartU3Ec__Iterator15_t210434316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck/<VideoStart>c__Iterator15::Reset()
extern "C"  void U3CVideoStartU3Ec__Iterator15_Reset_m3539395516 (U3CVideoStartU3Ec__Iterator15_t210434316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
