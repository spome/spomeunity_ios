﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PublicSpotMessage
struct  PublicSpotMessage_t3890192988  : public Il2CppObject
{
public:
	// System.String PublicSpotMessage::message_id
	String_t* ___message_id_0;
	// System.String PublicSpotMessage::type
	String_t* ___type_1;
	// System.String PublicSpotMessage::title
	String_t* ___title_2;
	// System.String PublicSpotMessage::realfilename
	String_t* ___realfilename_3;
	// System.String PublicSpotMessage::pref
	String_t* ___pref_4;
	// System.String PublicSpotMessage::addr01
	String_t* ___addr01_5;
	// System.String PublicSpotMessage::addr02
	String_t* ___addr02_6;
	// System.String PublicSpotMessage::latitude
	String_t* ___latitude_7;
	// System.String PublicSpotMessage::longtitude
	String_t* ___longtitude_8;
	// System.String PublicSpotMessage::create_data
	String_t* ___create_data_9;
	// System.Int32 PublicSpotMessage::messageindex
	int32_t ___messageindex_10;

public:
	inline static int32_t get_offset_of_message_id_0() { return static_cast<int32_t>(offsetof(PublicSpotMessage_t3890192988, ___message_id_0)); }
	inline String_t* get_message_id_0() const { return ___message_id_0; }
	inline String_t** get_address_of_message_id_0() { return &___message_id_0; }
	inline void set_message_id_0(String_t* value)
	{
		___message_id_0 = value;
		Il2CppCodeGenWriteBarrier(&___message_id_0, value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(PublicSpotMessage_t3890192988, ___type_1)); }
	inline String_t* get_type_1() const { return ___type_1; }
	inline String_t** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(String_t* value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier(&___type_1, value);
	}

	inline static int32_t get_offset_of_title_2() { return static_cast<int32_t>(offsetof(PublicSpotMessage_t3890192988, ___title_2)); }
	inline String_t* get_title_2() const { return ___title_2; }
	inline String_t** get_address_of_title_2() { return &___title_2; }
	inline void set_title_2(String_t* value)
	{
		___title_2 = value;
		Il2CppCodeGenWriteBarrier(&___title_2, value);
	}

	inline static int32_t get_offset_of_realfilename_3() { return static_cast<int32_t>(offsetof(PublicSpotMessage_t3890192988, ___realfilename_3)); }
	inline String_t* get_realfilename_3() const { return ___realfilename_3; }
	inline String_t** get_address_of_realfilename_3() { return &___realfilename_3; }
	inline void set_realfilename_3(String_t* value)
	{
		___realfilename_3 = value;
		Il2CppCodeGenWriteBarrier(&___realfilename_3, value);
	}

	inline static int32_t get_offset_of_pref_4() { return static_cast<int32_t>(offsetof(PublicSpotMessage_t3890192988, ___pref_4)); }
	inline String_t* get_pref_4() const { return ___pref_4; }
	inline String_t** get_address_of_pref_4() { return &___pref_4; }
	inline void set_pref_4(String_t* value)
	{
		___pref_4 = value;
		Il2CppCodeGenWriteBarrier(&___pref_4, value);
	}

	inline static int32_t get_offset_of_addr01_5() { return static_cast<int32_t>(offsetof(PublicSpotMessage_t3890192988, ___addr01_5)); }
	inline String_t* get_addr01_5() const { return ___addr01_5; }
	inline String_t** get_address_of_addr01_5() { return &___addr01_5; }
	inline void set_addr01_5(String_t* value)
	{
		___addr01_5 = value;
		Il2CppCodeGenWriteBarrier(&___addr01_5, value);
	}

	inline static int32_t get_offset_of_addr02_6() { return static_cast<int32_t>(offsetof(PublicSpotMessage_t3890192988, ___addr02_6)); }
	inline String_t* get_addr02_6() const { return ___addr02_6; }
	inline String_t** get_address_of_addr02_6() { return &___addr02_6; }
	inline void set_addr02_6(String_t* value)
	{
		___addr02_6 = value;
		Il2CppCodeGenWriteBarrier(&___addr02_6, value);
	}

	inline static int32_t get_offset_of_latitude_7() { return static_cast<int32_t>(offsetof(PublicSpotMessage_t3890192988, ___latitude_7)); }
	inline String_t* get_latitude_7() const { return ___latitude_7; }
	inline String_t** get_address_of_latitude_7() { return &___latitude_7; }
	inline void set_latitude_7(String_t* value)
	{
		___latitude_7 = value;
		Il2CppCodeGenWriteBarrier(&___latitude_7, value);
	}

	inline static int32_t get_offset_of_longtitude_8() { return static_cast<int32_t>(offsetof(PublicSpotMessage_t3890192988, ___longtitude_8)); }
	inline String_t* get_longtitude_8() const { return ___longtitude_8; }
	inline String_t** get_address_of_longtitude_8() { return &___longtitude_8; }
	inline void set_longtitude_8(String_t* value)
	{
		___longtitude_8 = value;
		Il2CppCodeGenWriteBarrier(&___longtitude_8, value);
	}

	inline static int32_t get_offset_of_create_data_9() { return static_cast<int32_t>(offsetof(PublicSpotMessage_t3890192988, ___create_data_9)); }
	inline String_t* get_create_data_9() const { return ___create_data_9; }
	inline String_t** get_address_of_create_data_9() { return &___create_data_9; }
	inline void set_create_data_9(String_t* value)
	{
		___create_data_9 = value;
		Il2CppCodeGenWriteBarrier(&___create_data_9, value);
	}

	inline static int32_t get_offset_of_messageindex_10() { return static_cast<int32_t>(offsetof(PublicSpotMessage_t3890192988, ___messageindex_10)); }
	inline int32_t get_messageindex_10() const { return ___messageindex_10; }
	inline int32_t* get_address_of_messageindex_10() { return &___messageindex_10; }
	inline void set_messageindex_10(int32_t value)
	{
		___messageindex_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
