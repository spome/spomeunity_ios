﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t3199167241;

#include "AssemblyU2DCSharp_TMPro_TMP_TextElement1232249705.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Sprite
struct  TMP_Sprite_t3889423907  : public TMP_TextElement_t1232249705
{
public:
	// System.String TMPro.TMP_Sprite::name
	String_t* ___name_8;
	// System.Int32 TMPro.TMP_Sprite::hashCode
	int32_t ___hashCode_9;
	// UnityEngine.Vector2 TMPro.TMP_Sprite::pivot
	Vector2_t4282066565  ___pivot_10;
	// System.Single TMPro.TMP_Sprite::scale
	float ___scale_11;
	// UnityEngine.Sprite TMPro.TMP_Sprite::sprite
	Sprite_t3199167241 * ___sprite_12;

public:
	inline static int32_t get_offset_of_name_8() { return static_cast<int32_t>(offsetof(TMP_Sprite_t3889423907, ___name_8)); }
	inline String_t* get_name_8() const { return ___name_8; }
	inline String_t** get_address_of_name_8() { return &___name_8; }
	inline void set_name_8(String_t* value)
	{
		___name_8 = value;
		Il2CppCodeGenWriteBarrier(&___name_8, value);
	}

	inline static int32_t get_offset_of_hashCode_9() { return static_cast<int32_t>(offsetof(TMP_Sprite_t3889423907, ___hashCode_9)); }
	inline int32_t get_hashCode_9() const { return ___hashCode_9; }
	inline int32_t* get_address_of_hashCode_9() { return &___hashCode_9; }
	inline void set_hashCode_9(int32_t value)
	{
		___hashCode_9 = value;
	}

	inline static int32_t get_offset_of_pivot_10() { return static_cast<int32_t>(offsetof(TMP_Sprite_t3889423907, ___pivot_10)); }
	inline Vector2_t4282066565  get_pivot_10() const { return ___pivot_10; }
	inline Vector2_t4282066565 * get_address_of_pivot_10() { return &___pivot_10; }
	inline void set_pivot_10(Vector2_t4282066565  value)
	{
		___pivot_10 = value;
	}

	inline static int32_t get_offset_of_scale_11() { return static_cast<int32_t>(offsetof(TMP_Sprite_t3889423907, ___scale_11)); }
	inline float get_scale_11() const { return ___scale_11; }
	inline float* get_address_of_scale_11() { return &___scale_11; }
	inline void set_scale_11(float value)
	{
		___scale_11 = value;
	}

	inline static int32_t get_offset_of_sprite_12() { return static_cast<int32_t>(offsetof(TMP_Sprite_t3889423907, ___sprite_12)); }
	inline Sprite_t3199167241 * get_sprite_12() const { return ___sprite_12; }
	inline Sprite_t3199167241 ** get_address_of_sprite_12() { return &___sprite_12; }
	inline void set_sprite_12(Sprite_t3199167241 * value)
	{
		___sprite_12 = value;
		Il2CppCodeGenWriteBarrier(&___sprite_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
