﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProgressBar.Utils.FillerProperty
struct FillerProperty_t3769378557;

#include "codegen/il2cpp-codegen.h"

// System.Void ProgressBar.Utils.FillerProperty::.ctor(System.Single,System.Single)
extern "C"  void FillerProperty__ctor_m955200959 (FillerProperty_t3769378557 * __this, float ___Min0, float ___Max1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
