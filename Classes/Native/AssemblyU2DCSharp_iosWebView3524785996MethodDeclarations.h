﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iosWebView
struct iosWebView_t3524785996;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void iosWebView::.ctor()
extern "C"  void iosWebView__ctor_m2508658383 (iosWebView_t3524785996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iosWebView::.cctor()
extern "C"  void iosWebView__cctor_m4271869630 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// iosWebView iosWebView::get_Instance()
extern "C"  iosWebView_t3524785996 * iosWebView_get_Instance_m3613006564 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iosWebView::Awake()
extern "C"  void iosWebView_Awake_m2746263602 (iosWebView_t3524785996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iosWebView::Act(System.String)
extern "C"  void iosWebView_Act_m2518930019 (iosWebView_t3524785996 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iosWebView::WebBack()
extern "C"  void iosWebView_WebBack_m3641098376 (iosWebView_t3524785996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iosWebView::<Act>m__6(System.String)
extern "C"  void iosWebView_U3CActU3Em__6_m1814597860 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
