﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t3134621005;
// System.Object
struct Il2CppObject;
// EmailRecv
struct EmailRecv_t4124314498;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EmailRecv/<GetEmailList>c__IteratorB
struct  U3CGetEmailListU3Ec__IteratorB_t1941116220  : public Il2CppObject
{
public:
	// UnityEngine.WWW EmailRecv/<GetEmailList>c__IteratorB::<www>__0
	WWW_t3134621005 * ___U3CwwwU3E__0_0;
	// System.Int32 EmailRecv/<GetEmailList>c__IteratorB::$PC
	int32_t ___U24PC_1;
	// System.Object EmailRecv/<GetEmailList>c__IteratorB::$current
	Il2CppObject * ___U24current_2;
	// EmailRecv EmailRecv/<GetEmailList>c__IteratorB::<>f__this
	EmailRecv_t4124314498 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetEmailListU3Ec__IteratorB_t1941116220, ___U3CwwwU3E__0_0)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CGetEmailListU3Ec__IteratorB_t1941116220, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetEmailListU3Ec__IteratorB_t1941116220, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CGetEmailListU3Ec__IteratorB_t1941116220, ___U3CU3Ef__this_3)); }
	inline EmailRecv_t4124314498 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline EmailRecv_t4124314498 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(EmailRecv_t4124314498 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
