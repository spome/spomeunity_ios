﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ErrorMessage/PositiveButtonListner
struct PositiveButtonListner_t2125458946;
// ErrorMessage
struct ErrorMessage_t1367556351;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ErrorMessage1367556351.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject2362096582.h"

// System.Void ErrorMessage/PositiveButtonListner::.ctor(ErrorMessage)
extern "C"  void PositiveButtonListner__ctor_m3471104954 (PositiveButtonListner_t2125458946 * __this, ErrorMessage_t1367556351 * ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessage/PositiveButtonListner::onClick(UnityEngine.AndroidJavaObject,System.Int32)
extern "C"  void PositiveButtonListner_onClick_m2775208798 (PositiveButtonListner_t2125458946 * __this, AndroidJavaObject_t2362096582 * ___obj0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
