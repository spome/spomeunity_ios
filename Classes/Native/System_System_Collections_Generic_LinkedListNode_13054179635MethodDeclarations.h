﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedListNode_13787551078MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedListNode`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
#define LinkedListNode_1__ctor_m1315986257(__this, ___list0, ___value1, method) ((  void (*) (LinkedListNode_1_t3054179635 *, LinkedList_1_t1312080097 *, Action_2_t3437444928 *, const MethodInfo*))LinkedListNode_1__ctor_m648136130_gshared)(__this, ___list0, ___value1, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedListNode_1__ctor_m413901425(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method) ((  void (*) (LinkedListNode_1_t3054179635 *, LinkedList_1_t1312080097 *, Action_2_t3437444928 *, LinkedListNode_1_t3054179635 *, LinkedListNode_1_t3054179635 *, const MethodInfo*))LinkedListNode_1__ctor_m448391458_gshared)(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::Detach()
#define LinkedListNode_1_Detach_m805446959(__this, method) ((  void (*) (LinkedListNode_1_t3054179635 *, const MethodInfo*))LinkedListNode_1_Detach_m3406254942_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::get_List()
#define LinkedListNode_1_get_List_m4070831923(__this, method) ((  LinkedList_1_t1312080097 * (*) (LinkedListNode_1_t3054179635 *, const MethodInfo*))LinkedListNode_1_get_List_m3467110818_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::get_Next()
#define LinkedListNode_1_get_Next_m356638698(__this, method) ((  LinkedListNode_1_t3054179635 * (*) (LinkedListNode_1_t3054179635 *, const MethodInfo*))LinkedListNode_1_get_Next_m1427618777_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<System.Action`2<System.Boolean,TMPro.TMP_FontAsset>>::get_Value()
#define LinkedListNode_1_get_Value_m3422304047(__this, method) ((  Action_2_t3437444928 * (*) (LinkedListNode_1_t3054179635 *, const MethodInfo*))LinkedListNode_1_get_Value_m702633824_gshared)(__this, method)
