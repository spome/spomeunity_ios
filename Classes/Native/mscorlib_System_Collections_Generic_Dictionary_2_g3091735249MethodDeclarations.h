﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>
struct Dictionary_2_t3091735249;
// System.Collections.Generic.IEqualityComparer`1<CurrentState/States>
struct IEqualityComparer_1_t3349047165;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<CurrentState/States,System.Object>[]
struct KeyValuePair_2U5BU5D_t1634179042;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<CurrentState/States,System.Object>>
struct IEnumerator_1_t607413708;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,System.Object>
struct ValueCollection_t1792340962;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22990515955.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_CurrentState_States2558012761.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En114091345.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m1947495226_gshared (Dictionary_2_t3091735249 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1947495226(__this, method) ((  void (*) (Dictionary_2_t3091735249 *, const MethodInfo*))Dictionary_2__ctor_m1947495226_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2932095153_gshared (Dictionary_2_t3091735249 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2932095153(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3091735249 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2932095153_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3658029451_gshared (Dictionary_2_t3091735249 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3658029451(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3091735249 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3658029451_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1016635771_gshared (Dictionary_2_t3091735249 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m1016635771(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3091735249 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m1016635771_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3819194446_gshared (Dictionary_2_t3091735249 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3819194446(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3091735249 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3819194446_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m2752026675_gshared (Dictionary_2_t3091735249 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m2752026675(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3091735249 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2752026675_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1175851582_gshared (Dictionary_2_t3091735249 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1175851582(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3091735249 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1175851582_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2841630833_gshared (Dictionary_2_t3091735249 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2841630833(__this, ___key0, method) ((  void (*) (Dictionary_2_t3091735249 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2841630833_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2812859100_gshared (Dictionary_2_t3091735249 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2812859100(__this, method) ((  bool (*) (Dictionary_2_t3091735249 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2812859100_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3360493448_gshared (Dictionary_2_t3091735249 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3360493448(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3091735249 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3360493448_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2327724064_gshared (Dictionary_2_t3091735249 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2327724064(__this, method) ((  bool (*) (Dictionary_2_t3091735249 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2327724064_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m932960391_gshared (Dictionary_2_t3091735249 * __this, KeyValuePair_2_t2990515955  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m932960391(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3091735249 *, KeyValuePair_2_t2990515955 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m932960391_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4060905627_gshared (Dictionary_2_t3091735249 * __this, KeyValuePair_2_t2990515955  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4060905627(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3091735249 *, KeyValuePair_2_t2990515955 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4060905627_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2873906347_gshared (Dictionary_2_t3091735249 * __this, KeyValuePair_2U5BU5D_t1634179042* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2873906347(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3091735249 *, KeyValuePair_2U5BU5D_t1634179042*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2873906347_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3026719360_gshared (Dictionary_2_t3091735249 * __this, KeyValuePair_2_t2990515955  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3026719360(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3091735249 *, KeyValuePair_2_t2990515955 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3026719360_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2887405386_gshared (Dictionary_2_t3091735249 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2887405386(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3091735249 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m2887405386_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m769430341_gshared (Dictionary_2_t3091735249 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m769430341(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3091735249 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m769430341_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3203281730_gshared (Dictionary_2_t3091735249 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3203281730(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3091735249 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3203281730_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m485898077_gshared (Dictionary_2_t3091735249 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m485898077(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3091735249 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m485898077_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m1517578914_gshared (Dictionary_2_t3091735249 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1517578914(__this, method) ((  int32_t (*) (Dictionary_2_t3091735249 *, const MethodInfo*))Dictionary_2_get_Count_m1517578914_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m526969097_gshared (Dictionary_2_t3091735249 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m526969097(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3091735249 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m526969097_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m2988929914_gshared (Dictionary_2_t3091735249 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m2988929914(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3091735249 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m2988929914_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3704779442_gshared (Dictionary_2_t3091735249 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3704779442(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3091735249 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3704779442_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3212200773_gshared (Dictionary_2_t3091735249 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3212200773(__this, ___size0, method) ((  void (*) (Dictionary_2_t3091735249 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3212200773_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3300756097_gshared (Dictionary_2_t3091735249 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3300756097(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3091735249 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3300756097_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t2990515955  Dictionary_2_make_pair_m4166768333_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m4166768333(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2990515955  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m4166768333_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m1006115209_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1006115209(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m1006115209_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2490067502_gshared (Dictionary_2_t3091735249 * __this, KeyValuePair_2U5BU5D_t1634179042* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m2490067502(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3091735249 *, KeyValuePair_2U5BU5D_t1634179042*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2490067502_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m300606526_gshared (Dictionary_2_t3091735249 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m300606526(__this, method) ((  void (*) (Dictionary_2_t3091735249 *, const MethodInfo*))Dictionary_2_Resize_m300606526_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1499384507_gshared (Dictionary_2_t3091735249 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1499384507(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3091735249 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m1499384507_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m3648595813_gshared (Dictionary_2_t3091735249 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3648595813(__this, method) ((  void (*) (Dictionary_2_t3091735249 *, const MethodInfo*))Dictionary_2_Clear_m3648595813_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3914963851_gshared (Dictionary_2_t3091735249 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3914963851(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3091735249 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m3914963851_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m420885003_gshared (Dictionary_2_t3091735249 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m420885003(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3091735249 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m420885003_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3951705560_gshared (Dictionary_2_t3091735249 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3951705560(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3091735249 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m3951705560_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1735043980_gshared (Dictionary_2_t3091735249 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1735043980(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3091735249 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1735043980_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2725270149_gshared (Dictionary_2_t3091735249 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2725270149(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3091735249 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m2725270149_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2151889124_gshared (Dictionary_2_t3091735249 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2151889124(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3091735249 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m2151889124_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::get_Values()
extern "C"  ValueCollection_t1792340962 * Dictionary_2_get_Values_m395799771_gshared (Dictionary_2_t3091735249 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m395799771(__this, method) ((  ValueCollection_t1792340962 * (*) (Dictionary_2_t3091735249 *, const MethodInfo*))Dictionary_2_get_Values_m395799771_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m51756516_gshared (Dictionary_2_t3091735249 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m51756516(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3091735249 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m51756516_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m3613396644_gshared (Dictionary_2_t3091735249 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m3613396644(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t3091735249 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3613396644_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m494590056_gshared (Dictionary_2_t3091735249 * __this, KeyValuePair_2_t2990515955  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m494590056(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3091735249 *, KeyValuePair_2_t2990515955 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m494590056_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::GetEnumerator()
extern "C"  Enumerator_t114091345  Dictionary_2_GetEnumerator_m3695924095_gshared (Dictionary_2_t3091735249 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m3695924095(__this, method) ((  Enumerator_t114091345  (*) (Dictionary_2_t3091735249 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3695924095_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m1001023502_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1001023502(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1001023502_gshared)(__this /* static, unused */, ___key0, ___value1, method)
