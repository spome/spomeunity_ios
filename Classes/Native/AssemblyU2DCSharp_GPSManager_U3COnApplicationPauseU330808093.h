﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GPSManager/<OnApplicationPause>c__IteratorE
struct  U3COnApplicationPauseU3Ec__IteratorE_t330808093  : public Il2CppObject
{
public:
	// System.Boolean GPSManager/<OnApplicationPause>c__IteratorE::pauseState
	bool ___pauseState_0;
	// System.Int32 GPSManager/<OnApplicationPause>c__IteratorE::<waitTime>__0
	int32_t ___U3CwaitTimeU3E__0_1;
	// System.Int32 GPSManager/<OnApplicationPause>c__IteratorE::$PC
	int32_t ___U24PC_2;
	// System.Object GPSManager/<OnApplicationPause>c__IteratorE::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean GPSManager/<OnApplicationPause>c__IteratorE::<$>pauseState
	bool ___U3CU24U3EpauseState_4;

public:
	inline static int32_t get_offset_of_pauseState_0() { return static_cast<int32_t>(offsetof(U3COnApplicationPauseU3Ec__IteratorE_t330808093, ___pauseState_0)); }
	inline bool get_pauseState_0() const { return ___pauseState_0; }
	inline bool* get_address_of_pauseState_0() { return &___pauseState_0; }
	inline void set_pauseState_0(bool value)
	{
		___pauseState_0 = value;
	}

	inline static int32_t get_offset_of_U3CwaitTimeU3E__0_1() { return static_cast<int32_t>(offsetof(U3COnApplicationPauseU3Ec__IteratorE_t330808093, ___U3CwaitTimeU3E__0_1)); }
	inline int32_t get_U3CwaitTimeU3E__0_1() const { return ___U3CwaitTimeU3E__0_1; }
	inline int32_t* get_address_of_U3CwaitTimeU3E__0_1() { return &___U3CwaitTimeU3E__0_1; }
	inline void set_U3CwaitTimeU3E__0_1(int32_t value)
	{
		___U3CwaitTimeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3COnApplicationPauseU3Ec__IteratorE_t330808093, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3COnApplicationPauseU3Ec__IteratorE_t330808093, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EpauseState_4() { return static_cast<int32_t>(offsetof(U3COnApplicationPauseU3Ec__IteratorE_t330808093, ___U3CU24U3EpauseState_4)); }
	inline bool get_U3CU24U3EpauseState_4() const { return ___U3CU24U3EpauseState_4; }
	inline bool* get_address_of_U3CU24U3EpauseState_4() { return &___U3CU24U3EpauseState_4; }
	inline void set_U3CU24U3EpauseState_4(bool value)
	{
		___U3CU24U3EpauseState_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
