﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_TMP_XmlTagStack_1_gen1808196333.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"

// System.Void TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>::.ctor(T[])
extern "C"  void TMP_XmlTagStack_1__ctor_m1407720700_gshared (TMP_XmlTagStack_1_t1808196333 * __this, Color32U5BU5D_t2960766953* ___tagStack0, const MethodInfo* method);
#define TMP_XmlTagStack_1__ctor_m1407720700(__this, ___tagStack0, method) ((  void (*) (TMP_XmlTagStack_1_t1808196333 *, Color32U5BU5D_t2960766953*, const MethodInfo*))TMP_XmlTagStack_1__ctor_m1407720700_gshared)(__this, ___tagStack0, method)
// System.Void TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>::Clear()
extern "C"  void TMP_XmlTagStack_1_Clear_m3856632875_gshared (TMP_XmlTagStack_1_t1808196333 * __this, const MethodInfo* method);
#define TMP_XmlTagStack_1_Clear_m3856632875(__this, method) ((  void (*) (TMP_XmlTagStack_1_t1808196333 *, const MethodInfo*))TMP_XmlTagStack_1_Clear_m3856632875_gshared)(__this, method)
// System.Void TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>::SetDefault(T)
extern "C"  void TMP_XmlTagStack_1_SetDefault_m1314292475_gshared (TMP_XmlTagStack_1_t1808196333 * __this, Color32_t598853688  ___item0, const MethodInfo* method);
#define TMP_XmlTagStack_1_SetDefault_m1314292475(__this, ___item0, method) ((  void (*) (TMP_XmlTagStack_1_t1808196333 *, Color32_t598853688 , const MethodInfo*))TMP_XmlTagStack_1_SetDefault_m1314292475_gshared)(__this, ___item0, method)
// System.Void TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>::Add(T)
extern "C"  void TMP_XmlTagStack_1_Add_m3523358847_gshared (TMP_XmlTagStack_1_t1808196333 * __this, Color32_t598853688  ___item0, const MethodInfo* method);
#define TMP_XmlTagStack_1_Add_m3523358847(__this, ___item0, method) ((  void (*) (TMP_XmlTagStack_1_t1808196333 *, Color32_t598853688 , const MethodInfo*))TMP_XmlTagStack_1_Add_m3523358847_gshared)(__this, ___item0, method)
// T TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>::Remove()
extern "C"  Color32_t598853688  TMP_XmlTagStack_1_Remove_m3675256391_gshared (TMP_XmlTagStack_1_t1808196333 * __this, const MethodInfo* method);
#define TMP_XmlTagStack_1_Remove_m3675256391(__this, method) ((  Color32_t598853688  (*) (TMP_XmlTagStack_1_t1808196333 *, const MethodInfo*))TMP_XmlTagStack_1_Remove_m3675256391_gshared)(__this, method)
