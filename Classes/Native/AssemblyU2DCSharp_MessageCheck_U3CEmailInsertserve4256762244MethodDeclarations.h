﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageCheck/<EmailInsertserver>c__Iterator18
struct U3CEmailInsertserverU3Ec__Iterator18_t4256762244;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageCheck/<EmailInsertserver>c__Iterator18::.ctor()
extern "C"  void U3CEmailInsertserverU3Ec__Iterator18__ctor_m3927714919 (U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageCheck/<EmailInsertserver>c__Iterator18::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEmailInsertserverU3Ec__Iterator18_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m705126923 (U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageCheck/<EmailInsertserver>c__Iterator18::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEmailInsertserverU3Ec__Iterator18_System_Collections_IEnumerator_get_Current_m3499940767 (U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MessageCheck/<EmailInsertserver>c__Iterator18::MoveNext()
extern "C"  bool U3CEmailInsertserverU3Ec__Iterator18_MoveNext_m86372141 (U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck/<EmailInsertserver>c__Iterator18::Dispose()
extern "C"  void U3CEmailInsertserverU3Ec__Iterator18_Dispose_m3451034340 (U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck/<EmailInsertserver>c__Iterator18::Reset()
extern "C"  void U3CEmailInsertserverU3Ec__Iterator18_Reset_m1574147860 (U3CEmailInsertserverU3Ec__Iterator18_t4256762244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
