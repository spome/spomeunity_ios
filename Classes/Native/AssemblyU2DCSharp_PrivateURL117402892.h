﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PrivateURL
struct  PrivateURL_t117402892  : public Il2CppObject
{
public:

public:
};

struct PrivateURL_t117402892_StaticFields
{
public:
	// System.String PrivateURL::_id
	String_t* ____id_22;
	// System.String PrivateURL::_pw
	String_t* ____pw_23;
	// System.String PrivateURL::_uniquekey
	String_t* ____uniquekey_24;
	// System.String PrivateURL::_customerId
	String_t* ____customerId_25;
	// System.String PrivateURL::_userloginemail
	String_t* ____userloginemail_26;
	// System.String PrivateURL::_selectmessageid
	String_t* ____selectmessageid_27;

public:
	inline static int32_t get_offset_of__id_22() { return static_cast<int32_t>(offsetof(PrivateURL_t117402892_StaticFields, ____id_22)); }
	inline String_t* get__id_22() const { return ____id_22; }
	inline String_t** get_address_of__id_22() { return &____id_22; }
	inline void set__id_22(String_t* value)
	{
		____id_22 = value;
		Il2CppCodeGenWriteBarrier(&____id_22, value);
	}

	inline static int32_t get_offset_of__pw_23() { return static_cast<int32_t>(offsetof(PrivateURL_t117402892_StaticFields, ____pw_23)); }
	inline String_t* get__pw_23() const { return ____pw_23; }
	inline String_t** get_address_of__pw_23() { return &____pw_23; }
	inline void set__pw_23(String_t* value)
	{
		____pw_23 = value;
		Il2CppCodeGenWriteBarrier(&____pw_23, value);
	}

	inline static int32_t get_offset_of__uniquekey_24() { return static_cast<int32_t>(offsetof(PrivateURL_t117402892_StaticFields, ____uniquekey_24)); }
	inline String_t* get__uniquekey_24() const { return ____uniquekey_24; }
	inline String_t** get_address_of__uniquekey_24() { return &____uniquekey_24; }
	inline void set__uniquekey_24(String_t* value)
	{
		____uniquekey_24 = value;
		Il2CppCodeGenWriteBarrier(&____uniquekey_24, value);
	}

	inline static int32_t get_offset_of__customerId_25() { return static_cast<int32_t>(offsetof(PrivateURL_t117402892_StaticFields, ____customerId_25)); }
	inline String_t* get__customerId_25() const { return ____customerId_25; }
	inline String_t** get_address_of__customerId_25() { return &____customerId_25; }
	inline void set__customerId_25(String_t* value)
	{
		____customerId_25 = value;
		Il2CppCodeGenWriteBarrier(&____customerId_25, value);
	}

	inline static int32_t get_offset_of__userloginemail_26() { return static_cast<int32_t>(offsetof(PrivateURL_t117402892_StaticFields, ____userloginemail_26)); }
	inline String_t* get__userloginemail_26() const { return ____userloginemail_26; }
	inline String_t** get_address_of__userloginemail_26() { return &____userloginemail_26; }
	inline void set__userloginemail_26(String_t* value)
	{
		____userloginemail_26 = value;
		Il2CppCodeGenWriteBarrier(&____userloginemail_26, value);
	}

	inline static int32_t get_offset_of__selectmessageid_27() { return static_cast<int32_t>(offsetof(PrivateURL_t117402892_StaticFields, ____selectmessageid_27)); }
	inline String_t* get__selectmessageid_27() const { return ____selectmessageid_27; }
	inline String_t** get_address_of__selectmessageid_27() { return &____selectmessageid_27; }
	inline void set__selectmessageid_27(String_t* value)
	{
		____selectmessageid_27 = value;
		Il2CppCodeGenWriteBarrier(&____selectmessageid_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
