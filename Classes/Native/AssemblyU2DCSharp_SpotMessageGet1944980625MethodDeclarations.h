﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpotMessageGet
struct SpotMessageGet_t1944980625;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "mscorlib_System_String7231557.h"

// System.Void SpotMessageGet::.ctor()
extern "C"  void SpotMessageGet__ctor_m1288590122 (SpotMessageGet_t1944980625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpotMessageGet::OnEnable()
extern "C"  void SpotMessageGet_OnEnable_m2042211356 (SpotMessageGet_t1944980625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SpotMessageGet::MessageGetJsonStart()
extern "C"  Il2CppObject * SpotMessageGet_MessageGetJsonStart_m827736907 (SpotMessageGet_t1944980625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SpotMessageGet::GreatManCheckProgress(UnityEngine.WWW)
extern "C"  Il2CppObject * SpotMessageGet_GreatManCheckProgress_m3593636778 (SpotMessageGet_t1944980625 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpotMessageGet::MessageGetTakeJson(System.String)
extern "C"  void SpotMessageGet_MessageGetTakeJson_m763779946 (SpotMessageGet_t1944980625 * __this, String_t* ___js0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpotMessageGet::OnDisable()
extern "C"  void SpotMessageGet_OnDisable_m3619947153 (SpotMessageGet_t1944980625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
