﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.TagAttribute
struct TagAttribute_t1331787960;
struct TagAttribute_t1331787960_marshaled_pinvoke;
struct TagAttribute_t1331787960_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct TagAttribute_t1331787960;
struct TagAttribute_t1331787960_marshaled_pinvoke;

extern "C" void TagAttribute_t1331787960_marshal_pinvoke(const TagAttribute_t1331787960& unmarshaled, TagAttribute_t1331787960_marshaled_pinvoke& marshaled);
extern "C" void TagAttribute_t1331787960_marshal_pinvoke_back(const TagAttribute_t1331787960_marshaled_pinvoke& marshaled, TagAttribute_t1331787960& unmarshaled);
extern "C" void TagAttribute_t1331787960_marshal_pinvoke_cleanup(TagAttribute_t1331787960_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct TagAttribute_t1331787960;
struct TagAttribute_t1331787960_marshaled_com;

extern "C" void TagAttribute_t1331787960_marshal_com(const TagAttribute_t1331787960& unmarshaled, TagAttribute_t1331787960_marshaled_com& marshaled);
extern "C" void TagAttribute_t1331787960_marshal_com_back(const TagAttribute_t1331787960_marshaled_com& marshaled, TagAttribute_t1331787960& unmarshaled);
extern "C" void TagAttribute_t1331787960_marshal_com_cleanup(TagAttribute_t1331787960_marshaled_com& marshaled);
