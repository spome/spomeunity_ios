﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LitJson.JsonData
struct JsonData_t1715015430;
// EmailRecv
struct EmailRecv_t4124314498;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EmailRecv/<TakeJson>c__AnonStorey23
struct  U3CTakeJsonU3Ec__AnonStorey23_t1761236520  : public Il2CppObject
{
public:
	// LitJson.JsonData EmailRecv/<TakeJson>c__AnonStorey23::EventJS
	JsonData_t1715015430 * ___EventJS_0;
	// EmailRecv EmailRecv/<TakeJson>c__AnonStorey23::<>f__this
	EmailRecv_t4124314498 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_EventJS_0() { return static_cast<int32_t>(offsetof(U3CTakeJsonU3Ec__AnonStorey23_t1761236520, ___EventJS_0)); }
	inline JsonData_t1715015430 * get_EventJS_0() const { return ___EventJS_0; }
	inline JsonData_t1715015430 ** get_address_of_EventJS_0() { return &___EventJS_0; }
	inline void set_EventJS_0(JsonData_t1715015430 * value)
	{
		___EventJS_0 = value;
		Il2CppCodeGenWriteBarrier(&___EventJS_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CTakeJsonU3Ec__AnonStorey23_t1761236520, ___U3CU3Ef__this_1)); }
	inline EmailRecv_t4124314498 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline EmailRecv_t4124314498 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(EmailRecv_t4124314498 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
