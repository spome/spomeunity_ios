﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_TMPro_TextAlignmentOptions3798547742.h"
#include "AssemblyU2DCSharp_TMPro_Extents2060714539.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_t2462355872 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_1;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_2;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_3;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_4;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_6;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_7;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_8;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_9;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_10;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_11;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_12;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_13;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_14;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_15;
	// System.Single TMPro.TMP_LineInfo::maxScale
	float ___maxScale_16;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_17;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_t2060714539  ___lineExtents_18;

public:
	inline static int32_t get_offset_of_characterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___characterCount_0)); }
	inline int32_t get_characterCount_0() const { return ___characterCount_0; }
	inline int32_t* get_address_of_characterCount_0() { return &___characterCount_0; }
	inline void set_characterCount_0(int32_t value)
	{
		___characterCount_0 = value;
	}

	inline static int32_t get_offset_of_spaceCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___spaceCount_1)); }
	inline int32_t get_spaceCount_1() const { return ___spaceCount_1; }
	inline int32_t* get_address_of_spaceCount_1() { return &___spaceCount_1; }
	inline void set_spaceCount_1(int32_t value)
	{
		___spaceCount_1 = value;
	}

	inline static int32_t get_offset_of_wordCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___wordCount_2)); }
	inline int32_t get_wordCount_2() const { return ___wordCount_2; }
	inline int32_t* get_address_of_wordCount_2() { return &___wordCount_2; }
	inline void set_wordCount_2(int32_t value)
	{
		___wordCount_2 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___firstCharacterIndex_3)); }
	inline int32_t get_firstCharacterIndex_3() const { return ___firstCharacterIndex_3; }
	inline int32_t* get_address_of_firstCharacterIndex_3() { return &___firstCharacterIndex_3; }
	inline void set_firstCharacterIndex_3(int32_t value)
	{
		___firstCharacterIndex_3 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___firstVisibleCharacterIndex_4)); }
	inline int32_t get_firstVisibleCharacterIndex_4() const { return ___firstVisibleCharacterIndex_4; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_4() { return &___firstVisibleCharacterIndex_4; }
	inline void set_firstVisibleCharacterIndex_4(int32_t value)
	{
		___firstVisibleCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___lastCharacterIndex_5)); }
	inline int32_t get_lastCharacterIndex_5() const { return ___lastCharacterIndex_5; }
	inline int32_t* get_address_of_lastCharacterIndex_5() { return &___lastCharacterIndex_5; }
	inline void set_lastCharacterIndex_5(int32_t value)
	{
		___lastCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___lastVisibleCharacterIndex_6)); }
	inline int32_t get_lastVisibleCharacterIndex_6() const { return ___lastVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_6() { return &___lastVisibleCharacterIndex_6; }
	inline void set_lastVisibleCharacterIndex_6(int32_t value)
	{
		___lastVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_length_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___length_7)); }
	inline float get_length_7() const { return ___length_7; }
	inline float* get_address_of_length_7() { return &___length_7; }
	inline void set_length_7(float value)
	{
		___length_7 = value;
	}

	inline static int32_t get_offset_of_lineHeight_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___lineHeight_8)); }
	inline float get_lineHeight_8() const { return ___lineHeight_8; }
	inline float* get_address_of_lineHeight_8() { return &___lineHeight_8; }
	inline void set_lineHeight_8(float value)
	{
		___lineHeight_8 = value;
	}

	inline static int32_t get_offset_of_ascender_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___ascender_9)); }
	inline float get_ascender_9() const { return ___ascender_9; }
	inline float* get_address_of_ascender_9() { return &___ascender_9; }
	inline void set_ascender_9(float value)
	{
		___ascender_9 = value;
	}

	inline static int32_t get_offset_of_baseline_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___baseline_10)); }
	inline float get_baseline_10() const { return ___baseline_10; }
	inline float* get_address_of_baseline_10() { return &___baseline_10; }
	inline void set_baseline_10(float value)
	{
		___baseline_10 = value;
	}

	inline static int32_t get_offset_of_descender_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___descender_11)); }
	inline float get_descender_11() const { return ___descender_11; }
	inline float* get_address_of_descender_11() { return &___descender_11; }
	inline void set_descender_11(float value)
	{
		___descender_11 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___maxAdvance_12)); }
	inline float get_maxAdvance_12() const { return ___maxAdvance_12; }
	inline float* get_address_of_maxAdvance_12() { return &___maxAdvance_12; }
	inline void set_maxAdvance_12(float value)
	{
		___maxAdvance_12 = value;
	}

	inline static int32_t get_offset_of_width_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___width_13)); }
	inline float get_width_13() const { return ___width_13; }
	inline float* get_address_of_width_13() { return &___width_13; }
	inline void set_width_13(float value)
	{
		___width_13 = value;
	}

	inline static int32_t get_offset_of_marginLeft_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___marginLeft_14)); }
	inline float get_marginLeft_14() const { return ___marginLeft_14; }
	inline float* get_address_of_marginLeft_14() { return &___marginLeft_14; }
	inline void set_marginLeft_14(float value)
	{
		___marginLeft_14 = value;
	}

	inline static int32_t get_offset_of_marginRight_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___marginRight_15)); }
	inline float get_marginRight_15() const { return ___marginRight_15; }
	inline float* get_address_of_marginRight_15() { return &___marginRight_15; }
	inline void set_marginRight_15(float value)
	{
		___marginRight_15 = value;
	}

	inline static int32_t get_offset_of_maxScale_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___maxScale_16)); }
	inline float get_maxScale_16() const { return ___maxScale_16; }
	inline float* get_address_of_maxScale_16() { return &___maxScale_16; }
	inline void set_maxScale_16(float value)
	{
		___maxScale_16 = value;
	}

	inline static int32_t get_offset_of_alignment_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___alignment_17)); }
	inline int32_t get_alignment_17() const { return ___alignment_17; }
	inline int32_t* get_address_of_alignment_17() { return &___alignment_17; }
	inline void set_alignment_17(int32_t value)
	{
		___alignment_17 = value;
	}

	inline static int32_t get_offset_of_lineExtents_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2462355872, ___lineExtents_18)); }
	inline Extents_t2060714539  get_lineExtents_18() const { return ___lineExtents_18; }
	inline Extents_t2060714539 * get_address_of_lineExtents_18() { return &___lineExtents_18; }
	inline void set_lineExtents_18(Extents_t2060714539  value)
	{
		___lineExtents_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: TMPro.TMP_LineInfo
struct TMP_LineInfo_t2462355872_marshaled_pinvoke
{
	int32_t ___characterCount_0;
	int32_t ___spaceCount_1;
	int32_t ___wordCount_2;
	int32_t ___firstCharacterIndex_3;
	int32_t ___firstVisibleCharacterIndex_4;
	int32_t ___lastCharacterIndex_5;
	int32_t ___lastVisibleCharacterIndex_6;
	float ___length_7;
	float ___lineHeight_8;
	float ___ascender_9;
	float ___baseline_10;
	float ___descender_11;
	float ___maxAdvance_12;
	float ___width_13;
	float ___marginLeft_14;
	float ___marginRight_15;
	float ___maxScale_16;
	int32_t ___alignment_17;
	Extents_t2060714539_marshaled_pinvoke ___lineExtents_18;
};
// Native definition for marshalling of: TMPro.TMP_LineInfo
struct TMP_LineInfo_t2462355872_marshaled_com
{
	int32_t ___characterCount_0;
	int32_t ___spaceCount_1;
	int32_t ___wordCount_2;
	int32_t ___firstCharacterIndex_3;
	int32_t ___firstVisibleCharacterIndex_4;
	int32_t ___lastCharacterIndex_5;
	int32_t ___lastVisibleCharacterIndex_6;
	float ___length_7;
	float ___lineHeight_8;
	float ___ascender_9;
	float ___baseline_10;
	float ___descender_11;
	float ___maxAdvance_12;
	float ___width_13;
	float ___marginLeft_14;
	float ___marginRight_15;
	float ___maxScale_16;
	int32_t ___alignment_17;
	Extents_t2060714539_marshaled_com ___lineExtents_18;
};
