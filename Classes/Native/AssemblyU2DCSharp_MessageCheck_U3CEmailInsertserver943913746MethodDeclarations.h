﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageCheck/<EmailInsertserverProgress>c__Iterator19
struct U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageCheck/<EmailInsertserverProgress>c__Iterator19::.ctor()
extern "C"  void U3CEmailInsertserverProgressU3Ec__Iterator19__ctor_m3795497625 (U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageCheck/<EmailInsertserverProgress>c__Iterator19::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEmailInsertserverProgressU3Ec__Iterator19_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m599915545 (U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageCheck/<EmailInsertserverProgress>c__Iterator19::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEmailInsertserverProgressU3Ec__Iterator19_System_Collections_IEnumerator_get_Current_m3434697133 (U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MessageCheck/<EmailInsertserverProgress>c__Iterator19::MoveNext()
extern "C"  bool U3CEmailInsertserverProgressU3Ec__Iterator19_MoveNext_m1712183227 (U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck/<EmailInsertserverProgress>c__Iterator19::Dispose()
extern "C"  void U3CEmailInsertserverProgressU3Ec__Iterator19_Dispose_m944266390 (U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck/<EmailInsertserverProgress>c__Iterator19::Reset()
extern "C"  void U3CEmailInsertserverProgressU3Ec__Iterator19_Reset_m1441930566 (U3CEmailInsertserverProgressU3Ec__Iterator19_t943913746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
