﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1792340962MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,UnityEngine.GameObject>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m85586033(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1296206596 *, Dictionary_2_t2595600883 *, const MethodInfo*))ValueCollection__ctor_m1155904981_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,UnityEngine.GameObject>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3751541153(__this, ___item0, method) ((  void (*) (ValueCollection_t1296206596 *, GameObject_t3674682005 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1984952253_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,UnityEngine.GameObject>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3918126826(__this, method) ((  void (*) (ValueCollection_t1296206596 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m71912198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,UnityEngine.GameObject>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3133882089(__this, ___item0, method) ((  bool (*) (ValueCollection_t1296206596 *, GameObject_t3674682005 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2971646985_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,UnityEngine.GameObject>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1376320974(__this, ___item0, method) ((  bool (*) (ValueCollection_t1296206596 *, GameObject_t3674682005 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1738162926_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,UnityEngine.GameObject>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3593044394(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1296206596 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1114479828_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,UnityEngine.GameObject>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m177103726(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1296206596 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3338296458_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,UnityEngine.GameObject>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4058651389(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1296206596 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4075324549_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,UnityEngine.GameObject>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1409057948(__this, method) ((  bool (*) (ValueCollection_t1296206596 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1246822844_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,UnityEngine.GameObject>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2626671740(__this, method) ((  bool (*) (ValueCollection_t1296206596 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3101622684_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,UnityEngine.GameObject>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m266790382(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1296206596 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3106526280_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,UnityEngine.GameObject>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1547919480(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1296206596 *, GameObjectU5BU5D_t2662109048*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2635984092_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,UnityEngine.GameObject>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2532166881(__this, method) ((  Enumerator_t527434291  (*) (ValueCollection_t1296206596 *, const MethodInfo*))ValueCollection_GetEnumerator_m3505452287_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<CurrentState/States,UnityEngine.GameObject>::get_Count()
#define ValueCollection_get_Count_m3267897270(__this, method) ((  int32_t (*) (ValueCollection_t1296206596 *, const MethodInfo*))ValueCollection_get_Count_m3710668130_gshared)(__this, method)
