﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_TMP_XmlTagStack_1_gen1085191720.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void TMPro.TMP_XmlTagStack`1<System.Object>::.ctor(T[])
extern "C"  void TMP_XmlTagStack_1__ctor_m1298610153_gshared (TMP_XmlTagStack_1_t1085191720 * __this, ObjectU5BU5D_t1108656482* ___tagStack0, const MethodInfo* method);
#define TMP_XmlTagStack_1__ctor_m1298610153(__this, ___tagStack0, method) ((  void (*) (TMP_XmlTagStack_1_t1085191720 *, ObjectU5BU5D_t1108656482*, const MethodInfo*))TMP_XmlTagStack_1__ctor_m1298610153_gshared)(__this, ___tagStack0, method)
// System.Void TMPro.TMP_XmlTagStack`1<System.Object>::Clear()
extern "C"  void TMP_XmlTagStack_1_Clear_m3412009054_gshared (TMP_XmlTagStack_1_t1085191720 * __this, const MethodInfo* method);
#define TMP_XmlTagStack_1_Clear_m3412009054(__this, method) ((  void (*) (TMP_XmlTagStack_1_t1085191720 *, const MethodInfo*))TMP_XmlTagStack_1_Clear_m3412009054_gshared)(__this, method)
// System.Void TMPro.TMP_XmlTagStack`1<System.Object>::SetDefault(T)
extern "C"  void TMP_XmlTagStack_1_SetDefault_m2092229870_gshared (TMP_XmlTagStack_1_t1085191720 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define TMP_XmlTagStack_1_SetDefault_m2092229870(__this, ___item0, method) ((  void (*) (TMP_XmlTagStack_1_t1085191720 *, Il2CppObject *, const MethodInfo*))TMP_XmlTagStack_1_SetDefault_m2092229870_gshared)(__this, ___item0, method)
// System.Void TMPro.TMP_XmlTagStack`1<System.Object>::Add(T)
extern "C"  void TMP_XmlTagStack_1_Add_m183880172_gshared (TMP_XmlTagStack_1_t1085191720 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define TMP_XmlTagStack_1_Add_m183880172(__this, ___item0, method) ((  void (*) (TMP_XmlTagStack_1_t1085191720 *, Il2CppObject *, const MethodInfo*))TMP_XmlTagStack_1_Add_m183880172_gshared)(__this, ___item0, method)
// T TMPro.TMP_XmlTagStack`1<System.Object>::Remove()
extern "C"  Il2CppObject * TMP_XmlTagStack_1_Remove_m3510387636_gshared (TMP_XmlTagStack_1_t1085191720 * __this, const MethodInfo* method);
#define TMP_XmlTagStack_1_Remove_m3510387636(__this, method) ((  Il2CppObject * (*) (TMP_XmlTagStack_1_t1085191720 *, const MethodInfo*))TMP_XmlTagStack_1_Remove_m3510387636_gshared)(__this, method)
