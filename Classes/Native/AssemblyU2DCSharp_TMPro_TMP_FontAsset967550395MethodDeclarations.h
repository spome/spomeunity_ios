﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.TMP_FontAsset
struct TMP_FontAsset_t967550395;
// TMPro.FaceInfo
struct FaceInfo_t3469866561;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Glyph>
struct Dictionary_2_t1996520333;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.KerningPair>
struct Dictionary_2_t1902096943;
// TMPro.KerningTable
struct KerningTable_t618956984;
// TMPro.LineBreakingTable
struct LineBreakingTable_t1570184089;
// TMPro.TMP_Glyph[]
struct TMP_GlyphU5BU5D_t3862879011;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Char>
struct Dictionary_2_t2859885777;
// UnityEngine.TextAsset
struct TextAsset_t3836129977;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t4230808090;
// TMPro.TMP_Glyph
struct TMP_Glyph_t1999257094;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_FaceInfo3469866561.h"
#include "AssemblyU2DCSharp_TMPro_KerningTable618956984.h"
#include "UnityEngine_UnityEngine_TextAsset3836129977.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_TMPro_TMP_Glyph1999257094.h"

// System.Void TMPro.TMP_FontAsset::.ctor()
extern "C"  void TMP_FontAsset__ctor_m1374249676 (TMP_FontAsset_t967550395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.FaceInfo TMPro.TMP_FontAsset::get_fontInfo()
extern "C"  FaceInfo_t3469866561 * TMP_FontAsset_get_fontInfo_m727406630 (TMP_FontAsset_t967550395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Glyph> TMPro.TMP_FontAsset::get_characterDictionary()
extern "C"  Dictionary_2_t1996520333 * TMP_FontAsset_get_characterDictionary_m1702741607 (TMP_FontAsset_t967550395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.KerningPair> TMPro.TMP_FontAsset::get_kerningDictionary()
extern "C"  Dictionary_2_t1902096943 * TMP_FontAsset_get_kerningDictionary_m3243023852 (TMP_FontAsset_t967550395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.KerningTable TMPro.TMP_FontAsset::get_kerningInfo()
extern "C"  KerningTable_t618956984 * TMP_FontAsset_get_kerningInfo_m1514805692 (TMP_FontAsset_t967550395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.LineBreakingTable TMPro.TMP_FontAsset::get_lineBreakingInfo()
extern "C"  LineBreakingTable_t1570184089 * TMP_FontAsset_get_lineBreakingInfo_m2581656048 (TMP_FontAsset_t967550395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_FontAsset::OnEnable()
extern "C"  void TMP_FontAsset_OnEnable_m2715410746 (TMP_FontAsset_t967550395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_FontAsset::OnDisable()
extern "C"  void TMP_FontAsset_OnDisable_m3014291763 (TMP_FontAsset_t967550395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_FontAsset::OnValidate()
extern "C"  void TMP_FontAsset_OnValidate_m529151629 (TMP_FontAsset_t967550395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_FontAsset::AddFaceInfo(TMPro.FaceInfo)
extern "C"  void TMP_FontAsset_AddFaceInfo_m1609591921 (TMP_FontAsset_t967550395 * __this, FaceInfo_t3469866561 * ___faceInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_FontAsset::AddGlyphInfo(TMPro.TMP_Glyph[])
extern "C"  void TMP_FontAsset_AddGlyphInfo_m389390869 (TMP_FontAsset_t967550395 * __this, TMP_GlyphU5BU5D_t3862879011* ___glyphInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_FontAsset::AddKerningInfo(TMPro.KerningTable)
extern "C"  void TMP_FontAsset_AddKerningInfo_m3736070005 (TMP_FontAsset_t967550395 * __this, KerningTable_t618956984 * ___kerningTable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_FontAsset::ReadFontDefinition()
extern "C"  void TMP_FontAsset_ReadFontDefinition_m4284761328 (TMP_FontAsset_t967550395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Char> TMPro.TMP_FontAsset::GetCharacters(UnityEngine.TextAsset)
extern "C"  Dictionary_2_t2859885777 * TMP_FontAsset_GetCharacters_m775574570 (TMP_FontAsset_t967550395 * __this, TextAsset_t3836129977 * ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_FontAsset::HasCharacter(System.Int32)
extern "C"  bool TMP_FontAsset_HasCharacter_m3559847300 (TMP_FontAsset_t967550395 * __this, int32_t ___character0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_FontAsset::HasCharacter(System.Char)
extern "C"  bool TMP_FontAsset_HasCharacter_m2741496906 (TMP_FontAsset_t967550395 * __this, Il2CppChar ___character0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TMPro.TMP_FontAsset::HasCharacters(System.String,System.Collections.Generic.List`1<System.Char>&)
extern "C"  bool TMP_FontAsset_HasCharacters_m108915821 (TMP_FontAsset_t967550395 * __this, String_t* ___text0, List_1_t4230808090 ** ___missingCharacters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TMPro.TMP_FontAsset::<AddGlyphInfo>m__F(TMPro.TMP_Glyph)
extern "C"  int32_t TMP_FontAsset_U3CAddGlyphInfoU3Em__F_m1083333916 (Il2CppObject * __this /* static, unused */, TMP_Glyph_t1999257094 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
