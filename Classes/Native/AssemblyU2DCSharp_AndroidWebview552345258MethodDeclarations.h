﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidWebview
struct AndroidWebview_t552345258;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void AndroidWebview::.ctor()
extern "C"  void AndroidWebview__ctor_m1655737649 (AndroidWebview_t552345258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidWebview::.cctor()
extern "C"  void AndroidWebview__cctor_m3601130652 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AndroidWebview AndroidWebview::get_Instance()
extern "C"  AndroidWebview_t552345258 * AndroidWebview_get_Instance_m3239679012 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidWebview::Awake()
extern "C"  void AndroidWebview_Awake_m1893342868 (AndroidWebview_t552345258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidWebview::Call(System.String,System.String)
extern "C"  void AndroidWebview_Call_m335597645 (AndroidWebview_t552345258 * __this, String_t* ___androidmethod0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidWebview::Call(System.String)
extern "C"  void AndroidWebview_Call_m1677830609 (AndroidWebview_t552345258 * __this, String_t* ___androidmethod0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
