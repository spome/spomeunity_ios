﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// PublicSpotMessage
struct PublicSpotMessage_t3890192988;
// TMPro.TMP_Sprite
struct TMP_Sprite_t3889423907;
// TMPro.MaterialManager/MaskingMaterial
struct MaskingMaterial_t2640101498;
// TMPro.TMP_Glyph
struct TMP_Glyph_t1999257094;
// TMPro.KerningPair
struct KerningPair_t1904833704;
// TMPro.TMP_Style
struct TMP_Style_t2010577547;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t967550395;
// TMPro.TMP_Text
struct TMP_Text_t980027659;

#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_CurrentState_States2558012761.h"
#include "AssemblyU2DCSharp_Messages3897517420.h"
#include "AssemblyU2DCSharp_PublicSpotMessage3890192988.h"
#include "AssemblyU2DCSharp_TMPro_TMP_Sprite3889423907.h"
#include "AssemblyU2DCSharp_TMPro_MaterialManager_MaskingMat2640101498.h"
#include "AssemblyU2DCSharp_TMPro_TMP_Glyph1999257094.h"
#include "AssemblyU2DCSharp_TMPro_KerningPair1904833704.h"
#include "AssemblyU2DCSharp_TMPro_TMP_Style2010577547.h"
#include "AssemblyU2DCSharp_TMPro_TMP_FontAsset967550395.h"
#include "AssemblyU2DCSharp_TMPro_XML_TagAttribute35801856.h"
#include "AssemblyU2DCSharp_TMPro_TMP_CharacterInfo3968677457.h"
#include "AssemblyU2DCSharp_TMPro_TMP_LineInfo2462355872.h"
#include "AssemblyU2DCSharp_TMPro_TMP_MeshInfo801067801.h"
#include "AssemblyU2DCSharp_TMPro_TMP_LinkInfo2467896998.h"
#include "AssemblyU2DCSharp_TMPro_TMP_WordInfo1302080950.h"
#include "AssemblyU2DCSharp_TMPro_TMP_PageInfo2133163707.h"
#include "AssemblyU2DCSharp_TMPro_TMP_Text980027659.h"

#pragma once
// CurrentState/States[]
struct StatesU5BU5D_t2154503460  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Messages[]
struct MessagesU5BU5D_t3583122277  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Messages_t3897517420  m_Items[1];

public:
	inline Messages_t3897517420  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Messages_t3897517420 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Messages_t3897517420  value)
	{
		m_Items[index] = value;
	}
};
// PublicSpotMessage[]
struct PublicSpotMessageU5BU5D_t2129009845  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PublicSpotMessage_t3890192988 * m_Items[1];

public:
	inline PublicSpotMessage_t3890192988 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PublicSpotMessage_t3890192988 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PublicSpotMessage_t3890192988 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TMPro.TMP_Sprite[]
struct TMP_SpriteU5BU5D_t4229301746  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TMP_Sprite_t3889423907 * m_Items[1];

public:
	inline TMP_Sprite_t3889423907 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TMP_Sprite_t3889423907 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TMP_Sprite_t3889423907 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TMPro.MaterialManager/MaskingMaterial[]
struct MaskingMaterialU5BU5D_t2672417887  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MaskingMaterial_t2640101498 * m_Items[1];

public:
	inline MaskingMaterial_t2640101498 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MaskingMaterial_t2640101498 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MaskingMaterial_t2640101498 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TMPro.TMP_Glyph[]
struct TMP_GlyphU5BU5D_t3862879011  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TMP_Glyph_t1999257094 * m_Items[1];

public:
	inline TMP_Glyph_t1999257094 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TMP_Glyph_t1999257094 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TMP_Glyph_t1999257094 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TMPro.KerningPair[]
struct KerningPairU5BU5D_t1752767417  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KerningPair_t1904833704 * m_Items[1];

public:
	inline KerningPair_t1904833704 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KerningPair_t1904833704 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KerningPair_t1904833704 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TMPro.TMP_Style[]
struct TMP_StyleU5BU5D_t1865566954  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TMP_Style_t2010577547 * m_Items[1];

public:
	inline TMP_Style_t2010577547 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TMP_Style_t2010577547 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TMP_Style_t2010577547 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TMPro.TMP_FontAsset[]
struct TMP_FontAssetU5BU5D_t1096097018  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TMP_FontAsset_t967550395 * m_Items[1];

public:
	inline TMP_FontAsset_t967550395 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TMP_FontAsset_t967550395 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TMP_FontAsset_t967550395 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TMPro.XML_TagAttribute[]
struct XML_TagAttributeU5BU5D_t3301714177  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) XML_TagAttribute_t35801856  m_Items[1];

public:
	inline XML_TagAttribute_t35801856  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline XML_TagAttribute_t35801856 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, XML_TagAttribute_t35801856  value)
	{
		m_Items[index] = value;
	}
};
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t2688989516  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TMP_CharacterInfo_t3968677457  m_Items[1];

public:
	inline TMP_CharacterInfo_t3968677457  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TMP_CharacterInfo_t3968677457 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TMP_CharacterInfo_t3968677457  value)
	{
		m_Items[index] = value;
	}
};
// TMPro.TMP_LineInfo[]
struct TMP_LineInfoU5BU5D_t2827998177  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TMP_LineInfo_t2462355872  m_Items[1];

public:
	inline TMP_LineInfo_t2462355872  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TMP_LineInfo_t2462355872 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TMP_LineInfo_t2462355872  value)
	{
		m_Items[index] = value;
	}
};
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t3450595428  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TMP_MeshInfo_t801067801  m_Items[1];

public:
	inline TMP_MeshInfo_t801067801  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TMP_MeshInfo_t801067801 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TMP_MeshInfo_t801067801  value)
	{
		m_Items[index] = value;
	}
};
// TMPro.TMP_LinkInfo[]
struct TMP_LinkInfoU5BU5D_t3523101699  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TMP_LinkInfo_t2467896998  m_Items[1];

public:
	inline TMP_LinkInfo_t2467896998  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TMP_LinkInfo_t2467896998 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TMP_LinkInfo_t2467896998  value)
	{
		m_Items[index] = value;
	}
};
// TMPro.TMP_WordInfo[]
struct TMP_WordInfoU5BU5D_t3309586099  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TMP_WordInfo_t1302080950  m_Items[1];

public:
	inline TMP_WordInfo_t1302080950  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TMP_WordInfo_t1302080950 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TMP_WordInfo_t1302080950  value)
	{
		m_Items[index] = value;
	}
};
// TMPro.TMP_PageInfo[]
struct TMP_PageInfoU5BU5D_t3051601914  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TMP_PageInfo_t2133163707  m_Items[1];

public:
	inline TMP_PageInfo_t2133163707  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TMP_PageInfo_t2133163707 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TMP_PageInfo_t2133163707  value)
	{
		m_Items[index] = value;
	}
};
// TMPro.TMP_Text[]
struct TMP_TextU5BU5D_t455914090  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TMP_Text_t980027659 * m_Items[1];

public:
	inline TMP_Text_t980027659 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TMP_Text_t980027659 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TMP_Text_t980027659 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
