﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<CurrentState/States,System.Object>
struct Dictionary_2_t3091735249;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1023568657.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<CurrentState/States,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3272932380_gshared (Enumerator_t1023568657 * __this, Dictionary_2_t3091735249 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3272932380(__this, ___host0, method) ((  void (*) (Enumerator_t1023568657 *, Dictionary_2_t3091735249 *, const MethodInfo*))Enumerator__ctor_m3272932380_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<CurrentState/States,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3951878085_gshared (Enumerator_t1023568657 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3951878085(__this, method) ((  Il2CppObject * (*) (Enumerator_t1023568657 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3951878085_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<CurrentState/States,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4074224409_gshared (Enumerator_t1023568657 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4074224409(__this, method) ((  void (*) (Enumerator_t1023568657 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4074224409_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<CurrentState/States,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1783775678_gshared (Enumerator_t1023568657 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1783775678(__this, method) ((  void (*) (Enumerator_t1023568657 *, const MethodInfo*))Enumerator_Dispose_m1783775678_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<CurrentState/States,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m715451653_gshared (Enumerator_t1023568657 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m715451653(__this, method) ((  bool (*) (Enumerator_t1023568657 *, const MethodInfo*))Enumerator_MoveNext_m715451653_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<CurrentState/States,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m902046301_gshared (Enumerator_t1023568657 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m902046301(__this, method) ((  Il2CppObject * (*) (Enumerator_t1023568657 *, const MethodInfo*))Enumerator_get_Current_m902046301_gshared)(__this, method)
