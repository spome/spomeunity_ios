﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.InputField
struct InputField_t609046876;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EmailChange
struct  EmailChange_t3079654988  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.InputField EmailChange::Name
	InputField_t609046876 * ___Name_2;
	// UnityEngine.UI.InputField EmailChange::Email
	InputField_t609046876 * ___Email_3;

public:
	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(EmailChange_t3079654988, ___Name_2)); }
	inline InputField_t609046876 * get_Name_2() const { return ___Name_2; }
	inline InputField_t609046876 ** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(InputField_t609046876 * value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier(&___Name_2, value);
	}

	inline static int32_t get_offset_of_Email_3() { return static_cast<int32_t>(offsetof(EmailChange_t3079654988, ___Email_3)); }
	inline InputField_t609046876 * get_Email_3() const { return ___Email_3; }
	inline InputField_t609046876 ** get_address_of_Email_3() { return &___Email_3; }
	inline void set_Email_3(InputField_t609046876 * value)
	{
		___Email_3 = value;
		Il2CppCodeGenWriteBarrier(&___Email_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
