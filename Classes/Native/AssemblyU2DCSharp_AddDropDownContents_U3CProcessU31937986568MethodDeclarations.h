﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AddDropDownContents/<Process>c__Iterator1
struct U3CProcessU3Ec__Iterator1_t1937986568;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AddDropDownContents/<Process>c__Iterator1::.ctor()
extern "C"  void U3CProcessU3Ec__Iterator1__ctor_m1320430179 (U3CProcessU3Ec__Iterator1_t1937986568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AddDropDownContents/<Process>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CProcessU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3058628367 (U3CProcessU3Ec__Iterator1_t1937986568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AddDropDownContents/<Process>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CProcessU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m269940899 (U3CProcessU3Ec__Iterator1_t1937986568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AddDropDownContents/<Process>c__Iterator1::MoveNext()
extern "C"  bool U3CProcessU3Ec__Iterator1_MoveNext_m2969380273 (U3CProcessU3Ec__Iterator1_t1937986568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddDropDownContents/<Process>c__Iterator1::Dispose()
extern "C"  void U3CProcessU3Ec__Iterator1_Dispose_m1816332768 (U3CProcessU3Ec__Iterator1_t1937986568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddDropDownContents/<Process>c__Iterator1::Reset()
extern "C"  void U3CProcessU3Ec__Iterator1_Reset_m3261830416 (U3CProcessU3Ec__Iterator1_t1937986568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
