﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.IO.FileInfo
struct FileInfo_t3233670074;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// LitJson.JsonData
struct JsonData_t1715015430;
// System.Object
struct Il2CppObject;
// AlbumPathCheck
struct AlbumPathCheck_t1505991252;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AlbumPathCheck/<UploadFileCo>c__Iterator3
struct  U3CUploadFileCoU3Ec__Iterator3_t1598057078  : public Il2CppObject
{
public:
	// System.String AlbumPathCheck/<UploadFileCo>c__Iterator3::localFileName
	String_t* ___localFileName_0;
	// UnityEngine.WWW AlbumPathCheck/<UploadFileCo>c__Iterator3::<localFile>__0
	WWW_t3134621005 * ___U3ClocalFileU3E__0_1;
	// System.IO.FileInfo AlbumPathCheck/<UploadFileCo>c__Iterator3::<fileinfo>__1
	FileInfo_t3233670074 * ___U3CfileinfoU3E__1_2;
	// System.Double AlbumPathCheck/<UploadFileCo>c__Iterator3::<byteCount>__2
	double ___U3CbyteCountU3E__2_3;
	// UnityEngine.WWWForm AlbumPathCheck/<UploadFileCo>c__Iterator3::<form>__3
	WWWForm_t461342257 * ___U3CformU3E__3_4;
	// System.String AlbumPathCheck/<UploadFileCo>c__Iterator3::uploadURL
	String_t* ___uploadURL_5;
	// UnityEngine.WWW AlbumPathCheck/<UploadFileCo>c__Iterator3::<upload>__4
	WWW_t3134621005 * ___U3CuploadU3E__4_6;
	// LitJson.JsonData AlbumPathCheck/<UploadFileCo>c__Iterator3::<EventJS>__5
	JsonData_t1715015430 * ___U3CEventJSU3E__5_7;
	// System.Int32 AlbumPathCheck/<UploadFileCo>c__Iterator3::$PC
	int32_t ___U24PC_8;
	// System.Object AlbumPathCheck/<UploadFileCo>c__Iterator3::$current
	Il2CppObject * ___U24current_9;
	// System.String AlbumPathCheck/<UploadFileCo>c__Iterator3::<$>localFileName
	String_t* ___U3CU24U3ElocalFileName_10;
	// System.String AlbumPathCheck/<UploadFileCo>c__Iterator3::<$>uploadURL
	String_t* ___U3CU24U3EuploadURL_11;
	// AlbumPathCheck AlbumPathCheck/<UploadFileCo>c__Iterator3::<>f__this
	AlbumPathCheck_t1505991252 * ___U3CU3Ef__this_12;

public:
	inline static int32_t get_offset_of_localFileName_0() { return static_cast<int32_t>(offsetof(U3CUploadFileCoU3Ec__Iterator3_t1598057078, ___localFileName_0)); }
	inline String_t* get_localFileName_0() const { return ___localFileName_0; }
	inline String_t** get_address_of_localFileName_0() { return &___localFileName_0; }
	inline void set_localFileName_0(String_t* value)
	{
		___localFileName_0 = value;
		Il2CppCodeGenWriteBarrier(&___localFileName_0, value);
	}

	inline static int32_t get_offset_of_U3ClocalFileU3E__0_1() { return static_cast<int32_t>(offsetof(U3CUploadFileCoU3Ec__Iterator3_t1598057078, ___U3ClocalFileU3E__0_1)); }
	inline WWW_t3134621005 * get_U3ClocalFileU3E__0_1() const { return ___U3ClocalFileU3E__0_1; }
	inline WWW_t3134621005 ** get_address_of_U3ClocalFileU3E__0_1() { return &___U3ClocalFileU3E__0_1; }
	inline void set_U3ClocalFileU3E__0_1(WWW_t3134621005 * value)
	{
		___U3ClocalFileU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClocalFileU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CfileinfoU3E__1_2() { return static_cast<int32_t>(offsetof(U3CUploadFileCoU3Ec__Iterator3_t1598057078, ___U3CfileinfoU3E__1_2)); }
	inline FileInfo_t3233670074 * get_U3CfileinfoU3E__1_2() const { return ___U3CfileinfoU3E__1_2; }
	inline FileInfo_t3233670074 ** get_address_of_U3CfileinfoU3E__1_2() { return &___U3CfileinfoU3E__1_2; }
	inline void set_U3CfileinfoU3E__1_2(FileInfo_t3233670074 * value)
	{
		___U3CfileinfoU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfileinfoU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CbyteCountU3E__2_3() { return static_cast<int32_t>(offsetof(U3CUploadFileCoU3Ec__Iterator3_t1598057078, ___U3CbyteCountU3E__2_3)); }
	inline double get_U3CbyteCountU3E__2_3() const { return ___U3CbyteCountU3E__2_3; }
	inline double* get_address_of_U3CbyteCountU3E__2_3() { return &___U3CbyteCountU3E__2_3; }
	inline void set_U3CbyteCountU3E__2_3(double value)
	{
		___U3CbyteCountU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CformU3E__3_4() { return static_cast<int32_t>(offsetof(U3CUploadFileCoU3Ec__Iterator3_t1598057078, ___U3CformU3E__3_4)); }
	inline WWWForm_t461342257 * get_U3CformU3E__3_4() const { return ___U3CformU3E__3_4; }
	inline WWWForm_t461342257 ** get_address_of_U3CformU3E__3_4() { return &___U3CformU3E__3_4; }
	inline void set_U3CformU3E__3_4(WWWForm_t461342257 * value)
	{
		___U3CformU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CformU3E__3_4, value);
	}

	inline static int32_t get_offset_of_uploadURL_5() { return static_cast<int32_t>(offsetof(U3CUploadFileCoU3Ec__Iterator3_t1598057078, ___uploadURL_5)); }
	inline String_t* get_uploadURL_5() const { return ___uploadURL_5; }
	inline String_t** get_address_of_uploadURL_5() { return &___uploadURL_5; }
	inline void set_uploadURL_5(String_t* value)
	{
		___uploadURL_5 = value;
		Il2CppCodeGenWriteBarrier(&___uploadURL_5, value);
	}

	inline static int32_t get_offset_of_U3CuploadU3E__4_6() { return static_cast<int32_t>(offsetof(U3CUploadFileCoU3Ec__Iterator3_t1598057078, ___U3CuploadU3E__4_6)); }
	inline WWW_t3134621005 * get_U3CuploadU3E__4_6() const { return ___U3CuploadU3E__4_6; }
	inline WWW_t3134621005 ** get_address_of_U3CuploadU3E__4_6() { return &___U3CuploadU3E__4_6; }
	inline void set_U3CuploadU3E__4_6(WWW_t3134621005 * value)
	{
		___U3CuploadU3E__4_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuploadU3E__4_6, value);
	}

	inline static int32_t get_offset_of_U3CEventJSU3E__5_7() { return static_cast<int32_t>(offsetof(U3CUploadFileCoU3Ec__Iterator3_t1598057078, ___U3CEventJSU3E__5_7)); }
	inline JsonData_t1715015430 * get_U3CEventJSU3E__5_7() const { return ___U3CEventJSU3E__5_7; }
	inline JsonData_t1715015430 ** get_address_of_U3CEventJSU3E__5_7() { return &___U3CEventJSU3E__5_7; }
	inline void set_U3CEventJSU3E__5_7(JsonData_t1715015430 * value)
	{
		___U3CEventJSU3E__5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEventJSU3E__5_7, value);
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CUploadFileCoU3Ec__Iterator3_t1598057078, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CUploadFileCoU3Ec__Iterator3_t1598057078, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3ElocalFileName_10() { return static_cast<int32_t>(offsetof(U3CUploadFileCoU3Ec__Iterator3_t1598057078, ___U3CU24U3ElocalFileName_10)); }
	inline String_t* get_U3CU24U3ElocalFileName_10() const { return ___U3CU24U3ElocalFileName_10; }
	inline String_t** get_address_of_U3CU24U3ElocalFileName_10() { return &___U3CU24U3ElocalFileName_10; }
	inline void set_U3CU24U3ElocalFileName_10(String_t* value)
	{
		___U3CU24U3ElocalFileName_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3ElocalFileName_10, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EuploadURL_11() { return static_cast<int32_t>(offsetof(U3CUploadFileCoU3Ec__Iterator3_t1598057078, ___U3CU24U3EuploadURL_11)); }
	inline String_t* get_U3CU24U3EuploadURL_11() const { return ___U3CU24U3EuploadURL_11; }
	inline String_t** get_address_of_U3CU24U3EuploadURL_11() { return &___U3CU24U3EuploadURL_11; }
	inline void set_U3CU24U3EuploadURL_11(String_t* value)
	{
		___U3CU24U3EuploadURL_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EuploadURL_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_12() { return static_cast<int32_t>(offsetof(U3CUploadFileCoU3Ec__Iterator3_t1598057078, ___U3CU3Ef__this_12)); }
	inline AlbumPathCheck_t1505991252 * get_U3CU3Ef__this_12() const { return ___U3CU3Ef__this_12; }
	inline AlbumPathCheck_t1505991252 ** get_address_of_U3CU3Ef__this_12() { return &___U3CU3Ef__this_12; }
	inline void set_U3CU3Ef__this_12(AlbumPathCheck_t1505991252 * value)
	{
		___U3CU3Ef__this_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
