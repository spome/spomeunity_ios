﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageCheck/<ImageLoad>c__Iterator16
struct U3CImageLoadU3Ec__Iterator16_t4085388203;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageCheck/<ImageLoad>c__Iterator16::.ctor()
extern "C"  void U3CImageLoadU3Ec__Iterator16__ctor_m3047818784 (U3CImageLoadU3Ec__Iterator16_t4085388203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageCheck/<ImageLoad>c__Iterator16::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CImageLoadU3Ec__Iterator16_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3487958258 (U3CImageLoadU3Ec__Iterator16_t4085388203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageCheck/<ImageLoad>c__Iterator16::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CImageLoadU3Ec__Iterator16_System_Collections_IEnumerator_get_Current_m3855887494 (U3CImageLoadU3Ec__Iterator16_t4085388203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MessageCheck/<ImageLoad>c__Iterator16::MoveNext()
extern "C"  bool U3CImageLoadU3Ec__Iterator16_MoveNext_m3589806804 (U3CImageLoadU3Ec__Iterator16_t4085388203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck/<ImageLoad>c__Iterator16::Dispose()
extern "C"  void U3CImageLoadU3Ec__Iterator16_Dispose_m3979405917 (U3CImageLoadU3Ec__Iterator16_t4085388203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageCheck/<ImageLoad>c__Iterator16::Reset()
extern "C"  void U3CImageLoadU3Ec__Iterator16_Reset_m694251725 (U3CImageLoadU3Ec__Iterator16_t4085388203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
