﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ErrorMessage/<AndroidAlertDialog>c__AnonStorey24
struct  U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220  : public Il2CppObject
{
public:
	// System.String ErrorMessage/<AndroidAlertDialog>c__AnonStorey24::title
	String_t* ___title_0;
	// System.String ErrorMessage/<AndroidAlertDialog>c__AnonStorey24::message
	String_t* ___message_1;
	// System.String ErrorMessage/<AndroidAlertDialog>c__AnonStorey24::button
	String_t* ___button_2;

public:
	inline static int32_t get_offset_of_title_0() { return static_cast<int32_t>(offsetof(U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220, ___title_0)); }
	inline String_t* get_title_0() const { return ___title_0; }
	inline String_t** get_address_of_title_0() { return &___title_0; }
	inline void set_title_0(String_t* value)
	{
		___title_0 = value;
		Il2CppCodeGenWriteBarrier(&___title_0, value);
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier(&___message_1, value);
	}

	inline static int32_t get_offset_of_button_2() { return static_cast<int32_t>(offsetof(U3CAndroidAlertDialogU3Ec__AnonStorey24_t2424498220, ___button_2)); }
	inline String_t* get_button_2() const { return ___button_2; }
	inline String_t** get_address_of_button_2() { return &___button_2; }
	inline void set_button_2(String_t* value)
	{
		___button_2 = value;
		Il2CppCodeGenWriteBarrier(&___button_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
