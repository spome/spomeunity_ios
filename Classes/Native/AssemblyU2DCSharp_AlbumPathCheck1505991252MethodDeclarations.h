﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AlbumPathCheck
struct AlbumPathCheck_t1505991252;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.WWW
struct WWW_t3134621005;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"

// System.Void AlbumPathCheck::.ctor()
extern "C"  void AlbumPathCheck__ctor_m2916651207 (AlbumPathCheck_t1505991252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck::ChangeToggle(System.Int32)
extern "C"  void AlbumPathCheck_ChangeToggle_m4061799826 (AlbumPathCheck_t1505991252 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck::AndroidAlbumPath(System.String)
extern "C"  void AlbumPathCheck_AndroidAlbumPath_m3497750624 (AlbumPathCheck_t1505991252 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck::AndroidAlbumFileName(System.String)
extern "C"  void AlbumPathCheck_AndroidAlbumFileName_m3922339870 (AlbumPathCheck_t1505991252 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck::OnValueChanged(System.Int32)
extern "C"  void AlbumPathCheck_OnValueChanged_m2658262640 (AlbumPathCheck_t1505991252 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck::MessageSend()
extern "C"  void AlbumPathCheck_MessageSend_m1793086292 (AlbumPathCheck_t1505991252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck::OnDisable()
extern "C"  void AlbumPathCheck_OnDisable_m135014830 (AlbumPathCheck_t1505991252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AlbumPathCheck::Send(System.String,System.String,System.Int32,System.String,System.String)
extern "C"  Il2CppObject * AlbumPathCheck_Send_m1955910846 (AlbumPathCheck_t1505991252 * __this, String_t* ___title0, String_t* ___filename1, int32_t ___pref2, String_t* ___addr013, String_t* ___addr024, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AlbumPathCheck::UploadFileCo(System.String,System.String)
extern "C"  Il2CppObject * AlbumPathCheck_UploadFileCo_m1802983664 (AlbumPathCheck_t1505991252 * __this, String_t* ___localFileName0, String_t* ___uploadURL1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck::UploadFile(System.String,System.String)
extern "C"  void AlbumPathCheck_UploadFile_m3385188612 (AlbumPathCheck_t1505991252 * __this, String_t* ___localFileName0, String_t* ___uploadURL1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AlbumPathCheck::CheckProgress(UnityEngine.WWW)
extern "C"  Il2CppObject * AlbumPathCheck_CheckProgress_m2259070848 (AlbumPathCheck_t1505991252 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck::addressActive()
extern "C"  void AlbumPathCheck_addressActive_m2345188255 (AlbumPathCheck_t1505991252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck::mapAtive()
extern "C"  void AlbumPathCheck_mapAtive_m2160111366 (AlbumPathCheck_t1505991252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck::mapClickGEO(System.String)
extern "C"  void AlbumPathCheck_mapClickGEO_m1310738264 (AlbumPathCheck_t1505991252 * __this, String_t* ___geo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck::mapLat(System.String)
extern "C"  void AlbumPathCheck_mapLat_m1375933602 (AlbumPathCheck_t1505991252 * __this, String_t* ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck::mapLon(System.String)
extern "C"  void AlbumPathCheck_mapLon_m1910637942 (AlbumPathCheck_t1505991252 * __this, String_t* ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlbumPathCheck::OnEnable()
extern "C"  void AlbumPathCheck_OnEnable_m544320863 (AlbumPathCheck_t1505991252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
