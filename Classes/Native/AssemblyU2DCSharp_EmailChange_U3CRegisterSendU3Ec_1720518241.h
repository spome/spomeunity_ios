﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t461342257;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Object
struct Il2CppObject;
// EmailChange
struct EmailChange_t3079654988;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EmailChange/<RegisterSend>c__Iterator8
struct  U3CRegisterSendU3Ec__Iterator8_t1720518241  : public Il2CppObject
{
public:
	// UnityEngine.WWWForm EmailChange/<RegisterSend>c__Iterator8::<form>__0
	WWWForm_t461342257 * ___U3CformU3E__0_0;
	// UnityEngine.WWW EmailChange/<RegisterSend>c__Iterator8::<w>__1
	WWW_t3134621005 * ___U3CwU3E__1_1;
	// System.Int32 EmailChange/<RegisterSend>c__Iterator8::$PC
	int32_t ___U24PC_2;
	// System.Object EmailChange/<RegisterSend>c__Iterator8::$current
	Il2CppObject * ___U24current_3;
	// EmailChange EmailChange/<RegisterSend>c__Iterator8::<>f__this
	EmailChange_t3079654988 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRegisterSendU3Ec__Iterator8_t1720518241, ___U3CformU3E__0_0)); }
	inline WWWForm_t461342257 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t461342257 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t461342257 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CformU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CwU3E__1_1() { return static_cast<int32_t>(offsetof(U3CRegisterSendU3Ec__Iterator8_t1720518241, ___U3CwU3E__1_1)); }
	inline WWW_t3134621005 * get_U3CwU3E__1_1() const { return ___U3CwU3E__1_1; }
	inline WWW_t3134621005 ** get_address_of_U3CwU3E__1_1() { return &___U3CwU3E__1_1; }
	inline void set_U3CwU3E__1_1(WWW_t3134621005 * value)
	{
		___U3CwU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CRegisterSendU3Ec__Iterator8_t1720518241, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CRegisterSendU3Ec__Iterator8_t1720518241, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CRegisterSendU3Ec__Iterator8_t1720518241, ___U3CU3Ef__this_4)); }
	inline EmailChange_t3079654988 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline EmailChange_t3079654988 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(EmailChange_t3079654988 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
