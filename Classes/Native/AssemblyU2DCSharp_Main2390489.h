﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.EventSystems.EventSystem
struct EventSystem_t2276120119;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main
struct  Main_t2390489  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.EventSystems.EventSystem Main::m_event
	EventSystem_t2276120119 * ___m_event_2;

public:
	inline static int32_t get_offset_of_m_event_2() { return static_cast<int32_t>(offsetof(Main_t2390489, ___m_event_2)); }
	inline EventSystem_t2276120119 * get_m_event_2() const { return ___m_event_2; }
	inline EventSystem_t2276120119 ** get_address_of_m_event_2() { return &___m_event_2; }
	inline void set_m_event_2(EventSystem_t2276120119 * value)
	{
		___m_event_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_event_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
