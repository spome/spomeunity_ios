﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ErrorMessage
struct ErrorMessage_t1367556351;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ErrorMessage
struct  ErrorMessage_t1367556351  : public MonoBehaviour_t667441552
{
public:

public:
};

struct ErrorMessage_t1367556351_StaticFields
{
public:
	// ErrorMessage ErrorMessage::EM
	ErrorMessage_t1367556351 * ___EM_2;
	// System.Boolean ErrorMessage::_PopupON
	bool ____PopupON_3;
	// UnityEngine.AndroidJavaObject ErrorMessage::activity
	AndroidJavaObject_t2362096582 * ___activity_4;

public:
	inline static int32_t get_offset_of_EM_2() { return static_cast<int32_t>(offsetof(ErrorMessage_t1367556351_StaticFields, ___EM_2)); }
	inline ErrorMessage_t1367556351 * get_EM_2() const { return ___EM_2; }
	inline ErrorMessage_t1367556351 ** get_address_of_EM_2() { return &___EM_2; }
	inline void set_EM_2(ErrorMessage_t1367556351 * value)
	{
		___EM_2 = value;
		Il2CppCodeGenWriteBarrier(&___EM_2, value);
	}

	inline static int32_t get_offset_of__PopupON_3() { return static_cast<int32_t>(offsetof(ErrorMessage_t1367556351_StaticFields, ____PopupON_3)); }
	inline bool get__PopupON_3() const { return ____PopupON_3; }
	inline bool* get_address_of__PopupON_3() { return &____PopupON_3; }
	inline void set__PopupON_3(bool value)
	{
		____PopupON_3 = value;
	}

	inline static int32_t get_offset_of_activity_4() { return static_cast<int32_t>(offsetof(ErrorMessage_t1367556351_StaticFields, ___activity_4)); }
	inline AndroidJavaObject_t2362096582 * get_activity_4() const { return ___activity_4; }
	inline AndroidJavaObject_t2362096582 ** get_address_of_activity_4() { return &___activity_4; }
	inline void set_activity_4(AndroidJavaObject_t2362096582 * value)
	{
		___activity_4 = value;
		Il2CppCodeGenWriteBarrier(&___activity_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
