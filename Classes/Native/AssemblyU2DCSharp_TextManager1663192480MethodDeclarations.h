﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TextManager
struct TextManager_t1663192480;

#include "codegen/il2cpp-codegen.h"

// System.Void TextManager::.ctor()
extern "C"  void TextManager__ctor_m456557003 (TextManager_t1663192480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
