﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPSManager/<Start>c__IteratorD
struct U3CStartU3Ec__IteratorD_t1827712127;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GPSManager/<Start>c__IteratorD::.ctor()
extern "C"  void U3CStartU3Ec__IteratorD__ctor_m142249724 (U3CStartU3Ec__IteratorD_t1827712127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GPSManager/<Start>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m925952416 (U3CStartU3Ec__IteratorD_t1827712127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GPSManager/<Start>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3853135156 (U3CStartU3Ec__IteratorD_t1827712127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GPSManager/<Start>c__IteratorD::MoveNext()
extern "C"  bool U3CStartU3Ec__IteratorD_MoveNext_m547373664 (U3CStartU3Ec__IteratorD_t1827712127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPSManager/<Start>c__IteratorD::Dispose()
extern "C"  void U3CStartU3Ec__IteratorD_Dispose_m3456281657 (U3CStartU3Ec__IteratorD_t1827712127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPSManager/<Start>c__IteratorD::Reset()
extern "C"  void U3CStartU3Ec__IteratorD_Reset_m2083649961 (U3CStartU3Ec__IteratorD_t1827712127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
