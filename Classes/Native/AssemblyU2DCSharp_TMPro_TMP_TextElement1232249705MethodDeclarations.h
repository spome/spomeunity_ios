﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.TMP_TextElement
struct TMP_TextElement_t1232249705;

#include "codegen/il2cpp-codegen.h"

// System.Void TMPro.TMP_TextElement::.ctor()
extern "C"  void TMP_TextElement__ctor_m3772960478 (TMP_TextElement_t1232249705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
