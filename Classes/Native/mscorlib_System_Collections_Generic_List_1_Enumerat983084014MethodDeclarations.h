﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<PublicSpotMessage>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3024925507(__this, ___l0, method) ((  void (*) (Enumerator_t983084014 *, List_1_t963411244 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PublicSpotMessage>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1501176623(__this, method) ((  void (*) (Enumerator_t983084014 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<PublicSpotMessage>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m798979995(__this, method) ((  Il2CppObject * (*) (Enumerator_t983084014 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PublicSpotMessage>::Dispose()
#define Enumerator_Dispose_m157024744(__this, method) ((  void (*) (Enumerator_t983084014 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PublicSpotMessage>::VerifyState()
#define Enumerator_VerifyState_m2137820449(__this, method) ((  void (*) (Enumerator_t983084014 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PublicSpotMessage>::MoveNext()
#define Enumerator_MoveNext_m2959601819(__this, method) ((  bool (*) (Enumerator_t983084014 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<PublicSpotMessage>::get_Current()
#define Enumerator_get_Current_m1153764760(__this, method) ((  PublicSpotMessage_t3890192988 * (*) (Enumerator_t983084014 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
