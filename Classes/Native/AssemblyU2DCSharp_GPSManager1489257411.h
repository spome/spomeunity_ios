﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_LocationState3286232508.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GPSManager
struct  GPSManager_t1489257411  : public MonoBehaviour_t667441552
{
public:

public:
};

struct GPSManager_t1489257411_StaticFields
{
public:
	// LocationState GPSManager::state
	int32_t ___state_3;
	// System.Double GPSManager::latitude
	double ___latitude_4;
	// System.Double GPSManager::longitude
	double ___longitude_5;
	// System.Double GPSManager::distance
	double ___distance_6;
	// System.Double GPSManager::TargetLatitude
	double ___TargetLatitude_7;
	// System.Double GPSManager::TargetLongitude
	double ___TargetLongitude_8;

public:
	inline static int32_t get_offset_of_state_3() { return static_cast<int32_t>(offsetof(GPSManager_t1489257411_StaticFields, ___state_3)); }
	inline int32_t get_state_3() const { return ___state_3; }
	inline int32_t* get_address_of_state_3() { return &___state_3; }
	inline void set_state_3(int32_t value)
	{
		___state_3 = value;
	}

	inline static int32_t get_offset_of_latitude_4() { return static_cast<int32_t>(offsetof(GPSManager_t1489257411_StaticFields, ___latitude_4)); }
	inline double get_latitude_4() const { return ___latitude_4; }
	inline double* get_address_of_latitude_4() { return &___latitude_4; }
	inline void set_latitude_4(double value)
	{
		___latitude_4 = value;
	}

	inline static int32_t get_offset_of_longitude_5() { return static_cast<int32_t>(offsetof(GPSManager_t1489257411_StaticFields, ___longitude_5)); }
	inline double get_longitude_5() const { return ___longitude_5; }
	inline double* get_address_of_longitude_5() { return &___longitude_5; }
	inline void set_longitude_5(double value)
	{
		___longitude_5 = value;
	}

	inline static int32_t get_offset_of_distance_6() { return static_cast<int32_t>(offsetof(GPSManager_t1489257411_StaticFields, ___distance_6)); }
	inline double get_distance_6() const { return ___distance_6; }
	inline double* get_address_of_distance_6() { return &___distance_6; }
	inline void set_distance_6(double value)
	{
		___distance_6 = value;
	}

	inline static int32_t get_offset_of_TargetLatitude_7() { return static_cast<int32_t>(offsetof(GPSManager_t1489257411_StaticFields, ___TargetLatitude_7)); }
	inline double get_TargetLatitude_7() const { return ___TargetLatitude_7; }
	inline double* get_address_of_TargetLatitude_7() { return &___TargetLatitude_7; }
	inline void set_TargetLatitude_7(double value)
	{
		___TargetLatitude_7 = value;
	}

	inline static int32_t get_offset_of_TargetLongitude_8() { return static_cast<int32_t>(offsetof(GPSManager_t1489257411_StaticFields, ___TargetLongitude_8)); }
	inline double get_TargetLongitude_8() const { return ___TargetLongitude_8; }
	inline double* get_address_of_TargetLongitude_8() { return &___TargetLongitude_8; }
	inline void set_TargetLongitude_8(double value)
	{
		___TargetLongitude_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
