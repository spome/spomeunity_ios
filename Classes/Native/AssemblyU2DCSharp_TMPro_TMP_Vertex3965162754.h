﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Vertex
struct  TMP_Vertex_t3965162754 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_Vertex::position
	Vector3_t4282066566  ___position_0;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv
	Vector2_t4282066565  ___uv_1;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv2
	Vector2_t4282066565  ___uv2_2;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv4
	Vector2_t4282066565  ___uv4_3;
	// UnityEngine.Color32 TMPro.TMP_Vertex::color
	Color32_t598853688  ___color_4;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TMP_Vertex_t3965162754, ___position_0)); }
	inline Vector3_t4282066566  get_position_0() const { return ___position_0; }
	inline Vector3_t4282066566 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t4282066566  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(TMP_Vertex_t3965162754, ___uv_1)); }
	inline Vector2_t4282066565  get_uv_1() const { return ___uv_1; }
	inline Vector2_t4282066565 * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Vector2_t4282066565  value)
	{
		___uv_1 = value;
	}

	inline static int32_t get_offset_of_uv2_2() { return static_cast<int32_t>(offsetof(TMP_Vertex_t3965162754, ___uv2_2)); }
	inline Vector2_t4282066565  get_uv2_2() const { return ___uv2_2; }
	inline Vector2_t4282066565 * get_address_of_uv2_2() { return &___uv2_2; }
	inline void set_uv2_2(Vector2_t4282066565  value)
	{
		___uv2_2 = value;
	}

	inline static int32_t get_offset_of_uv4_3() { return static_cast<int32_t>(offsetof(TMP_Vertex_t3965162754, ___uv4_3)); }
	inline Vector2_t4282066565  get_uv4_3() const { return ___uv4_3; }
	inline Vector2_t4282066565 * get_address_of_uv4_3() { return &___uv4_3; }
	inline void set_uv4_3(Vector2_t4282066565  value)
	{
		___uv4_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(TMP_Vertex_t3965162754, ___color_4)); }
	inline Color32_t598853688  get_color_4() const { return ___color_4; }
	inline Color32_t598853688 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color32_t598853688  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: TMPro.TMP_Vertex
struct TMP_Vertex_t3965162754_marshaled_pinvoke
{
	Vector3_t4282066566_marshaled_pinvoke ___position_0;
	Vector2_t4282066565_marshaled_pinvoke ___uv_1;
	Vector2_t4282066565_marshaled_pinvoke ___uv2_2;
	Vector2_t4282066565_marshaled_pinvoke ___uv4_3;
	Color32_t598853688_marshaled_pinvoke ___color_4;
};
// Native definition for marshalling of: TMPro.TMP_Vertex
struct TMP_Vertex_t3965162754_marshaled_com
{
	Vector3_t4282066566_marshaled_com ___position_0;
	Vector2_t4282066565_marshaled_com ___uv_1;
	Vector2_t4282066565_marshaled_com ___uv2_2;
	Vector2_t4282066565_marshaled_com ___uv4_3;
	Color32_t598853688_marshaled_com ___color_4;
};
