﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Loading/<Load>c__Iterator13
struct U3CLoadU3Ec__Iterator13_t1639883204;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Loading/<Load>c__Iterator13::.ctor()
extern "C"  void U3CLoadU3Ec__Iterator13__ctor_m1834599975 (U3CLoadU3Ec__Iterator13_t1639883204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Loading/<Load>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2542970123 (U3CLoadU3Ec__Iterator13_t1639883204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Loading/<Load>c__Iterator13::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m3406700191 (U3CLoadU3Ec__Iterator13_t1639883204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Loading/<Load>c__Iterator13::MoveNext()
extern "C"  bool U3CLoadU3Ec__Iterator13_MoveNext_m87464045 (U3CLoadU3Ec__Iterator13_t1639883204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loading/<Load>c__Iterator13::Dispose()
extern "C"  void U3CLoadU3Ec__Iterator13_Dispose_m2012267684 (U3CLoadU3Ec__Iterator13_t1639883204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loading/<Load>c__Iterator13::Reset()
extern "C"  void U3CLoadU3Ec__Iterator13_Reset_m3776000212 (U3CLoadU3Ec__Iterator13_t1639883204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
