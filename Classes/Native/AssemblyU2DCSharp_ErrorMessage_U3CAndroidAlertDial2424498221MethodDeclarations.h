﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ErrorMessage/<AndroidAlertDialog>c__AnonStorey25
struct U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221;

#include "codegen/il2cpp-codegen.h"

// System.Void ErrorMessage/<AndroidAlertDialog>c__AnonStorey25::.ctor()
extern "C"  void U3CAndroidAlertDialogU3Ec__AnonStorey25__ctor_m331464974 (U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessage/<AndroidAlertDialog>c__AnonStorey25::<>m__2()
extern "C"  void U3CAndroidAlertDialogU3Ec__AnonStorey25_U3CU3Em__2_m1621385885 (U3CAndroidAlertDialogU3Ec__AnonStorey25_t2424498221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
