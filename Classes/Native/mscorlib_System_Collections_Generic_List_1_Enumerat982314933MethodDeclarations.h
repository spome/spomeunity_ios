﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<TMPro.TMP_Sprite>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m633579578(__this, ___l0, method) ((  void (*) (Enumerator_t982314933 *, List_1_t962642163 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TMPro.TMP_Sprite>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m12431320(__this, method) ((  void (*) (Enumerator_t982314933 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<TMPro.TMP_Sprite>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2146662606(__this, method) ((  Il2CppObject * (*) (Enumerator_t982314933 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TMPro.TMP_Sprite>::Dispose()
#define Enumerator_Dispose_m603612319(__this, method) ((  void (*) (Enumerator_t982314933 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TMPro.TMP_Sprite>::VerifyState()
#define Enumerator_VerifyState_m2317139032(__this, method) ((  void (*) (Enumerator_t982314933 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<TMPro.TMP_Sprite>::MoveNext()
#define Enumerator_MoveNext_m714292616(__this, method) ((  bool (*) (Enumerator_t982314933 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<TMPro.TMP_Sprite>::get_Current()
#define Enumerator_get_Current_m3914756081(__this, method) ((  TMP_Sprite_t3889423907 * (*) (Enumerator_t982314933 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
