﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenDebug
struct ScreenDebug_t1599402279;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreenDebug::.ctor()
extern "C"  void ScreenDebug__ctor_m1292232228 (ScreenDebug_t1599402279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenDebug::OnGUI()
extern "C"  void ScreenDebug_OnGUI_m787630878 (ScreenDebug_t1599402279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
