﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProgressBar.ProgressRadialBehaviour
struct ProgressRadialBehaviour_t2198524043;

#include "codegen/il2cpp-codegen.h"

// System.Void ProgressBar.ProgressRadialBehaviour::.ctor()
extern "C"  void ProgressRadialBehaviour__ctor_m4118557090 (ProgressRadialBehaviour_t2198524043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ProgressBar.ProgressRadialBehaviour::get_Value()
extern "C"  float ProgressRadialBehaviour_get_Value_m3819232596 (ProgressRadialBehaviour_t2198524043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar.ProgressRadialBehaviour::set_Value(System.Single)
extern "C"  void ProgressRadialBehaviour_set_Value_m4154902679 (ProgressRadialBehaviour_t2198524043 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ProgressBar.ProgressRadialBehaviour::get_TransitoryValue()
extern "C"  float ProgressRadialBehaviour_get_TransitoryValue_m1525230865 (ProgressRadialBehaviour_t2198524043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar.ProgressRadialBehaviour::set_TransitoryValue(System.Single)
extern "C"  void ProgressRadialBehaviour_set_TransitoryValue_m4230546426 (ProgressRadialBehaviour_t2198524043 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProgressBar.ProgressRadialBehaviour::get_isDone()
extern "C"  bool ProgressRadialBehaviour_get_isDone_m3339697635 (ProgressRadialBehaviour_t2198524043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProgressBar.ProgressRadialBehaviour::get_isPaused()
extern "C"  bool ProgressRadialBehaviour_get_isPaused_m1222803599 (ProgressRadialBehaviour_t2198524043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar.ProgressRadialBehaviour::Start()
extern "C"  void ProgressRadialBehaviour_Start_m3065694882 (ProgressRadialBehaviour_t2198524043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar.ProgressRadialBehaviour::Update()
extern "C"  void ProgressRadialBehaviour_Update_m553113003 (ProgressRadialBehaviour_t2198524043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar.ProgressRadialBehaviour::SetFillerSize(System.Single)
extern "C"  void ProgressRadialBehaviour_SetFillerSize_m1120266744 (ProgressRadialBehaviour_t2198524043 * __this, float ___fill0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar.ProgressRadialBehaviour::SetFillerSizeAsPercentage(System.Single)
extern "C"  void ProgressRadialBehaviour_SetFillerSizeAsPercentage_m2120422124 (ProgressRadialBehaviour_t2198524043 * __this, float ___Percent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar.ProgressRadialBehaviour::OnComplete()
extern "C"  void ProgressRadialBehaviour_OnComplete_m3171670234 (ProgressRadialBehaviour_t2198524043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar.ProgressRadialBehaviour::IncrementValue(System.Single)
extern "C"  void ProgressRadialBehaviour_IncrementValue_m1811676999 (ProgressRadialBehaviour_t2198524043 * __this, float ___inc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar.ProgressRadialBehaviour::DecrementValue(System.Single)
extern "C"  void ProgressRadialBehaviour_DecrementValue_m638797419 (ProgressRadialBehaviour_t2198524043 * __this, float ___dec0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
