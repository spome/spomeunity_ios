﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen1462553450MethodDeclarations.h"

// System.Void System.Func`2<TMPro.TMP_Glyph,System.Int32>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1334643005(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t4216775979 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3664646529_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<TMPro.TMP_Glyph,System.Int32>::Invoke(T)
#define Func_2_Invoke_m3986588057(__this, ___arg10, method) ((  int32_t (*) (Func_2_t4216775979 *, TMP_Glyph_t1999257094 *, const MethodInfo*))Func_2_Invoke_m3426833477_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<TMPro.TMP_Glyph,System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m297035212(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t4216775979 *, TMP_Glyph_t1999257094 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m3471527160_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<TMPro.TMP_Glyph,System.Int32>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m1760357339(__this, ___result0, method) ((  int32_t (*) (Func_2_t4216775979 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m2021309615_gshared)(__this, ___result0, method)
