﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TMPro.TMP_Settings
struct TMP_Settings_t2708659969;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t967550395;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t3955808645;
// TMPro.TMP_StyleSheet
struct TMP_StyleSheet_t3103963276;

#include "codegen/il2cpp-codegen.h"

// System.Void TMPro.TMP_Settings::.ctor()
extern "C"  void TMP_Settings__ctor_m374285686 (TMP_Settings_t2708659969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TMP_Settings TMPro.TMP_Settings::get_instance()
extern "C"  TMP_Settings_t2708659969 * TMP_Settings_get_instance_m548729892 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TMP_Settings TMPro.TMP_Settings::LoadDefaultSettings()
extern "C"  TMP_Settings_t2708659969 * TMP_Settings_LoadDefaultSettings_m3325775418 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TMP_Settings TMPro.TMP_Settings::GetSettings()
extern "C"  TMP_Settings_t2708659969 * TMP_Settings_GetSettings_m2187819093 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TMP_FontAsset TMPro.TMP_Settings::GetFontAsset()
extern "C"  TMP_FontAsset_t967550395 * TMP_Settings_GetFontAsset_m561656429 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TMP_SpriteAsset TMPro.TMP_Settings::GetSpriteAsset()
extern "C"  TMP_SpriteAsset_t3955808645 * TMP_Settings_GetSpriteAsset_m1444944941 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TMPro.TMP_StyleSheet TMPro.TMP_Settings::GetStyleSheet()
extern "C"  TMP_StyleSheet_t3103963276 * TMP_Settings_GetStyleSheet_m71424171 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
