﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3113111828.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_TMPro_XML_TagAttribute35801856.h"

// System.Void System.Array/InternalEnumerator`1<TMPro.XML_TagAttribute>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1256556117_gshared (InternalEnumerator_1_t3113111828 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1256556117(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3113111828 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1256556117_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<TMPro.XML_TagAttribute>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m70982955_gshared (InternalEnumerator_1_t3113111828 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m70982955(__this, method) ((  void (*) (InternalEnumerator_1_t3113111828 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m70982955_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<TMPro.XML_TagAttribute>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m181330135_gshared (InternalEnumerator_1_t3113111828 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m181330135(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3113111828 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m181330135_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<TMPro.XML_TagAttribute>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3994158700_gshared (InternalEnumerator_1_t3113111828 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3994158700(__this, method) ((  void (*) (InternalEnumerator_1_t3113111828 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3994158700_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<TMPro.XML_TagAttribute>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2936644375_gshared (InternalEnumerator_1_t3113111828 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2936644375(__this, method) ((  bool (*) (InternalEnumerator_1_t3113111828 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2936644375_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<TMPro.XML_TagAttribute>::get_Current()
extern "C"  XML_TagAttribute_t35801856  InternalEnumerator_1_get_Current_m2990750556_gshared (InternalEnumerator_1_t3113111828 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2990750556(__this, method) ((  XML_TagAttribute_t35801856  (*) (InternalEnumerator_1_t3113111828 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2990750556_gshared)(__this, method)
