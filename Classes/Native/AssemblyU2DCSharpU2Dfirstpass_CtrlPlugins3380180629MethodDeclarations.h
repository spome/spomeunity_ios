﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CtrlPlugins
struct CtrlPlugins_t3380180629;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void CtrlPlugins::.ctor()
extern "C"  void CtrlPlugins__ctor_m1274795330 (CtrlPlugins_t3380180629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::Wikitude_image(System.String)
extern "C"  void CtrlPlugins_Wikitude_image_m584846514 (Il2CppObject * __this /* static, unused */, String_t* ___js0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::Wikitude_video(System.String)
extern "C"  void CtrlPlugins_Wikitude_video_m729742738 (Il2CppObject * __this /* static, unused */, String_t* ___js0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::Wikitude_(System.String,System.Single,System.Single)
extern "C"  void CtrlPlugins_Wikitude__m3979698143 (Il2CppObject * __this /* static, unused */, String_t* ___js0, float ___latitude1, float ___longtitude2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::TakeAlbumAction(System.Int32)
extern "C"  void CtrlPlugins_TakeAlbumAction_m1557766895 (Il2CppObject * __this /* static, unused */, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::MKMapAction()
extern "C"  void CtrlPlugins_MKMapAction_m961154292 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::open_(System.Single,System.Single)
extern "C"  void CtrlPlugins_open__m1429916571 (Il2CppObject * __this /* static, unused */, float ___latitude0, float ___longtitude1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::close_()
extern "C"  void CtrlPlugins_close__m2296396553 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::Alert_ios_(System.String,System.String)
extern "C"  void CtrlPlugins_Alert_ios__m3736249511 (Il2CppObject * __this /* static, unused */, String_t* ___result0, String_t* ___sentence1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::Login_(System.Int32)
extern "C"  void CtrlPlugins_Login__m552377705 (Il2CppObject * __this /* static, unused */, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::Init()
extern "C"  void CtrlPlugins_Init_m3317795026 (CtrlPlugins_t3380180629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::isWikitude_image(System.String)
extern "C"  void CtrlPlugins_isWikitude_image_m2952737800 (Il2CppObject * __this /* static, unused */, String_t* ___js0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::isWikitude_video(System.String)
extern "C"  void CtrlPlugins_isWikitude_video_m3097634024 (Il2CppObject * __this /* static, unused */, String_t* ___js0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::isWikitude(System.String,System.Single,System.Single)
extern "C"  void CtrlPlugins_isWikitude_m2708272430 (Il2CppObject * __this /* static, unused */, String_t* ___js0, float ___latitude1, float ___longtitude2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::isMKMapAction()
extern "C"  void CtrlPlugins_isMKMapAction_m1390583882 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::doTakeAlbumAction(System.Int32)
extern "C"  void CtrlPlugins_doTakeAlbumAction_m2126393988 (Il2CppObject * __this /* static, unused */, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::Open(System.Single,System.Single)
extern "C"  void CtrlPlugins_Open_m1700334148 (Il2CppObject * __this /* static, unused */, float ___latitude0, float ___longtitude1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::Close()
extern "C"  void CtrlPlugins_Close_m2985654872 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::Alert_ios(System.String,System.String)
extern "C"  void CtrlPlugins_Alert_ios_m486937204 (Il2CppObject * __this /* static, unused */, String_t* ___result0, String_t* ___sentence1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CtrlPlugins::Login(System.Int32)
extern "C"  void CtrlPlugins_Login_m1267639130 (Il2CppObject * __this /* static, unused */, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
