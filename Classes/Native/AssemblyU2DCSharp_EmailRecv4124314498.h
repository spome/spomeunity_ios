﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EmailRecv
struct  EmailRecv_t4124314498  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject EmailRecv::m_Button
	GameObject_t3674682005 * ___m_Button_2;
	// UnityEngine.GameObject EmailRecv::m_Parent
	GameObject_t3674682005 * ___m_Parent_3;
	// UnityEngine.GameObject EmailRecv::m_TargetGO
	GameObject_t3674682005 * ___m_TargetGO_4;

public:
	inline static int32_t get_offset_of_m_Button_2() { return static_cast<int32_t>(offsetof(EmailRecv_t4124314498, ___m_Button_2)); }
	inline GameObject_t3674682005 * get_m_Button_2() const { return ___m_Button_2; }
	inline GameObject_t3674682005 ** get_address_of_m_Button_2() { return &___m_Button_2; }
	inline void set_m_Button_2(GameObject_t3674682005 * value)
	{
		___m_Button_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Button_2, value);
	}

	inline static int32_t get_offset_of_m_Parent_3() { return static_cast<int32_t>(offsetof(EmailRecv_t4124314498, ___m_Parent_3)); }
	inline GameObject_t3674682005 * get_m_Parent_3() const { return ___m_Parent_3; }
	inline GameObject_t3674682005 ** get_address_of_m_Parent_3() { return &___m_Parent_3; }
	inline void set_m_Parent_3(GameObject_t3674682005 * value)
	{
		___m_Parent_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_Parent_3, value);
	}

	inline static int32_t get_offset_of_m_TargetGO_4() { return static_cast<int32_t>(offsetof(EmailRecv_t4124314498, ___m_TargetGO_4)); }
	inline GameObject_t3674682005 * get_m_TargetGO_4() const { return ___m_TargetGO_4; }
	inline GameObject_t3674682005 ** get_address_of_m_TargetGO_4() { return &___m_TargetGO_4; }
	inline void set_m_TargetGO_4(GameObject_t3674682005 * value)
	{
		___m_TargetGO_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_TargetGO_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
