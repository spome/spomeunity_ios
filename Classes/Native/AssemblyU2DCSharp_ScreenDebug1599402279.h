﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenDebug
struct  ScreenDebug_t1599402279  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GUIStyle ScreenDebug::debugStyle
	GUIStyle_t2990928826 * ___debugStyle_3;

public:
	inline static int32_t get_offset_of_debugStyle_3() { return static_cast<int32_t>(offsetof(ScreenDebug_t1599402279, ___debugStyle_3)); }
	inline GUIStyle_t2990928826 * get_debugStyle_3() const { return ___debugStyle_3; }
	inline GUIStyle_t2990928826 ** get_address_of_debugStyle_3() { return &___debugStyle_3; }
	inline void set_debugStyle_3(GUIStyle_t2990928826 * value)
	{
		___debugStyle_3 = value;
		Il2CppCodeGenWriteBarrier(&___debugStyle_3, value);
	}
};

struct ScreenDebug_t1599402279_StaticFields
{
public:
	// System.Int32 ScreenDebug::SCREEN_DENSITY
	int32_t ___SCREEN_DENSITY_2;

public:
	inline static int32_t get_offset_of_SCREEN_DENSITY_2() { return static_cast<int32_t>(offsetof(ScreenDebug_t1599402279_StaticFields, ___SCREEN_DENSITY_2)); }
	inline int32_t get_SCREEN_DENSITY_2() const { return ___SCREEN_DENSITY_2; }
	inline int32_t* get_address_of_SCREEN_DENSITY_2() { return &___SCREEN_DENSITY_2; }
	inline void set_SCREEN_DENSITY_2(int32_t value)
	{
		___SCREEN_DENSITY_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
