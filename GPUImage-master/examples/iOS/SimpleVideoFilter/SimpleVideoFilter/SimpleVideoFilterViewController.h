#import <UIKit/UIKit.h>
#import "GPUImage.h"

@interface SimpleVideoFilterViewController : UIViewController
{
    GPUImageVideoCamera *videoCamera;
    //GPUImageOutput<GPUImageInput> *filter;
    //GPUImageMovieWriter *movieWriter;
    
    GPUImageMovie *movieFile;
    GPUImageView *filterView;
    GPUImageView *filterView2;
    GPUImageChromaKeyBlendFilter *filter;
    GPUImageSepiaFilter *filter2;
    GPUImageMovieWriter* movieWriter;
    GPUImagePicture* picture;
}

//- (IBAction)updateSliderValue:(id)sender;

@end
