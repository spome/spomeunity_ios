//
//  ViewController.h
//  Unity-iPhone
//
//  Created by 田島 on 2016/09/23.
//
//
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController<MKMapViewDelegate>

@property IBOutlet MKMapView *mapView;

@end