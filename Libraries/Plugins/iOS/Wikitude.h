//
//  Wikitude.h
//  Unity-iPhone
//
//  Created by 田島 on 2016/09/28.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "GPUImage.h"

@interface Wikitude : UIViewController
{
    
    UIViewController *unitypickerview;
    
    UIImagePickerController *picker;
    AVAssetWriterInput *assetWriterVideoIn;
    AVAssetWriterInput *assetWriterAudioIn;
    AVAssetWriter *assetWriter;
    NSURL *outputURL;
    
    GPUImagePicture *picture;
    GPUImageMovie *movieFile;
    GPUImageView *filterView;
    
    UIView *background;
    UIButton *btn_video;
    
    int type;
}

- (id)initWikitude:(const char *)js_  latitude:(float)latitude longtitude:(float)longtitude;

@end

@interface AVPlayerView : UIView

@end