//
//  ViewController.m
//  Unity-iPhone
//
//  Created by 田島 on 2016/09/23.
//
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Delegate をセット
    self.mapView.delegate = self;
    
    // 緯度・軽度を設定
    CLLocationCoordinate2D location;
    location.latitude = 35.68154;
    location.longitude = 139.752498;
    
    [self.mapView setCenterCoordinate:location animated:NO];
    
    // 縮尺を設定
    MKCoordinateRegion region = self.mapView.region;
    region.center = location;
    region.span.latitudeDelta = 0.02;
    region.span.longitudeDelta = 0.02;
    
    [self.mapView setRegion:region animated:NO];
    
    // 表示タイプを航空写真と地図のハイブリッドに設定
    self.mapView.mapType = MKMapTypeHybrid;
    
    // view に追加
    [self.view addSubview:self.mapView];
}

@end
