//
//  PluginConnector.m
//  Unity-iPhone
//
//  Created by 田島 on 2016/09/23.
//
//

#import "ViewController.h"

extern "C"{
    UIView *uv;
    MKMapView *mv;
    UIViewController* parent;
    
    void Alert_ios_(const char *result_,const char *sentence_){
        NSString *result = [NSString stringWithUTF8String:result_];
        NSString *sentence = [NSString stringWithUTF8String:sentence_];
        
        parent = UnityGetGLViewController();
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:result
                                                                       message:sentence
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [parent presentViewController:alert animated:YES completion:nil];
        
        
    }
    
    
    void Login_(int num)
    {
        if(num == 1){
            parent = UnityGetGLViewController();
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"ログイン成功"
                                                                           message:@"ログインに成功しました。"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [parent presentViewController:alert animated:YES completion:nil];
            
        }
        if(num == 2){
            parent = UnityGetGLViewController();
            
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"ログイン失敗"
                                                                           message:@"IDまたはパスワードが違います。"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [parent presentViewController:alert animated:YES completion:nil];
            
        }
    }
    
    void open_ (float latitude, float longtitude)
    {
            // 生成
            mv = [[MKMapView alloc] init];
            mv.frame = 	CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,([UIScreen mainScreen].bounds.size.height) * 0.9f);
        
            //
            CLLocationCoordinate2D co;
            co.latitude = latitude; // 経度
            co.longitude = longtitude; // 緯度
            [mv setCenterCoordinate:co animated:NO];
        
            // 縮尺を指定
            MKCoordinateRegion cr = mv.region;
            cr.center = co;
            cr.span.latitudeDelta = 0.02;
            cr.span.longitudeDelta = 0.02;
            [mv setRegion:cr animated:NO];
        
            // addSubview
            [parent.view addSubview:mv];
        
            MKPointAnnotation* pin = [[MKPointAnnotation alloc] init];
            pin.coordinate = CLLocationCoordinate2DMake(co.latitude, co.longitude);
            [mv addAnnotation:pin];
    }
    void close_()
    {
        
        [uv removeFromSuperview];
        [mv removeFromSuperview];
    }
}