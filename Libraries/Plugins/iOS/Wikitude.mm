//
//  Wikitude.m
//  Unity-iPhone
//
//  Created by 田島 on 2016/09/28.
//
//

#import "Wikitude.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <mach/mach_time.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Photos/Photos.h>
#import "ViewController.h"

extern "C" UIViewController *UnityGetGLViewController();

@interface Wikitude () <UINavigationControllerDelegate, UIImagePickerControllerDelegate,MKMapViewDelegate,UIGestureRecognizerDelegate>

@property (nonatomic, strong) AVPlayerView *playerView;
@property (nonatomic, strong) AVPlayer     *player;
@property (nonatomic, strong) ALAssetsLibrary *assetsLibrary;
@property (nonatomic, strong) NSMutableArray *assets;
@property (strong, nonatomic) NSMutableString *mstSelectedImage;
@property  MKMapView *_map;
@end

@implementation AVPlayerView

+(Class)layerClass
{
    return [AVPlayerLayer class];
}
@end


@implementation Wikitude

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(id)initTakeAlbumAction:(int)num_{
    
    self = [super init];
    
    unitypickerview = UnityGetGLViewController();
    if(num_ == 0){
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            [imagePickerController setAllowsEditing:NO];
            imagePickerController.delegate = self;
            type = 0;
            [unitypickerview presentViewController:imagePickerController animated:YES completion:nil];
        }
    }
    if(num_ == 1){
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            UIImagePickerController* videoPickerController = [[UIImagePickerController alloc]init];
            videoPickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            videoPickerController.mediaTypes =[UIImagePickerController availableMediaTypesForSourceType:videoPickerController.sourceType];
            videoPickerController.allowsEditing = NO;
            videoPickerController.delegate = self;
            type = 1;
            [unitypickerview presentViewController:videoPickerController animated:YES completion:nil];
        }
    }
    
    return self;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(type == 0){
        UIImage *myUIImage = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSData *imageData = UIImagePNGRepresentation(myUIImage);
        
        NSLog(@"myData:%lu",(unsigned long)[imageData length]);
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        // 適当なファイル名をつける.
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"tmp.png"];
        [imageData writeToFile:filePath atomically:YES];
        NSLog(@"filePath = %@",filePath);
        _mstSelectedImage = (NSMutableString *)filePath;
        
        UnitySendMessage("メッセージを登録_Panel", "AndroidAlbumFileName", "tmp.png");
        UnitySendMessage("メッセージを登録_Panel", "AndroidAlbumPath", MakeStringCopy([_mstSelectedImage UTF8String]));
        
    }
    if(type == 1){
        NSURL *videoPath = [info objectForKey:UIImagePickerControllerReferenceURL];
        NSString *videoPathurl = [self videoAssetURLToTempFile:videoPath];
        NSLog(@"%@",videoPathurl);
        const char *videoPathurl_char = [videoPathurl UTF8String];
        UnitySendMessage("メッセージを登録_Panel", "AndroidAlbumFileName", "TestName.mov");
        UnitySendMessage("メッセージを登録_Panel", "AndroidAlbumPath", videoPathurl_char);
    }
    
    [unitypickerview dismissViewControllerAnimated:YES completion:nil];
}

char* MakeStringCopy (const char* string)
{
    if (string == NULL)
        return NULL;
    
    char* res = (char*)malloc(strlen(string) + 1);
    strcpy(res, string);
    return res;
}

-(NSString*) videoAssetURLToTempFile:(NSURL*)url
{
    
    NSString * surl = [url absoluteString];
    NSString * ext = [surl substringFromIndex:[surl rangeOfString:@"ext="].location + 4];
    NSTimeInterval ti = [[NSDate date]timeIntervalSinceReferenceDate];
    NSString * filename = [NSString stringWithFormat: @"%f.%@",ti,ext];
    NSString * tmpfile = [NSTemporaryDirectory() stringByAppendingPathComponent:filename];
    
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
    {
        
        ALAssetRepresentation * rep = [myasset defaultRepresentation];
        
        NSUInteger size = [rep size];
        const int bufferSize = 8192;
        
        NSLog(@"Writing to %@",tmpfile);
        FILE* f = fopen([tmpfile cStringUsingEncoding:1], "wb+");
        if (f == NULL) {
            NSLog(@"Can not create tmp file.");
            return;
        }
        
        Byte * buffer = (Byte*)malloc(bufferSize);
        long int read = 0, offset = 0, written = 0;
        NSError* err;
        if (size != 0) {
            do {
                read = [rep getBytes:buffer
                          fromOffset:offset
                              length:bufferSize
                               error:&err];
                written = fwrite(buffer, sizeof(char), read, f);
                offset += read;
            } while (read != 0);
            
            
        }
        fclose(f);
    };
    
    ALAssetsLibraryAccessFailureBlock failureblock  = ^(NSError *myerror)
    {
        NSLog(@"Can not get asset - %@",[myerror localizedDescription]);
        
    };
    
    if(url)
    {
        ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
        [assetslibrary assetForURL:url
                       resultBlock:resultblock
                      failureBlock:failureblock];
    }
    return tmpfile;
}

-(UIImage*)photoFromALAssets:(NSURL*)imageurl
{
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    ALAssetsLibrary* library = [[ALAssetsLibrary alloc] init];
    
    __block UIImage* image = nil;
    //ALAssetsLibrary読み込みの待ち合わせ
    dispatch_async(queue,
                   ^{
                       [library assetForURL:imageurl
                                resultBlock: ^(ALAsset *asset)
                        {
                            ALAssetRepresentation *representation;
                            representation = [asset defaultRepresentation];
                            image = [[UIImage alloc] initWithCGImage:representation.fullResolutionImage];
                            dispatch_semaphore_signal(sema);
                        }
                               failureBlock:^(NSError *error) {
                                   NSLog(@"error:%@", error);
                                   dispatch_semaphore_signal(sema);
                               }];
                   });
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    return image;
}
- (id)initWikitude_image:(const char *)js_{

     self = [super init];
    
    return self;
}

- (id)initWikitude_video:(const char *)js_{
    
    self = [super init];
    
    NSString *js = [NSString stringWithUTF8String:js_];
    NSLog(@"%@",js);
    NSURL* url = [NSURL fileURLWithPath:js];
    
    UIViewController *unityvc = UnityGetGLViewController();
    
    background = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].applicationFrame.size.height* 1.2f)];
    
    [background setBackgroundColor:[UIColor blackColor]];
    
    self.player = [[AVPlayer alloc] initWithURL:url];
    // viewのlayerに`AVPlayer`のインスタンスをセット
    self.playerView = [[AVPlayerView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].applicationFrame.size.height)];
    [(AVPlayerLayer*)self.playerView.layer setPlayer:self.player];
    
    // 該当のビューを表示
    [unityvc.view  addSubview:background];
    [unityvc.view  addSubview:self.playerView];
    [self.player play];
    NSLog(@"a");
    
    btn_video = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn_video.frame = CGRectMake([UIScreen mainScreen].applicationFrame.size.width * 0.3f, [UIScreen mainScreen].applicationFrame.size.height * 0.85, 150, 50);
    //btn.layer.position = CGPoint(self.view.frame.width/2, 100);
    [btn_video setTitle:@"キャンセル" forState:UIControlStateNormal];
    [btn_video setTitle:@"OK" forState:UIControlStateHighlighted];
    [btn_video setTitle:@"押せません" forState:UIControlStateDisabled];
    [btn_video.titleLabel setFont:[UIFont systemFontOfSize:25]];
    // ボタンがタッチダウンされた時にhogeメソッドを呼び出す
    [btn_video addTarget:self action:@selector(hoge_video:)forControlEvents:UIControlEventTouchDown];
    [unityvc.view addSubview:btn_video];
    
    return self;
}
-(void)hoge_video:(UIButton*)button{
    [self.player pause];
    [background removeFromSuperview];
    [btn_video removeFromSuperview];
    [self.playerView removeFromSuperview];
       UnitySendMessage("EventSystem", "MeChekClose","");
}

- (id)initWikitude:(const char *)js_  latitude:(float)latitude longtitude:(float)longtitude{
    
    self = [super init];
    
    //UIViewController *unityvc = UnityGetGLViewController();
    
    picker = [[UIImagePickerController alloc] init];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [picker setSourceType:UIImagePickerControllerSourceTypeCamera];
        //UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"image.png"]];
        //imageView.frame = CGRectMake(100,200,50,50);
        //picker.cameraOverlayView = imageView;
        picker.showsCameraControls = NO;
        // 動画のURLを生成
        NSString *js = [NSString stringWithUTF8String:js_];
        NSLog(@"%@",js);
        NSURL* url = [NSURL fileURLWithPath:js];
        
        
            
        UIViewController *unityvc = UnityGetGLViewController();
        //UIView *view = UnityGetGLViewController().view;
        
        //NSURL *fileURL = [[NSBundle mainBundle] URLForResource:@"AR_ColorTEST1" withExtension:@"mp4"];
        
        filterView = [[GPUImageView alloc]  initWithFrame:CGRectMake(-170, -400, [UIScreen mainScreen].bounds.size.width * 2, [UIScreen mainScreen].applicationFrame.size.height * 2)];
        //filterView = [[GPUImageView alloc]  initWithFrame:CGRectMake(10,10,100,30)];
        
           // GPUImageVideoCamera *videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPreset640x480 cameraPosition:AVCaptureDevicePositionBack];
            
        NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"image2" ofType:@"png"];
        UIImage  *image     = [UIImage imageWithContentsOfFile:imagePath];
        picture = [[GPUImagePicture alloc] initWithImage:image smoothlyScaleOutput:YES];
            
        GPUImageChromaKeyBlendFilter *filter = [[GPUImageChromaKeyBlendFilter alloc] init];
        [filter setColorToReplaceRed:0.0 green:0.99 blue:0.0];
        
        movieFile = [[GPUImageMovie alloc] initWithURL:url];
        movieFile.playAtActualSpeed = YES;
        
        [movieFile addTarget:filter];
        [picture addTarget:filter];
        
        [filterView setBackgroundColor:[UIColor clearColor]];
        [filterView setOpaque:NO];
            
        [filter addTarget:filterView];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btn.frame = CGRectMake([UIScreen mainScreen].applicationFrame.size.width * 0.3f, [UIScreen mainScreen].applicationFrame.size.height * 0.85, 150, 50);
        //btn.layer.position = CGPoint(self.view.frame.width/2, 100);
        [btn setTitle:@"キャンセル" forState:UIControlStateNormal];
        [btn setTitle:@"OK" forState:UIControlStateHighlighted];
        [btn setTitle:@"押せません" forState:UIControlStateDisabled];
        [btn.titleLabel setFont:[UIFont systemFontOfSize:25]];
        // ボタンがタッチダウンされた時にhogeメソッドを呼び出す
        [btn addTarget:self action:@selector(hoge:)
        forControlEvents:UIControlEventTouchDown];
        
        [self.view addSubview:filterView];
        [self.view addSubview:btn];
        
        picker.cameraOverlayView = self.view;
            
        [unityvc presentViewController:picker animated:YES completion:nil];
            
        [picture processImage];
        [movieFile startProcessing];
        
        //[self readMovie:url];
        
        //NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"/var/mobile/Containers/Data/Application/F1FA56B6-2BFA-49EA-BA9E-F96AB1DA7D0F/Documents" ofType:@"mp4"]];
        //NSURL *url = [NSURL URLWithString:js];
        
        // URLを元に`AVPlayer`を生成
        
       // self.player = [[AVPlayer alloc] initWithURL:url];
        
        // viewのlayerに`AVPlayer`のインスタンスをセット
      //  self.playerView = [[AVPlayerView alloc] initWithFrame:self.view.frame];
      //  [(AVPlayerLayer*)self.playerView.layer setPlayer:self.player];
        
        // 該当のビューを表示
        //[self.view addSubview:self.playerView];
        //picker.cameraOverlayView = self.playerView;
       // [self.player play];
    }
      return self;
}

-(void)hoge:(UIButton*)button{
    [movieFile cancelProcessing];
    [filterView removeFromSuperview];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (id)initMKMapAction{
    
    self = [super init];
    UIView *mkmapunity = UnityGetGLViewController().view;
    
    __map = [[MKMapView alloc] init];
    __map.frame = CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,([UIScreen mainScreen].bounds.size.height));
    
    UILongPressGestureRecognizer *longPressGesture;
    longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                     action:@selector(handleLongPressGesture:)];
    [__map addGestureRecognizer:longPressGesture];
    __map.delegate = self;
    [mkmapunity addSubview:__map];
    return self;
}

- (void)handleLongPressGesture:(UILongPressGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan) {  // 長押し検出開始時のみ動作
        
        CGPoint touchedPoint = [gesture locationInView:__map];
        
        NSLog(@"touchedPoint x:[%f]", touchedPoint.x);
        NSLog(@"touchedPoint y:[%f]", touchedPoint.y);
        CLLocationCoordinate2D touchCoordinate = [__map convertPoint:touchedPoint toCoordinateFromView:__map];
        
        NSLog(@"touchCoordinate latitude:%f  longitude:%f", touchCoordinate.latitude, touchCoordinate.longitude);
        NSString *strx = [NSString stringWithFormat:@"%f", touchCoordinate.latitude];
        NSString *stry = [NSString stringWithFormat:@"%f", touchCoordinate.longitude];
        const char *x = [strx UTF8String];
        const char *y = [stry UTF8String];
        UnitySendMessage("メッセージを登録_Panel", "mapLat", x);
        UnitySendMessage("メッセージを登録_Panel", "mapLon", y);
        
        
        [self setAnnotation:touchCoordinate mapMove:NO animated:NO];
    }
}
-(void)setAnnotation:(CLLocationCoordinate2D) point mapMove:(BOOL)mapMove
            animated:(BOOL)animated{
    // ピンを全て削除
    [__map removeAnnotations: __map.annotations];
    // 新しいピンを作成
    MKPointAnnotation *anno = [[MKPointAnnotation alloc] init];
    anno.coordinate = point;
    
    // マップの表示を変更
    if (mapMove) {
        MKCoordinateSpan CoordinateSpan = MKCoordinateSpanMake(0.005,0.005);
        MKCoordinateRegion CoordinateRegion = MKCoordinateRegionMake(point,CoordinateSpan);
        [__map setRegion:CoordinateRegion animated:animated];
    }
    // ピンを追加
    [__map addAnnotation:anno];
    
    // ピンの周りに円を表示
    MKCircle* circle = [MKCircle circleWithCenterCoordinate:point radius:500];  // 半径500m
    [__map removeOverlays:__map.overlays];
    [__map addOverlay:circle];
    [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:2.0f]];
    [__map removeFromSuperview];
    UnitySendMessage("EventSystem", "MeChekClose","");
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView
            viewForAnnotation:(id )annotation
{
    static NSString* Identifier = @"PinAnnotationIdentifier";
    MKPinAnnotationView* pinView;
    pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:Identifier];
    
    if (pinView == nil) {
        pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                  reuseIdentifier:Identifier];
        pinView.animatesDrop = YES;
        return pinView;
    }
    pinView.annotation = annotation;
    return pinView;
}
/*
-(id)initMessageChekClose{
    [__map removeFromSuperview];
    return self;
}
*/

@end

extern "C" {
    void *Wikitude_(const char *js,float latitude, float longtitude);
    void *Wikitude_image(const char *js);
    void *Wikitude_video(const char *js);
    void *TakeAlbumAction(int num);
    void *MKMapAction();
    //void *MKMapActionclose();
}
void *Wikitude_(const char *js,float latitude, float longtitude){
    
    id instance = [[Wikitude alloc] initWikitude:js latitude:latitude longtitude:longtitude];
    return (__bridge_retained void *)instance;
}

void *Wikitude_image(const char *js){
    
    id instance = [[Wikitude alloc] initWikitude_image:js];
    return (__bridge_retained void *)instance;
}

void *Wikitude_video(const char *js){
    
    id instance = [[Wikitude alloc] initWikitude_video:js];
    return (__bridge_retained void *)instance;
}

void *TakeAlbumAction(int num){
    
    id instance = [[Wikitude alloc] initTakeAlbumAction:num];
    return (__bridge_retained void *)instance;
}

void *MKMapAction(){
    id instance = [[Wikitude alloc] initMKMapAction];
    return (__bridge_retained void *)instance;
}
/*
void *MKMapActionclose(){
    id instance = [[Wikitude alloc] initMessageChekClose];
    return (__bridge_retained void *)instance;
}*/